<?php

class M_data_owner_part extends CI_Model  {
    
		
	function __construct()
    {
        parent::__construct();
		
    }
	function get_dataOwner()
	{
		 $query = $this->_get_dataOwner();
        if ($_POST['length'] != -1)
            $query .= " limit " . $_POST['start'] . "," . $_POST['length'];
        return $this->db->query($query)->result();
 
	}
	  public function counts() {
        $query = $this->_get_dataOwner();
        return $this->db->query($query)->num_rows();
    }
	function _get_dataOwner()
	{
        $query = "SELECT * FROM data_owner WHERE 1=1 ";
        if (isset($_POST['search']['value'])) {
            $searchkey = $_POST['search']['value'];
            $query .= " AND (
			nama LIKE '%" . $searchkey . "%' or 
			alamat LIKE '%" . $searchkey . "%' or
			hp LIKE '%" . $searchkey . "%' or
			hp2 LIKE '%" . $searchkey . "%' or
			email LIKE '%" . $searchkey . "%' 
			) ";
        }

        $column = array('', '', 'hp2', 'nama', 'hp', 'email', 'alamat');
        $i = 0;
        foreach ($column as $item) {
            $column[$i] = $item;
        }

      /*  if (isset($_POST['order'])) {
		//	$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            $query .= " order by " . $column[$_POST['order']['0']['column']] . " " . $_POST['order']['0']['dir'];
        } else if (isset($order)) {
            $order = $order;
			//	$this->db->order_by(key($order), $order[key($order)]);
			       $query .= " order by nama ASC";
        }*/
		$query.=" order by nama ASC" ;
        return $query;
	}
	
	function getKode()
	{
			  $carikode = $this->db->query("SELECT max(id_owner) as id_owner from data_owner")->row();
			  $datakode =$carikode->id_owner;//$carikode->kode;
			  if ($datakode) {
		  	    $kode = (int) $datakode;
		   		return	$newID = sprintf("%02s", $kode+1);
		   	  } else {
				return "01";
			  }
	}
	function getKodeAgen()
	{
			  $carikode = $this->db->query("SHOW TABLE STATUS LIKE 'data_owner'")->row();
			  $datakode =isset($carikode->Auto_increment)?($carikode->Auto_increment):"1";//$carikode->kode;
		  	    $kode = (int) $datakode;
		   		$newID = sprintf("%03s", $kode);
				$tahun=date('Y');
				$bulan=date('m');
				$bulan=$this->tanggal->bulanRomawi($bulan);
		   	 return "BREA/$newID/$bulan/$tahun";			  
	}
	function cekOwner($kode,$hp1,$hp2,$email,$alamat)
	{
		$where="";
		if($hp1){
		$where.=" OR hp='".$hp1."'";
		}
	 
		if($hp2){
		$where.=" OR hp2='".$hp2."'";
		}
		 
		if($email){
		$where.=" OR email='".$email."'";
		}
		 
		if($alamat){
		$where.=" OR alamat='".$alamat."'";
		}
		if($where!=""){
			$where = substr($where,3);
		return $this->db->query("SELECT * from data_owner where ".$where."  ")->num_rows();
		}else{return 0;};
	}
	function cekOwnerEdit($kode,$hp1,$hp2,$email,$alamat)
	{
		$where="";
		if($hp1){
		$where.=" OR hp='".$hp1."'";
		}
	 
		if($hp2){
		$where.=" OR hp2='".$hp2."'";
		}
		 
		if($email){
		$where.=" OR email='".$email."'";
		}
		 
		if($alamat){
		$where.=" OR alamat='".$alamat."'";
		}
		if($where!=""){
			$where = substr($where,3);
		return $this->db->query("SELECT * from data_owner where (".$where.") AND id_owner!='".$kode."' ")->num_rows();
		}else{return 0;};
	}
	function uploadGambar($form,$kode)
	{
		
		if(isset($_FILES[$form]['type']))
		{
		return	$this->upload_img($kode,$form);
		}else{
			return $this->gambarDefauld($form,$kode);
		}
	}
	function gambarDefauld($form,$kode)
	{
	$data=$this->db->get_where("data_owner",array("id_owner"=>$kode))->row();	
	return isset($data->$form)?($data->$form):"";
	}
	public function upload_img($kode,$form)
	{	//$this->m_konfig->log("admin","Upload photo");
		///
			$nama=date("YmdHis");
		  $lokasi_file = $_FILES[$form]['tmp_name'];
		  $tipe_file   = $_FILES[$form]['type'];
		  $nama_file   = $_FILES[$form]['name'];
		  //if($tipe_file)
		  //{
		  $daprof=$this->getGambarkode($kode);
			if($daprof!="")
			{
			 $path = "file_upload/owner/".$daprof;
			 if (file_exists($path)) {
				unlink($path);
				}
			}
		  
		  
			$jenis=explode(".",$nama_file);
			$nama_file=$jenis[0];
			// $jenis="jpg";
			$nama=str_replace("/","",$kode.$nama.$nama_file);
			 $target_path = "file_upload/owner/".$nama.".jpg";
			 //
	//	  }
		  //
		if (!empty($lokasi_file)) {
		move_uploaded_file($lokasi_file,$target_path);
		//if($jenis=="png"){
		//$this->konversi->UploadImageResize($target_path,$jenis,200);
		}
		return $nama.".jpg";
		}
	function insert()
	{
	$kode=$this->db->query("SELECT MAX(CONVERT(id_owner,SIGNED)+1) AS max FROM data_owner")->row();
	$kode=$kode->max;
	$data=array(
	"id_owner"=>$kode,
	"nama"=>$this->input->post("nama"),
	"hp"=>$hp1=$hp=$this->input->post("hp1"),
	"hp2"=>$hp2=$this->input->post("hp2"),
	"email"=>$email=$this->input->post("email"),
	"alamat"=>$alamat=$this->input->post("alamat"),
	"jk"=>$this->input->post("jk"),
	"poto"=>$this->uploadGambar("poto",$kode),
	"ktp"=>$this->uploadGambar("ktp",$kode),
	"kk"=>$this->uploadGambar("kk",$kode),
	"npwp"=>$this->uploadGambar("npwp",$kode),
	);
	$cek=$this->cekOwner($kode,$hp1,$hp2,$email,$alamat);
	if(!$cek){
		return $this->db->insert("data_owner",$data);
	}else
	{
		return false;
	}

	}
	
	function update()
	{
		$kode=$this->input->post("kode");
		$data=array(
		"nama"=>$this->input->post("nama"),
		"hp"=>$hp1=$hp=$this->input->post("hp1"),
		"hp2"=>$hp2=$this->input->post("hp2"),
		"email"=>$email=$this->input->post("email"),
		"alamat"=>$alamat=$this->input->post("alamat"),
		"jk"=>$this->input->post("jk"),
		"poto"=>$this->uploadGambar("poto",$kode),
		"ktp"=>$this->uploadGambar("ktp",$kode),
		"kk"=>$this->uploadGambar("kk",$kode),
		"npwp"=>$this->uploadGambar("npwp",$kode),
		);
		$cek=$this->cekOwnerEdit($kode,$hp1,$hp2,$email,$alamat);
		if(!$cek){
			$this->db->where("id_owner",$kode);
			return $this->db->update("data_owner",$data);
		}else
		{
			return false;
		}

	}
	
	function HapusAll()
	{
		$hapus=$this->input->post("hapus");
		foreach($hapus as $id)
		{
		 $this->hapus($id);
		}	return true;
	}
	function getGambarkode($id)
	{
		$this->db->where("id_owner",$id);
	return	$data=$this->db->get("data_owner")->row();
	}
	function hapus($id)
	{
		  $daprof=$this->getGambarkode($id);
			if($daprof)
			 {
				 /// hapus poto profile
				 $path = "file_upload/owner/".$daprof->poto;
				 if (file_exists($path)) {
					unlink($path);
				 }
				 ////hapus poto ktp
				  $path = "file_upload/owner/".$daprof->ktp;
				 if (file_exists($path)) {
					unlink($path);
				 } ////hapus poto kk
				  $path = "file_upload/owner/".$daprof->kk;
				 if (file_exists($path)) {
					unlink($path);
				 }
				 ////hapus poto npwp
				  $path = "file_upload/owner/".$daprof->npwp;
				 if (file_exists($path)) {
					unlink($path);
				 }
			 }
		$this->db->where("id_owner",$id);
		return	$this->db->delete("data_owner");
	}
	function export()
	{
//////start
        $objPHPExcel = new PHPExcel();
//style
        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'rotation' => 0,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '6CCECB')
            ),
            'borders' =>
            array('allborders' =>
                array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'),
                ),
            ),
        );
     //   $objPHPExcel->getActiveSheet(0)->getColumnDimension('F')->setWidth(25);
     //   $objPHPExcel->getActiveSheet(0)->getColumnDimension('G')->setWidth(25);
     //   $objPHPExcel->getActiveSheet(0)->getColumnDimension('H')->setWidth(35);




//create column
        $objPHPExcel->getActiveSheet(0)->setCellValue('A1', 'NAMA');
        $objPHPExcel->getActiveSheet(0)->setCellValue('B1', 'HP1');
        $objPHPExcel->getActiveSheet(0)->setCellValue('C1', 'HP2');
        $objPHPExcel->getActiveSheet(0)->setCellValue('D1', 'EMAIL');
        $objPHPExcel->getActiveSheet(0)->setCellValue('E1', 'ALAMAT');
        $objPHPExcel->getActiveSheet(0)->setCellValue('F1', 'KK');
        $objPHPExcel->getActiveSheet(0)->setCellValue('G1', 'KTP');
        $objPHPExcel->getActiveSheet(0)->setCellValue('H1', 'NPWP');

//make a border column
        $objPHPExcel->getActiveSheet(0)->getStyle('A1:H1')->applyFromArray($style);

        $database = $this->_get_dataAgen();
        $shit = 1;
        $database = $this->db->query($database)->result();
        foreach ($database as $list) {
            $shit++;
//create data per row
           $objPHPExcel->getActiveSheet(0)->setCellValue('A' . $shit . '', '`'.$list->nama);
          $objPHPExcel->getActiveSheet(0)->setCellValue('B' . $shit . '', $list->hp);
          $objPHPExcel->getActiveSheet(0)->setCellValue('C' . $shit . '', '`'.$list->hp2);
            $objPHPExcel->getActiveSheet(0)->setCellValue('D' . $shit . '', $list->email);
            $objPHPExcel->getActiveSheet(0)->setCellValue('E' . $shit . '', $list->alamat);          
			$objPHPExcel->getActiveSheet(0)->setCellValue('F' . $shit . '', '`'.$list->hp2);
            $objPHPExcel->getActiveSheet(0)->setCellValue('G' . $shit . '', $list->email);
            $objPHPExcel->getActiveSheet(0)->setCellValue('H' . $shit . '', $list->alamat);
 
        }

//auto size column
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('G')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('H')->setAutoSize(true);

// Rename worksheet (worksheet, not filename)
        $objPHPExcel->getActiveSheet(0)->setTitle('Data Agen');
        
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Data-Agen.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
//////finish
    
	}
	function jmlListing($id)
	{
		$this->db->where("agen",$id);
	return	$this->db->get("data_property")->num_rows();
	}
	function jmlPenjualan($id)
	{
		$this->db->where("agen",$id);
		$this->db->where("status","1");
	return	$this->db->get("data_property")->num_rows();
	}
	function jmlPelanggan($id)
	{
		$this->db->where("id_owner",$id);
	return	$this->db->get("data_pelanggan")->num_rows();
	}
	function downloadFormat()
	{
	//////start
		$objPHPExcel = new PHPExcel();
		//style
		$style = array( 
		 'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				  'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				  'rotation'   => 0,
		  ),
		   'fill' => array(
				  'type' => PHPExcel_Style_Fill::FILL_SOLID,
				  'color' => array('rgb' => 'ccccff')
			  ),
		 'borders' => 
		  array( 'allborders' => 
			array( 'style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'), 
			  ), 
			), 
		);	
		
		$head = array( 
			'font'  => array(
			'bold'  => true,
			'color' => array('rgb' => 'FFFFFF'),
			),
			
		   'fill' => array(
				  'type' => PHPExcel_Style_Fill::FILL_SOLID,
				  'color' => array('rgb' => '009966')
			  ),
		 'borders' => 
		  array( 'allborders' => 
			array( 'style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'), 
			  ), 
			), 
		);
		
		
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('A')->setWidth(25);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('B')->setWidth(25);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('C')->setWidth(25);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('D')->setWidth(25);

		
		
		//create column
		
		$objPHPExcel->getActiveSheet(0)->setCellValue('A1', 'Nama');
		$objPHPExcel->getActiveSheet(0)->setCellValue('B1', 'Nomor Hp');
		$objPHPExcel->getActiveSheet(0)->setCellValue('C1', 'Email');
		$objPHPExcel->getActiveSheet(0)->setCellValue('D1', 'Alamat');
		
		//make a border column
		$objPHPExcel->getActiveSheet(0)->getStyle('A1:D1')->applyFromArray($style);
		
		
		
		// Rename worksheet (worksheet, not filename)
		$objPHPExcel->getActiveSheet(0)->setTitle('Data Agen');
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		
		header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Form Agen.xlsx"');
		header('Cache-Control: max-age=0');
		 
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		//////finish
		
	}
	function importData()
	{
	$sukses=0;$gagal=0;$edit=0;
		 $file   = explode('.',$_FILES['userfile']['name']);
		$length = count($file);
		if($file[$length -1] == 'xlsx' || $file[$length -1] == 'xls'){//jagain barangkali uploadnya selain file excel <span class="wp-smiley wp-emoji wp-emoji-smile" title=":-)">:-)</span>
        $tmp    = $_FILES['userfile']['tmp_name'];//Baca dari tmp folder jadi file ga perlu jadi sampah di server :-p
		      
				 // load excel
			    $file = $_FILES['userfile']['tmp_name'];
			    $load = PHPExcel_IOFactory::load($file);
                $sheets = $load->getActiveSheet()->toArray(null,true,true,true);
				$i=1;
				foreach ($sheets as $sheet) {
				if ($i > 1) {						
						$hp=str_replace("'","",$sheet[1]);
						$hp=str_replace("`","",$hp);
						$hp=str_replace("+62","0",$hp);
									
							//get form
															
						$sql=array(
						"id_owner"=>$kode=$this->getKode(),
						"nama"=>$sheet[0],
						"hp"=>$hp=$this->db->escape_str($hp),
						"email"=>$sheet[2],
						"alamat"=>$sheet[3],
						);		
				       
						$cek=$this->cekOwner($hp,$kode);
					if($cek==0)//jika data kontak tidak ada maka tambahkan
					{  
							$this->db->insert("data_owner",$sql); $sukses++;
					}else{ //jika data kontak ada maka edit yg ada
						$sql=array(
						"nama"=>$sheet[0],
						"hp"=>$hp=$this->db->escape_str($hp),
						"email"=>$sheet[2],
						"alamat"=>$sheet[3],
						);						
							$this->db->where("hp",$hp);
							$this->db->update("data_owner",$sql);
							$edit++;
					};
					   
				}
				$i++;
                }
               
		}else{
        exit('<br><b style="color:red">Upload Gagal! Gunakan Format Ms.Excell yang telah di sediakan</b>');   //pesan error tipe file tidak tepat
		}

		return $sukses."-".$edit."-".$gagal;
	}
}