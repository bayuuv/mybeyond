<?php

class M_data_owner extends CI_Model  {
    
		
	function __construct()
    {
        parent::__construct();
		
    }
		function get_dataProperty($id)
	{
		 $query = $this->_get_dataProperty($id);
        if ($_POST['length'] != -1)
            $query .= " limit " . $_POST['start'] . "," . $_POST['length'];
        return $this->db->query($query)->result();
 
	}
	  public function countsList($id) {
        $query = $this->_get_dataProperty($id);
        return $this->db->query($query)->num_rows();
    }
	public function getDataOwnerByName($id) {
        $this->db->where("nama",$id);
        return $this->db->get("data_owner")->row();
    }
	
	public function getDataOwnerByHP($id) {
        $this->db->where("hp",$id);
        return $this->db->get("data_owner")->row();
    }
	
	
	function _get_dataProperty($id)
	{	
        $query = "SELECT * FROM data_property WHERE id_owner=$id ";
        if (isset($_POST['search']['value'])) {
            $searchkey = $_POST['search']['value'];
            $query .= " AND (
			kode_prop LIKE '%" . $searchkey . "%' or 
			data_property.desc  LIKE '%" . $searchkey . "%' or
			keterangan LIKE '%" . $searchkey . "%' or
			alamat_detail LIKE '%" . $searchkey . "%' or
			nama_area LIKE '%" . $searchkey . "%' or
			komplek LIKE '%" . $searchkey . "%'  
			) ";
        }

        $column = array('');
        $i = 0;
        foreach ($column as $item) {
            $column[$i] = $item;
        }

      /*  if (isset($_POST['order'])) {
		//	$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            $query .= " order by " . $column[$_POST['order']['0']['column']] . " " . $_POST['order']['0']['dir'];
        } else if (isset($order)) {
            $order = $order;
			//	$this->db->order_by(key($order), $order[key($order)]);
			       $query .= " order by nama ASC";
        }*/
		$query.=" order by created ASC" ;
        return $query;
	}
  
	
	
	
	
	
	
	
	
	
	
	function get_dataAgen()
	{
		 $query = $this->_get_dataOwner();
        if ($_POST['length'] != -1)
            $query .= " limit " . $_POST['start'] . "," . $_POST['length'];
        return $this->db->query($query)->result();
 
	}
	  public function counts() {
        $query = $this->_get_dataOwner();
        return $this->db->query($query)->num_rows();
    }
	function _get_dataOwner()
	{
        $query = "SELECT * FROM data_owner WHERE 1=1 ";
        if (isset($_POST['search']['value'])) {
            $searchkey = $_POST['search']['value'];
            $query .= " AND (
			nama LIKE '%" . $searchkey . "%' or 
			alamat LIKE '%" . $searchkey . "%' or
			email LIKE '%" . $searchkey . "%' or
			hp LIKE '%" . $searchkey . "%' 
			) ";
        }

        $column = array('', '', 'nama', 'hp', 'email', 'alamat');
        $i = 0;
        foreach ($column as $item) {
            $column[$i] = $item;
        }

      /*  if (isset($_POST['order'])) {
		//	$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            $query .= " order by " . $column[$_POST['order']['0']['column']] . " " . $_POST['order']['0']['dir'];
        } else if (isset($order)) {
            $order = $order;
			//	$this->db->order_by(key($order), $order[key($order)]);
			       $query .= " order by nama ASC";
        }*/
		$query.=" order by nama ASC" ;
        return $query;
	}
	
	
	function cekOwner($hp,$kode=NULL)
	{
		if($kode==NULL)
		{
			$this->db->where("hp",$hp);
	return	$this->db->get("data_owner")->num_rows();
		}else{
			$this->db->where("id_owner!=",$kode);
			$this->db->where("hp",$hp);
	return	$this->db->get("data_owner")->num_rows();
		}
			
	}
	/*function insert()
	{
	$data=array(
	"id_owner"=>date('dmYHis'),
	"nama"=>$this->input->post("nama"),
	"hp"=>$hp=$this->input->post("hp"),
	"hp2"=>$hp=$this->input->post("hp2"),
	"email"=>$this->input->post("email"),
	"alamat"=>$this->input->post("alamat"),
	);
	$cek=$this->cekOwner($hp);
	if(!$cek){
		return $this->db->insert("data_owner",$data);
	}else
	{
		return false;
	}

	}*/
	
		function uploadGambar($form,$kode)
	{
		
		if(isset($_FILES[$form]['type']))
		{
		return	$this->upload_img($kode,$form);
		}else{
			return $this->gambarDefauld($form,$kode);
		}
	}
	function gambarDefauld($form,$kode)
	{
	$data=$this->db->get_where("data_owner",array("id_owner"=>$kode))->row();	
	return isset($data->$form)?($data->$form):"";
	}
	public function upload_img($kode,$form)
	{	//$this->m_konfig->log("admin","Upload photo");
		///
			$nama=date("YmdHis");
		  $lokasi_file = $_FILES[$form]['tmp_name'];
		  $tipe_file   = $_FILES[$form]['type'];
		  $nama_file   = $_FILES[$form]['name'];
		  //if($tipe_file)
		  //{
		  $daprof=$this->gambarDefauld($form,$kode);
			if($daprof!="")
			 {
				 $path = "file_upload/owner/".$daprof;
				 if (file_exists($path)) {
					unlink($path);
				 }
		   }
		  
		  
			$jenis=explode(".",$nama_file);
			$nama_file=$jenis[0];
			// $jenis="jpg";
			$nama=str_replace("/","",$kode.$nama.$nama_file);
			 $target_path = "file_upload/owner/".$nama.".jpg";
			 //
	//	  }
		  //
		if (!empty($lokasi_file)) {
		move_uploaded_file($lokasi_file,$target_path);
		//if($jenis=="png"){
		//$this->konversi->UploadImageResize($target_path,$jenis,200);
		}
		return $nama.".jpg";
		}
	function insert()
	{
	$kode=date('dmYHis');
	$data=array(
	"id_owner"=>$kode,
	"nama"=>$this->input->post("nama"),
	"hp"=>$hp=$this->input->post("hp"),
	"hp2"=> $this->input->post("hp2"),
	"email"=>$this->input->post("email"),
	"alamat"=>$this->input->post("alamat"),
	"jk"=>$this->input->post("jk"),
	"tgl_lahir"=>$this->tanggal->eng_($this->input->post("tgl_lahir"),"-"),
	"poto"=>$this->uploadGambar("poto",$kode),
	"ktp"=>$this->uploadGambar("ktp",$kode),
	"kk"=>$this->uploadGambar("kk",$kode),
	"npwp"=>$this->uploadGambar("npwp",$kode),
	"bank"=>$this->input->post("bank"),
	"no_rek"=>$this->input->post("rek"),
	"atas_nama_bank"=>$this->input->post("an"),
	"nama_wakil"=>$this->input->post("nama2"),
	"nomor_wakil"=>$this->input->post("hp_wakil"),
	"hubungan_wakil"=>$this->input->post("hubungan"),
	"nama_pasangan"=>$this->input->post("nama_pasangan"),
	"tgl_lahir_pasangan"=>$this->tanggal->eng_($this->input->post("tgl_lahir_pasangan"),"-"),
	"anak1"=>$this->input->post("anak1"),
	"tgl_lahir_anak1"=>$this->tanggal->eng_($this->input->post("tgl_lahir_anak1"),"-"),
	"anak2"=>$this->input->post("anak2"),
	"tgl_lahir_anak2"=>$this->tanggal->eng_($this->input->post("tgl_lahir_anak2"),"-"),
	"anak3"=>$this->input->post("anak3"),
	"tgl_lahir_anak3"=>$this->tanggal->eng_($this->input->post("tgl_lahir_anak3"),"-"),
	"anak4"=>$this->input->post("anak4"),
	"tgl_lahir_anak4"=>$this->tanggal->eng_($this->input->post("tgl_lahir_anak4"),"-"),
	"anak5"=>$this->input->post("anak5"),
	"tgl_lahir_anak5"=>$this->tanggal->eng_($this->input->post("tgl_lahir_anak5"),"-"),

	);
	$cek=$this->cekOwner($hp);
	if(!$cek){
		return $this->db->insert("data_owner",$data);
	}else
	{
		return false;
	}

	}
	
	function update()
	{
	$kode=$this->input->post("id_owner");
	$data=array(
	"nama"=>$this->input->post("nama"),
	"hp"=>$hp=$this->input->post("hp"),
	"hp2"=> $this->input->post("hp2"),
	"email"=>$this->input->post("email"),
	"alamat"=>$this->input->post("alamat"),
	"jk"=>$this->input->post("jk"),
	"tgl_lahir"=>$this->tanggal->eng_($this->input->post("tgl_lahir"),"-"),
	"poto"=>$this->uploadGambar("poto",$kode),
	"ktp"=>$this->uploadGambar("ktp",$kode),
	"kk"=>$this->uploadGambar("kk",$kode),
	"npwp"=>$this->uploadGambar("npwp",$kode),
	"bank"=>$this->input->post("bank"),
	"no_rek"=>$this->input->post("rek"),
	"atas_nama_bank"=>$this->input->post("an"),
	"nama_wakil"=>$this->input->post("nama2"),
	"nomor_wakil"=>$this->input->post("hp_wakil"),
	"hubungan_wakil"=>$this->input->post("hubungan"),
	"nama_pasangan"=>$this->input->post("nama_pasangan"),
	"tgl_lahir_pasangan"=>$this->tanggal->eng_($this->input->post("tgl_lahir_pasangan"),"-"),
	"anak1"=>$this->input->post("anak1"),
	"tgl_lahir_anak1"=>$this->tanggal->eng_($this->input->post("tgl_lahir_anak1"),"-"),
	"anak2"=>$this->input->post("anak2"),
	"tgl_lahir_anak2"=>$this->tanggal->eng_($this->input->post("tgl_lahir_anak2"),"-"),
	"anak3"=>$this->input->post("anak3"),
	"tgl_lahir_anak3"=>$this->tanggal->eng_($this->input->post("tgl_lahir_anak3"),"-"),
	"anak4"=>$this->input->post("anak4"),
	"tgl_lahir_anak4"=>$this->tanggal->eng_($this->input->post("tgl_lahir_anak4"),"-"),
	"anak5"=>$this->input->post("anak5"),
	"tgl_lahir_anak5"=>$this->tanggal->eng_($this->input->post("tgl_lahir_anak5"),"-"),

	);
	$cek=$this->cekOwner($hp,$kode);
	if(!$cek){
		$this->db->where("id_owner",$this->input->post("id_owner"));
		return $this->db->update("data_owner",$data);
	}else
	{
		return false;
	}

	}
	
	/*
	function update()
	{
	$data=array(
	//"kode_agen"=>$this->getKode(),
	"nama"=>$this->input->post("nama"),
	"hp"=>$hp=$this->input->post("hp"),
	"hp2"=>$hp=$this->input->post("hp2"),
	"email"=>$this->input->post("email"),
	"alamat"=>$this->input->post("alamat"),
	);
	//$cek=$this->cekOwnerUpdate($hp);
	if(1==1){
		$this->db->where("id_owner",$this->input->post("id_owner"));
		return $this->db->update("data_owner",$data);
	}else
	{
		return false;
	}

	}*/
	
	function HapusAll()
	{
		$hapus=$this->input->post("hapus");
		foreach($hapus as $id)
		{
		$this->db->where("id_owner",$id);
		$this->db->delete("data_owner");
		}	return true;
	}
	function hapus($id)
	{
		$this->db->where("id_owner",$id);
		return	$this->db->delete("data_owner");
	}
	function export()
	{

		
//////start
        $objPHPExcel = new PHPExcel();
//style
        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'rotation' => 0,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '6CCECB')
            ),
            'borders' =>
            array('allborders' =>
                array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'),
                ),
            ),
        ); $style2 = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '6CCECB')
            ),
            'borders' =>
            array('allborders' =>
                array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'),
                ),
            ),
        );
     //   $objPHPExcel->getActiveSheet(0)->getColumnDimension('F')->setWidth(25);
     //   $objPHPExcel->getActiveSheet(0)->getColumnDimension('G')->setWidth(25);
     //   $objPHPExcel->getActiveSheet(0)->getColumnDimension('H')->setWidth(35);




//create column
		 $objPHPExcel->getActiveSheet(0)->mergeCells('A1:G1');
         $objPHPExcel->getActiveSheet(0)->setCellValue('A1', 'DATA PROFILE');
         $objPHPExcel->getActiveSheet(0)->setCellValue('A2', 'LISTING TOTAL');
         $objPHPExcel->getActiveSheet(0)->setCellValue('B2', 'NAME');
         $objPHPExcel->getActiveSheet(0)->setCellValue('C2', 'BORN');
		 $objPHPExcel->getActiveSheet(0)->setCellValue('D2', 'HP1');
         $objPHPExcel->getActiveSheet(0)->setCellValue('E2', 'HP2');
		 $objPHPExcel->getActiveSheet(0)->setCellValue('F2', 'ADRESS');
		 $objPHPExcel->getActiveSheet(0)->setCellValue('G2', 'EMAIL');
		 
		
		$objPHPExcel->getActiveSheet(0)->mergeCells('H1:J1');
		$objPHPExcel->getActiveSheet(0)->setCellValue('H1', 'BANK ACCOUNT');
        $objPHPExcel->getActiveSheet(0)->setCellValue('H2', 'BANK');
        $objPHPExcel->getActiveSheet(0)->setCellValue('I2', 'NO.REK');
        $objPHPExcel->getActiveSheet(0)->setCellValue('J2', 'NAME');
		
	
		$objPHPExcel->getActiveSheet(0)->mergeCells('K1:P1');
		$objPHPExcel->getActiveSheet(0)->setCellValue('K1', 'FAMILY');
        $objPHPExcel->getActiveSheet(0)->setCellValue('K2', 'COUPLE NAME');
        $objPHPExcel->getActiveSheet(0)->setCellValue('L2', 'BORN');
        $objPHPExcel->getActiveSheet(0)->setCellValue('M2', 'CHILD 1');
        $objPHPExcel->getActiveSheet(0)->setCellValue('N2', 'BORN');
        $objPHPExcel->getActiveSheet(0)->setCellValue('O2', 'CHILD 2');
        $objPHPExcel->getActiveSheet(0)->setCellValue('P2', 'BORN');
		
		

//make a border column
        $objPHPExcel->getActiveSheet(0)->getStyle('A1:P1')->applyFromArray($style);
        $objPHPExcel->getActiveSheet(0)->getStyle('A2:P2')->applyFromArray($style);

       $database = $this->db->query("SELECT * FROM data_owner")->result();
        $shit = 2;$jk="";
        
        foreach ($database as $list) {
            $shit++;
			 if($list->jk=="l"){ $jk="Mr."; $pasangan="WIFE";}else{ $jk="Mrs."; $pasangan="HUSBAND";};
//create data per row
if($list->nama_pasangan)
			{
				$pas=$list->nama_pasangan ."( ".$pasangan." )";
			}else{
				$pas="";
			}
    
          $objPHPExcel->getActiveSheet(0)->setCellValue('A' . $shit . '', $this->reff->jumlahListingOwner($list->id_owner));
          $objPHPExcel->getActiveSheet(0)->setCellValue('B' . $shit .'',$jk." ".$list->nama);
            $objPHPExcel->getActiveSheet(0)->setCellValue('C' . $shit . '', $this->tanggal->hariLengkap($list->tgl_lahir,"/"));
            $objPHPExcel->getActiveSheet(0)->setCellValue('D' . $shit . '','`'. $list->hp);
            $objPHPExcel->getActiveSheet(0)->setCellValue('E' . $shit . '','`'.  $list->hp2);
            $objPHPExcel->getActiveSheet(0)->setCellValue('F' . $shit . '', $list->alamat);
            $objPHPExcel->getActiveSheet(0)->setCellValue('G' . $shit . '', $list->email);
  
            
            $objPHPExcel->getActiveSheet(0)->setCellValue('H' . $shit . '',  ucwords($list->bank));
            $objPHPExcel->getActiveSheet(0)->setCellValue('I' . $shit . '','`'.    $list->no_rek);
            $objPHPExcel->getActiveSheet(0)->setCellValue('J' . $shit . '',   $list->atas_nama_bank);
			
            $objPHPExcel->getActiveSheet(0)->setCellValue('K' . $shit . '',   $pas);
            $objPHPExcel->getActiveSheet(0)->setCellValue('L' . $shit . '',   $this->tanggal->hariLengkap($list->tgl_lahir_pasangan,"/"));
			
            $objPHPExcel->getActiveSheet(0)->setCellValue('M' . $shit . '',   $list->anak1);
            $objPHPExcel->getActiveSheet(0)->setCellValue('N' . $shit . '',   $this->tanggal->hariLengkap($list->tgl_lahir_anak1,"/"));			
            $objPHPExcel->getActiveSheet(0)->setCellValue('O' . $shit . '',   $list->anak2);
            $objPHPExcel->getActiveSheet(0)->setCellValue('P' . $shit . '',   $this->tanggal->hariLengkap($list->tgl_lahir_anak2,"/"));
			
		
			
        }

//auto size column
	$abjad="A";
for($a=1;$a<=16;$a++)
{

        $objPHPExcel->getActiveSheet(0)->getColumnDimension($abjad++)->setAutoSize(true);
}

		
 
	   
/*------------------------------------------------------------*/
		$objPHPExcel->getActiveSheet(0)->setTitle('VENDOR');
		$objPHPExcel->setActiveSheetIndex(0);
        header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="VENDOR.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
//////finish
    
	}
	
	
 
	
	function downloadFormat()
	{
	//////start
		$objPHPExcel = new PHPExcel();
		//style
		$style = array( 
		 'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				  'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				  'rotation'   => 0,
		  ),
		   'fill' => array(
				  'type' => PHPExcel_Style_Fill::FILL_SOLID,
				  'color' => array('rgb' => 'ccccff')
			  ),
		 'borders' => 
		  array( 'allborders' => 
			array( 'style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'), 
			  ), 
			), 
		);	
		
		$head = array( 
			'font'  => array(
			'bold'  => true,
			'color' => array('rgb' => 'FFFFFF'),
			),
			
		   'fill' => array(
				  'type' => PHPExcel_Style_Fill::FILL_SOLID,
				  'color' => array('rgb' => '009966')
			  ),
		 'borders' => 
		  array( 'allborders' => 
			array( 'style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'), 
			  ), 
			), 
		);
		
		
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('A')->setWidth(25);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('B')->setWidth(25);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('C')->setWidth(25);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('D')->setWidth(25);

		
		
		//create column
		
		$objPHPExcel->getActiveSheet(0)->setCellValue('A1', 'Nama');
		$objPHPExcel->getActiveSheet(0)->setCellValue('B1', 'Nomor Hp');
		$objPHPExcel->getActiveSheet(0)->setCellValue('C1', 'Email');
		$objPHPExcel->getActiveSheet(0)->setCellValue('D1', 'Alamat');
		
		//make a border column
		$objPHPExcel->getActiveSheet(0)->getStyle('A1:D1')->applyFromArray($style);
		
		
		
		// Rename worksheet (worksheet, not filename)
		$objPHPExcel->getActiveSheet(0)->setTitle('Data Agen');
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		
		header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Form Agen.xlsx"');
		header('Cache-Control: max-age=0');
		 
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		//////finish
		
	}
	function importData2()
	{
	$sukses=0;$gagal=0;$edit=0;
		 $file   = explode('.',$_FILES['userfile']['name']);
		$length = count($file);
		if($file[$length -1] == 'xlsx' || $file[$length -1] == 'xls'){//jagain barangkali uploadnya selain file excel <span class="wp-smiley wp-emoji wp-emoji-smile" title=":-)">:-)</span>
        $tmp    = $_FILES['userfile']['tmp_name'];//Baca dari tmp folder jadi file ga perlu jadi sampah di server :-p
		      
				 // load excel
			    $file = $_FILES['userfile']['tmp_name'];
			    $load = PHPExcel_IOFactory::load($file);
                $sheets = $load->getActiveSheet()->toArray(null,true,true,true);
				$i=1; $idowner=1;
				foreach ($sheets as $sheet) {
				if ($i > 1) {						
					//	$hp=str_replace("'","",$sheet[1]);
					//	$hp=str_replace("`","",$hp);
					//	$hp=str_replace("+62","0",$hp);
									
							//get form
															
	
		//  $carikode = $this->db->query("SHOW TABLE STATUS LIKE 'data_property'")->row();
			    $datakode =$sheet[0];
		  	    $kode = (int) $datakode;
		   		$set27=$sheet[27];
				if($set27!="")
				{
					$set27=$sheet[27];
				}else{
					$set27="06-13-17";
				}
				$pecahsit=explode("-",$set27);
				$sheet27=$pecahsit[1].$pecahsit[0].$pecahsit[2];
				//$newID = sprintf("%03s", $kode);
				$valtgl=str_replace("`","",$sheet[27]);
				$valtgl=str_replace("-","/",$sheet[27]);
				$valtgl="20".$pecahsit[2]."-".$pecahsit[0]."-".$pecahsit[1];//$this->tanggal->eng_($valtgl,"-");
				
				//$thn=substr($valtgl,2,2);
				//$bln=sprintf("%02s",substr($valtgl,5,2));
				//$tgl=sprintf("%02s",substr($valtgl,8,2));
				$urut=sprintf("%03s",$sheet[0]);
				////$tgl=sprintf("%02s",date('d'));
//$bulan=$this->tanggal->bulanRomawi($bulan);
				$tgl=$sheet[27];
				$tgl=str_replace("/","",$tgl);
				$tgl=str_replace("-","",$tgl);
				$tgl1=SUBSTR($tgl,0,4);
				$tgl2=SUBSTR($tgl,4,2);
				if($sheet[27]!="")
				{
				$tglfix=$sheet27;
				}else
				{
				$tglfix="130617";
				}
				$kode=$tglfix."-".$urut;	
	
						$sql=array(
								"kode_prop"=>$kode,
								"jenis_prop"=>$sheet[7],
							//	"jenis_listing"=>$sheet[3],
								"type_jual"=>$sheet[3],
							//	"desc"=>$this->input->post("desc"),
								"id_prov"=>$sheet[29],
								"id_kab"=>$sheet[30],
								"id_owner"=>$this->addDataOwner($idowner++,$sheet[1],$sheet[4],$sheet[5],$sheet[6]),
							//	"komplek"=>$this->input->post("nama_komplek"),
								"nama_area"=>$sheet[2],
							//	"lat_area"=>$this->input->post("lat_area"),
							//	"long_area"=>$this->input->post("long_area"),
								"alamat_detail"=>$sheet[2],//$this->input->post("alamat_detail"),
							//	"lat_detail"=>$this->input->post("lat"),
							//	"long_detail"=>$this->input->post("long"),
								"luas_tanah"=>$sheet[9],
								"luas_bangunan"=>$sheet[10],
							//	"tahun_dibangun"=>$this->input->post("tahun_dibangun"),
								"harga"=>$sheet[8],
								"kamar_tidur"=>$sheet[14],
								"kamar_mandi"=>$sheet[15],
								"kamar_tidur_p"=>$sheet[16],
								"kamar_mandi_p"=>$sheet[17],
								"jml_lantai"=>$sheet[11],
								"jml_garasi"=>$sheet[12],
								"jml_carports"=>$sheet[13],
								"daya_listrik"=>$sheet[18],
								"hadap"=>$sheet[21],
								//"type_sewa"=>$this->input->post("type_sewa"),
								"jenis_sertifikat"=>$sheet[22],
								//"kelengkapan"=>"1",
								"agen"=>$sheet[26],
								"keterangan"=>$sheet[23],
								"air"=>$sheet[19],
								"furniture"=>$sheet[20],
								"area_listing"=>$sheet[28],
								//"gambar1"=>$this->uploadGambar($kode,"upload1"),
							//	"gambar2"=>$this->uploadGambar($kode,"upload2"),
							//	"gambar3"=>$this->uploadGambar($kode,"upload3"),
							//	"gambar4"=>$this->uploadGambar($kode,"upload4"),
							//	"gambar5"=>$this->uploadGambar($kode,"upload5"),
							//	"gambar_utama"=>$this->input->post("set"),
								"tgl_masuk_listing"=>$valtgl,
								"media_promosi"=>$sheet[25],
								"fee_persen"=>$sheet[24],
						);		
				       
					//	$cek=$this->cekOwner($hp);
					if(1==1)//jika data kontak tidak ada maka tambahkan
					{  
							$this->db->insert("data_property",$sql); $sukses++;
					}	/*else{ //jika data kontak ada maka edit yg ada
						$sql=array(
						"nama"=>$sheet[0],
						"hp"=>$hp=$this->db->escape_str($hp),
						"email"=>$sheet[2],
						"alamat"=>$sheet[3],
						);						
							$this->db->where("hp",$hp);
							$this->db->update("data_owner",$sql);
							$edit++;
					};*/
					   
				}
				$i++;
                }
               
		}else{
        exit('<br><b style="color:red">Upload Gagal! Gunakan Format Ms.Excell yang telah di sediakan</b>');   //pesan error tipe file tidak tepat
		}

		return $sukses."-".$edit."-".$gagal;
	}	
	function addDataOwner($id,$nama,$hp1,$hp2,$email)
	{
		$cek=$this->db->query("SELECT * from data_owner where nama='".$nama."' ")->num_rows();
		if($cek){ return false; }
		
		$data=array(
		"id_owner"=>$id,
		"nama"=>$nama,
		"hp"=>$hp1,
		"hp2"=>$hp2,
		"email"=>$email,
		);
		$this->db->insert("data_owner",$data);
		return $id;
	}
	function importData()
	{
	$sukses=0;$gagal=0;$edit=0;
		 $file   = explode('.',$_FILES['userfile']['name']);
		$length = count($file);
		if($file[$length -1] == 'xlsx' || $file[$length -1] == 'xls'){//jagain barangkali uploadnya selain file excel <span class="wp-smiley wp-emoji wp-emoji-smile" title=":-)">:-)</span>
        $tmp    = $_FILES['userfile']['tmp_name'];//Baca dari tmp folder jadi file ga perlu jadi sampah di server :-p
		      
				 // load excel
			    $file = $_FILES['userfile']['tmp_name'];
			    $load = PHPExcel_IOFactory::load($file);
                $sheets = $load->getActiveSheet()->toArray(null,true,true,true);
				$i=1;
				foreach ($sheets as $sheet) {
				if ($i > 1) {						
						$hp=str_replace("'","",$sheet[1]);
						$hp=str_replace("`","",$hp);
						$hp=str_replace("+62","0",$hp);
									
							//get form
															
						$sql=array(
						"nama"=>$sheet[0],
						"hp"=>$hp=$this->db->escape_str($hp),
						"email"=>$sheet[2],
						"alamat"=>$sheet[3],
						);		
				       
						$cek=$this->cekOwner($hp);
					if($cek==0)//jika data kontak tidak ada maka tambahkan
					{  
							$this->db->insert("data_owner",$sql); $sukses++;
					}else{ //jika data kontak ada maka edit yg ada
						$sql=array(
						"nama"=>$sheet[0],
						"hp"=>$hp=$this->db->escape_str($hp),
						"email"=>$sheet[2],
						"alamat"=>$sheet[3],
						);						
							$this->db->where("hp",$hp);
							$this->db->update("data_owner",$sql);
							$edit++;
					};
					   
				}
				$i++;
                }
               
		}else{
        exit('<br><b style="color:red">Upload Gagal! Gunakan Format Ms.Excell yang telah di sediakan</b>');   //pesan error tipe file tidak tepat
		}

		return $sukses."-".$edit."-".$gagal;
	}
	function go_cek_duplikat_owner_ada()
	{
		$where="";
		$hp1=$this->input->post("hp1");
		if($hp1){
		$where.=" OR hp='".$hp1."'";
		}
		$hp2=$this->input->post("hp2");
		if($hp2){
		$where.=" OR hp2='".$hp2."'";
		}
		$email=$this->input->post("email");
		if($email){
		$where.=" OR email='".$email."'";
		}
		$alamat=$this->input->post("alamat");
		if($alamat){
		$where.=" OR alamat='".$alamat."'";
		}
		$id_own=$this->input->post("id_own");
		if($where!=""){
			$where = substr($where,3);
		return $this->db->query("SELECT * from data_owner where   ".$where."  AND id_owner!='".$id_own."' ")->num_rows();
		}return 0;
	}
	function go_cek_duplikat_owner()
	{
		$where="";
		$hp1=$this->input->post("hp1");
		if($hp1){
		$where.=" OR hp='".$hp1."'";
		}
		$hp2=$this->input->post("hp2");
		if($hp2){
		$where.=" OR hp2='".$hp2."'";
		}
		$email=$this->input->post("email");
		if($email){
		$where.=" OR email='".$email."'";
		}
		$alamat=$this->input->post("alamat");
		if($alamat){
		$where.=" OR alamat='".$alamat."'";
		}
		if($where!=""){
			$where = substr($where,3);
		return $this->db->query("SELECT * from data_owner where ".$where."  ")->num_rows();
		}else{return 0;};
	}
}