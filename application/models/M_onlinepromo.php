<?php

class M_onlinepromo extends CI_Model  {
    
		
	function __construct()
    {
        parent::__construct();
		
    }
	
	/*---------------------------------------------*/
		
	function get_dataOnlinePromo()
	{
		 $query = $this->_get_dataOnlinePromo();
        if ($_POST['length'] != -1)
            $query .= " limit " . $_POST['start'] . "," . $_POST['length'];
        return $this->db->query($query)->result();
 
	}
	  public function counts() {
        $query = $this->_get_dataOnlinePromo();
        return $this->db->query($query)->num_rows();
    }
	function _get_dataOnlinePromo()
	{
	$dan="";
	$agen=$this->input->get("agen");
	if($agen){
	$dan.=" AND agen='$agen'";
	}
	$tahun_sell=$this->input->get("tahun_sell");
	if($tahun_sell=='2018'){
	$dan.=" AND year(tgl_promoonline)='2018'";
	}elseif($tahun_sell=='1111'){
	$dan.="";
	}
	$bulan_sell=$this->input->get("bulan_sell");
	if($bulan_sell=='01'){
	$dan.=" AND month(tgl_promoonline)='01'";
	}elseif($bulan_sell=='02'){
	$dan.=" AND month(tgl_promoonline)='02'";
	}elseif($bulan_sell=='03'){
	$dan.=" AND month(tgl_promoonline)='03'";
	}elseif($bulan_sell=='04'){
	$dan.=" AND month(tgl_promoonline)='04'";
	}elseif($bulan_sell=='05'){
	$dan.=" AND month(tgl_promoonline)='05'";
	}elseif($bulan_sell=='06'){
	$dan.=" AND month(tgl_promoonline)='06'";
	}elseif($bulan_sell=='07'){
	$dan.=" AND month(tgl_promoonline)='07'";
	}elseif($bulan_sell=='08'){
	$dan.=" AND month(tgl_promoonline)='08'";
	}elseif($bulan_sell=='09'){
	$dan.=" AND month(tgl_promoonline)='09'";
	}elseif($bulan_sell=='10'){
	$dan.=" AND month(tgl_promoonline)='10'";
	}elseif($bulan_sell=='11'){
	$dan.=" AND month(tgl_promoonline)='11'";
	}elseif($bulan_sell=='12'){
	$dan.=" AND month(tgl_promoonline)='12'";
	}elseif($bulan_sell=='1111'){
	$dan.="";
	}
	$kategori=$this->input->get("kategori");
	if($kategori=='1'){
	$dan.=" AND jenis_promoonline='1'";
	}elseif($kategori=='2'){
	$dan.=" AND jenis_promoonline='2'";
	}elseif($kategori=='3'){
	$dan.=" AND jenis_promoonline='3'";
	}elseif($kategori=='4'){
	$dan.=" AND jenis_promoonline='4'";
	}elseif($kategori=='5'){
	$dan.=" AND jenis_promoonline='5'";
	}elseif($kategori=='6'){
	$dan.=" AND jenis_promoonline='6'";
	}elseif($kategori=='7'){
	$dan.=" AND jenis_promoonline='7'";
	}elseif($kategori=='8'){
	$dan.=" AND jenis_promoonline='8'";
	}elseif($kategori=='9'){
	$dan.=" AND jenis_promoonline='9'";
	}
        if($this->session->userdata("id")==64){ // Ajeng
			$query = "SELECT a.*, b.nama FROM promo_online AS a 
			LEFT JOIN data_agen AS b ON a.agen = b.kode_agen WHERE 1=1 AND agen IN ('BREA/005/II/2017', 'BREA/012/VIII/2017') $dan ";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				nama LIKE '%" . $searchkey . "%'
				) ";
			}
		}elseif($this->session->userdata("id")==151){ // Rhafa
			$query = "SELECT a.*, b.nama FROM promo_online AS a 
			LEFT JOIN data_agen AS b ON a.agen = b.kode_agen WHERE 1=1 AND agen='BREA/039/I/2018' $dan ";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				nama LIKE '%" . $searchkey . "%'
				) ";
			}
		}elseif($this->session->userdata("id")==133){ // Kiki 
			$query = "SELECT a.*, b.nama FROM promo_online AS a 
			LEFT JOIN data_agen AS b ON a.agen = b.kode_agen WHERE 1=1 AND agen IN ('BREA/046/II/2018') $dan ";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				nama LIKE '%" . $searchkey . "%'
				) ";
			}
		}elseif($this->session->userdata("id")==146){ // Yudi
			$query = "SELECT a.*, b.nama FROM promo_online AS a 
			LEFT JOIN data_agen AS b ON a.agen = b.kode_agen WHERE 1=1 AND agen IN ('BREA/032/XI/2017') $dan ";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				nama LIKE '%" . $searchkey . "%'
				) ";
			}
		}elseif($this->session->userdata("id")==152){ // Vivi
			$query = "SELECT a.*, b.nama FROM promo_online AS a 
			LEFT JOIN data_agen AS b ON a.agen = b.kode_agen WHERE 1=1 AND agen NOT IN ('BREA/010/I/2017', 'BREA/046/II/2018', 'BREA/032/XI/2017', 'BREA/001/I/2017', 'BREA/005/II/2017', 'BREA/012/VIII/2017', 'BREA/066/VII/2018') $dan ";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				nama LIKE '%" . $searchkey . "%'
				) ";
			}
		}elseif($this->session->userdata("id")==161){ // Yema 
			$query = "SELECT a.*, b.nama FROM promo_online AS a 
			LEFT JOIN data_agen AS b ON a.agen = b.kode_agen WHERE 1=1 AND agen IN ('BREA/001/I/2017') $dan ";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				nama LIKE '%" . $searchkey . "%'
				) ";
			}
		}elseif($this->session->userdata("id")==162){ // Frans
			$query = "SELECT a.*, b.nama FROM promo_online AS a 
			LEFT JOIN data_agen AS b ON a.agen = b.kode_agen WHERE 1=1 AND agen IN ('BREA/046/II/2018', 'BREA/010/I/2017') $dan ";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				nama LIKE '%" . $searchkey . "%'
				) ";
			}
		}elseif($this->session->userdata("id")==165){ // Rena
			$query = "SELECT a.*, b.nama FROM promo_online AS a 
			LEFT JOIN data_agen AS b ON a.agen = b.kode_agen WHERE 1=1 AND agen IN ('BREA/005/II/2017', 'BREA/012/VIII/2017', 'BREA/066/VII/2018') $dan ";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				nama LIKE '%" . $searchkey . "%'
				) ";
			}
		}else{		
			$query = "SELECT a.*, b.nama FROM promo_online AS a 
			LEFT JOIN data_agen AS b ON a.agen = b.kode_agen WHERE 1=1 $dan ";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				nama LIKE '%" . $searchkey . "%'
				) ";
			}
		}

        $column = array('', '', 'id_promoonline','tgl_promoonline', 'jenis_promoonline', 'jumlah_promoonline', 'nama');
        $i = 0;
        foreach ($column as $item) {
            $column[$i] = $item;
        }

      /*  if (isset($_POST['order'])) {
		//	$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            $query .= " order by " . $column[$_POST['order']['0']['column']] . " " . $_POST['order']['0']['dir'];
        } else if (isset($order)) {
            $order = $order;
			//	$this->db->order_by(key($order), $order[key($order)]);
			       $query .= " order by nama ASC";
        }*/
		$query.=" order by tgl_promoonline DESC" ;
        return $query;
	}
	
		
	function insert()
	{
	$data=array(
	"agen"=>$agn=$this->input->post("agen"),
	"tgl_promoonline"=>$tglp=$this->tanggal->eng_($this->input->post("tgl_promo"),"-"),
	"jenis_promoonline"=>$cat=$this->input->post("kategori"),
	"jumlah_promoonline"=>$this->input->post("jumlah"),
	);
		$cek=$this->_cekMonitoring($agn,$tglp,$cat);
		if($cek){
			return false;
		}else{
		return $this->db->insert("promo_online",$data);
		}
	}
	
	function _cekMonitoring($agn,$tglp,$cat)
	{
		  $this->db->where("agen",$agn);
		  $this->db->where("tgl_promoonline",$tglp);
	      $this->db->where("jenis_promoonline",$cat);
	 return   $this->db->get("promo_online")->num_rows();
	}
	
	function update()
	{
	
	$array=array(
	"agen"=>$this->input->post("agenx"),
	"tgl_promoonline"=>$this->tanggal->eng_($this->input->post("tgl_promo"),"-"),
	"jenis_promoonline"=>$this->input->post("kategori"),
	"jumlah_promoonline"=>$this->input->post("jumlah"),
	);
		$this->db->where("id_promoonline",$this->input->post("id_promoonline"));
		return $this->db->update("promo_online",$array);
	}
	
	
	function HapusAll()
	{
		$hapus=$this->input->post("hapus");
		foreach($hapus as $id)
		{
		$this->db->where("id_promoonline",$id);
		$this->db->delete("promo_online");
		}	return true;
	}
	function hapus($id)
	{
		$this->db->where("id_promoonline",$id);
		return	$this->db->delete("promo_online");
	}
	
}