<?php

class M_data_agen extends CI_Model  {
    
		
	function __construct()
    {
        parent::__construct();
		
    }
	/*---------------------------------------------*/
	function ajax_listing()
	{
		 $query = $this->_get_dataListing();
        if ($_POST['length'] != -1)
            $query .= " limit " . $_POST['start'] . "," . $_POST['length'];
        return $this->db->query($query)->result();
 
	}
	 public function counts_listing() {
        $query = $this->_get_dataListing();
        return $this->db->query($query)->num_rows();
    }
	function _get_dataListing()
	{
		$id_agen=$this->uri->segment(3);
		$filter="";
		if($id_agen){ $filter="id_agen='".$id_agen."'";}
        $query = "SELECT * FROM report_listing WHERE 1=1 AND $filter ";
        if (isset($_POST['search']['value'])) {
            $searchkey = $_POST['search']['value'];
            $query .= " AND (
			kode_listing LIKE '%" . $searchkey . "%' or 
			ket LIKE '%" . $searchkey . "%' or
			tgl LIKE '%" . $searchkey . "%'
			) ";
        }

        $column = array('');
        $i = 0;
        foreach ($column as $item) {
            $column[$i] = $item;
        }
		$query.=" order by tgl DESC" ;
        return $query;
	}
	/*---------------------------------------------*/
	
	/*---------------------------------------------*/
	function ajax_costumer()
	{
		 $query = $this->_get_dataCostumer();
        if ($_POST['length'] != -1)
            $query .= " limit " . $_POST['start'] . "," . $_POST['length'];
        return $this->db->query($query)->result();
 
	}
	 public function counts_costumer() {
        $query = $this->_get_dataCostumer();
        return $this->db->query($query)->num_rows();
    }
	function _get_dataCostumer()
	{
		$id_agen=$this->uri->segment(3);
		$filter="";
		if($id_agen){ $filter="id_agen='".$id_agen."'";}
        $query = "SELECT * FROM report_costumer WHERE 1=1 AND $filter ";
        if (isset($_POST['search']['value'])) {
            $searchkey = $_POST['search']['value'];
            $query .= " AND (
			ket LIKE '%" . $searchkey . "%' or
			tgl LIKE '%" . $searchkey . "%'
			) ";
        }

        $column = array('');
        $i = 0;
        foreach ($column as $item) {
            $column[$i] = $item;
        }
		$query.=" order by tgl DESC" ;
        return $query;
	}
	/*---------------------------------------------*/
	
	function get_dataAgen()
	{
		 $query = $this->_get_dataAgen();
        if ($_POST['length'] != -1)
            $query .= " limit " . $_POST['start'] . "," . $_POST['length'];
        return $this->db->query($query)->result();
 
	}
	
	  public function counts() {
        $query = $this->_get_dataAgen();
        return $this->db->query($query)->num_rows();
    }
	function _get_dataAgen()
	{
        $query = "SELECT * FROM data_agen WHERE jabatan IN ('1','11', '12') ";
        if (isset($_POST['search']['value'])) {
            $searchkey = $_POST['search']['value'];
            $query .= " AND (
			nama LIKE '%" . $searchkey . "%' or 
			alamat LIKE '%" . $searchkey . "%' or
			kode_agen LIKE '%" . $searchkey . "%' or
			email LIKE '%" . $searchkey . "%' or
			hp LIKE '%" . $searchkey . "%' 
			) ";
        }

        $column = array('', '', 'kode_agen', 'nama', 'hp', 'email', 'alamat');
        $i = 0;
        foreach ($column as $item) {
            $column[$i] = $item;
        }

      /*  if (isset($_POST['order'])) {
		//	$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            $query .= " order by " . $column[$_POST['order']['0']['column']] . " " . $_POST['order']['0']['dir'];
        } else if (isset($order)) {
            $order = $order;
			//	$this->db->order_by(key($order), $order[key($order)]);
			       $query .= " order by nama ASC";
        }*/
		$query.=" order by kode_agen ASC" ;
        return $query;
	}
	
	/*---------------------------------------------*/
	
	function get_dataNetwork()
	{
		 $query = $this->_get_dataNetwork();
        if ($_POST['length'] != -1)
            $query .= " limit " . $_POST['start'] . "," . $_POST['length'];
        return $this->db->query($query)->result();
 
	}
	
	  public function countsN() {
        $query = $this->_get_dataNetwork();
        return $this->db->query($query)->num_rows();
    }
	function _get_dataNetwork()
	{
        $query = "SELECT * FROM data_agen WHERE jabatan=100 ";
        if (isset($_POST['search']['value'])) {
            $searchkey = $_POST['search']['value'];
            $query .= " AND (
			nama LIKE '%" . $searchkey . "%' or 
			alamat LIKE '%" . $searchkey . "%' or
			kode_agen LIKE '%" . $searchkey . "%' or
			email LIKE '%" . $searchkey . "%' or
			hp LIKE '%" . $searchkey . "%' 
			) ";
        }

        $column = array('', '', 'kode_agen', 'nama', 'hp', 'email', 'alamat');
        $i = 0;
        foreach ($column as $item) {
            $column[$i] = $item;
        }

      /*  if (isset($_POST['order'])) {
		//	$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            $query .= " order by " . $column[$_POST['order']['0']['column']] . " " . $_POST['order']['0']['dir'];
        } else if (isset($order)) {
            $order = $order;
			//	$this->db->order_by(key($order), $order[key($order)]);
			       $query .= " order by nama ASC";
        }*/
		$query.=" order by kode_agen DESC" ;
        return $query;
	}
	
	function getKode()
	{
			  $carikode = $this->db->query("SELECT max(id_agen) as id_agen from data_agen")->row();
			  $datakode =$carikode->id_agen;//$carikode->kode;
			  if ($datakode) {
		  	    $kode = (int) $datakode;
		   		return	$newID = sprintf("%02s", $kode+1);
		   	  } else {
				return "01";
			  }
	}
	function getKodeAgen()
	{
			  $carikode = $this->db->query("SHOW TABLE STATUS LIKE 'data_agen'")->row();
			  $datakode =isset($carikode->Auto_increment)?($carikode->Auto_increment):"1";//$carikode->kode;
		  	    $kode = (int) $datakode;
		   		$newID = sprintf("%03s", $kode);
				$tahun=date('Y');
				$bulan=date('m');
				$bulan=$this->tanggal->bulanRomawi($bulan);
		   	 return "BREA/$newID/$bulan/$tahun";			  
	}
	function cekAgen($kode)
	{
		if(!$kode){ return 10;}
		//	$this->db->where("hp",$hp);
			$this->db->where("kode_agen",$kode);
	return	$this->db->get("data_agen")->num_rows();
	}
	function uploadGambar($form,$kode)
	{
		
		if(isset($_FILES[$form]['type']))
		{
		return	$this->upload_img($kode,$form);
		}else{
			return $this->gambarDefauld($form,$kode);
		}
	}
	function gambarDefauld($form,$kode)
	{
	$data=$this->db->get_where("data_agen",array("kode_agen"=>$kode))->row();	
	return isset($data->$form)?($data->$form):"";
	}
	public function upload_img($kode,$form)
	{	//$this->m_konfig->log("admin","Upload photo");
		///
			$nama=date("YmdHis");
		  $lokasi_file = $_FILES[$form]['tmp_name'];
		  $tipe_file   = $_FILES[$form]['type'];
		  $nama_file   = $_FILES[$form]['name'];
		  //if($tipe_file)
		  //{
		  $daprof=$this->gambarDefauld($form,$kode);
			if($daprof!="")
			 {
				 $path = "file_upload/agen/".$daprof;
				 if (file_exists($path)) {
					unlink($path);
				 }
		   }
		  
		  
			$jenis=explode(".",$nama_file);
			$nama_file=$jenis[0];
			// $jenis="jpg";
			$nama=str_replace("/","",$kode.$nama.$nama_file);
			 $target_path = "file_upload/agen/".$nama.".jpg";
			 //
	//	  }
		  //
		if (!empty($lokasi_file)) {
		move_uploaded_file($lokasi_file,$target_path);
		//if($jenis=="png"){
		//$this->konversi->UploadImageResize($target_path,$jenis,200);
		}
		return $nama.".jpg";
		}
	function insert()
	{
	$data=array(
	"kode_agen"=>$kode=$this->input->post("kode"),
	"upline"=>$this->input->post("upline"),
	"jabatan"=>$this->input->post("jabatan"),
	"bpjs"=>$this->input->post("bpjs"),
	"nama"=>$this->input->post("nama"),
	"hp"=>$hp=$this->input->post("hp"),
	"hp2"=>$hp=$this->input->post("hp_2"),
	"email"=>$this->input->post("email"),
	"alamat"=>$this->input->post("alamat"),
	"jk"=>$this->input->post("jk"),
	"tgl_lahir"=>$this->tanggal->eng_($this->input->post("tgl_lahir"),"-"),
	"tgl_masuk_kerja"=>$this->tanggal->eng_($this->input->post("tgl_masuk_kerja"),"-"),
	"tgl_habis_kontrak"=>$this->tanggal->eng_($this->input->post("tgl_habis_kontrak"),"-"),
	"poto"=>$this->uploadGambar("poto",$kode),
	"ktp"=>$this->uploadGambar("ktp",$kode),
	"kk"=>$this->uploadGambar("kk",$kode),
	"npwp"=>$this->uploadGambar("npwp",$kode),
	"bank"=>$this->input->post("bank"),
	"no_rek"=>$this->input->post("rek"),
	"atas_nama_bank"=>$this->input->post("an"),
	"nama_wakil"=>$this->input->post("nama2"),
	"nomor_wakil"=>$this->input->post("hp2"),
	"hubungan_wakil"=>$this->input->post("hubungan"),
	"nama_pasangan"=>$this->input->post("nama_pasangan"),
	"tgl_lahir_pasangan"=>$this->tanggal->eng_($this->input->post("tgl_lahir_pasangan"),"-"),
	"anak1"=>$this->input->post("anak1"),
	"tgl_lahir_anak1"=>$this->tanggal->eng_($this->input->post("tgl_lahir_anak1"),"-"),
	"anak2"=>$this->input->post("anak2"),
	"tgl_lahir_anak2"=>$this->tanggal->eng_($this->input->post("tgl_lahir_anak2"),"-"),
	"anak3"=>$this->input->post("anak3"),
	"tgl_lahir_anak3"=>$this->tanggal->eng_($this->input->post("tgl_lahir_anak3"),"-"),
	"anak4"=>$this->input->post("anak4"),
	"tgl_lahir_anak4"=>$this->tanggal->eng_($this->input->post("tgl_lahir_anak4"),"-"),
	"anak5"=>$this->input->post("anak5"),
	"tgl_lahir_anak5"=>$this->tanggal->eng_($this->input->post("tgl_lahir_anak5"),"-"),
	"username"=>$this->input->post("username"),
	"password"=>$this->input->post("password"),
		"fb"=>$this->input->post("fb"),
			"twt"=>$this->input->post("twt"),
				"ig"=>$this->input->post("ig"),
	);
	$cek=$this->cekAgen($hp,$kode);
	if(1==1){
		return $this->db->insert("data_agen",$data);
	}else
	{ 
		return false;
	}

	}
	
	function update()
	{
	$data=array(
	"kode_agen"=>$kode=$this->input->post("kode"),
	"upline"=>$this->input->post("upline"),
	"jabatan"=>$this->input->post("jabatan"),
	"bpjs"=>$this->input->post("bpjs"),
	"nama"=>$this->input->post("nama"),
	"hp"=>$hp=$this->input->post("hp"),
	"hp2"=>$hp=$this->input->post("hp_2"),
	"email"=>$this->input->post("email"),
	"alamat"=>$this->input->post("alamat"),
	"jk"=>$this->input->post("jk"),
	"tgl_lahir"=>$this->tanggal->eng_($this->input->post("tgl_lahir"),"-"),
	"tgl_masuk_kerja"=>$this->tanggal->eng_($this->input->post("tgl_masuk_kerja"),"-"),
	"tgl_habis_kontrak"=>$this->tanggal->eng_($this->input->post("tgl_habis_kontrak"),"-"),
	"poto"=>$this->uploadGambar("poto",$kode),
	"ktp"=>$this->uploadGambar("ktp",$kode),
	"kk"=>$this->uploadGambar("kk",$kode),
	"npwp"=>$this->uploadGambar("npwp",$kode),
	"bank"=>$this->input->post("bank"),
	"no_rek"=>$this->input->post("rek"),
	"atas_nama_bank"=>$this->input->post("an"),
	"nama_wakil"=>$this->input->post("nama2"),
	"nomor_wakil"=>$this->input->post("hp2"),
	"hubungan_wakil"=>$this->input->post("hubungan"),
	"nama_pasangan"=>$this->input->post("nama_pasangan"),
	"tgl_lahir_pasangan"=>$this->tanggal->eng_($this->input->post("tgl_lahir_pasangan"),"-"),
	"anak1"=>$this->input->post("anak1"),
	"tgl_lahir_anak1"=>$this->tanggal->eng_($this->input->post("tgl_lahir_anak1"),"-"),
	"anak2"=>$this->input->post("anak2"),
	"tgl_lahir_anak2"=>$this->tanggal->eng_($this->input->post("tgl_lahir_anak2"),"-"),
	"anak3"=>$this->input->post("anak3"),
	"tgl_lahir_anak3"=>$this->tanggal->eng_($this->input->post("tgl_lahir_anak3"),"-"),
	"anak4"=>$this->input->post("anak4"),
	"tgl_lahir_anak4"=>$this->tanggal->eng_($this->input->post("tgl_lahir_anak4"),"-"),
	"anak5"=>$this->input->post("anak5"),
	"tgl_lahir_anak5"=>$this->tanggal->eng_($this->input->post("tgl_lahir_anak5"),"-"),
	"username"=>$this->input->post("username"),
	"password"=>$this->input->post("password"),
		"fb"=>$this->input->post("fb"),
			"twt"=>$this->input->post("twt"),
				"ig"=>$this->input->post("ig"),
	);
	//$cek=$this->cekAgenUpdate($hp);
	if(1==1){
		$this->db->where("id_agen",$this->input->post("id_agen"));
		return $this->db->update("data_agen",$data);
	}else
	{
		return false;
	}

	}
	
	function HapusAll()
	{
		$hapus=$this->input->post("hapus");
		foreach($hapus as $id)
		{
		 $this->hapus($id);
		}	return true;
	}
	function getGambarkode($id)
	{
		$this->db->where("id_agen",$id);
	return	$data=$this->db->get("data_agen")->row();
	}
	function hapus($id)
	{
		  $daprof=$this->getGambarkode($id);
			if($daprof)
			 {
				 /// hapus poto profile
				 $path = "file_upload/agen/".$daprof->poto;
				 if (file_exists($path)) {
					unlink($path);
				 }
				 ////hapus poto ktp
				  $path = "file_upload/agen/".$daprof->ktp;
				 if (file_exists($path)) {
					unlink($path);
				 } ////hapus poto kk
				  $path = "file_upload/agen/".$daprof->kk;
				 if (file_exists($path)) {
					unlink($path);
				 }
				 ////hapus poto npwp
				  $path = "file_upload/agen/".$daprof->npwp;
				 if (file_exists($path)) {
					unlink($path);
				 }
			 }
		$this->db->where("id_agen",$id);
		return	$this->db->delete("data_agen");
	}
	function export_member()
	{
		
//////start
        $objPHPExcel = new PHPExcel();
//style
        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'rotation' => 0,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '6CCECB')
            ),
            'borders' =>
            array('allborders' =>
                array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'),
                ),
            ),
        ); $style2 = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '6CCECB')
            ),
            'borders' =>
            array('allborders' =>
                array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'),
                ),
            ),
        );
     //   $objPHPExcel->getActiveSheet(0)->getColumnDimension('F')->setWidth(25);
     //   $objPHPExcel->getActiveSheet(0)->getColumnDimension('G')->setWidth(25);
     //   $objPHPExcel->getActiveSheet(0)->getColumnDimension('H')->setWidth(35);




//create column
		 $objPHPExcel->getActiveSheet(0)->mergeCells('A1:L1');
         $objPHPExcel->getActiveSheet(0)->setCellValue('A1', 'DATA PROFILE');
         $objPHPExcel->getActiveSheet(0)->setCellValue('A2', 'ID AGEN');
         $objPHPExcel->getActiveSheet(0)->setCellValue('B2', 'NAME');
         $objPHPExcel->getActiveSheet(0)->setCellValue('C2', 'BORN');
		 $objPHPExcel->getActiveSheet(0)->setCellValue('D2', 'HP1');
         $objPHPExcel->getActiveSheet(0)->setCellValue('E2', 'HP2');
		 $objPHPExcel->getActiveSheet(0)->setCellValue('F2', 'ADRESS');
		 $objPHPExcel->getActiveSheet(0)->setCellValue('G2', 'EMAIL');
		 $objPHPExcel->getActiveSheet(0)->setCellValue('H2', 'FACEBOOK');
		 $objPHPExcel->getActiveSheet(0)->setCellValue('I2', 'TWITTER');
		 $objPHPExcel->getActiveSheet(0)->setCellValue('J2', 'INSTAGRAM');
		 $objPHPExcel->getActiveSheet(0)->setCellValue('K2', 'BPJS');
        $objPHPExcel->getActiveSheet(0)->setCellValue('L2', 'DATE OF WORK ENTRY ');
        $objPHPExcel->getActiveSheet(0)->setCellValue('M2', 'CONTRACT EXPIRATION DATE ');
       
	    $objPHPExcel->getActiveSheet(0)->mergeCells('N1:P1');
		$objPHPExcel->getActiveSheet(0)->setCellValue('N1', 'SELLING INFORMATION');
        $objPHPExcel->getActiveSheet(0)->setCellValue('N2', 'LISTING');
        $objPHPExcel->getActiveSheet(0)->setCellValue('O2', 'SELLING');
        $objPHPExcel->getActiveSheet(0)->setCellValue('P2', 'CUSTOMER');
		
		$objPHPExcel->getActiveSheet(0)->mergeCells('Q1:S1');
		$objPHPExcel->getActiveSheet(0)->setCellValue('Q1', 'BANK ACCOUNT');
        $objPHPExcel->getActiveSheet(0)->setCellValue('Q2', 'BANK');
        $objPHPExcel->getActiveSheet(0)->setCellValue('R2', 'NO.REK');
        $objPHPExcel->getActiveSheet(0)->setCellValue('S2', 'NAME');
		
		$objPHPExcel->getActiveSheet(0)->mergeCells('T1:V1');
		$objPHPExcel->getActiveSheet(0)->setCellValue('T1', 'EMERGENCY CONCTACT');
        $objPHPExcel->getActiveSheet(0)->setCellValue('T2', 'NAME');
        $objPHPExcel->getActiveSheet(0)->setCellValue('U2', 'HP');
        $objPHPExcel->getActiveSheet(0)->setCellValue('V2', 'RELATIONSHIP');
		
		$objPHPExcel->getActiveSheet(0)->mergeCells('W1:AB1');
		$objPHPExcel->getActiveSheet(0)->setCellValue('W1', 'FAMILY');
        $objPHPExcel->getActiveSheet(0)->setCellValue('W2', 'COUPLE NAME');
        $objPHPExcel->getActiveSheet(0)->setCellValue('X2', 'BORN');
        $objPHPExcel->getActiveSheet(0)->setCellValue('Y2', 'CHILD 1');
        $objPHPExcel->getActiveSheet(0)->setCellValue('Z2', 'BORN');
        $objPHPExcel->getActiveSheet(0)->setCellValue('AA2', 'CHILD 2');
        $objPHPExcel->getActiveSheet(0)->setCellValue('AB2', 'BORN');
		
		

//make a border column
        $objPHPExcel->getActiveSheet(0)->getStyle('A1:AB1')->applyFromArray($style);
        $objPHPExcel->getActiveSheet(0)->getStyle('A2:AB2')->applyFromArray($style);

       $database = $this->_get_dataAgen();
        $shit = 2;$jk="";
        $database = $this->db->query($database)->result();
        foreach ($database as $list) {
            $shit++;
			 if($list->jk=="l"){ $jk="Mr."; $pasangan="WIFE";}else{ $jk="Mrs."; $pasangan="HUSBAND";};
			 
			 if($list->nama_pasangan)
			{
				$pas=$list->nama_pasangan ."( ".$pasangan." )";
			}else{
				$pas="";
			}
			 
//create data per row
           $objPHPExcel->getActiveSheet(0)->setCellValue('A' . $shit . '', '`'.$list->kode_agen);
          $objPHPExcel->getActiveSheet(0)->setCellValue('B' . $shit . '', $jk." ".$list->nama);
          $objPHPExcel->getActiveSheet(0)->setCellValue('C' . $shit .'',$this->tanggal->hariLengkap($list->tgl_lahir,"/"));
            $objPHPExcel->getActiveSheet(0)->setCellValue('D' . $shit . '','`'. $list->hp);
            $objPHPExcel->getActiveSheet(0)->setCellValue('E' . $shit . '','`'. $list->hp2);
            $objPHPExcel->getActiveSheet(0)->setCellValue('F' . $shit . '', $list->alamat);
            $objPHPExcel->getActiveSheet(0)->setCellValue('G' . $shit . '', $list->email);
            $objPHPExcel->getActiveSheet(0)->setCellValue('H' . $shit . '', $list->fb);
            $objPHPExcel->getActiveSheet(0)->setCellValue('I' . $shit . '', $list->twt);
            $objPHPExcel->getActiveSheet(0)->setCellValue('J' . $shit . '', $list->ig);
            $objPHPExcel->getActiveSheet(0)->setCellValue('K' . $shit . '', '`'. $list->bpjs);
            $objPHPExcel->getActiveSheet(0)->setCellValue('L' . $shit . '', $this->tanggal->ind($list->tgl_masuk_kerja,"/"));
            $objPHPExcel->getActiveSheet(0)->setCellValue('M' . $shit . '', $this->tanggal->ind($list->tgl_habis_kontrak,"/"));

			$objPHPExcel->getActiveSheet(0)->setCellValue('N' . $shit . '', $this->jmlListing($list->id_agen));
            $objPHPExcel->getActiveSheet(0)->setCellValue('O' . $shit . '', $this->jmlPenjualan($list->id_agen));
            $objPHPExcel->getActiveSheet(0)->setCellValue('P' . $shit . '', $this->reff->jmlPelanggan($list->id_agen));
			
			
            $objPHPExcel->getActiveSheet(0)->setCellValue('Q' . $shit . '',  ucwords($list->bank));
            $objPHPExcel->getActiveSheet(0)->setCellValue('R' . $shit . '','`'.    $list->no_rek);
            $objPHPExcel->getActiveSheet(0)->setCellValue('S' . $shit . '',   $list->atas_nama_bank);
			
            $objPHPExcel->getActiveSheet(0)->setCellValue('T' . $shit . '',   $list->nama_wakil);
            $objPHPExcel->getActiveSheet(0)->setCellValue('U' . $shit . '', '`'.   $list->nomor_wakil);
            $objPHPExcel->getActiveSheet(0)->setCellValue('V' . $shit . '',   $list->hubungan_wakil);
			
            $objPHPExcel->getActiveSheet(0)->setCellValue('W' . $shit . '',  $pas);
            $objPHPExcel->getActiveSheet(0)->setCellValue('X' . $shit . '',   $this->tanggal->hariLengkap($list->tgl_lahir_pasangan,"/"));
			
            $objPHPExcel->getActiveSheet(0)->setCellValue('Y' . $shit . '',   $list->anak1);
            $objPHPExcel->getActiveSheet(0)->setCellValue('Z' . $shit . '',   $this->tanggal->hariLengkap($list->tgl_lahir_anak1,"/"));			
            $objPHPExcel->getActiveSheet(0)->setCellValue('AA' . $shit . '',   $list->anak2);
            $objPHPExcel->getActiveSheet(0)->setCellValue('AB' . $shit . '',   $this->tanggal->hariLengkap($list->tgl_lahir_anak2,"/"));
			
		
			
        }

//auto size column
	$abjad="A";
for($a=1;$a<=27;$a++)
{

        $objPHPExcel->getActiveSheet(0)->getColumnDimension($abjad++)->setAutoSize(true);
}

// Rename worksheet (worksheet, not filename)
        $objPHPExcel->getActiveSheet(0)->setTitle('MEMBER');
        
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
        
/*------------------------------------------------------------*/

$getAgen=$this->db->query("SELECT * FROM data_agen WHERE id_agen IN (SELECT id_agen FROM report_listing) OR id_agen IN(SELECT id_agen FROM report_costumer) order by nama ASC")->result();
$nosheet=0;
foreach($getAgen as $val)
{
	$nosheet++;
        $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, $val->nama);
        $objPHPExcel->addSheet($myWorkSheet, $nosheet);
		$abjad="A";
		for($a=1;$a<=11;$a++)
		{
        $objPHPExcel->getActiveSheet($nosheet)->getColumnDimension($abjad++)->setAutoSize(true);
		}
		
		
		$objPHPExcel->getSheet($nosheet)->mergeCells('A1:E1');
        $objPHPExcel->getSheet($nosheet)->setCellValue('A1', 'LISTING PROGRESS');
        $objPHPExcel->getSheet($nosheet)->setCellValue('A2', 'No');
        $objPHPExcel->getSheet($nosheet)->setCellValue('B2', 'Date');
        $objPHPExcel->getSheet($nosheet)->setCellValue('C2', 'Code Listing');
        $objPHPExcel->getSheet($nosheet)->setCellValue('D2', 'Kategory');
        $objPHPExcel->getSheet($nosheet)->setCellValue('E2', 'Description');
        $objPHPExcel->getSheet($nosheet)->getStyle('A1:E1')->applyFromArray($style2);
        $objPHPExcel->getSheet($nosheet)->getStyle('A2:E2')->applyFromArray($style2);
///
		$objPHPExcel->getSheet($nosheet)->mergeCells('G1:K1');
        $objPHPExcel->getSheet($nosheet)->setCellValue('G1', 'COSTUMERS PROGRESS');
        $objPHPExcel->getSheet($nosheet)->setCellValue('G2', 'No');
        $objPHPExcel->getSheet($nosheet)->setCellValue('H2', 'Date');
        $objPHPExcel->getSheet($nosheet)->setCellValue('I2', 'Costumer');
        $objPHPExcel->getSheet($nosheet)->setCellValue('J2', 'Kategory');
        $objPHPExcel->getSheet($nosheet)->setCellValue('K2', 'Description');
        $objPHPExcel->getSheet($nosheet)->getStyle('G1:K1')->applyFromArray($style2);
        $objPHPExcel->getSheet($nosheet)->getStyle('G2:K2')->applyFromArray($style2);
		
		
			$shit = 2;$jk="";$list="";$no=1;
			$database = $this->db->query("SELECT * from report_listing where id_agen='".$val->id_agen."' ")->result();
			foreach ($database as $list) {
            $shit++;
			$objPHPExcel->getActiveSheet($nosheet)->setCellValue('A' . $shit . '',   $no++);	
			$objPHPExcel->getActiveSheet($nosheet)->setCellValue('B' . $shit . '',   $this->tanggal->indjam($list->tgl,"/"));	
			$objPHPExcel->getActiveSheet($nosheet)->setCellValue('C' . $shit . '',   $list->kode_listing);		
			$objPHPExcel->getActiveSheet($nosheet)->setCellValue('D' . $shit . '',   $this->reff->getNamaTitleListing($list->id_title));		
			$objPHPExcel->getActiveSheet($nosheet)->setCellValue('E' . $shit . '',   $list->ket);		
			}
			
			$shiti = 2;$jk="";$list="";$nom=1;
			$database = $this->db->query("SELECT * from report_costumer where id_agen='".$val->id_agen."' ")->result();
			foreach ($database as $list) {
            $shiti++;
			$objPHPExcel->getActiveSheet($nosheet)->setCellValue('G' . $shiti . '',   $nom++);	
			$objPHPExcel->getActiveSheet($nosheet)->setCellValue('H' . $shiti . '',   $this->tanggal->indjam($list->tgl,"/"));	
			$objPHPExcel->getActiveSheet($nosheet)->setCellValue('I' . $shiti . '',   $this->reff->getNamaPelanggan($list->id_costumer));		
			$objPHPExcel->getActiveSheet($nosheet)->setCellValue('J' . $shiti . '',   $this->reff->getNamaTitleCostumer($list->id_title));		
			$objPHPExcel->getActiveSheet($nosheet)->setCellValue('K' . $shiti . '',   $list->ket);		
			}
			
			
		
		
///
}
       
	   
/*------------------------------------------------------------*/
		$objPHPExcel->setActiveSheetIndex(0);
        header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Data-Member.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
//////finish
    
	}
	function jmlListing($id)
	{
		$this->db->where("agen",$id);
	return	$this->db->get("data_property")->num_rows();
	}
	function jmlPenjualan($id)
	{
		$this->db->where("agen",$id);
		$this->db->where("status","1");
	return	$this->db->get("data_property")->num_rows();
	}

	function downloadFormat()
	{
	//////start
		$objPHPExcel = new PHPExcel();
		//style
		$style = array( 
		 'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				  'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				  'rotation'   => 0,
		  ),
		   'fill' => array(
				  'type' => PHPExcel_Style_Fill::FILL_SOLID,
				  'color' => array('rgb' => 'ccccff')
			  ),
		 'borders' => 
		  array( 'allborders' => 
			array( 'style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'), 
			  ), 
			), 
		);	
		
		$head = array( 
			'font'  => array(
			'bold'  => true,
			'color' => array('rgb' => 'FFFFFF'),
			),
			
		   'fill' => array(
				  'type' => PHPExcel_Style_Fill::FILL_SOLID,
				  'color' => array('rgb' => '009966')
			  ),
		 'borders' => 
		  array( 'allborders' => 
			array( 'style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'), 
			  ), 
			), 
		);
		
		
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('A')->setWidth(25);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('B')->setWidth(25);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('C')->setWidth(25);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('D')->setWidth(25);

		
		
		//create column
		
		$objPHPExcel->getActiveSheet(0)->setCellValue('A1', 'Nama');
		$objPHPExcel->getActiveSheet(0)->setCellValue('B1', 'Nomor Hp');
		$objPHPExcel->getActiveSheet(0)->setCellValue('C1', 'Email');
		$objPHPExcel->getActiveSheet(0)->setCellValue('D1', 'Alamat');
		
		//make a border column
		$objPHPExcel->getActiveSheet(0)->getStyle('A1:D1')->applyFromArray($style);
		
		
		
		// Rename worksheet (worksheet, not filename)
		$objPHPExcel->getActiveSheet(0)->setTitle('Data Agen');
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		
		header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Form Agen.xlsx"');
		header('Cache-Control: max-age=0');
		 
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		//////finish
		
	}
	function importData()
	{
	$sukses=0;$gagal=0;$edit=0;
		 $file   = explode('.',$_FILES['userfile']['name']);
		$length = count($file);
		if($file[$length -1] == 'xlsx' || $file[$length -1] == 'xls'){//jagain barangkali uploadnya selain file excel <span class="wp-smiley wp-emoji wp-emoji-smile" title=":-)">:-)</span>
        $tmp    = $_FILES['userfile']['tmp_name'];//Baca dari tmp folder jadi file ga perlu jadi sampah di server :-p
		      
				 // load excel
			    $file = $_FILES['userfile']['tmp_name'];
			    $load = PHPExcel_IOFactory::load($file);
                $sheets = $load->getActiveSheet()->toArray(null,true,true,true);
				$i=1;
				foreach ($sheets as $sheet) {
				if ($i > 1) {						
					//	$hp=str_replace("'","",$sheet[1]);
					//	$hp=str_replace("`","",$hp);
					//	$hp=str_replace("+62","0",$hp);
									
							//get form
															
						$sql=array(
						"kode_agen"=>$kode=$sheet[1],
						"nama"=>$sheet[0],
					//	"hp"=>$hp=$this->db->escape_str($hp),
					//	"email"=>$sheet[2],
					//	"alamat"=>$sheet[3],
						);		
				       
						$cek=$this->cekAgen($kode);
					if($cek==0)//jika data kontak tidak ada maka tambahkan
					{  
							$this->db->insert("data_agen",$sql); $sukses++;
					}/*else{ //jika data kontak ada maka edit yg ada
						$sql=array(
						"nama"=>$sheet[0],
						"hp"=>$hp=$this->db->escape_str($hp),
						"email"=>$sheet[2],
						"alamat"=>$sheet[3],
						);						
							$this->db->where("hp",$hp);
							$this->db->update("data_agen",$sql);
							$edit++;
					};*/
					   
				}
				$i++;
                }
               
		}else{
        exit('<br><b style="color:red">Upload Gagal! Gunakan Format Ms.Excell yang telah di sediakan</b>');   //pesan error tipe file tidak tepat
		}

		return $sukses."-".$edit."-".$gagal;
	}
	
}