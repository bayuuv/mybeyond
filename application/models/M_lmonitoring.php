<?php

class M_lmonitoring extends CI_Model  {
    
		
	function __construct()
    {
        parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
    }
	
	/*---------------------------------------------*/
		
	function get_dataLmonitoring()
	{
		 $query = $this->_get_dataLmonitoring();
        if ($_POST['length'] != -1)
            $query .= " limit " . $_POST['start'] . "," . $_POST['length'];
        return $this->db->query($query)->result();
 
	}
	
	public function counts() {
        $query = $this->_get_dataLmonitoring();
        return $this->db->query($query)->num_rows();
    }
	
	function _get_dataLmonitoring()
	{
	$dan="";
	$agen=$this->input->get("agen");
	if($agen){
		$dan.=" AND agen='$agen'";
	}   
		if($this->session->userdata("id")==151){ // Rafha
			$query = "SELECT id_agen, kode_agen, nama FROM data_agen
			WHERE jabatan IN ('1','11', '12') AND aktifasi = '1' AND kode_agen='BREA/039/I/2018' $dan";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				nama LIKE '%" . $searchkey . "%'
				) ";
			}
		}elseif($this->session->userdata("id")==146){ // yudi
			$query = "SELECT id_agen, kode_agen, nama FROM data_agen
			WHERE jabatan IN ('1','11', '12') AND aktifasi = '1' AND kode_agen='BREA/032/XI/2017' $dan";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				nama LIKE '%" . $searchkey . "%'
				) ";
			}
		}elseif($this->session->userdata("id")==152){ // vivi
			$query = "SELECT id_agen, kode_agen, nama FROM data_agen
			WHERE jabatan IN ('1','11', '12') AND aktifasi = '1' AND kode_agen NOT IN ('BREA/046/II/2018', 'BREA/009/III/2017', 'BREA/032/XI/2017', 'BREA/001/I/2017', 'BREA/005/II/2017', 'BREA/012/VIII/2017', 'BREA/066/VII/2018', 'BREA/010/I/2017') $dan";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				nama LIKE '%" . $searchkey . "%'
				) ";
			}
		}elseif($this->session->userdata("id")==161){ // Yema
			$query = "SELECT id_agen, kode_agen, nama FROM data_agen
			WHERE jabatan IN ('1','11', '12') AND aktifasi = '1' AND kode_agen IN ('BREA/001/I/2017') $dan";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				nama LIKE '%" . $searchkey . "%'
				) ";
			}
		}elseif($this->session->userdata("id")==162){ // Frans
			$query = "SELECT id_agen, kode_agen, nama FROM data_agen
			WHERE jabatan IN ('1','11', '12') AND aktifasi = '1' AND kode_agen IN ('BREA/046/II/2018', 'BREA/010/I/2017') $dan";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				nama LIKE '%" . $searchkey . "%'
				) ";
			}
		}elseif($this->session->userdata("id")==165){ // Rena
			$query = "SELECT id_agen, kode_agen, nama FROM data_agen
			WHERE jabatan IN ('1','11', '12') AND aktifasi = '1' AND kode_agen IN ('BREA/005/II/2017', 'BREA/012/VIII/2017', 'BREA/066/VII/2018') $dan";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				nama LIKE '%" . $searchkey . "%'
				) ";
			}
		}else{
			$query = "SELECT id_agen, kode_agen, nama FROM data_agen
			WHERE jabatan IN ('1','11', '12') AND aktifasi = '1' $dan";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				nama LIKE '%" . $searchkey . "%'
				) ";
			}
		}

        $column = array('','','id_agen','kode_agen', 'nama');
        $i = 0;
        foreach ($column as $item) {
            $column[$i] = $item;
        }

      /*  if (isset($_POST['order'])) {
		//	$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            $query .= " order by " . $column[$_POST['order']['0']['column']] . " " . $_POST['order']['0']['dir'];
        } else if (isset($order)) {
            $order = $order;
			//	$this->db->order_by(key($order), $order[key($order)]);
			       $query .= " order by nama ASC";
        }*/
		$query.=" order by id_agen ASC" ;
        return $query;
	}
	
	function getTargetGoal($id,$datee)
	{	
		$this->db->where("agen",$id);
		$this->db->where("kategori","1");
		$this->db->where("bulan",$datee);
		$data=$this->db->get("tr_target")->row();
	return isset($data->jumlah)?($data->jumlah):"0";
	}
	
	function getTargetGoal1($id,$datee)
	{
		$pisah = explode("-",$datee);
		$tahun = $pisah[0];
		$bulan = $pisah[1];
		
		//$data=$this->db->query("SELECT SUM(total_komisi) as tk from data_selling WHERE year(tgl_closing) = '".$tahun."' AND month(tgl_closing) = '".$bulan."' AND (kode_agen='".$id ."' OR selling='".$id."')")->row();
		$data=$this->db->query("SELECT SUM(nominal_komisi_listing) as tk from data_selling WHERE year(tgl_closing) = '".$tahun."' AND month(tgl_closing) = '".$bulan."' AND kode_agen='".$id ."'")->row();
		isset($data->tk)?($data->tk):"0";
		$data2=$this->db->query("SELECT SUM(nominal_komisi_selling) as tk from data_selling WHERE year(tgl_closing) = '".$tahun."' AND month(tgl_closing) = '".$bulan."' AND selling='".$id ."'")->row();
		isset($data2->tk)?($data2->tk):"0";
		$totaldata = $data->tk + $data2->tk;
	return isset($totaldata)?($totaldata):"0";
	}
	
	function getTargetGoal2($id,$datee)
	{
		$pisah = explode("-",$datee);
		$tahun = $pisah[0];
		$bulan = $pisah[1]+1;
		
		//$data=$this->db->query("SELECT SUM(total_komisi) as tk from data_selling WHERE year(tgl_closing) = '".$tahun."' AND month(tgl_closing) = '".$bulan."' AND (kode_agen='".$id ."' OR selling='".$id."')")->row();
		$data=$this->db->query("SELECT SUM(nominal_komisi_listing) as tk from data_selling WHERE year(tgl_closing) = '".$tahun."' AND month(tgl_closing) = '".$bulan."' AND kode_agen='".$id ."'")->row();
		isset($data->tk)?($data->tk):"0";
		$data2=$this->db->query("SELECT SUM(nominal_komisi_selling) as tk from data_selling WHERE year(tgl_closing) = '".$tahun."' AND month(tgl_closing) = '".$bulan."' AND selling='".$id ."'")->row();
		isset($data2->tk)?($data2->tk):"0";
		$totaldata = $data->tk + $data2->tk;
	return isset($totaldata)?($totaldata):"0";
	}
	
	function getTargetGoal3($id,$datee)
	{
		$pisah = explode("-",$datee);
		$tahun = $pisah[0];
		$bulan = $pisah[1]+2;
		
		//$data=$this->db->query("SELECT SUM(total_komisi) as tk from data_selling WHERE year(tgl_closing) = '".$tahun."' AND month(tgl_closing) = '".$bulan."' AND (kode_agen='".$id ."' OR selling='".$id."')")->row();
		$data=$this->db->query("SELECT SUM(nominal_komisi_listing) as tk from data_selling WHERE year(tgl_closing) = '".$tahun."' AND month(tgl_closing) = '".$bulan."' AND kode_agen='".$id ."'")->row();
		isset($data->tk)?($data->tk):"0";
		$data2=$this->db->query("SELECT SUM(nominal_komisi_selling) as tk from data_selling WHERE year(tgl_closing) = '".$tahun."' AND month(tgl_closing) = '".$bulan."' AND selling='".$id ."'")->row();
		isset($data2->tk)?($data2->tk):"0";
		$totaldata = $data->tk + $data2->tk;
	return isset($totaldata)?($totaldata):"0";
	}
	
	function getTargetGoal4($id,$datee)
	{
		$pisah = explode("-",$datee);
		$tahun = $pisah[0];
		$bulan = $pisah[1]+3;
		
		//$data=$this->db->query("SELECT SUM(total_komisi) as tk from data_selling WHERE year(tgl_closing) = '".$tahun."' AND month(tgl_closing) = '".$bulan."' AND (kode_agen='".$id ."' OR selling='".$id."')")->row();
		$data=$this->db->query("SELECT SUM(nominal_komisi_listing) as tk from data_selling WHERE year(tgl_closing) = '".$tahun."' AND month(tgl_closing) = '".$bulan."' AND kode_agen='".$id ."'")->row();
		isset($data->tk)?($data->tk):"0";
		$data2=$this->db->query("SELECT SUM(nominal_komisi_selling) as tk from data_selling WHERE year(tgl_closing) = '".$tahun."' AND month(tgl_closing) = '".$bulan."' AND selling='".$id ."'")->row();
		isset($data2->tk)?($data2->tk):"0";
		$totaldata = $data->tk + $data2->tk;
	return isset($totaldata)?($totaldata):"0";
	}
	
	function getTargetTransaksi($id,$datee)
	{	
		$this->db->where("agen",$id);
		$this->db->where("kategori","2");
		$this->db->where("bulan",$datee);
		$data=$this->db->get("tr_target")->row();
	return isset($data->jumlah)?($data->jumlah):"0";
	}
	
	function getTargetTransaksi1($id,$datee)
	{
		$pisah = explode("-",$datee);
		$tahun = $pisah[0];
		$bulan = $pisah[1];
		
		return $this->db->query("SELECT total_komisi from data_selling WHERE year(tgl_closing) = '".$tahun."' AND month(tgl_closing) = '".$bulan."' AND (kode_agen='".$id ."' OR selling='".$id."')")->num_rows();
	//return isset($data->tk)?($data->tk):"---";
	}
	
	function getTargetTransaksi2($id,$datee)
	{
		$pisah = explode("-",$datee);
		$tahun = $pisah[0];
		$bulan = $pisah[1]+1;
		
		return $this->db->query("SELECT total_komisi from data_selling WHERE year(tgl_closing) = '".$tahun."' AND month(tgl_closing) = '".$bulan."' AND (kode_agen='".$id ."' OR selling='".$id."')")->num_rows();
	//return isset($data->tk)?($data->tk):"---";
	}
	
	function getTargetTransaksi3($id,$datee)
	{
		$pisah = explode("-",$datee);
		$tahun = $pisah[0];
		$bulan = $pisah[1]+2;
		
		return $this->db->query("SELECT total_komisi from data_selling WHERE year(tgl_closing) = '".$tahun."' AND month(tgl_closing) = '".$bulan."' AND (kode_agen='".$id ."' OR selling='".$id."')")->num_rows();
	//return isset($data->tk)?($data->tk):"---";
	}
	
	function getTargetTransaksi4($id,$datee)
	{
		$pisah = explode("-",$datee);
		$tahun = $pisah[0];
		$bulan = $pisah[1]+3;
		
		return $this->db->query("SELECT total_komisi from data_selling WHERE year(tgl_closing) = '".$tahun."' AND month(tgl_closing) = '".$bulan."' AND (kode_agen='".$id ."' OR selling='".$id."')")->num_rows();
	//return isset($data->tk)?($data->tk):"---";
	}
	
	function getTargetListing($id,$datee)
	{	
		$this->db->where("agen",$id);
		$this->db->where("kategori","3");
		$this->db->where("bulan",$datee);
		$data=$this->db->get("tr_target")->row();
	return isset($data->jumlah)?($data->jumlah):"0";
	}
	
	function getTargetListing1($id,$datee)
	{
		$pisah = explode("-",$datee);
		$tahun = $pisah[0];
		$bulan = $pisah[1];
		
		return $this->db->query("SELECT kode_prop from data_property WHERE year(created_time) = '".$tahun."' AND month(created_time) = '".$bulan."' AND agen='".$id ."'")->num_rows();
	//return isset($data->tk)?($data->tk):"---";
	}
	
	function getTargetListing2($id,$datee)
	{
		$pisah = explode("-",$datee);
		$tahun = $pisah[0];
		$bulan = $pisah[1]+1;
		
		return $this->db->query("SELECT kode_prop from data_property WHERE year(created_time) = '".$tahun."' AND month(created_time) = '".$bulan."' AND agen='".$id ."'")->num_rows();
	//return isset($data->tk)?($data->tk):"---";
	}
	
	function getTargetListing3($id,$datee)
	{
		$pisah = explode("-",$datee);
		$tahun = $pisah[0];
		$bulan = $pisah[1]+2;
		
		return $this->db->query("SELECT kode_prop from data_property WHERE year(created_time) = '".$tahun."' AND month(created_time) = '".$bulan."' AND agen='".$id ."'")->num_rows();
	//return isset($data->tk)?($data->tk):"---";
	}
	
	function getTargetListing4($id,$datee)
	{
		$pisah = explode("-",$datee);
		$tahun = $pisah[0];
		$bulan = $pisah[1]+3;
		
		return $this->db->query("SELECT kode_prop from data_property WHERE year(created_time) = '".$tahun."' AND month(created_time) = '".$bulan."' AND agen='".$id ."'")->num_rows();
	//return isset($data->tk)?($data->tk):"---";
	}
	
	function getTargetPromoOnline($id,$datee)
	{	
		$this->db->where("agen",$id);
		$this->db->where("kategori","4");
		$this->db->where("bulan",$datee);
		$data=$this->db->get("tr_target")->row();
	return isset($data->jumlah)?($data->jumlah):"0";
	}
	
	function getTargetPromoOnline1($id,$datee)
	{
		$pisah = explode("-",$datee);
		$tahun = $pisah[0];
		$bulan = $pisah[1];
		
		$data=$this->db->query("SELECT SUM(jumlah_promoonline) AS jumlah from promo_online WHERE year(tgl_promoonline) = '".$tahun."' AND month(tgl_promoonline) ='".$bulan."' AND agen='".$id ."'")->row();
	return isset($data->jumlah)?($data->jumlah):"0";
	}
	
	function getTargetPromoOnline2($id,$datee)
	{
		$pisah = explode("-",$datee);
		$tahun = $pisah[0];
		$bulan = $pisah[1]+1;
		
		$data=$this->db->query("SELECT SUM(jumlah_promoonline) AS jumlah from promo_online WHERE year(tgl_promoonline) = '".$tahun."' AND month(tgl_promoonline) ='".$bulan."' AND agen='".$id ."'")->row();
	return isset($data->jumlah)?($data->jumlah):"0";
	}
	
	function getTargetPromoOnline3($id,$datee)
	{
		$pisah = explode("-",$datee);
		$tahun = $pisah[0];
		$bulan = $pisah[1]+2;
		
		$data=$this->db->query("SELECT SUM(jumlah_promoonline) AS jumlah from promo_online WHERE year(tgl_promoonline) = '".$tahun."' AND month(tgl_promoonline) ='".$bulan."' AND agen='".$id ."'")->row();
	return isset($data->jumlah)?($data->jumlah):"0";
	}
	
	function getTargetPromoOnline4($id,$datee)
	{
		$pisah = explode("-",$datee);
		$tahun = $pisah[0];
		$bulan = $pisah[1]+3;
		
		$data=$this->db->query("SELECT SUM(jumlah_promoonline) AS jumlah from promo_online WHERE year(tgl_promoonline) = '".$tahun."' AND month(tgl_promoonline) ='".$bulan."' AND agen='".$id ."'")->row();
	return isset($data->jumlah)?($data->jumlah):"0";
	}
	
	function getTargetPromoOffline($id,$datee)
	{	
		$this->db->where("agen",$id);
		$this->db->where("kategori","7");
		$this->db->where("bulan",$datee);
		$data=$this->db->get("tr_target")->row();
	return isset($data->jumlah)?($data->jumlah):"0";
	}
	
	function getTargetPromoOffline1($id,$datee)
	{
		$pisah = explode("-",$datee);
		$tahun = $pisah[0];
		$bulan = $pisah[1];
		
		return $this->db->query("SELECT a.id_promo from data_promo AS a LEFT JOIN data_property AS b ON a.kode_prop = b.kode_prop WHERE year(a.tgl_promo) = '".$tahun."' AND month(a.tgl_promo) ='".$bulan."' AND a.status = '1' AND b.agen='".$id ."'")->num_rows();
	//return isset($data->tk)?($data->tk):"---";
	}
	
	function getTargetPromoOffline2($id,$datee)
	{
		$pisah = explode("-",$datee);
		$tahun = $pisah[0];
		$bulan = $pisah[1]+1;
		
		return $this->db->query("SELECT a.id_promo from data_promo AS a LEFT JOIN data_property AS b ON a.kode_prop = b.kode_prop WHERE year(a.tgl_promo) = '".$tahun."' AND month(a.tgl_promo) ='".$bulan."' AND a.status = '1' AND b.agen='".$id ."'")->num_rows();
	//return isset($data->tk)?($data->tk):"---";
	}
	
	function getTargetPromoOffline3($id,$datee)
	{
		$pisah = explode("-",$datee);
		$tahun = $pisah[0];
		$bulan = $pisah[1]+2;
		
		return $this->db->query("SELECT a.id_promo from data_promo AS a LEFT JOIN data_property AS b ON a.kode_prop = b.kode_prop WHERE year(a.tgl_promo) = '".$tahun."' AND month(a.tgl_promo) ='".$bulan."' AND a.status = '1' AND b.agen='".$id ."'")->num_rows();
	//return isset($data->tk)?($data->tk):"---";
	}
	
	function getTargetPromoOffline4($id,$datee)
	{
		$pisah = explode("-",$datee);
		$tahun = $pisah[0];
		$bulan = $pisah[1]+3;
		
		return $this->db->query("SELECT a.id_promo from data_promo AS a LEFT JOIN data_property AS b ON a.kode_prop = b.kode_prop WHERE year(a.tgl_promo) = '".$tahun."' AND month(a.tgl_promo) ='".$bulan."' AND a.status = '1' AND b.agen='".$id ."'")->num_rows();
	//return isset($data->tk)?($data->tk):"---";
	}
	
	function getTargetCallin($id,$datee)
	{	
		$this->db->where("agen",$id);
		$this->db->where("kategori","5");
		$this->db->where("bulan",$datee);
		$data=$this->db->get("tr_target")->row();
	return isset($data->jumlah)?($data->jumlah):"0";
	}
	
	function getTargetCallin1($id,$datee)
	{
		$pisah = explode("-",$datee);
		$tahun = $pisah[0];
		$bulan = $pisah[1];
		
		$data=$this->db->query("SELECT SUM(jumlah) AS jumlah from tr_pa WHERE year(tgl_promo) = '".$tahun."' AND month(tgl_promo) = '".$bulan."' AND kategori = '1' AND agen='".$id ."'")->row();
	return isset($data->jumlah)?($data->jumlah):"0";
	}
	
	function getTargetCallin2($id,$datee)
	{
		$pisah = explode("-",$datee);
		$tahun = $pisah[0];
		$bulan = $pisah[1]+1;
		
		$data=$this->db->query("SELECT SUM(jumlah) AS jumlah from tr_pa WHERE year(tgl_promo) = '".$tahun."' AND month(tgl_promo) = '".$bulan."' AND kategori = '1' AND agen='".$id ."'")->row();
	return isset($data->jumlah)?($data->jumlah):"0";
	}
	
	function getTargetCallin3($id,$datee)
	{
		$pisah = explode("-",$datee);
		$tahun = $pisah[0];
		$bulan = $pisah[1]+2;
		
		$data=$this->db->query("SELECT SUM(jumlah) AS jumlah from tr_pa WHERE year(tgl_promo) = '".$tahun."' AND month(tgl_promo) = '".$bulan."' AND kategori = '1' AND agen='".$id ."'")->row();
	return isset($data->jumlah)?($data->jumlah):"0";
	}
	
	function getTargetCallin4($id,$datee)
	{
		$pisah = explode("-",$datee);
		$tahun = $pisah[0];
		$bulan = $pisah[1]+3;
		
		$data=$this->db->query("SELECT SUM(jumlah) AS jumlah from tr_pa WHERE year(tgl_promo) = '".$tahun."' AND month(tgl_promo) = '".$bulan."' AND kategori = '1' AND agen='".$id ."'")->row();
	return isset($data->jumlah)?($data->jumlah):"0";
	}
	
	function getTargetShowing($id,$datee)
	{	
		$this->db->where("agen",$id);
		$this->db->where("kategori","6");
		$this->db->where("bulan",$datee);
		$data=$this->db->get("tr_target")->row();
	return isset($data->jumlah)?($data->jumlah):"0";
	}
	
	function getTargetShowing1($id,$datee)
	{
		$pisah = explode("-",$datee);
		$tahun = $pisah[0];
		$bulan = $pisah[1];
		
		$data=$this->db->query("SELECT SUM(jumlah) AS jumlah from tr_pa WHERE year(tgl_promo) = '".$tahun."' AND month(tgl_promo) = '".$bulan."' AND kategori = '2' AND agen='".$id ."'")->row();
	return isset($data->jumlah)?($data->jumlah):"0";
	}
	
	function getTargetShow1($id,$datee)
	{
		$pisah = explode("-",$datee);
		$tahun = $pisah[0];
		$bulan = $pisah[1];
		
		return $this->db->query("SELECT id_showing from data_showing WHERE year(tgl_showing) = '".$tahun."' AND month(tgl_showing) ='".$bulan."' AND agen='".$id ."'")->num_rows();
	//return isset($data->tk)?($data->tk):"---";
	}
	
	function getTargetShowing2($id,$datee)
	{
		$pisah = explode("-",$datee);
		$tahun = $pisah[0];
		$bulan = $pisah[1]+1;
		
		$data=$this->db->query("SELECT SUM(jumlah) AS jumlah from tr_pa WHERE year(tgl_promo) = '".$tahun."' AND month(tgl_promo) = '".$bulan."' AND kategori = '2' AND agen='".$id ."'")->row();
	return isset($data->jumlah)?($data->jumlah):"0";
	}
	
	function getTargetShow2($id,$datee)
	{
		$pisah = explode("-",$datee);
		$tahun = $pisah[0];
		$bulan = $pisah[1]+1;
		
		return $this->db->query("SELECT id_showing from data_showing WHERE year(tgl_showing) = '".$tahun."' AND month(tgl_showing) ='".$bulan."' AND agen='".$id ."'")->num_rows();
	//return isset($data->tk)?($data->tk):"---";
	}
	
	function getTargetShowing3($id,$datee)
	{
		$pisah = explode("-",$datee);
		$tahun = $pisah[0];
		$bulan = $pisah[1]+2;
		
		$data=$this->db->query("SELECT SUM(jumlah) AS jumlah from tr_pa WHERE year(tgl_promo) = '".$tahun."' AND month(tgl_promo) = '".$bulan."' AND kategori = '2' AND agen='".$id ."'")->row();
	return isset($data->jumlah)?($data->jumlah):"0";
	}
	
	function getTargetShow3($id,$datee)
	{
		$pisah = explode("-",$datee);
		$tahun = $pisah[0];
		$bulan = $pisah[1]+2;
		
		return $this->db->query("SELECT id_showing from data_showing WHERE year(tgl_showing) = '".$tahun."' AND month(tgl_showing) ='".$bulan."' AND agen='".$id ."'")->num_rows();
	//return isset($data->tk)?($data->tk):"---";
	}
	
	function getTargetShowing4($id,$datee)
	{
		$pisah = explode("-",$datee);
		$tahun = $pisah[0];
		$bulan = $pisah[1]+3;
		
		$data=$this->db->query("SELECT SUM(jumlah) AS jumlah from tr_pa WHERE year(tgl_promo) = '".$tahun."' AND month(tgl_promo) = '".$bulan."' AND kategori = '2' AND agen='".$id ."'")->row();
	return isset($data->jumlah)?($data->jumlah):"0";
	}
	
	function getTargetShow4($id,$datee)
	{
		$pisah = explode("-",$datee);
		$tahun = $pisah[0];
		$bulan = $pisah[1]+3;
		
		return $this->db->query("SELECT id_showing from data_showing WHERE year(tgl_showing) = '".$tahun."' AND month(tgl_showing) ='".$bulan."' AND agen='".$id ."'")->num_rows();
	//return isset($data->tk)?($data->tk):"---";
	}
	
	function format_angka($angka){
    // cek nilai $angka, kalau bukan format angka (ada karakter selain 0-9) keluarkan pesan "Bukan format angka!"
    if (!is_numeric($angka)) return "Bukan format angka!";    
    // lakukan pemformatan dengan fungsi number_format
    $hasil  = number_format(
                (double)$angka,     // parsing bahwa nilai $angka harus angka (double), boleh int atau float
                2,                  // jumlah angka dibelakang koma
                ",",                // pemisah desimal -> 0,00
                "."                 // pemisah ribuan  -> 1.000
              );
	
	$hapus = substr($hasil, 0, -2);
	//$jadi = rtrim($hapus,", ");
	$jadi = str_replace(',', ' ', $hapus);
	
    return $jadi;
	}
	
	function export()
	{
		//error_reporting(0);
		//header ("Content-type: text/html; charset=utf-8");
		
		//$this->load->library('PHPExcel');
        //$this->load->helper('download');
//////start
        $objPHPExcel = new PHPExcel();
//style
        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'rotation' => 0,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '6CCECB')
            ),
            'borders' =>
            array('allborders' =>
                array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'),
                ),
            ),
        );
		
		$style2 = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'rotation' => 0,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'F5A9BC')
            ),
            'borders' =>
            array('allborders' =>
                array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'),
                ),
            ),
        );
		
		$style3 = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'rotation' => 0,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'A9F5F2')
            ),
            'borders' =>
            array('allborders' =>
                array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'),
                ),
            ),
        );
		
		$style4 = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'rotation' => 0,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'D8CEF6')
            ),
            'borders' =>
            array('allborders' =>
                array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'),
                ),
            ),
        );
		
		$style5 = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'rotation' => 0,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'E1F5A9')
            ),
            'borders' =>
            array('allborders' =>
                array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'),
                ),
            ),
        );
		
		$style6 = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'rotation' => 0,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'E0F8F7')
            ),
            'borders' =>
            array('allborders' =>
                array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'),
                ),
            ),
        );
		
		$style7 = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'rotation' => 0,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'FACC2E')
            ),
            'borders' =>
            array('allborders' =>
                array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'),
                ),
            ),
        );
		
		$style8 = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'rotation' => 0,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'CEF6F5')
            ),
            'borders' =>
            array('allborders' =>
                array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'),
                ),
            ),
        );
		
		$style9 = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'rotation' => 0,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'F6CEEC')
            ),
            'borders' =>
            array('allborders' =>
                array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'),
                ),
            ),
        );
		
		$style10 = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'rotation' => 0,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'F6E3CE')
            ),
            'borders' =>
            array('allborders' =>
                array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'),
                ),
            ),
        );
		
		$style11 = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'rotation' => 0,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'FFFF00')
            ),
            'borders' =>
            array('allborders' =>
                array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'),
                ),
            ),
        );
		
		$style12 = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'rotation' => 0,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'FABF8F')
            ),
            'borders' =>
            array('allborders' =>
                array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'),
                ),
            ),
        );
		
     //   $objPHPExcel->getActiveSheet(0)->getColumnDimension('F')->setWidth(25);
     //   $objPHPExcel->getActiveSheet(0)->getColumnDimension('G')->setWidth(25);
     //   $objPHPExcel->getActiveSheet(0)->getColumnDimension('H')->setWidth(35);


	//create column
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:A3');
		$objPHPExcel->getActiveSheet(0)->setCellValue('A1', 'NO');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B1:B3');
        $objPHPExcel->getActiveSheet(0)->setCellValue('B1', 'MEMBER');
		
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C1:I1');
		$objPHPExcel->getActiveSheet(0)->setCellValue('C1', 'GOAL');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C2:C3');
		$objPHPExcel->getActiveSheet(0)->setCellValue('C2', 'TARGET');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('D2:G2');
		$objPHPExcel->getActiveSheet(0)->setCellValue('D2', 'REALISASI');
		
		$today = date("Y-m-d");
		$pisah = explode("-",$today);
		$blnn = $pisah[1];
		
		if($blnn == '01' or $blnn == '02' or $blnn =='03' or $blnn == '04')
		{
			$objPHPExcel->getActiveSheet(0)->setCellValue('D3', 'Jan ');
			$objPHPExcel->getActiveSheet(0)->setCellValue('E3', 'Feb');
			$objPHPExcel->getActiveSheet(0)->setCellValue('F3', 'Mar');
			$objPHPExcel->getActiveSheet(0)->setCellValue('G3', 'Apr');
		}elseif($blnn == '05' or $blnn == '06' or $blnn == '07' or $blnn == '08')
		{
			$objPHPExcel->getActiveSheet(0)->setCellValue('D3', 'Mei');
			$objPHPExcel->getActiveSheet(0)->setCellValue('E3', 'Jun');
			$objPHPExcel->getActiveSheet(0)->setCellValue('F3', 'Jul');
			$objPHPExcel->getActiveSheet(0)->setCellValue('G3', 'Agt');
		}elseif($blnn == '09' or $blnn == '10' or $blnn == '11' or $blnn == '12')
		{
			$objPHPExcel->getActiveSheet(0)->setCellValue('D3', 'Sep');
			$objPHPExcel->getActiveSheet(0)->setCellValue('E3', 'Okt');
			$objPHPExcel->getActiveSheet(0)->setCellValue('F3', 'Nop');
			$objPHPExcel->getActiveSheet(0)->setCellValue('G3', 'Des');
		}
		
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('H2:H3');
		$objPHPExcel->getActiveSheet(0)->setCellValue('H2', 'TOTAL');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('I2:I3');
		$objPHPExcel->getActiveSheet(0)->setCellValue('I2', 'SISA');
		
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('J1:P1');
		$objPHPExcel->getActiveSheet(0)->setCellValue('J1', 'DEALING');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('J2:J3');
		$objPHPExcel->getActiveSheet(0)->setCellValue('J2', 'TARGET');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('K2:N2');
		$objPHPExcel->getActiveSheet(0)->setCellValue('K2', 'REALISASI');
		
		$today = date("Y-m-d");
		$pisah = explode("-",$today);
		$blnn = $pisah[1];
		
		if($blnn == '01' or $blnn == '02' or $blnn =='03' or $blnn == '04')
		{
			$objPHPExcel->getActiveSheet(0)->setCellValue('K3', 'Jan');
			$objPHPExcel->getActiveSheet(0)->setCellValue('L3', 'Feb');
			$objPHPExcel->getActiveSheet(0)->setCellValue('M3', 'Mar');
			$objPHPExcel->getActiveSheet(0)->setCellValue('N3', 'Apr');
		}elseif($blnn == '05' or $blnn == '06' or $blnn == '07' or $blnn == '08')
		{
			$objPHPExcel->getActiveSheet(0)->setCellValue('K3', 'Mei');
			$objPHPExcel->getActiveSheet(0)->setCellValue('L3', 'Jun');
			$objPHPExcel->getActiveSheet(0)->setCellValue('M3', 'Jul');
			$objPHPExcel->getActiveSheet(0)->setCellValue('N3', 'Agt');
		}elseif($blnn == '09' or $blnn == '10' or $blnn == '11' or $blnn == '12')
		{
			$objPHPExcel->getActiveSheet(0)->setCellValue('K3', 'Sep');
			$objPHPExcel->getActiveSheet(0)->setCellValue('L3', 'Okt');
			$objPHPExcel->getActiveSheet(0)->setCellValue('M3', 'Nop');
			$objPHPExcel->getActiveSheet(0)->setCellValue('N3', 'Des');
		}
		
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('O2:O3');
		$objPHPExcel->getActiveSheet(0)->setCellValue('O2', 'TOTAL');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('P2:P3');
		$objPHPExcel->getActiveSheet(0)->setCellValue('P2', 'SISA');
		
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('Q1:W1');
		$objPHPExcel->getActiveSheet(0)->setCellValue('Q1', 'LISTING');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('Q2:Q3');
		$objPHPExcel->getActiveSheet(0)->setCellValue('Q2', 'TARGET');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('R2:U2');
		$objPHPExcel->getActiveSheet(0)->setCellValue('R2', 'REALISASI');
		
		$today = date("Y-m-d");
		$pisah = explode("-",$today);
		$blnn = $pisah[1];
		
		if($blnn == '01' or $blnn == '02' or $blnn =='03' or $blnn == '04')
		{
			$objPHPExcel->getActiveSheet(0)->setCellValue('R3', 'Jan');
			$objPHPExcel->getActiveSheet(0)->setCellValue('S3', 'Feb');
			$objPHPExcel->getActiveSheet(0)->setCellValue('T3', 'Mar');
			$objPHPExcel->getActiveSheet(0)->setCellValue('U3', 'Apr');
		}elseif($blnn == '05' or $blnn == '06' or $blnn == '07' or $blnn == '08')
		{
			$objPHPExcel->getActiveSheet(0)->setCellValue('R3', 'Mei');
			$objPHPExcel->getActiveSheet(0)->setCellValue('S3', 'Jun');
			$objPHPExcel->getActiveSheet(0)->setCellValue('T3', 'Jul');
			$objPHPExcel->getActiveSheet(0)->setCellValue('U3', 'Agt');
		}elseif($blnn == '09' or $blnn == '10' or $blnn == '11' or $blnn == '12')
		{
			$objPHPExcel->getActiveSheet(0)->setCellValue('R3', 'Sep');
			$objPHPExcel->getActiveSheet(0)->setCellValue('S3', 'Okt');
			$objPHPExcel->getActiveSheet(0)->setCellValue('T3', 'Nop');
			$objPHPExcel->getActiveSheet(0)->setCellValue('U3', 'Des');
		}
		
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('V2:V3');
		$objPHPExcel->getActiveSheet(0)->setCellValue('V2', 'TOTAL');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('W2:W3');
		$objPHPExcel->getActiveSheet(0)->setCellValue('W2', 'SISA');
		
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('X1:AB1');
		$objPHPExcel->getActiveSheet(0)->setCellValue('X1', 'PROMOTION ONLINE');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('X2:X3');
		$objPHPExcel->getActiveSheet(0)->setCellValue('X2', 'TARGET');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('Y2:AB2');
		$objPHPExcel->getActiveSheet(0)->setCellValue('Y2', 'REALISASI');
		
		$today = date("Y-m-d");
		$pisah = explode("-",$today);
		$blnn = $pisah[1];
		
		if($blnn == '01' or $blnn == '02' or $blnn =='03' or $blnn == '04')
		{
			$objPHPExcel->getActiveSheet(0)->setCellValue('Y3', 'Jan');
			$objPHPExcel->getActiveSheet(0)->setCellValue('Z3', 'Feb');
			$objPHPExcel->getActiveSheet(0)->setCellValue('AA3', 'Mar');
			$objPHPExcel->getActiveSheet(0)->setCellValue('AB3', 'Apr');
		}elseif($blnn == '05' or $blnn == '06' or $blnn == '07' or $blnn == '08')
		{
			$objPHPExcel->getActiveSheet(0)->setCellValue('Y3', 'Mei');
			$objPHPExcel->getActiveSheet(0)->setCellValue('Z3', 'Jun');
			$objPHPExcel->getActiveSheet(0)->setCellValue('AA3', 'Jul');
			$objPHPExcel->getActiveSheet(0)->setCellValue('AB3', 'Agt');
		}elseif($blnn == '09' or $blnn == '10' or $blnn == '11' or $blnn == '12')
		{
			$objPHPExcel->getActiveSheet(0)->setCellValue('Y3', 'Sep');
			$objPHPExcel->getActiveSheet(0)->setCellValue('Z3', 'Okt');
			$objPHPExcel->getActiveSheet(0)->setCellValue('AA3', 'Nop');
			$objPHPExcel->getActiveSheet(0)->setCellValue('AB3', 'Des');
		}
		
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AC1:AH1');
		$objPHPExcel->getActiveSheet(0)->setCellValue('AC1', 'PROMOTION OFFLINE');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AC2:AF2');
		$objPHPExcel->getActiveSheet(0)->setCellValue('AC2', 'REALISASI');
		
		$today = date("Y-m-d");
		$pisah = explode("-",$today);
		$blnn = $pisah[1];
		
		if($blnn == '01' or $blnn == '02' or $blnn =='03' or $blnn == '04')
		{
			$objPHPExcel->getActiveSheet(0)->setCellValue('AC3', 'Jan');
			$objPHPExcel->getActiveSheet(0)->setCellValue('AD3', 'Feb');
			$objPHPExcel->getActiveSheet(0)->setCellValue('AE3', 'Mar');
			$objPHPExcel->getActiveSheet(0)->setCellValue('AF3', 'Apr');
		}elseif($blnn == '05' or $blnn == '06' or $blnn == '07' or $blnn == '08')
		{
			$objPHPExcel->getActiveSheet(0)->setCellValue('AC3', 'Mei');
			$objPHPExcel->getActiveSheet(0)->setCellValue('AD3', 'Jun');
			$objPHPExcel->getActiveSheet(0)->setCellValue('AE3', 'Jul');
			$objPHPExcel->getActiveSheet(0)->setCellValue('AF3', 'Agt');
		}elseif($blnn == '09' or $blnn == '10' or $blnn == '11' or $blnn == '12')
		{
			$objPHPExcel->getActiveSheet(0)->setCellValue('AC3', 'Sep');
			$objPHPExcel->getActiveSheet(0)->setCellValue('AD3', 'Okt');
			$objPHPExcel->getActiveSheet(0)->setCellValue('AE3', 'Nop');
			$objPHPExcel->getActiveSheet(0)->setCellValue('AF3', 'Des');
		}
		
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AG2:AG3');
		$objPHPExcel->getActiveSheet(0)->setCellValue('AG2', 'TOTAL');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AH2:AH3');
		$objPHPExcel->getActiveSheet(0)->setCellValue('AH2', 'SISA');
		
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AI1:AO1');
		$objPHPExcel->getActiveSheet(0)->setCellValue('AI1', 'CALL IN');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AI2:AI3');
		$objPHPExcel->getActiveSheet(0)->setCellValue('AI2', 'TARGET');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AJ2:AM2');
		$objPHPExcel->getActiveSheet(0)->setCellValue('AJ2', 'REALISASI');
		
		$today = date("Y-m-d");
		$pisah = explode("-",$today);
		$blnn = $pisah[1];
		
		if($blnn == '01' or $blnn == '02' or $blnn =='03' or $blnn == '04')
		{
			$objPHPExcel->getActiveSheet(0)->setCellValue('AJ3', 'Jan');
			$objPHPExcel->getActiveSheet(0)->setCellValue('AK3', 'Feb');
			$objPHPExcel->getActiveSheet(0)->setCellValue('AL3', 'Mar');
			$objPHPExcel->getActiveSheet(0)->setCellValue('AM3', 'Apr');
		}elseif($blnn == '05' or $blnn == '06' or $blnn == '07' or $blnn == '08')
		{
			$objPHPExcel->getActiveSheet(0)->setCellValue('AJ3', 'Mei');
			$objPHPExcel->getActiveSheet(0)->setCellValue('AK3', 'Jun');
			$objPHPExcel->getActiveSheet(0)->setCellValue('AL3', 'Jul');
			$objPHPExcel->getActiveSheet(0)->setCellValue('AM3', 'Agt');
		}elseif($blnn == '09' or $blnn == '10' or $blnn == '11' or $blnn == '12')
		{
			$objPHPExcel->getActiveSheet(0)->setCellValue('AJ3', 'Sep');
			$objPHPExcel->getActiveSheet(0)->setCellValue('AK3', 'Okt');
			$objPHPExcel->getActiveSheet(0)->setCellValue('AL3', 'Nop');
			$objPHPExcel->getActiveSheet(0)->setCellValue('AM3', 'Des');
		}
		
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AN2:AN3');
		$objPHPExcel->getActiveSheet(0)->setCellValue('AN2', 'TOTAL');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AO2:AO3');
		$objPHPExcel->getActiveSheet(0)->setCellValue('AO2', 'SISA');	
		
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AP1:AV1');
		$objPHPExcel->getActiveSheet(0)->setCellValue('AP1', 'SHOWING');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AP2:AP3');
		$objPHPExcel->getActiveSheet(0)->setCellValue('AP2', 'TARGET');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AQ2:AT2');
		$objPHPExcel->getActiveSheet(0)->setCellValue('AQ2', 'REALISASI');
		
		$today = date("Y-m-d");
		$pisah = explode("-",$today);
		$blnn = $pisah[1];
		
		if($blnn == '01' or $blnn == '02' or $blnn =='03' or $blnn == '04')
		{
			$objPHPExcel->getActiveSheet(0)->setCellValue('AQ3', 'Jan');
			$objPHPExcel->getActiveSheet(0)->setCellValue('AR3', 'Feb');
			$objPHPExcel->getActiveSheet(0)->setCellValue('AS3', 'Mar');
			$objPHPExcel->getActiveSheet(0)->setCellValue('AT3', 'Apr');
		}elseif($blnn == '05' or $blnn == '06' or $blnn == '07' or $blnn == '08')
		{
			$objPHPExcel->getActiveSheet(0)->setCellValue('AQ3', 'Mei');
			$objPHPExcel->getActiveSheet(0)->setCellValue('AR3', 'Jun');
			$objPHPExcel->getActiveSheet(0)->setCellValue('AS3', 'Jul');
			$objPHPExcel->getActiveSheet(0)->setCellValue('AT3', 'Agt');
		}elseif($blnn == '09' or $blnn == '10' or $blnn == '11' or $blnn == '12')
		{
			$objPHPExcel->getActiveSheet(0)->setCellValue('AQ3', 'Sep');
			$objPHPExcel->getActiveSheet(0)->setCellValue('AR3', 'Okt');
			$objPHPExcel->getActiveSheet(0)->setCellValue('AS3', 'Nop');
			$objPHPExcel->getActiveSheet(0)->setCellValue('AT3', 'Des');
		}
	
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AU2:AU3');
		$objPHPExcel->getActiveSheet(0)->setCellValue('AU2', 'TOTAL');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AV2:AV3');
		$objPHPExcel->getActiveSheet(0)->setCellValue('AV2', 'SISA');
		

	//make a border column
		$batas = $this->lmonitoring->counts() + 3;
		
        $objPHPExcel->getActiveSheet(0)->getStyle('C1:I1')->applyFromArray($style);
		$objPHPExcel->getActiveSheet(0)->getStyle('J1:P1')->applyFromArray($style2);
		$objPHPExcel->getActiveSheet(0)->getStyle('Q1:W1')->applyFromArray($style3);
		$objPHPExcel->getActiveSheet(0)->getStyle('X1:AB1')->applyFromArray($style4);
		$objPHPExcel->getActiveSheet(0)->getStyle('AC1:AH1')->applyFromArray($style5);
		$objPHPExcel->getActiveSheet(0)->getStyle('AI1:AO1')->applyFromArray($style6);
		$objPHPExcel->getActiveSheet(0)->getStyle('AP1:AV1')->applyFromArray($style7);
		$objPHPExcel->getActiveSheet(0)->getStyle('C2:C'.$batas.'')->applyFromArray($style8);
		$objPHPExcel->getActiveSheet(0)->getStyle('J2:J'.$batas.'')->applyFromArray($style8);
		$objPHPExcel->getActiveSheet(0)->getStyle('Q2:Q'.$batas.'')->applyFromArray($style8);
		$objPHPExcel->getActiveSheet(0)->getStyle('X2:X'.$batas.'')->applyFromArray($style8);
		$objPHPExcel->getActiveSheet(0)->getStyle('AI2:AI'.$batas.'')->applyFromArray($style8);
		$objPHPExcel->getActiveSheet(0)->getStyle('AP2:AP'.$batas.'')->applyFromArray($style8);
		$objPHPExcel->getActiveSheet(0)->getStyle('H2:H'.$batas.'')->applyFromArray($style9);
		$objPHPExcel->getActiveSheet(0)->getStyle('O2:O'.$batas.'')->applyFromArray($style9);
		$objPHPExcel->getActiveSheet(0)->getStyle('V2:V'.$batas.'')->applyFromArray($style9);
		$objPHPExcel->getActiveSheet(0)->getStyle('AG2:AG'.$batas.'')->applyFromArray($style9);
		$objPHPExcel->getActiveSheet(0)->getStyle('AN2:AN'.$batas.'')->applyFromArray($style9);
		$objPHPExcel->getActiveSheet(0)->getStyle('AU2:AU'.$batas.'')->applyFromArray($style9);
		$objPHPExcel->getActiveSheet(0)->getStyle('I2:I'.$batas.'')->applyFromArray($style10);
		$objPHPExcel->getActiveSheet(0)->getStyle('P2:P'.$batas.'')->applyFromArray($style10);
		$objPHPExcel->getActiveSheet(0)->getStyle('W2:W'.$batas.'')->applyFromArray($style10);
		$objPHPExcel->getActiveSheet(0)->getStyle('AH2:AH'.$batas.'')->applyFromArray($style10);
		$objPHPExcel->getActiveSheet(0)->getStyle('AO2:AO'.$batas.'')->applyFromArray($style10);
		$objPHPExcel->getActiveSheet(0)->getStyle('AV2:AV'.$batas.'')->applyFromArray($style10);
		$objPHPExcel->getActiveSheet(0)->getStyle('D2:G2')->applyFromArray($style11);
		$objPHPExcel->getActiveSheet(0)->getStyle('D3:G3')->applyFromArray($style11);
		$objPHPExcel->getActiveSheet(0)->getStyle('K2:N2')->applyFromArray($style11);
		$objPHPExcel->getActiveSheet(0)->getStyle('K3:N3')->applyFromArray($style11);
		$objPHPExcel->getActiveSheet(0)->getStyle('R2:U2')->applyFromArray($style11);
		$objPHPExcel->getActiveSheet(0)->getStyle('R3:U3')->applyFromArray($style11);
		$objPHPExcel->getActiveSheet(0)->getStyle('Y2:AB2')->applyFromArray($style11);
		$objPHPExcel->getActiveSheet(0)->getStyle('Y3:AB3')->applyFromArray($style11);
		$objPHPExcel->getActiveSheet(0)->getStyle('AC2:AF2')->applyFromArray($style11);
		$objPHPExcel->getActiveSheet(0)->getStyle('AC3:AF3')->applyFromArray($style11);
		$objPHPExcel->getActiveSheet(0)->getStyle('AJ2:AM2')->applyFromArray($style11);
		$objPHPExcel->getActiveSheet(0)->getStyle('AJ3:AM3')->applyFromArray($style11);
		$objPHPExcel->getActiveSheet(0)->getStyle('AQ2:AT2')->applyFromArray($style11);
		$objPHPExcel->getActiveSheet(0)->getStyle('AQ3:AT3')->applyFromArray($style11);
		$objPHPExcel->getActiveSheet(0)->getStyle('A1:B1')->applyFromArray($style12);
		$objPHPExcel->getActiveSheet(0)->getStyle('A2:B2')->applyFromArray($style12);
		$objPHPExcel->getActiveSheet(0)->getStyle('A3:B3')->applyFromArray($style12);

	/*check point*/

	// Read the existing excel file
	//$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
	//$objReader = PHPExcel_IOFactory::createReader($inputFileType);
	//$objPHPExcel = $objReader->load($inputFileName);
	
	// Update it's data
	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	//$objPHPExcel->setActiveSheetIndex(0);
	
		$database = $this->lmonitoring->_get_dataLmonitoring();
        $shit = 3;
		$no = 0;
        $database = $this->db->query($database)->result();
		
		$today = date("Y-m-d");
		$pisah = explode("-",$today);
		$blnn = $pisah[1];
		
		if($blnn == '01' or $blnn == '02' or $blnn =='03' or $blnn == '04')
		{
			$datee = "".$pisah[0]."-01-01";
		}elseif($blnn == '05' or $blnn == '06' or $blnn == '07' or $blnn == '08')
		{
			$datee = "".$pisah[0]."-05-01";
		}elseif($blnn == '09' or $blnn == '10' or $blnn == '11' or $blnn == '12')
		{
			$datee = "".$pisah[0]."-09-01";
		}
		
        foreach ($database as $val) {
			
		$shit++;
		$no++;
		
		$id_agen = $val->id_agen;
		
		$targetGoal = $this->lmonitoring->getTargetGoal($val->kode_agen,$datee);
		$targetGoal1 = $this->lmonitoring->getTargetGoal1($val->kode_agen,$datee);
		$targetGoal2 = $this->lmonitoring->getTargetGoal2($val->kode_agen,$datee);
		$targetGoal3 = $this->lmonitoring->getTargetGoal3($val->kode_agen,$datee);
		$targetGoal4 = $this->lmonitoring->getTargetGoal4($val->kode_agen,$datee);
		$totalTargetGoal = $targetGoal1 + $targetGoal2 + $targetGoal3 + $targetGoal4;
		$sisaTargetGoal = $targetGoal - $totalTargetGoal;
		
		$targetTransaksi = $this->lmonitoring->getTargetTransaksi($val->kode_agen,$datee);
		$targetTransaksi1 = $this->lmonitoring->getTargetTransaksi1($val->kode_agen,$datee);
		$targetTransaksi2 = $this->lmonitoring->getTargetTransaksi2($val->kode_agen,$datee);
		$targetTransaksi3 = $this->lmonitoring->getTargetTransaksi3($val->kode_agen,$datee);
		$targetTransaksi4 = $this->lmonitoring->getTargetTransaksi4($val->kode_agen,$datee);
		$totalTargetTransaksi = $targetTransaksi1 + $targetTransaksi2 + $targetTransaksi3 + $targetTransaksi4;
		$sisaTargetTransaksi = $targetTransaksi - $totalTargetTransaksi;
		
		$targetListing = $this->lmonitoring->getTargetListing($val->kode_agen,$datee);
		$targetListing1 = $this->lmonitoring->getTargetListing1($val->kode_agen,$datee);
		$targetListing2 = $this->lmonitoring->getTargetListing2($val->kode_agen,$datee);
		$targetListing3 = $this->lmonitoring->getTargetListing3($val->kode_agen,$datee);
		$targetListing4 = $this->lmonitoring->getTargetListing4($val->kode_agen,$datee);
		$totalTargetListing = $targetListing1 + $targetListing2 + $targetListing3 + $targetListing4;
		$sisaTargetListing = $targetListing - $totalTargetListing;
		
		$targetPromoOnline = $this->lmonitoring->getTargetPromoOnline($val->kode_agen,$datee);
		$targetPromoOnline1 = $this->lmonitoring->getTargetPromoOnline1($val->kode_agen,$datee);
		$targetPromoOnline2 = $this->lmonitoring->getTargetPromoOnline2($val->kode_agen,$datee);
		$targetPromoOnline3 = $this->lmonitoring->getTargetPromoOnline3($val->kode_agen,$datee);
		$targetPromoOnline4 = $this->lmonitoring->getTargetPromoOnline4($val->kode_agen,$datee);
		$totalTargetPromoOnline = $targetPromoOnline1 + $targetPromoOnline2 + $targetPromoOnline3 + $targetPromoOnline4;
		$sisaTargetPromoOnline = $targetPromoOnline - $totalTargetPromoOnline;
		
		$targetPromoOffline = $this->lmonitoring->getTargetPromoOffline($val->kode_agen,$datee);
		$targetPromoOffline1 = $this->lmonitoring->getTargetPromoOffline1($val->kode_agen,$datee);
		$targetPromoOffline2 = $this->lmonitoring->getTargetPromoOffline2($val->kode_agen,$datee);
		$targetPromoOffline3 = $this->lmonitoring->getTargetPromoOffline3($val->kode_agen,$datee);
		$targetPromoOffline4 = $this->lmonitoring->getTargetPromoOffline4($val->kode_agen,$datee);
		$totalTargetPromoOffline = $targetPromoOffline1 + $targetPromoOffline2 + $targetPromoOffline3 + $targetPromoOffline4;
		$sisaTargetPromoOffline = $targetPromoOffline - $totalTargetPromoOffline;
		
		$totalTargetPromo = $totalTargetPromoOffline + $totalTargetPromoOnline;
		$targetPromo = $targetPromoOffline + $targetPromoOnline;
		$sisaTargetPromo = $sisaTargetPromoOffline + $sisaTargetPromoOnline;
		
		$targetCallin = $this->lmonitoring->getTargetCallin($val->kode_agen,$datee);
		$targetCallin1 = $this->lmonitoring->getTargetCallin1($val->kode_agen,$datee);
		$targetCallin2 = $this->lmonitoring->getTargetCallin2($val->kode_agen,$datee);
		$targetCallin3 = $this->lmonitoring->getTargetCallin3($val->kode_agen,$datee);
		$targetCallin4 = $this->lmonitoring->getTargetCallin4($val->kode_agen,$datee);
		$totalTargetCallin = $targetCallin1 + $targetCallin2 + $targetCallin3 + $targetCallin4;
		$sisaTargetCallin = $targetCallin - $totalTargetCallin;
		
		$targetShowing = $this->lmonitoring->getTargetShowing($val->kode_agen,$datee);
		$targetShowing1 = $this->lmonitoring->getTargetShow1($val->kode_agen,$datee);
		$targetShowing2 = $this->lmonitoring->getTargetShow2($val->kode_agen,$datee);
		$targetShowing3 = $this->lmonitoring->getTargetShow3($val->kode_agen,$datee);
		$targetShowing4 = $this->lmonitoring->getTargetShow4($val->kode_agen,$datee);
		$totaTargetShowing = $targetShowing1 + $targetShowing2 + $targetShowing3 + $targetShowing4;
		$sisaTargetShowing = $targetShowing - $totaTargetShowing;
		
		$objPHPExcel->getActiveSheet(0)->setCellValue('A' . $shit . '', $no);
		$objPHPExcel->getActiveSheet(0)->setCellValue('B' . $shit . '', $val->nama);
		$objPHPExcel->getActiveSheet(0)->setCellValue('C' . $shit . '', $this->lmonitoring->format_angka($targetGoal));
		$objPHPExcel->getActiveSheet(0)->setCellValue('D' . $shit . '', $this->lmonitoring->format_angka($targetGoal1));
		$objPHPExcel->getActiveSheet(0)->setCellValue('E' . $shit . '', $this->lmonitoring->format_angka($targetGoal2));
		$objPHPExcel->getActiveSheet(0)->setCellValue('F' . $shit . '', $this->lmonitoring->format_angka($targetGoal3));
		$objPHPExcel->getActiveSheet(0)->setCellValue('G' . $shit . '', $this->lmonitoring->format_angka($targetGoal4));
		$objPHPExcel->getActiveSheet(0)->setCellValue('H' . $shit . '', $this->lmonitoring->format_angka($totalTargetGoal));
		$objPHPExcel->getActiveSheet(0)->setCellValue('I' . $shit . '', $this->lmonitoring->format_angka($sisaTargetGoal));
		$objPHPExcel->getActiveSheet(0)->setCellValue('J' . $shit . '', $this->lmonitoring->format_angka($targetTransaksi));
		$objPHPExcel->getActiveSheet(0)->setCellValue('K' . $shit . '', $this->lmonitoring->format_angka($targetTransaksi1));
		$objPHPExcel->getActiveSheet(0)->setCellValue('L' . $shit . '', $this->lmonitoring->format_angka($targetTransaksi2));
		$objPHPExcel->getActiveSheet(0)->setCellValue('M' . $shit . '', $this->lmonitoring->format_angka($targetTransaksi3));
		$objPHPExcel->getActiveSheet(0)->setCellValue('N' . $shit . '', $this->lmonitoring->format_angka($targetTransaksi4));
		$objPHPExcel->getActiveSheet(0)->setCellValue('O' . $shit . '', $this->lmonitoring->format_angka($totalTargetTransaksi));
		$objPHPExcel->getActiveSheet(0)->setCellValue('P' . $shit . '', $this->lmonitoring->format_angka($sisaTargetTransaksi));
		$objPHPExcel->getActiveSheet(0)->setCellValue('Q' . $shit . '', $this->lmonitoring->format_angka($targetListing));
		$objPHPExcel->getActiveSheet(0)->setCellValue('R' . $shit . '', $this->lmonitoring->format_angka($targetListing1));
		$objPHPExcel->getActiveSheet(0)->setCellValue('S' . $shit . '', $this->lmonitoring->format_angka($targetListing2));
		$objPHPExcel->getActiveSheet(0)->setCellValue('T' . $shit . '', $this->lmonitoring->format_angka($targetListing3));
		$objPHPExcel->getActiveSheet(0)->setCellValue('U' . $shit . '', $this->lmonitoring->format_angka($targetListing4));
		$objPHPExcel->getActiveSheet(0)->setCellValue('V' . $shit . '', $this->lmonitoring->format_angka($totalTargetListing));
		$objPHPExcel->getActiveSheet(0)->setCellValue('W' . $shit . '', $this->lmonitoring->format_angka($sisaTargetListing));
		$objPHPExcel->getActiveSheet(0)->setCellValue('X' . $shit . '', $this->lmonitoring->format_angka($targetPromo));
		$objPHPExcel->getActiveSheet(0)->setCellValue('Y' . $shit . '', $this->lmonitoring->format_angka($targetPromoOnline1));
		$objPHPExcel->getActiveSheet(0)->setCellValue('Z' . $shit . '', $this->lmonitoring->format_angka($targetPromoOnline2));
		$objPHPExcel->getActiveSheet(0)->setCellValue('AA' . $shit . '', $this->lmonitoring->format_angka($targetPromoOnline3));
		$objPHPExcel->getActiveSheet(0)->setCellValue('AB' . $shit . '', $this->lmonitoring->format_angka($targetPromoOnline4));
		$objPHPExcel->getActiveSheet(0)->setCellValue('AC' . $shit . '', $this->lmonitoring->format_angka($targetPromoOffline1));
		$objPHPExcel->getActiveSheet(0)->setCellValue('AD' . $shit . '', $this->lmonitoring->format_angka($targetPromoOffline2));
		$objPHPExcel->getActiveSheet(0)->setCellValue('AE' . $shit . '', $this->lmonitoring->format_angka($targetPromoOffline3));
		$objPHPExcel->getActiveSheet(0)->setCellValue('AF' . $shit . '', $this->lmonitoring->format_angka($targetPromoOffline4));
		$objPHPExcel->getActiveSheet(0)->setCellValue('AG' . $shit . '', $this->lmonitoring->format_angka($totalTargetPromo));
		$objPHPExcel->getActiveSheet(0)->setCellValue('AH' . $shit . '', $this->lmonitoring->format_angka($sisaTargetPromo));
		$objPHPExcel->getActiveSheet(0)->setCellValue('AI' . $shit . '', $this->lmonitoring->format_angka($targetCallin));
		$objPHPExcel->getActiveSheet(0)->setCellValue('AJ' . $shit . '', $this->lmonitoring->format_angka($targetCallin1));
		$objPHPExcel->getActiveSheet(0)->setCellValue('AK' . $shit . '', $this->lmonitoring->format_angka($targetCallin2));
		$objPHPExcel->getActiveSheet(0)->setCellValue('AL' . $shit . '', $this->lmonitoring->format_angka($targetCallin3));
		$objPHPExcel->getActiveSheet(0)->setCellValue('AM' . $shit . '', $this->lmonitoring->format_angka($targetCallin4));
		$objPHPExcel->getActiveSheet(0)->setCellValue('AN' . $shit . '', $this->lmonitoring->format_angka($totalTargetCallin));
		$objPHPExcel->getActiveSheet(0)->setCellValue('AO' . $shit . '', $this->lmonitoring->format_angka($sisaTargetCallin));
		$objPHPExcel->getActiveSheet(0)->setCellValue('AP' . $shit . '', $this->lmonitoring->format_angka($targetShowing));
		$objPHPExcel->getActiveSheet(0)->setCellValue('AQ' . $shit . '', $this->lmonitoring->format_angka($targetShowing1));
		$objPHPExcel->getActiveSheet(0)->setCellValue('AR' . $shit . '', $this->lmonitoring->format_angka($targetShowing2));
		$objPHPExcel->getActiveSheet(0)->setCellValue('AS' . $shit . '', $this->lmonitoring->format_angka($targetShowing3));
		$objPHPExcel->getActiveSheet(0)->setCellValue('AT' . $shit . '', $this->lmonitoring->format_angka($targetShowing4));
		$objPHPExcel->getActiveSheet(0)->setCellValue('AU' . $shit . '', $this->lmonitoring->format_angka($totaTargetShowing));
		$objPHPExcel->getActiveSheet(0)->setCellValue('AV' . $shit . '', $this->lmonitoring->format_angka($sisaTargetShowing));
		
		}
		
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('D')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('E')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('F')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('G')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('H')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('I')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('J')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('K')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('L')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('M')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('N')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('O')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('P')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('Q')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('R')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('S')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('T')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('U')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('V')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('W')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('X')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('Y')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('Z')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('AA')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('AB')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('AC')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('AD')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('AE')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('AF')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('AG')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('AH')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('AI')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('AJ')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('AK')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('AL')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('AM')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('AN')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('AO')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('AP')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('AQ')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('AR')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('AS')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('AT')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('AU')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('AV')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('AW')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('AX')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('AY')->setAutoSize(true);
		
	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
	
	// Generate an updated excel file
	// Redirect output to a client’s web browser (Excel2007)
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	//header('Content-Disposition: attachment;filename="' . $inputFileName . '"');
	header('Content-Disposition: attachment;filename="Data-Monitoring.xlsx"');
	header('Cache-Control: max-age=0');

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');

//////finish
    
	}
	
}