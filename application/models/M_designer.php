<?php

class M_designer extends CI_Model  {
    
		
	function __construct()
    {
        parent::__construct();
    }
	function kodeProperty()
	{
			  $carikode = $this->db->query("SELECT max(id_prop) as id_prop from data_property")->row();
			  $datakode =$carikode->id_prop;//$carikode->kode;
			 if ($datakode) {
		  	    $kode = (int) $datakode;
		   		return	$newID = sprintf("%05s", $kode+1);
		   	  } else {
				return "00001";
					 }
	}
	function kodeAgen()
	{
			  $carikode = $this->db->query("SELECT max(id_agen) as id_agen from data_agen")->row();
			  $datakode =$carikode->id_agen;//$carikode->kode;
			 if ($datakode) {
		  	    $kode = (int) $datakode;
		   		return	$newID = sprintf("%02s", $kode+1);
		   	  } else {
				return "01";
					 }
	}
	
	function getKodeList($agen)
	{
	return $agen.$this->kodeProperty();
	}
	function analisisKelengkapan()
	{
	$gambar1=isset($_FILES['gambar1']['type']);
	$gambar2=isset($_FILES['gambar2']['type']);
	$gambar3=isset($_FILES['gambar3']['type']);
	$gambar4=isset($_FILES['gambar4']['type']);
	$gambar5=isset($_FILES['gambar5']['type']);
	$jml=0;
	if($gambar1)
	{
		$jml++;
	}if($gambar2)
	{
		$jml++;
	}if($gambar3)
	{
		$jml++;
	}if($gambar4)
	{
		$jml++;
	}if($gambar5)
	{
		$jml++;
	}
	$total=$jml;	
	if($total==0)
	{
		return 0;
	}else{
		return 1;
	}
	
	}
	
	function uploadFile($id)
	{
		$kode=$this->input->post("kode");
		$form="formUpload".$id;
		$array=array(
		"desain"=>$this->uploadGambar($kode,$form),
		);
		$this->db->where("kode_prop",$kode);
	return	$this->db->update("data_property",$array);
	}
	function uploadGambar($kode,$form)
	{
		if(isset($_FILES[$form]['type']))
		{
		return	 $this->upload_img($kode,$form);
		}return "kosong";
	}
	public function upload_img($kode,$form)
	{	//$this->m_konfig->log("admin","Upload photo");
		///
			$nama=date("YmdHis");
		  $lokasi_file = $_FILES[$form]['tmp_name'];
		  $tipe_file   = $_FILES[$form]['type'];
		  $nama_file   = $_FILES[$form]['name'];
		  //if($tipe_file)
		  //{
		//  $daprof=$this->getGambarkode($kode);
		//	if($daprof!="")
		//	 {
		//		 $path = "file_upload/barang/".$daprof;
		//		 if (file_exists($path)) {
		//			unlink($path);
		//		 }
		//	 }
		  
		  
			$jenis=explode(".",$nama_file);
			$nama_file=$jenis[0];
			// $jenis="jpg";
			$nama=str_replace("/","",$kode.$nama.$nama_file);
			 $target_path = "file_upload/img/".$nama.".jpg";
			 //
	//	  }
		  //
		if (!empty($lokasi_file)) {
		move_uploaded_file($lokasi_file,$target_path);
		//if($jenis=="png"){
		//$this->konversi->UploadImageResize($target_path,$jenis,200);
		}
		return $nama.".jpg";
		}
		function getDataProp()
		{
		return	$this->db->get("data_property")->result();
		}
  
  
		function get_dataProperty()
	{
		 $query = $this->_get_dataProperty();
        if ($_POST['length'] != -1)
            $query .= " limit " . $_POST['start'] . "," . $_POST['length'];
        return $this->db->query($query)->result();
 
	}
	  public function counts() {
        $query = $this->_get_dataProperty();
        return $this->db->query($query)->num_rows();
    }
	function getKodeAgen($kode)
	{
		$this->db->where("id_agen",$kode);
	$data=$this->db->get("data_agen")->row();
	return isset($data->kode_agen)?($data->kode_agen):"";
	}
	function _get_dataProperty()
	{
	$dan="";
	$provinsi=$this->input->get("provinsi");
	if($provinsi){
	$dan.=" AND id_prov=".$provinsi;
	}
	$kabupaten=$this->input->get("kabupaten");
	if($kabupaten){
	$dan.=" AND id_kab=".$kabupaten;
	}
	//$area=$this->input->get("area");
	//if($area){
	//$dan.=" AND area=".$area;
	//}
	$lat_area=$this->input->get("lat_area");
	if($lat_area){
	$dan.=" AND lat_area=".$lat_area;
	}
	$long_area=$this->input->get("long_area");
	if($long_area){
	$dan.=" AND long_area=".$long_area;
	}
	$jenis_pro=$this->input->get("jenis_pro");
	if($jenis_pro){
	$dan.=" AND jenis_prop=".$jenis_pro;
	}$type_pro=$this->input->get("type_pro");
	
	if($type_pro){
	$dan.=" AND type_jual='".$type_pro."'";
	}
	
	$kamar_tidur=$this->input->get("kamar_tidur");
	if($kamar_tidur){
	$dan.=" AND kamar_tidur=".$kamar_tidur;
	}
	$kamar_mandi=$this->input->get("kamar_mandi");
	if($kamar_mandi){
	$dan.=" AND kamar_mandi=".$kamar_mandi;
	}
	$garasi=$this->input->get("garasi");
	if($garasi){
	$dan.=" AND jml_garasi=".$garasi;
	}
	$daya_listrik=$this->input->get("daya_listrik");
	if($daya_listrik){
	$dan.=" AND daya_listrik=".$daya_listrik;
	}
	
	$harga_min=$this->input->get("harga_min");
	if($harga_min){
	$dan.=" AND harga>='".str_replace(".","",$harga_min)."'";
	}
	$harga_max=$this->input->get("harga_max");
	if($harga_max){
	$dan.=" AND harga<='".str_replace(".","",$harga_max)."'";
	}
	
	
	$sertifikat=$this->input->get("sertifikat");
	if($sertifikat){
	$dan.=" AND jenis_sertifikat=".$sertifikat;
	}
	$agen=$this->input->get("agen");
	if($agen){
	$dan.=" AND agen=".$agen;
	}
	$type_sewa=$this->input->get("type_sewa");
	if($type_sewa){
	$dan.=" AND type_sewa=".$type_sewa;
	}
	$kelengkapan=$this->input->get("kelengkapan");
	if($kelengkapan){
		if($kelengkapan=='1')
		{
			$dan.=" AND (gambar1!='' AND gambar2!='' AND gambar3!='' AND gambar4!='' AND gambar5!='')    ";
		}elseif($kelengkapan=='2')
		{
			$dan.=" AND (gambar1 IS NULL AND gambar2 IS NULL AND gambar3 IS NULL AND gambar4 IS NULL AND gambar5 IS NULL ) ";
		}elseif($kelengkapan=='3')
		{
			$dan.=" AND desain!=''  ";
		}elseif($kelengkapan=='4')
		{
			$dan.=" AND (desain IS NULL or desain='')  ";
		}
	
	}
	$status_penjualan=$this->input->get("status_penjualan");
//	if($status_penjualan=='0'){
	$dan.=" AND status='0'";
//	}elseif($status_penjualan=='1'){
//	$dan.=" AND status='1'";
//	}
	 $gambar="(gambar1!='' and gambar2!='' and gambar3!='' and gambar4!='' and gambar5!=''  )";
        $query = "SELECT * FROM data_property WHERE $gambar $dan ";
        if (isset($_POST['search']['value'])) {
            $searchkey = $_POST['search']['value'];
            $query .= " AND (
			kode_prop LIKE '%" . $searchkey . "%' or 
			data_property.desc  LIKE '%" . $searchkey . "%' or
			keterangan LIKE '%" . $searchkey . "%' or
			alamat_detail LIKE '%" . $searchkey . "%' or
			nama_area LIKE '%" . $searchkey . "%' or
			komplek LIKE '%" . $searchkey . "%'  
			) ";
        }

        $column = array('');
        $i = 0;
        foreach ($column as $item) {
            $column[$i] = $item;
        }

      /*  if (isset($_POST['order'])) {
		//	$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            $query .= " order by " . $column[$_POST['order']['0']['column']] . " " . $_POST['order']['0']['dir'];
        } else if (isset($order)) {
            $order = $order;
			//	$this->db->order_by(key($order), $order[key($order)]);
			       $query .= " order by nama ASC";
        }*/
		$query.=" order by created ASC" ;
        return $query;
	}
	function delHistory($id)
	{
		$this->db->where("id",$id);
		return $this->db->delete('report_listing');
	}
	function saveReport()
	{
		$data=array(
		"id_agen"=>$this->session->userdata("id"),
		"kode_listing"=>$this->input->post("kode_listing"),
		"id_title"=>$this->input->post("title"),
		"ket"=>$this->input->post("text"),
		);
		
			return $this->db->insert("report_listing",$data);
	}
  
}