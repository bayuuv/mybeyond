<?php

class Umum extends CI_Model  {
    
		
	function __construct()
    {
        parent::__construct();
		
    }
	/*==============================================================*/
	function upload($idField,$id,$form,$path,$tabel) //nama id file,id, nama form, lokasi_file ex: upload_file/poto note: name form harus sama dengan nama file
	{
		if(isset($_FILES[$form]['type']))
		{
		return	$this->uploadFile($idField,$id,$form,$path,$tabel);
		}else{
			return  $daprof=$this->getFieldFile($idField,$id,$form,$path,$tabel);
		}
	}
	function getFieldFile($idField,$id,$form,$path,$tabel)
	{
	$this->db->where($idField,$id);
	$data=$this->db->get($tabel)->row();
	return isset($data->$form)?($data->$form):"";
	}
	
	 function uploadFile($idField,$id,$form,$path,$tabel)
	{	//$this->m_konfig->log("admin","Upload photo");
		///
		  $nama=date("YmdHis");
		  $lokasi_file = $_FILES[$form]['tmp_name'];
		  $tipe_file   = $_FILES[$form]['type'];
		  $nama_file   = $_FILES[$form]['name'];
		  
		  
		  $daprof=$this->getFieldFile($idField,$id,$form,$path,$tabel);
			if($daprof)
			 {
				 $path = $path."/".$daprof;
				 if (file_exists($path)) {
					unlink($path);
				 }
			 }
	
			$nama_file=str_replace(" ","_",$nama_file);
		
			// $jenis="jpg";
			$nama=str_replace("/","",$nama.$nama_file);
			 $target_path = $path."/".$nama;

		if (!empty($lokasi_file)) {
		move_uploaded_file($lokasi_file,$target_path);
		return $nama;
		}
	}
		
	/*==============================================================*/
}