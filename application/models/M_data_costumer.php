<?php

class M_data_costumer extends CI_Model  {
    
		
	function __construct()
    {
        parent::__construct();
		
    }
		function get_dataProperty($id)
	{
		 $query = $this->_get_dataProperty($id);
        if ($_POST['length'] != -1)
            $query .= " limit " . $_POST['start'] . "," . $_POST['length'];
        return $this->db->query($query)->result();
 
	}
	  public function countsList($id) {
        $query = $this->_get_dataProperty($id);
        return $this->db->query($query)->num_rows();
    }
	function _get_dataProperty($id)
	{	
        $query = "SELECT * FROM data_property WHERE id_pelanggan=$id ";
        if (isset($_POST['search']['value'])) {
            $searchkey = $_POST['search']['value'];
            $query .= " AND (
			kode_prop LIKE '%" . $searchkey . "%' or 
			data_property.desc  LIKE '%" . $searchkey . "%' or
			keterangan LIKE '%" . $searchkey . "%' or
			ket_detail LIKE '%" . $searchkey . "%' or
			nama_area LIKE '%" . $searchkey . "%' or
			komplek LIKE '%" . $searchkey . "%'  
			) ";
        }

        $column = array('');
        $i = 0;
        foreach ($column as $item) {
            $column[$i] = $item;
        }

      /*  if (isset($_POST['order'])) {
		//	$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            $query .= " order by " . $column[$_POST['order']['0']['column']] . " " . $_POST['order']['0']['dir'];
        } else if (isset($order)) {
            $order = $order;
			//	$this->db->order_by(key($order), $order[key($order)]);
			       $query .= " order by nama ASC";
        }*/
		$query.=" order by created ASC" ;
        return $query;
	}
  
	
	
	
	
	
	
	
	
	
	
	function get_dataAgen()
	{
		 $query = $this->_get_dataOwner();
        if ($_POST['length'] != -1)
            $query .= " limit " . $_POST['start'] . "," . $_POST['length'];
        return $this->db->query($query)->result();
 
	}
	  public function counts() {
        $query = $this->_get_dataOwner();
        return $this->db->query($query)->num_rows();
    }
	function _get_dataOwner()
	{
        $query = "SELECT * FROM data_pelanggan WHERE kode_agen='".$this->session->userdata('kode')."' ";
        if (isset($_POST['search']['value'])) {
            $searchkey = $_POST['search']['value'];
            $query .= " AND (
			nama LIKE '%" . $searchkey . "%' or 
			
			email LIKE '%" . $searchkey . "%' or
			hp LIKE '%" . $searchkey . "%' 
			) ";
        }

        $column = array('', '', 'nama', 'hp', 'email', 'ket');
        $i = 0;
        foreach ($column as $item) {
            $column[$i] = $item;
        }

      /*  if (isset($_POST['order'])) {
		//	$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            $query .= " order by " . $column[$_POST['order']['0']['column']] . " " . $_POST['order']['0']['dir'];
        } else if (isset($order)) {
            $order = $order;
			//	$this->db->order_by(key($order), $order[key($order)]);
			       $query .= " order by nama ASC";
        }*/
		$query.=" order by nama ASC" ;
        return $query;
	}
	
	
	function cekOwner($hp)
	{
			$this->db->where("hp",$hp);
	return	$this->db->get("data_pelanggan")->num_rows();
	}
	function insert()
	{
	$data=array(
	"nama"=>$this->input->post("nama"),
	"kode_agen"=>$this->session->userdata("kode"),
	"hp"=>$hp=$this->input->post("hp"),
	"jk"=>$this->input->post("jk"),
	"email"=>$this->input->post("email"),
	"ket"=>$this->input->post("ket"),
	);
	$cek=$this->cekOwner($hp);
	if(!$cek){
		return $this->db->insert("data_pelanggan",$data);
	}else
	{
		return false;
	}

	}
	
	function update()
	{
	$data=array(
	//"kode_agen"=>$this->getKode(),
	"nama"=>$this->input->post("nama"),
	"hp"=>$hp=$this->input->post("hp"),
	"email"=>$this->input->post("email"),
	"jk"=>$this->input->post("jk"),
	"ket"=>$this->input->post("ket"),
	);
	//$cek=$this->cekOwnerUpdate($hp);
	if(1==1){
		$this->db->where("id_pelanggan",$this->input->post("id_pelanggan"));
		return $this->db->update("data_pelanggan",$data);
	}else
	{
		return false;
	}

	}
	
	function HapusAll()
	{
		$hapus=$this->input->post("hapus");
		foreach($hapus as $id)
		{
		$this->db->where("id_pelanggan",$id);
		$this->db->delete("data_pelanggan");
		}	return true;
	}
	function hapus($id)
	{
		$this->db->where("id_pelanggan",$id);
		return	$this->db->delete("data_pelanggan");
	}
	function export()
	{
//////start
        $objPHPExcel = new PHPExcel();
//style
        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'rotation' => 0,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '6CCECB')
            ),
            'borders' =>
            array('allborders' =>
                array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'),
                ),
            ),
        );
     //   $objPHPExcel->getActiveSheet(0)->getColumnDimension('F')->setWidth(25);
     //   $objPHPExcel->getActiveSheet(0)->getColumnDimension('G')->setWidth(25);
     //   $objPHPExcel->getActiveSheet(0)->getColumnDimension('H')->setWidth(35);




//create column

        $objPHPExcel->getActiveSheet(0)->setCellValue('A1', 'NAMA');
        $objPHPExcel->getActiveSheet(0)->setCellValue('B1', 'NOMOR HP');
        $objPHPExcel->getActiveSheet(0)->setCellValue('C1', 'EMAIL');
        $objPHPExcel->getActiveSheet(0)->setCellValue('D1', 'ket');

//make a border column
        $objPHPExcel->getActiveSheet(0)->getStyle('A1:D1')->applyFromArray($style);

        $database = $this->_get_dataOwner();
        $shit = 1;
        $database = $this->db->query($database)->result();
        foreach ($database as $list) {
            $shit++;
//create data per row
          $objPHPExcel->getActiveSheet(0)->setCellValue('A' . $shit . '', $list->nama);
          $objPHPExcel->getActiveSheet(0)->setCellValue('B' . $shit . '', '`'.$list->hp);
            $objPHPExcel->getActiveSheet(0)->setCellValue('C' . $shit . '', $list->email);
            $objPHPExcel->getActiveSheet(0)->setCellValue('D' . $shit . '', $list->ket);

        }

//auto size column
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('D')->setAutoSize(true);



// Rename worksheet (worksheet, not filename)
        $objPHPExcel->getActiveSheet(0)->setTitle('Data Owner');
        
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Data-Costumer.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
//////finish
    
	}
	
	function downloadFormat()
	{
	//////start
		$objPHPExcel = new PHPExcel();
		//style
		$style = array( 
		 'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				  'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				  'rotation'   => 0,
		  ),
		   'fill' => array(
				  'type' => PHPExcel_Style_Fill::FILL_SOLID,
				  'color' => array('rgb' => 'ccccff')
			  ),
		 'borders' => 
		  array( 'allborders' => 
			array( 'style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'), 
			  ), 
			), 
		);	
		
		$head = array( 
			'font'  => array(
			'bold'  => true,
			'color' => array('rgb' => 'FFFFFF'),
			),
			
		   'fill' => array(
				  'type' => PHPExcel_Style_Fill::FILL_SOLID,
				  'color' => array('rgb' => '009966')
			  ),
		 'borders' => 
		  array( 'allborders' => 
			array( 'style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'), 
			  ), 
			), 
		);
		
		
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('A')->setWidth(25);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('B')->setWidth(25);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('C')->setWidth(25);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('D')->setWidth(25);

		
		
		//create column
		
		$objPHPExcel->getActiveSheet(0)->setCellValue('A1', 'Nama');
		$objPHPExcel->getActiveSheet(0)->setCellValue('B1', 'Nomor Hp');
		$objPHPExcel->getActiveSheet(0)->setCellValue('C1', 'Email');
		$objPHPExcel->getActiveSheet(0)->setCellValue('D1', 'Ket');
		
		//make a border column
		$objPHPExcel->getActiveSheet(0)->getStyle('A1:D1')->applyFromArray($style);
		
		
		
		// Rename worksheet (worksheet, not filename)
		$objPHPExcel->getActiveSheet(0)->setTitle('Data Agen');
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		
		header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Form Costumer.xlsx"');
		header('Cache-Control: max-age=0');
		 
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		//////finish
		
	}
	
	function importData2()
	{
	$sukses=0;$gagal=0;$edit=0;
		 $file   = explode('.',$_FILES['userfile']['name']);
		$length = count($file);
		if($file[$length -1] == 'xlsx' || $file[$length -1] == 'xls'){//jagain barangkali uploadnya selain file excel <span class="wp-smiley wp-emoji wp-emoji-smile" title=":-)">:-)</span>
        $tmp    = $_FILES['userfile']['tmp_name'];//Baca dari tmp folder jadi file ga perlu jadi sampah di server :-p
		      
				 // load excel
			    $file = $_FILES['userfile']['tmp_name'];
			    $load = PHPExcel_IOFactory::load($file);
                $sheets = $load->getActiveSheet()->toArray(null,true,true,true);
				$i=1;
				foreach ($sheets as $sheet) {
				if ($i > 1) {						
						$hp=str_replace("'","",$sheet[1]);
						$hp=str_replace("`","",$hp);
						$hp=str_replace("+62","0",$hp);
									
							//get form
															
						$sql=array(
						"nama"=>$sheet[0],
						"hp"=>$hp=$this->db->escape_str($hp),
						"email"=>$sheet[2],
						"ket"=>$sheet[3],
						);		
				       
						$cek=$this->cekOwner($hp);
					if($cek==0)//jika data kontak tidak ada maka tambahkan
					{  
							$this->db->insert("data_pelanggan",$sql); $sukses++;
					}else{ //jika data kontak ada maka edit yg ada
						$sql=array(
						"nama"=>$sheet[0],
						"hp"=>$hp=$this->db->escape_str($hp),
						"email"=>$sheet[2],
						"ket"=>$sheet[3],
						);						
							$this->db->where("hp",$hp);
							$this->db->update("data_pelanggan",$sql);
							$edit++;
					};
					   
				}
				$i++;
                }
               
		}else{
        exit('<br><b style="color:red">Upload Gagal! Gunakan Format Ms.Excell yang telah di sediakan</b>');   //pesan error tipe file tidak tepat
		}

		return $sukses."-".$edit."-".$gagal;
	}
	function saveReport()
	{
		$data=array(
		"id_agen"=>$this->session->userdata("id"),
		"id_costumer"=>$this->input->post("id"),
		"id_title"=>$this->input->post("title"),
		"ket"=>$this->input->post("text"),
		);
		
			return $this->db->insert("report_costumer",$data);
	}
	function delHistory($id)
	{
		$this->db->where("id",$id);
		return $this->db->delete('report_costumer');
	}
}