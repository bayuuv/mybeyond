<?php

class Reff extends CI_Model  {
    
		
	function __construct()
    {
        parent::__construct();
		
    }
	function updateCountNetwork()
	{
		$count=$this->db->query("SELECT id from count_network")->row();
		$count=$count->id;
	return	$this->db->query("UPDATE count_network set id='".($count+1)."' ");

	}
	function kodeNetwork()
	{
		  $carikode = $this->db->query("SELECT id as id_agen from count_network")->row();
			  $datakode =$carikode->id_agen;//$carikode->kode;
			 if ($datakode) {
		  	    $kode = (int) $datakode;
		   		$return=$newID = sprintf("%02s", $kode+1);
		   	  } else {
				$return="01";
					 }
		$bln=date('m');
		if($bln=="1"){
			$romawi="I";
		}if($bln=="2"){
			$romawi="II";
		}if($bln=="3"){
			$romawi="III";
		}if($bln=="4"){
			$romawi="IV";
		}if($bln=="5"){
			$romawi="V";
		}if($bln=="6"){
			$romawi="VI";
		}if($bln=="7"){
			$romawi="VII";
		}if($bln=="8"){
			$romawi="VIII";
		}if($bln=="9"){
			$romawi="IX";
		}if($bln=="10"){
			$romawi="X";
		}if($bln=="11"){
			$romawi="XI";
		}if($bln=="12"){
			$romawi="XII";
		}
		return "BNet/".$return."/".$romawi."/".date('Y');			 
	}
	function cancelBy($id)
	{
		if($id=="1")
		{
			return "By Owner";
		}
		if($id=="2"){
			return "By Other";
		}
		return "";
	}
	function getTypePro()
	{
	$data=$this->db->get("tr_type_property")->result();
	return $data;
	}
	
	function getFurniture()
	{
	return $this->db->get("tr_furniture")->result();
	}
	function getJabatan($id=array())
	{	
	if($id){ $this->db->where_not_in("id",$id);}
	return $this->db->get("tr_jabatan")->result();
	}
	function getJabatanAgen()
	{
	    $jb = array("1","11","12");
		$this->db->order_by("nama","ASC");
		$this->db->where_in("id",$jb);
	$data=$this->db->get("tr_jabatan")->result();
	return $data;
	}
	function getAir()
	{
	return $this->db->get("tr_air")->result();
	}
	
	function getKodePropById($id)
	{
		$this->db->where("id_prop",$id);
		$data=$this->db->get("data_property")->row();
	return isset($data->kode_prop)?($data->kode_prop):"";
	}
	function getAlamatListingByKode($id)
	{
		$this->db->where("kode_prop",$id);
		$data=$this->db->get("data_property")->row();
	return isset($data->alamat_detail)?($data->alamat_detail):"";
	}
	function getDataProperty($id)
	{
		$this->db->where("id_prop",$id);
	return	$data=$this->db->get("data_property")->row();
	}
	function getNamaJabatan($id)
	{
		$this->db->where("id",$id);
		$data=$this->db->get("tr_jabatan")->row();
	return isset($data->nama)?($data->nama):"";
	}
	function getNamaJenisSertifikat($id)
	{
		$this->db->where("id_sertifikat",$id);
		$data=$this->db->get("tr_sertifikat")->row();
	return isset($data->nama)?($data->nama):"";
	}
	function getNamaAir($id)
	{
		$this->db->where("id",$id);
		$data=$this->db->get("tr_air")->row();
	return isset($data->nama)?($data->nama):"";
	}
	function getNamaFurniture($id)
	{
		$this->db->where("id",$id);
		$data=$this->db->get("tr_furniture")->row();
	return isset($data->nama)?($data->nama):"";
	}
	
	
	function getNamaTypeSewa($id)
	{
		$this->db->where("id_sewa",$id);
		$data=$this->db->get("tr_sewa")->row();
	return isset($data->nama)?($data->nama):"";
	}function getNamaProv($id)
	{
		$this->db->where("id",$id);
		$data=$this->db->get("tr_provinsi")->row();
	return isset($data->provinsi)?($data->provinsi):"";
	}function getNamaHadap($id)
	{
		$this->db->where("id_hadap",$id);
		$data=$this->db->get("tr_hadap")->row();
	return isset($data->nama)?($data->nama):"";
	}
	function getlistingArea($area=null,$id)
	{
		$this->db->where("area_listing",$area);
		$this->db->where("id_prop!=",$id);
	return	$this->db->get("data_property")->result();
	}
	
	
	function getNamaKab($id)
	{
		$this->db->where("id",$id);
		$data=$this->db->get("tr_kabupaten")->row();
	return isset($data->kabupaten)?($data->kabupaten):"";
	}
	function getNamaJenis($id)
	{
		$this->db->where("id_type",$id);
	$data=$this->db->get("tr_type_property")->row();
	return isset($data->nama)?($data->nama):"---";
	}
	function getReportTitle($id)
	{
		$this->db->where("id",$id);
	$data=$this->db->get("tr_report_listing")->row();
	return isset($data->nama)?($data->nama):"---";
	}
	function getReportProp($id)
	{
		$this->db->where("kode_listing",$id);
		$this->db->order_by("tgl","ASC");
	return	$this->db->get("report_listing")->result();
	}
	function getNamaOwner($id)
	{
		$this->db->where("id_owner",$id);
	$data=$this->db->get("data_owner")->row();
	return isset($data->nama)?($data->nama):"---";
	}
	function getNamaOwnerByListing($id)
	{
		$id_owner=$this->db->query("SELECT id_owner from data_property where kode_prop='".$id."'")->row();
		return $this->getNamaOwner(isset($id_owner->id_owner)?($id_owner->id_owner):"");
	}
	function getJKOwnerByListing($id)
	{
		$id_owner=$this->db->query("SELECT id_owner from data_property where kode_prop='".$id."'")->row();
		$this->db->where("id_owner",$id_owner->id_owner);
	$data=$this->db->get("data_owner")->row();
	 if($data->jk == 'l'){
		return "Mr.";
	 }elseif($data->jk == 'p'){
		return "Mrs.";
	 }else{
		return "";
	 }
	}
	function getAlamatOwnerByListing($id)
	{
		$data=$this->db->query("SELECT a.alamat FROM `data_owner` as a LEFT JOIN `data_property` as b ON a.id_owner = b.id_owner WHERE b.kode_prop = '".$id."'")->row();
		//return $this->getNamaOwner(isset($id_owner->id_owner)?($id_owner->id_owner):"");
		//$data=$this->db->get("data_agen")->row();
	    return isset($data->alamat)?($data->alamat):"---";
	}
	 function getIdOwnerByListing($id)
	{
		$id_owner=$this->db->query("SELECT id_owner from data_property where kode_prop='".$id."'")->row();
		return  isset($id_owner->id_owner)?($id_owner->id_owner):"";
	}
	 
	function totalKomisiAgen($id,$today)
	{
	//$return=$this->db->query("SELECT SUM(nominal_komisi_listing_net) as tk from data_selling WHERE year(tgl_closing) = $today AND (kode_agen='".$id ."' OR selling='".$id."')")->row();
	$return=$this->db->query("SELECT SUM(total_komisi) as tk from data_selling WHERE year(tgl_closing) = $today AND (kode_agen='".$id ."' OR selling='".$id."')")->row();
	//$return=$this->db->query("SELECT SUM(total_komisi) as tk from data_selling WHERE sumber_selling = sumber_listing AND year(tgl_closing) = $today AND (kode_agen='".$id ."' OR selling='".$id."')")->row();
	return isset($return->tk)?($return->tk):"0";
	}
	
	function totalKomisiAgen2($id,$today)
	{
	$return=$this->db->query("SELECT SUM(nominal_komisi_listing) as tk from data_selling WHERE sumber_selling <> sumber_listing AND year(tgl_closing) = $today AND (kode_agen='".$id ."' OR selling='".$id."')")->row();
	return isset($return->tk)?($return->tk):"0";
	}
	
	function getDataOwnerById($id)
	{
		$this->db->where("id_owner",$id);
	return $data=$this->db->get("data_owner")->row();
	
	}
	function jmlReportById($id)
	{
		$this->db->where("id_costumer",$id);
	return $this->db->get("report_costumer")->num_rows();	 
	}
	function getNamaAgen($id)
	{
		if($id=="admin"){ return "Admin";	}
		$this->db->where("kode_agen",$id);
	$data=$this->db->get("data_agen")->row();
	return isset($data->nama)?($data->nama):"---";
	}function getNamaAgenId($id)
	{
		$this->db->where("id_agen",$id);
	$data=$this->db->get("data_agen")->row();
	return isset($data->nama)?($data->nama):"---";
	}
	 
	function getPotoAgen($id)
	{	
		$this->db->where("kode_agen",$id);
	$data=$this->db->get("data_agen")->row();
	return isset($data->poto)?($data->poto):"nopund.jpg";
	} 
	function getPotoKtp($id)
	{	
		$this->db->where("kode_agen",$id);
	$data=$this->db->get("data_agen")->row();
	return isset($data->ktp)?($data->ktp):"nopund.jpg";
	} function getPotoKK($id)
	{	
		$this->db->where("kode_agen",$id);
	$data=$this->db->get("data_agen")->row();
	return isset($data->kk)?($data->kk):"nopund.jpg";
	}  function getPotoNPWP($id)
	{	
		$this->db->where("kode_agen",$id);
	$data=$this->db->get("data_agen")->row();
	return isset($data->npwp)?($data->npwp):"nopund.jpg";
	} 
	function photoAdmin($id)
	{
			$this->db->where("id_admin",$id);
	$data=$this->db->get("admin")->row();
	return isset($data->poto)?($data->poto):"";
		
	}
	function getNamaAgenById($id)
	{
		$this->db->where("id_agen",$id);
	$data=$this->db->get("data_agen")->row();
	return isset($data->nama)?($data->nama):"---";
	}
	function getNamaAgenByKode($id)
	{
		$this->db->where("kode_agen",$id);
	$data=$this->db->get("data_agen")->row();
	return isset($data->nama)?($data->nama):"---";
	}
	function getUsernameAgenByKode($id)
	{
		$this->db->where("kode_agen",$id);
	$data=$this->db->get("data_agen")->row();
	return isset($data->username)?($data->username):"---";
	}
	function tgl_jatuh_tempo($kode)
	{
		$this->db->where("kode_prop",$kode);
	$data=$this->db->get("data_property")->row();
	return isset($data->tgl_jatuh_tempo)?($data->tgl_jatuh_tempo):"";
	}
	function cekJumlahGambar($id)
	{
		$this->db->where("id_prop",$id);
	$data=$this->db->get("data_property")->row();
	$gambar1=isset($data->gambar1)?($data->gambar1):"";
	$gambar2=isset($data->gambar2)?($data->gambar2):"";
	$gambar3=isset($data->gambar3)?($data->gambar3):"";
	$gambar4=isset($data->gambar4)?($data->gambar4):"";
	$gambar5=isset($data->gambar5)?($data->gambar5):"";
	$desain=isset($data->desain)?($data->desain):"";
	$jml=0;
	if($gambar1)
	{
		$jml++;
	}if($gambar2)
	{
		$jml++;
	}if($gambar3)
	{
		$jml++;
	}if($gambar4)
	{
		$jml++;
	}if($gambar5)
	{
		$jml++;
	}
	if($jml==0)
	{
		return "<font color='red'>Kosong</font>";
	}
	return $jml;	
	}
	function cekGambarDesain($id)
	{
		$this->db->where("id_prop",$id);
	$data=$this->db->get("data_property")->row();
	$desain=isset($data->desain)?($data->desain):"";
	if($desain)
	{
		return "Ada";
	}else
	{
		return "<font color='red'>Tidak Ada</font>";
	}
	
	}
	function cekGambarDesain2($id)
	{
		$this->db->where("id_prop",$id);
	$data=$this->db->get("data_property")->row();
	return isset($data->desain)?($data->desain):"";
	}
	function getKelengkapan($id)
	{
		$this->db->where("id_kelengkapan",$id);
	$data=$this->db->get("tr_kelengkapan")->row();
	return isset($data->nama)?($data->nama):"---";
	}
	function getNamaPelanggan($id)
	{
		$this->db->where("id_pelanggan",$id);
	$data=$this->db->get("data_pelanggan")->row();
	return isset($data->nama)?($data->nama):"";
	}
	function getNamaTitleListing($id)
	{
		$this->db->where("id",$id);
	$data=$this->db->get("tr_report_listing")->row();
	return isset($data->nama)?($data->nama):"---";
	}
	function getWeb()
	{
	return $this->db->get("web_kontak")->row();
	}
	function getNamaTitleCostumer($id)
	{
		$this->db->where("id",$id);
	$data=$this->db->get("tr_report_costumer")->row();
	return isset($data->nama)?($data->nama):"---";
	}
	function getTitleCostumer()
	{
	$data=$this->db->get("tr_report_costumer")->result();
	return $data;
	}
	function getTitleListing()
	{
	$data=$this->db->get("tr_report_listing")->result();
	return $data;
	}
	function getHpPelanggan($id)
	{
		$this->db->where("id_pelanggan",$id);
	$data=$this->db->get("data_pelanggan")->row();
	return isset($data->hp)?($data->hp):"---";
	}
	function getEmailPelanggan($id)
	{
		$this->db->where("id_pelanggan",$id);
	$data=$this->db->get("data_pelanggan")->row();
	return isset($data->email)?($data->email):"---";
	}
	function jumlahLeadNetwork()
	{
	return $this->db->query("SELECT * FROM data_pelanggan WHERE id_network='".$this->session->userdata('id')."' ")->num_rows();
	}
	function getFasilitas()
	{
	$data=$this->db->get("tr_fasilitas")->result();
	return $data;
	}
	function getMemberNetwork($id)
	{
		$this->db->where("id_network",$id);
	$data=$this->db->get("data_property")->row();
	$kode_agen=isset($data->agen)?($data->agen):"";
	return $this->reff->getNamaAgenByKode($kode_agen);
	}
	function jumlahListingOwner($id)
	{
		$this->db->where("id_owner",$id);
	return $data=$this->db->get("data_property")->num_rows();
	}
	function getAgenPp()
	{
		$jb = array("1","11","12");
		$this->db->select('a.*,b.nama AS jabatan');
		$this->db->from('data_agen a');
		$this->db->order_by("nama","ASC");
		$this->db->where("a.aktifasi","1");
		$this->db->where_in("a.jabatan",$jb);
		$this->db->join('tr_jabatan b', 'b.id = a.jabatan', 'left');
	$data=$this->db->get()->result();
	return $data;
	}
	
	function getAgen()
	{
	    $jb = array("1","11","12");
		$this->db->order_by("nama","ASC");
		$this->db->where_in("jabatan",$jb);
	$data=$this->db->get("data_agen")->result();
	return $data;
	}
	
	function getAgenAjeng()
	{
		$codex = array("BREA/005/II/2017", "BREA/012/VIII/2017");
		$this->db->order_by("nama","ASC");
		$this->db->where_in("kode_agen",$codex);
	$data=$this->db->get("data_agen")->result();
	return $data;
	}
	
	function getAgenRhafa()
	{
	    //$codex = array("BREA/058/VI/2018", "BREA/009/III/2017", "BREA/046/II/2018", "BREA/047/II/2018");
		$this->db->order_by("nama","ASC");
		//$this->db->where_in("kode_agen",$codex);
		$this->db->where("kode_agen","BREA/039/I/2018");
	$data=$this->db->get("data_agen")->result();
	return $data;
	}
	
	function getAgenKiki()
	{
		$codex = array("BREA/046/II/2018");
		$this->db->order_by("nama","ASC");
		//$this->db->where("kode_agen","BREA/001/I/2017");
		$this->db->where_in("kode_agen",$codex);
		$data=$this->db->get("data_agen")->result();
	return $data;
	}
	
	function getAgenYudi()
	{
	    $codex = array("BREA/032/XI/2017");
		$this->db->order_by("nama","ASC");
		$this->db->where_in("kode_agen",$codex);
	$data=$this->db->get("data_agen")->result();
	return $data;
	}
	
	function getAgenVivi()
	{
	    $codex = array("BREA/046/II/2018", "BREA/032/XI/2017", "BREA/001/I/2017", "BREA/005/II/2017", "BREA/012/VIII/2017", "BREA/066/VII/2018","BREA/010/I/2017");
		$jab = array("1","11", "12");
		$this->db->where_in("jabatan",$jab);
		$this->db->where("aktifasi","1");
		$this->db->where_not_in("kode_agen",$codex);
		$this->db->order_by("nama","ASC");
	$data=$this->db->get("data_agen")->result();
	return $data;
	}
	
	function getAgenYema()
	{
		$codex = array("BREA/001/I/2017");
		$this->db->order_by("nama","ASC");
		//$this->db->where("kode_agen","BREA/001/I/2017");
		$this->db->where_in("kode_agen",$codex);
		$data=$this->db->get("data_agen")->result();
	return $data;
	}
	
	function getAgenFrans()
	{
		$codex = array("BREA/046/II/2018","BREA/010/I/2017");
		$this->db->order_by("nama","ASC");
		//$this->db->where("kode_agen","BREA/001/I/2017");
		$this->db->where_in("kode_agen",$codex);
		$data=$this->db->get("data_agen")->result();
	return $data;
	}
	
	function getAgenRena()
	{
		$codex = array("BREA/005/II/2017", "BREA/012/VIII/2017", "BREA/066/VII/2018");
		$this->db->order_by("nama","ASC");
		$this->db->where_in("kode_agen",$codex);
		$data=$this->db->get("data_agen")->result();
	return $data;
	}
	
	function getAgenRow($kode)
	{
		$jb = array("1","11","12");
		$this->db->where_in("jabatan",$jb);
		$this->db->where("kode_agen",$kode);
	$data=$this->db->get("data_agen")->row();
	return $data;
	}
	function getAgenRowById($kode)
	{
		$jb = array("1","11","12");
		/*$this->db->where_in("jabatan",$jb);
		$this->db->where("id_agen",$kode);
	$data=$this->db->get("data_agen")->row();*/
	$this->db->select('a.*,b.nama AS jabatan');
	$this->db->from('data_agen a');
	$this->db->where_in("a.jabatan",$jb);
	$this->db->where("id_agen",$kode);
	$this->db->where("aktifasi",1);
	$this->db->join('tr_jabatan b', 'b.id = a.jabatan', 'left');
	$data=$this->db->get()->row();
	return $data;
	}
	function getSewa()
	{
	$data=$this->db->get("tr_sewa")->result();
	return $data;
	}
	function getOwner()
	{
	$data=$this->db->get("data_owner")->result();
	return $data;
	}
	function getOwner2()
	{
	$this->db->order_by("nama","ASC");
	$data=$this->db->get("data_owner")->result();
	return $data;
	}
	
	function getHadap()
	{
	$data=$this->db->get("tr_hadap")->result();
	return $data;
	}	function getSertifikat()
	{
	$data=$this->db->get("tr_sertifikat")->result();
	return $data;
	}
	
	function getProvinsi()
	{
		$this->db->order_by("provinsi","asc");
	$data=$this->db->get("tr_provinsi")->result();
	return $data;
	}
	function getKab($id)
	{
		$this->db->order_by("kabupaten","asc");
		$this->db->where("provinsi_id",$id);
	$data=$this->db->get("tr_kabupaten")->result();
	return $data;
	}
	function getList($id)
	{
		$this->db->where("kode_agen",$id);
		$return =$this->db->get("data_agen")->row();
		return $return->jabatan;
	}
	function getKomplek($id)
	{
		$this->db->order_by("komplek","asc");
		$this->db->where("id_kab",$id);
		$this->db->distinct("komplek");
	$data=$this->db->get("data_property")->result();
	return $data;
	}
	function jumlahListing($kode)
	{		$this->db->where("agen",$kode);
	$data=$this->db->get("data_property")->num_rows();
	return $data;
	}
	function jumlahSelling($kode)
	{
	$this->db->where("kode_agen",$kode);
	$data=$this->db->get("data_selling")->num_rows();
	return $data;
	}
		function jmlPelanggan($id)
	{
		$this->db->where("kode_agen",$id);
	return	$this->db->get("data_pelanggan")->num_rows();
	}
	function getTanggalJatuhTempo($id)
	{
		$this->db->where("kode_prop",$id);
	$return=$this->db->get("data_property")->row();
	return $tgl=$this->tanggal->ind(isset($return->tgl_jatuh_tempo)?($return->tgl_jatuh_tempo):"","/");
	}
  	function getCostumer($id)
	{
	//	$this->db->where("kode_agen",$id);
	return	$this->db->get("data_pelanggan")->result();
	}
	function getBuyer()
	{
		$this->db->order_by("nama","ASC");
	$data=$this->db->get("data_pelanggan")->result();
	return $data;
	}
	
  

	 function UploadImageResize($dir,$jenis,$width){
		 if($jenis=="jpg" or $jenis=="jpeg")
		 {
		   //direktori gambar
		   $vdir_upload = $dir;
		   $vfile_upload = $vdir_upload;

		   //identitas file asli
		   $im_src = imagecreatefromjpeg($vfile_upload);
		   $src_width = imageSX($im_src);
		   $src_height = imageSY($im_src);

		   //Set ukuran gambar hasil perubahan
		   $dst_width = $width;
		   $dst_height = ($dst_width/$src_width)*$src_height;

		   //proses perubahan ukuran
		   $im = imagecreatetruecolor($dst_width,$dst_height);
		   imagecopyresampled($im, $im_src, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);
		   imagejpeg($im,$vdir_upload,$width);
		   //remove chaceh
		   imagedestroy($im_src);
		   imagedestroy($im);
		 }
	 }
	 
	
	function watermark_image($image_old){
		$stamp_path = base_url()."file_upload/image/water.png";
		$image_path = base_url()."file_upload/img/".$image_old."";
		
		$file = pathinfo($image_path);
    
		// Declare valid formats
		$valid_formats = array("jpg", "jpeg", "gif", "png");
		
		// Check if image exists
		if(!file_exists($image_path)){
			return false;
		
		// Check if file meets extension requirements
		} else if(!in_array($file['extension'], $valid_formats)) {
			return false;
			
		} else {
			
			// Load the stamp and the photo to apply the watermark to
			$stamp = imagecreatefrompng($stamp_path);
			
			// Designate image depending on extension
			if($file['extension'] == 'jpg' || $file['extension'] == 'jpeg'){
				$image = imagecreatefromjpeg($image_path);
			} else if ($file['extension'] == 'png'){
				$image = imagecreatefrompng($image_path);
			} else if ($file['extension'] == 'gif'){
				$image = imagecreatefromgif($image_path);
			}
		

			// Set the margins for the stamp and get the height/width of the stamp image
			$marge_right = 10;
			$marge_bottom = 10;
			$sx = imagesx($stamp);
			$sy = imagesy($stamp);

			// Copy the stamp image onto our photo using the margin offsets and the photo 
			// width to calculate positioning of the stamp. 
			imagecopy(
				$image, 
				$stamp, 
				imagesx($image) - $sx - $marge_right, 
				imagesy($image) - $sy - $marge_bottom, 
				0, 
				0, 
				imagesx($stamp), 
				imagesy($stamp)
			);

			// Output as PNG file and free memory
			//header('Content-type: image/png');
			//imagepng($image);
			header('Content-type: image/jpeg');
			imagejpeg($image);
			imagedestroy($image);
		}
	
	}	
	 
	 function jumlahAset()
	 {	
		 $this->db->where("status","0");
		 $this->db->select("SUM(harga) as harga");
		$return =$this->db->get("data_property")->row();
		return $return->harga;
	 } 
	 function getKodeAgenById($id)
	 {
		 $this->db->where("id_agen",$id);
		 $data= $this->db->get("data_agen")->row();
	 return isset($data->kode_agen)?($data->kode_agen):"";
	 }
	 function jumlahOmzetByAgen($today)
	 {	
		$kode=$this->session->userdata("kode");
		//$kode=$this->getKodeAgenById($id);
		// $this->db->where("status","1");
		 //$this->db->where("kode_agen",$kode);
		 //$this->db->or_where("selling",$kode);
		 //$this->db->select("SUM(harga) as harga");
		//$return =$this->db->get("data_selling")->row();
		$data=$this->db->query("SELECT SUM(harga_net) AS harga FROM `data_selling` WHERE year(tgl_closing) = $today AND (kode_agen='$kode' OR selling='$kode')")->row();
		//$data=$this->db->query("SELECT SUM(harga) AS harga FROM `data_selling` WHERE sumber_listing = sumber_selling AND year(tgl_closing) = $today AND (kode_agen='$kode' OR selling='$kode')")->row();
		return $data->harga;
	 }  
	 /*function jumlahOmzetByAgen2($today)
	 {	
		$kode=$this->session->userdata("kode");
		$data=$this->db->query("SELECT SUM(harga/2) AS harga FROM `data_selling` WHERE sumber_listing <> sumber_selling AND year(tgl_closing) = $today AND (kode_agen='$kode' OR selling='$kode')")->row();
		return $data->harga;
	 }*/  
	 function omzetNetwork()
	 {
		
		$data=$this->db->query("SELECT SUM(harga_net) as jml from data_selling WHERE kode_listing IN (SELECT kode_prop from data_property where id_network='".$this->session->userdata("id")."' )")->row();
		return $data->jml;
	 }
	function jumlahOmzetByAgenByKode($kode)
	 {	
		//$kode=$this->session->userdata("kode");
		//$kode=$this->getKodeAgenById($id);
		// $this->db->where("status","1");
		 $this->db->where("kode_agen",$kode);
		 $this->db->or_where("selling",$kode);
		 $this->db->select("SUM(harga_net) as harga");
		$return =$this->db->get("data_selling")->row();
		return $return->harga;
	 }  

	 function jumlahOmzet($today)
	 {	
	 	//$this->db->where("status","1");
		 $this->db->select("SUM(harga_net) as harga");
		 $this->db->where("year(tgl_closing)",$today);
		$return =$this->db->get("data_selling")->row();
		return $return->harga;
	 }
	 function getDataOmzet()
	 {	
		//$this->db->select("SUM(harga) as jmlHarga, year(tgl_closing) AS TAHUN");
		//$data=$this->db->get("data_selling")->result();
	    //return $data;
		return $this->db->query("SELECT SUM(harga_net) as jmlHarga, year(tgl_closing) AS TAHUN FROM data_selling GROUP BY year(tgl_closing)")->result();
	 }
	 function getDataOmzetBulanan()
	 {
		$jmlbln = $this->db->query("SELECT SUM(harga_net) as jmlHarga1, DATE_FORMAT(tgl_closing, '%b %y') AS bulan FROM data_selling GROUP BY month(tgl_closing), year(tgl_closing)")->num_rows();
		$batas = $jmlbln - 6;
		return $this->db->query("SELECT SUM(harga_net) as jmlHarga1, DATE_FORMAT(tgl_closing, '%b %y') AS bulan FROM data_selling GROUP BY month(tgl_closing), year(tgl_closing) ORDER BY tgl_closing ASC LIMIT 6 OFFSET ".$batas."")->result();
	 }
	 
	  function jumlahClosing()
	 {	
		 
		 $this->db->select("SUM(harga) as harga");
		$return =$this->db->get("data_selling")->row();
		return $return->harga;
	 }  
	 
	 
	 function jumlahOwner()
	 {	 
		 return $this->db->get("data_owner")->num_rows();
	 } 
	 
	
	 function jumlahCostumer()
	 {	 
		 return $this->db->get("data_pelanggan")->num_rows();
	 } 
	 
	 function jumlahAgen()
	 {	 
	     $jb = array("1","11","12");
	     $this->db->where_in("jabatan",$jb);
		 return $this->db->get("data_agen")->num_rows();
	 } 
	 function jumlahNetwork()
	 {	
	     $this->db->where("jabatan","100");
	     // $this->db->or_where("jabatan","2");
	    //   $this->db->or_where("jabatan","5");
		 return $this->db->get("data_agen")->num_rows();
	 } 
	 function jumlahPegawai()
	 {	
	     $jb = array("1","11","12");
	     $this->db->where("jabatan!=","100");
	      $this->db->where_not_in("jabatan",$jb);
	    //   $this->db->or_where("jabatan","5");
		 return $this->db->get("data_agen")->num_rows();
	 }  
	 function listingTerjualTersewa()
	 {	 $this->db->where("status","1");
		 return $this->db->get("data_property")->num_rows();
	 } 
	 function listingTerjualTersewaOther()
	 {
	     $this->db->where("status","4");
		 return $this->db->get("data_property")->num_rows();
	 }
	 function listingCancel()
	 {	 $this->db->where("status","2");
		 return $this->db->get("data_property")->num_rows();
	 } 
	function getResultListing()
	 {	 
		 $this->db->where("status!=","2");
		 return $this->db->get("data_property")->result();
	 } 

	 function getLatById($id)
	 {	 $this->db->where("id_prop",$id);
		 $return=$this->db->get("data_property")->row();
		 return isset($return->lat_detail)?($return->lat_detail):"";
	 } 
	 function getGambarUtamaById($id)
	 {	 $this->db->where("id_prop",$id);
		 $return=$this->db->get("data_property")->row();
		 return isset($return->gambar_utama)?($return->gambar_utama):"nopund.jpg";
	 } 
	 function getLongById($id)
	 {	 $this->db->where("id_prop",$id);
		 $return=$this->db->get("data_property")->row();
		 return isset($return->long_detail)?($return->long_detail):"";
	 } 
	 
	 function totalListingReady()
	 {	
		 return $this->db->get("data_property")->num_rows();
	 } 
	 function totalListingBerfetcsheet()
	 {	 
		 return $this->db->query("SELECT * FROM data_property where status IN ('0','1') AND (desain!='')")->num_rows();
	 } 
	 function totalListingTidakBerfetcsheet()
	 {	 
		return $this->db->query("SELECT * FROM data_property where status IN ('0','1') AND (desain='' or desain IS NULL)")->num_rows();
	 }
	function totalPromoSpanduk($id)
	 {	
	 $this->db->where("kode_prop",$id);
	 $this->db->where("jenis_promo","0");
		 return $this->db->get("data_promo")->num_rows();
	 }
	 function totalPromoKoran($id)
	 {	
	 $this->db->where("kode_prop",$id);
	 $this->db->where("jenis_promo","1");
		 return $this->db->get("data_promo")->num_rows();
	 }
	 function totalPromoMaxi($id)
	 {	
	 $this->db->where("kode_prop",$id);
	 $this->db->where("jenis_promo","2");
		 return $this->db->get("data_promo")->num_rows();
	 }
	 function totalPromoPapan($id)
	 {	
	 $this->db->where("kode_prop",$id);
	 $this->db->where("jenis_promo","3");
		 return $this->db->get("data_promo")->num_rows();
	 }
	 function disewakanReady()
	 {	
	 $this->db->where("status","0");
	 $this->db->where("type_jual","sewa");
		 return $this->db->get("data_property")->num_rows();
	 }
	 function dijualReady()
	 {	
	  $this->db->where("status","0");
	 $this->db->where("type_jual","jual");
		 return $this->db->get("data_property")->num_rows();
	 }
	 function getTopAgen($limit,$today)
	 {
		return $this->db->query("
		SELECT a.nama,a.kode_agen,(SELECT SUM(b.harga_net) FROM data_selling b WHERE year(tgl_closing) =$today AND 
(a.kode_agen=b.kode_agen OR a.kode_agen=b.selling)) AS biaya, 
(SELECT COUNT(b.kode_agen)  FROM data_selling b WHERE 
year(tgl_closing) =$today AND (a.kode_agen=b.kode_agen OR a.kode_agen=b.selling)) AS j FROM data_agen a  WHERE 
kode_agen IN(SELECT kode_agen FROM data_selling) OR kode_agen IN(SELECT selling FROM data_selling) ORDER BY(SELECT SUM(b.harga)
FROM data_selling b WHERE year(tgl_closing) =$today AND (a.kode_agen=b.kode_agen OR a.kode_agen=b.selling)) DESC limit $limit")->result();
	/*return $this->db->query("
			SELECT a.nama,a.kode_agen,(SELECT SUM(b.harga) FROM data_selling b WHERE a.kode_agen=b.kode_agen OR a.kode_agen=b.selling) AS biaya, 
	(SELECT COUNT(b.kode_agen)  FROM data_selling b WHERE 
	a.kode_agen=b.kode_agen OR a.kode_agen=b.selling) AS j FROM data_agen a  WHERE 
	kode_agen IN(SELECT kode_agen FROM data_selling) OR kode_agen IN(SELECT selling FROM data_selling) ORDER BY(SELECT SUM(b.harga)
	FROM data_selling b WHERE a.kode_agen=b.kode_agen OR a.kode_agen=b.selling) DESC limit $limit")->result();*/
	 }
	 function getTopProp($limit)
	 {
		 return $this->db->query("SELECT jenis_prop,COUNT(*) AS jml FROM data_property  WHERE STATUS='0'   GROUP BY jenis_prop ORDER BY COUNT(*) DESC limit $limit")->result();
	 }
	

 function getTopPropAgen($limit)
	 {
		 return $this->db->query("SELECT jenis_prop,COUNT(*) AS jml FROM data_property  WHERE STATUS='0' and agen='".$this->session->userdata('kode')."'   GROUP BY jenis_prop ORDER BY COUNT(*) DESC limit $limit")->result();
	 }
	 
	function getLBListing($id)
	{
		$this->db->where("kode_prop",$id);
		$data=$this->db->get("data_property")->row();
	return isset($data->luas_bangunan)?($data->luas_bangunan):"";
	}
	
	function getLTListing($id)
	{
		$this->db->where("kode_prop",$id);
		$data=$this->db->get("data_property")->row();
	return isset($data->luas_tanah)?($data->luas_tanah):"";
	}
	 
	 

	/*-----------------------------------------------------AGEN----------------------------------------*/
	 function jumlahAsetAgen()
	 {	
		  $KODE=$this->session->userdata("kode");
		  $this->db->where("agen",$KODE);
		 $this->db->where("status","0");
		 $this->db->select("SUM(harga) as harga");
		$return =$this->db->get("data_property")->row();
		return $return->harga;
	 }  
	  function jumlahClosingAgen()
	 {	
		  $KODE=$this->session->userdata("kode");
		  $this->db->where("kode_agen",$KODE);
		 $this->db->select("SUM(harga) as harga");
		$return =$this->db->get("data_selling")->row();
		return $return->harga;
	 }  
	 
	 
	 function jumlahOwnerAgen()
	{	 
		  $KODE=$this->session->userdata("kode");
		 return $this->db->query("SELECT DISTINCT(id_owner)  FROM data_property WHERE agen='".$KODE."'")->num_rows();
	 } 
	 
	 function jumlahCostumerAgen()
	 {	 $KODE=$this->session->userdata("kode");
		  $this->db->where("kode_agen",$KODE);
		 return $this->db->get("data_pelanggan")->num_rows();
	 }  
	 function getWebKontak()
	 {	 
		  $this->db->where("id",1);
		  return $this->db->get("web_kontak")->row();
	 }
	 function getTentang()
	 {	 
		  $this->db->where("id",1);
		  $return=$this->db->get("web_tentang")->row();
		  return isset($return->content)?($return->content):"";
	 }  function getSlider()
	 {	 	
			$this->db->where("poto!=","");
		  return $this->db->get("web_slider")->result();
	 } 
	  function totalListingReadyAgen()
	 {	
			$KODE=$this->session->userdata("kode");
		  $this->db->where("agen",$KODE);
			//$this->db->where("status","0");
		 return $this->db->get("data_property")->num_rows();
	 } 
	 function totalListingReadyNetwork()
	 {	
			$KODE=$this->session->userdata("id");
		  $this->db->where("id_network",$KODE);
			 $this->db->where("status","0");
		 return $this->db->get("data_property")->num_rows();
	 } 
	 function totalListingReadyAgenByKode($KODE)
	 {	
			//$KODE=$this->session->userdata("kode");
		  $this->db->where("agen",$KODE);
			$this->db->where("status","0");
		 return $this->db->get("data_property")->num_rows();
	 } 
	 function disewakanReadyAgen()
	 {	
	  $KODE=$this->session->userdata("kode");
		  $this->db->where("agen",$KODE);
	 $this->db->where("status","0");
	 $this->db->where("type_jual","sewa");
		 return $this->db->get("data_property")->num_rows();
	 } function disewakanReadyNetwork()
	 {	
	  $KODE=$this->session->userdata("id");
		  $this->db->where("id_network",$KODE);
	 $this->db->where("status","0");
	 $this->db->where("type_jual","sewa");
		 return $this->db->get("data_property")->num_rows();
	 }
	 function dijualReadyAgen()
	 {	
	  $KODE=$this->session->userdata("kode");
		  $this->db->where("agen",$KODE);
	  $this->db->where("status","0");
	 $this->db->where("type_jual","jual");
		 return $this->db->get("data_property")->num_rows();
	 } function dijualReadyNetwork()
	 {	
	  $KODE=$this->session->userdata("id");
		  $this->db->where("id_network",$KODE);
	  $this->db->where("status","0");
	 $this->db->where("type_jual","jual");
		 return $this->db->get("data_property")->num_rows();
	 }
	  function listingTerjualTersewaAgen()
	 {	 
	  $KODE=$this->session->userdata("kode");
		  /*$this->db->where("agen",$KODE);
	 $this->db->where("status","1");
		 return $this->db->get("data_property")->num_rows();*/
		 $this->db->where("kode_agen",$KODE);
		 $this->db->or_where("selling",$KODE);
		 return $this->db->get("data_selling")->num_rows();
	 }  function listingTerjualTersewaNetwork()
	 {	 
	  $KODE=$this->session->userdata("id");
		  $this->db->where("id_network",$KODE);
	 $this->db->where("status","1");
		 return $this->db->get("data_property")->num_rows();
	 } 
	 function listingCancelAgen()
	 {	 
	  $KODE=$this->session->userdata("kode");
		  $this->db->where("agen",$KODE);
	 $this->db->where("status","2");
		 return $this->db->get("data_property")->num_rows();
	 } 
	  function dataTimeLine()
	 {	 
		 $this->db->order_by("id","ASC");
		 return $this->db->get("timeline")->result();
	 } 
	 
	
	 /*-----------------------------------------------------ENDAGEN----------------------------------------*/
	 
	 
	 /*--------------------WEBSITE-----------------------*/
	 function titleWeb($kode)
	 {
		 $title=$this->db->query('SELECT * from data_property where kode_prop="'.$kode.'"')->row();
	 	if($title->desc!="")
		{
			return isset($title->desc)?($title->desc):"";
		}else{
		return $this->getNamaJenis($kode);	
			}
	 }
	 
	 /*---------------------------NETWORK------------------------>*/
	 
	  function getDataNetwork()
	 {	 
		 $this->db->order_by("nama","ASC");
		 $this->db->where("jabatan","100");
		 return $this->db->get("data_agen")->result();
	 }
	 
	 /*---------------------------LOGO BANK------------------------>*/
	 
	 function getBanklogo()
	 {
		$this->db->order_by("nama","ASC");
		$data=$this->db->get("tr_bank")->result();
		return $data;
	}
	
	/*---------------------------CMA------------------------>*/
	 
	 function cekCMA($nama_area)
	 {
		$namaarea = addslashes($nama_area);
		return $this->db->query("SELECT * FROM `tr_cma` WHERE nama_area LIKE '%".$namaarea."%'")->num_rows();
	 	/*if($datas->id_cma!="")
		{
			return "CMA Ready";
		}else{
			return "CMA Not Ready";	
		}*/
	}
	  
}