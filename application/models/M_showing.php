<?php

class M_showing extends CI_Model  {
    
		
	function __construct()
    {
        parent::__construct();
		
    }
	
	/*---------------------------------------------*/
		
	function get_dataShowing()
	{
		 $query = $this->_get_dataShowing();
        if ($_POST['length'] != -1)
            $query .= " limit " . $_POST['start'] . "," . $_POST['length'];
        return $this->db->query($query)->result();
 
	}
	  public function counts() {
        $query = $this->_get_dataShowing();
        return $this->db->query($query)->num_rows();
    }
	function _get_dataShowing()
	{
	$dan="";
	$agen=$this->input->get("agen");
	if($agen){
	$dan.=" AND agen='$agen'";
	}
	$tahun=$this->input->get("tahun");
	if($tahun){
	$dan.=" AND year(tgl_showing) = '$tahun'";
	}
	$bulan=$this->input->get("bulan");
	if($bulan){
	$dan.=" AND month(tgl_showing) = '$bulan'";
	}
	$kelengkapan=$this->input->get("kelengkapan");
	if($kelengkapan == '1'){
	$dan.=" AND foto_showing != ''";
	}elseif($kelengkapan == '2'){
	$dan.=" AND foto_showing = ''";
	}
		
	
		if($this->session->userdata("id")==151){//Rhafa
			$query = "SELECT a.*, b.nama FROM data_showing AS a 
			LEFT JOIN data_agen AS b ON a.agen = b.kode_agen WHERE 1=1 AND a.agen='BREA/039/I/2018' $dan";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				nama LIKE '%" . $searchkey . "%'
				) ";
			}
		}elseif($this->session->userdata("id")==146){
			$query = "SELECT a.*, b.nama FROM data_showing AS a 
			LEFT JOIN data_agen AS b ON a.agen = b.kode_agen WHERE 1=1 AND a.agen IN ('BREA/032/XI/2017') $dan";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				nama LIKE '%" . $searchkey . "%' 
				) ";
			}
		}elseif($this->session->userdata("id")==152){//Vivi
			$query = "SELECT a.*, b.nama FROM data_showing AS a 
			LEFT JOIN data_agen AS b ON a.agen = b.kode_agen WHERE 1=1 AND a.agen NOT IN ('BREA/046/II/2018', 'BREA/032/XI/2017', 'BREA/001/I/2017', 'BREA/005/II/2017', 'BREA/012/VIII/2017', 'BREA/066/VII/2018', 'BREA/010/I/2017') $dan";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				nama LIKE '%" . $searchkey . "%' 
				) ";
			}
		}elseif($this->session->userdata("id")==161){//Yema
			$query = "SELECT a.*, b.nama FROM data_showing AS a 
			LEFT JOIN data_agen AS b ON a.agen = b.kode_agen WHERE 1=1 AND a.agen IN ('BREA/001/I/2017') $dan";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				nama LIKE '%" . $searchkey . "%' 
				) ";
			}
		}elseif($this->session->userdata("id")==162){//Frans
			$query = "SELECT a.*, b.nama FROM data_showing AS a 
			LEFT JOIN data_agen AS b ON a.agen = b.kode_agen WHERE 1=1 AND a.agen IN ('BREA/046/II/2018', 'BREA/010/I/2017') $dan";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				nama LIKE '%" . $searchkey . "%' 
				) ";
			}
		}elseif($this->session->userdata("id")==165){//Rena
			$query = "SELECT a.*, b.nama FROM data_showing AS a 
			LEFT JOIN data_agen AS b ON a.agen = b.kode_agen WHERE 1=1 AND a.agen IN ('BREA/005/II/2017', 'BREA/012/VIII/2017', 'BREA/066/VII/2018') $dan";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				nama LIKE '%" . $searchkey . "%' 
				) ";
			}
		}else{
			$query = "SELECT a.*, b.nama FROM data_showing AS a 
			LEFT JOIN data_agen AS b ON a.agen = b.kode_agen WHERE 1=1 $dan ";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				nama LIKE '%" . $searchkey . "%' 
				) ";
			}
		}
		/*
		$query = "SELECT a.*, b.alamat_detail, b.area_listing, c.nama FROM data_promo AS a 
		LEFT JOIN data_property AS b ON a.kode_prop = b.kode_prop
		LEFT JOIN data_agen AS c ON b.agen = c.kode_agen WHERE 1=1 $dan ";
        if (isset($_POST['search']['value'])) {
            $searchkey = $_POST['search']['value'];
            $query .= " AND (
			a.kode_prop LIKE '%" . $searchkey . "%' or
			nama LIKE '%" . $searchkey . "%' or 
			alamat_detail LIKE '%" . $searchkey . "%' or
			alamat_promo LIKE '%" . $searchkey . "%' or
			area_listing LIKE '%" . $searchkey . "%' 
			) ";
        }*/

        $column = array('', '', 'id_showing','tgl_showing', 'foto_showing', 'nama');
        $i = 0;
        foreach ($column as $item) {
            $column[$i] = $item;
        }

		$query.=" order by tgl_showing DESC" ;
        return $query;
	}
	
	function uploadGambar($form,$kode)
	{
		
		if(isset($_FILES[$form]['type']))
		{
		return	$this->upload_img($form,$kode);
		}else{
			return $this->gambarDefauld($form,$kode);
		}
	}
	function gambarDefauld($form,$kode)
	{
		$kode = $this->input->post("id_showing");
	$data=$this->db->get_where("data_showing",array("id_showing"=>$kode))->row();	
	return isset($data->foto_showing)?($data->foto_showing):"";
	}
	public function upload_img($form,$kode)
	{	//$this->m_konfig->log("admin","Upload photo");
		///
		  $nama=date("YmdHis");
		  $lokasi_file = $_FILES[$form]['tmp_name'];
		  $tipe_file   = $_FILES[$form]['type'];
		  $nama_file   = $_FILES[$form]['name'];
		  //if($tipe_file)
		  //{
		  $daprof=$this->gambarDefauld($form,$kode);
			if($daprof!="")
			 {
				 $path = "file_upload/img/".$daprof;
				 if (file_exists($path)) {
					unlink($path);
				 }
		   }
		  
		  
			//$jenis=explode(".",$nama_file);
			$nama_file=str_replace(" ","_",$nama_file);
			// $jenis="jpg";
			$nama=str_replace("/","",$kode.$nama.$nama_file);
			 $target_path = "file_upload/img/".$nama;
			 //
	//	  }
		  //
		if (!empty($lokasi_file)) {
		move_uploaded_file($lokasi_file,$target_path);
		//if($jenis=="png"){
		//$this->konversi->UploadImageResize($target_path,$jenis,200);
		}
	//	$this->reff->UploadImageResize($target_path,"jpg",800);
		return $nama;
	}
		
	function insert()
	{
	$kode1=date('dmYHis');
	$kode='sw-'.$kode1.'';
	$data=array(
	"tgl_showing"=>$this->tanggal->eng_($this->input->post("tgl_showing"),"-"),
	"foto_showing"=>$this->uploadGambar("foto_showing",$kode),
	"agen"=>$this->input->post("agen"),
	);
	
	return $this->db->insert("data_showing",$data);


	}
	
	function update()
	{
	$array=array(
	"tgl_showing"=>$this->tanggal->eng_($this->input->post("tgl_showing"),"-"),
	"foto_showing"=>$this->uploadGambar("foto_showing",$kode),
	"agen"=>$this->input->post("agen"),
	);
		$this->db->where("id_showing",$this->input->post("id_showing"));
		return $this->db->update("data_showing",$array);
	}
	
	
	function HapusAll()
	{
		$hapus=$this->input->post("hapus");
		foreach($hapus as $id)
		{
		$this->db->where("id_showing",$id);
		$this->db->delete("data_showing");
		}	return true;
	}
	function hapus($id)
	{
		$this->db->where("id_showing",$id);
		$data=$this->db->get("data_showing")->row();
		$foto = $data->foto_showing;
		$tempat_foto = ''.FCPATH.'file_upload/img/'.$foto;
		unlink($tempat_foto);
	
		$this->db->where("id_showing",$id);
		return	$this->db->delete("data_showing");
	}

	
}