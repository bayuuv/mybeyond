<?php

class M_offpromo extends CI_Model  {
    
		
	function __construct()
    {
        parent::__construct();
		
    }
	
	/*---------------------------------------------*/
		
	function get_dataOff()
	{
		 $query = $this->_get_dataOff();
        if ($_POST['length'] != -1)
            $query .= " limit " . $_POST['start'] . "," . $_POST['length'];
        return $this->db->query($query)->result();
 
	}
	  public function counts() {
        $query = $this->_get_dataOff();
        return $this->db->query($query)->num_rows();
    }
	function _get_dataOff()
	{
	$dan="";
	$agen=$this->input->get("agen");
	if($agen){
	$dan.=" AND b.agen='$agen'";
	}
	$kelengkapan=$this->input->get("kelengkapan");
	if($kelengkapan){
		if($kelengkapan=='1')
		{
			$dan.=" AND foto_promo is not null    ";
		}elseif($kelengkapan=='2')
		{
		    $dan.=" AND foto_promo is null     ";
		}
	}
	$jenis_promo=$this->input->get("jenis_promo");
	if($jenis_promo){
		if($jenis_promo=='0')
		{
			$dan.=" AND jenis_promo='0'";
		}elseif($jenis_promo=='1')
		{
		    $dan.=" AND jenis_promo='1'";
		}elseif($jenis_promo=='2')
		{
		    $dan.=" AND jenis_promo='2'";
		}elseif($jenis_promo=='3')
		{
		    $dan.=" AND jenis_promo='3'";
		}
	}
	$status_promo=$this->input->get("status_promo");
	if($status_promo){
		if($status_promo=='2')
		{
			$dan.=" AND a.status='0'";
		}elseif($status_promo=='1')
		{
		    $dan.=" AND a.status='1'";
		}
	}
        if($this->session->userdata("id")==64){//Ajeng
			$query = "SELECT a.*, b.alamat_detail, b.area_listing, c.nama FROM data_promo AS a 
			LEFT JOIN data_property AS b ON a.kode_prop = b.kode_prop
			LEFT JOIN data_agen AS c ON b.agen = c.kode_agen WHERE 1=1 AND b.agen IN ('BREA/005/II/2017', 'BREA/012/VIII/2017') $dan";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				a.kode_prop LIKE '%" . $searchkey . "%' or
				nama LIKE '%" . $searchkey . "%' or 
				alamat_detail LIKE '%" . $searchkey . "%' or
				alamat_promo LIKE '%" . $searchkey . "%' or
				area_listing LIKE '%" . $searchkey . "%' 
				) ";
			}
		}elseif($this->session->userdata("id")==151){//Rhafa
			$query = "SELECT a.*, b.alamat_detail, b.area_listing, c.nama FROM data_promo AS a 
			LEFT JOIN data_property AS b ON a.kode_prop = b.kode_prop
			LEFT JOIN data_agen AS c ON b.agen = c.kode_agen WHERE 1=1 AND b.agen='BREA/039/I/2018' $dan";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				a.kode_prop LIKE '%" . $searchkey . "%' or
				nama LIKE '%" . $searchkey . "%' or 
				alamat_detail LIKE '%" . $searchkey . "%' or
				alamat_promo LIKE '%" . $searchkey . "%' or
				area_listing LIKE '%" . $searchkey . "%' 
				) ";
			}
		}elseif($this->session->userdata("id")==133){//Kiki
			$query = "SELECT a.*, b.alamat_detail, b.area_listing, c.nama FROM data_promo AS a 
			LEFT JOIN data_property AS b ON a.kode_prop = b.kode_prop
			LEFT JOIN data_agen AS c ON b.agen = c.kode_agen WHERE 1=1 AND b.agen IN ('BREA/046/II/2018') $dan";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				a.kode_prop LIKE '%" . $searchkey . "%' or
				nama LIKE '%" . $searchkey . "%' or 
				alamat_detail LIKE '%" . $searchkey . "%' or
				alamat_promo LIKE '%" . $searchkey . "%' or
				area_listing LIKE '%" . $searchkey . "%' 
				) ";
			}
		}elseif($this->session->userdata("id")==146){
			$query = "SELECT a.*, b.alamat_detail, b.area_listing, c.nama FROM data_promo AS a 
			LEFT JOIN data_property AS b ON a.kode_prop = b.kode_prop
			LEFT JOIN data_agen AS c ON b.agen = c.kode_agen WHERE 1=1 AND b.agen IN ('BREA/032/XI/2017') $dan";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				a.kode_prop LIKE '%" . $searchkey . "%' or
				nama LIKE '%" . $searchkey . "%' or 
				alamat_detail LIKE '%" . $searchkey . "%' or
				alamat_promo LIKE '%" . $searchkey . "%' or
				area_listing LIKE '%" . $searchkey . "%' 
				) ";
			}
		}elseif($this->session->userdata("id")==152){//Vivi
			$query = "SELECT a.*, b.alamat_detail, b.area_listing, c.nama FROM data_promo AS a 
			LEFT JOIN data_property AS b ON a.kode_prop = b.kode_prop
			LEFT JOIN data_agen AS c ON b.agen = c.kode_agen WHERE 1=1 AND b.agen NOT IN ('BREA/046/II/2018', 'BREA/032/XI/2017', 'BREA/001/I/2017', 'BREA/005/II/2017', 'BREA/012/VIII/2017', 'BREA/066/VII/2018', 'BREA/010/I/2017') $dan";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				a.kode_prop LIKE '%" . $searchkey . "%' or
				nama LIKE '%" . $searchkey . "%' or 
				alamat_detail LIKE '%" . $searchkey . "%' or
				alamat_promo LIKE '%" . $searchkey . "%' or
				area_listing LIKE '%" . $searchkey . "%' 
				) ";
			}
		}elseif($this->session->userdata("id")==161){//Yema
			$query = "SELECT a.*, b.alamat_detail, b.area_listing, c.nama FROM data_promo AS a 
			LEFT JOIN data_property AS b ON a.kode_prop = b.kode_prop
			LEFT JOIN data_agen AS c ON b.agen = c.kode_agen WHERE 1=1 AND b.agen IN ('BREA/001/I/2017') $dan";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				a.kode_prop LIKE '%" . $searchkey . "%' or
				nama LIKE '%" . $searchkey . "%' or 
				alamat_detail LIKE '%" . $searchkey . "%' or
				alamat_promo LIKE '%" . $searchkey . "%' or
				area_listing LIKE '%" . $searchkey . "%' 
				) ";
			}
		}elseif($this->session->userdata("id")==162){//Fransiskus
			$query = "SELECT a.*, b.alamat_detail, b.area_listing, c.nama FROM data_promo AS a 
			LEFT JOIN data_property AS b ON a.kode_prop = b.kode_prop
			LEFT JOIN data_agen AS c ON b.agen = c.kode_agen WHERE 1=1 AND b.agen IN ('BREA/046/II/2018', 'BREA/010/I/2017') $dan";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				a.kode_prop LIKE '%" . $searchkey . "%' or
				nama LIKE '%" . $searchkey . "%' or 
				alamat_detail LIKE '%" . $searchkey . "%' or
				alamat_promo LIKE '%" . $searchkey . "%' or
				area_listing LIKE '%" . $searchkey . "%' 
				) ";
			}
		}elseif($this->session->userdata("id")==165){//Rena
			$query = "SELECT a.*, b.alamat_detail, b.area_listing, c.nama FROM data_promo AS a 
			LEFT JOIN data_property AS b ON a.kode_prop = b.kode_prop
			LEFT JOIN data_agen AS c ON b.agen = c.kode_agen WHERE 1=1 AND b.agen IN ('BREA/005/II/2017', 'BREA/012/VIII/2017', 'BREA/066/VII/2018') $dan";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				a.kode_prop LIKE '%" . $searchkey . "%' or
				nama LIKE '%" . $searchkey . "%' or 
				alamat_detail LIKE '%" . $searchkey . "%' or
				alamat_promo LIKE '%" . $searchkey . "%' or
				area_listing LIKE '%" . $searchkey . "%' 
				) ";
			}
		}else{
			$query = "SELECT a.*, b.alamat_detail, b.area_listing, c.nama FROM data_promo AS a 
			LEFT JOIN data_property AS b ON a.kode_prop = b.kode_prop
			LEFT JOIN data_agen AS c ON b.agen = c.kode_agen WHERE 1=1 $dan ";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				a.kode_prop LIKE '%" . $searchkey . "%' or
				nama LIKE '%" . $searchkey . "%' or 
				alamat_detail LIKE '%" . $searchkey . "%' or
				alamat_promo LIKE '%" . $searchkey . "%' or
				area_listing LIKE '%" . $searchkey . "%' 
				) ";
			}
		}
		/*
		$query = "SELECT a.*, b.alamat_detail, b.area_listing, c.nama FROM data_promo AS a 
		LEFT JOIN data_property AS b ON a.kode_prop = b.kode_prop
		LEFT JOIN data_agen AS c ON b.agen = c.kode_agen WHERE 1=1 $dan ";
        if (isset($_POST['search']['value'])) {
            $searchkey = $_POST['search']['value'];
            $query .= " AND (
			a.kode_prop LIKE '%" . $searchkey . "%' or
			nama LIKE '%" . $searchkey . "%' or 
			alamat_detail LIKE '%" . $searchkey . "%' or
			alamat_promo LIKE '%" . $searchkey . "%' or
			area_listing LIKE '%" . $searchkey . "%' 
			) ";
        }*/

        $column = array('', '', 'id_promo','kode_prop', 'nama', 'alamat_detail', 'area_listing', 'tgl_promo', 'alamat_promo', 'foto_promo', 'jenis_promo', 'status');
        $i = 0;
        foreach ($column as $item) {
            $column[$i] = $item;
        }

		$query.=" order by tgl_promo DESC" ;
        return $query;
	}
	
		function uploadGambar($form,$kode)
	{
		
		if(isset($_FILES[$form]['type']))
		{
		return	$this->upload_img($form,$kode);
		}else{
			return $this->gambarDefauld($form,$kode);
		}
	}
	function gambarDefauld($form,$kode)
	{
	$data=$this->db->get_where("data_promo",array("kode_prop"=>$kode))->row();	
	return isset($data->foto_promo)?($data->foto_promo):"";
	}
	public function upload_img($form,$kode)
	{	//$this->m_konfig->log("admin","Upload photo");
		///
			$nama=date("YmdHis");
		  $lokasi_file = $_FILES[$form]['tmp_name'];
		  $tipe_file   = $_FILES[$form]['type'];
		  $nama_file   = $_FILES[$form]['name'];
		  //if($tipe_file)
		  //{
		  $daprof=$this->gambarDefauld($form,$kode);
			if($daprof!="")
			 {
				 $path = "file_upload/img/".$daprof;
				 if (file_exists($path)) {
					unlink($path);
				 }
		   }
		  
		  
			//$jenis=explode(".",$nama_file);
			$nama_file=str_replace(" ","_",$nama_file);
			// $jenis="jpg";
			$nama=str_replace("/","",$kode.$nama.$nama_file);
			 $target_path = "file_upload/img/".$nama;
			 //
	//	  }
		  //
		if (!empty($lokasi_file)) {
		move_uploaded_file($lokasi_file,$target_path);
		//if($jenis=="png"){
		//$this->konversi->UploadImageResize($target_path,$jenis,200);
		}
	//	$this->reff->UploadImageResize($target_path,"jpg",800);
		return $nama;
	}
		
	function insert()
	{
	$kode1=date('dmYHis');
	$kode='pr-'.$kode1.'';
	$data=array(
	"kode_prop"=>$this->input->post("kode_prop"),
	"alamat_promo"=>$this->input->post("alamat_promo"),
	"tgl_promo"=>$this->tanggal->eng_($this->input->post("tgl_promo"),"-"),
	"foto_promo"=>$this->uploadGambar("foto_promo",$kode),
	"status"=>$this->input->post("status"),
	"jenis_promo"=>$this->input->post("jenis_promo"),
	);
	
	return $this->db->insert("data_promo",$data);


	}
	
	function updatePromo()
	{
		if(isset($_FILES[$form]['type']))
		{
			$kode1=date('dmYHis');
			$kode='pr-'.$kode1.'';
		}else{
			$kode=$this->input->post("kode_prop");
		}
	
	
	$array=array(
	"kode_prop"=>$this->input->post("kode_prop"),
	"alamat_promo"=>$this->input->post("alamat_promo"),
	"tgl_promo"=>$this->tanggal->eng_($this->input->post("tgl_promo"),"-"),
	"foto_promo"=>$this->uploadGambar("foto_promo",$kode),
	"status"=>$this->input->post("status"),
	"jenis_promo"=>$this->input->post("jenis_promo"),
	);
		$this->db->where("id_promo",$this->input->post("id_promo"));
		return $this->db->update("data_promo",$array);
	}
	
	
	function HapusAll()
	{
		$hapus=$this->input->post("hapus");
		foreach($hapus as $id)
		{
		$this->db->where("id_promo",$id);
		$this->db->delete("data_promo");
		}	return true;
	}
	function hapus($id)
	{
		$this->db->where("id_promo",$id);
		$data=$this->db->get("data_promo")->row();
		$foto = $data->foto_promo;
		$tempat_foto = ''.FCPATH.'file_upload/img/'.$foto;
		unlink($tempat_foto);
	
		$this->db->where("id_promo",$id);
		return	$this->db->delete("data_promo");
	}
	function export()
	{
//////start
        $objPHPExcel = new PHPExcel();
//style
        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'rotation' => 0,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '6CCECB')
            ),
            'borders' =>
            array('allborders' =>
                array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'),
                ),
            ),
        );
     //   $objPHPExcel->getActiveSheet(0)->getColumnDimension('F')->setWidth(25);
     //   $objPHPExcel->getActiveSheet(0)->getColumnDimension('G')->setWidth(25);
     //   $objPHPExcel->getActiveSheet(0)->getColumnDimension('H')->setWidth(35);




//create column

        $objPHPExcel->getActiveSheet(0)->setCellValue('A1', 'NAMA');
        $objPHPExcel->getActiveSheet(0)->setCellValue('B1', 'NOMOR HP');
        $objPHPExcel->getActiveSheet(0)->setCellValue('C1', 'EMAIL');
        $objPHPExcel->getActiveSheet(0)->setCellValue('D1', 'ALAMAT');

//make a border column
        $objPHPExcel->getActiveSheet(0)->getStyle('A1:D1')->applyFromArray($style);

        $database = $this->_get_dataOwner();
        $shit = 1;
        $database = $this->db->query($database)->result();
        foreach ($database as $list) {
            $shit++;
//create data per row
          $objPHPExcel->getActiveSheet(0)->setCellValue('A' . $shit . '', $list->nama);
          $objPHPExcel->getActiveSheet(0)->setCellValue('B' . $shit . '', '`'.$list->hp);
            $objPHPExcel->getActiveSheet(0)->setCellValue('C' . $shit . '', $list->email);
            $objPHPExcel->getActiveSheet(0)->setCellValue('D' . $shit . '', $list->alamat);

        }

//auto size column
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('D')->setAutoSize(true);



// Rename worksheet (worksheet, not filename)
        $objPHPExcel->getActiveSheet(0)->setTitle('Data Owner');
        
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Data-Owner.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
//////finish
    
	}
	
	function downloadFormat()
	{
	//////start
		$objPHPExcel = new PHPExcel();
		//style
		$style = array( 
		 'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				  'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				  'rotation'   => 0,
		  ),
		   'fill' => array(
				  'type' => PHPExcel_Style_Fill::FILL_SOLID,
				  'color' => array('rgb' => 'ccccff')
			  ),
		 'borders' => 
		  array( 'allborders' => 
			array( 'style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'), 
			  ), 
			), 
		);	
		
		$head = array( 
			'font'  => array(
			'bold'  => true,
			'color' => array('rgb' => 'FFFFFF'),
			),
			
		   'fill' => array(
				  'type' => PHPExcel_Style_Fill::FILL_SOLID,
				  'color' => array('rgb' => '009966')
			  ),
		 'borders' => 
		  array( 'allborders' => 
			array( 'style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'), 
			  ), 
			), 
		);
		
		
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('A')->setWidth(25);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('B')->setWidth(25);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('C')->setWidth(25);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('D')->setWidth(25);

		
		
		//create column
		
		$objPHPExcel->getActiveSheet(0)->setCellValue('A1', 'Nama');
		$objPHPExcel->getActiveSheet(0)->setCellValue('B1', 'Nomor Hp');
		$objPHPExcel->getActiveSheet(0)->setCellValue('C1', 'Email');
		$objPHPExcel->getActiveSheet(0)->setCellValue('D1', 'Alamat');
		
		//make a border column
		$objPHPExcel->getActiveSheet(0)->getStyle('A1:D1')->applyFromArray($style);
		
		
		
		// Rename worksheet (worksheet, not filename)
		$objPHPExcel->getActiveSheet(0)->setTitle('Data Agen');
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		
		header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Form Agen.xlsx"');
		header('Cache-Control: max-age=0');
		 
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		//////finish
		
	}
	function importData2()
	{
	$sukses=0;$gagal=0;$edit=0;
		 $file   = explode('.',$_FILES['userfile']['name']);
		$length = count($file);
		if($file[$length -1] == 'xlsx' || $file[$length -1] == 'xls'){//jagain barangkali uploadnya selain file excel <span class="wp-smiley wp-emoji wp-emoji-smile" title=":-)">:-)</span>
        $tmp    = $_FILES['userfile']['tmp_name'];//Baca dari tmp folder jadi file ga perlu jadi sampah di server :-p
		      
				 // load excel
			    $file = $_FILES['userfile']['tmp_name'];
			    $load = PHPExcel_IOFactory::load($file);
                $sheets = $load->getActiveSheet()->toArray(null,true,true,true);
				$i=1; $idowner=1;
				foreach ($sheets as $sheet) {
				if ($i > 1) {						
					//	$hp=str_replace("'","",$sheet[1]);
					//	$hp=str_replace("`","",$hp);
					//	$hp=str_replace("+62","0",$hp);
									
							//get form
															
	
		//  $carikode = $this->db->query("SHOW TABLE STATUS LIKE 'data_property'")->row();
			    $datakode =$sheet[0];
		  	    $kode = (int) $datakode;
		   		$set27=$sheet[27];
				if($set27!="")
				{
					$set27=$sheet[27];
				}else{
					$set27="06-13-17";
				}
				$pecahsit=explode("-",$set27);
				$sheet27=$pecahsit[1].$pecahsit[0].$pecahsit[2];
				//$newID = sprintf("%03s", $kode);
				$valtgl=str_replace("`","",$sheet[27]);
				$valtgl=str_replace("-","/",$sheet[27]);
				$valtgl="20".$pecahsit[2]."-".$pecahsit[0]."-".$pecahsit[1];//$this->tanggal->eng_($valtgl,"-");
				
				//$thn=substr($valtgl,2,2);
				//$bln=sprintf("%02s",substr($valtgl,5,2));
				//$tgl=sprintf("%02s",substr($valtgl,8,2));
				$urut=sprintf("%03s",$sheet[0]);
				////$tgl=sprintf("%02s",date('d'));
//$bulan=$this->tanggal->bulanRomawi($bulan);
				$tgl=$sheet[27];
				$tgl=str_replace("/","",$tgl);
				$tgl=str_replace("-","",$tgl);
				$tgl1=SUBSTR($tgl,0,4);
				$tgl2=SUBSTR($tgl,4,2);
				if($sheet[27]!="")
				{
				$tglfix=$sheet27;
				}else
				{
				$tglfix="130617";
				}
				$kode=$tglfix."-".$urut;	
	
						$sql=array(
								"kode_prop"=>$kode,
								"jenis_prop"=>$sheet[7],
							//	"jenis_listing"=>$sheet[3],
								"type_jual"=>$sheet[3],
							//	"desc"=>$this->input->post("desc"),
								"id_prov"=>$sheet[29],
								"id_kab"=>$sheet[30],
								"id_owner"=>$this->addDataOwner($idowner++,$sheet[1],$sheet[4],$sheet[5],$sheet[6]),
							//	"komplek"=>$this->input->post("nama_komplek"),
								"nama_area"=>$sheet[2],
							//	"lat_area"=>$this->input->post("lat_area"),
							//	"long_area"=>$this->input->post("long_area"),
								"alamat_detail"=>$sheet[2],//$this->input->post("alamat_detail"),
							//	"lat_detail"=>$this->input->post("lat"),
							//	"long_detail"=>$this->input->post("long"),
								"luas_tanah"=>$sheet[9],
								"luas_bangunan"=>$sheet[10],
							//	"tahun_dibangun"=>$this->input->post("tahun_dibangun"),
								"harga"=>$sheet[8],
								"kamar_tidur"=>$sheet[14],
								"kamar_mandi"=>$sheet[15],
								"kamar_tidur_p"=>$sheet[16],
								"kamar_mandi_p"=>$sheet[17],
								"jml_lantai"=>$sheet[11],
								"jml_garasi"=>$sheet[12],
								"jml_carports"=>$sheet[13],
								"daya_listrik"=>$sheet[18],
								"hadap"=>$sheet[21],
								//"type_sewa"=>$this->input->post("type_sewa"),
								"jenis_sertifikat"=>$sheet[22],
								//"kelengkapan"=>"1",
								"agen"=>$sheet[26],
								"keterangan"=>$sheet[23],
								"air"=>$sheet[19],
								"furniture"=>$sheet[20],
								"area_listing"=>$sheet[28],
								//"gambar1"=>$this->uploadGambar($kode,"upload1"),
							//	"gambar2"=>$this->uploadGambar($kode,"upload2"),
							//	"gambar3"=>$this->uploadGambar($kode,"upload3"),
							//	"gambar4"=>$this->uploadGambar($kode,"upload4"),
							//	"gambar5"=>$this->uploadGambar($kode,"upload5"),
							//	"gambar_utama"=>$this->input->post("set"),
								"tgl_masuk_listing"=>$valtgl,
								"media_promosi"=>$sheet[25],
								"fee_persen"=>$sheet[24],
						);		
				       
					//	$cek=$this->cekOwner($hp);
					if(1==1)//jika data kontak tidak ada maka tambahkan
					{  
							$this->db->insert("data_property",$sql); $sukses++;
					}	/*else{ //jika data kontak ada maka edit yg ada
						$sql=array(
						"nama"=>$sheet[0],
						"hp"=>$hp=$this->db->escape_str($hp),
						"email"=>$sheet[2],
						"alamat"=>$sheet[3],
						);						
							$this->db->where("hp",$hp);
							$this->db->update("data_owner",$sql);
							$edit++;
					};*/
					   
				}
				$i++;
                }
               
		}else{
        exit('<br><b style="color:red">Upload Gagal! Gunakan Format Ms.Excell yang telah di sediakan</b>');   //pesan error tipe file tidak tepat
		}

		return $sukses."-".$edit."-".$gagal;
	}	
	function addDataOwner($id,$nama,$hp1,$hp2,$email)
	{
		$cek=$this->db->query("SELECT * from data_owner where nama='".$nama."' ")->num_rows();
		if($cek){ return false; }
		
		$data=array(
		"id_owner"=>$id,
		"nama"=>$nama,
		"hp"=>$hp1,
		"hp2"=>$hp2,
		"email"=>$email,
		);
		$this->db->insert("data_owner",$data);
		return $id;
	}
	function importData()
	{
	$sukses=0;$gagal=0;$edit=0;
		 $file   = explode('.',$_FILES['userfile']['name']);
		$length = count($file);
		if($file[$length -1] == 'xlsx' || $file[$length -1] == 'xls'){//jagain barangkali uploadnya selain file excel <span class="wp-smiley wp-emoji wp-emoji-smile" title=":-)">:-)</span>
        $tmp    = $_FILES['userfile']['tmp_name'];//Baca dari tmp folder jadi file ga perlu jadi sampah di server :-p
		      
				 // load excel
			    $file = $_FILES['userfile']['tmp_name'];
			    $load = PHPExcel_IOFactory::load($file);
                $sheets = $load->getActiveSheet()->toArray(null,true,true,true);
				$i=1;
				foreach ($sheets as $sheet) {
				if ($i > 1) {						
						$hp=str_replace("'","",$sheet[1]);
						$hp=str_replace("`","",$hp);
						$hp=str_replace("+62","0",$hp);
									
							//get form
															
						$sql=array(
						"nama"=>$sheet[0],
						"hp"=>$hp=$this->db->escape_str($hp),
						"email"=>$sheet[2],
						"alamat"=>$sheet[3],
						);		
				       
						$cek=$this->cekOwner($hp);
					if($cek==0)//jika data kontak tidak ada maka tambahkan
					{  
							$this->db->insert("data_owner",$sql); $sukses++;
					}else{ //jika data kontak ada maka edit yg ada
						$sql=array(
						"nama"=>$sheet[0],
						"hp"=>$hp=$this->db->escape_str($hp),
						"email"=>$sheet[2],
						"alamat"=>$sheet[3],
						);						
							$this->db->where("hp",$hp);
							$this->db->update("data_owner",$sql);
							$edit++;
					};
					   
				}
				$i++;
                }
               
		}else{
        exit('<br><b style="color:red">Upload Gagal! Gunakan Format Ms.Excell yang telah di sediakan</b>');   //pesan error tipe file tidak tepat
		}

		return $sukses."-".$edit."-".$gagal;
	}
	function go_cek_duplikat_owner_ada()
	{
		$where="";
		$hp1=$this->input->post("hp1");
		if($hp1){
		$where.=" OR hp='".$hp1."'";
		}
		$hp2=$this->input->post("hp2");
		if($hp2){
		$where.=" OR hp2='".$hp2."'";
		}
		$email=$this->input->post("email");
		if($email){
		$where.=" OR email='".$email."'";
		}
		$alamat=$this->input->post("alamat");
		if($alamat){
		$where.=" OR alamat='".$alamat."'";
		}
		$id_own=$this->input->post("id_own");
		if($where!=""){
			$where = substr($where,3);
		return $this->db->query("SELECT * from data_owner where   ".$where."  AND id_owner!='".$id_own."' ")->num_rows();
		}return 0;
	}
	function go_cek_duplikat_owner()
	{
		$where="";
		$hp1=$this->input->post("hp1");
		if($hp1){
		$where.=" OR hp='".$hp1."'";
		}
		$hp2=$this->input->post("hp2");
		if($hp2){
		$where.=" OR hp2='".$hp2."'";
		}
		$email=$this->input->post("email");
		if($email){
		$where.=" OR email='".$email."'";
		}
		$alamat=$this->input->post("alamat");
		if($alamat){
		$where.=" OR alamat='".$alamat."'";
		}
		if($where!=""){
			$where = substr($where,3);
		return $this->db->query("SELECT * from data_owner where ".$where."  ")->num_rows();
		}else{return 0;};
	}
	
}