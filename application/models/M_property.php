<?php

class M_property extends CI_Model  {
    
		
	function __construct()
    {
        parent::__construct();
    }
	function kodeProperty()
	{
			  $carikode = $this->db->query("SELECT max(id_prop) as id_prop from data_property")->row();
			  $datakode =$carikode->id_prop;//$carikode->kode;
			 if ($datakode) {
		  	    $kode = (int) $datakode;
		   		return	$newID = sprintf("%05s", $kode+1);
		   	  } else {
				return "00001";
					 }
	}
	function kodeAgen()
	{
			  $carikode = $this->db->query("SELECT max(id_agen) as id_agen from data_agen")->row();
			  $datakode =$carikode->id_agen;//$carikode->kode;
			 if ($datakode) {
		  	    $kode = (int) $datakode;
		   		return	$newID = sprintf("%02s", $kode+1);
		   	  } else {
				return "01";
					 }
	}
	
	function getKodeList($agen)
	{
	return $agen.$this->kodeProperty();
	}
	function analisisKelengkapan()
	{
	$gambar1=isset($_FILES['gambar1']['type']);
	$gambar2=isset($_FILES['gambar2']['type']);
	$gambar3=isset($_FILES['gambar3']['type']);
	$gambar4=isset($_FILES['gambar4']['type']);
	$gambar5=isset($_FILES['gambar5']['type']);
	$jml=0;
	if($gambar1)
	{
		$jml++;
	}if($gambar2)
	{
		$jml++;
	}if($gambar3)
	{
		$jml++;
	}if($gambar4)
	{
		$jml++;
	}if($gambar5)
	{
		$jml++;
	}
	$total=$jml;	
	if($total==0)
	{
		return 0;
	}else{
		return 1;
	}
	
	}
	function addOwner($id,$nama=null)
	{
		if($id)
		{
			$data=array(
			'nama'=>$this->input->post("nama_own"),
			'hp'=>$this->input->post("hp1_own"),
			'hp2'=>$this->input->post("hp2_own"),
			'email'=>$this->input->post("email_own"),
			'alamat'=>$this->input->post("alamat_own"),
			'jk'=>$this->input->post("jk_own"),
			);
			$this->db->where("id_owner",$id);
			$this->db->update("data_owner",$data);
			return $id;
		}else{
		if($nama)
		{			
			$return=$this->db->query("SELECT MAX(CONVERT(id_owner,SIGNED)+1) AS maxs FROM data_owner")->row();
			$return=isset($return->maxs)?($return->maxs):"";
			$data=array(
			'id_owner'=>$return,
			'nama'=>$this->input->get_post("nama_own"),
			'hp'=>$this->input->get_post("hp1_own"),
			'hp2'=>$this->input->get_post("hp2_own"),
			'email'=>$this->input->get_post("email_own"),
			'alamat'=>$this->input->get_post("alamat_own"),
			'jk'=>$this->input->get_post("jk_own"),
			);
			$this->db->insert("data_owner",$data);
			return $return;
		}
		}
	}
	function cariKode($kode)
	{
		$return=$this->db->query("SELECT * FROM data_property WHERE kode_prop='".$kode."'")->num_rows();	
		if($return)
		{
			  $carikode = $this->db->query("SHOW TABLE STATUS LIKE 'data_property'")->row();
			  $datakode =isset($carikode->Auto_increment)?($carikode->Auto_increment):"1";//$carikode->kode;
		  	    $kode = (int) $datakode;
		   		$newID = sprintf("%03s", $kode);
				$thn=substr(date('Y'),2,2);
				$bln=sprintf("%02s",date('m'));
				$tgl=sprintf("%02s",date('d'));
		   	 return  $tgl.$bln.$thn."-".$newID;	
		}else{
			return $kode;
		}
	}
	function insert()
	{
		$agen=$this->input->post("agen");
		$jenis=$this->input->post("type_pro");
		$kode=$this->input->post("kode");
		$kode=$this->cariKode($kode);
		$uploads1=$this->uploadGambar($kode,"upload1");
		$gambars1=$this->reff->watermark_image($kode);
		$array=array(
		"id_created"=>$this->session->userdata("id"),
		"kode_prop"=>$kode,
		"jenis_prop"=>$jp=$this->input->post("type_pro"),
		"jenis_listing"=>$this->input->post("jenis_list"),
		"type_jual"=>$this->input->post("type_list"),
		"desc"=>$des=$this->input->post("desc"),
		"id_prov"=>$this->input->post("provinsi"),
		"id_kab"=>$this->input->post("kabupaten"),
		"id_owner"=>$ido=$this->addOwner($this->input->post("id_own"),$this->input->post("nama_own")),
		"komplek"=>$this->input->post("nama_komplek"),
		"nama_area"=>$this->input->post("area"),
		"lat_area"=>$this->input->post("lat_area"),
		"long_area"=>$this->input->post("long_area"),
		"alamat_detail"=>$k=$this->input->post("alamat_detail"),
		"lat_detail"=>$this->input->post("lat"),
		"long_detail"=>$this->input->post("long"),
		"luas_tanah"=>$this->input->post("luas_tanah"),
		"luas_bangunan"=>$this->input->post("luas_bangunan"),
		"tahun_dibangun"=>$this->input->post("tahun_dibangun"),
		"harga"=>str_replace(".","",$this->input->post("harga")),
		"kamar_tidur"=>$this->input->post("kamar_tidur"),
		"kamar_mandi"=>$this->input->post("kamar_mandi"),
		"kamar_tidur_p"=>$this->input->post("kamar_tidur_pembantu"),
		"kamar_mandi_p"=>$this->input->post("kamar_mandi_pembantu"),
		"jml_lantai"=>$this->input->post("jumlah_lantai"),
		"jml_garasi"=>$this->input->post("garasi"),
		"jml_carports"=>$this->input->post("carports"),
		"daya_listrik"=>$this->input->post("daya_listrik"),
		"hadap"=>$this->input->post("hadap"),
		"type_sewa"=>$this->input->post("type_sewa"),
		"jenis_sertifikat"=>$this->input->post("sertifikat"),
		"kelengkapan"=>$this->analisisKelengkapan(),
		"agen"=>$ag=$this->input->post("agen"),
		"keterangan"=>$this->input->post("keterangan"),
		"gambar1"=>$this->uploadGambar($kode,"upload1"),
		//"gambar1"=>$gambars1,
		"gambar2"=>$this->uploadGambar($kode,"upload2"),
		"gambar3"=>$this->uploadGambar($kode,"upload3"),
		"gambar4"=>$this->uploadGambar($kode,"upload4"),
		"gambar5"=>$this->uploadGambar($kode,"upload5"),
		"desain"=>$this->uploadGambar($kode,"upload6"),
		"gambar_utama"=>$this->input->post("set"),
		"fee_persen"=>str_replace(",",".",$this->input->post("fee_persen")),
		"fee_up"=>str_replace(".","",$this->input->post("fee_up")),
		"media_promosi"=>$this->input->post("media_iklan").",".$this->input->post("media_spanduk"),
		"tgl_masuk_listing"=>$tml=$this->tanggal->eng_($this->input->post("tgl_masuk_listing"),"-"),
		"tgl_expired"=>$this->tanggal->eng_($this->input->post("tgl_expired"),"-"),
		"air"=>$this->input->post("air"),
		"furniture"=>$this->input->post("furniture"),
		"area_listing"=>$area=$this->input->post("area_listing"),
		"tgl_expired"=>$this->tanggal->eng_($this->input->post("tgl_masuk_listing"),"-"),
		"harga_tanah"=>$hrg=str_replace(".","",$this->input->post("harga_tanah")),
		);
	if($k == '' && $this->input->post("harga") == ''){
		return "kurangdetail";
	}else{
		$cek=$this->_cekListingan($tml,$hrg,$area,$jp,$ag,$ido,$k);
		if($cek)
		{
			return false;
		}else{
				return	$this->db->insert("data_property",$array);
		}
	}

	}
	
	function _cekListingan($tml,$des,$area,$jp,$ag,$ido,$k)
	{
	//    $this->db->where("id_owner",$ido);
	//    $this->db->where("agen",$ag);
	 //   $this->db->where("jenis_prop",$jp);
	//    $this->db->where("tgl_masuk_listing",$tml);
	//    $this->db->where("area_listing",$area);
	      $this->db->where("alamat_detail",$k);
	 //   $this->db->where("desc",$des);
	 return   $this->db->get("data_property")->num_rows();
	}
	
	function update()
	{	$id_prop=$this->input->post("id_property");
		$agen=$this->input->post("agen");
		$jenis=$this->input->post("type_pro");
		$array=array(
		"id_modified"=>$this->session->userdata("id"),
		"updated_time"=>date("Y-m-d H:i:s"),
		"kode_prop"=>$this->input->post("kode"),
		"jenis_prop"=>$this->input->post("type_pro"),
		"jenis_listing"=>$this->input->post("jenis_list"),
		"type_jual"=>$this->input->post("type_list"),
		"desc"=>$this->input->post("desc"),
		"id_prov"=>$this->input->post("provinsi"),
		"id_kab"=>$this->input->post("kabupaten"),
		"id_owner"=>$this->addOwner($this->input->post("id_own"),$this->input->post("nama_own")),
		"komplek"=>$this->input->post("nama_komplek"),
		"nama_area"=>$this->input->post("area"),
		"lat_area"=>$this->input->post("lat_area"),
		"long_area"=>$this->input->post("long_area"),
		"alamat_detail"=>$this->input->post("alamat_detail"),
		"lat_detail"=>$this->input->post("lat"),
		"long_detail"=>$this->input->post("long"),
		"luas_tanah"=>$this->input->post("luas_tanah"),
		"luas_bangunan"=>$this->input->post("luas_bangunan"),
		"tahun_dibangun"=>$this->input->post("tahun_dibangun"),
		"harga"=>str_replace(".","",$this->input->post("harga")),
		"kamar_tidur"=>$this->input->post("kamar_tidur"),
		"kamar_mandi"=>$this->input->post("kamar_mandi"),
		"kamar_tidur_p"=>$this->input->post("kamar_tidur_pembantu"),
		"kamar_mandi_p"=>$this->input->post("kamar_mandi_pembantu"),
		"jml_lantai"=>$this->input->post("jumlah_lantai"),
		"jml_garasi"=>$this->input->post("garasi"),
		"jml_carports"=>$this->input->post("carports"),
		"daya_listrik"=>$this->input->post("daya_listrik"),
		"hadap"=>$this->input->post("hadap"),
		"type_sewa"=>$this->input->post("type_sewa"),
		"jenis_sertifikat"=>$this->input->post("sertifikat"),
		"kelengkapan"=>$this->analisisKelengkapan(),
		"agen"=>$this->input->post("agen"),
		"keterangan"=>$this->input->post("keterangan"),
		"gambar1"=>$this->uploadGambarUpdate($id_prop,"upload1"),
		"gambar2"=>$this->uploadGambarUpdate($id_prop,"upload2"),
		"gambar3"=>$this->uploadGambarUpdate($id_prop,"upload3"),
		"gambar4"=>$this->uploadGambarUpdate($id_prop,"upload4"),
		"gambar5"=>$this->uploadGambarUpdate($id_prop,"upload5"),
		"desain"=>$this->uploadGambar($id_prop,"upload6"),
		"gambar_utama"=>$this->input->post("set"),
		"fee_persen"=>str_replace(",",".",$this->input->post("fee_persen")),
		"fee_up"=>str_replace(".","",$this->input->post("fee_up")),
		"media_promosi"=>$this->input->post("media_iklan").",".$this->input->post("media_spanduk"),
		"tgl_masuk_listing"=>$this->tanggal->eng_($this->input->post("tgl_masuk_listing"),"-"),
		"tgl_expired"=>$this->tanggal->eng_($this->input->post("tgl_masuk_listing"),"-"),
		"air"=>$this->input->post("air"),
		"furniture"=>$this->input->post("furniture"),
		"area_listing"=>$this->input->post("area_listing"),
		"harga_tanah"=>str_replace(".","",$this->input->post("harga_tanah")),
		);
		if($this->input->post("alamat_detail") == '' && $this->input->post("harga") == ''){
			return "kurangdetailnya";
		}else{
		$this->db->where("id_prop",$id_prop);
		return	$this->db->update("data_property",$array);
		}
	}
	
	
	
	function HapusAll()
	{
		$hapus=$this->input->post("hapus");
		foreach($hapus as $id)
		{
		 $this->hapus($id);
		}	return true;
	}
	function hapus($id)
	{
		$this->hapusGambar($id);
		$this->db->where("id_prop",$id);
		$this->db->delete("data_property");
	}
	function hapusF($id)
	{
		$this->hapusGambarF($id);
		$array=array(
		"desain"=>"",
		"desaintv"=>"",
	 	);
		$this->db->where("id_prop",$id);
		$this->db->update("data_property",$array);
	}
	function hapusCeleb($id)
	{
		$this->hapusGambarCeleb($id);
		$array=array(
		"id_celeb"=>"",
	 	);
		$this->db->where("id_celeb",$id);
		$this->db->update("data_selling",$array);
		$this->db->where("id_celeb",$id);
		$this->db->delete("tr_celeb");
	}
	function hapusCelebSI($id)
	{
		$this->hapusGambarCelebSI($id);
		$array=array(
		"id_celeb"=>"",
	 	);
		$this->db->where("id_celeb",$id);
		$this->db->update("data_selling",$array);
		$this->db->where("id_celeb",$id);
		$this->db->delete("tr_celeb");
	}
	function HapusAllSelling()
	{
		$hapus=$this->input->post("hapus");
		foreach($hapus as $id)
		{
		 $this->hapusSelling($id);
		}	return true;
	}
	function hapusSelling($id)
	{
		$this->hapusZip($id);
		/*update data listing */
		$this->db->where("id",$id);
		$data=$this->db->get("data_selling")->row();
		$kode = $data->kode_listing;
		$this->db->where("kode_prop",$kode);
		$array=array(
		"status"=>"0",
	 	);
		$this->db->update("data_property",$array);
		
		$this->db->where("id",$id);
		$this->db->delete("data_selling");
	}
	function hapusGambar($id)
	{
		for($i=1;$i<=5;$i++)
		{
			$daprof=$this->geFieldGambar($id,"gambar".$i);
			if($daprof)
			 {
				 $path = "file_upload/img/".$daprof;
				 if (file_exists($path)) {
					unlink($path);
				 }
			 }
		}
	}
	function hapusGambarF($id)
	{
		$this->db->where("id_prop",$id);
		$query=$this->db->get("data_property");
		foreach ($query->result() as $row)
		{
			$desain = $row->desain;
			$desaintv = $row->desaintv;
		}
		$path = "file_upload/img/".$desain;
		if (file_exists($path)) {
			unlink($path);
		}
		$path2 = "file_upload/img/".$desaintv;
		if (file_exists($path2)) {
			unlink($path2);
		}
	}
	function hapusGambarCeleb($id)
	{
		$this->db->where("id_celeb",$id);
		$query=$this->db->get("tr_celeb");
		foreach ($query->result() as $row)
		{
			$gambarsosmed = $row->gambar_sosmed;
			$gambartv = $row->gambar_tv;
			$gambarinternal = $row->gambar_internal;
			$gambartv2 = $row->gambar_tv2;
			$gambarinternal2 = $row->gambar_internal2;
		}
		$path = "file_upload/img/".$gambarsosmed;
		if (file_exists($path)) {
			unlink($path);
		}
		$path2 = "file_upload/img/".$gambartv;
		if (file_exists($path2)) {
			unlink($path2);
		}
		$path3 = "file_upload/img/".$gambartv2;
		if (file_exists($path3)) {
			unlink($path3);
		}
		$path4 = "file_upload/img/".$gambarinternal;
		if (file_exists($path4)) {
			unlink($path4);
		}
		$path5 = "file_upload/img/".$gambarinternal2;
		if (file_exists($path5)) {
			unlink($path5);
		}
	}
	function hapusGambarCelebSI($id)
	{
		$this->db->where("id_celeb",$id);
		$query=$this->db->get("tr_celeb");
		foreach ($query->result() as $row)
		{
			$gambarsosmed = $row->gambar_sosmed;
			$gambarinternal = $row->gambar_internal;
			$gambarinternal2 = $row->gambar_internal2;
		}
		$path = "file_upload/img/".$gambarsosmed;
		if (file_exists($path)) {
			unlink($path);
		}
		$path2 = "file_upload/img/".$gambarinternal;
		if (file_exists($path2)) {
			unlink($path2);
		}
		$path3 = "file_upload/img/".$gambarinternal2;
		if (file_exists($path3)) {
			unlink($path3);
		}
	}
	function hapusZip($id)
	{
		for($i=1;$i<=5;$i++)
		{
			 $path = "file_upload/dok/".$id.".zip";
				 if (file_exists($path)) {
					unlink($path);
				 }
		}
	}
	function uploadGambarUpdate($id_prop,$form)
	{
		if(isset($_FILES[$form]['type']))
		{
		return	$this->upload_imgUpdate($id_prop,$form);
		}else{
			return  $daprof=$this->geFieldGambar($id_prop,$form);
		}
	}
	function geFieldGambar($id_prop,$form)
	{
		$gambar=str_replace("upload","gambar",$form);
		$this->db->where("id_prop",$id_prop);
	$data=$this->db->get("data_property")->row();
	return isset($data->$gambar)?($data->$gambar):"";
	}
	public function upload_imgUpdate($id_prop,$form)
	{	//$this->m_konfig->log("admin","Upload photo");
		///
		  $nama=date("YmdHis");
		  $lokasi_file = $_FILES[$form]['tmp_name'];
		  $tipe_file   = $_FILES[$form]['type'];
		  $nama_file   = $_FILES[$form]['name'];
		  
		  
		  $daprof=$this->geFieldGambar($id_prop,$form);
			if($daprof)
			 {
				 $path = "file_upload/img/".$daprof;
				 if (file_exists($path)) {
					unlink($path);
				 }
			 }
		  
		  
			//$jenis=explode(".",$nama_file);
			//$nama_file=$jenis[0];
			$nama_file=str_replace(" ","_",$nama_file);
		
			// $jenis="jpg";
			$nama=str_replace("/","",$nama.$nama_file);
			 $target_path = "file_upload/img/".$nama;
			 //
	//	  }
		  //
		if (!empty($lokasi_file)) {
		move_uploaded_file($lokasi_file,$target_path);
		
			$config['source_image'] = $target_path;
			$config['wm_type'] = 'overlay';
			$config['wm_overlay_path'] = 'file_upload/image/water.png';
			$config['quality'] = 50;
			$config['wm_vrt_alignment'] = 'bottom';
			$config['wm_hor_alignment'] = 'left';
			$this->load->library('image_lib', $config);
			
			$this->image_lib->initialize($config);
			$this->image_lib->watermark(); 
		
		//if($jenis=="png"){
		//$this->konversi->UploadImageResize($target_path,$jenis,200);
		}
	//	$this->reff->UploadImageResize($target_path,"jpg",800);
		//$this->reff->watermark_image($target_path);
		return $nama;
		}
		
		
		
		
		
		function uploadGambar($kode,$form)
	{
		if(isset($_FILES[$form]['type']))
		{
		return	$this->upload_img($kode,$form);
		}
	}
	public function upload_img($kode,$form)
	{	//$this->m_konfig->log("admin","Upload photo");
		///
			$nama=date("YmdHis");
		  $lokasi_file = $_FILES[$form]['tmp_name'];
		  $tipe_file   = $_FILES[$form]['type'];
		  $nama_file   = $_FILES[$form]['name'];
		  
		  $watermarksrc = base_url()."file_upload/image/water.png";
		  //if($tipe_file)
		  //{
		//  $daprof=$this->getGambarkode($kode);
		//	if($daprof!="")
		//	 {
		//		 $path = "file_upload/barang/".$daprof;
		//		 if (file_exists($path)) {
		//			unlink($path);
		//		 }
		//	 }
		  
		  
			//$jenis=explode(".",$nama_file);
			$nama_file=str_replace(" ","_",$nama_file);
			// $jenis="jpg";
			$nama=str_replace("/","",$kode.$nama.$nama_file);
			 $target_path = "file_upload/img/".$nama;
			 //
	//	  }
		  //
		if (!empty($lokasi_file)) {
			move_uploaded_file($lokasi_file,$target_path);
			
			$config['source_image'] = $target_path;
			$config['wm_type'] = 'overlay';
			$config['wm_overlay_path'] = 'file_upload/image/water.png';
			$config['quality'] = 50;
			$config['wm_vrt_alignment'] = 'bottom';
			$config['wm_hor_alignment'] = 'left';
			$this->load->library('image_lib', $config);
			
			$this->image_lib->initialize($config);
			$this->image_lib->watermark(); 
		}
	//	$this->reff->UploadImageResize($target_path,"jpg",800);
		return $nama;
	}
		
		
	function getDataProp()
	{
	return	$this->db->get("data_property")->result();
	}
	function get_one($id)
	{
		$this->db->where("id_prop",$id);
	return	$this->db->get("data_property");
	}
  
  /*------------------------------------------------------------------------------*/
		function get_dataProperty()
	{
		 $query = $this->_get_dataProperty();
        if ($_POST['length'] != -1)
            $query .= " limit " . $_POST['start'] . "," . $_POST['length'];
        return $this->db->query($query)->result();
 
	}
	  public function counts() {
        $query = $this->_get_dataProperty();
        return $this->db->query($query)->num_rows();
    }
	function _get_dataProperty()
	{
	$dan="";
	$provinsi=$this->input->get("provinsi");
	if($provinsi){
	$dan.=" AND id_prov=".$provinsi;
	}
	$kabupaten=$this->input->get("kabupaten");
	if($kabupaten){
	$dan.=" AND id_kab=".$kabupaten;
	}
	$area=$this->input->get("area");
	if($area){
	$dan.=" AND (area_listing LIKE '%".$area."%')";
	}
	//$lat_area=$this->input->get("lat_area");
//	if($lat_area){
	//$dan.=" AND lat_area=".$lat_area;
//	}
//$long_area=$this->input->get("long_area");
///if($long_area){
//$dan.=" AND long_area=".$long_area;
//}
	$jenis_pro=$this->input->get("jenis_pro");
	if($jenis_pro){
	$dan.=" AND jenis_prop=".$jenis_pro;
	}$type_pro=$this->input->get("type_pro");
	
	if($type_pro){
	$dan.=" AND type_jual='".$type_pro."'";
	}
	
	$kamar_tidur=$this->input->get("kamar_tidur");
	if($kamar_tidur){
	$dan.=" AND kamar_tidur=".$kamar_tidur;
	}
	$kamar_mandi=$this->input->get("kamar_mandi");
	if($kamar_mandi){
	$dan.=" AND kamar_mandi=".$kamar_mandi;
	}
	$garasi=$this->input->get("garasi");
	if($garasi){
	$dan.=" AND jml_garasi=".$garasi;
	}
	$daya_listrik=$this->input->get("daya_listrik");
	if($daya_listrik){
	$dan.=" AND daya_listrik=".$daya_listrik;
	}
	
	$harga_min=$this->input->get("harga_min");
	if($harga_min){
	$dan.=" AND harga>='".str_replace(".","",$harga_min)."'";
	}
	$harga_max=$this->input->get("harga_max");
	if($harga_max){
	$dan.=" AND harga<='".str_replace(".","",$harga_max)."'";
	}
	
	
	$sertifikat=$this->input->get("sertifikat");
	if($sertifikat){
	$dan.=" AND jenis_sertifikat='".$sertifikat."'";
	}
	$agen=$this->input->get("agen");
	if($agen){
	$dan.=" AND agen='".$agen."'";
	}
	$type_sewa=$this->input->get("type_sewa");
	if($type_sewa){
	$dan.=" AND type_sewa=".$type_sewa;
	}
	$kelengkapan=$this->input->get("kelengkapan");
	if($kelengkapan){
		if($kelengkapan=='1')
		{
		//	$dan.=" AND (gambar1  IS NOT NULL AND gambar2  IS NOT NULL AND gambar3  IS NOT NULL AND gambar4  IS NOT NULL AND gambar5  IS NOT NULL)    ";
		$dan.=" AND (gambar1 is not null and gambar1!='' )    ";
		}elseif($kelengkapan=='2')
		{
		//	$dan.=" AND (gambar1 IS NULL AND gambar2 IS NULL AND gambar3 IS NULL AND gambar4 IS NULL AND gambar5 IS NULL)"; //OR (gambar1='' AND gambar2='' AND gambar3='' AND gambar4='' AND gambar5='')  ";
	//	$dan.="AND (CHAR_LENGTH(gambar1)<1 AND CHAR_LENGTH(gambar2)<1 AND CHAR_LENGTH(gambar3)<1 AND CHAR_LENGTH(gambar4)<1 AND CHAR_LENGTH(gambar5)<1  )";
		    	$dan.=" AND (CHAR_LENGTH(gambar1)<1 or gambar1 is null  )    ";
		}elseif($kelengkapan=='3')
		{
			$dan.=" AND desain!=''  ";
		}elseif($kelengkapan=='4')
		{
				$dan.=" AND (desain IS NULL or desain='')  ";
		}
	
	}
	$status_penjualan=$this->input->get("status_penjualan");
	if($status_penjualan=='0'){
	$dan.=" AND status='0'";
	}elseif($status_penjualan=='1'){
	$dan.=" AND status='1'";
	}elseif($status_penjualan=='2'){
		$dan.=" AND status='2'";
	}
	
        if($this->session->userdata("id")==64){ // Ajeng
			$query = "SELECT * FROM data_property WHERE 1=1 AND agen IN ('BREA/005/II/2017', 'BREA/012/VIII/2017') $dan ";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				kode_prop LIKE '%" . $searchkey . "%' or 
				data_property.desc  LIKE '%" . $searchkey . "%' or
				keterangan LIKE '%" . $searchkey . "%' or
				alamat_detail LIKE '%" . $searchkey . "%' or 
				nama_area LIKE '%" . $searchkey . "%' or
				area_listing LIKE '%" . $searchkey . "%' or
				agen LIKE '%" . $searchkey . "%' or
				komplek LIKE '%" . $searchkey . "%'  
				) ";
			}
		}elseif($this->session->userdata("id")==151){ // Rafha
			$query = "SELECT * FROM data_property WHERE 1=1 AND agen='BREA/039/I/2018' $dan ";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				kode_prop LIKE '%" . $searchkey . "%' or 
				data_property.desc  LIKE '%" . $searchkey . "%' or
				keterangan LIKE '%" . $searchkey . "%' or
				alamat_detail LIKE '%" . $searchkey . "%' or 
				nama_area LIKE '%" . $searchkey . "%' or
				area_listing LIKE '%" . $searchkey . "%' or
				agen LIKE '%" . $searchkey . "%' or
				komplek LIKE '%" . $searchkey . "%'  
				) ";
			}
		}elseif($this->session->userdata("id")==133){ // Kiki
			$query = "SELECT * FROM data_property WHERE 1=1 AND agen IN ('BREA/046/II/2018') $dan ";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				kode_prop LIKE '%" . $searchkey . "%' or 
				data_property.desc  LIKE '%" . $searchkey . "%' or
				keterangan LIKE '%" . $searchkey . "%' or
				alamat_detail LIKE '%" . $searchkey . "%' or 
				nama_area LIKE '%" . $searchkey . "%' or
				area_listing LIKE '%" . $searchkey . "%' or
				agen LIKE '%" . $searchkey . "%' or
				komplek LIKE '%" . $searchkey . "%'  
				) ";
			}
		}elseif($this->session->userdata("id")==146){ // yudi
			$query = "SELECT * FROM data_property WHERE 1=1 AND agen IN ('BREA/032/XI/2017') $dan ";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				kode_prop LIKE '%" . $searchkey . "%' or 
				data_property.desc  LIKE '%" . $searchkey . "%' or
				keterangan LIKE '%" . $searchkey . "%' or
				alamat_detail LIKE '%" . $searchkey . "%' or 
				nama_area LIKE '%" . $searchkey . "%' or
				area_listing LIKE '%" . $searchkey . "%' or
				agen LIKE '%" . $searchkey . "%' or
				komplek LIKE '%" . $searchkey . "%'  
				) ";
			}
		}elseif($this->session->userdata("id")==152){ // vivi
			$query = "SELECT * FROM data_property WHERE 1=1 AND agen NOT IN ('BREA/032/XI/2017', 'BREA/001/I/2017', 'BREA/005/II/2017', 'BREA/012/VIII/2017', 'BREA/066/VII/2018', 'BREA/046/II/2018', 'BREA/010/I/2017') $dan ";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				kode_prop LIKE '%" . $searchkey . "%' or 
				data_property.desc  LIKE '%" . $searchkey . "%' or
				keterangan LIKE '%" . $searchkey . "%' or
				alamat_detail LIKE '%" . $searchkey . "%' or 
				nama_area LIKE '%" . $searchkey . "%' or
				area_listing LIKE '%" . $searchkey . "%' or
				agen LIKE '%" . $searchkey . "%' or
				komplek LIKE '%" . $searchkey . "%'  
				) ";
			}
		}elseif($this->session->userdata("id")==161){ // Yema
			$query = "SELECT * FROM data_property WHERE 1=1 AND agen IN ('BREA/001/I/2017') $dan ";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				kode_prop LIKE '%" . $searchkey . "%' or 
				data_property.desc  LIKE '%" . $searchkey . "%' or
				keterangan LIKE '%" . $searchkey . "%' or
				alamat_detail LIKE '%" . $searchkey . "%' or 
				nama_area LIKE '%" . $searchkey . "%' or
				area_listing LIKE '%" . $searchkey . "%' or
				agen LIKE '%" . $searchkey . "%' or
				komplek LIKE '%" . $searchkey . "%'  
				) ";
			}
		}elseif($this->session->userdata("id")==162){ // Fransiskus
			$query = "SELECT * FROM data_property WHERE 1=1 AND agen IN ('BREA/046/II/2018', 'BREA/010/I/2017') $dan ";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				kode_prop LIKE '%" . $searchkey . "%' or 
				data_property.desc  LIKE '%" . $searchkey . "%' or
				keterangan LIKE '%" . $searchkey . "%' or
				alamat_detail LIKE '%" . $searchkey . "%' or 
				nama_area LIKE '%" . $searchkey . "%' or
				area_listing LIKE '%" . $searchkey . "%' or
				agen LIKE '%" . $searchkey . "%' or
				komplek LIKE '%" . $searchkey . "%'  
				) ";
			}
		}elseif($this->session->userdata("id")==165){ // Rena
			$query = "SELECT * FROM data_property WHERE 1=1 AND agen IN ('BREA/005/II/2017', 'BREA/012/VIII/2017', 'BREA/066/VII/2018') $dan ";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				kode_prop LIKE '%" . $searchkey . "%' or 
				data_property.desc  LIKE '%" . $searchkey . "%' or
				keterangan LIKE '%" . $searchkey . "%' or
				alamat_detail LIKE '%" . $searchkey . "%' or 
				nama_area LIKE '%" . $searchkey . "%' or
				area_listing LIKE '%" . $searchkey . "%' or
				agen LIKE '%" . $searchkey . "%' or
				komplek LIKE '%" . $searchkey . "%'  
				) ";
			}
		}else{
			$query = "SELECT * FROM data_property WHERE 1=1 $dan ";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				kode_prop LIKE '%" . $searchkey . "%' or 
				data_property.desc  LIKE '%" . $searchkey . "%' or
				keterangan LIKE '%" . $searchkey . "%' or
				alamat_detail LIKE '%" . $searchkey . "%' or 
				nama_area LIKE '%" . $searchkey . "%' or
				area_listing LIKE '%" . $searchkey . "%' or
				agen LIKE '%" . $searchkey . "%' or
				komplek LIKE '%" . $searchkey . "%'  
				) ";
			}
		}

        $column = array('');
        $i = 0;
        foreach ($column as $item) {
            $column[$i] = $item;
        }

      /*  if (isset($_POST['order'])) {
		//	$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            $query .= " order by " . $column[$_POST['order']['0']['column']] . " " . $_POST['order']['0']['dir'];
        } else if (isset($order)) {
            $order = $order;
			//	$this->db->order_by(key($order), $order[key($order)]);
			       $query .= " order by nama ASC";
        }*/
		$query.=" order by id_prop ASC" ;
        return $query;
	}
	
	/*-----------------------------------------------------------------------*/
	
	/*------------------------------------------------------------------------------*/
		function ajax_selling()
	{
		 $query = $this->_ajax_selling();
        if ($_POST['length'] != -1)
            $query .= " limit " . $_POST['start'] . "," . $_POST['length'];
        return $this->db->query($query)->result();
 
	}
	  public function counts_ajax_selling() {
        $query = $this->_ajax_selling();
        return $this->db->query($query)->num_rows();
    }
	function _ajax_selling()
	{
	$dan="";
	$agen=$this->input->get("agen");
	if($agen){
	//$dan.=" AND selling='".$agen."' OR a.kode_agen='".$agen."'";
	$dan.=" AND (a.kode_agen='".$agen."' OR a.selling='".$agen."')";
	}
	$vendor=$this->input->get("vendor");
	if($vendor){
	$dan.=" AND e.id_owner='".$vendor."'";
	}
	$buyer=$this->input->get("buyer");
	if($buyer){
	$dan.=" AND id_pelanggan='".$buyer."'";
	}
	$tahun_sell=$this->input->get("tahun_sell");
	if($tahun_sell=='2017'){
	$dan.=" AND year(tgl_closing)='2017'";
	}elseif($tahun_sell=='2018'){
	$dan.=" AND year(tgl_closing)='2018'";
	}elseif($tahun_sell=='2016'){
	$dan.=" AND year(tgl_closing)='2016'";
	}elseif($tahun_sell=='1111'){
	$dan.="";
	}
	
        if($tahun_sell <> '' OR $tahun_sell == '1111'){
		$query = "SELECT a.*, b.alamat_detail, c.nama, f.gambar_sosmed, f.gambar_tv, f.gambar_internal, f.gambar_tv2, f.gambar_internal2, f.id_celeb FROM data_selling AS a
			LEFT JOIN data_property AS b ON a.kode_listing = b.kode_prop
			LEFT JOIN data_agen AS c ON a.kode_agen = c.kode_agen
			LEFT JOIN data_agen AS d ON a.selling = d.kode_agen
			LEFT JOIN data_owner AS e ON b.id_owner = e.id_owner
			LEFT JOIN tr_celeb AS f ON a.id_celeb = f.id_celeb
			WHERE 1=1 $dan ";
		}else{
		$query = "SELECT a.*, b.alamat_detail, c.nama, f.gambar_sosmed, f.gambar_tv, f.gambar_internal, f.gambar_tv2, f.gambar_internal2, f.id_celeb FROM data_selling AS a
				LEFT JOIN data_property AS b ON a.kode_listing = b.kode_prop
				LEFT JOIN data_agen AS c ON a.kode_agen = c.kode_agen
				LEFT JOIN data_agen AS d ON a.selling = d.kode_agen
				LEFT JOIN data_owner AS e ON b.id_owner = e.id_owner
				LEFT JOIN tr_celeb AS f ON a.id_celeb = f.id_celeb
				WHERE 1=1 AND year(tgl_closing)='2018' $dan ";
		}
        if (isset($_POST['search']['value'])) {
            $searchkey = $_POST['search']['value'];
            $query .= " AND (
			a.ket LIKE '%" . $searchkey . "%' or 
			a.kode_listing  LIKE '%" . $searchkey . "%' or
			a.kode_agen LIKE '%" . $searchkey . "%' or
			a.harga LIKE '%" . $searchkey . "%' or
			a.tgl_closing LIKE '%" . $searchkey . "%' or
			a.selling LIKE '%" . $searchkey . "%' or
			b.alamat_detail LIKE '%" . $searchkey . "%' or
			c.nama LIKE '%" . $searchkey . "%' or
			d.nama LIKE '%" . $searchkey . "%' 
			) ";
        }

        $column = array('');
        $i = 0;
        foreach ($column as $item) {
            $column[$i] = $item;
        }

      /*  if (isset($_POST['order'])) {
		//	$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            $query .= " order by " . $column[$_POST['order']['0']['column']] . " " . $_POST['order']['0']['dir'];
        } else if (isset($order)) {
            $order = $order;
			//	$this->db->order_by(key($order), $order[key($order)]);
			       $query .= " order by nama ASC";
        }*/
		$query.=" order by a.tgl_closing DESC, a.id DESC" ;
        return $query;
	}
	
	/*-----------------------------------------------------------------------*/
	
	
	
	function insertSellingJual()
	{
		if($this->input->post("options")=="1")
		{
			$selling=$this->input->post("selling1");
		}else{
			$selling=$this->input->post("selling2");
		}
		 if($this->input->post("options2")=="1")
		{
			$listing=$this->input->post("listing1");
		}else{
			$listing=$this->input->post("listing2");
		}
		
		$nominal_komisi_listing = str_replace(".","",$this->input->post("terhitung_listing"));
		$nominal_komisi_selling = str_replace(".","",$this->input->post("terhitung_selling"));
		
		$komisi_persen_listing = $this->input->post("komisi_persen_listing");
		$komisi_persen_listing_net = $this->input->post("komisi_persen_listing");
		$komisi_persen_selling = $this->input->post("komisi_persen_selling");
		$komisi_persen_selling_net = $this->input->post("komisi_persen_selling");
		
		$jabatan_listing = $this->reff->getList($listing);
		$jabatan_selling = $this->reff->getList($selling);
			
		if($this->input->post("options") != $this->input->post("options2"))
		{
			/*if ($jabatan_listing == '11'){
				$nominal_komisi_listinggross =  str_replace(".","",$this->input->post("terhitung_listing"))*0.7;
				$nominal_komisi_sellinggross =  str_replace(".","",$this->input->post("terhitung_selling"))*0.3;
				$komisi_persen_listing_net = '70';
				$komisi_persen_selling_net = '30';
			}elseif($jabatan_selling == '11'){
				$nominal_komisi_listinggross =  str_replace(".","",$this->input->post("terhitung_listing"))*0.3;
				$nominal_komisi_sellinggross =  str_replace(".","",$this->input->post("terhitung_selling"))*0.7;
				$komisi_persen_selling_net = '70';
				$komisi_persen_listing_net = '30';
			}else{
			$nominal_komisi_listinggross =  str_replace(".","",$this->input->post("terhitung_listing"))/2;
			$nominal_komisi_sellinggross =  str_replace(".","",$this->input->post("terhitung_selling"))/2;
			}*/
			$nominal_komisi_listinggross =  str_replace(".","",$this->input->post("terhitung_listing"))/2;
			$nominal_komisi_sellinggross =  str_replace(".","",$this->input->post("terhitung_selling"))/2;
			$sumber_selling =  $this->input->post("options");
		    $sumber_listing =  $this->input->post("options2");
			$harga_net = str_replace(".","",$this->input->post("harga"))/2;
		}elseif($listing != $selling && $this->input->post("options") == $this->input->post("options2")){
			
			/*if ($jabatan_listing == '11'){
				$nominal_komisi_listinggross =  str_replace(".","",$this->input->post("terhitung_listing"))*0.7;
				$nominal_komisi_sellinggross =  str_replace(".","",$this->input->post("terhitung_selling"))/2;
				$komisi_persen_listing_net = '70';
			}elseif($jabatan_selling == '11'){
				$nominal_komisi_listinggross =  str_replace(".","",$this->input->post("terhitung_listing"))/2;
				$nominal_komisi_sellinggross =  str_replace(".","",$this->input->post("terhitung_selling"))*0.7;
				$komisi_persen_selling_net = '70';
			}else{
				$nominal_komisi_listinggross =  str_replace(".","",$this->input->post("terhitung_listing"))/2;
				$nominal_komisi_sellinggross =  str_replace(".","",$this->input->post("terhitung_selling"))/2;
			}*/
			$nominal_komisi_listinggross =  str_replace(".","",$this->input->post("terhitung_listing"))/2;
			$nominal_komisi_sellinggross =  str_replace(".","",$this->input->post("terhitung_selling"))/2;
			$sumber_selling =  '3';
		    $sumber_listing =  $this->input->post("options2");
			//$harga_net = str_replace(".","",$this->input->post("harga"))/2;
			$harga_net = str_replace(".","",$this->input->post("harga"));
		}else{
			if ($jabatan_listing == '11'){
				$nominal_komisi_listing =  str_replace(".","",$this->input->post("terhitung"))*0.7;
				$nominal_komisi_selling =  str_replace(".","",$this->input->post("terhitung"))*0.3;
				
				$nominal_komisi_listinggross =  $nominal_komisi_listing;
				$nominal_komisi_sellinggross =  $nominal_komisi_selling;
				
				$komisi_persen_listing = '70';
				$komisi_persen_listing_net = '70';
				$komisi_persen_selling = '30';
				$komisi_persen_selling_net = '30';
				
			}else{
				$nominal_komisi_listinggross =  str_replace(".","",$this->input->post("terhitung_listing"));
				$nominal_komisi_sellinggross =  str_replace(".","",$this->input->post("terhitung_selling"));
			}
			$sumber_selling =  $this->input->post("options");
		    $sumber_listing =  $this->input->post("options2");
			$harga_net = str_replace(".","",$this->input->post("harga"));
		}
		 
		$data=array(
		"harga"=>str_replace(".","",$this->input->post("harga")),
		"kode_listing"=> $kodelis=$this->input->post("kode_listing"),
		"kode_agen"=> $listing,
		"id_pelanggan"=> $this->input->post("id_pelanggan"),
		//"nominal_bayar"=> $this->input->post("nominal_bayar"),
		//"tgl_pelunasan"=> $this->tanggal->eng_($this->input->post("tgl_pelunasan"),"-"),
		"tgl_closing"=> $this->tanggal->eng_($this->input->post("tgl_closing"),"-"),
		"selling"=> $selling,
		"total_komisi"=>  str_replace(".","",$this->input->post("terhitung") ),
		"komisi_persen"=>  $this->input->post("komisi_persen") ,
		"ket"=>  $this->input->post("ket") ,
		"sumber_selling"=>  $sumber_selling ,
		"sumber_listing"=>  $sumber_listing ,
		"komisi_persen_listing"=>  $komisi_persen_listing ,
		"komisi_persen_listing_net"=>  $komisi_persen_listing_net ,
		"nominal_komisi_listing"=>  $nominal_komisi_listing ,
		"nominal_komisi_listing_net"=> $nominal_komisi_listinggross,
		"komisi_persen_selling"=>  $komisi_persen_selling ,
		"komisi_persen_selling_net"=>  $komisi_persen_selling_net ,
		"nominal_komisi_selling"=>  $nominal_komisi_selling ,
		"nominal_komisi_selling_net"=> $nominal_komisi_sellinggross,
		"harga_net"=>$harga_net,
		);
			$this->updateListingStatus($kodelis,"1","");
	return	$this->db->insert("data_selling",$data);
	}
	
	function insertSellingSewa()
	{
		if($this->input->post("options")=="1")
		{
			$selling=$this->input->post("selling1");
		}else{
			$selling=$this->input->post("selling2");
		}
		 if($this->input->post("options2")=="1")
		{
			$listing=$this->input->post("listing1");
		}else{
			$listing=$this->input->post("listing2");
		}
		
		$nominal_komisi_listing = str_replace(".","",$this->input->post("terhitung_listingx"));
		$nominal_komisi_selling = str_replace(".","",$this->input->post("terhitung_sellingx"));
		
		$komisi_persen_listing = $this->input->post("komisi_persen_listingx");
		$komisi_persen_listing_net = $this->input->post("komisi_persen_listingx");
		$komisi_persen_selling = $this->input->post("komisi_persen_sellingx");
		$komisi_persen_selling_net = $this->input->post("komisi_persen_sellingx");
		
		$jabatan_listing = $this->reff->getList($listing);
		$jabatan_selling = $this->reff->getList($selling);
			
		if($this->input->post("options") != $this->input->post("options2"))
		{
			/*if ($jabatan_listing == '11'){
				$nominal_komisi_listinggross =  str_replace(".","",$this->input->post("terhitung_listingx"))*0.7;
				$nominal_komisi_sellinggross =  str_replace(".","",$this->input->post("terhitung_sellingx"))*0.3;
				$komisi_persen_listing_net = '70';
				$komisi_persen_selling_net = '30';
			}elseif($jabatan_selling == '11'){
				$nominal_komisi_listinggross =  str_replace(".","",$this->input->post("terhitung_listingx"))*0.3;
				$nominal_komisi_sellinggross =  str_replace(".","",$this->input->post("terhitung_sellingx"))*0.7;
				$komisi_persen_selling_net = '70';
				$komisi_persen_listing_net = '30';
			}else{
			$nominal_komisi_listinggross =  str_replace(".","",$this->input->post("terhitung_listingx"))/2;
			$nominal_komisi_sellinggross =  str_replace(".","",$this->input->post("terhitung_sellingx"))/2;
			}*/
			$nominal_komisi_listinggross =  str_replace(".","",$this->input->post("terhitung_listingx"))/2;
			$nominal_komisi_sellinggross =  str_replace(".","",$this->input->post("terhitung_sellingx"))/2;
			$sumber_selling =  $this->input->post("options");
		    $sumber_listing =  $this->input->post("options2");
			$harga_net = str_replace(".","",$this->input->post("hargax"))/2;
		}elseif($listing != $selling && $this->input->post("options") == $this->input->post("options2")){
			
			/*if ($jabatan_listing == '11'){
				$nominal_komisi_listinggross =  str_replace(".","",$this->input->post("terhitung_listingx"))*0.7;
				$nominal_komisi_sellinggross =  str_replace(".","",$this->input->post("terhitung_sellingx"))/2;
				$komisi_persen_listing_net = '70';
			}elseif($jabatan_selling == '11'){
				$nominal_komisi_listinggross =  str_replace(".","",$this->input->post("terhitung_listingx"))/2;
				$nominal_komisi_sellinggross =  str_replace(".","",$this->input->post("terhitung_sellingx"))*0.7;
				$komisi_persen_selling_net = '70';
			}else{
				$nominal_komisi_listinggross =  str_replace(".","",$this->input->post("terhitung_listingx"))/2;
				$nominal_komisi_sellinggross =  str_replace(".","",$this->input->post("terhitung_sellingx"))/2;
			}*/
			$nominal_komisi_listinggross =  str_replace(".","",$this->input->post("terhitung_listingx"))/2;
			$nominal_komisi_sellinggross =  str_replace(".","",$this->input->post("terhitung_sellingx"))/2;
			$sumber_selling =  '3';
		    $sumber_listing =  $this->input->post("options2");
			//$harga_net = str_replace(".","",$this->input->post("harga"))/2;
			$harga_net = str_replace(".","",$this->input->post("hargax"));
		}else{
			if ($jabatan_listing == '11'){
				$nominal_komisi_listing =  str_replace(".","",$this->input->post("terhitungx"))*0.7;
				$nominal_komisi_selling =  str_replace(".","",$this->input->post("terhitungx"))*0.3;
				
				$nominal_komisi_listinggross =  $nominal_komisi_listing;
				$nominal_komisi_sellinggross =  $nominal_komisi_selling;
				
				$komisi_persen_listing = '70';
				$komisi_persen_listing_net = '70';
				$komisi_persen_selling = '30';
				$komisi_persen_selling_net = '30';
				
			}else{
				$nominal_komisi_listinggross =  str_replace(".","",$this->input->post("terhitung_listingx"));
				$nominal_komisi_sellinggross =  str_replace(".","",$this->input->post("terhitung_sellingx"));
			}
			$sumber_selling =  $this->input->post("options");
		    $sumber_listing =  $this->input->post("options2");
			$harga_net = str_replace(".","",$this->input->post("hargax"));
		}
		 
		$data=array(
		"harga"=>str_replace(".","",$this->input->post("hargax")),
		"kode_listing"=> $kodelis=$this->input->post("kode_listing"),
		"kode_agen"=> $listing,
		"id_pelanggan"=> $this->input->post("id_pelanggan"),
		//"nominal_bayar"=> $this->input->post("nominal_bayar"),
		//"tgl_pelunasan"=> $this->tanggal->eng_($this->input->post("tgl_pelunasan"),"-"),
		"tgl_closing"=> $this->tanggal->eng_($this->input->post("tgl_closing"),"-"),
		"selling"=> $selling,
		"type_selling"=> "sewa" ,
		"total_komisi"=>  str_replace(".","",$this->input->post("terhitungx") ),
		"komisi_persen"=>  $this->input->post("komisi_persenx") ,
		"ket"=>  $this->input->post("ket") ,
		"sumber_selling"=>  $sumber_selling ,
		"sumber_listing"=>  $sumber_listing ,
		"komisi_persen_listing"=>  $komisi_persen_listing ,
		"komisi_persen_listing_net"=>  $komisi_persen_listing_net ,
		"nominal_komisi_listing"=>  $nominal_komisi_listing ,
		"nominal_komisi_listing_net"=> $nominal_komisi_listinggross,
		"komisi_persen_selling"=>  $komisi_persen_selling ,
		"komisi_persen_selling_net"=>  $komisi_persen_selling_net ,
		"nominal_komisi_selling"=>  $nominal_komisi_selling ,
		"nominal_komisi_selling_net"=> $nominal_komisi_sellinggross,
		"harga_net"=>$harga_net,
		);
		$this->db->insert("data_selling",$data);
		$this->updateListingStatus($kodelis,"1",$this->tanggal->eng_($this->input->post("tgl_jatuh_tempo"),"-"));
		$kode=$this->db->query("SELECT max(id) as mak from data_selling")->row();
		return $this->uploadFIleZip($kode->mak,"zip");
	}
	
	
	function UpdateSelling()
	{
		
	 
		
		
		if($this->input->post("options")=="1")
		{
			$selling=$this->input->post("selling1");
		}else{
			$selling=$this->input->post("selling2");
		}
		 if($this->input->post("options2")=="1")
		{
			$listing=$this->input->post("listing1");
		}else{
			$listing=$this->input->post("listing2");
		}
		
		$nominal_komisi_listing = str_replace(".","",$this->input->post("terhitung_listing"));
		$nominal_komisi_selling = str_replace(".","",$this->input->post("terhitung_selling"));
		
		$komisi_persen_listing = $this->input->post("komisi_persen_listing");
		$komisi_persen_listing_net = $this->input->post("komisi_persen_listing");
		$komisi_persen_selling = $this->input->post("komisi_persen_selling");
		$komisi_persen_selling_net = $this->input->post("komisi_persen_selling");
		
		$jabatan_listing = $this->reff->getList($listing);
		$jabatan_selling = $this->reff->getList($selling);
			
		if($this->input->post("options") != $this->input->post("options2"))
		{
			/*if ($jabatan_listing == '11'){
				$nominal_komisi_listinggross =  str_replace(".","",$this->input->post("terhitung_listing"))*0.7;
				$nominal_komisi_sellinggross =  str_replace(".","",$this->input->post("terhitung_selling"))*0.3;
				$komisi_persen_listing_net = '70';
				$komisi_persen_selling_net = '30';
			}elseif($jabatan_selling == '11'){
				$nominal_komisi_listinggross =  str_replace(".","",$this->input->post("terhitung_listing"))*0.3;
				$nominal_komisi_sellinggross =  str_replace(".","",$this->input->post("terhitung_selling"))*0.7;
				$komisi_persen_selling_net = '70';
				$komisi_persen_listing_net = '30';
			}else{
			$nominal_komisi_listinggross =  str_replace(".","",$this->input->post("terhitung_listing"))/2;
			$nominal_komisi_sellinggross =  str_replace(".","",$this->input->post("terhitung_selling"))/2;
			}*/
			$nominal_komisi_listinggross =  str_replace(".","",$this->input->post("terhitung_listing"))/2;
			$nominal_komisi_sellinggross =  str_replace(".","",$this->input->post("terhitung_selling"))/2;
			$sumber_selling =  $this->input->post("options");
		    $sumber_listing =  $this->input->post("options2");
			$harga_net = str_replace(".","",$this->input->post("harga"))/2;
		}elseif($listing != $selling && $this->input->post("options") == $this->input->post("options2")){
			
			/*if ($jabatan_listing == '11'){
				$nominal_komisi_listinggross =  str_replace(".","",$this->input->post("terhitung_listing"))*0.7;
				$nominal_komisi_sellinggross =  str_replace(".","",$this->input->post("terhitung_selling"))/2;
				$komisi_persen_listing_net = '70';
			}elseif($jabatan_selling == '11'){
				$nominal_komisi_listinggross =  str_replace(".","",$this->input->post("terhitung_listing"))/2;
				$nominal_komisi_sellinggross =  str_replace(".","",$this->input->post("terhitung_selling"))*0.7;
				$komisi_persen_selling_net = '70';
			}else{
				$nominal_komisi_listinggross =  str_replace(".","",$this->input->post("terhitung_listing"))/2;
				$nominal_komisi_sellinggross =  str_replace(".","",$this->input->post("terhitung_selling"))/2;
			}*/
			$nominal_komisi_listinggross =  str_replace(".","",$this->input->post("terhitung_listing"))/2;
			$nominal_komisi_sellinggross =  str_replace(".","",$this->input->post("terhitung_selling"))/2;
			$sumber_selling =  '3';
		    $sumber_listing =  $this->input->post("options2");
			//$harga_net = str_replace(".","",$this->input->post("harga"))/2;
			$harga_net = str_replace(".","",$this->input->post("harga"));
		}else{
			if ($jabatan_listing == '11'){
				$nominal_komisi_listing =  str_replace(".","",$this->input->post("terhitung"))*0.7;
				$nominal_komisi_selling =  str_replace(".","",$this->input->post("terhitung"))*0.3;
				
				$nominal_komisi_listinggross =  $nominal_komisi_listing;
				$nominal_komisi_sellinggross =  $nominal_komisi_selling;
				
				$komisi_persen_listing = '70';
				$komisi_persen_listing_net = '70';
				$komisi_persen_selling = '30';
				$komisi_persen_selling_net = '30';
				
			}else{
				$nominal_komisi_listinggross =  str_replace(".","",$this->input->post("terhitung_listing"));
				$nominal_komisi_sellinggross =  str_replace(".","",$this->input->post("terhitung_selling"));
			}
			$sumber_selling =  $this->input->post("options");
		    $sumber_listing =  $this->input->post("options2");
			$harga_net = str_replace(".","",$this->input->post("harga"));
		}
		 
		$data=array(
		"harga"=>str_replace(".","",$this->input->post("harga")),
		"kode_listing"=> $kodelis=$this->input->post("kode_listing"),
		"kode_agen"=> $listing,
		"id_pelanggan"=> $this->input->post("id_pelanggan"),
		//"nominal_bayar"=> $this->input->post("nominal_bayar"),
		//"tgl_pelunasan"=> $this->tanggal->eng_($this->input->post("tgl_pelunasan"),"-"),
		"tgl_closing"=> $this->tanggal->eng_($this->input->post("tgl_closing"),"-"),
		"selling"=> $selling,
		"total_komisi"=>  str_replace(".","",$this->input->post("terhitung") ),
		"komisi_persen"=>  $this->input->post("komisi_persen") ,
		"ket"=>  $this->input->post("ket") ,
		"sumber_selling"=>  $sumber_selling ,
		"sumber_listing"=>  $sumber_listing ,
		"komisi_persen_listing"=>  $komisi_persen_listing ,
		"komisi_persen_listing_net"=>  $komisi_persen_listing_net ,
		"nominal_komisi_listing"=>  $nominal_komisi_listing ,
		"nominal_komisi_listing_net"=> $nominal_komisi_listinggross,
		"komisi_persen_selling"=>  $komisi_persen_selling ,
		"komisi_persen_selling_net"=>  $komisi_persen_selling_net ,
		"nominal_komisi_selling"=>  $nominal_komisi_selling ,
		"nominal_komisi_selling_net"=> $nominal_komisi_sellinggross,
		"harga_net"=>$harga_net,
		);
		$this->db->where("id",$id=$this->input->post("id"));
		$this->db->update("data_selling",$data);
		//$this->updateListingStatus($kodelis,"1",$this->tanggal->eng_($this->input->post("tgl_jatuh_tempo"),"-"));
		//$kode=$this->db->query("SELECT max(id) as mak from data_selling")->row();
		return $this->uploadFIleZip($id,"zip");
	}
	function UpdateSellingSewa()
	{
		
	 
		
		
		if($this->input->post("options")=="1")
		{
			$selling=$this->input->post("selling1");
		}else{
			$selling=$this->input->post("selling2");
		}
		 if($this->input->post("options2")=="1")
		{
			$listing=$this->input->post("listing1");
		}else{
			$listing=$this->input->post("listing2");
		}
		
		$nominal_komisi_listing = str_replace(".","",$this->input->post("terhitung_listingx"));
		$nominal_komisi_selling = str_replace(".","",$this->input->post("terhitung_sellingx"));
		
		$komisi_persen_listing = $this->input->post("komisi_persen_listingx");
		$komisi_persen_listing_net = $this->input->post("komisi_persen_listingx");
		$komisi_persen_selling = $this->input->post("komisi_persen_sellingx");
		$komisi_persen_selling_net = $this->input->post("komisi_persen_sellingx");
		
		$jabatan_listing = $this->reff->getList($listing);
		$jabatan_selling = $this->reff->getList($selling);
			
		if($this->input->post("options") != $this->input->post("options2"))
		{
			/*if ($jabatan_listing == '11'){
				$nominal_komisi_listinggross =  str_replace(".","",$this->input->post("terhitung_listingx"))*0.7;
				$nominal_komisi_sellinggross =  str_replace(".","",$this->input->post("terhitung_sellingx"))*0.3;
				$komisi_persen_listing_net = '70';
				$komisi_persen_selling_net = '30';
			}elseif($jabatan_selling == '11'){
				$nominal_komisi_listinggross =  str_replace(".","",$this->input->post("terhitung_listingx"))*0.3;
				$nominal_komisi_sellinggross =  str_replace(".","",$this->input->post("terhitung_sellingx"))*0.7;
				$komisi_persen_selling_net = '70';
				$komisi_persen_listing_net = '30';
			}else{
			$nominal_komisi_listinggross =  str_replace(".","",$this->input->post("terhitung_listingx"))/2;
			$nominal_komisi_sellinggross =  str_replace(".","",$this->input->post("terhitung_sellingx"))/2;
			}*/
			$nominal_komisi_listinggross =  str_replace(".","",$this->input->post("terhitung_listingx"))/2;
			$nominal_komisi_sellinggross =  str_replace(".","",$this->input->post("terhitung_sellingx"))/2;
			$sumber_selling =  $this->input->post("options");
		    $sumber_listing =  $this->input->post("options2");
			$harga_net = str_replace(".","",$this->input->post("hargax"))/2;
		}elseif($listing != $selling && $this->input->post("options") == $this->input->post("options2")){
			
			/*if ($jabatan_listing == '11'){
				$nominal_komisi_listinggross =  str_replace(".","",$this->input->post("terhitung_listingx"))*0.7;
				$nominal_komisi_sellinggross =  str_replace(".","",$this->input->post("terhitung_sellingx"))/2;
				$komisi_persen_listing_net = '70';
			}elseif($jabatan_selling == '11'){
				$nominal_komisi_listinggross =  str_replace(".","",$this->input->post("terhitung_listingx"))/2;
				$nominal_komisi_sellinggross =  str_replace(".","",$this->input->post("terhitung_sellingx"))*0.7;
				$komisi_persen_selling_net = '70';
			}else{
				$nominal_komisi_listinggross =  str_replace(".","",$this->input->post("terhitung_listingx"))/2;
				$nominal_komisi_sellinggross =  str_replace(".","",$this->input->post("terhitung_sellingx"))/2;
			}*/
			$nominal_komisi_listinggross =  str_replace(".","",$this->input->post("terhitung_listingx"))/2;
			$nominal_komisi_sellinggross =  str_replace(".","",$this->input->post("terhitung_sellingx"))/2;
			$sumber_selling =  '3';
		    $sumber_listing =  $this->input->post("options2");
			//$harga_net = str_replace(".","",$this->input->post("harga"))/2;
			$harga_net = str_replace(".","",$this->input->post("hargax"));
		}else{
			if ($jabatan_listing == '11'){
				$nominal_komisi_listing =  str_replace(".","",$this->input->post("terhitungx"))*0.7;
				$nominal_komisi_selling =  str_replace(".","",$this->input->post("terhitungx"))*0.3;
				
				$nominal_komisi_listinggross =  $nominal_komisi_listing;
				$nominal_komisi_sellinggross =  $nominal_komisi_selling;
				
				$komisi_persen_listing = '70';
				$komisi_persen_listing_net = '70';
				$komisi_persen_selling = '30';
				$komisi_persen_selling_net = '30';
				
			}else{
				$nominal_komisi_listinggross =  str_replace(".","",$this->input->post("terhitung_listingx"));
				$nominal_komisi_sellinggross =  str_replace(".","",$this->input->post("terhitung_sellingx"));
			}
			$sumber_selling =  $this->input->post("options");
		    $sumber_listing =  $this->input->post("options2");
			$harga_net = str_replace(".","",$this->input->post("hargax"));
		}
		 
		$data=array(
		"harga"=>str_replace(".","",$this->input->post("hargax")),
		"kode_listing"=> $kodelis=$this->input->post("kode_listing"),
		"kode_agen"=> $listing,
		"id_pelanggan"=> $this->input->post("id_pelanggan"),
		//"nominal_bayar"=> $this->input->post("nominal_bayar"),
		//"tgl_pelunasan"=> $this->tanggal->eng_($this->input->post("tgl_pelunasan"),"-"),
		"tgl_closing"=> $this->tanggal->eng_($this->input->post("tgl_closing"),"-"),
		"selling"=> $selling,
		"type_selling"=> "sewa" ,
		"total_komisi"=>  str_replace(".","",$this->input->post("terhitungx") ),
		"komisi_persen"=>  $this->input->post("komisi_persenx") ,
		"ket"=>  $this->input->post("ket") ,
		"sumber_selling"=>  $sumber_selling ,
		"sumber_listing"=>  $sumber_listing ,
		"komisi_persen_listing"=>  $komisi_persen_listing ,
		"komisi_persen_listing_net"=>  $komisi_persen_listing_net ,
		"nominal_komisi_listing"=>  $nominal_komisi_listing ,
		"nominal_komisi_listing_net"=> $nominal_komisi_listinggross,
		"komisi_persen_selling"=>  $komisi_persen_selling ,
		"komisi_persen_selling_net"=>  $komisi_persen_selling_net ,
		"nominal_komisi_selling"=>  $nominal_komisi_selling ,
		"nominal_komisi_selling_net"=> $nominal_komisi_sellinggross,
		"harga_net"=>$harga_net,
		);
		$this->db->where("id",$id=$this->input->post("id"));
		$this->db->update("data_selling",$data);
		$this->updateListingStatus($kodelis,"1",$this->tanggal->eng_($this->input->post("tgl_jatuh_tempo"),"-"));
		//$this->updateListingStatus($kodelis,"1",$this->tanggal->eng_($this->input->post("tgl_jatuh_tempo"),"-"));
		//$kode=$this->db->query("SELECT max(id) as mak from data_selling")->row();
		return $this->uploadFIleZip($id,"zip");
	}
	
	 function uploadFIleZip($kode,$form)
	 {
		if(isset($_FILES[$form]['type']))
		{
		return	$this->do_uploadFIleZip($kode,$form);
		}
 
	 }
	 public function do_uploadFIleZip($kode,$form)
	{	 
		  $lokasi_file = $_FILES[$form]['tmp_name'];
		  $tipe_file   = $_FILES[$form]['type'];
		  $nama_file   = $_FILES[$form]['name'];
		  $target_path = "file_upload/dok/".$kode.".zip";
		if (!empty($lokasi_file)) {
		move_uploaded_file($lokasi_file,$target_path);
		}	
	}
	function updateListingStatus($kode,$sts,$masa)
	{
		$array=array(
		"status"=>$sts,
		"tgl_jatuh_tempo"=>$masa,
		"alasan_cancel"=>$this->input->post("pilih_cancel"),
		"ket_cancel"=>$this->input->post("ket_cancel"),
		);
		$this->db->where("kode_prop",$kode);
	return	$this->db->update("data_property",$array);
	}
	
	
		function soldByOwner($kode)
	{
		$array=array(
		"status"=>"4",
	 	);
		$this->db->where("id_prop",$kode);
	return	$this->db->update("data_property",$array);
	}
	
		
  function saveReport()
	{
		$data=array(
		"id_agen"=>$this->session->userdata("id"),
		"kode_listing"=>$this->input->post("kode_listing"),
		"id_title"=>$this->input->post("title"),
		"ket"=>$this->input->post("text"),
		);
		
			return $this->db->insert("report_listing",$data);
	}
	
	function delHistory($id)
	{
		$this->db->where("id",$id);
		return $this->db->delete('report_listing');
	}
	function cek_alamat_js()
	{
		$this->db->where("alamat_detail",$this->input->post("alamat"));
		$this->db->where("alamat_detail!=","");
	return	$this->db->get("data_property")->num_rows();
	}function cek_alamat_js_edit()
	{
		$this->db->where("alamat_detail",$this->input->post("alamat"));
		$this->db->where("kode_prop!=",$this->input->post("id"));
		$this->db->where("alamat_detail!=","");
	return	$this->db->get("data_property")->num_rows();
	}
	
	function export()
	{
//////start
        $objPHPExcel = new PHPExcel();
//style
        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'rotation' => 0,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '6CCECB')
            ),
            'borders' =>
            array('allborders' =>
                array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'),
                ),
            ),
        );
     //   $objPHPExcel->getActiveSheet(0)->getColumnDimension('F')->setWidth(25);
     //   $objPHPExcel->getActiveSheet(0)->getColumnDimension('G')->setWidth(25);
     //   $objPHPExcel->getActiveSheet(0)->getColumnDimension('H')->setWidth(35);




//create column
        $objPHPExcel->getActiveSheet(0)->setCellValue('A1', 'ID');
        $objPHPExcel->getActiveSheet(0)->setCellValue('B1', 'TYPE');
        $objPHPExcel->getActiveSheet(0)->setCellValue('C1', 'PRICE');
        $objPHPExcel->getActiveSheet(0)->setCellValue('D1', 'LOCATION');
        $objPHPExcel->getActiveSheet(0)->setCellValue('E1', 'DISTRIC');
        $objPHPExcel->getActiveSheet(0)->setCellValue('F1', 'VENDOR');
        $objPHPExcel->getActiveSheet(0)->setCellValue('G1', 'MEMBER');
        $objPHPExcel->getActiveSheet(0)->setCellValue('H1', 'SALES STATUS');
		$objPHPExcel->getActiveSheet(0)->setCellValue('I1', 'KT');
        $objPHPExcel->getActiveSheet(0)->setCellValue('J1', 'KM');
        $objPHPExcel->getActiveSheet(0)->setCellValue('K1', 'KTP');
        $objPHPExcel->getActiveSheet(0)->setCellValue('L1', 'KMP');
        $objPHPExcel->getActiveSheet(0)->setCellValue('M1', 'FLOOR');
        $objPHPExcel->getActiveSheet(0)->setCellValue('N1', 'GARAGE');
        $objPHPExcel->getActiveSheet(0)->setCellValue('O1', 'CARPORT');
        $objPHPExcel->getActiveSheet(0)->setCellValue('P1', 'COMPAS');
        $objPHPExcel->getActiveSheet(0)->setCellValue('Q1', 'ELECTRICITY');
		$objPHPExcel->getActiveSheet(0)->setCellValue('R1', 'WATER');
		$objPHPExcel->getActiveSheet(0)->setCellValue('S1', 'FURNITURE');
        $objPHPExcel->getActiveSheet(0)->setCellValue('T1', 'BUILDING');
        $objPHPExcel->getActiveSheet(0)->setCellValue('U1', 'LAND');
        $objPHPExcel->getActiveSheet(0)->setCellValue('V1', 'STATUS');
        $objPHPExcel->getActiveSheet(0)->setCellValue('W1', 'DESCRIPTION');
        $objPHPExcel->getActiveSheet(0)->setCellValue('X1', 'NOTE');



//make a border column
        $objPHPExcel->getActiveSheet(0)->getStyle('A1:X1')->applyFromArray($style);

        $database = $this->_get_dataProperty();
        $shit = 1;
        $database = $this->db->query($database)->result();
        foreach ($database as $val) {
            $shit++;
			$type=$val->type_jual;
			if($type=="sewa")
			{
				if($val->status=="1")
				{	$status="Sell";
				}elseif($val->status=="2"){
					 $status=" Cancel ".$this->reff->cancelBy($val->alasan_cancel)." ";
				}else{
				 $status="Rent  ";
					 	}
			}elseif($type=="jual"){
				if($val->status=="1")
				{
				 $status="Sold  ";
				}elseif($val->status=="2"){
					 $status=" Cancel ".$this->reff->cancelBy($val->alasan_cancel)."    ";
				}else{
				 $status="Sell";
				}
				
			}else{
			 		
					$status=" Cancel  ";
			}
//create data per row
           $objPHPExcel->getActiveSheet(0)->setCellValue('A' . $shit . '', '`'.$val->kode_prop);
           $objPHPExcel->getActiveSheet(0)->setCellValue('B' . $shit . '', ''.$this->reff->getNamaJenis($val->jenis_prop));
           $objPHPExcel->getActiveSheet(0)->setCellValue('C' . $shit . '', ''.$val->harga+$val->fee_up);
           $objPHPExcel->getActiveSheet(0)->setCellValue('D' . $shit . '', ''.$val->alamat_detail);
           $objPHPExcel->getActiveSheet(0)->setCellValue('E' . $shit . '', ''.$val->nama_area);
           $objPHPExcel->getActiveSheet(0)->setCellValue('F' . $shit . '', ''.$this->reff->getNamaOwner($val->id_owner));
           $objPHPExcel->getActiveSheet(0)->setCellValue('G' . $shit . '', ''.$this->reff->getNamaAgen($val->agen));
           $objPHPExcel->getActiveSheet(0)->setCellValue('H' . $shit . '', ''.$status);
           $objPHPExcel->getActiveSheet(0)->setCellValue('I' . $shit . '', ''.$val->kamar_tidur);
           $objPHPExcel->getActiveSheet(0)->setCellValue('J' . $shit . '', $val->kamar_mandi);
           $objPHPExcel->getActiveSheet(0)->setCellValue('K' . $shit . '', $val->kamar_tidur_p);
           $objPHPExcel->getActiveSheet(0)->setCellValue('L' . $shit . '', $val->kamar_mandi_p);
           $objPHPExcel->getActiveSheet(0)->setCellValue('M' . $shit . '', $val->jml_lantai);
           $objPHPExcel->getActiveSheet(0)->setCellValue('N' . $shit . '', $val->jml_garasi);
           $objPHPExcel->getActiveSheet(0)->setCellValue('O' . $shit . '', $val->jml_carports);
           $objPHPExcel->getActiveSheet(0)->setCellValue('P' . $shit . '', $this->reff->getNamaHadap($val->hadap));
           $objPHPExcel->getActiveSheet(0)->setCellValue('Q' . $shit . '', $val->daya_listrik);
           $objPHPExcel->getActiveSheet(0)->setCellValue('R' . $shit . '', $this->reff->getNamaAir($val->air));
           $objPHPExcel->getActiveSheet(0)->setCellValue('S' . $shit . '', $this->reff->getNamaFurniture($val->furniture));
           $objPHPExcel->getActiveSheet(0)->setCellValue('T' . $shit . '', $val->luas_bangunan);
           $objPHPExcel->getActiveSheet(0)->setCellValue('U' . $shit . '', $val->luas_tanah);
           $objPHPExcel->getActiveSheet(0)->setCellValue('V' . $shit . '', $this->reff->getNamaJenisSertifikat($val->jenis_sertifikat));
           $objPHPExcel->getActiveSheet(0)->setCellValue('W' . $shit . '', $val->desc);
           $objPHPExcel->getActiveSheet(0)->setCellValue('X' . $shit . '', $val->keterangan);     
        }

//auto size column
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('G')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('H')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('I')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('J')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('K')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('L')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('M')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('N')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('O')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('P')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('Q')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('R')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('S')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('T')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('U')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('V')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('W')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('X')->setAutoSize(true);

// Rename worksheet (worksheet, not filename)
        $objPHPExcel->getActiveSheet(0)->setTitle('Data Listing');
        
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Data-Listing.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
//////finish
    
	}	
	
	
	function export_selling()
	{
//////start
        $objPHPExcel = new PHPExcel();
//style
        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'rotation' => 0,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '6CCECB')
            ),
            'borders' =>
            array('allborders' =>
                array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'),
                ),
            ),
        );
     //   $objPHPExcel->getActiveSheet(0)->getColumnDimension('F')->setWidth(25);
     //   $objPHPExcel->getActiveSheet(0)->getColumnDimension('G')->setWidth(25);
     //   $objPHPExcel->getActiveSheet(0)->getColumnDimension('H')->setWidth(35);




//create column
		$objPHPExcel->getActiveSheet(0)->mergeCells('A1:H1');
        $objPHPExcel->getActiveSheet(0)->setCellValue('A1', 'TRANSACTION');
        $objPHPExcel->getActiveSheet(0)->setCellValue('A2', 'CLOSING DATE');
        $objPHPExcel->getActiveSheet(0)->setCellValue('B2', 'AREA');
        $objPHPExcel->getActiveSheet(0)->setCellValue('C2', 'ID');
        $objPHPExcel->getActiveSheet(0)->setCellValue('D2', 'VENDOR');
        $objPHPExcel->getActiveSheet(0)->setCellValue('E2', 'BUYER');
        $objPHPExcel->getActiveSheet(0)->setCellValue('F2', 'DEAL PRICE');
		$objPHPExcel->getActiveSheet(0)->setCellValue('G2', 'DEAL PRICE NET');
        $objPHPExcel->getActiveSheet(0)->setCellValue('H2', 'COMMISION TOTAL');
		
		
		$objPHPExcel->getActiveSheet(0)->mergeCells('I1:K1');
        $objPHPExcel->getActiveSheet(0)->setCellValue('I1', 'LISTING');
		$objPHPExcel->getActiveSheet(0)->setCellValue('I2', 'MEMBER');
        $objPHPExcel->getActiveSheet(0)->setCellValue('J2', 'COMMISION GROSS');
		$objPHPExcel->getActiveSheet(0)->setCellValue('K2', 'COMMISION NET');
      
	  
		$objPHPExcel->getActiveSheet(0)->mergeCells('L1:N1');
        $objPHPExcel->getActiveSheet(0)->setCellValue('L1', 'SELLING');
		$objPHPExcel->getActiveSheet(0)->setCellValue('L2', 'MEMBER');
        $objPHPExcel->getActiveSheet(0)->setCellValue('M2', 'COMMISION GROSS');
		$objPHPExcel->getActiveSheet(0)->setCellValue('N2', 'COMMISION NET');
		
        $objPHPExcel->getActiveSheet(0)->setCellValue('O1', 'NOTE');
      

//make a border column
        $objPHPExcel->getActiveSheet(0)->getStyle('A1:O1')->applyFromArray($style);
        $objPHPExcel->getActiveSheet(0)->getStyle('A2:O2')->applyFromArray($style);

        $database = $this->_ajax_selling();
        $shit = 2;
        $database = $this->db->query($database)->result();
        foreach ($database as $val) {
            $shit++;
			if($this->reff->getNamaPelanggan(isset($val->id_pelanggan)?($val->id_pelanggan):""))
			{
			$buyer=$this->reff->getNamaPelanggan(isset($val->id_pelanggan)?($val->id_pelanggan):"");
			}else{
			$buyer=$val->id_pelanggan;
			}
			
			$kp="";	$kpl= $kps="";
			
			if($val->sumber_selling=="1")
			{
				$ejen=$this->reff->getNamaAgen($val->selling);
				/*$ks=$val->nominal_komisi_selling;
				$ksg=$val->nominal_komisi_selling_net;*/
				$ks=number_format($val->nominal_komisi_selling,2,",",".");
				$ksg=number_format($val->nominal_komisi_listing_net,2,",",".");
			}elseif($val->sumber_selling=="3"){
				$ejen=$this->reff->getNamaAgen($val->selling);
				/*$ks=$val->nominal_komisi_selling;
				$ksg=$val->nominal_komisi_selling_net;*/
				$ks=number_format($val->nominal_komisi_selling,2,",",".");
				$ksg=number_format($val->nominal_komisi_selling_net,2,",",".");
			}else{
				$ejen=$val->selling;
				$ks="";
				$ksg="";
			}
			
			
				$ejenListing=$val->kode_agen;
			if($val->sumber_listing=="1")
			{
				$ejenListing=$this->reff->getNamaAgen($val->kode_agen);
				/*$kl=$val->nominal_komisi_listing;
				$klg=$val->nominal_komisi_listing_net;*/
				$kl=number_format($val->nominal_komisi_listing,2,",",".");
				$klg=number_format($val->nominal_komisi_listing_net,2,",",".");
			}else{
				$kl="";
				$klg="";
			} 
			
			
			if($val->komisi_persen){
				//$kp=$val->komisi_persen."%";
				$kp="";
			}
			
//create data per row
           $objPHPExcel->getActiveSheet(0)->setCellValue('A' . $shit . '', $this->tanggal->ind($val->tgl_closing,"/"));
           $objPHPExcel->getActiveSheet(0)->setCellValue('B' . $shit . '', $this->reff->getAlamatListingByKode($val->kode_listing));
           $objPHPExcel->getActiveSheet(0)->setCellValue('C' . $shit . '', $val->kode_listing);
           $objPHPExcel->getActiveSheet(0)->setCellValue('D' . $shit . '', $this->reff->getNamaOwnerByListing($val->kode_listing));
           $objPHPExcel->getActiveSheet(0)->setCellValue('E' . $shit . '', $buyer);
           $objPHPExcel->getActiveSheet(0)->setCellValue('F' . $shit . '', number_format($val->harga,2,",","."));
		   $objPHPExcel->getActiveSheet(0)->setCellValue('G' . $shit . '', number_format($val->harga_net,2,",","."));
           $objPHPExcel->getActiveSheet(0)->setCellValue('H' . $shit . '', number_format($val->total_komisi,2,",","."));
           $objPHPExcel->getActiveSheet(0)->setCellValue('I' . $shit . '', $ejenListing);
           $objPHPExcel->getActiveSheet(0)->setCellValue('J' . $shit . '', $kpl."". $kl);
		   $objPHPExcel->getActiveSheet(0)->setCellValue('K' . $shit . '', $kpl."". $klg);
           $objPHPExcel->getActiveSheet(0)->setCellValue('L' . $shit . '', $ejen);
           $objPHPExcel->getActiveSheet(0)->setCellValue('M' . $shit . '', $kps."". $ks);
		   $objPHPExcel->getActiveSheet(0)->setCellValue('N' . $shit . '', $kps."". $ksg);
           $objPHPExcel->getActiveSheet(0)->setCellValue('O' . $shit . '', $val->ket);

 
        }

//auto size column
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('G')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('H')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('I')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('J')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('K')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('L')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('M')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('N')->setAutoSize(true);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('O')->setAutoSize(true);


// Rename worksheet (worksheet, not filename)
        $objPHPExcel->getActiveSheet(0)->setTitle('TRANSACTION');
        
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="TRANSACTION.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
//////finish
    
	}
	
 
	function saveStatus($id,$sts)
	{
		$this->db->set("status_profile",$sts);
		$this->db->where("id_agen",$id);
	return	$this->db->update("data_agen");
	}
	
	function im_factsheet($kode){
		$database="SELECT a.kode_prop, a.komplek, a.area_listing, a.desc, a.gambar1, a.gambar2, a.gambar3, a.gambar4, a.gambar5, a.luas_tanah, a.luas_bangunan, 
		a.harga, a.fee_up, a.kamar_tidur, a.kamar_mandi, a.daya_listrik, a.jml_lantai, a.jenis_sertifikat, a.furniture, a.jml_carports, a.jml_garasi, a.hadap, a.air, b.nama, b.kode_agen, b.poto2 
		FROM `data_property` AS a LEFT JOIN data_agen AS b ON a.agen = b.kode_agen WHERE a.kode_prop = '".$kode."'";
		$database = $this->db->query($database)->result();
        foreach ($database as $val) {
			
			if($val->gambar1 != '')
			{
				$im1 = str_replace(' ', '%20', $val->gambar1);
				$gambar1 = ''.base_url().'file_upload/img/'.$im1.'';
			}else{
				$gambar1 = ''.base_url().'file_upload/img/not-avaliable.jpg';
			}
			$firstUrl = $gambar1;
			
			if($val->gambar2 != '')
			{
				$im2 = str_replace(' ', '%20', $val->gambar2);
				$gambar2 = ''.base_url().'file_upload/img/'.$im2.'';
			}else{
				$gambar2 = ''.base_url().'file_upload/img/not-avaliable.jpg';
			}
			$secondUrl = $gambar2;
			
			if($val->gambar3 != '')
			{
				$im3 = str_replace(' ', '%20', $val->gambar3);
				$gambar3 = ''.base_url().'file_upload/img/'.$im3.'';
			}else{
				$gambar3 = ''.base_url().'file_upload/img/not-avaliable.jpg';
			}
			$thirdUrl = $gambar3;
			
			if($val->gambar4 != '')
			{
				$im4 = str_replace(' ', '%20', $val->gambar4);
				$gambar4 = ''.base_url().'file_upload/img/'.$im4.'';
			}else{
				$gambar4 = ''.base_url().'file_upload/img/not-avaliable.jpg';
			}
			$fourthUrl = $gambar4;
			
			if($val->gambar5 != '')
			{
				$im5 = str_replace(' ', '%20', $val->gambar5);
				$gambar5 = ''.base_url().'file_upload/img/'.$im5.'';
			}else{
				$gambar5 = ''.base_url().'file_upload/img/not-avaliable.jpg';
			}
			$fifthUrl = $gambar5;

			if($val->poto2 != ''){
				$agen = ''.base_url().'file_upload/agen/'.$val->poto2.'';
			}else{
				$agen = ''.base_url().'file_upload/img/not-avaliable.png';
			}
			
			$biru = ''.base_url().'file_upload/image/biru.png';
			
			// The text to draw
			$kode_prop = $val->kode_prop;
			$text = $val->desc;
			$area = $val->area_listing;
			$lt = "".$val->luas_tanah." M2";
			$kmr = $val->kamar_tidur." Kamar";
			$lrk = "".$val->daya_listrik." Watt";
			$sertifikat = $this->reff->getNamaJenisSertifikat($val->jenis_sertifikat);
			$car = "".$val->jml_garasi." GARASI & ".$val->jml_carports." CARPORT";
			$hadap = $this->reff->getNamaHadap($val->hadap);
			$lb = "".$val->luas_bangunan." M2";
			$km = $val->kamar_mandi." Kamar Mandi";
			$floor = $val->jml_lantai." LANTAI";
			$furniture = $this->reff->getNamaFurniture($val->furniture);
			$air = $this->reff->getNamaAir($val->air);
			$harga = number_format($val->harga+$val->fee_up,0,",",".");
			$kodedb = $val->kode_agen;
			$namadb = $val->nama;
			
		}
		
		$alamat = explode(" ",$text);
		$namaagen = explode(" ",$namadb);
		$kodedbx = explode("/",$kodedb);
		$kodeagen = "".$kodedbx[0]." / ".$kodedbx[1]." / ".$kodedbx[2]." / ".$kodedbx[3]."";
		
		$background = imagecreatefromjpeg(''.base_url().'file_upload/image/fact.jpg');
		$outputImage = $background;
		$src_width = $outputImage[0];
		$src_height = $outputImage[1];

		$data = getimagesize($firstUrl);
		$width = $data[0];
		$height = $data[1];

		$data1 = getimagesize($secondUrl);
		$width1 = $data1[0];
		$height1 = $data1[1];

		$data2 = getimagesize($thirdUrl);
		$width2 = $data2[0];
		$height2 = $data2[1];

		$data3 = getimagesize($fourthUrl);
		$width3 = $data3[0];
		$height3 = $data3[1];

		$data4 = getimagesize($fifthUrl);
		$width4 = $data4[0];
		$height4 = $data4[1];

		$data5 = getimagesize($agen);
		$width5 = $data5[0];
		$height5 = $data5[1];
		
		$data6 = getimagesize($biru);
		$width6 = $data6[0];
		$height6 = $data6[1];
		$aksi_pic6 = imagecreatefrompng($biru);
		
		$info1  = pathinfo($firstUrl);
		$exten1 = $info1['extension'];
		if($exten1 == 'jpg' || $exten1 == 'jpeg' || $exten1 == 'JPG'){
				$aksi_pic1 = imagecreatefromjpeg($firstUrl);
		}elseif($exten1 == 'png'){
				$aksi_pic1 = imagecreatefrompng($firstUrl);
		}

		$info2  = pathinfo($secondUrl);
		$exten2 = $info2['extension'];
		if($exten2 == 'jpg' || $exten2 == 'jpeg' || $exten2 == 'JPG'){
				$aksi_pic2 = imagecreatefromjpeg($secondUrl);
		}elseif($exten2 == 'png'){
				$aksi_pic2 = imagecreatefrompng($secondUrl);
		}

		$info3  = pathinfo($thirdUrl);
		$exten3 = $info3['extension'];
		if($exten3 == 'jpg' || $exten3 == 'jpeg' || $exten3 == 'JPG'){
				$aksi_pic3 = imagecreatefromjpeg($thirdUrl);
		}elseif($exten3 == 'png'){
				$aksi_pic3 = imagecreatefrompng($thirdUrl);
		}
			
		$info4  = pathinfo($fourthUrl);
		$exten4 = $info4['extension'];
		if($exten4 == 'jpg' || $exten4 == 'jpeg' || $exten4 == 'JPG'){
				$aksi_pic4 = imagecreatefromjpeg($fourthUrl);
		}elseif($exten4 == 'png'){
				$aksi_pic4 = imagecreatefrompng($fourthUrl);
		}
			
		$info5  = pathinfo($fifthUrl);
		$exten5 = $info5['extension'];
		if($exten5 == 'jpg' || $exten5 == 'jpeg' || $exten5 == 'JPG'){
				$aksi_pic5 = imagecreatefromjpeg($fifthUrl);
		}elseif($exten5 == 'png'){
				$aksi_pic5 = imagecreatefrompng($fifthUrl);
		}
			

		$first = $aksi_pic1;
		$image_p = imagecreatetruecolor(3210, 2145);

		$second = $aksi_pic2;
		$image_p2 = imagecreatetruecolor(3040, 2030);

		$third = $aksi_pic3;
		$image_p3 = imagecreatetruecolor(2364, 1560);

		$fourth = $aksi_pic4;
		$image_p4 = imagecreatetruecolor(2364, 1570);

		$fifth = $aksi_pic5;
		$image_p5 = imagecreatetruecolor(1995, 1340);

		$six = imagecreatefrompng($agen);
		$image_p6 = imagecreatetruecolor(1995, 2240);
		
		$background = imagecolorallocatealpha($image_p6,0,0,0,0);
		imagecolortransparent($image_p6, $background);
		imagealphablending($image_p6, true);
		
		$seven = imagecreatefrompng($biru);
		$image_p7 = imagecreatetruecolor(2000, 300);
		
		$background2 = imagecolorallocatealpha($image_p7,0,0,0,0);
		imagecolortransparent($image_p7, $background2);
		imagealphablending($image_p7, true);

		imagecopyresampled($image_p, $first, 0, 0, 0, 0, 3210, 2145, $width, $height);
		imagecopyresampled($image_p2, $second, 0, 0, 0, 0, 3040, 2030, $width1, $height1);
		imagecopyresampled($image_p3, $third, 0, 0, 0, 0, 2364, 1560, $width2, $height2);
		imagecopyresampled($image_p4, $fourth, 0, 0, 0, 0, 2364, 1570, $width3, $height3);
		imagecopyresampled($image_p5, $fifth, 0, 0, 0, 0, 1995, 1340, $width4, $height4);
		imagecopyresampled($image_p6, $six, 0, 0, 0, 0, 1995, 2240, $width5, $height5);
		imagecopyresampled($image_p7, $seven, 0, 0, 0, 0, 2000, 300, $width6, $height6);

		imagecopymerge($outputImage,$image_p,164,1200,0,0, 3210, 2145, 100);
		imagecopymerge($outputImage,$image_p2,164,3530,0,0, 3040, 2030,100);
		imagecopymerge($outputImage,$image_p3,3380,3530,0,0, 2364, 1560,100);
		imagecopymerge($outputImage,$image_p4,3380,5293,0,0, 2364, 1570,100);
		imagecopymerge($outputImage,$image_p5,3560,1990,0,0, 1995, 1340,100);
		imagecopymerge($outputImage,$image_p6,3560,7030,0,0, 1995, 2240,100);
		imagecopymerge($outputImage,$image_p7,3690,8860,0,0, 2000, 300,100);

		$white = imagecolorallocate($outputImage, 255, 255, 255);
		$black = imagecolorallocate($outputImage, 0, 0, 0);
		$blueM = ImageColorAllocate($outputImage, 6, 170, 180);
		$blueN = ImageColorAllocate($outputImage, 16, 74, 85);
		$silverx = ImageColorAllocate($outputImage, 88, 89, 91);

		// Replace path by your own font path
		$futura = "/home/mybe2/public_html/public_page_asset/assets/libraries/font/Futura_Heavy_font.ttf";
		$avant = "/home/mybe2/public_html/public_page_asset/assets/libraries/font/Avgardd_0.ttf";
		$avant2 = '/home/mybe2/public_html/public_page_asset/assets/libraries/font/Avgardn_0.ttf';
		$geome = "/home/mybe2/public_html/public_page_asset/assets/libraries/font/geometric-415-medium-italic-bt-59658868559b1_0.ttf";
		$hum = "/home/mybe2/public_html/public_page_asset/assets/libraries/font/tt1161m_0.ttf";

		imagettftext($outputImage, 100, 0, 1050, 850, $black, $futura, $kode_prop);
		imagettftext($outputImage, 250, 0, 3550, 750, $white, $avant, $alamat[0]);
		imagettftext($outputImage, 250, 0, 3550, 1050, $white, $avant, $alamat[1]);
		imagettftext($outputImage, 250, 0, 3550, 1320, $white, $avant, $alamat[2]);
		imagettftext($outputImage, 100, 0, 3980, 1815, $blueM, $geome, $area);
		imagettftext($outputImage, 100, 0, 680, 6383, $blueN, $hum, $lt);
		imagettftext($outputImage, 100, 0, 680, 6733, $blueN, $hum, $kmr);
		imagettftext($outputImage, 100, 0, 680, 7050, $blueN, $hum, $lrk);
		imagettftext($outputImage, 100, 0, 680, 7350, $blueN, $hum, $sertifikat);
		imagettftext($outputImage, 100, 0, 680, 7690, $blueN, $hum, $car);
		imagettftext($outputImage, 100, 0, 680, 7990, $blueN, $hum, $hadap);
		imagettftext($outputImage, 100, 0, 2085, 6383, $blueN, $hum, $lb);
		imagettftext($outputImage, 100, 0, 2085, 6733, $blueN, $hum, $km);
		imagettftext($outputImage, 100, 0, 2085, 7050, $blueN, $hum, $floor);
		imagettftext($outputImage, 100, 0, 2085, 7350, $blueN, $hum, $furniture);
		imagettftext($outputImage, 100, 0, 2085, 7990, $blueN, $hum, $air);
		imagettftext($outputImage, 100, 0, 600, 8390, $silverx, $hum, $harga);
		imagettftext($outputImage, 120, 0, 3950, 9070, $white, $avant, $kodeagen);
		imagettftext($outputImage, 250, 0, 4200, 9530, $white, $avant2, strtoupper($namaagen[0]));

		//imagejpeg($outputImage, APPLICATION_PATH .'test.jpg');
		$desainfact = 'factsheet_'.$kode.'.jpg';
		
		imagejpeg($outputImage, '/home/mybe2/public_html/file_upload/img/'.$desainfact.'');
		
		//proses kecilkan foto
		//$ambilgambar = ''.base_url().'file_upload/img/factsheet_'.$kode.'.jpg';
		$ambilgambar = '/home/mybe2/public_html/file_upload/img/factsheet_'.$kode.'.jpg';
		$src = getimagesize($ambilgambar);
		$src_width = $src[0];
		$src_height = $src[1];

		$dst_width = 1500;
		$dst_height = ($dst_width/$src_width)*$src_height;

		//proses perubahan ukuran
		$im = imagecreatetruecolor($dst_width,$dst_height);
		imagecopyresampled($im, $outputImage, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);

		//Simpan gambar
		$desainkecil = 'factsheet_'.$kode.'_kecil.jpg';
		imagejpeg($im,'/home/mybe2/public_html/file_upload/img/'.$desainkecil.'');

		imagedestroy($outputImage);
		imagedestroy($im);
		
		$array=array(
			"desain"=>$desainkecil,
			"desaintv"=>$desainfact
		);
		
		$this->db->where("kode_prop",$kode);
		return	$this->db->update("data_property",$array);
		
		/*
		if(file_exists(''.base_url().'file_upload/img/test.jpg'))
		{
			return true;
		}else{
	    	return false;
		}*/
		
	}
	
	
	function createCeleb(){
		$id = $this->input->post("id");
		
		$database="SELECT a.harga, a.total_komisi, a.nominal_komisi_listing, a.sumber_listing, a.sumber_selling, a.type_selling, a.kode_agen, a.selling,  d.id AS id_jabatan, b.desc, b.gambar1, c.nama, 
		c.poto2, c.hp, c.foto,  f.id AS id_jabatan2, d.nama AS jabatan, e.poto2 AS fotoagen, e.nama AS namaagen, f.nama AS jabatan2, e.foto AS foto2   
		FROM data_selling AS a 
		LEFT JOIN data_property AS b ON a.kode_listing = b.kode_prop 
		LEFT JOIN data_agen AS c ON a.kode_agen = c.kode_agen
		LEFT JOIN tr_jabatan AS d ON c.jabatan = d.id
        LEFT JOIN data_agen AS e ON a.selling = e.kode_agen
		LEFT JOIN tr_jabatan AS f ON e.jabatan = f.id
		WHERE a.id = '".$id."'";
		$database = $this->db->query($database)->result();
        foreach ($database as $val) {
			// The text to draw
			$harga_db = "Rp ".number_format($val->harga,0,",",".")."";
			$komisi_db = "Rp ".number_format($val->nominal_komisi_listing,0,",",".")."";
			$total_komisi_db = "Rp ".number_format($val->total_komisi,0,",",".")."";
			$sumber_listing_db = $val->sumber_listing;
			$sumber_selling_db = $val->sumber_selling;
			$tempat_db = $val->desc;
			$rumah_db = $val->gambar1;
			$nama_db = $val->nama;
			$foto_db = $val->poto2;
			$foto = $val->foto;
			$hp_db = $val->hp;
			$jabatan_db = $val->jabatan;
			$type_selling_db = $val->type_selling;
			$fotoagen = $val->fotoagen;
			$namaagen = $val->namaagen;
			$jabatan2 = $val->jabatan2;
			$foto2 = $val->foto2;
			$kode_agen = $val->kode_agen;
			$selling = $val->selling;
			$id_jabatan = $val->id_jabatan;
			$id_jabatan2 = $val->id_jabatan2;
		}
		$tgl_pecah = explode("/",$this->input->post("tgl_closing"));
		$tgl = $tgl_pecah[0];
		$bln = $this->konversi->bulan($tgl_pecah[1]);
		$thn = $tgl_pecah[2];
		
		$rumah = ''.base_url().'file_upload/img/'.$rumah_db.'';
		$bulet = ''.base_url().'file_upload/image/bulet.png';
		$point = ''.base_url().'file_upload/image/point.png';
		$phone = ''.base_url().'file_upload/image/phone.png';
		
		if($sumber_listing_db == $sumber_selling_db){
			
			//----Versi Sosmed------//
			
			$background = imagecreatefromjpeg(''.base_url().'file_upload/image/celebSosmed.jpg');
			$outputImage = $background;
			
			if($type_selling_db == 'jual'){
				$atas = ''.base_url().'file_upload/image/celeb-atas-sosmed.png';
			}else{
				$atas = ''.base_url().'file_upload/image/celeb-atas2-sosmed.png';
			}
			
				$agen = ''.base_url().'file_upload/agen/'.$foto_db.'';
				$agenx = $nama_db;
				$jabatan_db = $jabatan_db;
				$fotox = ''.base_url().'file_upload/agen/'.$foto.'';
			
			$agenn = explode(" ",$agenx);
			$agen_db = $agenn[0];

			$data1 = getimagesize($rumah);
			$width1 = $data1[0];
			$height1 = $data1[1];

			$data2 = getimagesize($atas);
			$width2 = $data2[0];
			$height2 = $data2[1];

			$data3 = getimagesize($bulet);
			$width3 = $data3[0];
			$height3 = $data3[1];

			$data4 = getimagesize($agen);
			$width4 = $data4[0];
			$height4 = $data4[1];
			
			$data5 = getimagesize($point);
			$width5 = $data5[0];
			$height5 = $data5[1];
			
			$data6 = getimagesize($phone);
			$width6 = $data6[0];
			$height6 = $data6[1];

			$first = imagecreatefromjpeg($rumah);
			$image_p1 = imagecreatetruecolor(2970, 2210);

			//$second = imagecreatefrompng($atas);
			//$image_p2 = imagecreatetruecolor(3000, 900);
			$second = imagecreatefrompng($atas);
			$image_p2 = imagecreatetruecolor(3000, 2200);

			$tiga = imagecreatefrompng($bulet);
			$image_p3 = imagecreatetruecolor(800, 800);

			$empat = imagecreatefrompng($agen);
			$image_p4 = imagecreatetruecolor(1200, 1200);
			
			$lima = imagecreatefrompng($point);
			$image_p5 = imagecreatetruecolor(189, 182);
			
			$enam = imagecreatefrompng($phone);
			$image_p6 = imagecreatetruecolor(153, 146);

			//transparan
			$background = imagecolorallocatealpha($image_p2,0,0,0,0);
			imagecolortransparent($image_p2, $background);
			imagealphablending($image_p2, true);

			$background2 = imagecolorallocatealpha($image_p3,0,0,0,0);
			imagecolortransparent($image_p3, $background2);
			imagealphablending($image_p3, true);

			$background3 = imagecolorallocatealpha($image_p4,0,0,0,0);
			imagecolortransparent($image_p4, $background3);
			imagealphablending($image_p4, true);
			
			$background4 = imagecolorallocatealpha($image_p5,0,0,0,0);
			imagecolortransparent($image_p5, $background4);
			imagealphablending($image_p5, true);
			
			$background5 = imagecolorallocatealpha($image_p6,0,0,0,0);
			imagecolortransparent($image_p6, $background5);
			imagealphablending($image_p6, true);

			imagecopyresampled($image_p1, $first, 0, 0, 0, 0, 2970, 2210, $width1, $height1);
			imagecopyresampled($image_p2, $second, 0, 0, 0, 0, 3000, 2200, $width2, $height2);
			imagecopyresampled($image_p3, $tiga, 0, 0, 0, 0, 800, 800, $width3, $height3);
			imagecopyresampled($image_p4, $empat, 0, 0, 0, 0, 1200, 1200, $width4, $height4);
			imagecopyresampled($image_p5, $lima, 0, 0, 0, 0, 189, 182, $width5, $height5);
			imagecopyresampled($image_p6, $enam, 0, 0, 0, 0, 153, 146, $width6, $height6);

			imagecopymerge($outputImage,$image_p1, 35, 1, 0, 0, 2970, 2210,100);
			imagecopymerge($outputImage,$image_p2, 35, 1, 0, 0, 3000, 2200,100);
			imagecopymerge($outputImage,$image_p3, 40, 1850, 0 , 0, 800, 800,100);
			imagecopymerge($outputImage,$image_p4, 1840, 1620, 0 , 0, 1200, 1200,100);
			imagecopymerge($outputImage,$image_p5, 780, 2480, 0 , 0, 189, 182,100);
			imagecopymerge($outputImage,$image_p6, 1460, 2700, 0 , 0, 153, 146,100);

			//color font
			$item = imagecolorallocate($outputImage, 50, 50, 50);
			$kuning = imagecolorallocate($outputImage, 245, 197, 11);
			$abu = imagecolorallocate($outputImage, 198, 199, 203);

			// Replace path by your own font path			
			$kenyan = '/home/mybe2/public_html/public_page_asset/assets/libraries/font/kenyan_coffee_bd.ttf';
			$cafeta = '/home/mybe2/public_html/public_page_asset/assets/libraries/font/cafeta.ttf';

			//put text to image
			imagettftext($outputImage, 200, 0, 320, 2170, $item, $kenyan, $tgl);
			if($tgl_pecah[1] == '1' or $tgl_pecah[1] == '2' or $tgl_pecah[1] == '8' or $tgl_pecah[1] == '9' or $tgl_pecah[1] == '10' or $tgl_pecah[1] == '11' or $tgl_pecah[1] == '12'){
				imagettftext($outputImage, 100, 0, 250, 2330, $item, $cafeta, $bln);
			}elseif($tgl_pecah[1] == '5'){
				imagettftext($outputImage, 110, 0, 340, 2330, $item, $cafeta, $bln);
			}elseif($tgl_pecah[1] == '6' or $tgl_pecah[1] == '7'){
				imagettftext($outputImage, 110, 0, 330, 2330, $item, $cafeta, $bln);
			}else{
				imagettftext($outputImage, 110, 0, 300, 2330, $item, $cafeta, $bln);
			}
			//imagettftext($outputImage, 100, 0, 200, 2330, $item, $cafeta, $bln);
			imagettftext($outputImage, 100, 0, 330, 2500, $item, $kenyan, $thn);
			imagettftext($outputImage, 100, 0, 990, 2630, $kuning, $kenyan, $tempat_db);
			if($id_jabatan == '12' or $id_jabatan == '3'){
			imagettftext($outputImage, 100, 0, 450, 2823, $kuning, $cafeta, $agen_db);
			imagettftext($outputImage, 70, 0, 780, 2823, $abu, $cafeta, $jabatan_db);
			}else{
			imagettftext($outputImage, 100, 0, 520, 2823, $kuning, $cafeta, $agen_db);
			imagettftext($outputImage, 70, 0, 900, 2823, $abu, $cafeta, $jabatan_db);
			}
			imagettftext($outputImage, 100, 0, 1650, 2823, $kuning, $cafeta, $hp_db);
			
			if($this->input->post("banklogo") <> ''){
				$usupported = ''.base_url().'file_upload/bank/'.$this->input->post("banklogo").'';
				
				$data_u = getimagesize($usupported);
				$width_u = $data_u[0];
				$height_u = $data_u[1];
			
				//$gambar_u = imagecreatefrompng($usupported);
				$gambar_u = imagecreatefromjpeg($usupported);
				$image_u = imagecreatetruecolor(3000, 2200);
				
				//$background_u = imagecolorallocatealpha($image_u,0,0,0,0);
				//imagecolortransparent($image_u, $background_u);
				//imagealphablending($image_u, true);
								
				imagecopyresampled($image_u, $gambar_u, 0, 0, 0, 0, 650, 450, $width_u, $height_u);
				imagecopymerge($outputImage,$image_u, 2340, 1120, 0, 0, 650, 450,100);
				
				imagettftext($outputImage, 70, 0, 2270, 1150, $item, $cafeta, "Supported by:");
			}
			
			$desaincelebSosmed = 'celebSosmed_'.$id.'.jpg';
		
			imagejpeg($outputImage, '/home/mybe2/public_html/file_upload/img/'.$desaincelebSosmed.'');

			imagedestroy($outputImage);
			
			//----Versi Internal------//
			
			$background = imagecreatefromjpeg(''.base_url().'file_upload/image/celebInternal.jpg');
			$outputImage = $background;
			
			$data1 = getimagesize($rumah);
			$width1 = $data1[0];
			$height1 = $data1[1];

			$data2 = getimagesize($fotox);
			$width2 = $data2[0];
			$height2 = $data2[1];

			$first = imagecreatefromjpeg($rumah);
			$image_p1 = imagecreatetruecolor(1224, 1784);

			$second = imagecreatefrompng($fotox);
			$image_p2 = imagecreatetruecolor(700, 700);

			//transparan
			$background = imagecolorallocatealpha($image_p2,0,0,0,0);
			imagecolortransparent($image_p2, $background);
			imagealphablending($image_p2, true);

			imagecopyresampled($image_p1, $first, 0, 0, 0, 0, 1224, 1784, $width1, $height1);
			imagecopyresampled($image_p2, $second, 0, 0, 0, 0, 700, 700, $width2, $height2);

			imagecopymerge($outputImage,$image_p1, 137, 1, 0, 0, 1224, 1784,100);
			imagecopymerge($outputImage,$image_p2, 1800, 680, 0, 0, 700, 700,100);

			//color font
			$biru = imagecolorallocate($outputImage, 69, 175, 175);
			$putih = imagecolorallocate($outputImage, 255, 255, 255);

			// Replace path by your own font path
			$fetish = '/home/mybe2/public_html/public_page_asset/assets/libraries/font/Fashion_Fetish_Heavy_0.ttf';
			$fetish2 = '/home/mybe2/public_html/public_page_asset/assets/libraries/font/Fashion_Fetish_Regular_0.ttf';
			$futura = '/home/mybe2/public_html/public_page_asset/assets/libraries/font/Futura_Heavy_font.ttf';

			//put text to image
			imagettftext($outputImage, 70, 0, 1970, 1490, $putih, $fetish, $agen_db);
			if($id_jabatan == '12'){
				imagettftext($outputImage, 48, 0, 1740, 1610, $putih, $fetish2, $jabatan_db);
			}else{
			imagettftext($outputImage, 50, 0, 1820, 1610, $putih, $fetish2, $jabatan_db);
			}
			imagettftext($outputImage, 60, 0, 1780, 1865, $putih, $fetish, $harga_db);
			imagettftext($outputImage, 60, 0, 1780, 2080, $putih, $fetish, $total_komisi_db);
			imagettftext($outputImage, 90, 0, 320, 2020, $biru, $fetish, $tgl);
			imagettftext($outputImage, 60, 0, 530, 2020, $biru, $fetish, $bln);
			imagettftext($outputImage, 60, 0, 210, 2160, $putih, $futura, $tempat_db);

			$desaincelebInternal = 'celebInternal_'.$id.'.jpg';
			
			imagejpeg($outputImage, '/home/mybe2/public_html/file_upload/img/'.$desaincelebInternal.'');

			imagedestroy($outputImage);
			
			$array = array(
					"gambar_sosmed"=>$desaincelebSosmed,
					"gambar_internal"=>$desaincelebInternal
			);
			
			$this->db->insert("tr_celeb",$array);
			
			$res1 = $this->db->select('id_celeb')->order_by('id_celeb','desc')->limit(1)->get('tr_celeb')->row('id_celeb');
			
			$array2=array(
			"id_celeb"=>$res1
			);
			
			$this->db->where("id",$id);
			return	$this->db->update("data_selling",$array2);
			
		}else{
			
			if($sumber_selling_db == '3' or $sumber_listing_db == '3'){
				
				//----Versi Internal 1------//
				
				$background = imagecreatefromjpeg(''.base_url().'file_upload/image/celebInternal.jpg');
				$outputImage = $background;
				
					$img2 = ''.base_url().'file_upload/agen/'.$foto.'';
					$jabatanT = $jabatan_db;
					$idjab = $id_jabatan;
					
					$namaPisahx = explode(" ", $nama_db);
				
				$data1 = getimagesize($rumah);
				$width1 = $data1[0];
				$height1 = $data1[1];

				$data2 = getimagesize($img2);
				$width2 = $data2[0];
				$height2 = $data2[1];

				$first = imagecreatefromjpeg($rumah);
				$image_p1 = imagecreatetruecolor(1224, 1784);

				$second = imagecreatefrompng($img2);
				$image_p2 = imagecreatetruecolor(700, 700);

				//transparan
				$background = imagecolorallocatealpha($image_p2,0,0,0,0);
				imagecolortransparent($image_p2, $background);
				imagealphablending($image_p2, true);

				imagecopyresampled($image_p1, $first, 0, 0, 0, 0, 1224, 1784, $width1, $height1);
				imagecopyresampled($image_p2, $second, 0, 0, 0, 0, 700, 700, $width2, $height2);

				imagecopymerge($outputImage,$image_p1, 137, 1, 0, 0, 1224, 1784,100);
				imagecopymerge($outputImage,$image_p2, 1800, 680, 0, 0, 700, 700,100);

				//color font
				$biru = imagecolorallocate($outputImage, 69, 175, 175);
				$putih = imagecolorallocate($outputImage, 255, 255, 255);

				// Replace path by your own font path
				$fetish = '/home/mybe2/public_html/public_page_asset/assets/libraries/font/Fashion_Fetish_Heavy_0.ttf';
				$fetish2 = '/home/mybe2/public_html/public_page_asset/assets/libraries/font/Fashion_Fetish_Regular_0.ttf';
				$futura = '/home/mybe2/public_html/public_page_asset/assets/libraries/font/Futura_Heavy_font.ttf';

				//put text to image
				imagettftext($outputImage, 70, 0, 1970, 1490, $putih, $fetish, $namaPisahx[0]);
				if($idjab == '12'){
					imagettftext($outputImage, 48, 0, 1740, 1610, $putih, $fetish2, $jabatanT);
				}else{
					imagettftext($outputImage, 50, 0, 1820, 1610, $putih, $fetish2, $jabatanT);
				}
				imagettftext($outputImage, 60, 0, 1780, 1865, $putih, $fetish, $harga_db);
				imagettftext($outputImage, 60, 0, 1780, 2080, $putih, $fetish, $komisi_db);
				imagettftext($outputImage, 90, 0, 320, 2020, $biru, $fetish, $tgl);
				imagettftext($outputImage, 60, 0, 530, 2020, $biru, $fetish, $bln);
				imagettftext($outputImage, 60, 0, 210, 2160, $putih, $futura, $tempat_db);

				$desaincelebInternal = 'celebInternal_a_'.$id.'.jpg';
				
				imagejpeg($outputImage, '/home/mybe2/public_html/file_upload/img/'.$desaincelebInternal.'');

				imagedestroy($outputImage);
				
				//----Versi Internal 2------//
				
				$background = imagecreatefromjpeg(''.base_url().'file_upload/image/celebInternal.jpg');
				$outputImage = $background;
				
					$img2 = ''.base_url().'file_upload/agen/'.$foto2.'';
					$jabatanT = $jabatan2;
					$idjab = $id_jabatan2;
				
				$data1 = getimagesize($rumah);
				$width1 = $data1[0];
				$height1 = $data1[1];

				$data2 = getimagesize($img2);
				$width2 = $data2[0];
				$height2 = $data2[1];

				$first = imagecreatefromjpeg($rumah);
				$image_p1 = imagecreatetruecolor(1224, 1784);

				$second = imagecreatefrompng($img2);
				$image_p2 = imagecreatetruecolor(700, 700);

				//transparan
				$background = imagecolorallocatealpha($image_p2,0,0,0,0);
				imagecolortransparent($image_p2, $background);
				imagealphablending($image_p2, true);

				imagecopyresampled($image_p1, $first, 0, 0, 0, 0, 1224, 1784, $width1, $height1);
				imagecopyresampled($image_p2, $second, 0, 0, 0, 0, 700, 700, $width2, $height2);

				imagecopymerge($outputImage,$image_p1, 137, 1, 0, 0, 1224, 1784,100);
				imagecopymerge($outputImage,$image_p2, 1800, 680, 0, 0, 700, 700,100);

				//color font
				$biru = imagecolorallocate($outputImage, 69, 175, 175);
				$putih = imagecolorallocate($outputImage, 255, 255, 255);

				// Replace path by your own font path
				$fetish = '/home/mybe2/public_html/public_page_asset/assets/libraries/font/Fashion_Fetish_Heavy_0.ttf';
				$fetish2 = '/home/mybe2/public_html/public_page_asset/assets/libraries/font/Fashion_Fetish_Regular_0.ttf';
				$futura = '/home/mybe2/public_html/public_page_asset/assets/libraries/font/Futura_Heavy_font.ttf';
				
				$namaPisah2 = explode(" ", $namaagen);

				//put text to image
				imagettftext($outputImage, 70, 0, 1970, 1490, $putih, $fetish, $namaPisah2[0]);
				if($idjab == '12'){
					imagettftext($outputImage, 48, 0, 1740, 1610, $putih, $fetish2, $jabatanT);
				}else{
					imagettftext($outputImage, 50, 0, 1820, 1610, $putih, $fetish2, $jabatanT);
				}
				imagettftext($outputImage, 60, 0, 1780, 1865, $putih, $fetish, $harga_db);
				imagettftext($outputImage, 60, 0, 1780, 2080, $putih, $fetish, $komisi_db);
				imagettftext($outputImage, 90, 0, 320, 2020, $biru, $fetish, $tgl);
				imagettftext($outputImage, 60, 0, 530, 2020, $biru, $fetish, $bln);
				imagettftext($outputImage, 60, 0, 210, 2160, $putih, $futura, $tempat_db);

				$desaincelebInternal2 = 'celebInternal_b_'.$id.'.jpg';
				
				imagejpeg($outputImage, '/home/mybe2/public_html/file_upload/img/'.$desaincelebInternal2.'');

				imagedestroy($outputImage);
				
			//----Versi Sosmed------//
			
			$background = imagecreatefromjpeg(''.base_url().'file_upload/image/celebSosmed.jpg');
			$outputImage = $background;
			
			if($type_selling_db == 'jual'){
				$atas = ''.base_url().'file_upload/image/celeb-atas-sosmed.png';
			}else{
				$atas = ''.base_url().'file_upload/image/celeb-atas2-sosmed.png';
			}
			
			$kode1=date('dmYHis');
			$kode='cl-'.$kode1.'';
			
			$uagen = $this->uploadGambarPromo($kode,"uploadMarketing");
			$ulogo = $this->uploadGambarPromo($kode,"uploadLogo");
			
			if($sumber_listing_db == '1' && $sumber_selling_db == '2'){
				$agen = ''.base_url().'file_upload/agen/'.$foto_db.'';
				$logo = ''.base_url().'file_upload/image/logo.png';
				$agen2 = ''.base_url().'file_upload/img/'.$uagen.'';
				$logo2 = ''.base_url().'file_upload/img/'.$ulogo.'';
			}elseif($sumber_listing_db == '2' && $sumber_selling_db == '1'){
				$agen2 = ''.base_url().'file_upload/agen/'.$fotoagen.'';
				$logo2 = ''.base_url().'file_upload/image/logo.png';
				$agen = ''.base_url().'file_upload/img/'.$uagen.'';
				$logo = ''.base_url().'file_upload/img/'.$ulogo.'';
			}elseif($sumber_listing_db == '1' && $sumber_selling_db == '3'){
				$agen = ''.base_url().'file_upload/agen/'.$foto_db.'';
				$logo = ''.base_url().'file_upload/image/logo.png';
				$agen2 = ''.base_url().'file_upload/agen/'.$fotoagen.'';
				$logo2 = ''.base_url().'file_upload/image/logo.png';
			}

			$data1 = getimagesize($rumah);
			$width1 = $data1[0];
			$height1 = $data1[1];

			$data2 = getimagesize($atas);
			$width2 = $data2[0];
			$height2 = $data2[1];

			$data3 = getimagesize($bulet);
			$width3 = $data3[0];
			$height3 = $data3[1];

			$data4 = getimagesize($agen);
			$width4 = $data4[0];
			$height4 = $data4[1];

			$data5 = getimagesize($agen2);
			$width5 = $data5[0];
			$height5 = $data5[1];

			$data6 = getimagesize($logo);
			$width6 = $data6[0];
			$height6 = $data6[1];

			$data7 = getimagesize($logo2);
			$width7 = $data7[0];
			$height7 = $data7[1];
			
			$data8 = getimagesize($point);
			$width8 = $data8[0];
			$height8 = $data8[1];


			$first = imagecreatefromjpeg($rumah);
			$image_p1 = imagecreatetruecolor(2970, 2210);

			$second = imagecreatefrompng($atas);
			$image_p2 = imagecreatetruecolor(3000, 2220);

			$tiga = imagecreatefrompng($bulet);
			$image_p3 = imagecreatetruecolor(600, 600);

			$empat = imagecreatefrompng($agen);
			$image_p4 = imagecreatetruecolor(900, 900);

			$lima = imagecreatefrompng($agen2);
			$image_p5 = imagecreatetruecolor(900, 900);

			$enam = imagecreatefrompng($logo);
			$image_p6 = imagecreatetruecolor(500, 500);

			$tujuh = imagecreatefrompng($logo2);
			$image_p7 = imagecreatetruecolor(500, 500);
			
			$delapan = imagecreatefrompng($point);
			$image_p8 = imagecreatetruecolor(189, 182);

			//transparan
			$background = imagecolorallocatealpha($image_p2,0,0,0,0);
			imagecolortransparent($image_p2, $background);
			imagealphablending($image_p2, true);

			$background2 = imagecolorallocatealpha($image_p3,0,0,0,0);
			imagecolortransparent($image_p3, $background2);
			imagealphablending($image_p3, true);

			$background3 = imagecolorallocatealpha($image_p4,0,0,0,0);
			imagecolortransparent($image_p4, $background3);
			imagealphablending($image_p4, true);

			$background4 = imagecolorallocatealpha($image_p5,0,0,0,0);
			imagecolortransparent($image_p5, $background4);
			imagealphablending($image_p5, true);

			$background5 = imagecolorallocatealpha($image_p6,0,0,0,0);
			imagecolortransparent($image_p6, $background5);
			imagealphablending($image_p6, true);

			$background6 = imagecolorallocatealpha($image_p7,0,0,0,0);
			imagecolortransparent($image_p7, $background6);
			imagealphablending($image_p7, true);
			
			$background7 = imagecolorallocatealpha($image_p8,0,0,0,0);
			imagecolortransparent($image_p8, $background7);
			imagealphablending($image_p8, true);

			imagecopyresampled($image_p1, $first, 0, 0, 0, 0, 2970, 2210, $width1, $height1);
			imagecopyresampled($image_p2, $second, 0, 0, 0, 0, 3000, 2220, $width2, $height2);
			imagecopyresampled($image_p3, $tiga, 0, 0, 0, 0, 600, 600, $width3, $height3);
			imagecopyresampled($image_p4, $empat, 0, 0, 0, 0, 900, 900, $width4, $height4);
			imagecopyresampled($image_p5, $lima, 0, 0, 0, 0, 900, 900, $width5, $height5);
			imagecopyresampled($image_p6, $enam, 0, 0, 0, 0, 500, 500, $width6, $height6);
			imagecopyresampled($image_p7, $tujuh, 0, 0, 0, 0, 500, 500, $width7, $height7);
			imagecopyresampled($image_p8, $delapan, 0, 0, 0, 0, 189, 182, $width8, $height8);

			imagecopymerge($outputImage,$image_p1, 35, 1, 0, 0, 2970, 2210,100);
			imagecopymerge($outputImage,$image_p2, 35, 1, 0, 0, 3000, 2220,100);
			imagecopymerge($outputImage,$image_p3, 153, 1850, 0 , 0, 600, 600,100);
			imagecopymerge($outputImage,$image_p4, 970, 1790, 0, 0, 900, 900,100);
			imagecopymerge($outputImage,$image_p5, 1860, 1790, 0, 0, 900, 900,100);
			imagecopymerge($outputImage,$image_p6, 1620, 2250, 0, 0, 500, 500,100);
			imagecopymerge($outputImage,$image_p7, 2500, 2250, 0, 0, 500, 500,100);
			imagecopymerge($outputImage,$image_p8, 280, 2700, 0, 0, 189, 182,100);

			//color font
			$item = imagecolorallocate($outputImage, 50, 50, 50);
			$putih = imagecolorallocate($outputImage, 255, 255, 255);
			$kuning = imagecolorallocate($outputImage, 245, 197, 11);
			$abu = imagecolorallocate($outputImage, 198, 199, 203);

			// Replace path by your own font path			
			$kenyan = '/home/mybe2/public_html/public_page_asset/assets/libraries/font/kenyan_coffee_bd.ttf';
			$cafeta = '/home/mybe2/public_html/public_page_asset/assets/libraries/font/cafeta.ttf';
			
			$pisah = '&';
			if($sumber_listing_db == '1'){
				$namaxx = $nama_db;
			}elseif($sumber_selling_db == '1'){
				$namaxx = $namaagen;
			}
			
			$agenPisah = explode(" ",$namaxx);
			
			if($sumber_listing_db == '1' && $sumber_selling_db == '2'){
				$agenT1 = $agenPisah[0];
				$pisahx = explode(" ",$selling);
				$agenT2 = $pisahx[0];
			}elseif($sumber_listing_db == '2' && $sumber_selling_db == '1'){
				$pisah2 = explode(" ",$kode_agen);
				$agenT1 = $pisah2[0];
				$agenT2 = $agenPisah[0];
			}elseif($sumber_listing_db == '1' && $sumber_selling_db == '3'){
				$agenT1 = $agenPisah[0];
				$namaagenPisah = explode(" ", $namaagen);
				$agenT2 = $namaagenPisah[0];
			}

			/*$char = strlen($agenPisah[0]);
			$posisi = $char*10;
			$posisi2 = $posisi*8+20;
			$posisi3 = $posisi2+100;*/
			
			$namatulis = "".$agenT1." & ".$agenT2."";

			//put text to image
			imagettftext($outputImage, 150, 0, 360, 2090, $item, $kenyan, $tgl);
			if($tgl_pecah[1] == '1' or $tgl_pecah[1] == '2' or $tgl_pecah[1] == '8' or $tgl_pecah[1] == '9' or $tgl_pecah[1] == '10' or $tgl_pecah[1] == '11' or $tgl_pecah[1] == '12'){
				imagettftext($outputImage, 100, 0, 250, 2220, $item, $cafeta, $bln);
			}elseif($tgl_pecah[1] == '5'){
				imagettftext($outputImage, 110, 0, 350, 2220, $item, $cafeta, $bln);
			}elseif($tgl_pecah[1] == '6' or $tgl_pecah[1] == '7'){
				imagettftext($outputImage, 110, 0, 330, 2220, $item, $cafeta, $bln);
			}else{
				imagettftext($outputImage, 110, 0, 300, 2220, $item, $cafeta, $bln);
			}
			imagettftext($outputImage, 70, 0, 370, 2340, $item, $kenyan, $thn);
			/*imagettftext($outputImage, 130, 0, $posisi, 2630, $kuning, $cafeta, $agenT1);
			imagettftext($outputImage, 130, 0, $posisi2, 2630, $putih, $cafeta, $pisah);
			imagettftext($outputImage, 130, 0, $posisi3, 2630, $kuning, $cafeta, $agenT2);
			imagettftext($outputImage, 130, 0, 50, 2630, $kuning, $cafeta, $agenT1);
			imagettftext($outputImage, 130, 0, 500, 2630, $putih, $cafeta, $pisah);
			imagettftext($outputImage, 130, 0, 700, 2630, $kuning, $cafeta, $agenT2);*/
			imagettftext($outputImage, 130, 0, 100, 2630, $kuning, $cafeta, $namatulis);
			imagettftext($outputImage, 100, 0, 500, 2860, $kuning, $cafeta, $tempat_db);
			
			if($this->input->post("banklogo") <> ''){
				$usupported = ''.base_url().'file_upload/bank/'.$this->input->post("banklogo").'';
				
				$data_u = getimagesize($usupported);
				$width_u = $data_u[0];
				$height_u = $data_u[1];
			
				//$gambar_u = imagecreatefrompng($usupported);
				$gambar_u = imagecreatefromjpeg($usupported);
				$image_u = imagecreatetruecolor(3000, 2200);
				
				//$background_u = imagecolorallocatealpha($image_u,0,0,0,0);
				//imagecolortransparent($image_u, $background_u);
				//imagealphablending($image_u, true);
								
				imagecopyresampled($image_u, $gambar_u, 0, 0, 0, 0, 650, 450, $width_u, $height_u);
				imagecopymerge($outputImage,$image_u, 2340, 1120, 0, 0, 650, 450,100);
				
				imagettftext($outputImage, 70, 0, 2270, 1150, $item, $cafeta, "Supported by:");
			}
			
			$desaincelebSosmed = 'celebSosmed_'.$id.'.jpg';
		
			imagejpeg($outputImage, '/home/mybe2/public_html/file_upload/img/'.$desaincelebSosmed.'');
			
			imagedestroy($outputImage);
				
				$array3=array(
				"gambar_sosmed"=>$desaincelebSosmed,
				"gambar_internal"=>$desaincelebInternal,
				"gambar_internal2"=>$desaincelebInternal2
				);
				
				$this->db->insert("tr_celeb",$array3);
				
				$res1 = $this->db->select('id_celeb')->order_by('id_celeb','desc')->limit(1)->get('tr_celeb')->row('id_celeb');
				
				$array2=array(
				"id_celeb"=>$res1
				);
				
				$this->db->where("id",$id);
				return	$this->db->update("data_selling",$array2);
			}else{
				//----Versi Internal------//
				
				$background = imagecreatefromjpeg(''.base_url().'file_upload/image/celebInternal.jpg');
				$outputImage = $background;
				
				if($sumber_listing_db == '1'){
					$img2 = ''.base_url().'file_upload/agen/'.$foto.'';
					$jabatanT = $jabatan_db;
					$idjab = $id_jabatan;
				}elseif($sumber_selling_db == '1'){
					$img2 = ''.base_url().'file_upload/agen/'.$foto2.'';
					$jabatanT = $jabatan2;
					$idjab = $id_jabatan2;
				}
				
				if($sumber_listing_db == '1'){
					$namaxx = $nama_db;
				}elseif($sumber_selling_db == '1'){
					$namaxx = $namaagen;
				}
			
				$agenPisah = explode(" ",$namaxx);
				
				$data1 = getimagesize($rumah);
				$width1 = $data1[0];
				$height1 = $data1[1];

				$data2 = getimagesize($img2);
				$width2 = $data2[0];
				$height2 = $data2[1];

				$first = imagecreatefromjpeg($rumah);
				$image_p1 = imagecreatetruecolor(1224, 1784);

				$second = imagecreatefrompng($img2);
				$image_p2 = imagecreatetruecolor(700, 700);

				//transparan
				$background = imagecolorallocatealpha($image_p2,0,0,0,0);
				imagecolortransparent($image_p2, $background);
				imagealphablending($image_p2, true);

				imagecopyresampled($image_p1, $first, 0, 0, 0, 0, 1224, 1784, $width1, $height1);
				imagecopyresampled($image_p2, $second, 0, 0, 0, 0, 700, 700, $width2, $height2);

				imagecopymerge($outputImage,$image_p1, 137, 1, 0, 0, 1224, 1784,100);
				imagecopymerge($outputImage,$image_p2, 1800, 680, 0, 0, 700, 700,100);

				//color font
				$biru = imagecolorallocate($outputImage, 69, 175, 175);
				$putih = imagecolorallocate($outputImage, 255, 255, 255);

				// Replace path by your own font path
				$fetish = '/home/mybe2/public_html/public_page_asset/assets/libraries/font/Fashion_Fetish_Heavy_0.ttf';
				$fetish2 = '/home/mybe2/public_html/public_page_asset/assets/libraries/font/Fashion_Fetish_Regular_0.ttf';
				$futura = '/home/mybe2/public_html/public_page_asset/assets/libraries/font/Futura_Heavy_font.ttf';

				//put text to image
				imagettftext($outputImage, 70, 0, 1970, 1490, $putih, $fetish, $agenPisah[0]);
				if($idjab == '12'){
					imagettftext($outputImage, 48, 0, 1740, 1610, $putih, $fetish2, $jabatanT);
				}else{
					imagettftext($outputImage, 50, 0, 1820, 1610, $putih, $fetish2, $jabatanT);
				}
				imagettftext($outputImage, 60, 0, 1780, 1865, $putih, $fetish, $harga_db);
				imagettftext($outputImage, 60, 0, 1780, 2080, $putih, $fetish, $komisi_db);
				imagettftext($outputImage, 90, 0, 320, 2020, $biru, $fetish, $tgl);
				imagettftext($outputImage, 60, 0, 530, 2020, $biru, $fetish, $bln);
				imagettftext($outputImage, 60, 0, 210, 2160, $putih, $futura, $tempat_db);

				$desaincelebInternal = 'celebInternal_'.$id.'.jpg';
				
				imagejpeg($outputImage, '/home/mybe2/public_html/file_upload/img/'.$desaincelebInternal.'');

				imagedestroy($outputImage);
				
			//----Versi Sosmed------//
			
			$background = imagecreatefromjpeg(''.base_url().'file_upload/image/celebSosmed.jpg');
			$outputImage = $background;
			
			if($type_selling_db == 'jual'){
				$atas = ''.base_url().'file_upload/image/celeb-atas-sosmed.png';
			}else{
				$atas = ''.base_url().'file_upload/image/celeb-atas2-sosmed.png';
			}
			
			$kode1=date('dmYHis');
			$kode='cl-'.$kode1.'';
			
			$uagen = $this->uploadGambarPromo($kode,"uploadMarketing");
			$ulogo = $this->uploadGambarPromo($kode,"uploadLogo");
			
			if($sumber_listing_db == '1' && $sumber_selling_db == '2'){
				$agen = ''.base_url().'file_upload/agen/'.$foto_db.'';
				$logo = ''.base_url().'file_upload/image/logo.png';
				$agen2 = ''.base_url().'file_upload/img/'.$uagen.'';
				$logo2 = ''.base_url().'file_upload/img/'.$ulogo.'';
			}elseif($sumber_listing_db == '2' && $sumber_selling_db == '1'){
				$agen2 = ''.base_url().'file_upload/agen/'.$fotoagen.'';
				$logo2 = ''.base_url().'file_upload/image/logo.png';
				$agen = ''.base_url().'file_upload/img/'.$uagen.'';
				$logo = ''.base_url().'file_upload/img/'.$ulogo.'';
			}elseif($sumber_listing_db == '1' && $sumber_selling_db == '3'){
				$agen = ''.base_url().'file_upload/agen/'.$foto_db.'';
				$logo = ''.base_url().'file_upload/image/logo.png';
				$agen2 = ''.base_url().'file_upload/agen/'.$fotoagen.'';
				$logo2 = ''.base_url().'file_upload/image/logo.png';
			}

			$data1 = getimagesize($rumah);
			$width1 = $data1[0];
			$height1 = $data1[1];

			$data2 = getimagesize($atas);
			$width2 = $data2[0];
			$height2 = $data2[1];

			$data3 = getimagesize($bulet);
			$width3 = $data3[0];
			$height3 = $data3[1];

			$data4 = getimagesize($agen);
			$width4 = $data4[0];
			$height4 = $data4[1];

			$data5 = getimagesize($agen2);
			$width5 = $data5[0];
			$height5 = $data5[1];

			$data6 = getimagesize($logo);
			$width6 = $data6[0];
			$height6 = $data6[1];

			$data7 = getimagesize($logo2);
			$width7 = $data7[0];
			$height7 = $data7[1];
			
			$data8 = getimagesize($point);
			$width8 = $data8[0];
			$height8 = $data8[1];


			$first = imagecreatefromjpeg($rumah);
			$image_p1 = imagecreatetruecolor(2970, 2210);

			$second = imagecreatefrompng($atas);
			$image_p2 = imagecreatetruecolor(3000, 2230);

			$tiga = imagecreatefrompng($bulet);
			$image_p3 = imagecreatetruecolor(600, 600);

			$empat = imagecreatefrompng($agen);
			$image_p4 = imagecreatetruecolor(900, 900);

			$lima = imagecreatefrompng($agen2);
			$image_p5 = imagecreatetruecolor(900, 900);

			$enam = imagecreatefrompng($logo);
			$image_p6 = imagecreatetruecolor(500, 500);

			$tujuh = imagecreatefrompng($logo2);
			$image_p7 = imagecreatetruecolor(500, 500);
			
			$delapan = imagecreatefrompng($point);
			$image_p8 = imagecreatetruecolor(189, 182);

			//transparan
			$background = imagecolorallocatealpha($image_p2,0,0,0,0);
			imagecolortransparent($image_p2, $background);
			imagealphablending($image_p2, true);

			$background2 = imagecolorallocatealpha($image_p3,0,0,0,0);
			imagecolortransparent($image_p3, $background2);
			imagealphablending($image_p3, true);

			$background3 = imagecolorallocatealpha($image_p4,0,0,0,0);
			imagecolortransparent($image_p4, $background3);
			imagealphablending($image_p4, true);

			$background4 = imagecolorallocatealpha($image_p5,0,0,0,0);
			imagecolortransparent($image_p5, $background4);
			imagealphablending($image_p5, true);

			$background5 = imagecolorallocatealpha($image_p6,0,0,0,0);
			imagecolortransparent($image_p6, $background5);
			imagealphablending($image_p6, true);

			$background6 = imagecolorallocatealpha($image_p7,0,0,0,0);
			imagecolortransparent($image_p7, $background6);
			imagealphablending($image_p7, true);
			
			$background7 = imagecolorallocatealpha($image_p8,0,0,0,0);
			imagecolortransparent($image_p8, $background7);
			imagealphablending($image_p8, true);

			imagecopyresampled($image_p1, $first, 0, 0, 0, 0, 2970, 2210, $width1, $height1);
			imagecopyresampled($image_p2, $second, 0, 0, 0, 0, 3000, 2230, $width2, $height2);
			imagecopyresampled($image_p3, $tiga, 0, 0, 0, 0, 600, 600, $width3, $height3);
			imagecopyresampled($image_p4, $empat, 0, 0, 0, 0, 900, 900, $width4, $height4);
			imagecopyresampled($image_p5, $lima, 0, 0, 0, 0, 900, 900, $width5, $height5);
			imagecopyresampled($image_p6, $enam, 0, 0, 0, 0, 500, 500, $width6, $height6);
			imagecopyresampled($image_p7, $tujuh, 0, 0, 0, 0, 500, 500, $width7, $height7);
			imagecopyresampled($image_p8, $delapan, 0, 0, 0, 0, 189, 182, $width8, $height8);

			imagecopymerge($outputImage,$image_p1, 35, 1, 0, 0, 2970, 2210,100);
			imagecopymerge($outputImage,$image_p2, 35, 1, 0, 0, 3000, 2230,100);
			imagecopymerge($outputImage,$image_p3, 153, 1850, 0 , 0, 600, 600,100);
			imagecopymerge($outputImage,$image_p4, 970, 1782, 0, 0, 900, 900,100);
			imagecopymerge($outputImage,$image_p5, 1860, 1782, 0, 0, 900, 900,100);
			imagecopymerge($outputImage,$image_p6, 1610, 2254, 0, 0, 500, 500,100);
			imagecopymerge($outputImage,$image_p7, 2500, 2254, 0, 0, 500, 500,100);
			imagecopymerge($outputImage,$image_p8, 280, 2700, 0, 0, 189, 182,100);

			//color font
			$item = imagecolorallocate($outputImage, 50, 50, 50);
			$putih = imagecolorallocate($outputImage, 255, 255, 255);
			$kuning = imagecolorallocate($outputImage, 245, 197, 11);
			$abu = imagecolorallocate($outputImage, 198, 199, 203);

			// Replace path by your own font path			
			$kenyan = '/home/mybe2/public_html/public_page_asset/assets/libraries/font/kenyan_coffee_bd.ttf';
			$cafeta = '/home/mybe2/public_html/public_page_asset/assets/libraries/font/cafeta.ttf';
			
			$pisah = '&';
			if($sumber_listing_db == '1'){
				$namaxx = $nama_db;
			}elseif($sumber_selling_db == '1'){
				$namaxx = $namaagen;
			}
			
			$agenPisah = explode(" ",$namaxx);
			
			if($sumber_listing_db == '1' && $sumber_selling_db == '2'){
				$agenT1 = $agenPisah[0];
				$pisahx = explode(" ",$selling);
				$agenT2 = $pisahx[0];
			}elseif($sumber_listing_db == '2' && $sumber_selling_db == '1'){
				$pisah2 = explode(" ",$kode_agen);
				$agenT1 = $pisah2[0];
				$agenT2 = $agenPisah[0];
			}elseif($sumber_listing_db == '1' && $sumber_selling_db == '3'){
				$agenT1 = $agenPisah[0];
				$namaagenPisah = explode(" ", $namaagen);
				$agenT2 = $namaagenPisah[0];
			}

			/*$char = strlen($agenPisah[0]);
			$posisi = $char*10;
			$posisi2 = $posisi*8+20;
			$posisi3 = $posisi2+100;*/
			
			$namatulis = "".$agenT1." & ".$agenT2."";

			//put text to image
			imagettftext($outputImage, 150, 0, 360, 2090, $item, $kenyan, $tgl);
			if($tgl_pecah[1] == '1' or $tgl_pecah[1] == '2' or $tgl_pecah[1] == '8' or $tgl_pecah[1] == '9' or $tgl_pecah[1] == '10' or $tgl_pecah[1] == '11' or $tgl_pecah[1] == '12'){
				imagettftext($outputImage, 100, 0, 250, 2220, $item, $cafeta, $bln);
			}elseif($tgl_pecah[1] == '5'){
				imagettftext($outputImage, 110, 0, 350, 2220, $item, $cafeta, $bln);
			}elseif($tgl_pecah[1] == '6' or $tgl_pecah[1] == '7'){
				imagettftext($outputImage, 110, 0, 330, 2220, $item, $cafeta, $bln);
			}else{
				imagettftext($outputImage, 110, 0, 300, 2220, $item, $cafeta, $bln);
			}
			imagettftext($outputImage, 70, 0, 370, 2340, $item, $kenyan, $thn);
			/*imagettftext($outputImage, 130, 0, $posisi, 2630, $kuning, $cafeta, $agenT1);
			imagettftext($outputImage, 130, 0, $posisi2, 2630, $putih, $cafeta, $pisah);
			imagettftext($outputImage, 130, 0, $posisi3, 2630, $kuning, $cafeta, $agenT2);
			imagettftext($outputImage, 130, 0, 50, 2630, $kuning, $cafeta, $agenT1);
			imagettftext($outputImage, 130, 0, 500, 2630, $putih, $cafeta, $pisah);
			imagettftext($outputImage, 130, 0, 700, 2630, $kuning, $cafeta, $agenT2);*/
			imagettftext($outputImage, 130, 0, 100, 2630, $kuning, $cafeta, $namatulis);
			imagettftext($outputImage, 100, 0, 500, 2860, $kuning, $cafeta, $tempat_db);
			
			if($this->input->post("banklogo") <> ''){
				$usupported = ''.base_url().'file_upload/bank/'.$this->input->post("banklogo").'';
				
				$data_u = getimagesize($usupported);
				$width_u = $data_u[0];
				$height_u = $data_u[1];
			
				//$gambar_u = imagecreatefrompng($usupported);
				$gambar_u = imagecreatefromjpeg($usupported);
				$image_u = imagecreatetruecolor(3000, 2200);
				
				//$background_u = imagecolorallocatealpha($image_u,0,0,0,0);
				//imagecolortransparent($image_u, $background_u);
				//imagealphablending($image_u, true);
								
				imagecopyresampled($image_u, $gambar_u, 0, 0, 0, 0, 650, 450, $width_u, $height_u);
				imagecopymerge($outputImage,$image_u, 2340, 1120, 0, 0, 650, 450,100);
				
				//imagecopyresampled($image_u, $gambar_u, 0, 0, 0, 0, 500, 300, $width_u, $height_u);
				//imagecopymerge($outputImage,$image_u, 2370, 1150, 0, 0, 500, 300,100);
				
				imagettftext($outputImage, 70, 0, 2270, 1150, $item, $cafeta, "Supported by:");
			}
			
			$desaincelebSosmed = 'celebSosmed_'.$id.'.jpg';
		
			imagejpeg($outputImage, '/home/mybe2/public_html/file_upload/img/'.$desaincelebSosmed.'');
			
			imagedestroy($outputImage);
				
				$array3=array(
				"gambar_internal"=>$desaincelebInternal,
				"gambar_sosmed"=>$desaincelebSosmed
				);
				
				$this->db->insert("tr_celeb",$array3);
				
				$res1 = $this->db->select('id_celeb')->order_by('id_celeb','desc')->limit(1)->get('tr_celeb')->row('id_celeb');
				
				$array2=array(
				"id_celeb"=>$res1
				);
				
				$this->db->where("id",$id);
				return	$this->db->update("data_selling",$array2);
			}
		}
	}
	
	function createCelebTV(){
		$id = $this->input->post("id");
		
		$database="SELECT a.harga, a.total_komisi, a.nominal_komisi_listing, a.sumber_listing, a.sumber_selling, a.type_selling, a.kode_agen, a.selling,  d.id AS id_jabatan, b.desc, b.gambar1, c.nama, 
		c.poto2, c.hp, c.foto,  f.id AS id_jabatan2, d.nama AS jabatan, e.poto2 AS fotoagen, e.nama AS namaagen, e.hp AS hp2, f.nama AS jabatan2, e.foto AS foto2   
		FROM data_selling AS a 
		LEFT JOIN data_property AS b ON a.kode_listing = b.kode_prop 
		LEFT JOIN data_agen AS c ON a.kode_agen = c.kode_agen
		LEFT JOIN tr_jabatan AS d ON c.jabatan = d.id
        LEFT JOIN data_agen AS e ON a.selling = e.kode_agen
		LEFT JOIN tr_jabatan AS f ON e.jabatan = f.id
		WHERE a.id = '".$id."'";
		$database = $this->db->query($database)->result();
        foreach ($database as $val) {
			// The text to draw
			$harga_db = "Rp ".number_format($val->harga,0,",",".")."";
			$komisi_db = "Rp ".number_format($val->nominal_komisi_listing,0,",",".")."";
			$total_komisi_db = "Rp ".number_format($val->total_komisi,0,",",".")."";
			$sumber_listing_db = $val->sumber_listing;
			$sumber_selling_db = $val->sumber_selling;
			$tempat_db = $val->desc;
			$rumah_db = $val->gambar1;
			$nama_db = $val->nama;
			$foto_db = $val->poto2;
			$foto = $val->foto;
			$hp_db = $val->hp;
			$jabatan_db = $val->jabatan;
			$type_selling_db = $val->type_selling;
			$fotoagen = $val->fotoagen;
			$namaagen = $val->namaagen;
			$jabatan2 = $val->jabatan2;
			$hp2_db = $val->hp2;
			$foto2 = $val->foto2;
			$kode_agen = $val->kode_agen;
			$selling = $val->selling;
			$id_jabatan = $val->id_jabatan;
			$id_jabatan2 = $val->id_jabatan2;
		}
		$tgl_pecah = explode("/",$this->input->post("tgl_closing"));
		$tgl = $tgl_pecah[0];
		$bln = $this->konversi->bulan($tgl_pecah[1]);
		$thn = $tgl_pecah[2];
		
		if($sumber_listing_db == $sumber_selling_db){
			//----Versi TV-----//
			
			ini_set("memory_limit","1256M");
			ini_set('max_execution_time', 300);

			$background = imagecreatefromjpeg(''.base_url().'file_upload/image/celebTV.jpg');
			$outputImage = $background;
			
			if($type_selling_db == 'jual'){
				$atas = ''.base_url().'file_upload/image/celeb-atas-tv.png';
			}else{
				$atas = ''.base_url().'file_upload/image/celeb-atas2-tv.png';
			}

			$rumah = ''.base_url().'file_upload/img/'.$rumah_db.'';
			$bulet = ''.base_url().'file_upload/image/bulet.png';
			$agen = ''.base_url().'file_upload/agen/'.$foto_db.'';
			
			$namaTV = explode(" ",$nama_db);

			$data1 = getimagesize($rumah);
			$width1 = $data1[0];
			$height1 = $data1[1];

			$data2 = getimagesize($atas);
			$width2 = $data2[0];
			$height2 = $data2[1];

			$data3 = getimagesize($bulet);
			$width3 = $data3[0];
			$height3 = $data3[1];

			$data4 = getimagesize($agen);
			$width4 = $data4[0];
			$height4 = $data4[1];

			$first = imagecreatefromjpeg($rumah);
			$image_p1 = imagecreatetruecolor(6520, 6920);

			$second = imagecreatefrompng($atas);
			$image_p2 = imagecreatetruecolor(6600, 2500);

			$tiga = imagecreatefrompng($bulet);
			$image_p3 = imagecreatetruecolor(2000, 2000);

			$empat = imagecreatefrompng($agen);
			$image_p4 = imagecreatetruecolor(3000, 3000);

			//transparan
			$background = imagecolorallocatealpha($image_p2,0,0,0,0);
			imagecolortransparent($image_p2, $background);
			imagealphablending($image_p2, true);

			$background2 = imagecolorallocatealpha($image_p3,0,0,0,0);
			imagecolortransparent($image_p3, $background2);
			imagealphablending($image_p3, true);

			$background3 = imagecolorallocatealpha($image_p4,0,0,0,0);
			imagecolortransparent($image_p4, $background3);
			imagealphablending($image_p4, true);

			imagecopyresampled($image_p1, $first, 0, 0, 0, 0, 6520, 6920, $width1, $height1);
			imagecopyresampled($image_p2, $second, 0, 0, 0, 0, 6600, 2500, $width2, $height2);
			imagecopyresampled($image_p3, $tiga, 0, 0, 0, 0, 2000, 2000, $width3, $height3);
			imagecopyresampled($image_p4, $empat, 0, 0, 0, 0, 3000, 3000, $width4, $height4);

			imagecopymerge($outputImage,$image_p1,20,1812,0,0, 6520, 6920,100);
			imagecopymerge($outputImage,$image_p2,10,12,0,0, 6600, 2500,100);
			imagecopymerge($outputImage,$image_p3,4500,2800,0,0, 2000, 2000,100);
			imagecopymerge($outputImage,$image_p4,3590,7260,0,0, 3000, 3000,100);

			//color font
			$item = imagecolorallocate($outputImage, 50, 50, 50);
			$kuning = imagecolorallocate($outputImage, 245, 197, 11);
			$abu = imagecolorallocate($outputImage, 198, 199, 203);

			// Replace path by your own font path
			$kenyan = '/home/mybe2/public_html/public_page_asset/assets/libraries/font/kenyan_coffee_bd.ttf';
			$cafeta = '/home/mybe2/public_html/public_page_asset/assets/libraries/font/cafeta.ttf';

			//put text to image
			imagettftext($outputImage, 500, 0, 5200, 3600, $item, $kenyan, $tgl);
			if($tgl_pecah[1] == '1' or $tgl_pecah[1] == '2' or $tgl_pecah[1] == '8' or $tgl_pecah[1] == '9' or $tgl_pecah[1] == '10' or $tgl_pecah[1] == '11' or $tgl_pecah[1] == '12'){
				imagettftext($outputImage, 300, 0, 4850, 4000, $item, $cafeta, $bln);
			}elseif($tgl_pecah[1] == '5'){
				imagettftext($outputImage, 300, 0, 5250, 4000, $item, $cafeta, $bln);
			}elseif($tgl_pecah[1] == '6' or $tgl_pecah[1] == '7'){
				imagettftext($outputImage, 300, 0, 5200, 4000, $item, $cafeta, $bln);
			}else{
				imagettftext($outputImage, 300, 0, 5100, 4000, $item, $cafeta, $bln);
			}
			//imagettftext($outputImage, 300, 0, 4850, 4000, $item, $cafeta, $bln);
			imagettftext($outputImage, 200, 0, 5270, 4300, $item, $kenyan, $thn);
			imagettftext($outputImage, 300, 0, 900, 9770, $kuning, $kenyan, $tempat_db);
			imagettftext($outputImage, 150, 0, 470, 10220, $kuning, $cafeta, $namaTV[0]);
			if($id_jabatan == '12'){
				imagettftext($outputImage, 145, 0, 950, 10220, $abu, $cafeta, $jabatan_db);
				imagettftext($outputImage, 150, 0, 2400, 10220, $kuning, $cafeta, $hp_db);
			}else{
				imagettftext($outputImage, 150, 0, 1070, 10220, $abu, $cafeta, $jabatan_db);
				imagettftext($outputImage, 150, 0, 2300, 10220, $kuning, $cafeta, $hp_db);
			}
			
			//imagettftext($outputImage, 150, 0, 3300, 10220, $kuning, $cafeta, $tlp2);

			$desaincelebTV = 'celebTV_'.$id.'.jpg';
		
			imagejpeg($outputImage, '/home/mybe2/public_html/file_upload/img/'.$desaincelebTV.'');

			imagedestroy($outputImage);
			
			$res1 = $this->db->select('id_celeb')->order_by('id_celeb','desc')->limit(1)->get('tr_celeb')->row('id_celeb');
			
			$array=array(
				"gambar_tv"=>$desaincelebTV
			);
			
			$this->db->where("id_celeb",$res1);
			return	$this->db->update("tr_celeb",$array);
			
		}else{
			
			if($sumber_selling_db == '3' or $sumber_listing_db == '3'){
				
				$res1 = $this->db->select('id_celeb')->order_by('id_celeb','desc')->limit(1)->get('tr_celeb')->row('id_celeb');
				
				$array3=array(
				"gambar_tv"=>"celebTV_a_".$id.".jpg",
				"gambar_tv2"=>"celebTV_b_".$id.".jpg"
				);
				
				$this->db->where("id_celeb",$res1);
				$this->db->update("tr_celeb",$array3);
		
				//----Versi TV 1-----//
				ini_set("memory_limit","1256M");
				ini_set('max_execution_time', 400);

				$background = imagecreatefromjpeg(''.base_url().'file_upload/image/celebTV.jpg');
				$outputImage = $background;
				
				if($type_selling_db == 'jual'){
					$atas = ''.base_url().'file_upload/image/celeb-atas-tv.png';
				}else{
					$atas = ''.base_url().'file_upload/image/celeb-atas2-tv.png';
				}

				$rumah = ''.base_url().'file_upload/img/'.$rumah_db.'';
				$bulet = ''.base_url().'file_upload/image/bulet.png';
				
					$agen = ''.base_url().'file_upload/agen/'.$foto_db.'';
					$agenx = $nama_db;
					$jabatan_db = $jabatan_db;
					$fotox = ''.base_url().'file_upload/agen/'.$foto.'';
				

				$data1 = getimagesize($rumah);
				$width1 = $data1[0];
				$height1 = $data1[1];

				$data2 = getimagesize($atas);
				$width2 = $data2[0];
				$height2 = $data2[1];

				$data3 = getimagesize($bulet);
				$width3 = $data3[0];
				$height3 = $data3[1];

				$data4 = getimagesize($agen);
				$width4 = $data4[0];
				$height4 = $data4[1];

				$first = imagecreatefromjpeg($rumah);
				$image_p1 = imagecreatetruecolor(6520, 6920);

				$second = imagecreatefrompng($atas);
				$image_p2 = imagecreatetruecolor(6600, 2500);

				$tiga = imagecreatefrompng($bulet);
				$image_p3 = imagecreatetruecolor(2000, 2000);

				$empat = imagecreatefrompng($agen);
				$image_p4 = imagecreatetruecolor(3000, 3000);

				//transparan
				$background = imagecolorallocatealpha($image_p2,0,0,0,0);
				imagecolortransparent($image_p2, $background);
				imagealphablending($image_p2, true);

				$background2 = imagecolorallocatealpha($image_p3,0,0,0,0);
				imagecolortransparent($image_p3, $background2);
				imagealphablending($image_p3, true);

				$background3 = imagecolorallocatealpha($image_p4,0,0,0,0);
				imagecolortransparent($image_p4, $background3);
				imagealphablending($image_p4, true);

				imagecopyresampled($image_p1, $first, 0, 0, 0, 0, 6520, 6920, $width1, $height1);
				imagecopyresampled($image_p2, $second, 0, 0, 0, 0, 6600, 2500, $width2, $height2);
				imagecopyresampled($image_p3, $tiga, 0, 0, 0, 0, 2000, 2000, $width3, $height3);
				imagecopyresampled($image_p4, $empat, 0, 0, 0, 0, 3000, 3000, $width4, $height4);

				imagecopymerge($outputImage,$image_p1,20,1812,0,0, 6520, 6920,100);
				imagecopymerge($outputImage,$image_p2,10,12,0,0, 6600, 2500,100);
				imagecopymerge($outputImage,$image_p3,4500,2800,0,0, 2000, 2000,100);
				imagecopymerge($outputImage,$image_p4,3590,7260,0,0, 3000, 3000,100);

				//color font
				$item = imagecolorallocate($outputImage, 50, 50, 50);
				$kuning = imagecolorallocate($outputImage, 245, 197, 11);
				$abu = imagecolorallocate($outputImage, 198, 199, 203);

				// Replace path by your own font path
				$kenyan = '/home/mybe2/public_html/public_page_asset/assets/libraries/font/kenyan_coffee_bd.ttf';
				$cafeta = '/home/mybe2/public_html/public_page_asset/assets/libraries/font/cafeta.ttf';
				
				$namaPisah1 = explode(" ", $nama_db);

				//put text to image
				imagettftext($outputImage, 500, 0, 5200, 3600, $item, $kenyan, $tgl);
				if($tgl_pecah[1] == '1' or $tgl_pecah[1] == '2' or $tgl_pecah[1] == '8' or $tgl_pecah[1] == '9' or $tgl_pecah[1] == '10' or $tgl_pecah[1] == '11' or $tgl_pecah[1] == '12'){
					imagettftext($outputImage, 300, 0, 4850, 4000, $item, $cafeta, $bln);
				}elseif($tgl_pecah[1] == '5'){
					imagettftext($outputImage, 300, 0, 5250, 4000, $item, $cafeta, $bln);
				}elseif($tgl_pecah[1] == '6' or $tgl_pecah[1] == '7'){
					imagettftext($outputImage, 300, 0, 5200, 4000, $item, $cafeta, $bln);
				}else{
					imagettftext($outputImage, 300, 0, 5100, 4000, $item, $cafeta, $bln);
				}
				//imagettftext($outputImage, 300, 0, 4850, 4000, $item, $cafeta, $bln);
				imagettftext($outputImage, 200, 0, 5270, 4300, $item, $kenyan, $thn);
				imagettftext($outputImage, 300, 0, 900, 9770, $kuning, $kenyan, $tempat_db);
				imagettftext($outputImage, 150, 0, 470, 10220, $kuning, $cafeta, $namaPisah1[0]);
				if($id_jabatan == '12'){
					imagettftext($outputImage, 145, 0, 950, 10220, $abu, $cafeta, $jabatan_db);
					imagettftext($outputImage, 150, 0, 2400, 10220, $kuning, $cafeta, $hp_db);
				}else{
					imagettftext($outputImage, 150, 0, 1070, 10220, $abu, $cafeta, $jabatan_db);
					imagettftext($outputImage, 150, 0, 2300, 10220, $kuning, $cafeta, $hp_db);
				}
				//imagettftext($outputImage, 150, 0, 2300, 10220, $kuning, $cafeta, $hp_db);
				//imagettftext($outputImage, 150, 0, 3300, 10220, $kuning, $cafeta, $tlp2);

				$desaincelebTV = 'celebTV_a_'.$id.'.jpg';
			
				imagejpeg($outputImage, '/home/mybe2/public_html/file_upload/img/'.$desaincelebTV.'');

				imagedestroy($outputImage);
				
				//----Versi TV 2-----//
				ini_set("memory_limit","1256M");
				ini_set('max_execution_time', 400);

				$background = imagecreatefromjpeg(''.base_url().'file_upload/image/celebTV.jpg');
				$outputImage = $background;
				
				if($type_selling_db == 'jual'){
					$atas = ''.base_url().'file_upload/image/celeb-atas-tv.png';
				}else{
					$atas = ''.base_url().'file_upload/image/celeb-atas2-tv.png';
				}

				$rumah = ''.base_url().'file_upload/img/'.$rumah_db.'';
				$bulet = ''.base_url().'file_upload/image/bulet.png';
				
					$agen = ''.base_url().'file_upload/agen/'.$fotoagen.'';
					$agenx = $namaagen;
					$jabatan_db = $jabatan2;
					$fotox = ''.base_url().'file_upload/agen/'.$foto2.'';

				$data1 = getimagesize($rumah);
				$width1 = $data1[0];
				$height1 = $data1[1];

				$data2 = getimagesize($atas);
				$width2 = $data2[0];
				$height2 = $data2[1];

				$data3 = getimagesize($bulet);
				$width3 = $data3[0];
				$height3 = $data3[1];

				$data4 = getimagesize($agen);
				$width4 = $data4[0];
				$height4 = $data4[1];

				$first = imagecreatefromjpeg($rumah);
				$image_p1 = imagecreatetruecolor(6520, 6920);

				$second = imagecreatefrompng($atas);
				$image_p2 = imagecreatetruecolor(6600, 2500);

				$tiga = imagecreatefrompng($bulet);
				$image_p3 = imagecreatetruecolor(2000, 2000);

				$empat = imagecreatefrompng($agen);
				$image_p4 = imagecreatetruecolor(3000, 3000);

				//transparan
				$background = imagecolorallocatealpha($image_p2,0,0,0,0);
				imagecolortransparent($image_p2, $background);
				imagealphablending($image_p2, true);

				$background2 = imagecolorallocatealpha($image_p3,0,0,0,0);
				imagecolortransparent($image_p3, $background2);
				imagealphablending($image_p3, true);

				$background3 = imagecolorallocatealpha($image_p4,0,0,0,0);
				imagecolortransparent($image_p4, $background3);
				imagealphablending($image_p4, true);

				imagecopyresampled($image_p1, $first, 0, 0, 0, 0, 6520, 6920, $width1, $height1);
				imagecopyresampled($image_p2, $second, 0, 0, 0, 0, 6600, 2500, $width2, $height2);
				imagecopyresampled($image_p3, $tiga, 0, 0, 0, 0, 2000, 2000, $width3, $height3);
				imagecopyresampled($image_p4, $empat, 0, 0, 0, 0, 3000, 3000, $width4, $height4);

				imagecopymerge($outputImage,$image_p1,20,1812,0,0, 6520, 6920,100);
				imagecopymerge($outputImage,$image_p2,10,12,0,0, 6600, 2500,100);
				imagecopymerge($outputImage,$image_p3,4500,2800,0,0, 2000, 2000,100);
				imagecopymerge($outputImage,$image_p4,3590,7260,0,0, 3000, 3000,100);

				//color font
				$item = imagecolorallocate($outputImage, 50, 50, 50);
				$kuning = imagecolorallocate($outputImage, 245, 197, 11);
				$abu = imagecolorallocate($outputImage, 198, 199, 203);

				// Replace path by your own font path
				$kenyan = '/home/mybe2/public_html/public_page_asset/assets/libraries/font/kenyan_coffee_bd.ttf';
				$cafeta = '/home/mybe2/public_html/public_page_asset/assets/libraries/font/cafeta.ttf';
				
				$namaPisah2 = explode(" ", $namaagen);

				//put text to image
				imagettftext($outputImage, 500, 0, 5200, 3600, $item, $kenyan, $tgl);
				if($tgl_pecah[1] == '1' or $tgl_pecah[1] == '2' or $tgl_pecah[1] == '8' or $tgl_pecah[1] == '9' or $tgl_pecah[1] == '10' or $tgl_pecah[1] == '11' or $tgl_pecah[1] == '12'){
					imagettftext($outputImage, 300, 0, 4850, 4000, $item, $cafeta, $bln);
				}elseif($tgl_pecah[1] == '5'){
					imagettftext($outputImage, 300, 0, 5250, 4000, $item, $cafeta, $bln);
				}elseif($tgl_pecah[1] == '6' or $tgl_pecah[1] == '7'){
					imagettftext($outputImage, 300, 0, 5200, 4000, $item, $cafeta, $bln);
				}else{
					imagettftext($outputImage, 300, 0, 5100, 4000, $item, $cafeta, $bln);
				}
				//imagettftext($outputImage, 300, 0, 4850, 4000, $item, $cafeta, $bln);
				imagettftext($outputImage, 200, 0, 5270, 4300, $item, $kenyan, $thn);
				imagettftext($outputImage, 300, 0, 900, 9770, $kuning, $kenyan, $tempat_db);
				imagettftext($outputImage, 150, 0, 470, 10220, $kuning, $cafeta, $namaPisah2[0]);
				if($id_jabatan == '12'){
					imagettftext($outputImage, 145, 0, 950, 10220, $abu, $cafeta, $jabatan_db);
					imagettftext($outputImage, 150, 0, 2400, 10220, $kuning, $cafeta, $hp2_db);
				}else{
					imagettftext($outputImage, 150, 0, 1070, 10220, $abu, $cafeta, $jabatan_db);
					imagettftext($outputImage, 150, 0, 2300, 10220, $kuning, $cafeta, $hp2_db);
				}
				//imagettftext($outputImage, 150, 0, 2300, 10220, $kuning, $cafeta, $hp_db);
				//imagettftext($outputImage, 150, 0, 3300, 10220, $kuning, $cafeta, $tlp2);

				$desaincelebTV2 = 'celebTV_b_'.$id.'.jpg';
			
				imagejpeg($outputImage, '/home/mybe2/public_html/file_upload/img/'.$desaincelebTV2.'');

				imagedestroy($outputImage);

			}else{
				
				//----Versi TV-----//
				ini_set("memory_limit","1256M");
				ini_set('max_execution_time', 300);

				$background = imagecreatefromjpeg(''.base_url().'file_upload/image/celebTV.jpg');
				$outputImage = $background;
				
				if($type_selling_db == 'jual'){
					$atas = ''.base_url().'file_upload/image/celeb-atas-tv.png';
				}else{
					$atas = ''.base_url().'file_upload/image/celeb-atas2-tv.png';
				}

				$rumah = ''.base_url().'file_upload/img/'.$rumah_db.'';
				$bulet = ''.base_url().'file_upload/image/bulet.png';
				
				if($sumber_listing_db == '1'){
					$agen = ''.base_url().'file_upload/agen/'.$foto_db.'';
					$jabatan = $jabatan_db;
					$namaPisahx = explode(" ", $nama_db);
					$nohp = $hp_db;
					$idjab = $id_jabatan;
				}elseif($sumber_selling_db == '1'){
					$agen = ''.base_url().'file_upload/agen/'.$fotoagen.'';
					$jabatan = $jabatan2;
					$namaPisahx = explode(" ", $namaagen);
					$nohp = $hp2_db;
					$idjab = $id_jabatan2;
				}

				$data1 = getimagesize($rumah);
				$width1 = $data1[0];
				$height1 = $data1[1];

				$data2 = getimagesize($atas);
				$width2 = $data2[0];
				$height2 = $data2[1];

				$data3 = getimagesize($bulet);
				$width3 = $data3[0];
				$height3 = $data3[1];

				$data4 = getimagesize($agen);
				$width4 = $data4[0];
				$height4 = $data4[1];

				$first = imagecreatefromjpeg($rumah);
				$image_p1 = imagecreatetruecolor(6520, 6920);

				$second = imagecreatefrompng($atas);
				$image_p2 = imagecreatetruecolor(6600, 2500);

				$tiga = imagecreatefrompng($bulet);
				$image_p3 = imagecreatetruecolor(2000, 2000);

				$empat = imagecreatefrompng($agen);
				$image_p4 = imagecreatetruecolor(3000, 3000);

				//transparan
				$background = imagecolorallocatealpha($image_p2,0,0,0,0);
				imagecolortransparent($image_p2, $background);
				imagealphablending($image_p2, true);

				$background2 = imagecolorallocatealpha($image_p3,0,0,0,0);
				imagecolortransparent($image_p3, $background2);
				imagealphablending($image_p3, true);

				$background3 = imagecolorallocatealpha($image_p4,0,0,0,0);
				imagecolortransparent($image_p4, $background3);
				imagealphablending($image_p4, true);

				imagecopyresampled($image_p1, $first, 0, 0, 0, 0, 6520, 6920, $width1, $height1);
				imagecopyresampled($image_p2, $second, 0, 0, 0, 0, 6600, 2500, $width2, $height2);
				imagecopyresampled($image_p3, $tiga, 0, 0, 0, 0, 2000, 2000, $width3, $height3);
				imagecopyresampled($image_p4, $empat, 0, 0, 0, 0, 3000, 3000, $width4, $height4);

				imagecopymerge($outputImage,$image_p1,20,1812,0,0, 6520, 6920,100);
				imagecopymerge($outputImage,$image_p2,10,12,0,0, 6600, 2500,100);
				imagecopymerge($outputImage,$image_p3,4500,2800,0,0, 2000, 2000,100);
				imagecopymerge($outputImage,$image_p4,3590,7260,0,0, 3000, 3000,100);

				//color font
				$item = imagecolorallocate($outputImage, 50, 50, 50);
				$kuning = imagecolorallocate($outputImage, 245, 197, 11);
				$abu = imagecolorallocate($outputImage, 198, 199, 203);

				// Replace path by your own font path
				$kenyan = '/home/mybe2/public_html/public_page_asset/assets/libraries/font/kenyan_coffee_bd.ttf';
				$cafeta = '/home/mybe2/public_html/public_page_asset/assets/libraries/font/cafeta.ttf';

				//put text to image
				imagettftext($outputImage, 500, 0, 5200, 3600, $item, $kenyan, $tgl);
				if($tgl_pecah[1] == '1' or $tgl_pecah[1] == '2' or $tgl_pecah[1] == '8' or $tgl_pecah[1] == '9' or $tgl_pecah[1] == '10' or $tgl_pecah[1] == '11' or $tgl_pecah[1] == '12'){
					imagettftext($outputImage, 300, 0, 4850, 4000, $item, $cafeta, $bln);
				}elseif($tgl_pecah[1] == '5'){
					imagettftext($outputImage, 300, 0, 5250, 4000, $item, $cafeta, $bln);
				}elseif($tgl_pecah[1] == '6' or $tgl_pecah[1] == '7'){
					imagettftext($outputImage, 300, 0, 5200, 4000, $item, $cafeta, $bln);
				}else{
					imagettftext($outputImage, 300, 0, 5100, 4000, $item, $cafeta, $bln);
				}
				//imagettftext($outputImage, 300, 0, 4850, 4000, $item, $cafeta, $bln);
				imagettftext($outputImage, 200, 0, 5270, 4300, $item, $kenyan, $thn);
				imagettftext($outputImage, 300, 0, 900, 9770, $kuning, $kenyan, $tempat_db);
				imagettftext($outputImage, 150, 0, 470, 10220, $kuning, $cafeta, $namaPisahx[0]);
				if($idjab == '12'){
					imagettftext($outputImage, 145, 0, 950, 10220, $abu, $cafeta, $jabatan);
					imagettftext($outputImage, 150, 0, 2400, 10220, $kuning, $cafeta, $nohp);
				}else{
					imagettftext($outputImage, 150, 0, 1070, 10220, $abu, $cafeta, $jabatan);
					imagettftext($outputImage, 150, 0, 2300, 10220, $kuning, $cafeta, $nohp);
				}
				//imagettftext($outputImage, 150, 0, 3300, 10220, $kuning, $cafeta, $tlp2);

				$desaincelebTV = 'celebTV_'.$id.'.jpg';
			
				imagejpeg($outputImage, '/home/mybe2/public_html/file_upload/img/'.$desaincelebTV.'');

				imagedestroy($outputImage);
				
				$res1 = $this->db->select('id_celeb')->order_by('id_celeb','desc')->limit(1)->get('tr_celeb')->row('id_celeb');
				
				$array3=array(
				"gambar_tv"=>$desaincelebTV
				);
				
				$this->db->where("id_celeb",$res1);
				return $this->db->update("tr_celeb",$array3);
			}
			/*
			if($sumber_selling_db == '3' or $sumber_listing_db == '3'){
				$desaincelebTVx = file_get_contents("".base_url()."file_upload/img/celebTV_a_".$id.".jpg");
				$desaincelebTV2x = file_get_contents("".base_url()."file_upload/img/celebTV_b_".$id.".jpg");
			}else{
				$desaincelebTVx = file_get_contents("".base_url()."file_upload/img/celebTV_".$id.".jpg");
			    $desaincelebTV2x = ' ';
			}
			
			$res1 = $this->db->select('id_celeb')->order_by('id_celeb','desc')->limit(1)->get('tr_celeb')->row('id_celeb');
				
				$array3=array(
				"gambar_tv"=>$desaincelebTVx,
				"gambar_tv2"=>$desaincelebTV2x
				);
				
				$this->db->where("id_celeb",$res1);
				return $this->db->update("tr_celeb",$array3);*/
		}
	}
	
	function uploadGambarPromo($kode,$form)
	{
		if(isset($_FILES[$form]['type']))
		{
		return	$this->upload_imgPromo($kode,$form);
		}else{
		return $foto_promo = 'not-avaliable.jpg';
		}
	}
	public function upload_imgPromo($kode,$form)
	{	//$this->m_konfig->log("admin","Upload photo");
		///
			$nama=date("YmdHis");
		  $lokasi_file = $_FILES[$form]['tmp_name'];
		  $tipe_file   = $_FILES[$form]['type'];
		  $nama_file   = $_FILES[$form]['name'];
		  //if($tipe_file)
		  //{
		//  $daprof=$this->getGambarkode($kode);
		//	if($daprof!="")
		//	 {
		//		 $path = "file_upload/barang/".$daprof;
		//		 if (file_exists($path)) {
		//			unlink($path);
		//		 }
		//	 }
		  
		  
			//$jenis=explode(".",$nama_file);
			$nama_file=str_replace(" ","_",$nama_file);
			// $jenis="jpg";
			$nama=str_replace("/","",$kode.$nama.$nama_file);
			 $target_path = "file_upload/img/".$nama;
			 //
	//	  }
		  //
		if (!empty($lokasi_file)) {
		move_uploaded_file($lokasi_file,$target_path);
		//if($jenis=="png"){
		//$this->konversi->UploadImageResize($target_path,$jenis,200);
		}
	//	$this->reff->UploadImageResize($target_path,"jpg",800);
		return $nama;
	}
	
	function insertPromo()
	{
	$kode1=date('dmYHis');
	$kode='pr-'.$kode1.'';
	$data=array(
	"kode_prop"=>$this->input->post("kode_prop"),
	"alamat_promo"=>$this->input->post("alamat_promo"),
	"tgl_promo"=>$this->tanggal->eng_($this->input->post("tgl_promo"),"-"),
	"foto_promo"=>$this->uploadGambarPromo($kode,"foto_promo"),
	"status"=>$this->input->post("status"),
	"jenis_promo"=>$this->input->post("jenis_promo"),
	);
	
	return $this->db->insert("data_promo",$data);


	}
	
	/*---------------------------------------------*/
		
	function get_dataPromo($id)
	{
		 $query = $this->_get_dataPromo($id);
        if ($_POST['length'] != -1)
            $query .= " limit " . $_POST['start'] . "," . $_POST['length'];
        return $this->db->query($query)->result();
 
	}
	  public function countsList($id) {
        $query = $this->_get_dataPromo($id);
        return $this->db->query($query)->num_rows();
    }
	function _get_dataPromo($id)
	{
       	if($this->session->userdata("id")==64){
			$query = "SELECT a.*, b.alamat_detail, b.area_listing, c.nama FROM data_promo AS a 
			LEFT JOIN data_property AS b ON a.kode_prop = b.kode_prop
			LEFT JOIN data_agen AS c ON b.agen = c.kode_agen WHERE b.id_prop =$id AND b.agen='BREA/001/I/2017'";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				a.kode_prop LIKE '%" . $searchkey . "%' or
				nama LIKE '%" . $searchkey . "%' or 
				alamat_detail LIKE '%" . $searchkey . "%' or
				alamat_promo LIKE '%" . $searchkey . "%' or
				area_listing LIKE '%" . $searchkey . "%' 
				) ";
			}
		}elseif($this->session->userdata("id")==126){
			$query = "SELECT a.*, b.alamat_detail, b.area_listing, c.nama FROM data_promo AS a 
			LEFT JOIN data_property AS b ON a.kode_prop = b.kode_prop
			LEFT JOIN data_agen AS c ON b.agen = c.kode_agen WHERE b.id_prop =$id AND b.agen IN ('BREA/024/V/2017', 'BREA/010/I/2017', 'BREA/012/VIII/2017', 'BREA/005/II/2017')";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				a.kode_prop LIKE '%" . $searchkey . "%' or
				nama LIKE '%" . $searchkey . "%' or 
				alamat_detail LIKE '%" . $searchkey . "%' or
				alamat_promo LIKE '%" . $searchkey . "%' or
				area_listing LIKE '%" . $searchkey . "%' 
				) ";
			}
		}elseif($this->session->userdata("id")==127){
			$query = "SELECT a.*, b.alamat_detail, b.area_listing, c.nama FROM data_promo AS a 
			LEFT JOIN data_property AS b ON a.kode_prop = b.kode_prop
			LEFT JOIN data_agen AS c ON b.agen = c.kode_agen WHERE b.id_prop =$id AND b.agen IN ('BREA/039/I/2018', 'BREA/046/II/2018', 'BREA/045/II/2018', 'BREA/007/II/2017', 'BREA/047/II/2018')";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				a.kode_prop LIKE '%" . $searchkey . "%' or
				nama LIKE '%" . $searchkey . "%' or 
				alamat_detail LIKE '%" . $searchkey . "%' or
				alamat_promo LIKE '%" . $searchkey . "%' or
				area_listing LIKE '%" . $searchkey . "%' 
				) ";
			}
		}elseif($this->session->userdata("id")==146){
			$query = "SELECT a.*, b.alamat_detail, b.area_listing, c.nama FROM data_promo AS a 
			LEFT JOIN data_property AS b ON a.kode_prop = b.kode_prop
			LEFT JOIN data_agen AS c ON b.agen = c.kode_agen WHERE b.id_prop =$id AND b.agen IN ('BREA/032/XI/2017')";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				a.kode_prop LIKE '%" . $searchkey . "%' or
				nama LIKE '%" . $searchkey . "%' or 
				alamat_detail LIKE '%" . $searchkey . "%' or
				alamat_promo LIKE '%" . $searchkey . "%' or
				area_listing LIKE '%" . $searchkey . "%' 
				) ";
			}
		}else{
			$query = "SELECT a.*, b.alamat_detail, b.area_listing, c.nama FROM data_promo AS a 
			LEFT JOIN data_property AS b ON a.kode_prop = b.kode_prop
			LEFT JOIN data_agen AS c ON b.agen = c.kode_agen WHERE b.id_prop =$id ";
			if (isset($_POST['search']['value'])) {
				$searchkey = $_POST['search']['value'];
				$query .= " AND (
				a.kode_prop LIKE '%" . $searchkey . "%' or
				nama LIKE '%" . $searchkey . "%' or 
				alamat_detail LIKE '%" . $searchkey . "%' or
				alamat_promo LIKE '%" . $searchkey . "%' or
				area_listing LIKE '%" . $searchkey . "%' 
				) ";
			}
		}

       $column = array('');
        $i = 0;
        foreach ($column as $item) {
            $column[$i] = $item;
        }

      /*  if (isset($_POST['order'])) {
		//	$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            $query .= " order by " . $column[$_POST['order']['0']['column']] . " " . $_POST['order']['0']['dir'];
        } else if (isset($order)) {
            $order = $order;
			//	$this->db->order_by(key($order), $order[key($order)]);
			       $query .= " order by nama ASC";
        }*/
		$query.=" order by tgl_promo DESC" ;
        return $query;
	}
	
	/*--------------------CMA-------------------------*/
	function get_dataCMA()
	{
		 $query = $this->_get_dataCMA();
        if ($_POST['length'] != -1)
            $query .= " limit " . $_POST['start'] . "," . $_POST['length'];
        return $this->db->query($query)->result();
 
	}
	  public function counts_t() {
        $query = $this->_get_dataCMA();
        return $this->db->query($query)->num_rows();
    }
	function _get_dataCMA()
	{
        $query = "SELECT * FROM tr_cma WHERE 1=1 ";
        if (isset($_POST['search']['value'])) {
            $searchkey = $_POST['search']['value'];
            $query .= " AND (
			nama_area LIKE '%" . $searchkey . "%'  
			) ";
        }

        $column = array('', '', 'nama_area', 'harga_area', 'harga_bangunan');
        $i = 0;
        foreach ($column as $item) {
            $column[$i] = $item;
        }

      /*  if (isset($_POST['order'])) {
		//	$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            $query .= " order by " . $column[$_POST['order']['0']['column']] . " " . $_POST['order']['0']['dir'];
        } else if (isset($order)) {
            $order = $order;
			//	$this->db->order_by(key($order), $order[key($order)]);
			       $query .= " order by nama ASC";
        }*/
		$query.=" order by id_cma DESC" ;
        return $query;
	}
	
	function simpan_cma()
	{
		$array=array(
		"id_prov"=>$this->input->post("provinsi"),
		"id_kab"=>$this->input->post("kabupaten"),
		"nama_area"=>$this->input->post("areax"),
		"lat_area"=>$la=$this->input->post("lat_areax"),
		"log_area"=>$lo=$this->input->post("long_areax"),
		"harga_area"=>$hrg=str_replace(".","",$this->input->post("harga_area")),
		//"harga_bangunan"=>$hrg=str_replace(".","",$this->input->post("harga_bangunan")),
		);
		
		if($this->input->post("harga_area") == '' && $this->input->post("harga_bangunan") == ''){
			return "kurang";
		}else{
		
			$cek=$this->_cekListingCMA($la,$lo);
			if($cek)
			{
				return false;
			}else{
				return	$this->db->insert("tr_cma",$array);
			}
		}

	}
	
	function update_cma()
	{
		$array=array(
		"id_prov"=>$this->input->post("provinsi"),
		"id_kab"=>$this->input->post("kabupaten"),
		"nama_area"=>$this->input->post("areax"),
		"lat_area"=>$this->input->post("lat_areax"),
		"log_area"=>$this->input->post("long_areax"),
		"harga_area"=>str_replace(".","",$this->input->post("harga_area")),
		//"harga_bangunan"=>$hrg=str_replace(".","",$this->input->post("harga_bangunan")),
		);
		if($this->input->post("harga_area") == '' && $this->input->post("harga_bangunan") == ''){
			return "kurang";
		}else{
			$this->db->where("id_cma",$id=$this->input->post("id"));
			return	$this->db->update("tr_cma",$array);
		}
	}
	
	function _cekListingCMA($la,$lo)
	{
	    $this->db->where("lat_area",$la);
		$this->db->where("log_area",$lo);
	 return   $this->db->get("tr_cma")->num_rows();
	}
	
	function get_cma($id)
	{
		$this->db->where("id_cma",$id);
		$datax=$this->db->get("tr_cma")->row();
	return $datax;
	}
	
	function hapus_cma()
	{
		$this->db->where("id_cma",$this->input->post("id"));
		return	$this->db->delete("tr_cma");
	}
	
	function HapusAllCMA()
	{
		$hapus=$this->input->post("hapus");
		foreach($hapus as $id)
		{
		$this->db->where("id_cma",$id);
		$this->db->delete("tr_cma");
		}	return true;
	}
	
	function insertHasilCMA()
	{	$kode_prop=$this->input->post("kode_prop");
		$array=array(
		"harga_area"=>str_replace(".","",$this->input->post("harga_area")),
		"harga_cma"=>str_replace(".","",$this->input->post("harga_cma")),
		"harga_bangunan"=>str_replace(".","",$this->input->post("harga_bangunan")),
		"hasil_cma"=>$this->input->post("hasil_cma"),
		);
		if($this->input->post("harga_cma") == '0'){
			return false;
		}else{
		$this->db->where("kode_prop",$kode_prop);
		return	$this->db->update("data_property",$array);
		}
	}
	
	function editHasilCMA()
	{	$kodeprop=$this->input->post("kode_prop");
		$array=array(
		"harga_area"=>str_replace(".","",$this->input->post("harga_area")),
		"harga_cma"=>str_replace(".","",$this->input->post("harga_cma")),
		"harga_bangunan"=>str_replace(".","",$this->input->post("harga_bangunan")),
		"hasil_cma"=>$this->input->post("hasil_cma"),
		);
		if($this->input->post("harga_cma") == '0'){
			return false;
		}else{
		$this->db->where("kode_prop",$kodeprop);
		return	$this->db->update("data_property",$array);
		}
	}

}