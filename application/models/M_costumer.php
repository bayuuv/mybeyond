<?php

class M_costumer extends CI_Model  {
    
		
	function __construct()
    {
        parent::__construct();
		
    }
	/*---------------------------------------------*/
	function ajax_listing()
	{
		 $query = $this->_get_dataListing();
        if ($_POST['length'] != -1)
            $query .= " limit " . $_POST['start'] . "," . $_POST['length'];
        return $this->db->query($query)->result();
 
	}
	 public function counts_listing() {
        $query = $this->_get_dataListing();
        return $this->db->query($query)->num_rows();
    }
	function _get_dataListing()
	{
		$id_agen=$this->uri->segment(3);
		$filter="";
		if($id_agen){ $filter="id_agen='".$id_agen."'";}
        $query = "SELECT * FROM report_listing WHERE 1=1 AND $filter ";
        if (isset($_POST['search']['value'])) {
            $searchkey = $_POST['search']['value'];
            $query .= " AND (
			kode_listing LIKE '%" . $searchkey . "%' or 
			ket LIKE '%" . $searchkey . "%' or
			tgl LIKE '%" . $searchkey . "%'
			) ";
        }

        $column = array('');
        $i = 0;
        foreach ($column as $item) {
            $column[$i] = $item;
        }
		$query.=" order by tgl DESC" ;
        return $query;
	}
	/*---------------------------------------------*/
	
	/*---------------------------------------------*/
	function ajax_costumer()
	{
		 $query = $this->_get_dataCostumer();
        if ($_POST['length'] != -1)
            $query .= " limit " . $_POST['start'] . "," . $_POST['length'];
        return $this->db->query($query)->result();
 
	}
	 public function counts_costumer() {
        $query = $this->_get_dataCostumer();
        return $this->db->query($query)->num_rows();
    }
	function _get_dataCostumer()
	{
		$id_agen=$this->uri->segment(3);
		$filter="";
		if($id_agen){ $filter="id_agen='".$id_agen."'";}
        $query = "SELECT * FROM report_costumer WHERE 1=1 AND $filter ";
        if (isset($_POST['search']['value'])) {
            $searchkey = $_POST['search']['value'];
            $query .= " AND (
			ket LIKE '%" . $searchkey . "%' or
			tgl LIKE '%" . $searchkey . "%'
			) ";
        }

        $column = array('');
        $i = 0;
        foreach ($column as $item) {
            $column[$i] = $item;
        }
		$query.=" order by tgl DESC" ;
        return $query;
	}
	/*---------------------------------------------*/
	
	function get_dataAgen()
	{
		 $query = $this->_get_dataAgen();
        if ($_POST['length'] != -1)
            $query .= " limit " . $_POST['start'] . "," . $_POST['length'];
        return $this->db->query($query)->result();
 
	}
	
	  public function counts() {
        $query = $this->_get_dataAgen();
        return $this->db->query($query)->num_rows();
    }
	function _get_dataAgen()
	{
        $query = "SELECT * FROM data_pelanggan WHERE 1=1 ";
        if (isset($_POST['search']['value'])) {
            $searchkey = $_POST['search']['value'];
            $query .= " AND (
			nama LIKE '%" . $searchkey . "%' or 
			alamat LIKE '%" . $searchkey . "%' or
			email LIKE '%" . $searchkey . "%' or
			hp LIKE '%" . $searchkey . "%' 
			) ";
        }

        $column = array('', '', 'nama', 'hp', 'email', 'alamat');
        $i = 0;
        foreach ($column as $item) {
            $column[$i] = $item;
        }

		/*  if (isset($_POST['order'])) {
		//	$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            $query .= " order by " . $column[$_POST['order']['0']['column']] . " " . $_POST['order']['0']['dir'];
        } else if (isset($order)) {
            $order = $order;
			//	$this->db->order_by(key($order), $order[key($order)]);
			       $query .= " order by nama ASC";
        }*/
		
		$query.=" order by nama ASC" ;
        return $query;
	}
	
	function getKode()
	{
			  $carikode = $this->db->query("SELECT max(id_agen) as id_agen from data_pelanggan")->row();
			  $datakode =$carikode->id_agen;//$carikode->kode;
			  if ($datakode) {
		  	    $kode = (int) $datakode;
		   		return	$newID = sprintf("%02s", $kode+1);
		   	  } else {
				return "01";
			  }
	}
	function getKodeAgen()
	{
			  $carikode = $this->db->query("SHOW TABLE STATUS LIKE 'data_pelanggan'")->row();
			  $datakode =isset($carikode->Auto_increment)?($carikode->Auto_increment):"1";//$carikode->kode;
		  	    $kode = (int) $datakode;
		   		$newID = sprintf("%03s", $kode);
				$tahun=date('Y');
				$bulan=date('m');
				$bulan=$this->tanggal->bulanRomawi($bulan);
		   	 return "BREA/$newID/$bulan/$tahun";			  
	}
	function cekAgen($kode)
	{
		if(!$kode){ return 10;}
		//	$this->db->where("hp",$hp);
			$this->db->where("kode_agen",$kode);
	return	$this->db->get("data_pelanggan")->num_rows();
	}
	function cekPelangganBaru($hp)
	{
			$this->db->where("hp",$hp);
	return	$this->db->get("data_pelanggan")->num_rows();
	}
	function cekPelangganLama($hp,$id)
	{
			$this->db->where("id_pelanggan!=",$id);
			$this->db->where("hp",$hp);
	return	$this->db->get("data_pelanggan")->num_rows();
	}
	function uploadGambar($form,$kode)
	{
		
		if(isset($_FILES[$form]['type']))
		{
		return	$this->upload_img($kode,$form);
		}else{
			return $this->gambarDefauld($form,$kode);
		}
	}
	function gambarDefauld($form,$kode)
	{
	$data=$this->db->get_where("data_pelanggan",array("kode_agen"=>$kode))->row();	
	return isset($data->$form)?($data->$form):"";
	}
	public function upload_img($kode,$form)
	{	//$this->m_konfig->log("admin","Upload photo");
		///
			$nama=date("YmdHis");
		  $lokasi_file = $_FILES[$form]['tmp_name'];
		  $tipe_file   = $_FILES[$form]['type'];
		  $nama_file   = $_FILES[$form]['name'];
		  //if($tipe_file)
		  //{
		  $daprof=$this->gambarDefauld($form,$kode);
			if($daprof!="")
			 {
				 $path = "file_upload/pelanggan/".$daprof;
				 if (file_exists($path)) {
					unlink($path);
				 }
		   }
		  
		  
			$jenis=explode(".",$nama_file);
			$nama_file=$jenis[0];
			// $jenis="jpg";
			$nama=str_replace("/","",$kode.$nama.$nama_file);
			 $target_path = "file_upload/pelanggan/".$nama.".jpg";
			 //
	//	  }
		  //
		if (!empty($lokasi_file)) {
		move_uploaded_file($lokasi_file,$target_path);
		//if($jenis=="png"){
		//$this->konversi->UploadImageResize($target_path,$jenis,200);
		}
		return $nama.".jpg";
		}
	function insert()
	{
	$data=array(
	"kode_agen"=>$kode=$this->input->post("agen"),
	"nama"=>$this->input->post("nama"),
	"hp"=>$hp=$this->input->post("hp"),
	"email"=>$this->input->post("email"),
	"alamat"=>$this->input->post("alamat"),
	"jk"=>$this->input->post("jk"),
	"tgl_lahir"=>$this->tanggal->eng_($this->input->post("tgl_lahir"),"-"),
	"poto"=>$this->uploadGambar("poto",$kode),
	"ktp"=>$this->uploadGambar("ktp",$kode),
	"kk"=>$this->uploadGambar("kk",$kode),
	"npwp"=>$this->uploadGambar("npwp",$kode),
	"bank"=>$this->input->post("bank"),
	"no_rek"=>$this->input->post("rek"),
	"atas_nama_bank"=>$this->input->post("an"),
	"nama_wakil"=>$this->input->post("nama2"),
	"nomor_wakil"=>$this->input->post("hp2"),
	"hubungan_wakil"=>$this->input->post("hubungan"),
	"nama_pasangan"=>$this->input->post("nama_pasangan"),
	"tgl_lahir_pasangan"=>$this->tanggal->eng_($this->input->post("tgl_lahir_pasangan"),"-"),
	"anak1"=>$this->input->post("anak1"),
	"tgl_lahir_anak1"=>$this->tanggal->eng_($this->input->post("tgl_lahir_anak1"),"-"),
	"anak2"=>$this->input->post("anak2"),
	"tgl_lahir_anak2"=>$this->tanggal->eng_($this->input->post("tgl_lahir_anak2"),"-"),
	"anak3"=>$this->input->post("anak3"),
	"tgl_lahir_anak3"=>$this->tanggal->eng_($this->input->post("tgl_lahir_anak3"),"-"),
	"anak4"=>$this->input->post("anak4"),
	"tgl_lahir_anak4"=>$this->tanggal->eng_($this->input->post("tgl_lahir_anak4"),"-"),
	"anak5"=>$this->input->post("anak5"),
	"tgl_lahir_anak5"=>$this->tanggal->eng_($this->input->post("tgl_lahir_anak5"),"-"),

	);
	$cek=$this->cekPelangganBaru($hp);
	if(!$cek){
		return $this->db->insert("data_pelanggan",$data);
	}else
	{
		return false;
	}

	}
	
	function update()
	{
	$id=$this->input->post("id_pelanggan");
	$data=array(
	"kode_agen"=>$kode=$this->input->post("agen"),
	"nama"=>$this->input->post("nama"),
	"hp"=>$hp=$this->input->post("hp"),
	"email"=>$this->input->post("email"),
	"alamat"=>$this->input->post("alamat"),
	"jk"=>$this->input->post("jk"),
	"tgl_lahir"=>$this->tanggal->eng_($this->input->post("tgl_lahir"),"-"),
	"poto"=>$this->uploadGambar("poto",$kode),
	"ktp"=>$this->uploadGambar("ktp",$kode),
	"kk"=>$this->uploadGambar("kk",$kode),
	"npwp"=>$this->uploadGambar("npwp",$kode),
	"bank"=>$this->input->post("bank"),
	"no_rek"=>$this->input->post("rek"),
	"atas_nama_bank"=>$this->input->post("an"),
	"nama_wakil"=>$this->input->post("nama2"),
	"nomor_wakil"=>$this->input->post("hp2"),
	"hubungan_wakil"=>$this->input->post("hubungan"),
	"nama_pasangan"=>$this->input->post("nama_pasangan"),
	"tgl_lahir_pasangan"=>$this->tanggal->eng_($this->input->post("tgl_lahir_pasangan"),"-"),
	"anak1"=>$this->input->post("anak1"),
	"tgl_lahir_anak1"=>$this->tanggal->eng_($this->input->post("tgl_lahir_anak1"),"-"),
	"anak2"=>$this->input->post("anak2"),
	"tgl_lahir_anak2"=>$this->tanggal->eng_($this->input->post("tgl_lahir_anak2"),"-"),
	"anak3"=>$this->input->post("anak3"),
	"tgl_lahir_anak3"=>$this->tanggal->eng_($this->input->post("tgl_lahir_anak3"),"-"),
	"anak4"=>$this->input->post("anak4"),
	"tgl_lahir_anak4"=>$this->tanggal->eng_($this->input->post("tgl_lahir_anak4"),"-"),
	"anak5"=>$this->input->post("anak5"),
	"tgl_lahir_anak5"=>$this->tanggal->eng_($this->input->post("tgl_lahir_anak5"),"-"),

	);
	$cek=$this->cekPelangganLama($hp,$id);
	if(!$cek){
		$this->db->where("id_pelanggan",$id);
		return $this->db->update("data_pelanggan",$data);
	}else
	{
		return false;
	}

	}
	
	function HapusAll()
	{
		$hapus=$this->input->post("hapus");
		foreach($hapus as $id)
		{
		 $this->hapus($id);
		}	return true;
	}
	function getGambarkode($id)
	{
		$this->db->where("id_pelanggan",$id);
	return	$data=$this->db->get("data_pelanggan")->row();
	}
	function hapus($id)
	{
		  $daprof=$this->getGambarkode($id);
			if($daprof)
			 {
				 /// hapus poto profile
				 $path = "file_upload/pelanggan/".$daprof->poto;
				 if (file_exists($path)) {
					unlink($path);
				 }
				 ////hapus poto ktp
				  $path = "file_upload/pelanggan/".$daprof->ktp;
				 if (file_exists($path)) {
					unlink($path);
				 } ////hapus poto kk
				  $path = "file_upload/pelanggan/".$daprof->kk;
				 if (file_exists($path)) {
					unlink($path);
				 }
				 ////hapus poto npwp
				  $path = "file_upload/pelanggan/".$daprof->npwp;
				 if (file_exists($path)) {
					unlink($path);
				 }
			 }
		$this->db->where("id_pelanggan",$id);
		return	$this->db->delete("data_pelanggan");
	}
	function export()
	{
//////start
        $objPHPExcel = new PHPExcel();
//style
        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'rotation' => 0,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '6CCECB')
            ),
            'borders' =>
            array('allborders' =>
                array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'),
                ),
            ),
        );
     //   $objPHPExcel->getActiveSheet(0)->getColumnDimension('F')->setWidth(25);
     //   $objPHPExcel->getActiveSheet(0)->getColumnDimension('G')->setWidth(25);
     //   $objPHPExcel->getActiveSheet(0)->getColumnDimension('H')->setWidth(35);




//create column
        $objPHPExcel->getActiveSheet(0)->setCellValue('A1', 'ID pelanggan');
        $objPHPExcel->getActiveSheet(0)->setCellValue('B1', 'NAMA');
        $objPHPExcel->getActiveSheet(0)->setCellValue('C1', 'NOMOR HP');
        $objPHPExcel->getActiveSheet(0)->setCellValue('D1', 'EMAIL');
        $objPHPExcel->getActiveSheet(0)->setCellValue('E1', 'ALAMAT');
        $objPHPExcel->getActiveSheet(0)->setCellValue('F1', 'JUMLAH LISTING');
        $objPHPExcel->getActiveSheet(0)->setCellValue('G1', 'JUMLAH PENJUALAN');
        $objPHPExcel->getActiveSheet(0)->setCellValue('H1', 'JUMLAH CUSTOMER');

//make a border column
        $objPHPExcel->getActiveSheet(0)->getStyle('A1:H1')->applyFromArray($style);

        $database = $this->_get_dataAgen();
        $shit = 1;
        $database = $this->db->query($database)->result();
        foreach ($database as $list) {
            $shit++;
//create data per row
           $objPHPExcel->getActiveSheet(0)->setCellValue('A' . $shit . '', '`'.$list->kode_agen);
          $objPHPExcel->getActiveSheet(0)->setCellValue('B' . $shit . '', $list->nama);
          $objPHPExcel->getActiveSheet(0)->setCellValue('C' . $shit . '', '`'.$list->hp);
            $objPHPExcel->getActiveSheet(0)->setCellValue('D' . $shit . '', $list->email);
            $objPHPExcel->getActiveSheet(0)->setCellValue('E' . $shit . '', $list->alamat);
            $objPHPExcel->getActiveSheet(0)->setCellValue('F' . $shit . '', $this->jmlListing($list->id_agen));
            $objPHPExcel->getActiveSheet(0)->setCellValue('G' . $shit . '', $this->jmlPenjualan($list->id_agen));
            $objPHPExcel->getActiveSheet(0)->setCellValue('H' . $shit . '', $this->jmlPelanggan($list->id_agen));
        }

//auto size column
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('G')->setAutoSize(true);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('H')->setAutoSize(true);

// Rename worksheet (worksheet, not filename)
        $objPHPExcel->getActiveSheet(0)->setTitle('Data Agen');
        
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Data-pelanggan.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
//////finish
    
	}

	

	function downloadFormat()
	{
	//////start
		$objPHPExcel = new PHPExcel();
		//style
		$style = array( 
		 'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				  'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				  'rotation'   => 0,
		  ),
		   'fill' => array(
				  'type' => PHPExcel_Style_Fill::FILL_SOLID,
				  'color' => array('rgb' => 'ccccff')
			  ),
		 'borders' => 
		  array( 'allborders' => 
			array( 'style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'), 
			  ), 
			), 
		);	
		
		$head = array( 
			'font'  => array(
			'bold'  => true,
			'color' => array('rgb' => 'FFFFFF'),
			),
			
		   'fill' => array(
				  'type' => PHPExcel_Style_Fill::FILL_SOLID,
				  'color' => array('rgb' => '009966')
			  ),
		 'borders' => 
		  array( 'allborders' => 
			array( 'style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'), 
			  ), 
			), 
		);
		
		
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('A')->setWidth(25);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('B')->setWidth(25);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('C')->setWidth(25);
		$objPHPExcel->getActiveSheet(0)->getColumnDimension('D')->setWidth(25);

		
		
		//create column
		
		$objPHPExcel->getActiveSheet(0)->setCellValue('A1', 'Nama');
		$objPHPExcel->getActiveSheet(0)->setCellValue('B1', 'Nomor Hp');
		$objPHPExcel->getActiveSheet(0)->setCellValue('C1', 'Email');
		$objPHPExcel->getActiveSheet(0)->setCellValue('D1', 'Alamat');
		
		//make a border column
		$objPHPExcel->getActiveSheet(0)->getStyle('A1:D1')->applyFromArray($style);
		
		
		
		// Rename worksheet (worksheet, not filename)
		$objPHPExcel->getActiveSheet(0)->setTitle('Data Agen');
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		
		header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Form Agen.xlsx"');
		header('Cache-Control: max-age=0');
		 
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		//////finish
		
	}
	function importData()
	{
	$sukses=0;$gagal=0;$edit=0;
		 $file   = explode('.',$_FILES['userfile']['name']);
		$length = count($file);
		if($file[$length -1] == 'xlsx' || $file[$length -1] == 'xls'){//jagain barangkali uploadnya selain file excel <span class="wp-smiley wp-emoji wp-emoji-smile" title=":-)">:-)</span>
        $tmp    = $_FILES['userfile']['tmp_name'];//Baca dari tmp folder jadi file ga perlu jadi sampah di server :-p
		      
				 // load excel
			    $file = $_FILES['userfile']['tmp_name'];
			    $load = PHPExcel_IOFactory::load($file);
                $sheets = $load->getActiveSheet()->toArray(null,true,true,true);
				$i=1;
				foreach ($sheets as $sheet) {
				if ($i > 1) {						
					//	$hp=str_replace("'","",$sheet[1]);
					//	$hp=str_replace("`","",$hp);
					//	$hp=str_replace("+62","0",$hp);
									
							//get form
															
						$sql=array(
						"kode_agen"=>$kode=$sheet[1],
						"nama"=>$sheet[0],
					//	"hp"=>$hp=$this->db->escape_str($hp),
					//	"email"=>$sheet[2],
					//	"alamat"=>$sheet[3],
						);		
				       
						$cek=$this->cekAgen($kode);
					if($cek==0)//jika data kontak tidak ada maka tambahkan
					{  
							$this->db->insert("data_pelanggan",$sql); $sukses++;
					}/*else{ //jika data kontak ada maka edit yg ada
						$sql=array(
						"nama"=>$sheet[0],
						"hp"=>$hp=$this->db->escape_str($hp),
						"email"=>$sheet[2],
						"alamat"=>$sheet[3],
						);						
							$this->db->where("hp",$hp);
							$this->db->update("data_pelanggan",$sql);
							$edit++;
					};*/
					   
				}
				$i++;
                }
               
		}else{
        exit('<br><b style="color:red">Upload Gagal! Gunakan Format Ms.Excell yang telah di sediakan</b>');   //pesan error tipe file tidak tepat
		}

		return $sukses."-".$edit."-".$gagal;
	}
	
	
	function ajax_costumer_report()
	{
		 $query = $this->_ajax_costumer_report();
        if ($_POST['length'] != -1)
            $query .= " limit " . $_POST['start'] . "," . $_POST['length'];
        return $this->db->query($query)->result();
 
	}
	 public function counts_costumer_report() {
        $query = $this->_ajax_costumer_report();
        return $this->db->query($query)->num_rows();
    }
	function _ajax_costumer_report()
	{
		$id_pelanggan=$this->uri->segment(3);
		$filter="";
		if($id_pelanggan){ $filter="id_costumer='".$id_pelanggan."'";}
        $query = "SELECT * FROM report_costumer WHERE 1=1 AND $filter ";
        if (isset($_POST['search']['value'])) {
            $searchkey = $_POST['search']['value'];
            $query .= " AND (
			ket LIKE '%" . $searchkey . "%' or
			tgl LIKE '%" . $searchkey . "%'
			) ";
        }

        $column = array('');
        $i = 0;
        foreach ($column as $item) {
            $column[$i] = $item;
        }
		$query.=" order by tgl DESC" ;
        return $query;
	}
	
	
}