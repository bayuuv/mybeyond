<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	
	 
	
	
	public function index()
	{	
		if($this->session->userdata("level")=="user")
		{
			redirect("dashboard");
		}
		$this->load->view('template/main');
	}
 

	 public function pdf()
  { 
		//load mPDF library
		$this->load->library('m_pdf');
		//load mPDF library


		//now pass the data//
		 $this->data['title']="MY PDF TITLE 1.";
		 $this->data['description']="";
		 $this->data['description']="";
		 //now pass the data //

		
		$html=$this->load->view('welcome_message',$this->data, true); //load the pdf_output.php by passing our data and get all data in $html varriable.
 	 
		//this the the PDF filename that user will get to download
		$pdfFilePath ="mypdfName-".time()."-download.pdf";

		
		//actually, you can pass mPDF parameter on this load() function
		$pdf = $this->m_pdf->load();
		//generate the PDF!
		$pdf->WriteHTML($html,2);
		//offer it to user via browser download! (The PDF won't be saved on your server HDD)
		$pdf->Output($pdfFilePath, "D");
		 
		 	
  }

 
   
 
 
	function jmlSmsUpdate()
	{
			$this->m_konfig->validasi_session(array("user","admin"));
	$id=$this->session->userdata("id");
	$this->db->where("CreatorID",$id);
	$data=$this->db->get("inbox")->num_rows();
	echo $data;
	}
	
	function isiSMSbaru($jml)
	{
			$this->m_konfig->validasi_session(array("user","admin"));
	$id=$this->session->userdata("id");
	$this->db->where("CreatorID",$id);
	$this->db->limit($jml);
	$this->db->order_by("ID","DESC");
	$data=$this->db->get("inbox")->result();
	foreach($data as $data)
	{?>
		<li class="item" style="padding:10px">
		<a href="<?php echo base_url();?>sms/inbox">
		<i class="fa fa-envelope-o"></i>
		<span class="content"><?php echo $data->TextDecoded?></span>
		<a style="color:black" href="<?php echo base_url();?>sms/send/<?php echo $data->SenderNumber; ?>" class="pull-right"><i class="fa fa-paper-plane"></i> Balas</span>
		</a>
		</li>
	<?php
	}
	}
	function getDataGroup()
	{
			$this->m_konfig->validasi_session(array("user","admin"));
	$id=$this->session->userdata("id");
	$this->db->where("ID",$id);
	$data=$this->db->get("pbk_groups")->row();
	echo json_encode($data);
		
	}
	
	
	
	function execAktifasi()
	{
		$this->m_konfig->validasi_session(array("user","admin"));
		$data=array(
		"aktifasi_sms_reg"=>$this->input->post("smsReg"),
		"aktifasi_balasan"=>$this->input->post("smsBalasan"),
		"aktifasi_kirim"=>$this->input->post("smsFormat"),
		"sms_reg"=>$this->input->post("kontenSmsReg"),
		"sms_unreg"=>$this->input->post("kontenSmsUnreg"),
		"kirim_ulang"=>$this->input->post("kirimUlang"),
		);
		$id=$this->session->userdata("id");
		$this->db->where("ID",$id);
	echo	$this->db->update("pbk_groups",$data);
	
	}
	
	function execAutoDelete()
	{
		$this->m_konfig->validasi_session(array("user","admin"));
		$data=array(
		"batas_inbox"=>$this->input->post("delInbox"),
		"batas_outbox"=>$this->input->post("delOutbox"),
		);
		$id=$this->session->userdata("id");
		$this->db->where("ID",$id);
	echo	$this->db->update("pbk_groups",$data);
	
	}
}
