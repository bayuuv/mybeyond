<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Replay extends CI_Controller {
		public function __construct() {
        parent::__construct();
       	$this->load->model('onreplay');
       	$this->load->model('schedul');
		$this->load->model("m_sms","sms");
		$this->load->model("Ultah","ultah");
		}

	function index()
	{
	$this->onreplay->play();
	$this->schedul->play();
	$this->ultah->play();
	}
	function paket()
	{
	$this->load->model('onload');
	$this->load->view("peringatan_paket");
	}
	
}
	