<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Akun extends CI_Controller {

	

	function __construct()
	{
		parent::__construct();	
		$this->m_konfig->validasi_session(array("agen","operator","designer","dm","network","promotion"));
		$this->load->model("M_data_agen","agen");
	}
	
	function _template($data)
	{
	$this->load->view('template/main',$data);	
	}
	
	public function index()
	{
	if($this->session->userdata("level")=="network")
	{
		$data['konten']="index_network";
	}elseif($this->session->userdata("level")=="agen")
	{
		$data['konten']="index";
	}else{
		$data['konten']="index_designer";
	}
	
	
	
	$this->_template($data);
	}
	function ajax_agen()
	{
		$list = $this->agen->get_dataAgen();
        $data = array();
        $no = $_POST['start']+1;
        foreach ($list as $val) {
			$row = array();
			$row[] =  '<input type="checkbox" class="pilih" onclick="pilcek()" name="hapus[]" value="'.$val->id_agen.'">';
			//$row[] = $no++;
            $row[] = "<a href='javascript:detail(`".$val->id_agen."`)'>".$val->kode_agen."</a>";
            $row[] = $val->nama;
            $row[] = $val->hp;
            $row[] = $val->email;
            $row[] = $val->alamat;
            $row[] = $this->reff->jumlahListing($val->kode_agen);
            $row[] = $this->reff->jumlahSelling($val->kode_agen);
            $row[] = $this->reff->jmlPelanggan($val->kode_agen);
            $row[] = '
			<a href="#" style="font-size:14px" onclick="edit(`' . $val->id_agen.'`)" class=" "><i class="fa fa-edit "> </i> Edit </a>
			| <a href="#" style="font-size:14px" onclick="hapus(`' . $val->id_agen . '`);" class=" "><i class="fa fa-trash"> </i> Hapus </a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->agen->counts(),
            "recordsFiltered" => $this->agen->counts(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	
	function ajax_costumer()
	{
		$list = $this->agen->ajax_costumer();
        $data = array();
        $no = $_POST['start']+1;
        foreach ($list as $val) {
			$nama=$this->reff->getNamaPelanggan($val->id_costumer);
			$hp=$this->reff->getHpPelanggan($val->id_costumer);
			$email=$this->reff->getEmailPelanggan($val->id_costumer);
			$row = array();
			$row[] = $no++;
           // $row[] = "<a href='javascript:detail(`".$val->id_agen."`)'>".$val->kode_agen."</a>";
            $row[] = $this->tanggal->indjam($val->tgl,"/");
            $row[] = "<a href='javascript:detailCostumer(`".$nama."`,`".$hp."`,`".$email."`)'>".$nama."</a>";
            $row[] = $this->reff->getNamaTitleCostumer($val->id_title);
            $row[] = $val->ket;
           
            $row[] = '
			<a href="#" style="font-size:14px" onclick="edit(`' . $val->id.'`)" class=" "><i class="fa fa-edit "> </i> Edit </a>
			| <a href="#" style="font-size:14px" onclick="hapus(`' . $val->id . '`);" class=" "><i class="fa fa-trash"> </i> Hapus </a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->agen->counts_costumer(),
            "recordsFiltered" => $this->agen->counts_costumer(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	function ajax_listing()
	{
		$list = $this->agen->ajax_listing();
        $data = array();
        $no = $_POST['start']+1;
        foreach ($list as $val) {
			$row = array();
			$row[] = $no++;
           // $row[] = "<a href='javascript:detail(`".$val->id_agen."`)'>".$val->kode_agen."</a>";
            $row[] = $this->tanggal->indjam($val->tgl,"/");
            $row[] = "<a href='javascript:detailListing(`".$val->kode_listing."`)'>".$val->kode_listing."</a>";
            $row[] = $this->reff->getNamaTitleListing($val->id_title);
            $row[] = $val->ket;
           
            $row[] = '
			<a href="#" style="font-size:14px" onclick="edit(`' . $val->id.'`)" class=" "><i class="fa fa-edit "> </i> Edit </a>
			| <a href="#" style="font-size:14px" onclick="hapus(`' . $val->id . '`);" class=" "><i class="fa fa-trash"> </i> Hapus </a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->agen->counts_listing(),
            "recordsFiltered" => $this->agen->counts_listing(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	function insert()
	{
		echo $this->agen->insert();
	}
	function getKodeAgen()
	{
		echo $this->agen->getKodeAgen();
	}
	function update()
	{
		echo $this->agen->update();
	}
	function HapusAll()
	{
	echo $this->agen->HapusAll();
	}
	function hapus($id)
	{
	echo $this->agen->hapus($id);
	}
	
	function export()
	{
		$this->agen->export();
	}
	
	function downloadFormat()
	{
	$this->agen->downloadFormat();
	}
	function importData()
	{
	$this->load->library("PHPExcel");
	 $data=$this->agen->importData();
		$data=explode("-",$data);
               ?><br><br>
			  <p style="color:green"><b>Import Data Selesai</b></p>
                <table class="tabel table-hover table-bordered" style="100%">
			       <?php 
				   if($data[0]){		   echo "<tr><td>Berhasil di simpan : ".$data[0]." kontak</td></tr>"; 			}?>
					<?php
					if($data[1]){	  	   echo "<tr><td>Diperbaharui : ".$data[1]." kontak</td></tr>";			} 
					if($data[2]){	  	   echo "<tr><td>Gagal di import : ".$data[2]." kontak</td></tr>";			} ?>
                </table>
                <?php
	}
	function cekpas()
	{
		$this->db->where("username",$this->input->post("user"));
		$this->db->where("password",$this->input->post("pass"));
	echo	$this->db->get("data_agen")->num_rows();
	}
	function getDetail($id)
	{
		$data["id"]=$id;
		$this->load->view("getDetail",$data);
	}
}