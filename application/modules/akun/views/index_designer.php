<?php $KODE=$this->session->userdata("kode");?>
  <div class="row">
    <div class="col-lg-12">

 
  <form action="javascript:saveAdd()" id="formulirAgen"   method="post" >	
<div class="row">
<div class="col-lg-12">
<div class="main-box clearfix">
<div >
<div class="tabs-wrapper">
<ul class="nav nav-tabs">
<li class="active b"><a href="#tab-home" data-toggle="tab">Profile</a></li>
<li class="b"><a href="#kontak2" data-toggle="tab">Kontak 2</a></li>
<li class="b"><a href="#keluarga" data-toggle="tab">Keluarga</a></li>
<li class="b"><a href="#akun" data-toggle="tab">Akun</a></li>
 <button type="submit" style="margin-top:5px;margin-right:5px" onclick="saveAdd()" id="submit" class="btn btn-success pull-right"><i class='fa fa-save'></i> Simpan</button> 
  <span class='load pull-right' style="margin-right:100px;margin-top:10px"></span>
</ul>
<div class="tab-content">
<div class="tab-pane fade in active" id="tab-home">

<div class="form-horizontal black col-md-6">
<?php if($this->session->userdata("level")!="dm")
{?>
<div class="form-group">
<label for="nama" class="b col-lg-2 control-label">Kode Agen</label>
<div class="col-lg-9">
<input type="text" class="form-control" id="kode" name="kode"  >
</div>
</div>
<?php }else{
	?>
	<input type="hidden" class="form-control" id="kode" name="kode"  >
	<?php
} ?>
 

<div class="form-group">
<label for="nama" class="b col-lg-2 control-label">Nama</label>
<div class="col-lg-9">
<input type="text" class="form-control" id="nama" name="nama"  >
<input type="hidden" id="id_agen" name="id_agen">
</div>
</div>

<div class="form-group">
<label for="nama" class="b col-lg-2 control-label">Jenis Kelamin</label>
<div class="col-lg-9">
<div class="radio">
<input type="radio" onclick="clikL()" id="l" name="jk" value="l"/>
<label for="l">
Laki-laki
</label>
 
<input type="radio" onclick="clikP()"  id="p" name="jk" value="2"/>
<label for="p">
Perempuan
</label>
</div>
</div>
</div>


<div class="form-group">
<label for="tgl_lahir" class="col-lg-2 control-label b">Tgl lahir</label>
<div class="col-lg-9">
<input type="text" class="form-control" id="maskedDate" name="tgl_lahir" placeholder="contoh:31/12/1990">
</div>
</div>


<div class="form-group">
<label for="hp" class="col-lg-2 control-label b">Hp</label>
<div class="col-lg-9">
<input type="text" class="form-control" id="hp" name="hp" >
</div>
</div>


<div class="form-group">
<label for="email" class="b col-lg-2 control-label">E-mail</label>
<div class="col-lg-9">
<input type="text" class="form-control" id="email" name="email" >
</div>
</div>

<div class="form-group">
<label for="alamat" class="b col-lg-2 control-label">Alamat</label>
<div class="col-lg-9">
<textarea name="alamat" id="alamat" class="form-control"></textarea>
</div>
</div>
</div>


<div class="form-horizontal black col-md-6">

<div class="form-group">
<label for="tgl_masuk_kerja" class="col-lg-2 control-label b">Tgl Masuk Kerja</label>
<div class="col-lg-9">
<input type="text" class="form-control" id="maskedDate5" readonly name="tgl_masuk_kerja" placeholder="contoh:31/12/1990">
</div>
</div>
<div class="form-group">
<label for="tgl_masuk_kerja" class="col-lg-2 control-label b">Tgl Habis Kontrak</label>
<div class="col-lg-9">
<input type="text" class="form-control" id="maskedDate6" readonly name="tgl_habis_kontrak" placeholder="contoh:31/12/1990">
</div>
</div>

<div class="form-group">

<label for="poto" class="b col-lg-2 control-label">Photo Profile</label>
<div class="col-lg-9">
<?php 
$poto=$this->reff->getPotoAgen($KODE);
if($poto!="nopund.jpg"){
?>
<span class="pull-right" style="font-size:12px"><a target='_blank' href="<?php echo base_url()?>file_upload/agen/<?php echo $poto;?>"><i class="fa fa-image"></i> Lihat dokumen</a></span>
<?php 
}else{?>
<span class="pull-right" style="font-size:12px;color:red">Belum diupload</span>
<?php
}
?>
<input type="file" class="form-control" id="poto" name="poto"  >
</div>
</div>

<div class="form-group">
<label for="ktp" class="b col-lg-2 control-label">Scand KTP</label>
<div class="col-lg-9">
<?php 
  $poto=$this->reff->getPotoKtp($KODE);
if($poto){
?>
<span class="pull-right" style="font-size:12px"><a target='_blank' href="<?php echo base_url()?>file_upload/agen/<?php echo $poto;?>"><i class="fa fa-image"></i> Lihat dokumen</a></span>
<?php 
}else{?>
<span class="pull-right" style="font-size:12px;color:red">Belum diupload</span>
<?php
}
?>
<input type="file" class="form-control" id="ktp" name="ktp"  >
</div>
</div>

<div class="form-group">
<label for="kk" class="b col-lg-2 control-label">Scand KK</label>
<div class="col-lg-9">
<?php 
  $poto=$this->reff->getPotoKK($KODE);
if($poto){
?>
<span class="pull-right" style="font-size:12px"><a target='_blank' href="<?php echo base_url()?>file_upload/agen/<?php echo $poto;?>"><i class="fa fa-image"></i> Lihat dokumen</a></span>
<?php 
}else{?>
<span class="pull-right" style="font-size:12px;color:red">Belum diupload</span>
<?php
}
?>
<input type="file" class="form-control" id="kk" name="kk"  >
</div>
</div>


<div class="form-group">
<label for="email" class="b col-lg-2 control-label">Scand NPWP</label>
<div class="col-lg-9">
<?php 
  $poto=$this->reff->getPotoNPWP($KODE);
if($poto){
?>
<span class="pull-right" style="font-size:12px"><a target='_blank' href="<?php echo base_url()?>file_upload/agen/<?php echo $poto;?>"><i class="fa fa-image"></i> Lihat dokumen</a></span>
<?php 
}else{?>
<span class="pull-right" style="font-size:12px;color:red">Belum diupload</span>
<?php
}
?>
<input type="file" class="form-control" id="npwp" name="npwp" >
</div>
</div>


<div class="form-group">
<label for="rek" class="b col-lg-2 control-label">Bank</label>
<div class="col-lg-9">
<select name="bank" class="form-control">
<option value=' '>=== Pilih ===</option>
<option value='bca'>BCA</option>
<option value='bni'>BNI</option>
<option value='bri'>BRI</option>
<option value='bjb'>BJB</option>
<option value='nisp'>NISP</option>
<option value='mandiri'>MANDIRI</option>
</select>
</div>
</div>


<div class="form-group">
<label for="rek" class="b col-lg-2 control-label">Nomor Rek.</label>
<div class="col-lg-9">
<input type="text" class="form-control" id="rek" name="rek" >
</div>
</div>

<div class="form-group">
<label for="an" class="b col-lg-2 control-label">Atas nama no.rek</label>
<div class="col-lg-9">
<input type="text" class="form-control" id="an" name="an" >
</div>
</div>


</div>





</div>
<div class="tab-pane fade" id="kontak2">


<div class="form-horizontal black col-md-12">
<div class="form-group">
<label for="nama2" class="b col-lg-2 control-label">Nama </label>
<div class="col-lg-9">
<input type="text" class="form-control" id="nama2" name="nama2"  >
</div>
</div>

<div class="form-group">
<label for="hp2" class="b col-lg-2 control-label">Hp</label>
<div class="col-lg-9">
<input type="text" class="form-control" id="hp2" name="hp2"  >
</div>
</div>


<div class="form-group">
<label for="hubungan" class="b col-lg-2 control-label">Hubungan </label>
<div class="col-lg-9">
<input type="text" class="form-control" id="hubungan" name="hubungan"  >
</div>
</div>





</div>



</div>
<div class="tab-pane fade" id="keluarga">

<div class="form-horizontal black col-md-12">
<div class="form-group">
<label for="nama_pasangan" class="b col-lg-2 control-label"><span class='pasangan'>Nama Pasangan</span></label>
<div class="col-lg-9">
<input type="text" class="form-control" id="nama_pasangan" name="nama_pasangan"  >
</div>
</div>

<div class="form-group">
<label for="tgl_lahir_pasangan" class="b col-lg-2 control-label"> Tgl Lahir </label>
<div class="col-lg-9">
<input type="text" class="form-control" id="maskedDate4" name="tgl_lahir_pasangan"  placeholder="contoh:31/12/1990" >
</div>
</div>

<div class="form-group">
<label for="anak1" class="b col-lg-2 control-label">Nama Anak 1</label>
<div class="col-lg-9">
<input type="text" class="form-control" id="anak1" name="anak1"  >
</div>
</div>


<div class="form-group">
<label for="tgl_lahir_anak1" class="b col-lg-2 control-label">Tgl lahir </label>
<div class="col-lg-9">
<input type="text" class="form-control" id="maskedDate2" name="tgl_lahir_anak1"  placeholder="contoh:31/12/1990" >
</div>
</div>



<div class="form-group">
<label for="anak1" class="b col-lg-2 control-label">Nama Anak 2</label>
<div class="col-lg-9">
<input type="text" class="form-control" id="anak2" name="anak2"  >
</div>
</div>


<div class="form-group">
<label for="tgl_lahir_anak2" class="b col-lg-2 control-label">Tgl lahir </label>
<div class="col-lg-9">
<input type="text" class="form-control" id="maskedDate33" name="tgl_lahir_anak2"   placeholder="contoh:31/12/1990">
</div>
</div>





</div>


</div>
<div class="tab-pane fade" id="akun">

<div class="form-horizontal black col-md-12">
<div class="form-group password">
<label for="username" class="b col-lg-3 control-label">Username </label>
<div class="col-lg-8">
<input type="text" class="form-control" id="username"  onkeyup="cekpass()" name="username"  >
</div>
</div>

<div class="form-group password">
<label for="password" class="b col-lg-3 control-label">Password</label>
<div class="col-lg-8">
<input type="text" class="form-control" id="password" onkeyup="cekpass()" name="password"  >
<span class="help-block"></span>
</div>
</div>
<script src="<?php echo base_url();?>plug/boostrap/js/jquery.maskedinput.min.js"></script>  
<script>
$("#maskedDate").mask("99/99/9999");
$("#maskedDate1").mask("99/99/9999");
$("#maskedDate2").mask("99/99/9999");
$("#maskedDate33").mask("99/99/9999");
$("#maskedDate3").mask("99/99/9999");
$("#maskedDate4").mask("99/99/9999");
$("#maskedDate5").mask("99/99/9999");
$("#maskedDate6").mask("99/99/9999");
 
</script>

<script>
function cekpass()
{
	var user=$("[name='username']").val();
	var pass=$("[name='password']").val();
	
	 	$.ajax({
		url:"<?php echo base_url();?>data_agen/cekpas",
		type: "POST",
		data:"user="+user+"&pass="+pass,
		success: function(data)
				{
					if(data>0)
					{
						$(".password").addClass("has-error");
						$(".help-block").html("Silahkan ganti username dan password anda");
						document.getElementById("submit").disabled = true;
					}else{
						$(".password").removeClass("has-error");
						$(".help-block").html("");
						document.getElementById("submit").disabled = false;
					}
				},
			});
}
</script>

</div>



</div>

</div>
</div>
</div>
</div>
</div>
</div>

    </div>
  </div>   <!-- /.row -->
  </form>

  
  
  
  <?php echo $this->load->view("js/form.phtml"); ?>
  
<script>
function saveAdd()
	{	
	 $(".load").html('<img src="<?php echo base_url()?>plug/img/load.gif"> Process Simpan...');
 		var url="<?php echo base_url();?>data_agen/update";	 
		$(".load").html("<img src='<?php echo base_url();?>plug/img/load.gif'> Please wait...");
		$('#formulirAgen').ajaxForm({
		url:url,
		type: "post",
		data: $('#formulirAgen').serialize(),
	//	dataType: "JSON",
		success: function(data)
				{
				
				$(".load").html('<font color="green"><i class="fa fa-check-circle fa-fw fa-lg"></i> Berhasil disimpan</font>');
				var poto=$("[name='poto']").val();
				var ktp=$("[name='ktp']").val();
				var kk=$("[name='kk']").val();
				var npwp=$("[name='npwp']").val();
				if(poto || ktp || kk || npwp)
				{
				window.location.href="";
				}
				edit();				
				},
				
		});
	}
</script>



<script>
$( document ).ready(function() {
edit();
});
 function edit()
	  {
		  $("#modalAdd").modal("show");
			 	$.ajax({
				url:"<?php echo base_url();?>data_pegawai/getEdit/"+<?php echo $this->session->userdata("id");?>,
				success: function(data)
						{			
						
				  var isi=data.split("::");
				  // alert(isi[3]);
				  $(".title").html(" Edit Data Pegawai");
				  $("[name='jabatan']").val(isi[0]);
				  $("[name='nama']").val(isi[1]);
				  $("[name='hp']").val(isi[4]);
				  $("[name='email']").val(isi[5]);
				  $("[name='alamat']").val(isi[6]);
				  $("[name='id_agen']").val(<?php echo $this->session->userdata("id");?>);
				  $("[name='tgl_lahir']").val(isi[3]);
				  $("[name='tgl_masuk_kerja']").val(isi[7]);
				  $("[name='bank']").val(isi[8]);
				  $("[name='rek']").val(isi[9]);
				  $("[name='an']").val(isi[10]);
				  $("[name='nama2']").val(isi[11]);
				  $("[name='hp2']").val(isi[12]);
				  $("[name='hubungan']").val(isi[13]);
				  $("[name='nama_pasangan']").val(isi[14]);
				  $("[name='tgl_lahir_pasangan']").val(isi[15]);
				  $("[name='anak1']").val(isi[16]);
				  $("[name='tgl_lahir_anak1']").val(isi[17]);
				  
				  $("[name='anak2']").val(isi[18]);
				  $("[name='tgl_lahir_anak2']").val(isi[19]);
				  
				  $("[name='anak3']").val(isi[20]);
				  $("[name='tgl_lahir_anak3']").val(isi[21]);
				  
				  $("[name='anak4']").val(isi[22]);
				  $("[name='tgl_lahir_anak4']").val(isi[23]);
				  
				  $("[name='anak5']").val(isi[24]);
				  $("[name='tgl_lahir_anak5']").val(isi[25]);
				  $("[name='username']").val(isi[26]);
				  $("[name='password']").val(isi[27]);
				 
				  $("[name='kode']").val(isi[29]);
				  $("[name='tgl_habis_kontrak']").val(isi[35]);
				 
					if(isi[2]=="p")
					{
						var jk="2";
					}else{
						var jk="l";
					}
			
				  $('input:radio[name=jk][value='+jk+']')[0].checked = true;
				
	
				},
			});
		 
		
	  }
	  function setHide()
	  {
					$("#formulirAgen")[0].reset();
					 
					$(".load").html('');
	  }
	  </script>

	  