<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Listing_ag2 extends CI_Controller {

	

	function __construct()
	{
		parent::__construct();	
		$this->m_konfig->validasi_session(array("agen","designer","dm","network"));
		$this->load->model("M_agen2","agen2");
	}
	
	function _template($data)
	{
	$this->load->view('template/main',$data);	
	}
	
	public function index()
	{

	$this->listing();
	}
	public function listing()
	{

	$data['konten']="listing";
	$this->_template($data);
	}
	public function add()
	{
	$data['konten']="add";
	$this->_template($data);
	}
	function getFormCMA($id)
	{	$data["id"]=$id;
		$this->load->view("formCMA",$data);
	}
	function getFormCMAEdit($id)
	{	$data["id"]=$id;
		$this->load->view("formEditCMA",$data);
	}
	function insertHasilCMA()
	{
		echo $this->agen2->insertHasilCMA();
	}
	function editHasilCMA()
	{
		echo $this->agen2->editHasilCMA();
	}
	function ajaxGetKab()
	{
		$id=$this->input->post("prov");
		$def=$this->input->post("def");
		$data=$this->reff->getKab($id);
		if($data)
		{
			$kab[""]="==== Pilih ====";
		 foreach ($data as $val) {
				 $kab[$val->id] = '['.$val->id.']'.' &nbsp;'.$val->kabupaten;
                 }
                 $dataKab = $kab;
                echo form_dropdown('kabupaten', $dataKab, $def, ' id="kabupaten" class="select2-container" style="width:100%" onchange="return cek_kab()"');     
		echo "<script>	$('#kabupaten').select2();</script>";
		}else{
			
			echo form_dropdown('kabupaten', array(), '', ' id="kabupaten" class="select2-container" style="width:100%"');     
			echo "<script>	$('#kabupaten').select2();</script>";
		}
	}
	function ajaxGetKab2()
	{
		$id=$this->input->post("prov");
		$def=$this->input->post("def");
		$data=$this->reff->getKab($id);
		if($data)
		{
			$kab[""]="==== Pilih ====";
		 foreach ($data as $val) {
				 $kab[$val->id] = '['.$val->id.']'.' &nbsp;'.$val->kabupaten;
                 }
                 $dataKab = $kab;
                echo form_dropdown('kabupaten', $dataKab, $def, ' id="kabupaten" class="form-control" style="width:100%" onchange="return cek_kab()"');     
		echo "<script>	$('#kabupaten').select2();</script>";
		}else{
			
			echo form_dropdown('kabupaten', array(), '', ' id="kabupaten" class="select2-container" style="width:100%"');     
			echo "<script>	$('#kabupaten').select2();</script>";
		}
	}
	function getKomplek($id)
	{
		$data=$this->reff->getKomplek($id);
		$result="";
		foreach($data as $data)
		{
			$result[]=$data->komplek;
		}
		echo json_encode($result);
	}
	function infoFlash()
	{
		$msg='<script>alert("Success! Tersimpan")</script>';
		$this->session->set_flashdata("info",$msg);
	}
	function insert()
	{
			$this->infoFlash();
	echo	$this->agen2->insert();
	}
	
	function ajax_property()
	{
		$list = $this->agen2->get_dataproperty();
        $data = array();
        $no = $_POST['start']+1;
        foreach ($list as $val) {
			$row = array();
			$gambar=$val->gambar_utama;
			if(!$gambar)
			{$gambar="nopund.jpg";}else{
				$gambar=$val->$gambar;
			}
			if($val->hasil_cma == 'MAHAL')
			{
				$hasilcma = "<font color='red'>MAHAL</font>";
			}elseif($val->hasil_cma == 'MURAH'){
				$hasilcma = "<font color='#510999'>MURAH</font>";
			}else{
				$hasilcma = "BELUM TERSEDIA";
			}
			
			if($val->harga_cma == 0){
			$row[] = '<div class="col-md-3" ><img style="width:100%" src="'.base_url().'file_upload/img/'.$gambar.'" alt=""/></div>
<div class="col-md-5"><a href="#" onclick="godetail(`'.$val->id_prop.'`,`'.$val->kode_prop.'`)"  class="user-link" style="color:black;font-weight:bold">'.$this->reff->getNamaJenis($val->jenis_prop).' - Rp '.number_format($val->harga+$val->fee_up,0,",",".").'</a><br>
<a href="#" onclick="godetail(`'.$val->id_prop.'`)"  class="user-link" style="color:black">'.$val->nama_area.' - '.$val->alamat_detail .'</a><br>
<b>Land Price : Rp '.number_format($val->harga_area,0,",",".").'</b><br>
<b>CMA Price : Rp '.number_format($val->harga_cma,0,",",".").'<b></br>
<b>Hasil CMA : '.$hasilcma.'</b>
</div>
<div class="col-md-4">
<button class="btn-info" onclick="godetail(`'.$val->id_prop.'`)"><i class="fa fa-search"></i>  Detail</button></center>
<button class="btn-success" onclick="gocma(`'.$val->id_prop.'`)"><i class="fa fa-magic"></i>  CMA</button>
</div>
';
	
			}else{
	$row[] = '<div class="col-md-3" ><img style="width:100%" src="'.base_url().'file_upload/img/'.$gambar.'" alt=""/></div>
<div class="col-md-5"><a href="#" onclick="godetail(`'.$val->id_prop.'`,`'.$val->kode_prop.'`)"  class="user-link" style="color:black;font-weight:bold">'.$this->reff->getNamaJenis($val->jenis_prop).' - Rp '.number_format($val->harga+$val->fee_up,0,",",".").'</a><br>
<a href="#" onclick="godetail(`'.$val->id_prop.'`)"  class="user-link" style="color:black">'.$val->nama_area.' - '.$val->alamat_detail .'</a><br>
<b>Land Price : Rp '.number_format($val->harga_area,0,",",".").'</b><br>
<b>CMA Price : Rp '.number_format($val->harga_cma,0,",",".").'<b></br>
<b>Hasil CMA : '.$hasilcma.'</b>
</div>
<div class="col-md-4">
<button class="btn-info" onclick="godetail(`'.$val->id_prop.'`)"><i class="fa fa-search"></i>  Detail</button></center>
<button class="btn-success" onclick="gocmaEdit(`'.$val->id_prop.'`)"><i class="fa fa-magic"></i>  Edit CMA</button>
</div>
';		
		
			}
			
			$data[] = $row;
			
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->agen2->counts(),
            "recordsFiltered" => $this->agen2->counts(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	function getListing()
	{
	$provinsi=$this->input->post("provinsi");
	$kabupaten=$this->input->post("kabupaten");
	$area=$this->input->post("area");
	$lat_area=$this->input->post("lat_area");
	$long_area=$this->input->post("long_area");
	$jenis_pro=$this->input->post("jenis_pro");
	$type_pro=$this->input->post("type_pro");
	$kamar_tidur=$this->input->post("kamar_tidur");
	$kamar_mandi=$this->input->post("kamar_mandi");
	$garasi=$this->input->post("garasi");
	$daya_listrik=$this->input->post("daya_listrik");
	$harga_min=$this->input->post("harga_min");
	$harga_max=$this->input->post("harga_max");
	$sertifikat=$this->input->post("sertifikat");
	$agen=$this->input->post("agen");
	$type_sewa=$this->input->post("type_sewa");
	$kelengkapan=$this->input->post("kelengkapan");
	$status_penjualan=$this->input->post("status_penjualan");
	$cma_cek=$this->input->post("cma_cek");
	$filter['filter']="?type_pro=$type_pro&provinsi=$provinsi&kabupaten=$kabupaten&area=$area&lat_area=$lat_area&long_area=$long_area&jenis_pro=$jenis_pro&kamar_mandi=$kamar_mandi&kamar_tidur=$kamar_tidur&garasi=$garasi&daya_listrik=$daya_listrik&harga_min=$harga_min&harga_max=$harga_max&sertifikat=$sertifikat&agen=$agen&type_sewa=$type_sewa&kelengkapan=$kelengkapan&status_penjualan=$status_penjualan&cma_cek=$cma_cek";
	echo	$this->load->view("getListing",$filter);
	}
	function getDataDetail()
	{
		$data['kode_prop']=$this->input->post("id");
	echo	$this->load->view("dataDetail",$data);
	}
	function getDataPromo()
	{
		$data['kode_prop']=$this->input->post("id");
	echo	$this->load->view("dataPromo",$data);
	}
	function goReportListing($id,$kode_listing)
	{
		$data["id"]=$id;
		$data["kode_listing"]=$kode_listing;
		$this->load->view("goReportListing",$data);
	}
	function delHistory($id)
	{
		echo $this->agen2->delHistory($id);
	}
		function getHistory()
	{
		$id=$this->input->post("id");
		$this->db->where("kode_listing",$id);
		$this->db->order_by("tgl","ASC");
		$data=$this->db->get("report_listing")->result();
		$level=$this->session->userdata("level");
		foreach($data as $val)
		{
			?>
			 
			<div class="conversation-item item-right clearfix black">
			<?php if($level=="agen"){?>
			<div class="conversation-user" style='margin-top:20px;color:red'>
			
			&nbsp;&nbsp;&nbsp;<a href='javascript:hapusH(`<?php echo $val->id;?>`,`<?php echo $id ?>`)'><i class='fa fa-trash' style='margin-left:-5px'></i></a>
			</div><?php } ?>
			<div class="conversation-body">
			<div class="name">
			<?php echo $this->reff->getNamaTitleListing($val->id_title)?>
			<?php if($level=="agen"){?>
			<a class="hapusmobile pull-right" style='color:red' href='javascript:hapusH(`<?php echo $val->id;?>`,`<?php echo $id ?>`)'><i class='fa fa-trash' style='margin-left:-5px'></i></a>
			<?php } ?></div>
			<div class="time hidden-xs">
			<?php echo $this->tanggal->hariLengkapJam($val->tgl,"/")?>
			</div>
			<div class="text">
			<?php echo $val->ket;?>
			</div>
			</div>
			</div>
			<?php
		}
	}
		function saveReport()
	{
		echo $this->agen2->saveReport();
	}
	
	function ajax_promodetail($id)
	{
		$list = $this->agen2->get_dataPromo($id);
        $data = array();
        $no = $_POST['start']+1;
        foreach ($list as $val) {
			if($val->jenis_promo == '1'){
				$jenis_promo = "Koran";
			}elseif($val->jenis_promo == '2'){
				$jenis_promo = "Maxi Banner";
			}if($val->jenis_promo == '3'){
				$jenis_promo = "Papan Tanah";
			}else{
				$jenis_promo = "Spanduk";
			}
			
			if($val->status == '1'){
				$status = "Aktif";
			}else{
				$status = "<font color='red'>Tidak Aktif</font>";
			}
			
			$row = array();
			$row[] = $no++;
            $row[] = $this->tanggal->ind($val->tgl_promo,"/");
            $row[] = $val->alamat_promo;
            $row[] = '<img src="'.base_url().'file_upload/img/'.$val->foto_promo.'" width="200px" height="200px" >';
			$row[] = $jenis_promo;
			$row[] = $status;
			
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->agen->countsList($id),
            "recordsFiltered" => $this->agen->countsList($id),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}

	
}