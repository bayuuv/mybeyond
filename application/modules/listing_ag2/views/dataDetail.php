
	  <style>
   
  .thumbnails  body {
      font: 100%/1.5 "Helvetica Neue", Helvetica, Arial, sans-serif;
      width: 80%;
      margin: 0 auto;
      padding-top: 3em;
      color: #333;
      background: #eee;
    }
   .thumbnails img {
      max-width: 30%;
      padding: 5px;
      border: 1px solid #ccc;
      height: auto;
      background: #fff;
      box-shadow: 1px 1px 7px rgba(0,0,0,0.1);
    }
 .thumbnails   h1 {
      font-size: 3em;
      line-height: 1;
      margin-bottom: 0.5em;
    }
  .thumbnails  p {
      margin-bottom: 1.5em;
    }
  .thumbnails  ul {
      list-style: none;
      margin-bottom: 1.5em;
    }
     .main-image {
      max-width: 80px;
      margin-bottom: 0.75em;
    }
    .thumbnails li {
      display: inline;
      margin: 0 5px 0 0;
    }
  </style>

  <?php
$this->db->where("id_prop",$kode_prop);
$val=$this->db->get("data_property")->row();
$gambarUtama=$val->gambar_utama;
			if(!$gambarUtama)
			{$gambarUtama="nopund.jpg";}else{
				$gambarUtama=$val->$gambarUtama;
			}
?>

  <div class="col-md-8">
 
  <div class="main-image" >
    <img src="<?php echo base_url()?>/file_upload/img/<?php echo $gambarUtama;?>" alt="Placeholder" class="custom">
 
  </div>  

  <ul class="thumbnails">
<?php

if($val->gambar1)
{?>
		 <li><a href="<?php echo base_url()?>/file_upload/img/<?php echo $val->gambar1;?>"><img src="<?php echo base_url()?>/file_upload/img/<?php echo $val->gambar1;?>" alt="Thumbnails"></a></li>
<?php } ?>

<?php if($val->gambar2)
{?>
		 <li><a href="<?php echo base_url()?>/file_upload/img/<?php echo $val->gambar2;?>"><img src="<?php echo base_url()?>/file_upload/img/<?php echo $val->gambar2;?>" alt="Thumbnails"></a></li>
<?php } ?>

<?php
if($val->gambar3)
{?>
		 <li><a href="<?php echo base_url()?>/file_upload/img/<?php echo $val->gambar3;?>"><img src="<?php echo base_url()?>/file_upload/img/<?php echo $val->gambar3;?>" alt="Thumbnails"></a></li>
<?php } ?>

<?php if($val->gambar4)
{?>
		 <li><a href="<?php echo base_url()?>/file_upload/img/<?php echo $val->gambar4;?>"><img src="<?php echo base_url()?>/file_upload/img/<?php echo $val->gambar4;?>" alt="Thumbnails"></a></li>
<?php } ?>

<?php if($val->gambar5)
{?>
		 <li><a href="<?php echo base_url()?>/file_upload/img/<?php echo $val->gambar5;?>"><img src="<?php echo base_url()?>/file_upload/img/<?php echo $val->gambar5;?>" alt="Thumbnails"></a></li>
<?php } ?>
<?php if($val->desain)
{?>
		 <li><a href="<?php echo base_url()?>/file_upload/img/<?php echo $val->desain;?>"><img src="<?php echo base_url()?>/file_upload/img/<?php echo $val->desain;?>" alt="Thumbnails"></a></li>
<?php } ?>

   
  </ul>
</div>



<style>
.border{
	border:#CCCCCC solid 0.01px;
	color:black;
	
}
.custom{
	max-width:550px; max-height:300px;
}
</style>
<style>
@media (max-width: 600px) {
 .border{
	border:#CCCCCC solid 0.01px;
	color:black;
	 font: 150%/1.5 "Helvetica Neue", Helvetica, Arial, sans-serif;
}
.custom{
	max-width:200px;max-height:250px;
}
}
</style>

<div class="col-lg-12">
 
<div class="list-group">
<a href="#" class="list-group-item active">
<b>Informasi Detail : <?php echo $val->kode_prop;?></b> 
</a>
<div class="main-box-body clearfix">
<div class="table-responsive">

<div class="col-md-6 border">Type Property : <?php echo $this->reff->getNamaJenis($val->jenis_prop); ?> (<?php echo $val->type_jual; ?>)</div>
<div class="col-md-6 border">Harga :Rp <?php echo number_format($val->harga,0,",","."); ?></div>

<div class="col-md-6 border">Pemilik :<?php echo $this->reff->getNamaOwner($val->id_owner); ?> </div>
<div class="col-md-6 border">Member :<?php echo $this->reff->getNamaAgen($val->agen); ?> </div>

<?php
if($val->type_jual=="sewa")
{?>
<div class="col-md-6 border">Type Sewa : <?php echo $this->reff->getNamaTypeSewa($val->type_sewa);?> </div>
<?php } ?>

<div class="col-md-6 border">Kamar Tidur : <?php echo  $val->kamar_tidur;?></div>
<div class="col-md-6 border">Kamar Mandi : <?php echo  $val->kamar_mandi;?></div>

<div class="col-md-6 border">KT Pembantu : <?php echo  $val->kamar_tidur_p;?></div>
<div class="col-md-6 border">KM Pembantu : <?php echo  $val->kamar_mandi_p;?></div>


<div class="col-md-6 border">Jml Lantai : <?php echo  $val->jml_lantai;?></div>
<div class="col-md-6 border">Jml Garasi : <?php echo  $val->jml_garasi;?></div>

<div class="col-md-6 border">Jml Carport : <?php echo  $val->jml_carports;?></div>
<div class="col-md-6 border">Daya Listrik : <?php echo  $val->daya_listrik;?></div>


<div class="col-md-6 border">Air : <?php echo $this->reff->getNamaAir($val->air);?> </div>
<div class="col-md-6 border">Furniture : <?php echo $this->reff->getNamaFurniture($val->furniture);?></div>


<div class="col-md-6 border">Luas Tanah : <?php echo $val->luas_tanah;?> M<sup>2<sup></div>
<div class="col-md-6 border">Luas Bangunan : <?php echo $val->luas_bangunan;?> M<sup>2<sup></div>


<div class="col-md-6 border">Sertifikat : <?php echo $this->reff->getNamaJenisSertifikat($val->jenis_sertifikat);?></div>
<div class="col-md-6 border">Hadap : <?php echo $this->reff->getNamaHadap($val->hadap);?></div>
<div class="col-md-12 border"><center>Deskripsi</center>  <?php echo $val->desc;?></div>
<div class="col-md-12 border"><center>Keterangan</center> <?php echo $val->keterangan;?></div>
<div class="col-md-12 border"><center>Lokasi</center>  <?php echo $this->reff->getNamaKab($val->id_kab);?> - <?php echo $val->nama_area;?> - <?php echo $val->alamat_detail;?> </div>


</div>
</div>
 

</div>
</div>


  <script src="<?php echo base_url()?>plug/galery/dist/jquery.simpleGal.js"></script>
 
<script src="<?php echo base_url()?>plug/boostrap/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo base_url()?>plug/boostrap/js/jquery.slimscroll.min.js"></script> 




<script>
	$(document).ready(function() {
		//BOX BUTTON SHOW AND CLOSE
	   $('.thumbnails').simpleGal({
      mainImage: '.custom'
    });
		$('.conversation-inner').slimScroll({
	        height: '225px',
	        wheelStep: 35,
	    });
		

	});
	</script>