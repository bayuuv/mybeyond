<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Data_property extends CI_Controller {

	

	function __construct()
	{
		parent::__construct();	
		$this->m_konfig->validasi_session(array("user","dm","operator","promotion","agen"));
		$this->load->model("M_property","property");
		date_default_timezone_set('Asia/Jakarta');
	}
	function cek_kode_list()
	{
		$kode=$this->input->get_post("kode");
	echo	$this->db->get_where("data_property",array("kode_prop",$kode))->num_rows();
	}
	function _template($data)
	{
	$this->load->view('template/main',$data);	
	}
	function getKodeList()
	{
		  $carikode = $this->db->query("SHOW TABLE STATUS LIKE 'data_property'")->row();
			  $datakode =isset($carikode->Auto_increment)?($carikode->Auto_increment):"1";//$carikode->kode;
		  	    $kode = (int) $datakode;
		   		$newID = sprintf("%03s", $kode);
				//Ajeng
				if($this->session->userdata("id")==165){//Rena
				$thnx=substr(date('Y'),2,2);
				$thn=$thnx+5;
				//Rafha
				}elseif($this->session->userdata("id")==151){//Rafha
				$thnx=substr(date('Y'),2,2);
				$thn=$thnx+7;
				//Kiki
				}elseif($this->session->userdata("id")==161){//Yema
				$thnx=substr(date('Y'),2,2);
				$thn=$thnx+8;
				//Yudi
				}elseif($this->session->userdata("id")==146){//Yudi
				$thnx=substr(date('Y'),2,2);
				$thn=$thnx+9;
				}elseif($this->session->userdata("id")==152){//Vivi
				$thnx=substr(date('Y'),2,2);
				$thn=$thnx+10;
				}else{
				$thn=substr(date('Y'),2,2);
				}
				$bln=sprintf("%02s",date('m'));
				$tgl=sprintf("%02s",date('d'));
//$bulan=$this->tanggal->bulanRomawi($bulan);
		   	 echo $tgl.$bln.$thn."-".$newID;	
	}
	public function index()
	{

	$data['konten']="index";
	$this->_template($data);
	}
	public function listing()
	{

	$data['konten']="listing";
	$this->_template($data);
	}
	public function selling()
	{

	$data['konten']="selling";
	$this->_template($data);
	}
	public function edit($id)
	{
	$data['konten']="edit";
	$this->_template($data);
	}
	public function add()
	{
	$data['konten']="add";
	$this->_template($data);
	}
	public function analysis()
	{
	$data['konten']="analysis";
	$this->_template($data);
	}
	public function off_promo()
	{
	$data['konten']="off_promo";
	$this->_template($data);
	}
	function ajaxGetKab()
	{
		$id=$this->input->post("prov");
		$def=$this->input->post("def");
		$data=$this->reff->getKab($id);
		if($data)
		{
			$kab[""]="==== Pilih ====";
		 foreach ($data as $val) {
				 $kab[$val->id] = '['.$val->id.']'.' &nbsp;'.$val->kabupaten;
                 }
                 $dataKab = $kab;
                echo form_dropdown('kabupaten', $dataKab, $def, ' id="kabupaten" class="select2-container" style="width:100%" onchange="return cek_kab()"');     
		echo "<script>	$('#kabupaten').select2();</script>";
		}else{
			
			echo form_dropdown('kabupaten', array(), '', ' id="kabupaten" class="select2-container" style="width:100%"');     
			echo "<script>	$('#kabupaten').select2();</script>";
		}
	}
	
	function ajaxGetKab2()
	{
		$id=$this->input->post("prov");
		$def=$this->input->post("def");
		$data=$this->reff->getKab($id);
		if($data)
		{
			$kab[""]="==== Pilih ====";
		 foreach ($data as $val) {
				 $kab[$val->id] = '['.$val->id.']'.' &nbsp;'.$val->kabupaten;
                 }
                 $dataKab = $kab;
                echo form_dropdown('kabupaten', $dataKab, $def, ' id="kabupaten" class="form-control" style="width:100%" onchange="return cek_kab()"');     
		echo "<script>	$('#kabupaten').select2();</script>";
		}else{
			
			echo form_dropdown('kabupaten', array(), '', ' id="kabupaten" class="select2-container" style="width:100%"');     
			echo "<script>	$('#kabupaten').select2();</script>";
		}
	}
	/*function ajaxGetList()
	{
		$id=$this->input->post("list");
		$kt=$this->input->post("kt");
		$jabatan=$this->reff->getList($id);
		if($jabatan == '11')
		{ 
			echo "<input type=\"text\" onkeyup=\"hitungko()\" class=\"form-control\" style=\"width:20%\" id=\"komisi_persen_listing\" name=\"komisi_persen_listing\"   value=\"70\">
				<input type=\"text\" onkeyup=\"hitungko()\" onchange=\"hitungko()\" value=\"".number_format(($kt*70)/100,0,",",".")."\" class=\"form-control pull-right\" style=\"position:absolute;width:70%;margin-top:-34px;margin-left:80px\" id=\"terhitung_listing\" name=\"terhitung_listing\"  >
					<input type=\"hidden\" name=\"jenis_komisi_listing\" value=\"fee_persen\">
					</div>
				<!-------------------------------------------------------------------------------------------------------->	
					<div class=\"cleafix col-md-12 col-sx-12\" style=\"height:5px\">&nbsp;</div>
					
				<!------------------------------------------------------------------------------------------------>	
				<label for=\"email\" class=\"b col-lg-3 control-label\">Selling</label>
				<div class=\"col-lg-8\">
				<div class=\"btn-group\">
				<label class=\"btn-defauld\" onclick=\"selling(1)\">
				<input type=\"radio\" name=\"x\"    id=\"option1\" checked> Member of beyond
				</label> 
				<label class=\"btn-defauld\" style=\"margin-left:20px\"  onclick=\"selling(2)\">
				<input type=\"radio\" name=\"x\"   id=\"option2\">  Co Broke
				</label>
				</div>
				<span id=\"agenBeyond\">
														".
														$ref_agen = $this->reff->getAgen()."
														".$array_agen[""] = "==== Pilih Agen ===="."
														".foreach ($ref_agen as $val) {."
															".$array_agen[$val->kode_agen] = $val->nama." - ".$val->kode_agen."
														".}."
														".$data = $array_agen."
														".form_dropdown('selling1', $data, '', '  id="selling1" class="black form-control" style="width:100%"')."
				</span>				
				<span id=\"agenLain\">
				 
					
					<input type=\"text\" class=\"form-control\" placeholder=\"Nama\" id=\"selling2\" name=\"selling2\"  >
					
					 
				</span>	
				";
		}else{
			echo "<input type=\"text\" onkeyup=\"hitungko()\" class=\"form-control\" style=\"width:20%\" id=\"komisi_persen_listing\" name=\"komisi_persen_listing\"   value=\"50\">
				<input type=\"text\" onkeyup=\"hitungko()\" onchange=\"hitungko()\" value=\"".number_format(($kt*50)/100,0,",",".")."\" class=\"form-control pull-right\" style=\"position:absolute;width:70%;margin-top:-34px;margin-left:80px\" id=\"terhitung_listing\" name=\"terhitung_listing\"  >
					<input type=\"hidden\" name=\"jenis_komisi_listing\" value=\"fee_persen\">
					</div>
				<!-------------------------------------------------------------------------------------------------------->	
					<div class=\"cleafix col-md-12 col-sx-12\" style=\"height:5px\">&nbsp;</div>
					
				<!------------------------------------------------------------------------------------------------>	
				<label for=\"email\" class=\"b col-lg-3 control-label\">Selling</label>
				<div class=\"col-lg-8\">
				<div class=\"btn-group\">
				<label class=\"btn-defauld\" onclick=\"selling(1)\">
				<input type=\"radio\" name=\"x\"    id=\"option1\" checked> Member of beyond
				</label> 
				<label class=\"btn-defauld\" style=\"margin-left:20px\"  onclick=\"selling(2)\">
				<input type=\"radio\" name=\"x\"   id=\"option2\">  Co Broke
				</label>
				</div>
				<span id=\"agenBeyond\">
														".
														$ref_agen = $this->reff->getAgen();
														$array_agen[""] = "==== Pilih Agen ====";
														foreach ($ref_agen as $val) {
															$array_agen[$val->kode_agen] = $val->nama." - ".$val->kode_agen;
														}
														$data = $array_agen;
														form_dropdown('selling1', $data, '', '  id="selling1" class="black form-control" style="width:100%"')."
														?>
				</span>				
				<span id=\"agenLain\">
				 
					
					<input type=\"text\" class=\"form-control\" placeholder=\"Nama\" id=\"selling2\" name=\"selling2\"  >
					
					 
				</span>	
				";
		}
	}*/
	
	function getKomplek($id)
	{
		$data=$this->reff->getKomplek($id);
		$result="";
		foreach($data as $data)
		{
			$result[]=$data->komplek;
		}
		echo json_encode($result);
	}
	function getOwn()
	{
		$data=$this->reff->getOwner();
		$result="";
		foreach($data as $data)
		{
			$result[]=$data->hp;
		}
		echo json_encode($result);
	}
	function infoFlash()
	{
		$msg='<script>alert("Success! Tersimpan")</script>';
		$this->session->set_flashdata("info",$msg);
	}
	function insert()
	{
			$this->infoFlash();
	echo	$this->property->insert();
	}
	function cek_alamat_js()
	{
	echo	$this->property->cek_alamat_js();
	}
	function cek_alamat_js_edit()
	{
	echo	$this->property->cek_alamat_js_edit();
	}
	function update()
	{
			$this->infoFlash();
	echo	$this->property->update();
	}
	function hapus($id)
	{
	echo	$this->property->hapus($id);
	}function hapusSelling($id)
	{
	echo	$this->property->hapusSelling($id);
	}
	function HapusAll($id)
	{
	echo	$this->property->HapusAll($id);
	}function HapusAllSelling($id)
	{
	echo	$this->property->HapusAllSelling($id);
	}
	function createFactsheet($id)
	{
		echo	$this->property->im_factsheet($id);
	}
	function hapusF($id)
	{
	echo	$this->property->hapusF($id);
	}
	function hapusCeleb($id)
	{
	echo	$this->property->hapusCeleb($id);
	}
	function hapusCelebSI($id)
	{
	echo	$this->property->hapusCelebSI($id);
	}
	
	function ajax_property()
	{
		$level=$this->session->userdata("level");
		$list = $this->property->get_dataProperty();
        $data = array();
        $no = $_POST['start']+1;
		
        foreach ($list as $val) {
			$cancel='';
			//$status=$val->status;
			$type=$val->type_jual;
			if($type=="sewa")
			{ 
				if($val->status=="1")
				{	$status="Rented ";
					$onclik="onclick='return setBelumLaku(`".$val->kode_prop."`,`Tersewa`)'";
					$setitle="Set Unsold";
				}elseif($val->status=="2"){
					$onclik="onclick='return setBelumLaku(`".$val->kode_prop."`,`Tersewa`)'";
					$setitle="Set Rented";$status="<font color='red'>Cancel ".$this->reff->cancelBy($val->alasan_cancel)."  </font>   ";
				}elseif($val->status=="0"){
					$onclik="onclick='goSewa(`".$val->id_prop."`)'";
					$setitle="Set Sold";$status="Rent  ";
					$cancel='<li style="margin-left:-10px" onclick="return cancel(`'.$val->kode_prop.'`)"  ><a href="#"><i class="fa fa-check-circle"></i> Cancel </a></li>';
				}else{
				 
					$onclik="onclick='return setBelumLaku(`".$val->kode_prop."`,`Tersewa`)'";
					$setitle="Set Rented";$status="<font color='red'>Sold By Other ".$this->reff->cancelBy($val->alasan_cancel)."  </font>   ";
				}
			}elseif($type=="jual"){
				if($val->status=="1")
				{
					$onclik="onclick='return setBelumLaku(`".$val->kode_prop."`,`Terjual`)'";
					$setitle="Set unsold";$status="Sold  ";
				}elseif($val->status=="2"){
					$onclik="onclick='return setBelumLaku(`".$val->kode_prop."`,`Terjual`)'";
					$setitle="Set Sold";$status="<font color='red'>Cancel ".$this->reff->cancelBy($val->alasan_cancel)."  </font>   ";
				}elseif($val->status=="0"){
					$onclik="onclick='goJual(`".$val->id_prop."`)'";
					$setitle="Set Sold";$status="Sell";
					$cancel='<li style="margin-left:-10px" onclick="return cancel(`'.$val->kode_prop.'`)"  ><a href="#"><i class="fa fa-check-circle"></i> Cancel </a></li>';
				}else{	
				$onclik="onclick='return setBelumLaku(`".$val->kode_prop."`,`Terjual`)'";
					$setitle="Set Unsold";$status="<font color='red'>Sold By Other ".$this->reff->cancelBy($val->alasan_cancel)."  </font>   ";
				}
				
			}else{
					$onclik="onclick='return setBelumLaku(`".$val->kode_prop."`,`Cancel`)'";
					$setitle="Set Sold";			
					$status="<font color='red'>Cancel</font>  ";
			}
			
			if($val->hasil_cma == 'MAHAL')
			{
				$hasilcma = "<font color='red'>MAHAL</font>";
				$tampilcma = "<li class=\"divider\"></li>
				<li style=\"margin-left:-10px\"><a href=\"#\" onclick=\"return editCMA(".$val->id_prop.")\"><i class=\"fa fa-magic\"></i>Edit CMA</a></li>";
			}elseif($val->hasil_cma == 'MURAH'){
				$hasilcma = "<font color='#510999'>MURAH</font>";
				$tampilcma = "<li class=\"divider\"></li>
				<li style=\"margin-left:-10px\"><a href=\"#\" onclick=\"return editCMA(".$val->id_prop.")\"><i class=\"fa fa-magic\"></i>Edit CMA</a></li>";
			}else{
				if($this->reff->cekCMA($val->nama_area) == '1'){
					$hasilcma = "CMA Ready";
					$tampilcma = "<li class=\"divider\"></li>
				<li style=\"margin-left:-10px\"><a href=\"#\" onclick=\"return setCMA(".$val->id_prop.")\"><i class=\"fa fa-magic\"></i>CMA</a></li>";
				}else{
					$hasilcma = "CMA Not Ready";
					$tampilcma = "";
				}
			}
			
			
			$row = array();
			$row[] =  '<input type="checkbox" class="pilih" onclick="pilcek()" name="hapus[]" value="'.$val->id_prop.'">';
			//$row[] = $no++;
            $row[] = "<a href='javascript:detail(`".$val->kode_prop."`)'>".$val->kode_prop."</a>";
            $row[] = $this->reff->getNamaJenis($val->jenis_prop);
            $row[] = number_format($val->harga+$val->fee_up,0,",",".");
			$row[] = number_format($val->harga_area,0,",",".");
			$row[] = number_format($val->harga_cma,0,",",".");
			$row[] = $hasilcma;
			//$row[] = $val->hasil_cma;
			$row[] = $val->alamat_detail;// + area listing + 										gx usah= alamat proprty(nanti di display) 
			$row[] = $val->area_listing; //|| area-listing
			
			if($level!="dm") {   $row[] = "<a href='javascript:detailVendor(`".$val->id_owner."`)'>".$this->reff->getNamaOwner($val->id_owner)."</a>"; }
			
          
            $row[] = $this->reff->getNamaAgen($val->agen). "<br>".$val->agen;
            $row[] = "Diupload : ".$this->reff->cekJumlahGambar($val->id_prop)."   <br>Factsheet :".$this->reff->cekGambarDesain($val->id_prop);
     
			$row[] = $status;
			
			//$row[] = "Spanduk : ".$this->reff->totalPromoSpanduk($val->kode_prop)." <br>Koran :".$this->reff->totalPromoKoran($val->kode_prop)." <br>Maxi Banner:".$this->reff->totalPromoMaxi($val->kode_prop)." <br>Papan Tanah :".$this->reff->totalPromoPapan($val->kode_prop)."";
			$row[] = "Spanduk : ".$this->reff->totalPromoSpanduk($val->kode_prop)." <br>Koran :".$this->reff->totalPromoKoran($val->kode_prop)." <br>Maxi Banner:".$this->reff->totalPromoMaxi($val->kode_prop)." <br>Papan Tanah :".$this->reff->totalPromoPapan($val->kode_prop)."<br><a href='#' onclick='return detailP(`".$val->id_prop."`)'> Detail</a>";
		   
		   if($this->session->userdata("level")!="dm")
		   {
			if($this->reff->cekGambarDesain2($val->id_prop) == '')
			{
			$row[] = '
				<div class="btn-group">
				<button type="button" class="btn-default dropdown-toggle" data-toggle="dropdown">
				Action <span class="caret"></span>
				</button>
				<ul class="dropdown-menu" role="menu" style="color:black;font-size:15px;margin-left:-40px">
				<li style="margin-left:-10px"><a href="'.base_url().'data_property/edit/'.$val->id_prop.'"><i class="fa fa-edit"></i> Edit</a></li>
				<li style="margin-left:-10px"><a href="#" onclick="return hapusP(`'.$val->id_prop.'`)"><i class="fa fa-trash-o"></i> Delete</a></li>
				<li style="margin-left:-10px"><a href="#" onclick="return setNetwork(`'.$val->id_prop.'`)"><i class="fa  fa-hand-o-right"></i> Set Network</a></li>
				<li style="margin-left:-10px"><a href="#" onclick="return setPromo(`'.$val->kode_prop.'`)"><i class="fa fa-trophy"></i> Offline Promosi</a></li>
				<li class="divider"></li>
				<li style="margin-left:-10px"><a href="#" onclick="return factsheetP(`'.$val->kode_prop.'`)"><i class="fa fa-folder"></i> Create Factsheet</a></li>
				'.$tampilcma.'
				<li class="divider"></li>
				<li style="margin-left:-10px" '.$onclik.' ><a href="#"><i class="fa fa-check-circle"></i> '.$setitle.'</a></li>
				'.$cancel.'

				</a></li>
				</ul>
				</div>';
			}else{
			$row[] = '
				<div class="btn-group">
				<button type="button" class="btn-default dropdown-toggle" data-toggle="dropdown">
				Action <span class="caret"></span>
				</button>
				<ul class="dropdown-menu" role="menu" style="color:black;font-size:15px;margin-left:-40px">
				<li style="margin-left:-10px"><a href="'.base_url().'data_property/edit/'.$val->id_prop.'"><i class="fa fa-edit"></i> Edit</a></li>
				<li style="margin-left:-10px"><a href="#" onclick="return hapusP(`'.$val->id_prop.'`)"><i class="fa fa-trash-o"></i> Delete</a></li>
				<li style="margin-left:-10px"><a href="#" onclick="return setNetwork(`'.$val->id_prop.'`)"><i class="fa  fa-hand-o-right"></i> Set Network</a></li>
				<li style="margin-left:-10px"><a href="#" onclick="return setPromo(`'.$val->kode_prop.'`)"><i class="fa fa-trophy"></i> Offline Promosi</a></li>
				<li class="divider"></li>
				<li style="margin-left:-10px"><a href="#" onclick="return hapusF(`'.$val->id_prop.'`)"><i class="fa fa-trash-o"></i> Delete Factsheet</a></li>
				'.$tampilcma.'
				<li class="divider"></li>
				<li style="margin-left:-10px" '.$onclik.' ><a href="#"><i class="fa fa-check-circle"></i> '.$setitle.'</a></li>
				'.$cancel.'

				</a></li>
				</ul>
				</div>';
			}
			
		   }else{
			    $row[] = '<a href="#" onclick="return detail(`'.$val->kode_prop.'`)" class="btn btn-primary"><i class="fa fa-eye"></i> detail</a>';
		   }
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->property->counts(),
            "recordsFiltered" => $this->property->counts(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	
	
	function ajax_selling()
	{
		$list = $this->property->ajax_selling();
        $data = array();
        $no = $_POST['start']+1;
        foreach ($list as $val) {
			
			if($val->type_selling=="sewa")
			{
				$onclick="editselling(`".$val->id."`)";
			}else{
				$onclick="editJulling(`".$val->id."`)";
			}
		
			
			if($val->sumber_selling=="1")
			{
				$ejen=$this->reff->getNamaAgen($val->selling)."<br> <font size='2px'>".$val->selling."</a>";
				$ks=number_format($val->nominal_komisi_selling,0,",",".");
				$ksg= number_format($val->nominal_komisi_selling_net,0,",",".");
			}elseif($val->sumber_selling=="3"){
				$ejen=$this->reff->getNamaAgen($val->selling)."<br> <font size='2px'>".$val->selling."</a>";
				$ks=number_format($val->nominal_komisi_selling,0,",",".");
				$ksg= number_format($val->nominal_komisi_selling_net,0,",",".");
			}else{
				$ejen= "<font size='2px'>".$val->selling."</font>";
				$ks="";
				$ksg="";
			}
			
			$ejenListing= "<font size='2px'>".$val->kode_agen."</font>";
			if($val->sumber_listing=="1")
			{
				$ejenListing=$this->reff->getNamaAgen($val->kode_agen)."<br> <font size='2px'>".$val->kode_agen."</a>";
				$kl=number_format($val->nominal_komisi_listing,0,",",".");
				$klg=number_format($val->nominal_komisi_listing_net,0,",",".");
			}else{
				$kl="";
				$klg="";
			} 
			
			if($val->gambar_tv <> ''){
				$gambar_tv = "| <a href='".base_url()."/file_upload/img/".$val->gambar_tv."' target=\"_blank\">TV</a>";
			}else{
				$gambar_tv = "";
			}
			
			if($val->gambar_sosmed <> ''){
				$gambar_sosmed = "<a href='".base_url()."/file_upload/img/".$val->gambar_sosmed."' target=\"_blank\">Sosmed</a>";
			}else{
				$gambar_sosmed = "";
			}
			
			if($val->gambar_internal <> ''){
				$gambar_internal = "| <a href='".base_url()."/file_upload/img/".$val->gambar_internal."' target=\"_blank\">Internal</a>";
			}else{
				$gambar_internal = "";
			}
			
			if($val->gambar_tv2 <> ''){
				$gambar_tv2 = "| <a href='".base_url()."/file_upload/img/".$val->gambar_tv2."' target=\"_blank\">TV 2</a>";
			}else{
				$gambar_tv2 = "";
			}
			
			if($val->gambar_internal2 <> ''){
				$gambar_internal2 = "| <a href='".base_url()."/file_upload/img/".$val->gambar_internal2."' target=\"_blank\">Internal 2</a>";
			}else{
				$gambar_internal2 = "";
			}
			
			
		//	$kp="";	$kpl=$val->komisi_persen_listing."%";	$kps=$val->komisi_persen_selling."%";
			//$kp="";	$kpl= $kps=$val->komisi_persen_selling."%";
			$kp="";	$kpl= $kps="";
			if($val->komisi_persen){
				//$kp=$val->komisi_persen."%";
				$kp="";
			}
			if($this->reff->getNamaPelanggan(isset($val->id_pelanggan)?($val->id_pelanggan):""))
			{
			$buyer=	"<a href='javascript:detailBuyer(`".$val->id_pelanggan."`)'>".$this->reff->getNamaPelanggan(isset($val->id_pelanggan)?($val->id_pelanggan):"")."</a>";
			}else{
			$buyer=$val->id_pelanggan;
			}
			$type=$val->type_selling;
			if($type=="sewa"){ $tgljth="<br/> exp: ".$this->reff->tgl_jatuh_tempo($val->kode_listing)." <br><a href='".base_url()."file_upload/dok/".$val->id.".zip' download>Download File</a>  "; }else{ $tgljth=""; };
			$row = array();
			$row[] =  '<input type="checkbox" class="pilih" onclick="pilcek()" name="hapus[]" value="'.$val->id.'">';
		//	$row[] = $no++;
          	$row[] = $this->tanggal->ind($val->tgl_closing,"/");
          	$row[] = $this->reff->getAlamatListingByKode($val->kode_listing);
          	$row[] = $val->kode_listing;
			$row[] = "<a href='javascript:detailVendor(`".$this->reff->getIdOwnerByListing($val->kode_listing)."`)'>".$this->reff->getNamaOwnerByListing($val->kode_listing)."</a>";
			$row[] = $buyer;
			
          	$row[] = number_format($val->harga,0,",",".");
			$row[] = number_format($val->harga_net,0,",",".");
			$row[] =$kp."". number_format($val->total_komisi,0,",",".");
            $row[] =$ejenListing;
				$row[] =$kpl."". $kl;
			$row[] = $klg;
            $row[] = $ejen;
			$row[] =$kps."". $ks;
			$row[] = $ksg;
           
            $row[] = $val->ket;
			$row[] = "".$gambar_sosmed." ".$gambar_internal." ".$gambar_tv." ".$gambar_internal2." ".$gambar_tv2."";
			if($val->gambar_tv <> '' && $val->gambar_sosmed <> '' && $val->gambar_internal <> ''){
			$row[] = '
			<a href="#" class="btn btn-xs btn-primary" onclick="'.$onclick.'" ><i class="fa fa-edit"></i> Edit </a>
			<a href="#" class="btn btn-danger" onclick="return hapusSelling(`'.$val->id.'`)" ><i class="fa fa-edit"></i> Delete </a>
			<a href="'.base_url().'/data_property/kuitansi/?id='.$val->id.'" class="btn btn-info"><i class="fa fa-print"></i> Print </a>
			<a href="#" class="btn btn-danger" onclick="return hapusCeleb(`'.$val->id_celeb.'`)" ><i class="fa fa-edit"></i> Delete Celebrate </a>
			';
			}elseif($val->gambar_tv == '' && $val->gambar_sosmed <> '' && $val->gambar_internal <> ''){
			$row[] = '
			<a href="#" class="btn btn-xs btn-primary" onclick="'.$onclick.'" ><i class="fa fa-edit"></i> Edit </a>
			<a href="#" class="btn btn-danger" onclick="return hapusSelling(`'.$val->id.'`)" ><i class="fa fa-edit"></i> Delete </a>
			<a href="'.base_url().'/data_property/kuitansi/?id='.$val->id.'" class="btn btn-info"><i class="fa fa-print"></i> Print </a>\
			<a href="#" class="btn btn-danger" onclick="return hapusCelebSI(`'.$val->id_celeb.'`)" ><i class="fa fa-edit"></i> Del Celebrate </a>
			<a href="#" class="btn btn-info" onclick="celebTV(`'.$val->id.'`)" ><i class="fa fa-share-alt"></i> Celebrate TV</a>
			';
			}else{
			$row[] = '
			<a href="#" class="btn btn-xs btn-primary" onclick="'.$onclick.'" ><i class="fa fa-edit"></i> Edit </a>
			<a href="#" class="btn btn-danger" onclick="return hapusSelling(`'.$val->id.'`)" ><i class="fa fa-edit"></i> Delete </a>
			<a href="'.base_url().'/data_property/kuitansi/?id='.$val->id.'" class="btn btn-info"><i class="fa fa-print"></i> Print </a>
			<a href="#" class="btn btn-xs btn-primary" onclick="celeb(`'.$val->id.'`)" ><i class="fa fa-share-alt"></i> Celebrate </a>
			';
			}
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->property->counts_ajax_selling(),
            "recordsFiltered" => $this->property->counts_ajax_selling(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	
	function getListing()
	{
	$provinsi=$this->input->post("provinsi");
	$kabupaten=$this->input->post("kabupaten");
	$area=$this->input->post("area");
	$lat_area=$this->input->post("lat_area");
	$long_area=$this->input->post("long_area");
	$jenis_pro=$this->input->post("jenis_pro");
	$type_pro=$this->input->post("type_pro");
	$kamar_tidur=$this->input->post("kamar_tidur");
	$kamar_mandi=$this->input->post("kamar_mandi");
	$garasi=$this->input->post("garasi");
	$daya_listrik=$this->input->post("daya_listrik");
	$harga_min=$this->input->post("harga_min");
	$harga_max=$this->input->post("harga_max");
	$sertifikat=$this->input->post("sertifikat");
	$agen=$this->input->post("agen");
	$type_sewa=$this->input->post("type_sewa");
	$kelengkapan=$this->input->post("kelengkapan");
	$status_penjualan=$this->input->post("status_penjualan");
	$jenis_list=$this->input->post("jenis_list");
	$furniture=$this->input->post("furniture");
	$air=$this->input->post("air");
	$fee_persen=$this->input->post("fee_persen");
	$fee_nominal=$this->input->post("fee_nominal");
	$fee_up=$this->input->post("fee_up");
	$tgl_masuk_listing=$this->tanggal->eng_($this->input->post("tgl_masuk_listing"),"-");
	$filter['filter']="?tgl_masuk_listing=$tgl_masuk_listing&fee_up=$fee_up&fee_nominal=$fee_nominal&fee_persen=$fee_persen&air=$air&furniture=$furniture&jenis_list=$jenis_list&type_pro=$type_pro&provinsi=$provinsi&kabupaten=$kabupaten&area=$area&lat_area=$lat_area&long_area=$long_area&jenis_pro=$jenis_pro&kamar_mandi=$kamar_mandi&kamar_tidur=$kamar_tidur&garasi=$garasi&daya_listrik=$daya_listrik&harga_min=$harga_min&harga_max=$harga_max&sertifikat=$sertifikat&agen=$agen&type_sewa=$type_sewa&kelengkapan=$kelengkapan&status_penjualan=$status_penjualan";
	echo	$this->load->view("getListing",$filter);
	}
	
	
	function getSelling()
	{
	$agen=$this->input->post("agen");
	$vendor=$this->input->post("vendor");
	$buyer=$this->input->post("buyer");
	$tahun_sell=$this->input->post("tahun_sell");
	$filter['filter']="?agen=$agen&vendor=$vendor&buyer=$buyer&tahun_sell=$tahun_sell";
	echo	$this->load->view("getSelling",$filter);
	}
	
	
	function getDataDetail()
	{
		$data['kode_prop']=$this->input->post("id");
	echo	$this->load->view("dataDetail",$data);
	}
	function getFormSewa($id)
	{	$data["id"]=$id;
		$this->load->view("formSewa",$data);
	}
	function getFormJual($id)
	{	$data["id"]=$id;
		$this->load->view("formJual",$data);
	}
	function getFormCMA($id)
	{	$data["id"]=$id;
		$this->load->view("formCMA",$data);
	}
	function getFormEditCMA($id)
	{	$data["id"]=$id;
		$this->load->view("formEditCMA",$data);
	}
	function getListingEditSewa($id)
	{	$data["id"]=$id;
		$this->load->view("FormEditSewa",$data);
	}
	function getListingEditJual($id)
	{	$data["id"]=$id;
		$this->load->view("FormEditJual",$data);
	}
	function getOffpromo($id)
	{	$data["id"]=$id;
		$this->load->view("FormOffpromo",$data);
	}
	function getCeleb($id)
	{	$data["id"]=$id;
		$this->load->view("FormCeleb",$data);
	}
	function AddCeleb()
	{
		echo $this->property->createCeleb();
	}
	function getCelebTV($id)
	{	$data["id"]=$id;
		$this->load->view("FormCelebTV",$data);
	}
	function AddCelebTV()
	{
		echo $this->property->createCelebTV();
	}
	function insertSellingJual()
	{
		echo $this->property->insertSellingJual();
	}function UpdateSelling()
	{
		echo $this->property->UpdateSelling();
	}function UpdateSellingSewa()
	{
		echo $this->property->UpdateSellingSewa();
	}function insertSellingSewa()
	{
		echo $this->property->insertSellingSewa();
	}
	function setBelumLaku($kode)
	{
	echo	$this->property->updateListingStatus($kode,"0","");
	}
	function setCancel($kode)
	{
	echo	$this->property->updateListingStatus($kode,"2","");
	}
	function soldByOwner($kode)
	{
		echo	$this->property->soldByOwner($kode,"2");
	}
	function insertHasilCMA()
	{
		echo $this->property->insertHasilCMA();
	}
	function editHasilCMA()
	{
		echo $this->property->editHasilCMA();
	}
	
	
	function getDataDetailPromo($id)
	{
		$data["id"]=$id;
		$this->load->view("getDataDetailOffPromo",$data);
	}
	
	function ajax_promodetail($id)
	{
		$list = $this->property->get_dataPromo($id);
        $data = array();
        $no = $_POST['start']+1;
        foreach ($list as $val) {
			if($val->jenis_promo == '1'){
				$jenis_promo = "Koran";
			}elseif($val->jenis_promo == '2'){
				$jenis_promo = "Maxi Banner";
			}if($val->jenis_promo == '3'){
				$jenis_promo = "Papan Tanah";
			}else{
				$jenis_promo = "Spanduk";
			}
			
			if($val->status == '1'){
				$status = "Aktif";
			}else{
				$status = "<font color='red'>Tidak Aktif</font>";
			}
			
			$row = array();
			$row[] = $no++;
            $row[] = $this->tanggal->ind($val->tgl_promo,"/");
			$row[] = $val->kode_prop;
			$row[] = $val->alamat_detail;
			$row[] = $val->area_listing;
			$row[] = $val->nama;
            $row[] = $val->alamat_promo;
            $row[] = '<img src="'.base_url().'file_upload/img/'.$val->foto_promo.'" width="200px" height="200px" >';
			$row[] = $jenis_promo;
			$row[] = $status;
			
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->property->countsList($id),
            "recordsFiltered" => $this->property->countsList($id),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	
	
	function getHistory()
	{
		 ?>						
			<ul style='width:100%;list-style:none;margin-left:-20px'>
			<?php
			$history=$this->reff->getReportProp($this->input->post("id"));
			foreach($history as $list)
			{?>
				 


						<br>
						<li class="clearfix" style="border-bottom:black solid 0.5px">
						<div class="post-time" style='font-size:12px'>
						<i class="fa fa-calendar"></i> <?php echo $this->tanggal->hariLengkapJam($list->tgl,"/")?>
						<a class="pull-right" style="font-size:15px;color:red" title="Delete" href="javascript:hapus(`<?php echo $list->id;?>`)">&times;</a>
						</div>
						<div class="title">
						<a href="#"><?php echo $this->reff->getReportTitle($list->id_title);?></a> 
						
						</div>

						<div class="text black sadow5">
									<?php echo $list->ket;?>
									</div>
						</li>

						 


						<?php }
						?>

						 </ul>
						<?php
		}
	 function goExpired()
	 {
		 $tgl=$this->input->post("tgl");
		$data=explode("/",$tgl);
		 if(count($data))
		 {
			 $thn=$data[2]+1;
			echo $data[0]."/".$data[1]."/".$thn; 
		 }
	 }
				
	function saveReport()
	{
		echo $this->property->saveReport();
	}
	
	function delHistory($id)
	{
		echo $this->property->delHistory($id);
	}
	
	function export()
	{
		$this->property->export();
	}
	
	function export_selling()
	{
		$this->property->export_selling();
	}
	
	function export_invoice()
	{
		$this->property->export_invoice();
	}
	function saveStatus()
	{
		$s=$this->input->get_post("s");
		$id=$this->input->get_post("id");
		echo $this->property->saveStatus($id,$s);
	}
	function cekNetwork($id)
	{
		$this->db->where("id_prop",$id);
		$data=$this->db->get("data_property")->row();
	return isset($data->id_network)?($data->id_network):"";
	}
	function setNetwork($id)
	{
		 $kab[""]="==== Pilih ====";
		 $this->db->where("jabatan","100");
		 $this->db->order_by("nama","ASC");
		 $data=$this->db->get("data_agen")->result();
		 foreach ($data as $val) {
				 $kab[$val->id_agen] = $val->nama." - ".$val->alamat;
                 }
                 $dataKab = $kab;
                echo form_dropdown('id_network', $dataKab, $this->cekNetwork($id), ' id="id_network" class="form-control select2-container" style="width:100%"');    
				echo "<input type='hidden' name='id_prop' value='".$id."'>";
	}
	function saveNetwork()
	{
		$id_prop=$this->input->get_post("id_prop");
		$id_network=$this->input->get_post("id_network");
		$this->db->set("id_network",$id_network);
		$this->db->where("id_prop",$id_prop);
		echo $this->db->update("data_property");
	}
	function AddPromoOff()
	{
		echo $this->property->insertPromo();
	}
	
	public function word(){
		 	  error_reporting(0);
		header ("Content-type: text/html; charset=utf-8");
		
		$this->load->library('PHPWord');
        $this->load->helper('download');
		
		$PHPWord = new PHPWord();	
		$val=$grid = $this->property->get_one($this->input->get_post("id"))->row();
		if($k=$val->komplek){ $kom= "- Komplek : $k ";}else{ $kom="";};
		
	    $document 		   = $PHPWord->loadTemplate('format2.docx');		
		$document->setValue('{kode_prop}',$grid->kode_prop);
		$document->setValue('{jenis}',$this->reff->getNamaJenis($grid->jenis_prop));
		$document->setValue('{vendor}', $this->reff->getNamaOwner($val->id_owner));
		$document->setValue('{land}', $val->luas_tanah);
		$document->setValue('{kt}', $val->kamar_tidur );
		$document->setValue('{km}', $val->kamar_mandi);
		$document->setValue('{ktp}', $val->kamar_tidur_p);
		$document->setValue('{kmp}', $val->kamar_mandi_p);
		$document->setValue('{electricity}', $val->daya_listrik);
		$document->setValue('{status}', $this->reff->getNamaJenisSertifikat($val->jenis_sertifikat));
		$document->setValue('{garage}',  $val->jml_garasi);
		$document->setValue('{compas}', $this->reff->getNamaHadap($val->hadap));
		$document->setValue('{entry_date}', $this->tanggal->ind($val->tgl_masuk_listing,"/"));
		$document->setValue('{expire_date}', $this->tanggal->ind($val->tgl_expired,"/"));
		$document->setValue('{desc}', $val->desc );
		$document->setValue('{loc}', $this->reff->getNamaKab($val->id_kab)." - ". $val->nama_area." - ".$val->alamat_detail." ".$kom );
		 
		$document->setValue('{price}', number_format($val->harga+$val->fee_up,0,",","."));
		$document->setValue('{member}', $this->reff->getNamaAgenByKode($val->agen));
		$document->setValue('{building}',$val->luas_bangunan);
		$document->setValue('{floor}', $val->jml_lantai);
		$document->setValue('{furniture}', $this->reff->getNamaFurniture($val->furniture));
		$document->setValue('{carport}', $val->jml_carports);
		$document->setValue('{water}', $this->reff->getNamaAir($val->air));
		
		//$document->setValue('{gambar1}',array('src' => base_url().'file_upload/img/'.$val->gambar1,'swh'=>'250'));
//	$document->setImageValue('{gambar1}',base_url().'file_upload/img/'.$val->gambar1);
				
		   $history=$this->reff->getReportProp($val->kode_prop);
			$date=array();$kategory=array();$note=array();
			foreach($history as $list)
			{								
				$date[]=$this->tanggal->hariLengkapJam($list->tgl,"/");
				$kategory[]= $this->reff->getReportTitle($list->id_title);
				$note[]= $list->ket;
			}
					$data1 = array(
					'date' => $date,
					'kategory' => $kategory,
					'note' => $note,
					 );
				$document->cloneRow('tb', $data1);	
		
	//	$image_path=base_url()."file_upload/img/".$val->gambar1;
	//	$document->save_image('{gambar1}',$image_path,$document);
		
		$namafile = str_replace(" ","",$val->kode_prop).'.docx';
		$tmp_file = "plug/".$namafile.'';
		$document->save($tmp_file);
		 
    
        $pth    =   file_get_contents(FCPATH."plug/".$namafile."");
		$nme    =   "ID:".str_replace(" ","",$val->kode_prop).".docx";
		unlink(FCPATH."plug/".$namafile);
		force_download($nme, $pth); 
		
	}
	public function kuitansi(){
		error_reporting(0);
		header ("Content-type: text/html; charset=utf-8");
		
		$this->load->library('PHPWord');
        $this->load->helper('download');
		
		$PHPWord = new PHPWord();	
		//$val=$grid = $this->property->get_one($this->input->get_post("id"))->row();
		//if($k=$val->komplek){ $kom= "- Komplek : $k ";}else{ $kom="";};
	    
	    $id = $this->input->get_post("id");
	    
        $this->db->where("id",$id);
        $db=$this->db->get("data_selling")->row();
        
        $tgldb = explode("-",$db->tgl_closing);
		$bln = $tgldb[1];
			
		$pph = $db->nominal_komisi_selling * 2.5 / 100;
		$feetotal = $db->nominal_komisi_selling - $pph;
		
		$sumber_selling = $db->sumber_selling;
		if($sumber_selling == '1'){
			$agenjual = $db->selling;
		}else{
			$agenjual = $db->kode_agen;
		}
		
		if($db->sumber_selling == $db->sumber_listing AND $db->kode_agen != $db->selling){
			$fee_agen = $db->nominal_komisi_listing_net;
			$fee_agen2 = $db->nominal_komisi_selling_net;
		}elseif($db->sumber_selling != $db->sumber_listing){
			$fee_agen = $db->nominal_komisi_listing_net;
			$fee_agen2 = $db->nominal_komisi_selling_net;
		}else{
			$fee_agen = $db->nominal_komisi_listing;
		}
		
		if($db->sumber_selling=="3"){
			$komisi_agen = $db->total_komisi;
			$document 		   = $PHPWord->loadTemplate('inv.docx');
			$namaagen = $this->reff->getNamaAgen($db->kode_agen);
		}elseif($db->sumber_selling <> $db->sumber_listing){
			if($db->sumber_selling=="1"){
				$komisi_agen = $db->nominal_komisi_listing;
			}elseif($db->sumber_listing=="1"){
				$komisi_agen = $db->total_komisi;
			}else{
				$komisi_agen = $db->total_komisi;
			}
			$document 		   = $PHPWord->loadTemplate('inv2.docx');
			if($db->sumber_selling == '1'){
				$namaagen = $this->reff->getNamaAgen($db->selling);
			}else{
				$namaagen = $this->reff->getNamaAgen($db->kode_agen);
			}
		}else{
			$komisi_agen = $db->total_komisi;
			$document 		   = $PHPWord->loadTemplate('inv2.docx');
			$namaagen = $this->reff->getNamaAgen($db->kode_agen);
		}
		
		$pph = $fee_agen/2*0.05;
		$jmlfee = $fee_agen-$pph;
		
		$pph2 = $fee_agen2/2*0.05;
		$jmlfee2 = $fee_agen2-$pph2;
		
		$document->setValue('{noinv}', 'BREA/'.$this->tanggal->bulanRomawi($bln).'/FEE/'.$db->id.'/PCXX/'.$tgldb[0].'');
		$document->setValue('{vendor}', $this->reff->getJKOwnerByListing($db->kode_listing).' '.$this->reff->getNamaOwnerByListing($db->kode_listing));
		$document->setValue('{agen}', $this->reff->getNamaAgen($agenjual));
		$document->setValue('{alamat}', $this->reff->getAlamatOwnerByListing($db->kode_listing));
		$document->setValue('{harga}', number_format($db->harga,0,",","."));
		$document->setValue('{properti}', $this->reff->getAlamatListingByKode($db->kode_listing));
		$document->setValue('{komisi}', number_format($komisi_agen,0,",","."));
		$document->setValue('{feeagen}', number_format($fee_agen,0,",","."));
		$document->setValue('{feeagen2}', number_format($fee_agen2,0,",","."));
		$document->setValue('{terbilang}', $this->konversi->terbilang($db->harga).' rupiah');
		$document->setValue('{tanggal}', $tgldb[2].' '.$this->konversi->bulan($bln).' '.$tgldb[0]);
		$document->setValue('{pph}', number_format($pph,0,",","."));
		$document->setValue('{jmlfee}', number_format($jmlfee,0,",","."));
		$document->setValue('{pph2}', number_format($pph2,0,",","."));
		$document->setValue('{jmlfee2}', number_format($jmlfee2,0,",","."));
		$document->setValue('{namaagen}', $namaagen);
		$document->setValue('{namaagen2}', $this->reff->getNamaAgen($db->selling));
		$document->setValue('{lb}', $this->reff->getLBListing($db->kode_listing));
		$document->setValue('{lt}', $this->reff->getLTListing($db->kode_listing));
		
				
	    $namafile = str_replace(" ","",$this->reff->getNamaOwnerByListing($db->kode_listing)).'.docx';
		$tmp_file = "plug/".$namafile.'';
		$document->save($tmp_file);
		 
    
        $pth    =   file_get_contents(FCPATH."plug/".$namafile."");
		$nme    =   "Invoice:".str_replace(" ","",$this->reff->getAlamatListingByKode($db->kode_listing)).".docx";
		unlink(FCPATH."plug/".$namafile);
		force_download($nme, $pth); 
		
	}
	
	public function export_invoices()
	{
		error_reporting(0);
		header ("Content-type: text/html; charset=utf-8");
		
		$this->load->library('PHPExcel');
        $this->load->helper('download');
//////start
        $objPHPExcel = new PHPExcel();
//style
    /*    $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'rotation' => 0,
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '6CCECB')
            ),
            'borders' =>
            array('allborders' =>
                array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'),
                ),
            ),
        );*/
     //   $objPHPExcel->getActiveSheet(0)->getColumnDimension('F')->setWidth(25);
     //   $objPHPExcel->getActiveSheet(0)->getColumnDimension('G')->setWidth(25);
     //   $objPHPExcel->getActiveSheet(0)->getColumnDimension('H')->setWidth(35);


	$inputFileName = 'invoice.xlsx';

	/*check point*/

	// Read the existing excel file
	$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
	$objReader = PHPExcel_IOFactory::createReader($inputFileType);
	$objPHPExcel = $objReader->load($inputFileName);
	
	$id = $this->input->get_post("id");
	    
    $this->db->where("id",$id);
    $db=$this->db->get("data_selling")->row();
        
    $tgldb = explode("-",$db->tgl_closing);
    $bln = $tgldb[1];
        
    $pph = $db->nominal_komisi_selling * 2.5 / 100;
    $feetotal = $db->nominal_komisi_selling - $pph;
	
	$sumber_selling = $db->sumber_selling;
	if($sumber_selling == '1'){
		$agenjual = $db->selling;
	}else{
		$agenjual = $db->kode_agen;
	}
	
	if($db->sumber_selling == $db->sumber_listing AND $db->kode_agen != $db->selling){
		$fee_agen = $db->nominal_komisi_listing_net;
		$fee_agen2 = $db->nominal_komisi_selling_net;
	}elseif($db->sumber_selling != $db->sumber_listing){
		$fee_agen = $db->nominal_komisi_listing_net;
		$fee_agen2 = $db->nominal_komisi_selling_net;
	}else{
		$fee_agen = $db->nominal_komisi_listing;
	}
	
	if($db->sumber_selling=="3"){
		$komisi_agen = $db->total_komisi;
	}elseif($db->sumber_selling <> $db->sumber_listing){
		$komisi_agen = $db->nominal_komisi_listing;
	}else{
		$komisi_agen = $db->total_komisi;
	}

	// Update it's data
	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);

	// Add column headers
	$objPHPExcel->getActiveSheet()
				->setCellValue('C7', $this->reff->getNamaOwnerByListing($db->kode_listing))
				->setCellValue('C8', $this->reff->getAlamatOwnerByListing($db->kode_listing))
				->setCellValue('C9', $this->reff->getAlamatListingByKode($db->kode_listing))
				->setCellValue('D10', $komisi_agen)
				->setCellValue('A11', 'TERBILANG ('.$this->konversi->terbilang($komisi_agen).' rupiah)')
				->setCellValue('C13', ''.$db->komisi_persen.'%')
				->setCellValue('E5', 'NO : BREA/'.$this->tanggal->bulanRomawi($bln).'/FEE/'.$db->id.'/PCXX/'.$tgldb[0].'')
				->setCellValue('E19', 'BANDUNG, '.$this->tanggal->harilengkap3($db->tgl_closing,'-').'')
				;
	
	$objPHPExcel->setActiveSheetIndex(1);

	// Add column headers
	$objPHPExcel->getActiveSheet()
				->setCellValue('C7', $this->reff->getNamaAgen($agenjual))
				->setCellValue('C9', $db->harga)
				->setCellValue('C10', '('.$this->konversi->terbilang($db->harga).' rupiah)')
				->setCellValue('C12', $fee_agen)
				;
	
	if($db->sumber_selling == $db->sumber_listing AND $db->kode_agen != $db->selling){
	$objPHPExcel->setActiveSheetIndex(2);
	
	$objPHPExcel->getActiveSheet()
				->setCellValue('C7', $this->reff->getNamaAgen($db->kode_agen))
				->setCellValue('C9', $db->harga)
				->setCellValue('C10', '('.$this->konversi->terbilang($db->harga).' rupiah)')
				->setCellValue('C12', $fee_agen2)
				;
	}elseif($db->sumber_selling == '3' AND $db->kode_agen != $db->selling){
	$objPHPExcel->setActiveSheetIndex(2);
	
	$objPHPExcel->getActiveSheet()
				->setCellValue('C7', $this->reff->getNamaAgen($db->selling))
				->setCellValue('C9', $db->harga)
				->setCellValue('C10', '('.$this->konversi->terbilang($db->harga).' rupiah)')
				->setCellValue('C12', $fee_agen2)
				;
	}
	
	// Generate an updated excel file
	// Redirect output to a client’s web browser (Excel2007)
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	//header('Content-Disposition: attachment;filename="' . $inputFileName . '"');
	header('Content-Disposition: attachment;filename="Invoice:'.str_replace(" ","",$this->reff->getAlamatListingByKode($db->kode_listing)).'.xlsx"');
	header('Cache-Control: max-age=0');

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');

//////finish
    
	}
	
	 function ajax_cma()
	{
		$list = $this->property->get_dataCMA();
        $data = array();
        $no = $_POST['start']+1;
        foreach ($list as $val) {
			$row = array();
			if($this->session->userdata("level")!="agen"){
			$row[] =  '<input type="checkbox" class="pilih" onclick="pilcek()" name="hapus[]" value="'.$val->id_cma.'">';
			}
			//$row[] = $no++;
            $row[] = $no++;
			$row[] = $val->nama_area;
			$row[] = number_format($val->harga_area,0,",",".");
			//$row[] = number_format($val->harga_bangunan,0,",",".");
			
			if($this->session->userdata("level")!="agen"){
            $row[] = '
			 <a href="'.base_url().'data_property/edit_analysis/'.$val->id_cma.'" style="font-size:14px"  class=" "><i class="fa fa-edit "> </i> Edit </a>
			| <a href="#" style="font-size:14px" onclick="hapus_cma(`' . $val->id_cma . '`);" class=" "><i class="fa fa-trash"> </i> Delete </a>';
			}
			
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->property->counts_t(),
            "recordsFiltered" => $this->property->counts_t(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	function edit_analysis($id)
	{
	$echo=$this->property->get_cma($id);
	$data['datax']=$echo;
	$data['konten']="edit_analysis";
	$this->_template($data);
	}
	
	function simpan_cma()
	{
		echo $this->property->simpan_cma();
	}
	function update_cma()
	{
	echo	$this->property->update_cma();
	}
	
	function hapus_cma()
	{
	echo	$this->property->hapus_cma();
	}
	function HapusAllCMA($id)
	{
	echo $this->property->HapusAllCMA($id);
	}
	
}