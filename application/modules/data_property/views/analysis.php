<style>
#mapx {
    height: 500px;
	width : 100%;
}

</style>

<script>
    // This example adds a search box to a map, using the Google Place Autocomplete
    // feature. People can enter geographical searches. The search box will return a
    // pick list containing a mix of places and predicted search terms.

    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    function initAutocomplete() {
        var map = new google.maps.Map(document.getElementById('mapx'), {
            center: {lat: -6.917463899999999, lng: 107.61912280000001},
            zoom: 10,
            mapTypeId: 'roadmap'
        });


        // Create the search box and link it to the UI element.
        var input = document.getElementById('areax');
        var searchBox = new google.maps.places.SearchBox(input);
        //   map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function () {
            searchBox.setBounds(map.getBounds());

        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function () {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }

            // Clear out the old markers.
            markers.forEach(function (marker) {
                marker.setMap(null);
            });
            markers = [];

            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function (place) {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                var icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                markers.push(new google.maps.Marker({
                    map: map,
                    icon: icon,
                    title: place.name,
					//draggable:true,
                    position: place.geometry.location
                }));

                document.getElementById('lat_areax').value = place.geometry.location.lat();
                document.getElementById('long_areax').value = place.geometry.location.lng();


                    var input = document.getElementById('alamat_detail');
           var autocomplete = new google.maps.places.Autocomplete(input);
           google.maps.event.addListener(autocomplete, 'place_changed', function () {
               var place = autocomplete.getPlace();
               // document.getElementById('city').value = place.name;
               document.getElementById('lat').value = place.geometry.location.lat();
               document.getElementById('long').value = place.geometry.location.lng();
               //alert("This function is working!");
               //alert(place.name);
               // alert(place.address_components[0].long_name);

           });


                /*
                 var bandung = new google.maps.LatLng(-6.917463899999999);
                 var cimahi = new google.maps.LatLng(-6.884082,107.541304);
                 var soreang = new google.maps.LatLng(-7.025202,107.525908);
                 var btscoverage = new google.maps.Polygon({
                 path: [bandung, cimahi, soreang],
                 strokeColor: "#0000F7",
                 strokeOpacity: 0.8,
                 strokeWeight: 2,
                 fillColor: "#0000F7",
                 fillOpacity: 0.3
                 });
                 btscoverage.setMap(map);
                 */

                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            map.fitBounds(bounds);

           
        });
    }




</script>
<script  src="https://maps.google.com/maps/api/js?libraries=places&key=AIzaSyChmzyUaK6RZopwMAMlE49VgRa9CQsEkFY&callback=initAutocomplete" defer type="text/javascript"></script>


<span class="create">
<form action="javascript:simpan()" id="form">
<span class="msg"></span>
<button class="btn-primary pull-right" onclick="simpan()" > <i class="fa fa-plus-circle"></i> Save</button>
<button  class="btn-danger pull-right" onclick="return cancel()"> <i class="fa fa-plus-circle"></i> Cancel</button>
 <div class="row">
  
    <div class="col-lg-12">
        <div class="main-box clearfix" >
			<div class="main-box-body clearfix left">
			<span class="msg pull-right" style="margin-top:10px;margin-right:20px"></span>
		<a href="#"><h4><i class="fa fa-thumb-tack"></i> Property :: CMA</h4></a>
		
		<hr>
		  
		<table width="100%"  >
		
		
		<tr>
			<td width="100px">
				<label for="inputEmail1" class=" black control-label"><b>PROVINCE</b></label>
			</td>
			<td align="left">
				<?php
                $ref_type = $this->reff->getProvinsi();
                $array_prov[""] = "==== choose ====";
                foreach ($ref_type as $val) {
                    $array_prov[$val->id] = '[' . $val->id . ']' . ' &nbsp;' . $val->provinsi;
                }
                $data = $array_prov;
                echo form_dropdown('provinsi', $data, 32, 'onchange="return cek_prov()"   id="provinsi" class="select2-container" style="width:100%"');
                ?>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		
		<tr>
			<td width="100px">
				<label for="inputEmail1" class=" black control-label"><b>City</b></label>
			</td>
			<td align="left">
				<span class="dataKab">
					<select id="kabupaten" name="kabupaten" class="select2-container" style="width:100%"></select>
                </span>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		
		<tr>
			<td width="100px">
				<label for="inputEmail1" class=" black control-label"><b>Location</b></label>
			</td>
			<td align="left">
				 <input type="text" name="areax" id="areax" placeholder="" class="form-control">
                 <input type="hidden" name="lat_areax" id="lat_areax" >
                 <input type="hidden" name="long_areax" id="long_areax" >
			</td>
		</tr> 
		<tr>
			<td>&nbsp;</td>
		</tr>
		
		<tr>
			<td width="100px">
				<label for="inputEmail1" class=" black control-label"><b>MAP</b></label>
			</td>
			<td>
				 <div id="mapx"></div>
			</td>
		</tr> 
		<tr>
			<td>&nbsp;</td>
		</tr>
		
		<tr>
			<td width="100px">
				<label for="inputEmail1" class=" black control-label"><b>Land Price</b></label>
			</td>
			<td align="left">
			<input type="text" class="form-control" onchange="" onkeydown="return numbersonly(this, event);" id="harga_area" name="harga_area">
			</td>
		</tr> 
		<tr>
			<td>&nbsp;</td>
		</tr>
		<!--
		
				<tr>
			<td width="100px">
				<label for="inputEmail1" class=" black control-label"><b>Building Price</b></label>
			</td>
			<td align="left">
			<input type="text" class="form-control" onchange="" onkeydown="return numbersonly(this, event);" id="harga_bangunan" name="harga_bangunan">
			</td>
		</tr> 
		<tr>
			<td>&nbsp;</td>
		</tr>-->
		
	 </table>
	 
	 
		
<div class="clearfix"></div>
<br>
		
		
		
	   </div>
     </div>
   </div>
  
 </div>
 </form>
</span>


 <span class="data_artikel">
 <?php if($this->session->userdata("level")!="agen"){ ?>
<button class="btn-primary pull-right" onclick="add()"> <i class="fa fa-plus-circle"></i> Add</button>
<?php } ?>
<div class="row">
    <div class="col-lg-12">
        <div class="main-box clearfix" >
			<div class="main-box-body clearfix ">
			<div class="table-responsive">
			 <?php if($this->session->userdata("level")!="agen"){ ?>
			<span style='position:absolute;margin-top:48px;z-index:222' class="cursor btnhapus">
			<a href="#" onclick="hapusAll()"><i class='fa fa-trash'></i> Delete Selected</a>
			</span>
			<?php } ?>
			<form action="#" name="delcheck" id="delcheck" class="form-horizontal" method="post">
			<table id='tabley' class="tabel black table-striped table-bordered table-hover dataTable" width="100%">
						<thead style="font-size:13px">	
							<?php if($this->session->userdata("level")!="agen"){ ?>
							<th class='thead' axis="date" width='5px'><input type="checkbox" id="checkbox-1" class="pilihsemua" value="ya" /></th>
							<?php } ?>
						<!--	<th class='thead' axis="string" width='15px'>No</th> -->
							<th class='thead' axis="date" width='10px'>NO</th>
							<th class='thead' >LOCATION</th>
							<th class='thead' >LAND PRICE</th>
							<!--<th class='thead' >BUILDING PRICE</th>-->
							<?php if($this->session->userdata("level")!="agen"){ ?>
							<th width='130px'>&nbsp;</th>
							<?php } ?>
						</thead>
			</table></form>
		</div>
	   </div>
     </div>
   </div>
 </div>
</span>

<?php echo $this->load->view("js/tabel.phtml");?>
<script src="<?php echo base_url() ?>plug/boostrap/js/select2.min.js"></script>
<script>
  		var table;
		table = $('#tabley').DataTable({ 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('data_property/ajax_cma/')?>",
            "type": "POST",
        },
		
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
		    "targets": [ 0,1,2], //last column
          "orderable": false, //set not orderable
        },
        ],

      });
</script>

  <script>
	  
  $(".create").hide();
  function hapusAll()
	{	
		var con=window.confirm("hapus data terpilih ?");
		if(con==false){ return false; };
		$.ajax({
		url:"<?php echo base_url();?>data_property/HapusAllCMA",
		type: "POST",
		data: $('#delcheck').serialize(),
	//	dataType: "JSON",
		success: function(data)
				{	 $(".btnhapus").hide();
					$(".pilihsemua").removeAttr("checked");
					$(".pilihsemua").val("ya");
					table.ajax.reload(null,false); //reload datatable ajax 
				},
				error: function (jqXHR, textStatus, errorThrown)
				{
					alert('Try Again!');
				}
		});
	
	
	}
	
	    $("#provinsi").change(function () {
            var prov = $("#provinsi").val();
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "<?php echo base_url() ?>data_property/ajaxGetKab",
                data: "prov=" + prov,
                success: function (data) {
                    $(".dataKab").html(data);
                }
            });

        });
		loadkab();
		
	function loadkab(){
            var prov = "32";
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "<?php echo base_url() ?>data_property/ajaxGetKab",
                data: "def=3273&prov=" + prov,
                success: function (data) {
                    $(".dataKab").html(data);
                }
            });

        };
  
  
  $(".btnhapus").hide();
  	$(".pilihsemua").click(function(){
	
		if($(".pilihsemua").val()=="ya") {
		$(".pilih").prop("checked", "checked");
		$(".pilihsemua").val("no");
		  $(".btnhapus").show();
		} else {
		$(".pilih").removeAttr("checked");
		$(".pilihsemua").val("ya");
		  $(".btnhapus").hide();
		}
	
	});
	
	function pilcek(){
		$(".btnhapus").show();
		$(".pilihsemua").removeAttr("checked");
		$(".pilihsemua").val("ya");
		 
	};

	  function hapus_cma(id)
	  {
	    var tanya=window.confirm("Hapus ?");
		if(tanya==false){ return false;};
	  	$.ajax({
		url:"<?php echo base_url();?>data_property/hapus_cma",
		type: "POST",
		data:"id="+id,
		success: function(data)
				{
				table.ajax.reload(null,false); //reload datatable ajax 
				},
			});
	  }
	  
	  var method;
	  function add()
	  {
				$('.msg').html("");
				$('#form')[0].reset();
				$(".data_artikel").hide();
				$(".create").show();
	  } function cancel()
	  {
				$(".data_artikel").show();
				$(".create").hide();
				return false;
	  }
	      //nice select boxes
    $('#provinsi').select2();
    $('#kabupaten').select2();
  </script>
  
  
  
  <?php echo $this->load->view("js/form.phtml");?>

 <script>
	 function simpan(id)
	  {
		var provinsi=$("[name='provinsi']").val();
		var kabupaten=$("[name='kabupaten']").val();
		var lat_areax=$("[name='lat_areax']").val();
		var long_areax=$("[name='long_areax']").val();
		var areax=$("[name='areax']").val();
		var harga_area=$("[name='harga_area']").val();
		//var harga_bangunan=$("[name='harga_bangunan']").val();
		$('.msg').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Proses Simpan ... ");
	  	$("#form").ajaxForm({
		url:"<?php echo base_url();?>data_property/simpan_cma/",
		type: "POST",
		data:$("#form").serialize(),
		success: function(data)
				{
				if(data==false){ alert("Gagal! Data Jalan sudah ada pada database"); $(".msg").html(""); $("[name='areax']").focus(); return false;}
				if(data=='kurang'){ $('.msg').html(""); $("[name='harga_area']").focus(); return false;}
				$('.msg').html("<i class='fa fa-check-circle'></i> Tersimpan ... ");
				table.ajax.reload(null,false); //reload datatable ajax 
				cancel();
				$('#form')[0].reset();
				},
			});
	  }
</script>	

  