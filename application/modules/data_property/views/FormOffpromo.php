<?php 
$this->db->where("kode_prop",$id);
$data=$this->db->get("data_property")->row();
$alamat_detail = $data->alamat_detail;
?>

<form action="javascript:saveAddPromooff()"  id="formPromoOff" class="form-horizontal black" method="post"  enctype="multipart/form-data"  >
<input type="hidden" name="kode_prop" value="<?php echo $id;?>">
<div class="form-group">
<label for="alamat_detail" class="b col-lg-3 control-label">Location</label>
<div class="col-lg-8">
<input type="text" class="form-control" id="alamat_detail"  name="alamat_detail" value="<?php echo $alamat_detail;?>" disabled="disabled">
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<label for="tgl_promo" class="b col-lg-3 control-label">Date</label>
<div class="col-lg-8">
<input type="text" class="form-control" id="tgl_promo"  name="tgl_promo" value="" required >
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<label for="nama" class="b col-lg-3 control-label">Image</label>
<div class="col-lg-8">
<input type="file" class="form-control" id="foto_promo" name="foto_promo"  >
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<label for="alamat_promo" class="b col-lg-3 control-label">Alamat </label>
<div class="col-lg-8">
<textarea name="alamat_promo" id="alamat_promo" class="form-control" required></textarea>
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<label for="jenis" class="b col-lg-3 control-label">Jenis</label>
<div class="col-lg-8">
<?php                                        
    $arrayT[""] = "==== Pilih Jenis Promosi ====";
    $arrayT["0"] = "SPANDUK";
    $arrayT["1"] = "KORAN";
	$arrayT["2"] = "MAXI BANNER";
	$arrayT["3"] = "PAPAN TANAH";
    $data = $arrayT;
    echo form_dropdown('jenis_promo', $data, '', '  id="jenis_promo"  class="form-control" required ');
?>
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<label for="jenis" class="b col-lg-3 control-label">Status</label>
<div class="col-lg-8">
<?php                                        
    $arrayS[""] = "==== Pilih Status Promosi ====";
	$arrayS["0"] = "Tidak Aktif";
    $arrayS["1"] = "Aktif";
    $data = $arrayS;
    echo form_dropdown('status', $data, '', '  id="status"  class="form-control" required ');
?>
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<div class="col-lg-offset-2 col-lg-9">
<span class='load'></span>
<button type="submit" class="btn btn-success pull-right" onclick="saveAddPromooff()"><i class='fa fa-save'></i> Save</button>
</div>
</div>
</form>


<?php echo $this->load->view("js/form.phtml"); ?>
<script>
function saveAddPromooff()
	{	
		var url="<?php echo base_url();?>data_property/AddPromoOff";
		$(".load").html("<img src='<?php echo base_url();?>plug/img/load.gif'> Please wait...");
		$("#formPromoOff").ajaxForm({
		url:url,
		type: "post",
		data: $('#formPromoOff').serialize(),
	//	dataType: "JSON",
		success: function(data)
				{
						  closemodal("modalOffpromo");
						  table.ajax.reload(null,false); //reload datatable ajax 
				},
				
		});
	}
</script>

<script src="<?php echo base_url();?>plug/boostrap/js/jquery.maskedinput.min.js"></script>  
<script>
$("#tgl_promo").mask("99/99/9999");
</script>



<script src="<?php echo base_url() ?>plug/boostrap/js/select2.min.js"></script>
    <script>
                                      $("document").ready(function () {
                                 //     alert();
                                      //nice select boxes
                                      $('#sel2').select2();
                                      $('#sel3').select2();
                                      $('#sel4').select2();
                                      
									  });
    </script>