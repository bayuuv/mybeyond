<!--<button class="btn-warning pull-right" onclick="exportData()">  <i class="fa fa-upload"></i> Export</button>-->

<div class="row">
    <div class="col-lg-12">

<div class="panel-group accordion" id="accordion">

<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
<i class="fa fa-filter"></i> Filter Pencarian
</a>
</h4>
</div>
<div id="collapseThree" class="panel-collapse collapse">
<div class="panel-body">

<form id="form" action="javascript:getListing()">
  <div class="form-group fg_prov col-md-3" > 
  <label class="control-label black b" ></label>
    <?php                                        
      $arrayT[""] = "==== Tahun ====";
	  $arrayT["1111"] = "Semua Tahun";
      $arrayT["2016"] = "2016";
	  $arrayT["2017"] = "2017";
      $arrayT["2018"] = "2018";
      $data = $arrayT;
      echo form_dropdown('tahun_sell', $data, '', '  id="tahun_sell"  class="form-control"');
    ?>                              								
  </div>	
	
	
	<div class='col-md-3'>
    <label for="agen"></label>						
      <?php
        $ref_agen = $this->reff->getAgen();
        $array_agen[""] = "==== Pilih Agen ====";
        foreach ($ref_agen as $val) {
        $array_agen[$val->kode_agen] = $val->nama;
        }
        $data = $array_agen;
        echo form_dropdown('agen', $data, '', 'onchange="return cek_agen()"   id="agen" class="select2-container" style="width:100%"');
      ?>
      <span class="help-block err_agen"></span>                   
  </div>

	<div class='col-md-3'>
    <label for="vendor"></label>						
      <?php
        $ref_vendor = $this->reff->getOwner2();
        $array_vendor[""] = "==== Pilih Vendor ====";
        foreach ($ref_vendor as $val) {
        $array_vendor[$val->id_owner] = $val->nama;
        }
        $data = $array_vendor;
        echo form_dropdown('vendor', $data, '', 'onchange="return cek_vendor()"  id="vendor" class="select2-container" style="width:100%"');
      ?>
      <span class="help-block err_vendor"></span>                   
  </div>		
  
  <div class='col-md-3'>
    <label for="buyer"></label>						
      <?php
        $ref_buyer = $this->reff->getBuyer();
        $array_buyer[""] = "==== Pilih Buyer ====";
        foreach ($ref_buyer as $val) {
        $array_buyer[$val->id_pelanggan] = $val->nama;
        }
        $data = $array_buyer;
        echo form_dropdown('buyer', $data, '', 'onchange="return cek_buyer()" id="buyer" class="select2-container"  style="width:100%"');
      ?>
      <span class="help-block err_buyer"></span> 

	<button class='btn-primary btn btn-block'><i class='fa fa-search'></i> Cari</button>		  
  </div>	
	
	</form>

</div>
</div>
</div>
</div>
 



	 
<div id="listing"></div>
 <?php echo $this->load->view("js/tabel.phtml");?>
 <script>
 getListing();
 function getListing()
	{	
	$("#listing").html("<img src='<?php echo base_url();?>plug/img/load.gif'> Please wait...");
		$.ajax({
		url:"<?php echo base_url();?>data_property/getSelling",
		type: "POST",
		data: $('#form').serialize(),
		success: function(data)
				{	
				$("#listing").html(data);
				},
		});	
	}

 </script>
 
 <!-- Bootstrap modal -->
  <div class="modal fade" id="modalDetail" role="dialog">
  <div class="modal-dialog modal-lg">
<div class="modal-content" >
      <div class="modal-body form">
<section class="content">
  <div class="row">
    <div class="col-lg-12 dataDetail">
		<!------------------------->
		<!------------------------->
    </div>
  </div>   <!-- /.row -->
</section><!-- /.content -->
  </div>
   </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
  



 <script>
  function saveAdd()
	{	
	if(method=="edit")
	{
		var url="<?php echo base_url();?>data_property/update";
	}else{
		var url="<?php echo base_url();?>data_property/insert";
	}
		$(".load").html("<img src='<?php echo base_url();?>plug/img/load.gif'> Please wait...");
		$.ajax({
		url:url,
		type: "POST",
		data: $('#formAgen').serialize(),
	//	dataType: "JSON",
		success: function(data)
				{
				if(data==false){ alert("Gagal! Agen sudah ada pada database"); $(".load").html(""); $("[name='group']").focus(); return false;}
				$(".load").html('<font color="green"><i class="fa fa-check-circle fa-fw fa-lg"></i> Berhasil di simpan</font>');
				table.ajax.reload(null,false);
				$("#formAgen")[0].reset();
				//$(".load").html("");
				if(method=="edit")
				{
					$("#modalAdd").modal("hide");
					$(".load").html('');
				}
				},
				
		});
	}
	
	 function hapus(id)
	  {
	    var tanya=window.confirm("Delete ?");
		if(tanya==false){ return false;};
	  	$.ajax({
		url:"<?php echo base_url();?>data_property/hapus/"+id,
		type: "POST",
		data:"id="+id,
		success: function(data)
				{
				table.ajax.reload(null,false); //reload datatable ajax 
				},
			});
	  }

</script>	



<script>
function exportData()
{
	window.location.href="<?php echo base_url()?>data_property/export_selling";
}

</script>






 <!-- Bootstrap modal -->
  <div class="modal fade" id="modalImport" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
     
      <div class="modal-body form">
<section class="content">
  <div class="row">
    <div class="col-lg-12">
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-download"></i> Import Data  <span class='namaGroup'></span></h4>
      </div>
      <!-- general form elements -->
      <div class="box box-primary black">	   <br>
        Silahkan <a href='<?php echo base_url();?>data_property/downloadFormat'><i class="fa fa-file-excel-o"></i> download format</a> sebelum upload.
          <div class="box-body">                      
            <form role="form" name="uploadfilexl" id="uploadfilexl" action="javascript:void();" method="post" enctype="multipart/form-data">
                <div class="form-group">
                  <input id="userfile"  name="userfile" type="file" class="form-control">
                  <input  name="idGroup" type="hidden">
                </div>				
                <button type="submit" onclick="javascript:simpanfile();" class="btn btn-primary pull-right">
                    <span class="fa fa-upload"></span>&nbsp;Upload
                </button>
                <div class="form-group">
                    <div class="msg"></div>
                    <div class="hasil"></div>
                </div>
            </form>
          </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div>   <!-- /.row -->
</section><!-- /.content -->
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
  
   <?php echo $this->load->view("js/form.phtml"); ?>
  <script type="text/javascript">
function simpanfile(){
    var userfile=$('#userfile').val();
    $('#uploadfilexl').ajaxForm({
     url:'<?php echo base_url();?>data_property/importData/',
     type: 'post',
     data:{"userfile":userfile},
     beforeSend: function() {
        var percentVal = 'Mengupload 0%';
        $('.msg').html(percentVal);
     },
     uploadProgress: function(event, position, total, percentComplete) {
        var percentVal = 'Mengupload ' + percentComplete + '%';
        $('.msg').html(percentVal);
     },
     beforeSubmit: function() {
      $('.hasil').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Silahkan Tunggu ... ");
     },
     complete: function(xhr) {
        $('.msg').html('');
     }, 
     success: function(resp) {
        $('.hasil').html(resp);
		table.ajax.reload(null,false);
		$("#uploadfilexl")[0].reset();
     },
    });     
};
</script>   



<script src="<?php echo base_url() ?>plug/boostrap/js/select2.min.js"></script>
 <script>
                                      $('#agen').select2();
                                      $('#vendor').select2();
                                      $('#buyer').select2();
    </script>
 <script>
        $("#provinsi").change(function () {
            var prov = $("#provinsi").val();
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "<?php echo base_url() ?>data_property/ajaxGetKab2",
                data: "prov=" + prov,
                success: function (data) {
                    $(".dataKab").html(data);
                }
            });

        });

	function loadkab(){
            var prov = "32";
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "<?php echo base_url() ?>data_property/ajaxGetKab",
                data: "def=3273&prov=" + prov,
                success: function (data) {
                    $(".dataKab").html(data);
                }
            });

        };
    </script>
	 <script>
        $('input.typeahead').typeahead({
            source: function (query, process) {
                var kab = $("#kabupaten").val();
                return $.get('<?php echo base_url() ?>data_property/getKomplek/' + kab, {query: query}, function (data) {
                    console.log(data);
                    data = $.parseJSON(data);
                    return process(data);
                });
            }
        });
    </script>
	
	<script>

	var f=jQuery.noConflict();
function detail(id)
{
			f("#modalDetail").modal("show");
	  f('.dataDetail').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Mohon Tunggu ... ");
	            f.ajax({
                type: "POST",
                dataType: "html",
                url: "<?php echo base_url() ?>data_property/getDataDetail",
                data: "id=" + id,
                success: function (data) {
			
                    f(".dataDetail").html(data);
                }
            });
	
}
function importData()
{
	 f('.msg').html('');
	  f('.hasil').html('');
	f("#modalImport").modal("show");
}
function goSewa(id)
{
	f("#modalSewa").modal("show");
	  f('#kontenSewa').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Mohon Tunggu ... ");
	            f.ajax({
                type: "POST",
                dataType: "html",
                url: "<?php echo base_url() ?>data_property/getFormSewa/"+id,
                success: function (data) {
		              f("#kontenSewa").html(data);
                }
            });
}
function goJual(id)
{
	f("#modalJual").modal("show");
	  f('#kontenJual').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Mohon Tunggu ... ");
	            f.ajax({
                type: "POST",
                dataType: "html",
                url: "<?php echo base_url() ?>data_property/getFormJual/"+id,
                success: function (data) {
		              f("#kontenJual").html(data);
		              }
            });
}
	</script>
	
  
  
  
  
	
 <!-- Bootstrap modal -->
  <div class="modal fade" id="modalJual" role="dialog">
  <div class="modal-dialog">
 
<div class="modal-content">
     
      <div class="modal-body form">
   
<section class="content">
  <div class="row">
    <div class="col-lg-12">
	
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-plus-circle"></i><span class='title'> Sales Form</span></h4>
      </div>
	<br>
<div id="kontenJual">
</div>


    </div>
  </div>   <!-- /.row -->

</section><!-- /.content -->
  </div>
   </div><!-- /.modal-content -->
		

      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
  


	
	
	
	
 <!-- Bootstrap modal -->
  <div class="modal fade" id="modalSewa" role="dialog">
  <div class="modal-dialog">
 
<div class="modal-content">
     
      <div class="modal-body form">
   
<section class="content">
  <div class="row">
    <div class="col-lg-12">
	
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-plus-circle"></i><span class='title'> Transaction Form</span></h4>
      </div>
	<br>
<div id="kontenSewa">
</div>


    </div>
  </div>   <!-- /.row -->

</section><!-- /.content -->
  </div>
   </div><!-- /.modal-content -->
		

      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
  
  
  
  <script>
  
	var f=jQuery.noConflict();
	
  function editselling(id)
{  
	f("#modalSewa").modal("show");
	  f('#kontenSewa').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Mohon Tunggu ... ");
	            f.ajax({
                type: "POST",
                dataType: "html",
                url: "<?php echo base_url() ?>data_property/getListingEditSewa/"+id,
                success: function (data) {
		              f("#kontenSewa").html(data);
                }
            });
}
function editJulling(id)
{
	f("#modalJual").modal("show");
	  f('#kontenJual').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Mohon Tunggu ... ");
	            f.ajax({
                type: "POST",
                dataType: "html",
                url: "<?php echo base_url() ?>data_property/getListingEditJual/"+id,
                success: function (data) {
		              f("#kontenJual").html(data);
		              }
            });
}

function celeb(id)
{
	f("#modalCeleb").modal("show");
	  f('#kontenCeleb').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Mohon Tunggu ... ");
	            f.ajax({
                type: "POST",
                dataType: "html",
                url: "<?php echo base_url() ?>data_property/getCeleb/"+id,
                success: function (data) {
		              f("#kontenCeleb").html(data);
		              }
            });
}

function celebTV(id)
{
	f("#modalCelebTV").modal("show");
	  f('#kontenCelebTV').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Mohon Tunggu ... ");
	            f.ajax({
                type: "POST",
                dataType: "html",
                url: "<?php echo base_url() ?>data_property/getCelebTV/"+id,
                success: function (data) {
		              f("#kontenCelebTV").html(data);
		              }
            });
}

 function closemodal(data)
{
	f("#"+data).modal("hide");
}
	</script>
	
  
  
  
  
	
 <!-- Bootstrap modal -->
  <div class="modal fade" id="modalJual" role="dialog">
  <div class="modal-dialog">
 
<div class="modal-content">
     
      <div class="modal-body form">
   
<section class="content">
  <div class="row">
    <div class="col-lg-12">
	
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-plus-circle"></i><span class='title'> Sales Form</span></h4>
      </div>
	<br>
<div id="kontenJual">
</div>


    </div>
  </div>   <!-- /.row -->

</section><!-- /.content -->
  </div>
   </div><!-- /.modal-content -->
		

      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
  
   
   <!-- Bootstrap modal -->
  <div class="modal fade" id="modalCeleb" role="dialog">
  <div class="modal-dialog">
 
<div class="modal-content">
     
      <div class="modal-body form">
   
<section class="content">
  <div class="row">
    <div class="col-lg-12">
	
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-plus-circle"></i><span class='title'> Celebrate Form</span></h4>
      </div>
	<br>
<div id="kontenCeleb">
</div>


    </div>
  </div>   <!-- /.row -->

</section><!-- /.content -->
  </div>
   </div><!-- /.modal-content -->
		

      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
  
    <!-- Bootstrap modal -->
  <div class="modal fade" id="modalCelebTV" role="dialog">
  <div class="modal-dialog">
 
<div class="modal-content">
     
      <div class="modal-body form">
   
<section class="content">
  <div class="row">
    <div class="col-lg-12">
	
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-plus-circle"></i><span class='title'> Celebrate TV Form</span></h4>
      </div>
	<br>
<div id="kontenCelebTV">
</div>


    </div>
  </div>   <!-- /.row -->

</section><!-- /.content -->
  </div>
   </div><!-- /.modal-content -->
		

      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
  


	
	
	
	
 <!-- Bootstrap modal  
  <div class="modal fade" id="modalSewa" role="dialog">
  <div class="modal-dialog">
 
<div class="modal-content">
     
      <div class="modal-body form">
   
<section class="content">
  <div class="row">
    <div class="col-lg-12">
	
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-plus-circle"></i><span class='title'> Transaksi Sewa</span></h4>
      </div>
	<br>
<div id="kontenSewa">
</div>


    </div>
  </div>   <!-- /.row  
</section><!-- /.content - 
  </div>
   </div><!-- /.modal-content  
		

      </div><!-- /.modal-dialog  
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
  
  
  
  
  
 <div class="modal fade" id="modalDetailVendor" role="dialog" >
  <div class="modal-dialog modal-lg">

<div class="modal-content" >
<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-info-circle"></i> Information Detail</h4>
      </div>
	  <!----------------------------------------------->
	 <div id="kontenDetailVendor"></div>
	  <!----------------------------------------------->
</div>	  
</div>	  
</div>	    

 <div class="modal fade" id="modalDetailBuyer" role="dialog" >
  <div class="modal-dialog modal-lg">

<div class="modal-content" >
<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-info-circle"></i> Information Detail</h4>
      </div>
	  <!----------------------------------------------->
	 <div id="kontenDetailBuyer"></div>
	  <!----------------------------------------------->
</div>	  
</div>	  
</div>	  

  <script>
//var f=jQuery.noConflict();
	 function detailVendor(id)
	  {
	//	getAkun();
		  f("#modalDetailVendor").modal("show");
			 	f.ajax({
				url:"<?php echo base_url();?>data_owner/getDetail/"+id,
				success: function(data)
						{			
						f("#kontenDetailVendor").html(data);
				},
			});
		 
		
	  }
</script> 

<script>
	 function detailBuyer(id)
	  {
		  f("#modalDetailBuyer").modal("show");
			 	f.ajax({
					url:"<?php echo base_url();?>costumer/getDetail/"+id,
				success: function(data)
						{			
						f("#kontenDetailBuyer").html(data);
				},
			});
		 
		
	  }
</script>

