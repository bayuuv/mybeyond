<style>
#mapx {
    height: 500px;
	width : 100%;
}

</style>

<script>
    // This example adds a search box to a map, using the Google Place Autocomplete
    // feature. People can enter geographical searches. The search box will return a
    // pick list containing a mix of places and predicted search terms.

    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    function initAutocomplete() {
        var map = new google.maps.Map(document.getElementById('mapx'), {
            center: {lat: <?php echo $datax->lat_area;?>, lng: <?php echo $datax->log_area;?>},
            zoom: 10,
            mapTypeId: 'roadmap'
        });


        // Create the search box and link it to the UI element.
        var input = document.getElementById('areax');
        var searchBox = new google.maps.places.SearchBox(input);
        //   map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function () {
            searchBox.setBounds(map.getBounds());

        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function () {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }

            // Clear out the old markers.
            markers.forEach(function (marker) {
                marker.setMap(null);
            });
            markers = [];

            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function (place) {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                var icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                markers.push(new google.maps.Marker({
                    map: map,
                    icon: icon,
                    title: place.name,
					//draggable:true,
                    position: place.geometry.location
                }));

                document.getElementById('lat_areax').value = place.geometry.location.lat();
                document.getElementById('long_areax').value = place.geometry.location.lng();


                    var input = document.getElementById('alamat_detail');
           var autocomplete = new google.maps.places.Autocomplete(input);
           google.maps.event.addListener(autocomplete, 'place_changed', function () {
               var place = autocomplete.getPlace();
               // document.getElementById('city').value = place.name;
               document.getElementById('lat').value = place.geometry.location.lat();
               document.getElementById('long').value = place.geometry.location.lng();
               //alert("This function is working!");
               //alert(place.name);
               // alert(place.address_components[0].long_name);

           });


                /*
                 var bandung = new google.maps.LatLng(-6.917463899999999);
                 var cimahi = new google.maps.LatLng(-6.884082,107.541304);
                 var soreang = new google.maps.LatLng(-7.025202,107.525908);
                 var btscoverage = new google.maps.Polygon({
                 path: [bandung, cimahi, soreang],
                 strokeColor: "#0000F7",
                 strokeOpacity: 0.8,
                 strokeWeight: 2,
                 fillColor: "#0000F7",
                 fillOpacity: 0.3
                 });
                 btscoverage.setMap(map);
                 */

                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            map.fitBounds(bounds);

           
        });
    }




</script>
<script  src="https://maps.google.com/maps/api/js?libraries=places&key=AIzaSyChmzyUaK6RZopwMAMlE49VgRa9CQsEkFY&callback=initAutocomplete" defer type="text/javascript"></script>

<form action="javascript:simpan()" id="form">
<input type="hidden" name="id" value="<?php echo $datax->id_cma;?>"> 

<div class="row">
  
    <div class="col-lg-12">
        <div class="main-box clearfix" >
			<div class="main-box-body clearfix left">
			<button class="btn-primary pull-right" onclick="simpan()" > <i class="fa fa-plus-circle"></i> Save</button>
			<button  class="btn-danger pull-right" onclick="cancel()"> <i class="fa fa-plus-circle"></i> Cancel</button>
			<span class="msg pull-right" style="margin-top:10px;margin-right:20px"></span>
		<a href="#"><h4><i class="fa fa-thumb-tack"></i> Property :: CMA</h4></a>
		
		<hr>
		  
		<table width="100%"  >
		
		
		<tr>
			<td width="100px">
				<label for="inputEmail1" class=" black control-label"><b>PROVINCE</b></label>
			</td>
			<td align="left">
				<?php
                $ref_type = $this->reff->getProvinsi();
                $array_prov[""] = "==== choose ====";
                foreach ($ref_type as $val) {
                    $array_prov[$val->id] = '[' . $val->id . ']' . ' &nbsp;' . $val->provinsi;
                }
                $data = $array_prov;
                echo form_dropdown('provinsi', $data, $datax->id_prov, 'onchange="return cek_prov()"   id="provinsi" class="select2-container" style="width:100%"');
                ?>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		
		<tr>
			<td width="100px">
				<label for="inputEmail1" class=" black control-label"><b>City</b></label>
			</td>
			<td align="left">
				<span class="dataKab">
					<select id="kabupaten" name="kabupaten" class="select2-container" style="width:100%"></select>
                </span>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		
		<tr>
			<td width="100px">
				<label for="inputEmail1" class=" black control-label"><b>Location</b></label>
			</td>
			<td align="left">
				 <input type="text" name="areax" id="areax" value="<?php echo $datax->nama_area;?>" class="form-control">
                 <input type="hidden" name="lat_areax" id="lat_areax"  value="<?php echo $datax->lat_area;?>">
                 <input type="hidden" name="long_areax" id="long_areax" value="<?php echo $datax->log_area;?>">
			</td>
		</tr> 
		<tr>
			<td>&nbsp;</td>
		</tr>
		
		<tr>
			<td width="100px">
				<label for="inputEmail1" class=" black control-label"><b>MAP</b></label>
			</td>
			<td>
				 <div id="mapx"></div>
			</td>
		</tr> 
		<tr>
			<td>&nbsp;</td>
		</tr>
		
		<tr>
			<td width="100px">
				<label for="inputEmail1" class=" black control-label"><b>Land Price</b></label>
			</td>
			<td align="left">
			<input type="text" class="form-control" onchange="" onkeydown="return numbersonly(this, event);" id="harga_area" name="harga_area" value="<?php echo $datax->harga_area;?>" /> 
			</td>
		</tr> 
		<tr>
			<td>&nbsp;</td>
		</tr>
		<!--
		<tr>
			<td width="100px">
				<label for="inputEmail1" class=" black control-label"><b>Building Price</b></label>
			</td>
			<td align="left">
			<input type="text" class="form-control" onchange="" onkeydown="return numbersonly(this, event);" id="harga_bangunan" name="harga_bangunan" value="<?php echo $datax->harga_bangunan;?>" /> 
			</td>
		</tr> 
		<tr>
			<td>&nbsp;</td>
		</tr>-->
		
	 </table>
	 
	 
		
<div class="clearfix"></div>
<br>
		
		
		
	   </div>
     </div>
   </div>
  
 </div>
 </form> 
 <script src="<?php echo base_url() ?>plug/boostrap/js/select2.min.js"></script>


  <script>
	
	    $("#provinsi").change(function () {
            var prov = $("#provinsi").val();
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "<?php echo base_url() ?>data_property/ajaxGetKab",
                data: "prov=" + prov,
                success: function (data) {
                    $(".dataKab").html(data);
                }
            });

        });
		loadkab();
		
	function loadkab(){
            var prov = "<?php echo $datax->id_prov;?>";
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "<?php echo base_url() ?>data_property/ajaxGetKab",
                data: "def=<?php echo $datax->id_kab;?>&prov=" + prov,
                success: function (data) {
                    $(".dataKab").html(data);
                }
            });

        };
	
	//nice select boxes
    $('#provinsi').select2();
    $('#kabupaten').select2();
  </script>
  
  
  
  <?php echo $this->load->view("js/form.phtml");?>

 <script>
	 function simpan(id)
	  {
		var provinsi=$("[name='provinsi']").val();
		var kabupaten=$("[name='kabupaten']").val();
		var lat_areax=$("[name='lat_areax']").val();
		var long_areax=$("[name='long_areax']").val();
		var areax=$("[name='areax']").val();
		var harga_area=$("[name='harga_area']").val();
		var harga_bangunan=$("[name='harga_bangunan']").val();
		$('.msg').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Proses Simpan ... ");
	  	$("#form").ajaxForm({
		url:"<?php echo base_url();?>data_property/update_cma/",
		type: "POST",
		data:$("#form").serialize(),
		success: function(data)
				{
				if(data==false){ alert("Gagal! Data Jalan sudah ada pada database"); $(".msg").html(""); $("[name='areax']").focus(); return false;}
				if(data=='kurang'){ $('.msg').html(""); $("[name='harga_area']").focus(); return false;}
				$('.msg').html("<i class='fa fa-check-circle'></i> Tersimpan ... ");
				window.location.href="<?php echo base_url()?>data_property/analysis";
				},
			});
	  }
	  function cancel()
	  {
		window.location.href="<?php echo base_url()?>data_property/analysis";
	  }
</script>	
