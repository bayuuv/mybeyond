<?php
$this->db->where("id",$id);
$db=$this->db->get("data_selling")->row();
?>
  
 
<form action="javascript:saveAddCeleb()"  id="formCeleb" class="form-horizontal black" method="post"  enctype="multipart/form-data"  >
<input type="hidden" name="kode_listing" value="<?php echo $db->kode_listing;?>">
<input type="hidden" name="kode_agen" value="<?php echo $db->kode_agen;?>">
<input type="hidden" name="selling" value="<?php echo $db->selling;?>">
<input type="hidden" name="type_selling" value="<?php echo $db->type_selling;?>">
<input type="hidden" name="sumber_selling" value="<?php echo $db->sumber_selling;?>">
<input type="hidden" name="sumber_listing" value="<?php echo $db->sumber_listing;?>">
<input type="hidden" name="id" value="<?php echo $id;?>">
<div class="form-group">
<label for="tgl_closing" class="b col-lg-3 control-label">Date of Closing</label>
<div class="col-lg-8">
<input type="text" class="form-control" id="tgl_closing"  name="tgl_closing" value="<?php echo $this->tanggal->ind($db->tgl_closing,"/");?>" >
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<?php
$sumber_selling=$db->sumber_selling;
$sumber_listing=$db->sumber_listing;
if($sumber_selling == '2' or $sumber_listing =='2'){
?>
<label for="uploadMarketing" class="b col-lg-3 control-label">Foto Marketing</label>
<div class="col-lg-8">
<input  type="file" class="form-control" id="uploadMarketing" name="uploadMarketing" onchange="validasiFile()" required  />
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<label for="uploadLogo" class="b col-lg-3 control-label">Foto Logo</label>
<div class="col-lg-8">
<input  type="file" class="form-control" id="uploadLogo" name="uploadLogo" onchange="validasiFile2()" />
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<?php } ?>

<label for="uploadSupported" class="b col-lg-3 control-label">Supported By</label>
<div class="col-lg-8">
<?php
        $ref_bank = $this->reff->getBanklogo();
        $array_bank[""] = "==== Pilih Bank ====";
        foreach ($ref_bank as $val) {
        $array_bank[$val->pic] = $val->nama;
        }
        $data = $array_bank;
		echo form_dropdown('banklogo', $data, '', '  id="banklogo"  class="form-control"');
?>
</div>

<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>

<div class="col-lg-offset-2 col-lg-9">
<span class='load'></span>
<button type="submit" class="btn btn-success pull-right" onclick="saveAddCeleb()" ><i class='fa fa-save'></i> Save</button>
</div>
</div>


</form>

   
<?php echo $this->load->view("js/form.phtml"); ?>
<script>
 
function saveAddCeleb()
	{	
		var url="<?php echo base_url();?>data_property/AddCeleb";
		$(".load").html("<img src='<?php echo base_url();?>plug/img/load.gif'> Please wait...");
		$("#formCeleb").ajaxForm({
			url:url,
			type: "post",
			data: $('#formCeleb').serialize(),
			//	dataType: "JSON",
			success: function(data)
				{
					closemodal("modalCeleb");
					table.ajax.reload(null,false); //reload datatable ajax 
				},
						
			});
	}
	
function validasiFile(){
	var inputFile = document.getElementById('uploadMarketing');
	var pathFile = inputFile.value;
	var ekstensiOk = /(\.png)$/i;
	if(!ekstensiOk.exec(pathFile)){
		alert('Silakan upload file yang memiliki ekstensi .png');
		inputFile.value = '';
		//return false;
	}
}

function validasiFile2(){
	var inputFile = document.getElementById('uploadLogo');
	var pathFile = inputFile.value;
	var ekstensiOk = /(\.png)$/i;
	if(!ekstensiOk.exec(pathFile)){
		alert('Silakan upload file yang memiliki ekstensi .png');
		inputFile.value = '';
		//return false;
	}
}
</script>
 
<script src="<?php echo base_url();?>plug/boostrap/js/jquery.maskedinput.min.js"></script>  
<script>
$("#tgl_closing").mask("99/99/9999");
</script>


 