<div class="main-box-body clearfix ">
<div class="table-responsive">
<span style='position:absolute;margin-top:48px;z-index:222' class="cursor btnhapus">
<a href="#" onclick="hapusAll()"><i class='fa fa-trash'></i> Hapus Terpilih</a>
</span>
<form action="#" name="delcheck" id="delcheck" class="form-horizontal" method="post">
<table id='table' class="tabel black table-striped table-bordered table-hover dataTable" width="100%">
		  	<thead>			
			<!--	<th class='thead' axis="string" width='15px'>No</th> --->
				<th class='thead' axis="date" width='5px'><input type="checkbox" id="checkbox-1" class="pilihsemua" value="ya" /></th>
				<th class='thead' >Nama</th>	
				<th class='thead' >Nomor Hp</th>	
				<th class='thead' width="120">Sebutan</th>	
				<th class='thead' width="120">Tanggal Lahir</th>	
			
				<th width='150px'>&nbsp;</th>
			</thead>
</table></form>
</div>
</div>

<link href="<?php echo base_url();?>/plug/datatables/css/dataTables.bootstrap.css" rel="stylesheet">
  <script src="<?php echo base_url()?>plug/datatables/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url()?>plug/datatables/js/dataTables.bootstrap.js"></script>	
  
<script>
  function hapusAll()
	{	
		var con=window.confirm("hapus data terpilih ?");
		if(con==false){ return false; };
		$.ajax({
		url:"<?php echo base_url();?>plugin/hapusAllUltah",
		type: "POST",
		data: $('#delcheck').serialize(),
	//	dataType: "JSON",
		success: function(data)
				{	 $(".btnhapus").hide();
					$(".pilihsemua").removeAttr("checked");
					$(".pilihsemua").val("ya");
					table.ajax.reload(null,false); //reload datatable ajax 
				},
				error: function (jqXHR, textStatus, errorThrown)
				{
					alert('Try Again!');
				}
		});
	
	
	}
  
  
  $(".btnhapus").hide();
  	$(".pilihsemua").click(function(){
	
		if($(".pilihsemua").val()=="ya") {
		$(".pilih").prop("checked", "checked");
		$(".pilihsemua").val("no");
		  $(".btnhapus").show();
		} else {
		$(".pilih").removeAttr("checked");
		$(".pilihsemua").val("ya");
		  $(".btnhapus").hide();
		}
	
	});
	
	function pilcek(){
		$(".btnhapus").show();
		$(".pilihsemua").removeAttr("checked");
		$(".pilihsemua").val("ya");
		 
	};
  
  
  
		var table; var group=$("[name='group']").val();
		table = $('#table').DataTable({ 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('plugin/ajakUltah')?>",
            "type": "POST",
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
           "targets": [ 0,-1], //last column
          "orderable": false, //set not orderable
        },
        ],

      });

	  function hapus(id)
	  {
	    var tanya=window.confirm("Hapus ?");
		if(tanya==false){ return false;};
	  	$.ajax({
		url:"<?php echo base_url();?>plugin/hapusUltah/"+id,
		type: "POST",
		data:"id="+id,
		success: function(data)
				{
				table.ajax.reload(null,false); //reload datatable ajax 
				},
			});
	  }
	  
	  
  </script>
  