<div class="row">
<span id="vkonten">

<a href="#" onclick="tagihan()" class="col-lg-3 col-sm-6 col-xs-12">
<div class="main-box infographic-box colored purple-bg">
<i class="fa fa-envelope"></i>
<span class="headline">File to SMS </span>
<span class="headline">&nbsp;</span>
</div>
<div class="col-lg-12 col-sm-12 col-xs-12">
<h3>Fungsi File To SMS</h3>
<p class="black" style='text-align:justify'>
Fungsi ini digunakan untuk mengirim sms dari File Ms.Excell yang di import
</p>
</div>
</a>

<a href="#" onclick="email()" class="col-lg-3 col-sm-6 col-xs-12">
<div class="main-box infographic-box colored red-bg">
<i class="fa fa-envelope"></i>
<span class="headline">File to Email </span>
<span class="headline">&nbsp;</span>
</div>
<div class="col-lg-12 col-sm-12 col-xs-12">
<h3>Fungsi File To Email</h3>
<p class="black" style='text-align:justify'>
Fungsi ini digunakan untuk mengirim e-mail dari File Ms.Excell yang di import
</p>
</div>
</a>


<a href="<?php echo base_url();?>plugin/ultah" class="col-lg-3 col-sm-6 col-xs-12">
<div class="main-box infographic-box colored emerald-bg">
<i class="fa fa-gift"></i>
<span class="headline">SMS Ulang Tahun</span>
<span class="headline"><?php echo $dataUltah; ?></span>
</div>

<div class="col-lg-12 col-sm-12 col-xs-12">
<h3>Fungsi SMS Ulang Tahun</h3>
<p class="black" style='text-align:justify'>
Mengirim SMS kepada data yang berulang tahun secara otomatis 
</p>
</div>

</a>











<a href="<?php echo base_url();?>autoresponder" class="col-lg-3 col-sm-6 col-xs-12">
<div class="main-box infographic-box colored blue-bg">
<i class="fa fa-refresh"></i>
<span class="headline">Autoresponder</span>
<span class="headline"> <?php echo $dataAuto; ?></span>
</div>

<div class="col-lg-12 col-sm-12 col-xs-12">
<h3>Fungsi Autoresponder</h3>
<p class="black" style='text-align:justify'>
Merespon SMS secara otomatis ketika sender mengirim sesuai Format yang telah ditentukan
</p>
</div>
</a>



<a href="<?php echo base_url();?>sms_terjadwal" class="col-lg-3 col-sm-6 col-xs-12">
<div class="main-box infographic-box colored red-bg">
<i class="fa fa-clock-o"></i>
<span class="headline">SMS Terjadwal</span>
<span class="headline"> <?php echo $dataTerjadwal; ?></span>
</div>
<div class="col-lg-12 col-sm-12 col-xs-12">
<h3>Fungsi SMS Terjadwal</h3>
<p class="black" style='text-align:justify'>
Mengirim SMS secara otomatis dan  berkala : Harian/Mingguan/Bulanan/Tahunan
</p>
</div>

</a>





</span>
</div>

















<script>
function tagihan()
{
	$("#modalSmsTagihan").modal("show");
}
</script>

<script>
function email()
{
	$("#modalEmail").modal("show");
}
</script>

 <script type="text/javascript">
function batasan(){
var inputMsg=formt.isi.value.length;
var nilai=(inputMsg/154)+1;
var hasil=Math.floor(nilai);
$(".jumlah").html(hasil);
$(".slide").html(inputMsg);
}


 $(document).ready(function() { 
	batasan()
	});
</script>	

  <!-- Bootstrap modal -->
  <div class="modal fade" id="modalSmsTagihan" role="dialog">
		<div class="modal-dialog"  >
               <div class="md-contents">
			 <div class="row">
<div class="col-lg-12">
<div class="main-box clearfix black">
<header class="main-box-header clearfix">
<h2>Silahkan Pilih</h2>
</header>
<div class="main-box-body clearfix">
<div class="tabs-wrapper">
<ul class="nav nav-tabs">
<li class="active"><a href="#tab-home" data-toggle="tab">Dengan Template SMS</a></li>
<li><a href="#tab-help" data-toggle="tab">Tanpa Template</a></li>

</ul>
<div class="tab-content">
<div class="tab-pane fade in active" id="tab-home">

<div class="modal-body">
	<form  action="javascript:simpanfile()" id="formt" class="form-horizontal " method="post" enctype="multipart/form-data">
<div class="col-md-12" style="background-color:white">	

<div class="form-group">
	<label for="poto" class="black col-lg-2 control-label">Konten SMS</label>
	 <div class="col-lg-9">
		<textarea class="form-control" onkeyup="batasan()" onchange="simpanSMS()" name="isi"><?php echo $smsTagihan; ?></textarea>
		<span class="help-block" style="right:7px;position:absolute;margin-top:-2px;color:brown"><span class="slide">0</span>/<span class="jumlah">1</span></span>

	 </div>
		</div>
		
		
<div class="form-group">
	<label for="inputPassword1" class="black col-lg-2 control-label"></label>
	 <div class="col-lg-9">
		<div class="black" style="margin-top:12px;font-size:12px">
		Sertakan variabel : {kolom1} , {kolom2} , dst.. di dalam konten SMS Tagihan dan
		variabel tersebut akan otomatis terisi dengan data yang tersusun di file Excell yang diupload ...
		</div>
		</div>
</div>		
		

			<?php  $modemDb=explode(",",$modem);	  $count=count($modemDb); if($count<=1){  $hide="display:none";}else{$hide="";}	?>
<div class="form-group" style='<?php echo $hide;?>;margin-left:-25px' ">
	<label for="inputNama" class="black col-lg-2 control-label">Modem</label>
	 <div class="col-lg-9">
			<?php
			$dbase="";
			foreach($modemDb as $op)
			{
			$dbase[$op]=$op;
			}
			$array=$dbase;
			echo form_dropdown("modem",$array,"","class='form-control pull-left' style='max-width:200px;margin-left:8px;".$hide."' ");
			?>		
	 </div>
</div>



<div class="form-group">
	<label for="poto" class="black col-lg-2 control-label">Upload File</label>
	 <div class="col-lg-9">
		<input type="file" class="form-control" name="userfile" id="userfile">
		<span class="help-block" style="right:7px;position:absolute;margin-top:-2px;color:brown">
		<a href='<?php echo base_url();?>format-1.xlsx' download>Download Format Pengisian</a>
		</span>

	 </div>
		</div>


 <div class="modal-footer">
            <button type="submit" id="btnSave" class="btn btn-primary pull-right" onclick="javascript:simpanfile()">Kirim</button>
			<span id="msg" style='padding-right:15px;margin-top:20px'></span>
            <button type="button" class="btn btn-danger pull-right" style="margin-right:10px" data-dismiss="modal">Tutup</button>
          </div>
		  
 <div class="form-group" style='padding-left:20px;padding-right:20px;margin-top:-10px'>
                    <span class="msg black"></span>
                    <span class="hasil black" style='margin-top:-50px'></span>
                </div>		  
		  
  </div>   
  

  
</form>
				</div>
				
			

</div>
<div class="tab-pane fade" id="tab-help">
<form  action="javascript:simpanfile2()" id="form2" class="form-horizontal " method="post" enctype="multipart/form-data">
<div>

	<?php  $modemDb=explode(",",$modem);	  $count=count($modemDb); if($count<=1){  $hide="display:none";}else{$hide="";}	?>
<div class="form-group" style='<?php echo $hide;?>;margin-left:-25px' ">
	<label for="inputNama" class="black col-lg-2 control-label">Modem</label>
	 <div class="col-lg-9">
			<?php
				echo form_dropdown("modem2",$array,"","class='form-control pull-left' style='max-width:200px;margin-left:8px;".$hide."' ");
			?>		
	 </div>
</div>



<div class="form-group">
	<label for="poto" class="black col-lg-2 control-label">Upload File</label>
	 <div class="col-lg-9">
		<input type="file" class="form-control" name="userfile2" id="userfile2">
		<span class="help-block" style="right:7px;position:absolute;margin-top:-2px;color:brown">
		<a href='<?php echo base_url();?>format-2.xlsx' download>Download Format Pengisian</a>
		</span>

	 </div>
		</div>


 <div class="modal-footer">
            <button type="submit" id="btnSave" class="btn btn-primary pull-right" onclick="javascript:simpanfile2()">Kirim</button>
			<span id="msg" style='padding-right:15px;margin-top:20px'></span>
            <button type="button" class="btn btn-danger pull-right" style="margin-right:10px" data-dismiss="modal">Tutup</button>
          </div>
		  
 <div class="form-group" style='padding-left:20px;padding-right:20px;margin-top:-10px'>
                    <span class="msg2 black"></span>
                    <span class="hasil2 black" style='margin-top:-50px'></span>
                </div>		  
		  
  </div>   
  </form>






</div>

</div>
</div>
</div>
</div>
</div>
</div>
		 
		 
		 
		 
    </div><!-- /.modal -->
    </div><!-- /.modal -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->












  <!-- Bootstrap modal -->
  <div class="modal fade" id="modalEmail" role="dialog">
		<div class="modal-dialog"  >
               <div class="md-contents">
			 <div class="row">
<div class="col-lg-12">
<div class="main-box clearfix black">
<header class="main-box-header clearfix">
<h2>Silahkan Pilih</h2>
</header>
<div class="main-box-body clearfix">
<div class="tabs-wrapper">
<ul class="nav nav-tabs">
<li class="active"><a href="#tab-home1" data-toggle="tab">Dengan Template Email</a></li>
<li><a href="#tab-help2" data-toggle="tab">Tanpa Template</a></li>

</ul>
<div class="tab-content">
<div class="tab-pane fade in active" id="tab-home1">

<div class="modal-body">
	<form  action="javascript:simpanfile()" id="formt" class="form-horizontal " method="post" enctype="multipart/form-data">
<div class="col-md-12" style="background-color:white">	

<div class="form-group">
	<label for="poto" class="black col-lg-2 control-label">Konten SMS</label>
	 <div class="col-lg-9">
		<textarea class="form-control" onkeyup="batasan()" onchange="simpanSMS()" name="isi"><?php echo $smsTagihan; ?></textarea>
		<span class="help-block" style="right:7px;position:absolute;margin-top:-2px;color:brown"><span class="slide">0</span>/<span class="jumlah">1</span></span>

	 </div>
		</div>
		
		
<div class="form-group">
	<label for="inputPassword1" class="black col-lg-2 control-label"></label>
	 <div class="col-lg-9">
		<div class="black" style="margin-top:12px;font-size:12px">
		Sertakan variabel : {kolom1} , {kolom2} , dst.. di dalam konten SMS Tagihan dan
		variabel tersebut akan otomatis terisi dengan data yang tersusun di file Excell yang diupload ...
		</div>
		</div>
</div>		
		




<div class="form-group">
	<label for="poto" class="black col-lg-2 control-label">Upload File</label>
	 <div class="col-lg-9">
		<input type="file" class="form-control" name="userfile" id="userfile">
		<span class="help-block" style="right:7px;position:absolute;margin-top:-2px;color:brown">
		<a href='<?php echo base_url();?>format-1.xlsx' download>Download Format Pengisian</a>
		</span>

	 </div>
		</div>


 <div class="modal-footer">
            <button type="submit" id="btnSave" class="btn btn-primary pull-right" onclick="javascript:simpanfile()">Kirim</button>
			<span id="msg" style='padding-right:15px;margin-top:20px'></span>
            <button type="button" class="btn btn-danger pull-right" style="margin-right:10px" data-dismiss="modal">Tutup</button>
          </div>
		  
 <div class="form-group" style='padding-left:20px;padding-right:20px;margin-top:-10px'>
                    <span class="msg black"></span>
                    <span class="hasil black" style='margin-top:-50px'></span>
                </div>		  
		  
  </div>   
  

  
</form>
				</div>
				
			

</div>
<div class="tab-pane fade" id="tab-help2">
<form  action="javascript:simpanfile2()" id="form2" class="form-horizontal " method="post" enctype="multipart/form-data">
<div>

	<?php  $modemDb=explode(",",$modem);	  $count=count($modemDb); if($count<=1){  $hide="display:none";}else{$hide="";}	?>
<div class="form-group" style='<?php echo $hide;?>;margin-left:-25px' ">
	<label for="inputNama" class="black col-lg-2 control-label">Modem</label>
	 <div class="col-lg-9">
			<?php
				echo form_dropdown("modem2",$array,"","class='form-control pull-left' style='max-width:200px;margin-left:8px;".$hide."' ");
			?>		
	 </div>
</div>



<div class="form-group">
	<label for="poto" class="black col-lg-2 control-label">Upload File</label>
	 <div class="col-lg-9">
		<input type="file" class="form-control" name="userfile2" id="userfile2">
		<span class="help-block" style="right:7px;position:absolute;margin-top:-2px;color:brown">
		<a href='<?php echo base_url();?>format-2.xlsx' download>Download Format Pengisian</a>
		</span>

	 </div>
		</div>


 <div class="modal-footer">
            <button type="submit" id="btnSave" class="btn btn-primary pull-right" onclick="javascript:simpanfile2()">Kirim</button>
			<span id="msg" style='padding-right:15px;margin-top:20px'></span>
            <button type="button" class="btn btn-danger pull-right" style="margin-right:10px" data-dismiss="modal">Tutup</button>
          </div>
		  
 <div class="form-group" style='padding-left:20px;padding-right:20px;margin-top:-10px'>
                    <span class="msg2 black"></span>
                    <span class="hasil2 black" style='margin-top:-50px'></span>
                </div>		  
		  
  </div>   
  </form>






</div>

</div>
</div>
</div>
</div>
</div>
</div>
		 
		 
		 
		 
    </div><!-- /.modal -->
    </div><!-- /.modal -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
		
		
		
<script>
  function simpanSMS()
	{	
	$.ajax({
		url:"<?php echo base_url();?>plugin/saveTagihan",
		type: "POST",
		data: $('#formt').serialize(),
		success: function(data)
				{
			
				}
		});
	}
</script>	






<?php echo $this->load->view("js/form.phtml"); ?>
  <script type="text/javascript">
function simpanfile(){
    var userfile=$('#userfile').val();
    var modem=$('[name="modem"]').val();
    $('#formt').ajaxForm({
     url:'<?php echo base_url();?>plugin/importTagihan/',
     type: 'post',
     data:"userfile="+userfile+"&modem="+modem,
     beforeSend: function() {
        var percentVal = 'Mengupload 0%';
        $('.msg').html(percentVal);
     },
     uploadProgress: function(event, position, total, percentComplete) {
        var percentVal = 'Mengupload ' + percentComplete + '%';
        $('.msg').html(percentVal);
     },
     beforeSubmit: function() {
      $('.hasil').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Silahkan Tunggu ... ");
     },
     complete: function(xhr) {
        $('.msg').html('');
     }, 
     success: function(resp) {
        $('.hasil').html(resp);
		dataTable();
		$("#formt")[0].reset();
     },
    });     
	
};
</script>    	  


 <script type="text/javascript">
function simpanfile2(){
    var userfile=$('#userfile2').val();
    var modem=$('[name="modem2"]').val();
    $('#form2').ajaxForm({
     url:'<?php echo base_url();?>plugin/importSMS/',
     type: 'post',
     data:"userfile="+userfile+"&modem="+modem,
     beforeSend: function() {
        var percentVal = 'Mengupload 0%';
        $('.msg2').html(percentVal);
     },
     uploadProgress: function(event, position, total, percentComplete) {
        var percentVal = 'Mengupload ' + percentComplete + '%';
        $('.msg2').html(percentVal);
     },
     beforeSubmit: function() {
      $('.hasil2').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Silahkan Tunggu ... ");
     },
     complete: function(xhr) {
        $('.msg2').html('');
     }, 
     success: function(resp) {
        $('.hasil2').html(resp);
		dataTable();
		$("#form2")[0].reset();
     },
    });     
	
};
</script>    	  




