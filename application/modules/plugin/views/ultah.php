<?php  $this->load->view("js/tanggal.phtml"); ?>  

 <script type="text/javascript">
function batasan(){
var inputMsg=formSet.isi.value.length;
var nilai=(inputMsg/154)+1;
var hasil=Math.floor(nilai);
$(".jumlah").html(hasil);
$(".slide").html(inputMsg);
}
</script>


<div class="row">
<div class="col-lg-12">

<div class="main-box clearfix ">

<header class="main-box-header clearfix black">

<div class="form-group col-md-4">
<div class="input-group col-md-12">
<button id="klik" class="btn btn-primary pull-left" type="button"  data-toggle="tooltip" data-placement="bottom">
<i class="fa fa-cog"></i> Pengaturan
</button>
</div>
</div>




<div class="btn-group pull-right">



<button class="btn btn-primary" type="button" onclick="download()" title="Export Ke Excell" data-toggle="tooltip" data-placement="bottom">
<i class="fa fa-file-excel-o"></i> Download
</button>

<button class="btn btn-primary" onclick="Import()" type="button" title="Import File" data-toggle="tooltip" data-placement="bottom">
<i class="fa fa-upload"></i> Import
</button>



<button class="btn btn-primary" type="button" onclick="add()" title="Tambah" data-toggle="tooltip" data-placement="bottom">
<i class="fa fa-plus"></i> Tambah 
</button>

</div>


<div class="col-lg-12 alert alert-danger" id="setting">
<div class="main-box">
<header class="main-box-header clearfix">
<h2>Pengaturan</h2>
</header>
<div class="main-box-body clearfix" >
<div class="row">

<form action="#" method="post" id="formSet">

<div class="col-lg-12">
<div class="row">
<div class="form-group col-md-12 black">
<label for="datepickerDate">Status</label>
<div class="input-group col-md-2">

<?php
if($statusUltah=="on")
{
$status="checked";	
}else{
	$status="";
}
?>

<div class="pull-left">
<div class="onoffswitch">
<input type="checkbox" name="status" class="onoffswitch-checkbox" id="myonoffswitch" <?php echo $status; ?>>
<label class="onoffswitch-label" for="myonoffswitch">
<div class="onoffswitch-inner"></div>
<div class="onoffswitch-switch"></div>
</label>
</div>
</div>






</div>

</div>
</div>
</div>

<div class="col-lg-12">
<div class="row">
<div class="form-group col-md-12 black">
<label for="datepickerDate">Waktu Pengiriman</label>
<div class="input-group col-md-2">
<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
<input type="text" name="jam" class="form-control" id="jam" value="<?php echo $jam; ?>">
</div>

</div>
</div>
</div>

<div class="col-lg-12 black">
<div class="row">
<div class="form-group col-md-12">
<label for="datepickerDate">Isi Konten SMS</label>
<div class="input-group">
<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
<textarea class="form-control" onkeyup="batasan()" name="isi"><?php echo $isi_sms; ?></textarea>
</div>
<span style="float:right"><span class="slide">0</span>/<span class="jumlah">1</span></span>

Variabel : {sebutan} ,{nama} , {usia}

</div>
</div>
</div>

<?php  $modemDb=explode(",",$modem);	  $count=count($modemDb); if($count<=1){  $hide="display:none";}else{$hide="";}	?>
				
<?php
$dbase="";
foreach($modemDb as $op)
{
$dbase[$op]=$op;
}
$array=$dbase;
echo form_dropdown("modem",$array,$modemUltah,"class='form-control pull-left' style='max-width:200px;margin-left:8px;".$hide."' ");
?>			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			

<button onclick="simpan()" class="btn btn-primary pull-right" type="button" title="Pengaturan" data-toggle="tooltip" data-placement="bottom">
<i class="fa fa-save"></i> simpan
</button>
<span class="load"></span> 
</form>



</div>
</div>
</div>
</div>



</header>
<hr>





<div id="dataview"></div>

</div>
</div>
</div>

  


  <script type="text/javascript">
  $("#setting").hide();
  var group=0;
  $(document).ready(function() { 
	dataTable(group);
	batasan()
	});
	

function escapeHtml(unsafe) {
  return unsafe
      .replace(/&/g, ":dan:")
      .replace(/</g, ":kurang:")
      .replace(/>/g, ":lebih:")
      .replace(/"/g, ":tikwa:")
      .replace(/'/g, ":tikji:")
	  .replace("/", "_");
}	
	
function dataTable(group)
{
/////
$.ajax({
		url:"<?php echo base_url();?>plugin/listDataUltah/",
		type:"POST",
		data:"group="+group,
		success: function(data)
				{
				$('#dataview').html(data);
				}
		});
////
}	
	
	function refresh()
	{
	group=$("[name='group']").val();
	dataTable(group);
	}

	
	function download()
	{
	var id=$("[name='group']").val();
	var cari=$(".cari").val();
	var cari=escapeHtml(cari);
	if(cari==""){ cari="_____";}
	window.location.href="<?php echo base_url();?>group/downloadKontak/"+id+"/"+cari;	
	}
	
	
  </script>		
  
  
<?php echo $this->load->view("js/autoComplit.phtml"); ?>  






<script>
var method;
function edit(id)
{
method="edit";
/////
$.ajax({
		url:"<?php echo base_url();?>plugin/editUltah/"+id,
		type:"POST",
		dataType:"JSON",
		success: function(data)
				{
					$('[name="nama"]').val(data.nama);
					$('[name="hp"]').val(data.hp);
					$('[name="sebutan"]').val(data.sebutan);
					$('[name="tanggal"]').val(data.tgl_lahir);
					$('[name="id_ultah"]').val(data.id_ultah);
				$("#modalAdd").modal("show");
				$(".title").html("Edit Data Ulang Tahun");
				}
		});
////
}

function add()
{
method="add";
				$("#modalAdd").modal("show");
					$("#form")[0].reset();
					$(".title").html("Tambah Data Ulang Tahun");
			
}
</script>




 <!-- Bootstrap modal -->
  <div class="modal fade" id="modalAdd" role="dialog">
  <div class="modal-dialog">

  
  
<div class="modal-content">
     
      <div class="modal-body form">
   
<section class="content">
  <div class="row">
    <div class="col-lg-12">
	
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-plus"></i>  <span class='title'>Tambah Data</span>  <span class='namaGroup'></span></h4>
      </div>
	<br>

<form action="javascript:saveAdd()" name="form" id="form" class="form-horizontal black" method="post" >
<div class="form-group">
<label for="nama" class="col-lg-2 control-label">Nama</label>
<div class="col-lg-9">
<input type="text" class="form-control" id="nama" name="nama" required="required" >
</div>
</div>
<div class="form-group">
<label for="nama" class="col-lg-2 control-label">Nomor Hp</label>
<div class="col-lg-9">
<input type="text" class="form-control" id="hp" name="hp" required="required" >
</div>
</div>

<div class="form-group">
<label for="sebutan" class="col-lg-2 control-label">Sebutan</label>
<div class="col-lg-9">
<input type="text" class="form-control"  name="sebutan" required="required">
<small>contoh : Bapak / Ibu</small>
</div>
</div>

<div class="form-group">
<label for="Tanggal" class="col-lg-2 control-label">Tanggal Lahir</label>
<div class="col-lg-9">
<input type="text" class="form-control" id="tanggal" name="tanggal" required="required">
<input type="hidden"  name="id_ultah">
</div>
</div>







<div class="form-group">
<div class="col-lg-offset-2 col-lg-9">
<span class='loads'></span>
<button type="submit" class="btn btn-success pull-right"><i class='fa fa-save'></i> Simpan</button>
</div>
</div>
</form>

	 
	 
       
    </div>
  </div>   <!-- /.row -->

</section><!-- /.content -->

          </div>
         
</div><!-- /.modal-content -->
		

		
		
		
 <script>
 
 
  function saveAdd()
	{	
		$(".loads").html("<img src='<?php echo base_url();?>plug/img/load.gif'> Please wait...");
		
		if(method=="add")
		{
		var	urls="<?php echo base_url();?>plugin/saveUltah";
		}else
		{
		var	urls="<?php echo base_url();?>plugin/updateUltah";
		}			
			
		
		$.ajax({
		url:urls,
		type: "POST",
		data: $('#form').serialize(),
		success: function(data)
				{
					$(".loads").html('<font color="green"><i class="fa fa-check-circle fa-fw fa-lg"></i> Berhasil di simpan</font>');
				dataTable(group);
				
				if(method=="save")
				{
				$("#form")[0].reset();
				}else{
				$("#modalAdd").modal("hide");	
				}
				
				
				
				
				
				
				},
				error: function (jqXHR, textStatus, errorThrown)
				{
					alert('Try Again!');
				}
		});
	}
</script>	 

<script>
  function simpan()
	{	
		$(".load").html("<img src='<?php echo base_url();?>plug/img/load.gif'> Please wait...");
		$.ajax({
		url:"<?php echo base_url();?>plugin/saveKonfig",
		type: "POST",
		data: $('#formSet').serialize(),
		success: function(data)
				{
					$(".load").html('<font color="green"><i class="fa fa-check-circle fa-fw fa-lg"></i> Berhasil di simpan</font>');
				},
				error: function (jqXHR, textStatus, errorThrown)
				{
					alert('Try Again!');
				}
		});
	}
</script>	
  
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
  
  
  
  
  
<script type="text/javascript">
$('#tanggal').daterangepicker({
    "singleDatePicker": true,
    "showDropdowns": true,
	locale: {
					format: 'YYYY/MM/DD'
				}
}, function(start, end, label) {
  console.log("New date range selected: ' + start.format('DD-MM-YYYY') + ' to ' + end.format('YYY-MM-DD') + ' (predefined range: ' + label + ')");
});

$("#klik").click(function(){
    $("#setting").toggle();
});

function Import()
{
	$("#modalImport").modal("show");
}

</script>

























 <!-- Bootstrap modal -->
  <div class="modal fade" id="modalImport" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
     
      <div class="modal-body form">
       
		

<section class="content">
  <div class="row">
    <div class="col-lg-12">
	
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-download"></i> Import Data <span class='namaGroup'></span></h4>
      </div>
	
      <!-- general form elements -->
      <div class="box box-primary black">	   <br>
        Silahkan <a href='<?php echo base_url();?>format-ultah.xlsx'><i class="fa fa-file-excel-o"></i> download format</a> sebelum upload.
          <div class="box-body">                      
            <form role="form" name="uploadfilexl" id="uploadfilexl" action="javascript:void();" method="post" enctype="multipart/form-data">
                <div class="form-group">
				
                  <input id="userfile" required name="userfile" type="file" class="form-control">
                  <input  name="idGroup" type="hidden">
                </div>
				   			
				
                <button type="submit" onclick="javascript:simpanfile();" class="btn btn-primary pull-right">
                    <span class="fa fa-upload"></span>&nbsp;Upload
                </button>
                <div class="form-group">
                    <div class="msg"></div>
                    <div class="hasil"></div>
                </div>
            </form>
          </div><!-- /.box-body -->
      </div><!-- /.box -->
	     
		  
              
       
    </div>
  </div>   <!-- /.row -->

</section><!-- /.content -->

          </div>
         
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->




<?php echo $this->load->view("js/form.phtml"); ?>
  <script type="text/javascript">
function simpanfile(){
    var userfile=$('#userfile').val();
    $('#uploadfilexl').ajaxForm({
     url:'<?php echo base_url();?>plugin/importUltah/',
     type: 'post',
     data:{"userfile":userfile},
     beforeSend: function() {
        var percentVal = 'Mengupload 0%';
        $('.msg').html(percentVal);
     },
     uploadProgress: function(event, position, total, percentComplete) {
        var percentVal = 'Mengupload ' + percentComplete + '%';
        $('.msg').html(percentVal);
     },
     beforeSubmit: function() {
      $('.hasil').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Silahkan Tunggu ... ");
     },
     complete: function(xhr) {
        $('.msg').html('');
     }, 
     success: function(resp) {
        $('.hasil').html(resp);
		dataTable();
		$("#uploadfilexl")[0].reset();
     },
    });     
	
};
</script>    	  

<script>
function download()
{
	window.location.href="<?php echo base_url()?>plugin/downloadUltah";
}
</script>






