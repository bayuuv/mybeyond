<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Plugin extends CI_Controller {

	

	function __construct()
	{
		parent::__construct();	
		$this->load->model('m_plugin','plugin');
		$this->load->model('m_ultah','ultah');
		$this->load->model('m_terjadwal','terjadwal');
		$this->load->model('m_autoresponder','auto');
		$this->load->model('m_sms','sms');
		$this->load->model('m_tagihan','tagihan');
		$this->m_konfig->validasi_global();
	}
	
	function _template($data)
	{
	$this->load->view('template/main',$data);	
	}
	function saveTagihan()
	{
		$ID=$this->session->userdata("id");
		$data=array(
		"sms_tagihan"=>$this->input->post("isi"),
		);
		$this->db->where("ID",$ID);
	echo	$this->db->update("pbk_groups",$data);
	}
	
	public function index()
	{
	$data['konten']="index";
	$data['modem']=$this->sms->modem();
	$data['dataUltah']=$this->ultah->dataUltah();
	$data['dataAuto']=$this->auto->dataAuto();
	$data['dataTerjadwal']=$this->terjadwal->dataTerjadwal();
	$data['smsTagihan']=$this->tagihan->smsTagihan();
	$this->_template($data);
	}
	public function ultah()
	{
	$data['konten']="ultah";
	$data['jam']=$this->ultah->jam();
	$data['isi_sms']=$this->ultah->isi_sms();
	$data['statusUltah']=$this->ultah->statusUltah();
	$data['modem']=$this->sms->modem();
	$data['modemUltah']=$this->ultah->modemUltah();
	$this->_template($data);
	}
	function saveKonfig()
	{
	$this->ultah->saveKonfigUltah();
	}
	function listDataUltah()
	{
		$this->load->view('listDataUltah');	
	}
	function ajakUltah()
	{
		$list = $this->ultah->get_dataultah();
		$datax = array();
		$no = $_POST['start'];
		foreach ($list as $data) {

			$row = array();
			
			$row[] =  '<input type="checkbox" class="pilih" onclick="pilcek()" name="hapus[]" value="'.$data->id_ultah.'">';
			$row[] =  $data->nama;
			$row[] =  $data->hp;
			$row[] =  $data->sebutan;
			$row[] =  $data->tgl_lahir;
			
					
			//add html for action
			$row[] = ' 
			<a href="'.base_url().'sms/send/'.$data->hp.'"><i class="fa fa-envelope"></i>  Sms </a> | 
			<a href="#" onclick="edit('.$data->id_ultah.')"><i class="fa fa-edit"></i>  Edit </a>  |
			<a href="#" onclick="javascript:hapus('.$data->id_ultah.')"><i class="fa fa-trash"></i>  Hapus </a> 
			';
			
			$datax[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->ultah->count_ultah("sentitems"),
						"recordsFiltered" => $this->ultah->count_ultah("sentitems"),
						"data" => $datax,
						);
		//output to json format
		echo json_encode($output);

	}
	function editUltah($id)
	{
			$this->db->where("id_ultah",$id);
	$data=$this->db->get("data_ultah")->row();
	echo json_encode($data);
	}
	function saveUltah()
	{
	echo $this->ultah->saveUltah();	
	}function updateUltah()
	{
	echo $this->ultah->updateUltah();	
	}
	function HapusAllUltah()
	{
	$IDG=$this->session->userdata("id");
	$hapus=$this->input->post("hapus");
		foreach($hapus as $id)
		{
		$this->db->where("id_ultah",$id);
		$this->db->where("GroupID",$IDG);
		$this->db->delete("data_ultah");
		}	echo true;
	}
	function hapusUltah($id)
	{
	$IDG=$this->session->userdata("id");
	$this->db->where("id_ultah",$id);
		$this->db->where("GroupID",$IDG);
	echo	$this->db->delete("data_ultah");
	}
	
	
	function importUltah()
	{
	$this->load->library("PHPExcel");
	 $data=$this->ultah->importUltah();
		$data=explode("-",$data);
               ?><br><br>
			  <p style="color:green"><b>Import  Selesai</b></p>
                <table class="tabel table-hover table-bordered" style="100%">
			       <?php 
				   if($data[0]){		   echo "<tr><td>Berhasil di simpan : ".$data[0]." kontak</td></tr>"; 			}?>
					<?php
					if($data[1]){	  	   echo "<tr><td>Diperbaharui : ".$data[1]." kontak</td></tr>";			} 
					if($data[2]){	  	   echo "<tr><td>Gagal di import : ".$data[2]." kontak</td></tr>";			} ?>
                </table>
                <?php
	}
	
	
	function importTagihan()
	{
	$this->load->library("PHPExcel");
	 $data=$this->tagihan->importTagihan();
		$data=explode("-",$data);
               ?>
                <table class="tabel table-hover table-bordered" style="100%">
			       <?php 
				   if($data[0]){		   echo "<tr style='background-color:#d8d6d5' ><td>Berhasil di Kirim : ".$data[0]." kontak</td></tr>"; 			}?>
					<?php
					if($data[1]){	  	   echo "<tr><td>Diperbaharui : ".$data[1]." kontak</td></tr>";			} 
					if($data[2]){	  	   echo "<tr><td>Gagal di import : ".$data[2]." kontak</td></tr>";			} ?>
                </table>
                <?php
	}
	function importSMS()
	{
	$this->load->library("PHPExcel");
	 $data=$this->tagihan->importSMS();
		$data=explode("-",$data);
               ?>
                <table class="tabel table-hover table-bordered" style="100%">
			       <?php 
				   if($data[0]){		   echo "<tr style='background-color:#d8d6d5' ><td>Berhasil di Kirim : ".$data[0]." kontak</td></tr>"; 			}?>
					<?php
					if($data[1]){	  	   echo "<tr><td>Diperbaharui : ".$data[1]." kontak</td></tr>";			} 
					if($data[2]){	  	   echo "<tr><td>Gagal di import : ".$data[2]." kontak</td></tr>";			} ?>
                </table>
                <?php
	}
	
	
	function downloadUltah()
	{
	$this->ultah->downloadUltah();
	}
	
	
	
	
	
}

