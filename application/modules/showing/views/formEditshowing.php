<?php
$this->db->where("id_showing",$id);
$db=$this->db->get("data_showing")->row();
?>
<form action="javascript:saveEditShowing()"  id="formEditx" class="form-horizontal black" method="post"  enctype="multipart/form-data"  >
<input type="hidden" name="id_showing" id="id_showing" value="<?php echo $id;?>">
<div class="form-group">
<label for="tgl_promo" class="b col-lg-3 control-label">Date</label>
<div class="col-lg-8">
<input type="text" class="form-control" id="tgl_showing"  name="tgl_showing" value="<?php echo $this->tanggal->ind($db->tgl_showing,"/");?>" >
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>

<label for="jenis" class="b col-lg-3 control-label">Agen</label>
<div class="col-lg-8">
<?php
    if($this->session->userdata("id")==64){
		$ref_agen = $this->reff->getAgenAjeng();
	}elseif($this->session->userdata("id")==151){
		$ref_agen = $this->reff->getAgenRhafa();
	}elseif($this->session->userdata("id")==133){
		$ref_agen = $this->reff->getAgenKiki();
	}elseif($this->session->userdata("id")==146){
		$ref_agen = $this->reff->getAgenYudi();
	}elseif($this->session->userdata("id")==152){
		$ref_agen = $this->reff->getAgenVivi();
	}elseif($this->session->userdata("id")==161){
		$ref_agen = $this->reff->getAgenYema();
	}elseif($this->session->userdata("id")==162){
		$ref_agen = $this->reff->getAgenFrans();
	}elseif($this->session->userdata("id")==165){
		$ref_agen = $this->reff->getAgenRena();
	}else{
		$ref_agen = $this->reff->getAgen();
    }
	$array_agen[""] = "==== choose ====";
    foreach ($ref_agen as $val) {
    $array_agen[$val->kode_agen] = $val->nama;
    }
    $data = $array_agen;
    echo form_dropdown('agen', $data, $db->agen, ' id="agenzz" class="select2-container" style="width:100%"');
?>
 <span class="help-block err_agen"></span>
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>

<label for="nama" class="b col-lg-3 control-label">Images</label>
 
<div class="col-lg-5">
<input type="file" class="form-control" id="foto_showing" name="foto_showing"  >
</div>
 
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>

<div class="col-lg-offset-2 col-lg-9">
<span class='load'></span>
<button type="submit" class="btn btn-success pull-right" ><i class='fa fa-save'></i> Save</button>
</div>
</div>
</form>
<!--
    </div>
  </div>   <!-- /.row -->
<!--
</section><!-- /.content -->
 <!-- </div>
   </div><!-- /.modal-content -->
		

 <!--     </div><!-- /.modal-dialog -->
 
<script>
function saveEditShowing()
	{	
		var url="<?php echo base_url();?>showing/update";
		$(".load").html("<img src='<?php echo base_url();?>plug/img/load.gif'> Please wait...");
		$.ajax({
		url:url,
		type: "post",
		data: $('#formEditx').serialize(),
	//	dataType: "JSON",
		success: function(data)
				{
					//alert(data);
					window.location.href="<?php echo base_url();?>showing";	
				},
				
		});
	}
</script>
<script src="<?php echo base_url();?>plug/boostrap/js/jquery.maskedinput.min.js"></script>  
<script>
$("#tgl_showing").mask("99/99/9999");
</script>
 <script>
  $('#agenzz').select2();
</script>