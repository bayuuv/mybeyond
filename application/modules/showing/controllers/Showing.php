<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Showing extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->m_konfig->validasi_session(array("user","promotion"));
		$this->load->model("M_showing","showing");
	}
	
	function _template($data)
	{
	$this->load->view('template/main',$data);	
	}
	
	public function index()
	{

	$data['konten']="index";
	$this->_template($data);
	}
		function getDetail($id)
	{
		$data["id"]=$id;
		$this->load->view("getDetail",$data);
	}
	function getShowing()
	{
	$agen=$this->input->post("agen");
	$tahun=$this->input->post("tahun");
	$bulan=$this->input->post("bulan");
	$kelengkapan=$this->input->post("kelengkapan");
	$filter['filter']="?agen=$agen&tahun=$tahun&bulan=$bulan&kelengkapan=$kelengkapan";
	echo	$this->load->view("getShowing",$filter);
	}
	function getAddShowing($id)
	{	$data["id"]=$id;
		$this->load->view("FormShowing",$data);
	}
	function ajax_showing()
	{
		$list = $this->showing->get_dataShowing();
        $data = array();
        $no = $_POST['start']+1;
        foreach ($list as $val) {
		
			$row = array();
			$row[] =  '<input type="checkbox" class="pilih" onclick="pilcek()" name="hapus[]" value="'.$val->id_showing.'">';
			//$row[] = $no++;
            $row[] = $this->tanggal->ind($val->tgl_showing,"/");
			$row[] = $val->nama;
            $row[] = '<img src="'.base_url().'file_upload/img/'.$val->foto_showing.'" width="200px" height="200px" >';
            $row[] = '
			<a href="#" style="font-size:14px" onclick="editshowing(`' . $val->id_showing .'`)" class=" "><i class="fa fa-edit "> </i> Edit </a>
			| <a href="#" style="font-size:14px" onclick="hapuss(`' . $val->id_showing . '`);" class=" "><i class="fa fa-trash"> </i> Hapus </a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->showing->counts(),
            "recordsFiltered" => $this->showing->counts(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	function insert()
	{
		echo $this->showing->insert();
	}
	function updatePromo()
	{
		echo $this->showing->updateShowing();
	}
	function HapusAll()
	{
	echo $this->showing->HapusAll();
	}
	function hapus($id)
	{
	echo $this->showing->hapus($id);
	}
	
	function export()
	{
		$this->showing->export();
	}
	function getEditshowing($id)
	{	
		$data["id"]=$id;
		$this->load->view("formEditshowing",$data);
	}
	
}