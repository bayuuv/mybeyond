<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>plug/boostrap/css/compiled/wizard.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>plug/typehead/typehead.css">
<style>
    #map {
        height: 300px;
    }

</style>
<style>
	.hilang{
		position:absolute;
		margin-top:-2000px;
	}
	</style>
<input type="hidden" name="count" value="1">
<script>
    // This example adds a search box to a map, using the Google Place Autocomplete
    // feature. People can enter geographical searches. The search box will return a
    // pick list containing a mix of places and predicted search terms.

    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    function initAutocomplete() {
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: -6.917463899999999, lng: 107.61912280000001},
            zoom: 13,
            mapTypeId: 'roadmap'
        });


        // Create the search box and link it to the UI element.
        var input = document.getElementById('area');
        var searchBox = new google.maps.places.SearchBox(input);
        //   map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function () {
            searchBox.setBounds(map.getBounds());

        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function () {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }

            // Clear out the old markers.
            markers.forEach(function (marker) {
                marker.setMap(null);
            });
            markers = [];

            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function (place) {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                var icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                markers.push(new google.maps.Marker({
                    map: map,
                    icon: icon,
                    title: place.name,
                    position: place.geometry.location
                }));

                document.getElementById('lat_area').value = place.geometry.location.lat();
                document.getElementById('long_area').value = place.geometry.location.lng();


                    var input = document.getElementById('alamat_detail');
           var autocomplete = new google.maps.places.Autocomplete(input);
           google.maps.event.addListener(autocomplete, 'place_changed', function () {
               var place = autocomplete.getPlace();
               // document.getElementById('city').value = place.name;
               document.getElementById('lat').value = place.geometry.location.lat();
               document.getElementById('long').value = place.geometry.location.lng();
               //alert("This function is working!");
               //alert(place.name);
               // alert(place.address_components[0].long_name);

           });


                /*
                 var bandung = new google.maps.LatLng(-6.917463899999999);
                 var cimahi = new google.maps.LatLng(-6.884082,107.541304);
                 var soreang = new google.maps.LatLng(-7.025202,107.525908);
                 var btscoverage = new google.maps.Polygon({
                 path: [bandung, cimahi, soreang],
                 strokeColor: "#0000F7",
                 strokeOpacity: 0.8,
                 strokeWeight: 2,
                 fillColor: "#0000F7",
                 fillOpacity: 0.3
                 });
                 btscoverage.setMap(map);
                 */

                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            map.fitBounds(bounds);

           
        });
    }




</script>
<script  src="https://maps.google.com/maps/api/js?libraries=places&key=AIzaSyA8V020aIxzsnq7PlhFS0a0z50wgIgW7rM&callback=initAutocomplete" defer type="text/javascript"></script>




<?php echo $this->load->view("js"); ?>

<div class="row">
    <div class="col-lg-12">
        <div class="main-box clearfix" >
            <br>
			<form id="form" action="javascript:simpanListing()" method="post">
            <!----------------------------------------------------------------------------------------------------------------------------->		
            <div class="main-box-body clearfix">
                <div id="myWizard" class="wizard">
                    <div class="wizard-inner">
                        <ul class="steps">
                            <li id="step1" class="active"><span class="badge  badge1 badge-primary">1</span>Step 1<span class="chevron"></span></li>
                            <li id="step2"><span class="badge badge2 ">2</span>Step 2<span class="chevron"></span></li>
                            <li id="step3"><span class="badge badge3">3</span>Step 3<span class="chevron"></span></li>
                            <li id="step4"><span class="badge badge4">4</span>Step 4<span class="chevron"></span></li>
							<span class="info" style="position:absolute;margin-left:20px;margin-top:7px"> <?php echo $this->session->flashdata("info");?>	</span>
                        </ul>
                        <div class="actions">
                            <button type="button" class="btn btn-default btn-mini btn-prev" onclick="return back()"> <i class="icon-arrow-left"></i>Prev</button>
                            <button type="button" class="lanjut btn btn-success btn-mini" onclick="return cek()"  >Next<i class="icon-arrow-right"></i></button>
                            <button class="save hilang  btn btn-danger btn-mini" onclick="simpanListing()">Save<i class="icon-arrow-right"></i></button>
							
                        </div>
                    </div>
                    <div class="step-content">
                        <!------------------------------------------------------------------------------------------------------------------------------>					
             
                            <br/>
                            <div class='step1'>
                                <div class="form-group fg_type_pro black">

                                    <div class="form-group black">
                                        <label class="control-label black b" >Pilih Tipe Properti</label>
                                        <?php
                                        $ref_type = $this->reff->getTypePro();
                                        $array[""] = "==== Pilih ====";
                                        foreach ($ref_type as $val) {
                                            $array[$val->id_type] = $val->nama;
                                        }
                                        $data = $array;
                                        echo form_dropdown('type_pro', $data, '', '  id="type_pro"  class="form-control" onchange="cek_type_pro()"');
                                        ?>
                                        <span class="help-block err_type_pro"></span>
                                    </div>
                                </div>

								<div class="form-group fg_pemilik black">

                                    <div class="form-group black">
                                        <label class="control-label black b" >Pemilik Property</label>
                                        <?php
                                        $ref_type = $this->reff->getOwner();
                                        $array_pemilik[""] = "==== Pilih ====";
                                        foreach ($ref_type as $val) {
                                            $array_pemilik[$val->id_owner] = $val->nama;
                                        }
                                        $data = $array_pemilik;
                                        echo form_dropdown('owner', $data, '', '  id="owner" style="width:100%"');
                                        ?>
                                        <span class="help-block err_pemilik"></span>
                                    </div>
                                </div>


                                <div class="form-group black fg_type_list black" >
                                    <label class="control-label black b">Pilih Type Listing</label>
                                    <br>
                                    <div class="btn-group" data-toggle="buttons" onchange="return cek_type_list()" >
                                        <label class="btn btn-default">
                                            <input type="radio" name="type_list" id="type_list" value='disewa' > DISEWA
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="radio" name="type_list" id="type_list" value='dijual'>DIJUAL
                                        </label>
                                        <span class="help-block err_type_list"></span>
                                    </div>
                                </div>

                                <div class="form-group black fg_desc" onchange="return cek_desc()">
                                    <label class="control-label black b" for="inputError">Deskripsi</label>
                                    <textarea onkeyup="cek_desc()" style="height:200px" name='desc' id="desc" class="form-control" placeholder="Tulis deskripsi property disini"></textarea>
                                    <span class="help-block err_desc"></span>
                                </div>

                            </div>							


                            <!------------------------------------------------------------------------------------------------------------------------------>							
                            <div class='step2 hilang'>
                                <div class='col-md-4'>
                                    <div class="form-group fg_prov col-md-12" > 
                                        <label class="control-label black b" >Pilih Provinsi</label>
                                        <?php
                                        $ref_type = $this->reff->getProvinsi();
                                        $array_prov[""] = "==== Pilih ====";
                                        foreach ($ref_type as $val) {
                                            $array_prov[$val->id] = '[' . $val->id . ']' . ' &nbsp;' . $val->provinsi;
                                        }
                                        $data = $array_prov;
                                        echo form_dropdown('provinsi', $data, 32, 'onchange="return cek_prov()"   id="provinsi" class="select2-container" style="width:100%"');
                                        ?>
                                        <span class="help-block err_prov"></span>
                                    </div>

                                    <div class="form-group fg_kab col-md-12"   style='margin-top:-20px'>
                                        <label class="control-label black b" >Pilih Kab/Kota</label>
                                        <span class="dataKab">
                                            <select id="kabupaten" name="kabupaten" class="select2-container" style="width:100%"></select>
                                        </span>
                                        <span class="help-block err_kab"></span>
                                    </div>

                                    <div class="form-group black fg_kom col-md-12"  style='margin-top:-20px'>
                                        <label class="control-label black b" for="inputError">Nama komplek</label>
                                        <input type="text"  name="nama_komplek"  autocomplete="off" onkeyup="return cek_komplek()" id="nama_komplek" class="form-control typeahead" style="width:100%" >
                                        <span class="help-block err_kom"></span>
                                    </div>	

                                    <div class="form-group black fg_area col-md-12"  style='margin-top:-14px'>
                                        <label class="control-label black b" for="inputError">Area</label>
                                        <input type="text" name="area" id="area" onkeyup="return cek_area()" placeholder=""  class="form-control col-md-12">
                                        <input type="hidden" name="lat_area" id="lat_area" class="form-control col-md-12">
                                        <input type="hidden" name="long_area" id="long_area" class="form-control col-md-12">
                                        <span class="help-block err_area"></span>
                                    </div>	

                                    <div class="form-group black fg_alamat col-md-12" style="margin-top:-10px">
                                        <label class="control-label black b" for="alamat_detail">Alamat Detail</label>
                                        <input type="text" name="alamat_detail" id="alamat_detail" onkeyup="return cek_alamat()" class="form-control col-md-12">
                                        <input type="hidden" name="lat" id="lat" class="form-control col-md-12">
                                        <input type="hidden" name="long" id="long" class="form-control col-md-12">
                                        <span class="help-block err_alamat"></span>
                                    </div>	

                                </div>							
                                <div class="col-md-5">
                                    <div id="map"></div>
                                </div>							




                            </div>
                      
                            <!--------------------------------------------------------------------------------------------------------------------------------->		
                            
                            <!------------------------------------------------------------------------------------------------------------------------------>							
                            <div class='step3 hilang'>
							
                                <div class='col-md-3'>
                                     <div class="form-group black fg_tanah col-md-12">
                                    	<label for="luas_tanah">Luas Tanah</label>
										<div class="input-group">
										<input type="number" class="form-control" id="luas_tanah" onchange="cek_tanah()" name="luas_tanah">
										<span class="input-group-addon">M<sup>2</sup></span>
										</div>  <span class="help-block err_tanah"></span>
                                    </div>	
                                </div>		
								<div class='col-md-3'>
                                     <div class="form-group black fg_bangunan col-md-12">
                                    	<label for="luas_bangunan">Luas Bangunan</label>
										<div class="input-group">
										<input type="number" class="form-control" id="luas_bangunan"  onchange="cek_bangunan()" name="luas_bangunan">
										<span class="input-group-addon">M<sup>2</sup></span>
										
										</div><span class="help-block err_bangunan"></span>
                                    </div>	
                                </div>		
								
								
									<div class='col-md-3'>
                                     <div class="form-group black fg_dibangun">
                                    	<label for="tahun_dibangun">Tahun dibangun</label>
										<div class="input-group" style="width:100%">
										<input type="number" class="form-control" id="tahun_dibangun" name="tahun_dibangun" >
									  
									</div>
                                    </div>	
                                </div>	
								
								
								
								<div class='col-md-3'>
                                     <div class="form-group black fg_harga col-md-12">
                                    	<label for="harga">Harga</label>
										<div class="input-group">
										<span class="input-group-addon">Rp</span>
										<input type="text" class="form-control" onchange="cek_harga()" onkeydown="return numbersonly(this, event);" id="harga" name="harga">
									  
									</div> <span class="help-block err_harga"></span>
                                    </div>	
                                </div>		
								
								<div class='col-md-3'>
                                     <div class="form-group black fg_tidur col-md-12">
                                    	<label for="kamar_tidur">Kamar Tidur</label>
										<div class="input-group" style="width:100%">
									
										<input type="number" class="form-control" id="kamar_tidur" name="kamar_tidur">
									   <span class="help-block err_tidur"></span>
									</div>
                                    </div>	
                                </div>		
								
								<div class='col-md-3'>
                                     <div class="form-group black fg_mandi col-md-12">
                                    	<label for="kamar_mandi">Kamar Mandi</label>
										<div class="input-group" style="width:100%">
									
										<input type="number" class="form-control" id="kamar_mandi" name="kamar_mandi">
									   <span class="help-block err_mandi"></span>
									</div>
                                    </div>	
                                </div>		
								
								<div class='col-md-3'>
                                     <div class="form-group black fg_tidur_p col-md-12">
                                    	<label for="kamar_tidur_pembantu">Kamar Tidur Pembantu</label>
										<div class="input-group" style="width:100%">
									
										<input type="number" class="form-control" id="kamar_tidur_pembantu" name="kamar_tidur_pembantu">
									   <span class="help-block err_tidur_p"></span>
									</div>
                                    </div>	
                                </div>		
								
								
								<div class='col-md-3'>
                                     <div class="form-group black fg_mandi_p col-md-12">
                                    	<label for="kamar_mandi_pembantu">Kamar Mandi Pembantu</label>
										<div class="input-group" style="width:100%">
										<input type="number" class="form-control" id="kamar_mandi_pembantu" name="kamar_mandi_pembantu">
									   <span class="help-block err_mandi_p"></span>
									</div>
                                    </div>	
                                </div>		

								<div class='col-md-3'>
                                     <div class="form-group black fg_lantai">
                                    	<label for="lantai">Jumlah Lantai</label>
										<div class="input-group" style="width:100%">
										
										<input type="number" class="form-control" id="jumlah_lantai" name="jumlah_lantai" >
									   <span class="help-block err_lantai"></span>
									</div>
                                    </div>	
                                </div>		
								
								<div class='col-md-3'>
                                     <div class="form-group black fg_garasi">
                                    	<label for="garasi">Jumlah Garasi</label>
										<div class="input-group" style="width:100%">
										<input type="number" class="form-control" id="garasi" name="garasi" >
									   <span class="help-block err_garasi"></span>
									</div>
                                    </div>	
                                </div>		
								

								<div class='col-md-3'>
                                     <div class="form-group black fg_carports">
                                    	<label for="carports">Jumlah Carports</label>
										<div class="input-group" style="width:100%">
										<input type="number" class="form-control" id="carports" name="carports" >
									   <span class="help-block err_carports"></span>
									</div>
                                    </div>	
                                </div>		
								
								<div class='col-md-3'>
                                     <div class="form-group black fg_listrik">
                                    	<label for="listrik">Daya Listrik</label>
										<div class="input-group" style="width:100%">
										<input type="number" class="form-control" id="daya_listrik" name="daya_listrik" >
									   <span class="help-block err_listrik"></span>
									</div>
                                    </div>	
                                </div>		
								
								
								<div class='col-md-3'>
                                     <div class="form-group black fg_hadap">
                                    	<label for="hadap">Hadap Bangunan</label>
										<div class="input-group" style="width:100%">
									 <?php
                                        $ref_type = $this->reff->getHadap();
                                        $array_ma[""] = "==== Pilih ====";
                                        foreach ($ref_type as $val) {
                                            $array_ma[$val->id_hadap] = $val->nama;
                                        }
                                        $data = $array_ma;
                                        echo form_dropdown('hadap', $data, '', 'onchange="return cek_hadap()"   id="hadap" class="select2-container" style="width:100%"');
                                        ?>
									   <span class="help-block err_hadap"></span>
									</div>
                                    </div>	
                                </div>		
									
								
								
								<div class='col-md-3'>
                                     <div class="form-group black fg_sewa">
                                    	<label for="sewa">Type Sewa</label>
										<div class="input-group" style="width:100%">
									 <?php
                                        $ref_sewa = $this->reff->getSewa();
                                        $array_sewa[""] = "==== Pilih ====";
                                        foreach ($ref_sewa as $val) {
                                            $array_sewa[$val->id_sewa] = $val->nama;
                                        }
                                        $data = $array_sewa;
                                        echo form_dropdown('type_sewa', $data, '', 'onchange="return cek_sewa()"   id="sewa" class="select2-container" style="width:100%"');
                                        ?>
									   <span class="help-block err_sewa"></span>
									</div>
                                    </div>	
                                </div>		
									
								
								
								
						
								
								<div class='col-md-3'>
                                     <div class="form-group black fg_sertifikat">
                                    	<label for="sertifikat">Jenis Sertifikat</label>
										<div class="input-group" style="width:100%">
									 <?php
                                        $ref_sert = $this->reff->getSertifikat();
                                        $array_sert[""] = "==== Pilih ====";
                                        foreach ($ref_sert as $val) {
                                            $array_sert[$val->id_sertifikat] = $val->nama;
                                        }
                                        $data = $array_sert;
                                        echo form_dropdown('sertifikat', $data, '', 'id="sertifikat" class="select2-container" style="width:100%"');
                                        ?>
									   <span class="help-block err_sertifikat"></span>
									</div>
                                    </div>	
                                </div>		
								
										
								
									
									
								<div class='col-md-3'>
                                     <div class="form-group black fg_agen">
                                    	<label for="agen">Pilih Agen</label>
										<div class="input-group" style="width:100%">
									 <?php
                                        $ref_agen = $this->reff->getAgen();
                                        $array_agen[""] = "==== Pilih ====";
                                        foreach ($ref_agen as $val) {
                                            $array_agen[$val->id_agen] = $val->nama;
                                        }
                                        $data = $array_agen;
                                        echo form_dropdown('agen', $data, '', 'onchange="return cek_agen()"   id="agen" class="select2-container" style="width:100%"');
                                        ?>
									   <span class="help-block err_agen"></span>
									</div>
                                    </div>	
                                </div>		
								
								<div class='col-md-12'>
								
							   <div class="form-group black fg_ket" >
                                    <label class="control-label black " for="inputError">Keterangan</label>
                                    <textarea style="height:200px" name='keterangan' id="keterangan" class="form-control" placeholder="Tulis keterangan tambahan disini"></textarea>
                                    <span class="help-block err_ket"></span>
                                </div>
                                </div>
								
                            </div>
                            <!--------------------------------------------------------------------------------------------------------------------------------->		
                           
<div class='step4 hilang'>
<!------------------------>


	<link rel="stylesheet" href="<?php echo base_url()?>plug/assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
	
	
	
	<!------------------------->
														<div class="col-sm-4">
											<div class="widget-box">
												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group">
															<div class="col-xs-12">
																<input  type="file" id="upload1" name="upload1" />
															</div>
														</div>
														<label  style="padding-left:10px;black">
															<input type="radio" checked value="gambar1" name="set" id="set"  />
															<span class="lbl black"> Set gambar utama</span>
														</label>
													
											
												</div>
											</div>
											</div>
										</div>
		<!------------------------->
		<!------------------------->
														<div class="col-sm-4">
											<div class="widget-box">
												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group">
															<div class="col-xs-12">
																<input  type="file" id="upload2" name="upload2" />
															</div>
														</div>
														<label  style="padding-left:10px;black">
															<input type="radio" value="gambar2" name="set" id="set"  />
															<span class="lbl black"> Set gambar utama</span>
														</label>
													
											
												</div>
											</div>
											</div>
										</div>
		<!------------------------->
		<!------------------------->
											<div class="col-sm-4">
											<div class="widget-box">
												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group">
															<div class="col-xs-12">
																<input  type="file" id="upload3" name="upload3" />
															</div>
														</div>
														<label  style="padding-left:10px;black">
															<input type="radio" value="gambar3" name="set" id="set"  />
															<span class="lbl black"> Set gambar utama</span>
														</label>
													
											
												</div>
											</div>
											</div>
										</div>
		<!------------------------->
		<!------------------------->
		<!------------------------->
										<div class="col-sm-6">
											<div class="widget-box">
												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group">
															<div class="col-xs-12">
																<input type="file" id="upload4" name="upload4" />
															</div>
														</div>
														<label  style="padding-left:10px;black">
															<input type="radio" name="set" id="set" value="gambar4"  />
															<span class="lbl black"> Set gambar utama</span>
														</label>
													
											
												</div>
											</div>
											</div>
										</div>
		<!------------------------->
		<!------------------------->
										<div class="col-sm-6">
											<div class="widget-box">
												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group">
															<div class="col-xs-12">
																<input  type="file" id="upload5" name="upload5" />
															</div>
														</div>
														<label  style="padding-left:10px;black">
															<input type="radio" value="gambar5" name="set" id="set"  />
															<span class="lbl black"> Set gambar utama</span>
														</label>
													
											
												</div>
											</div>
											</div>
										</div>
		<!------------------------->
	


		<script src="<?php echo base_url()?>plug/assets/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url()?>plug/assets/js/ace-elements.min.js"></script>
		<script src="<?php echo base_url()?>plug/assets/js/ace.min.js"></script>

		<script type="text/javascript">
			jQuery(function($) {
					$('#upload1').ace_file_input({
					style:'well',
					btn_choose:'Drop files here or click to choose',
					btn_change:null,
					no_icon:'ace-icon fa fa-cloud-upload',
					droppable:true,
					thumbnail:'fit'//large | fit
					,
					preview_error : function(filename, error_code) {

					}
			
				}).on('change', function(){
				});
				
				$('#upload2').ace_file_input({
					style:'well',
					btn_choose:'Drop files here or click to choose',
					btn_change:null,
					no_icon:'ace-icon fa fa-cloud-upload',
					droppable:true,
					thumbnail:'fit'//large | fit
					,
					preview_error : function(filename, error_code) {

					}
			
				}).on('change', function(){
				});
				
				$('#upload3').ace_file_input({
					style:'well',
					btn_choose:'Drop files here or click to choose',
					btn_change:null,
					no_icon:'ace-icon fa fa-cloud-upload',
					droppable:true,
					thumbnail:'fit'//large | fit
					,
					preview_error : function(filename, error_code) {

					}
			
				}).on('change', function(){
				});
				
				$('#upload4').ace_file_input({
					style:'well',
					btn_choose:'Drop files here or click to choose',
					btn_change:null,
					no_icon:'ace-icon fa fa-cloud-upload',
					droppable:true,
					thumbnail:'fit'//large | fit
					,
					preview_error : function(filename, error_code) {

					}
			
				}).on('change', function(){
				});
				
				$('#upload5').ace_file_input({
					style:'well',
					btn_choose:'Drop files here or click to choose',
					btn_change:null,
					no_icon:'ace-icon fa fa-cloud-upload',
					droppable:true,
					thumbnail:'fit'//large | fit
					,
					preview_error : function(filename, error_code) {

					}
			
				}).on('change', function(){
				});
				
				
			});
		</script>

<!------------------------>
</div>




</div>
						
                        </div>
	
                    </div>
					</form>
                </div>
            </div>
        </div>

    <script src="<?php echo base_url() ?>plug/boostrap/js/select2.min.js"></script>
    <script>
                                      $("document").ready(function () {
                                         // $(".step1").show();
                                          $(".step2").addClass("hilang");
                                          $(".step3").addClass("hilang");
                                          $(".step4").addClass("hilang");
                                          $(".step5").addClass("hilang");
                                      });

                                      //nice select boxes
                                      $('#sertifikat').select2();
                                      $('#provinsi').select2();
                                      $('#kabupaten').select2();
                                      $('#hadap').select2();
                                      $('#sewa').select2();
                                      $('#agen').select2();
                                      $('#owner').select2();
    </script>

    <script>
        $("#provinsi").change(function () {
            var prov = $("#provinsi").val();
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "<?php echo base_url() ?>data_property/ajaxGetKab",
                data: "prov=" + prov,
                success: function (data) {
                    $(".dataKab").html(data);
                }
            });

        });
		loadkab();
	function loadkab(){
            var prov = "32";
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "<?php echo base_url() ?>data_property/ajaxGetKab",
                data: "def=3273&prov=" + prov,
                success: function (data) {
                    $(".dataKab").html(data);
                }
            });

        };
    </script>
    <script src="<?php echo base_url() ?>plug/typehead/typehead.js"></script>
    <script>
        $('input.typeahead').typeahead({
            source: function (query, process) {
                var kab = $("#kabupaten").val();
                return $.get('<?php echo base_url() ?>data_property/getKomplek/' + kab, {query: query}, function (data) {
                    console.log(data);
                    data = $.parseJSON(data);
                    return process(data);
                });
            }
        });
		
    </script>
	