<div class="row">
    <div class="col-lg-12">

<div class="panel-group accordion" id="accordion">

<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
<i class="fa fa-filter"></i> Filter Pencarian
</a>
</h4>
</div>
<div id="collapseThree" class="panel-collapse collapse">
<div class="panel-body">
<!--------------------------->
<form id="form" action="javascript:getListing()">
  <div class="form-group fg_prov col-md-3" > 
                                        <label class="control-label black b" ></label>
                                        <?php
                                        $ref_type = $this->reff->getProvinsi();
                                        $array_prov[""] = "==== Pilih Provinsi ====";
                                        foreach ($ref_type as $val) {
                                            $array_prov[$val->id] = '[' . $val->id . ']' . ' &nbsp;' . $val->provinsi;
                                        }
                                        $data = $array_prov;
                                        echo form_dropdown('provinsi', $data, '', 'onchange="return cek_prov()"   id="provinsi" class="select2-container" style="width:100%"');
                                        ?>
                                     
                                  
                                 
                                        <label class="control-label black b" ></label>
                                        <span class="dataKab">
										<?php
                                          $kab[""]="==== Pilih ====";
										  $data=$this->reff->getKab(32);
										 foreach ($data as $val) {
												 $kab[$val->id] = '['.$val->id.']'.' &nbsp;'.$val->kabupaten;
												 }
												 $dataKab = $kab;
												echo form_dropdown('kabupaten', $dataKab, "", ' id="kabupaten" class="select2-container" style="width:100%" onchange="return cek_kab()"');     
										//echo "<script>	$('#kabupaten').select2();</script>"; ?>
                                        </span>
										
							 
                                        <label class="control-label black b" for="inputError"> </label>
                                        <input type="text" name="area" id="area" onchange="getArea()" placeholder="Area"  class="form-control">
										  <input type="hidden" name="lat_area" id="lat_area" class="form-control col-md-12">
                                        <input type="hidden" name="long_area" id="long_area" class="form-control col-md-12">

                                        
										
                                    
	   <div class="form-group black">
                                        <label class="control-label black b" ></label>
                                        <?php
                                        $ref_type = $this->reff->getTypePro();
                                        $array[""] = "==== Pilih Jenis Properti ====";
                                        foreach ($ref_type as $val) {
                                            $array[$val->id_type] = $val->nama;
                                        }
                                        $data = $array;
                                        echo form_dropdown('jenis_pro', $data, '', '  id="jenis_pro"  class="form-control"  ');
                                        ?>
                                      
                                  
                                      
                                    </div>									
    </div>	
	
	
	 <div class='col-md-3'>
                                     <div class="form-group black fg_tanah col-md-12">
                                    	 
												<label for="kamar_tidur"></label>
											<div class="input-group" style="width:100%">
											
												<input type="number" class="form-control" placeholder="Kamar Tidur" id="kamar_tidur" name="kamar_tidur">
											   <span class="help-block err_tidur"></span>
											</div>
										   
										
										 
												<label for="kamar_mandi"></label>
												<div class="input-group" style="width:100%">
											
												<input type="number" class="form-control" placeholder="Kamar Mandi" id="kamar_mandi" name="kamar_mandi">
											   <span class="help-block"></span>
											</div>
											
									 	<label for="garasi"></label>
												<div class="input-group" style="width:100%">
											
												<input type="number" class="form-control" placeholder="Garasi" id="garasi" name="garasi">
											   <span class="help-block"></span>
											</div>
									 
										
										
										 
												<label for="daya_listrik"></label>
												<div class="input-group" style="width:100%">
											
												<input type="number" class="form-control" placeholder="Daya Listrik" id="daya_listrik" name="daya_listrik">
											   <span class="help-block"></span>
											</div>
										
												
									
									</div>
                                </div>			
	 <div class='col-md-3'>
                                     <div class="form-group black fg_tanah col-md-12">
                                     
										<label for="kamar_mandi"></label>
								
												<div class="input-group">
												<span class="input-group-addon">Rp</span>
												<input type="text" onkeydown="return numbersonly(this, event);" class="form-control" placeholder="Harga Minimal" id="harga_min" name="harga_min">
											  
											</div>  
											
											<label for="harga_max"></label>
											<div class="input-group">
												<span class="input-group-addon">Rp</span>
												<input type="text" onkeydown="return numbersonly(this, event);" class="form-control" placeholder="Harga Maksimal" id="harga_max" name="harga_max">
											</div> 
											
										
									
                                  
                                    
									
                              
                              <label for="sertifikat"></label>
									 <?php
                                        $ref_sert = $this->reff->getSertifikat();
                                        $array_sert[""] = "==== Jenis Sertifikat ====";
                                        foreach ($ref_sert as $val) {
                                            $array_sert[$val->id_sertifikat] = $val->nama;
                                        }
                                        $data = $array_sert;
                                        echo form_dropdown('sertifikat', $data, '', 'id="sertifikat" class="select2-container" style="width:100%"');
                                        ?>
									

								<label class="control-label black b" ></label>
								<label class="control-label black b" ></label>
                                        <?php                                        
                                        $arrayT[""] = "==== Pilih Tipe Listing ====";
                                        $arrayT["dijual"] = "JUAL";
                                        $arrayT["disewa"] = "SEWA";
                                        $data = $arrayT;
                                        echo form_dropdown('type_pro', $data, '', '  id="type_pro"  class="form-control"');
                                        ?>
									   <span class="help-block err_agen"></span>
									   
									   
											
								 
                                </div>		
						    </div>	 
							
							
							<div class='col-md-3'>
							
							
                                     <div class="form-group black fg_tanah col-md-12">
                                    	 
									  
                                        
										
									
								 
                                        <label for="type_sewa"></label>
									 <?php
                                        $ref_sewa = $this->reff->getSewa();
                                        $array_sewa[""] = "==== Type Sewa ====";
                                        foreach ($ref_sewa as $val) {
                                            $array_sewa[$val->id_sewa] = $val->nama;
                                        }
                                        $data = $array_sewa;
                                        echo form_dropdown('type_sewa', $data, '', 'onchange="return cek_sewa()"   id="sewa" class="form-control" style="width:100%"');
                                        ?>
									 
                              
									<label for="kelengkapan"></label>
									 <?php
                                        $array_l[""] = "==== Kelengkapan Gambar ====";
										$array_l["1"] = "Gambar tersedia";
                                        $array_l["2"] = "Gambar tidak tersedia";
                                        $array_l["3"] = "Gambar Brosur tersedia";
                                        $array_l["4"] = "Gambar Brosur tidak tersedia";
                                        
                                        $data = $array_l;
                                        echo form_dropdown('kelengkapan', $data, '', 'id="kelengkapan" class="form-control" style="width:100%;margin-top:0px"');
                                        ?>
									

									   <label for="status_penjualan"></label>
									
									 <?php
                                       $array_s[""] = "==== Status Penjualan ====";
                                       $array_s["1"] = "Terjual/Tersewa";
                                       $array_s["0"] = "Belum Laku";
                                        $data = $array_s;
                                        echo form_dropdown('status_penjualan', $data, '', 'onchange="return cek_agen()"   id="status_penjualan" class="form-control" style="width:100%"');
                                        ?>
									   <span class="help-block err_agen"></span>
									   
									   
											<button class='btn-primary btn btn-block' style="margin-top:20px"><i class='fa fa-search'></i> Cari</button>
									
								 
                                </div>		
						    </div>
	
	</form>
<!--------------------------->

</div>
</div>
</div>

<br>

	 
	 <div id="listing"></div>
	 
	 
   </div>
   </div>
   </div>
 

 <?php echo $this->load->view("js/tabel.phtml");?>
 <script>
 getListing();
 function getListing()
	{	
	$("#listing").html("<img src='<?php echo base_url();?>plug/img/load.gif'> Please wait...");
		$.ajax({
		url:"<?php echo base_url();?>listing_ag/getListing",
		type: "POST",
		data: $('#form').serialize(),
		success: function(data)
				{	
				$("#listing").html(data);
				},
		});	
	}

 </script>
 
 <!-- Bootstrap modal -->
  <div class="modal fade" id="modalDetail" role="dialog">
  <div class="modal-dialog" >
<div class="modal-content" >
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
&nbsp;
      </div>
      <div class="modal-body form">
<section class="content">
  <div class="row">
    <div class="col-lg-12 dataDetail">
	
		<!------------------------->
		<!------------------------->
    </div>
  </div>   <!-- /.row -->
</section><!-- /.content -->
  </div>
   </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
   <!-- Bootstrap modal -->  
   <div class="modal fade" id="modalPromo" role="dialog">  
	<div class="modal-dialog" ><div class="modal-content" >	
		<div class="modal-header">        
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span></button>&nbsp;      
		</div>      
	<div class="modal-body form">
   <section class="content">  
	<div class="row">    
		<div class="col-lg-12 dataDetailPromo">			
		<!------------------------->		
		<!------------------------->    
		</div>  
	</div>   
   <!-- /.row -->
   </section>
   <!-- /.content -->  
   </div>   
   </div>
   <!-- /.modal-content -->      
   </div><!-- /.modal-dialog -->    
   </div><!-- /.modal -->  
   <!-- End Bootstrap modal -->



 <script>
  function saveAdd()
	{	
	if(method=="edit")
	{
		var url="<?php echo base_url();?>listing_ag/update";
	}else{
		var url="<?php echo base_url();?>listing_ag/insert";
	}
		$(".load").html("<img src='<?php echo base_url();?>plug/img/load.gif'> Please wait...");
		$.ajax({
		url:url,
		type: "POST",
		data: $('#formAgen').serialize(),
	//	dataType: "JSON",
		success: function(data)
				{
				if(data==false){ alert("Gagal! Agen sudah ada pada database"); $(".load").html(""); $("[name='group']").focus(); return false;}
				$(".load").html('<font color="green"><i class="fa fa-check-circle fa-fw fa-lg"></i> Berhasil di simpan</font>');
				table.ajax.reload(null,false);
				$("#formAgen")[0].reset();
				//$(".load").html("");
				if(method=="edit")
				{
					$("#modalAdd").modal("hide");
					$(".load").html('');
				}
				},
				
		});
	}
	
	 function hapus(id)
	  {
	    var tanya=window.confirm("Hapus ?");
		if(tanya==false){ return false;};
	  	$.ajax({
		url:"<?php echo base_url();?>listing_ag/hapus/"+id,
		type: "POST",
		data:"id="+id,
		success: function(data)
				{
				table.ajax.reload(null,false); //reload datatable ajax 
				},
			});
	  }

</script>	



<script>
function exportData()
{
	window.location.href="<?php echo base_url()?>listing_ag/export";
}

</script>






 <!-- Bootstrap modal -->
  <div class="modal fade" id="modalImport" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
     
      <div class="modal-body form">
<section class="content">
  <div class="row">
    <div class="col-lg-12">
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-download"></i> Import Data  <span class='namaGroup'></span></h4>
      </div>
      <!-- general form elements -->
      <div class="box box-primary black">	   <br>
        Silahkan <a href='<?php echo base_url();?>listing_ag/downloadFormat'><i class="fa fa-file-excel-o"></i> download format</a> sebelum upload.
          <div class="box-body">                      
            <form role="form" name="uploadfilexl" id="uploadfilexl" action="javascript:void();" method="post" enctype="multipart/form-data">
                <div class="form-group">
                  <input id="userfile"  name="userfile" type="file" class="form-control">
                  <input  name="idGroup" type="hidden">
                </div>				
                <button type="submit" onclick="javascript:simpanfile();" class="btn btn-primary pull-right">
                    <span class="fa fa-upload"></span>&nbsp;Upload
                </button>
                <div class="form-group">
                    <div class="msg"></div>
                    <div class="hasil"></div>
                </div>
            </form>
          </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div>   <!-- /.row -->
</section><!-- /.content -->
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
  
   <?php echo $this->load->view("js/form.phtml"); ?>
  <script type="text/javascript">
function simpanfile(){
    var userfile=$('#userfile').val();
    $('#uploadfilexl').ajaxForm({
     url:'<?php echo base_url();?>listing_ag/importData/',
     type: 'post',
     data:{"userfile":userfile},
     beforeSend: function() {
        var percentVal = 'Mengupload 0%';
        $('.msg').html(percentVal);
     },
     uploadProgress: function(event, position, total, percentComplete) {
        var percentVal = 'Mengupload ' + percentComplete + '%';
        $('.msg').html(percentVal);
     },
     beforeSubmit: function() {
      $('.hasil').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Silahkan Tunggu ... ");
     },
     complete: function(xhr) {
        $('.msg').html('');
     }, 
     success: function(resp) {
        $('.hasil').html(resp);
		table.ajax.reload(null,false);
		$("#uploadfilexl")[0].reset();
     },
    });     
};
</script>   



<script src="<?php echo base_url() ?>plug/boostrap/js/select2.min.js"></script>
 <script>
                                   
                                      //nice select boxes
                                      $('#sertifikat').select2();
                                      $('#provinsi').select2();
                                      $('#kabupaten').select2();
                                      $('#hadap').select2();
                                      //$('#sewa').select2();
                                      $('#agen').select2();
                                      $('#owner').select2();
    </script>
 <script>
        $("#provinsi").change(function () {
            var prov = $("#provinsi").val();
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "<?php echo base_url() ?>listing_ag/ajaxGetKab2",
                data: "prov=" + prov,
                success: function (data) {
                    $(".dataKab").html(data);
                }
            });

        });

	function loadkab(){
            var prov = "32";
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "<?php echo base_url() ?>listing_ag/ajaxGetKab",
                data: "def=3273&prov=" + prov,
                success: function (data) {
                    $(".dataKab").html(data);
                }
            });

        };
    </script>
	 <script>
        $('input.typeahead').typeahead({
            source: function (query, process) {
                var kab = $("#kabupaten").val();
                return $.get('<?php echo base_url() ?>listing_ag/getKomplek/' + kab, {query: query}, function (data) {
                    console.log(data);
                    data = $.parseJSON(data);
                    return process(data);
                });
            }
        });
    </script>
	
 

	
	<script>

	var f=jQuery.noConflict();
function godetail(id)
{
			f("#modalDetail").modal("show");
	  f('.dataDetail').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Mohon Tunggu ... ");
	            f.ajax({
                type: "POST",
                dataType: "html",
                url: "<?php echo base_url() ?>listing_ag/getDataDetail",
                data: "id=" + id,
                success: function (data) {
			
                    f(".dataDetail").html(data);
                }
            });
			
	
}	
	function gopromo(id)
	{			
		f("#modalPromo").modal("show");	  
		f('.dataDetailPromo').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Mohon Tunggu ... ");	            
		f.ajax({                
		type: "POST",                
		dataType: "html",                
		url: "<?php echo base_url() ?>listing_ag/getDataPromo/"+id,                
		data: "id=" + id,                
		success: function (data) {	
		
			f(".dataDetailPromo").html(data);                
		
		}            
		});				
	}
	</script>
	
	 
<script>

function history(id,kode_listing)
{
	f("#modalListingReport").modal("show");
	 f('.dataDetail').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Mohon Tunggu ... ");
	           f.ajax({
                type: "POST",
                dataType: "html",
                url: "<?php echo base_url();?>listing_ag/goReportListing/"+id+"/"+kode_listing,
                success: function (data) {
		        f(".dataDetail").html(data);
                }
            });
}	
</script>


 <!-- Bootstrap modal -->
  <div class="modal fade" id="modalListingReport" role="dialog">
  <div class="modal-dialog">
<div class="modal-content" >
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-info-circle"></i> Progress<span class='namaGroup'></span></h4>
      </div>
      <div class="modal-body form">
<section class="content">
  <div class="row">
    <div class="col-lg-12 dataDetail">
		<!------------------------->
	
		<!------------------------->
		
    </div>
	<div class="conversation-new-message">
<form action="javascript:simpan()" id="formL" >
<div class="form-group">
 <?php
                                        $ref_sert = $this->reff->getTitleListing();
                                        $array_sert[""] = "==== Pilih Katagori Report ====";
                                        foreach ($ref_sert as $val) {
                                            $array_sert[$val->id] = $val->nama;
                                        }
                                        $data = $array_sert;
                                        echo form_dropdown('titleListing', $data, '', 'required id="titleListing" class="form-control" style="width:100%"');
                                        ?>
<textarea class="form-control" rows="2" name="textListing" id="textListing" placeholder="Enter your repport..." required></textarea>
</div>
<div class="clearfix">
<span class="msg pull-left"></span>
<button type="submit"    class="btn btn-success pull-right">Send Progress</button>
</div>
</form>
</div>
  </div>   <!-- /.row -->
</section><!-- /.content -->
  </div>
   </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->

 <script>  
 function gocma(id)
{   
	f("#modalCMA").modal("show");
	  f('#kontenCMA').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Mohon Tunggu ... ");
	            f.ajax({
                type: "POST",
                dataType: "html",
                url: "<?php echo base_url() ?>listing_ag/getFormCMA/"+id,
                success: function (data) {
		              f("#kontenCMA").html(data);
		              }
            });
}
function gocmaEdit(id)
{   
	f("#modalCMAEdit").modal("show");
	  f('#kontenCMAEdit').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Mohon Tunggu ... ");
	            f.ajax({
                type: "POST",
                dataType: "html",
                url: "<?php echo base_url() ?>listing_ag/getFormCMAEdit/"+id,
                success: function (data) {
		              f("#kontenCMAEdit").html(data);
		              }
            });
}

function saveAddCMA()
	{	
		//var closeAddCMA();
		var url="<?php echo base_url();?>listing_ag/insertHasilCMA";
		f(".load").html("<img src='<?php echo base_url();?>plug/img/load.gif'> Please wait...");
		f.ajax({
		url:url,
		type: "post",
		data: f('#formJual').serialize(),
	//	dataType: "JSON",
		success: function(data)
				{
				 	f("#modalCMA").modal("hide");
					table.ajax.reload(null,false); //reload datatable ajax 	
				},
				
		});
	}


function saveEditCMA()
	{	
		//var closeAddCMA();
		var url="<?php echo base_url();?>listing_ag/editHasilCMA";
		f(".load").html("<img src='<?php echo base_url();?>plug/img/load.gif'> Please wait...");
		f.ajax({
		url:url,
		type: "post",
		data: f('#formJual1').serialize(),
	//	dataType: "JSON",
		success: function(data)
				{
				 	f("#modalCMAEdit").modal("hide");
					table.ajax.reload(null,false); //reload datatable ajax 	
				},
				
		});
	}
</script>
 
 <!-- Bootstrap modal -->
  <div class="modal fade" id="modalCMA" role="dialog">
  <div class="modal-dialog">
 
<div class="modal-content">
     
      <div class="modal-body form">
   
<section class="content">
  <div class="row">
    <div class="col-lg-12">
	
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-plus-circle"></i><span class='title'> Form </span></h4>
      </div>
	<br>
<div id="kontenCMA">
</div>


    </div>
  </div>   <!-- /.row -->

</section><!-- /.content -->
  </div>
   </div><!-- /.modal-content -->
		

      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
  
  
   <!-- Bootstrap modal -->
  <div class="modal fade" id="modalCMAEdit" role="dialog">
  <div class="modal-dialog">
 
<div class="modal-content">
     
      <div class="modal-body form">
   
<section class="content">
  <div class="row">
    <div class="col-lg-12">
	
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-plus-circle"></i><span class='title'> Edit Form </span></h4>
      </div>
	<br>
<div id="kontenCMAEdit">
</div>


    </div>
  </div>   <!-- /.row -->

</section><!-- /.content -->
  </div>
   </div><!-- /.modal-content -->
		

      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
  
  