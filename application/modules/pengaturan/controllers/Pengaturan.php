<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pengaturan extends CI_Controller {

	

	function __construct()
	{
		parent::__construct();	
		$this->load->model('m_pengaturan','pengaturan');
		$this->m_konfig->validasi_session(array("admin"));
	}
	
	function _template($data)
	{
	$this->load->view('template/main',$data);	
	}
	
	public function index()
	{

	$data['konten']="index";
	$data['blacklist']=$this->pengaturan->pengaturan(4);
	$data['namaAplikasi']=$this->pengaturan->pengaturan(3);
	$data['kontenInformasi']=$this->pengaturan->pengaturan(5);
	$data['center']=$this->pengaturan->pengaturan(2);
	$data['modem']=$this->pengaturan->pengaturan(6);
	$data['kontenInformasi']=$this->pengaturan->pengaturan(5);
	$this->_template($data);
	}

	
	
	function simpan()
	{
	echo $this->pengaturan->simpan();
	}

	
	
	
	
	
}

