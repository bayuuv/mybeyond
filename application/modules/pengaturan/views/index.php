<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>plug/boostrap/css/libs/summernote.min.css"/>
<div class="main-box clearfix black">
<header class="main-box-header clearfix">
<h2><i class="fa fa-cogs"></i> Pengaturan</h2>

<span class="pull-right"><a href="javascript:themeApp()"><i class="fa fa-tint"></i> Thema Aplikasi</a></span>
</header>
<div class="main-box-body clearfix">
<form class="form-horizontal black" id="formset" name="formset" action="javascript:void()">

<div class="form-group">
<label for="Nama" class="col-lg-2 control-label">Nama Aplikasi</label>
<div class="col-lg-10">
<input type="text" class="form-control" name="nama" value="<?php echo $namaAplikasi; ?>">
</div>
</div>


<div class="form-group">
<label for="Blacklist" class="col-lg-2 control-label">Nomor Blacklist</label>
<div class="col-lg-10">
<input type="text" name="blacklist" id="Blacklist" value="<?php echo $blacklist; ?>">
</div>
</div>

<div class="form-group">
<label for="Blacklist" class="col-lg-2 control-label">Nomor Center</label>
<div class="col-lg-10">
<input type="text" name="center" id="center" class="form-control" value="<?php echo $center; ?>">
</div>
</div>

<div class="form-group">
<label for="Blacklist" class="col-lg-2 control-label">Modem</label>
<div class="col-lg-10">
<input type="text" name="modem" id="modem"  value="<?php echo $modem; ?>">
</div>
</div>




<span class="msgset" style="margin-left:20px"></span>

<button type="submit" class="btn btn-success pull-right" onclick="simpanSet()"><i class="fa fa-save"></i> Simpan</button>
<div class="col-md-12" ><br><div style="background-color:white;color:black">
	<textarea style="background-color:white" name="isi" class="summernote form-control"><?php echo $kontenInformasi; ?></textarea></div>
</div>


</form>
</div>

</div>

<?php echo $this->load->view("js/form.phtml");?>
<script>
function simpanSet()
{
	$('.msgset').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Please wait...");
	$('#formset').ajaxForm({
	url:"<?php echo base_url();?>pengaturan/simpan",
	type: "POST",
    data: $('#formset').serialize(),
	success: function(data)
            {
			$('.msgset').html('<font color="green"><i class="fa fa-check-circle fa-fw fa-lg"></i> Tersimpan...</font>');
		
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Try Again!');
            }
	});
}	
	
	
	$('#Blacklist').selectize({
					persist: false,
					createOnBlur: true,
					create: true
				});
	$('#modem').selectize({
					persist: false,
					createOnBlur: true,
					create: true
				});
				
	$(function(){
		$('.summernote').summernote({
			height: 250,
			minHeight: 250,
		});
	});
	
</script>
<script src="<?php echo base_url();?>plug/boostrap/js/summernote.min.js"></script> 