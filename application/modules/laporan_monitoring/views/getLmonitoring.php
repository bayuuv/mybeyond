<!--<div style="margin-top:20px;"><button class="btn-danger pull-right" onclick="addTarget()"> <i class="fa fa-plus-circle"></i> Add</button></div>-->
<a href="<?php echo base_url()?>laporan_monitoring/export/<?php echo $filter;?>" class="sadow05 pull-right" style="margin-top:20px;color:black" >  <i class="fa fa-upload"></i> Export Data</a>
		<div class="clearfix"></div><div id="infoxx"></div>
		<div class="main-box clearfix" >
			<div class="main-box-body clearfix ">
			<div class="table-responsive">
			<span style='position:absolute;margin-top:48px;z-index:222' class="cursor btnhapus">
			<a href="#" onclick="hapusAll()"><i class='fa fa-trash'></i> Hapus Terpilih</a>
			</span>
			<form action="#" name="delcheck" id="delcheck" class="form-horizontal" method="post">
			<table id='table' class="tabel black table-striped table-bordered table-hover dataTable" width="100%">
						<thead style="font-size:13px">			
						  <tr>
							<th class="text-center" axis="string" rowspan="3">Member</th>
							<th class='text-center' axis="string" colspan="7">GOAL</th>
							<th class='text-center' axis="string" colspan="7">DEALING</th>
							<th class='text-center' axis="string" colspan="7">Listing</th>
							<th class='text-center' axis="string" colspan="5">Promotion Online</th>
							<th class='text-center' axis="string" colspan="6">Promotion Offline</th>
							<th class='text-center' axis="string" colspan="7">Call In</th>
							<th class='text-center' axis="string" colspan="7">Showing</th>
						  <tr>
							<th class='text-center' axis="string" rowspan="2">Target</th><!--GOAL-->
							<th class='text-center' axis="string" colspan="4">Realisasi</th>
							<th class='text-center' axis="string" rowspan="2">Total</th>
							<th class='text-center' axis="string" rowspan="2">Sisa</th>
							<th class='text-center' axis="string" rowspan="2">Target</th><!--DEALING-->
							<th class='text-center' axis="string" colspan="4">Realisasi</th>
							<th class='text-center' axis="string" rowspan="2">Total</th>
							<th class='text-center' axis="string" rowspan="2">Sisa</th>
							<th class='text-center' axis="string" rowspan="2">Target</th><!--LISTING-->
							<th class='text-center' axis="string" colspan="4">Realisasi</th>
							<th class='text-center' axis="string" rowspan="2">Total</th>
							<th class='text-center' axis="string" rowspan="2">Sisa</th>
							<th class='text-center' axis="string" rowspan="2">Target</th><!--PROMO ONline-->
							<th class='text-center' axis="string" colspan="4">Realisasi</th>
							<!--<th class='text-center' axis="string" rowspan="2">Total</th>
							<th class='text-center' axis="string" rowspan="2">Sisa</th>-->
							<!--<th class='text-center' axis="string" rowspan="2">Target</th>--><!--PROMO OFFLINE-->
							<th class='text-center' axis="string" colspan="4">Realisasi</th>
							<th class='text-center' axis="string" rowspan="2">Total</th>
							<th class='text-center' axis="string" rowspan="2">Sisa</th>
							<th class='text-center' axis="string" rowspan="2">Target</th><!--CALLIN-->
							<th class='text-center' axis="string" colspan="4">Realisasi</th>
							<th class='text-center' axis="string" rowspan="2">Total</th>
							<th class='text-center' axis="string" rowspan="2">Sisa</th>
							<th class='text-center' axis="string" rowspan="2">Target</th><!--SHOWING-->
							<th class='text-center' axis="string" colspan="4">Realisasi</th>
							<th class='text-center' axis="string" rowspan="2">Total</th>
							<th class='text-center' axis="string" rowspan="2">Sisa</th>
						  </tr>
						  <tr>
						  <?php 
							$bulan=$this->input->post("bulan");
							$tahun=$this->input->post("tahun");
							
							if($bulan != '' && $tahun != ''){
								$blnn = $bulan;
								$tahunn = $tahun;
							}else{   
								$today = date("Y-m-d");
								$pisah = explode("-",$today);
								$blnn = $pisah[1];
								$tahunn = $pisah[0];
							}
							
							if($blnn == '01' or $blnn == '02' or $blnn =='03' or $blnn == '04')
							{
								for ($i= 1; $i <= 7; $i++){
									echo "<th class='thead' axis=\"string\">Jan ".$tahunn."</th>";
									echo "<th class='thead' axis=\"string\">Feb ".$tahunn."</th>";
									echo "<th class='thead' axis=\"string\">Mar ".$tahunn."</th>";
									echo "<th class='thead' axis=\"string\">Apr ".$tahunn."</th>";
								}
							}elseif($blnn == '05' or $blnn == '06' or $blnn == '07' or $blnn == '08')
							{
								for ($i= 1; $i <= 7; $i++){
									echo "<th class='thead' axis=\"string\">Mei ".$tahunn."</th>";
									echo "<th class='thead' axis=\"string\">Jun ".$tahunn."</th>";
									echo "<th class='thead' axis=\"string\">Jul ".$tahunn."</th>";
									echo "<th class='thead' axis=\"string\">Agt ".$tahunn."</th>";
								}
							}elseif($blnn == '09' or $blnn == '10' or $blnn == '11' or $blnn == '12')
							{
								for ($i= 1; $i <= 7; $i++){
									echo "<th class='thead' axis=\"string\">Sep ".$tahunn."</th>";
									echo "<th class='thead' axis=\"string\">Okt ".$tahunn."</th>";
									echo "<th class='thead' axis=\"string\">Nop ".$tahunn."</th>";
									echo "<th class='thead' axis=\"string\">Des ".$tahunn."</th>";
								}
							}
						  ?>
						  </tr>
						</thead>
			</table></form>
			</div>
	   </div>
     </div>
 <!--  </div>
 </div> -->



<?php echo $this->load->view("js/tabel.phtml");?>

<script>
  
		var table;
		table = $('#table').DataTable({ 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('laporan_monitoring/ajax_lmonitoring/'.$filter.'')?>",
            "type": "POST",
        },
		
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
		    "targets": [0,1,2,3,4,5,6,7,8,9], //last column
          "orderable": false, //set not orderable
        },
        ],

      });
</script>

<script>
  function hapuss(id)
	  {
	    var tanya=window.confirm("Hapus ?");
		if(tanya==false){ return false;};
	  	$.ajax({
		url:"<?php echo base_url();?>target/hapus/"+id,
		type: "POST",
		data:"id="+id,
		success: function(data)
				{
				table.ajax.reload(null,false); //reload datatable ajax 
				},
			});
	  }
	  
  function hapusAll()
	{	
		var con=window.confirm("hapus data terpilih ?");
		if(con==false){ return false; };
		$.ajax({
		url:"<?php echo base_url();?>target/HapusAll",
		type: "POST",
		data: $('#delcheck').serialize(),
	//	dataType: "JSON",
		success: function(data)
				{	 $(".btnhapus").hide();
					$(".pilihsemua").removeAttr("checked");
					$(".pilihsemua").val("ya");
					table.ajax.reload(null,false); //reload datatable ajax 
				},
				error: function (jqXHR, textStatus, errorThrown)
				{
					alert('Try Again!');
				}
		});
	
	
	}
  
  
  $(".btnhapus").hide();
  	$(".pilihsemua").click(function(){
	
		if($(".pilihsemua").val()=="ya") {
		$(".pilih").prop("checked", "checked");
		$(".pilihsemua").val("no");
		  $(".btnhapus").show();
		} else {
		$(".pilih").removeAttr("checked");
		$(".pilihsemua").val("ya");
		  $(".btnhapus").hide();
		}
	
	});
	
	function pilcek(){
		$(".btnhapus").show();
		$(".pilihsemua").removeAttr("checked");
		$(".pilihsemua").val("ya");
		 
	};
	
</script>	
<script src="<?php echo base_url() ?>plug/boostrap/js/select2.min.js"></script>
 <script>
  $('#agen').select2();
  $('#agenx').select2();
</script>