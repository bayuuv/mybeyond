<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan_monitoring extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->m_konfig->validasi_session(array("user","promotion"));
		$this->load->model("M_lmonitoring","lmonitoring");
		date_default_timezone_set('Asia/Jakarta');
	}
	
	function _template($data)
	{
	$this->load->view('template/main',$data);	
	}
	
	public function index()
	{

	$data['konten']="index";
	$this->_template($data);
	}
	function getLmonitoring()
	{
	$tahun=$this->input->post("tahun");
	$bulan=$this->input->post("bulan");
	$filter['filter']="?tahun=$tahun&bulan=$bulan";
	echo	$this->load->view("getLmonitoring",$filter);
	}
	function ajax_lmonitoring()
	{
		$list = $this->lmonitoring->get_dataLmonitoring();
        $data = array();
        $no = $_POST['start']+1;
		
		$bulan=$this->input->get("bulan");
		$tahun=$this->input->get("tahun");
		
		if($bulan != '' && $tahun != ''){
			$blnn = $bulan;
			$tahunn = $tahun;
		}else{   
			$today = date("Y-m-d");
			$pisah = explode("-",$today);
			$blnn = $pisah[1];
			$tahunn = $pisah[0];
		}
		//$today = date("Y-m-d");
		//$pisah = explode("-",$today);
		//$blnn = $pisah[1];
		
		if($blnn == '01' or $blnn == '02' or $blnn =='03' or $blnn == '04')
		{
			$datee = "".$tahunn."-01-01";
		}elseif($blnn == '05' or $blnn == '06' or $blnn == '07' or $blnn == '08')
		{
			$datee = "".$tahunn."-05-01";
		}elseif($blnn == '09' or $blnn == '10' or $blnn == '11' or $blnn == '12')
		{
			$datee = "".$tahunn."-09-01";
		}
		
        foreach ($list as $val) {
		
		$id_agen = $val->id_agen;
		
		$targetGoal = $this->lmonitoring->getTargetGoal($val->kode_agen,$datee);
		$targetGoal1 = $this->lmonitoring->getTargetGoal1($val->kode_agen,$datee);
		$targetGoal2 = $this->lmonitoring->getTargetGoal2($val->kode_agen,$datee);
		$targetGoal3 = $this->lmonitoring->getTargetGoal3($val->kode_agen,$datee);
		$targetGoal4 = $this->lmonitoring->getTargetGoal4($val->kode_agen,$datee);
		$totalTargetGoal = $targetGoal1 + $targetGoal2 + $targetGoal3 + $targetGoal4;
		$sisaTargetGoal = $targetGoal - $totalTargetGoal;
		
		$targetTransaksi = $this->lmonitoring->getTargetTransaksi($val->kode_agen,$datee);
		$targetTransaksi1 = $this->lmonitoring->getTargetTransaksi1($val->kode_agen,$datee);
		$targetTransaksi2 = $this->lmonitoring->getTargetTransaksi2($val->kode_agen,$datee);
		$targetTransaksi3 = $this->lmonitoring->getTargetTransaksi3($val->kode_agen,$datee);
		$targetTransaksi4 = $this->lmonitoring->getTargetTransaksi4($val->kode_agen,$datee);
		$totalTargetTransaksi = $targetTransaksi1 + $targetTransaksi2 + $targetTransaksi3 + $targetTransaksi4;
		$sisaTargetTransaksi = $targetTransaksi - $totalTargetTransaksi;
		
		$targetListing = $this->lmonitoring->getTargetListing($val->kode_agen,$datee);
		$targetListing1 = $this->lmonitoring->getTargetListing1($val->kode_agen,$datee);
		$targetListing2 = $this->lmonitoring->getTargetListing2($val->kode_agen,$datee);
		$targetListing3 = $this->lmonitoring->getTargetListing3($val->kode_agen,$datee);
		$targetListing4 = $this->lmonitoring->getTargetListing4($val->kode_agen,$datee);
		$totalTargetListing = $targetListing1 + $targetListing2 + $targetListing3 + $targetListing4;
		$sisaTargetListing = $targetListing - $totalTargetListing;
		
		$targetPromoOnline = $this->lmonitoring->getTargetPromoOnline($val->kode_agen,$datee);
		$targetPromoOnline1 = $this->lmonitoring->getTargetPromoOnline1($val->kode_agen,$datee);
		$targetPromoOnline2 = $this->lmonitoring->getTargetPromoOnline2($val->kode_agen,$datee);
		$targetPromoOnline3 = $this->lmonitoring->getTargetPromoOnline3($val->kode_agen,$datee);
		$targetPromoOnline4 = $this->lmonitoring->getTargetPromoOnline4($val->kode_agen,$datee);
		$totalTargetPromoOnline = $targetPromoOnline1 + $targetPromoOnline2 + $targetPromoOnline3 + $targetPromoOnline4;
		$sisaTargetPromoOnline = $targetPromoOnline - $totalTargetPromoOnline;
		
		$targetPromoOffline = $this->lmonitoring->getTargetPromoOffline($val->kode_agen,$datee);
		$targetPromoOffline1 = $this->lmonitoring->getTargetPromoOffline1($val->kode_agen,$datee);
		$targetPromoOffline2 = $this->lmonitoring->getTargetPromoOffline2($val->kode_agen,$datee);
		$targetPromoOffline3 = $this->lmonitoring->getTargetPromoOffline3($val->kode_agen,$datee);
		$targetPromoOffline4 = $this->lmonitoring->getTargetPromoOffline4($val->kode_agen,$datee);
		$totalTargetPromoOffline = $targetPromoOffline1 + $targetPromoOffline2 + $targetPromoOffline3 + $targetPromoOffline4;
		$sisaTargetPromoOffline = $targetPromoOffline - $totalTargetPromoOffline;
		
		$targetPromo = $targetPromoOffline + $targetPromoOnline;
		$totalTargetPromo = $totalTargetPromoOffline + $totalTargetPromoOnline;
		$sisaTargetPromo = $sisaTargetPromoOffline + $sisaTargetPromoOnline;
		
		$targetCallin = $this->lmonitoring->getTargetCallin($val->kode_agen,$datee);
		$targetCallin1 = $this->lmonitoring->getTargetCallin1($val->kode_agen,$datee);
		$targetCallin2 = $this->lmonitoring->getTargetCallin2($val->kode_agen,$datee);
		$targetCallin3 = $this->lmonitoring->getTargetCallin3($val->kode_agen,$datee);
		$targetCallin4 = $this->lmonitoring->getTargetCallin4($val->kode_agen,$datee);
		$totalTargetCallin = $targetCallin1 + $targetCallin2 + $targetCallin3 + $targetCallin4;
		$sisaTargetCallin = $targetCallin - $totalTargetCallin;
		
		$targetShowing = $this->lmonitoring->getTargetShowing($val->kode_agen,$datee);
		$targetShowing1 = $this->lmonitoring->getTargetShowing1($val->kode_agen,$datee) + $this->lmonitoring->getTargetShow1($val->kode_agen,$datee);
		$targetShowing2 = $this->lmonitoring->getTargetShowing2($val->kode_agen,$datee) + $this->lmonitoring->getTargetShow2($val->kode_agen,$datee);
		$targetShowing3 = $this->lmonitoring->getTargetShowing3($val->kode_agen,$datee) + $this->lmonitoring->getTargetShow3($val->kode_agen,$datee);
		$targetShowing4 = $this->lmonitoring->getTargetShowing4($val->kode_agen,$datee) + $this->lmonitoring->getTargetShow4($val->kode_agen,$datee);
		$totaTargetShowing = $targetShowing1 + $targetShowing2 + $targetShowing3 + $targetShowing4;
		$sisaTargetShowing = $targetShowing - $totaTargetShowing;
		
		//$targetGoal1 = number_format($targetGoal,0,",",".");
		
			$row = array();
			//$row[] =  '<input type="checkbox" class="pilih" onclick="pilcek()" name="hapus[]" value="'.$val->id_target.'">';
			//$row[] = $no++;
			$row[] = $val->nama;
            $row[] = number_format($targetGoal,0,",",".");
			$row[] = number_format($targetGoal1,0,",",".");
			$row[] = number_format($targetGoal2,0,",",".");
			$row[] = number_format($targetGoal3,0,",",".");
			$row[] = number_format($targetGoal4,0,",",".");
			$row[] = number_format($totalTargetGoal,0,",",".");
			$row[] = number_format($sisaTargetGoal,0,",",".");
			$row[] = number_format($targetTransaksi,0,",",".");
			$row[] = number_format($targetTransaksi1,0,",",".");
			$row[] = number_format($targetTransaksi2,0,",",".");
			$row[] = number_format($targetTransaksi3,0,",",".");
			$row[] = number_format($targetTransaksi4,0,",",".");
			$row[] = number_format($totalTargetTransaksi,0,",",".");
			$row[] = number_format($sisaTargetTransaksi,0,",",".");
			$row[] = number_format($targetListing,0,",",".");
			$row[] = number_format($targetListing1,0,",",".");
			$row[] = number_format($targetListing2,0,",",".");
			$row[] = number_format($targetListing3,0,",",".");
			$row[] = number_format($targetListing4,0,",",".");
			$row[] = number_format($totalTargetListing,0,",",".");
			$row[] = number_format($sisaTargetListing,0,",",".");
			$row[] = number_format($targetPromo,0,",",".");
			$row[] = number_format($targetPromoOnline1,0,",",".");
			$row[] = number_format($targetPromoOnline2,0,",",".");
			$row[] = number_format($targetPromoOnline3,0,",",".");
			$row[] = number_format($targetPromoOnline4,0,",",".");
			$row[] = number_format($targetPromoOffline1,0,",",".");
			$row[] = number_format($targetPromoOffline2,0,",",".");
			$row[] = number_format($targetPromoOffline3,0,",",".");
			$row[] = number_format($targetPromoOffline4,0,",",".");
			$row[] = number_format($totalTargetPromo,0,",",".");
			$row[] = number_format($sisaTargetPromo,0,",",".");
			$row[] = number_format($targetCallin,0,",",".");
			$row[] = number_format($targetCallin1,0,",",".");
			$row[] = number_format($targetCallin2,0,",",".");
			$row[] = number_format($targetCallin3,0,",",".");
			$row[] = number_format($targetCallin4,0,",",".");
			$row[] = number_format($totalTargetCallin,0,",",".");
			$row[] = number_format($sisaTargetCallin,0,",",".");
			$row[] = number_format($targetShowing,0,",",".");
			$row[] = number_format($targetShowing1,0,",",".");
			$row[] = number_format($targetShowing2,0,",",".");
			$row[] = number_format($targetShowing3,0,",",".");
			$row[] = number_format($targetShowing4,0,",",".");
			$row[] = number_format($totaTargetShowing,0,",",".");
			$row[] = number_format($sisaTargetShowing,0,",",".");
			
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->lmonitoring->counts(),
            "recordsFiltered" => $this->lmonitoring->counts(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	
	function export()
	{
		$this->lmonitoring->export();
	}
	
}