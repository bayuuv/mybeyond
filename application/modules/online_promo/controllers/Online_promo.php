<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Online_promo extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->m_konfig->validasi_session(array("user","promotion"));
		$this->load->model("M_onlinepromo","onlinepromo");
	}
	
	function _template($data)
	{
	$this->load->view('template/main',$data);	
	}
	
	public function index()
	{

	$data['konten']="index";
	$this->_template($data);
	}
	function getOnlinePromo()
	{
	$agen=$this->input->post("agen");
	$bulan_sell=$this->input->post("bulan_sell");
	$tahun_sell=$this->input->post("tahun_sell");
	$kategori=$this->input->post("kategori");
	$filter['filter']="?agen=$agen&bulan_sell=$bulan_sell&tahun_sell=$tahun_sell&kategori=$kategori";
	echo	$this->load->view("getOnlinePromo",$filter);
	}
	function getAddOnlinePromo($id)
	{	$data["id"]=$id;
		$this->load->view("FormOnlinePromo",$data);
	}
	function ajax_onlinepromo()
	{
		$list = $this->onlinepromo->get_dataOnlinePromo();
        $data = array();
        $no = $_POST['start']+1;
        foreach ($list as $val) {
			if($val->jenis_promoonline == '1'){
				$jenis_promoonline = "Rumah123 Upload";
			}elseif($val->jenis_promoonline == '2'){
				$jenis_promoonline = "Rumah123 Refresh";
			}elseif($val->jenis_promoonline == '3'){
				$jenis_promoonline = "Urbanindo Upload";
			}elseif($val->jenis_promoonline == '4'){
				$jenis_promoonline = "Urbanindo Refresh";
			}elseif($val->jenis_promoonline == '5'){
				$jenis_promoonline = "OLX Upload";
			}elseif($val->jenis_promoonline == '6'){
				$jenis_promoonline = "OLX Refresh";
			}elseif($val->jenis_promoonline == '7'){
				$jenis_promoonline = "Rumah.com Upload";
			}elseif($val->jenis_promoonline == '8'){
				$jenis_promoonline = "Rumah.com Refresh";
			}else{
				$jenis_promoonline = "Lainnya";
			}
			
			$row = array();
			$row[] =  '<input type="checkbox" class="pilih" onclick="pilcek()" name="hapus[]" value="'.$val->id_promoonline.'">';
			//$row[] = $no++;
            $row[] = $this->tanggal->ind($val->tgl_promoonline,"/");
			$row[] = $val->nama;
			$row[] = $jenis_promoonline;
			$row[] = $val->jumlah_promoonline;
            $row[] = '
			<a href="#" style="font-size:14px" onclick="editpromo(`' . $val->id_promoonline .'`)" class=" "><i class="fa fa-edit "> </i> Edit </a>
			| <a href="#" style="font-size:14px" onclick="hapuss(`' . $val->id_promoonline . '`);" class=" "><i class="fa fa-trash"> </i> Hapus </a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->onlinepromo->counts(),
            "recordsFiltered" => $this->onlinepromo->counts(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	function insert()
	{
		echo $this->onlinepromo->insert();
	}
	function update()
	{
		echo $this->onlinepromo->update();
	}
	function HapusAll()
	{
	echo $this->onlinepromo->HapusAll();
	}
	function hapus($id)
	{
	echo $this->onlinepromo->hapus($id);
	}
	
	function export()
	{
		$this->onlinepromo->export();
	}
	function getEditonlinepromo($id)
	{	
		$data["id"]=$id;
		$this->load->view("FormEditonlinepromo",$data);
	}
	
}