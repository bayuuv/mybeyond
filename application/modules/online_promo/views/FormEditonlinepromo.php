<?php
$this->db->where("id_promoonline",$id);
$db=$this->db->get("promo_online")->row();
?>
<form action="javascript:saveAddPromoOnline()"  id="formMonitoring" class="form-horizontal black" method="post"  enctype="multipart/form-data"  >
<div class="form-group">
<input type="hidden" name="id_promoonline" id="id_promoonline" value="<?php echo $id;?>">
<label for="tgl_promo" class="b col-lg-3 control-label">Date</label>
<div class="col-lg-8">
<input type="text" class="form-control" id="tgl_promo"  name="tgl_promo" value="<?php echo $this->tanggal->ind($db->tgl_promoonline,"/");?>" >
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<label for="jenis" class="b col-lg-3 control-label">Agen</label>
<div class="col-lg-8">
<?php
    if($this->session->userdata("id")==64){
		$ref_agen = $this->reff->getAgenAjeng();
	}elseif($this->session->userdata("id")==151){
		$ref_agen = $this->reff->getAgenRhafa();
	}elseif($this->session->userdata("id")==133){
		$ref_agen = $this->reff->getAgenKiki();
	}elseif($this->session->userdata("id")==146){
		$ref_agen = $this->reff->getAgenYudi();
	}elseif($this->session->userdata("id")==152){
		$ref_agen = $this->reff->getAgenVivi();
	}elseif($this->session->userdata("id")==161){
		$ref_agen = $this->reff->getAgenYema();
	}elseif($this->session->userdata("id")==162){
		$ref_agen = $this->reff->getAgenFrans();
	}elseif($this->session->userdata("id")==165){
		$ref_agen = $this->reff->getAgenRena();
	}else{
		$ref_agen = $this->reff->getAgen();
    }
	$array_agen[""] = "==== choose ====";
    foreach ($ref_agen as $val) {
    $array_agen[$val->kode_agen] = $val->nama;
    }
    $data = $array_agen;
    echo form_dropdown('agenx', $data, $db->agen, ' id="agenx" class="select2-container" style="width:100%"');
?>
 <span class="help-block err_agen"></span>
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<label for="jenis" class="b col-lg-3 control-label">Kategori</label>
<div class="col-lg-8">
<?php                                        
    $arrayS[""] = "==== Pilih Kategori ====";
	$arrayS["1"] = "Rumah123 Upload";
    $arrayS["2"] = "Rumah123 Refresh";
	$arrayS["3"] = "Urbanindo Upload";
	$arrayS["4"] = "Urbanindo Refresh";
	$arrayS["5"] = "OLX Upload";
	$arrayS["6"] = "OLX Refresh";
	$arrayS["7"] = "Rumah.com Upload";
	$arrayS["8"] = "Rumah.com Refresh";
	$arrayS["9"] = "Lainnya";
    $data = $arrayS;
    echo form_dropdown('kategori', $data, $db->jenis_promoonline, '  id="kategori"  class="form-control"');
?>
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<label for="alamat_promo" class="b col-lg-3 control-label">Jumlah Promo </label>
<div class="col-lg-8">
<input type="text" class="form-control" id="jumlah"  name="jumlah" value="<?php echo $db->jumlah_promoonline;?>" >
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<div class="col-lg-offset-2 col-lg-9">
<span class='load'></span>
<button type="submit" class="btn btn-success pull-right" onclick="saveAddPromoOnline()"><i class='fa fa-save'></i> Save</button>
</div>
</div>
</form>


<?php echo $this->load->view("js/form.phtml"); ?>
<script>
function saveAddPromoOnline()
	{	
		var url="<?php echo base_url();?>online_promo/update";
		$(".load").html("<img src='<?php echo base_url();?>plug/img/load.gif'> Please wait...");
		$("#formMonitoring").ajaxForm({
		url:url,
		type: "post",
		data: $('#formMonitoring').serialize(),
	//	dataType: "JSON",
		success: function(data)
				{
						  closemodal("modalEditonlinepromo");
						  table.ajax.reload(null,false); //reload datatable ajax 
				},
				
		});
	}
</script>

<script src="<?php echo base_url();?>plug/boostrap/js/jquery.maskedinput.min.js"></script>  
<script>
$("#tgl_promo").mask("99/99/9999");
</script>
<script src="<?php echo base_url() ?>plug/boostrap/js/select2.min.js"></script>
 <script>
  $('#agenx').select2();
</script>