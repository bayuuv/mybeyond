<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {

	

	function __construct()
	{
		parent::__construct();	
		$this->load->model('m_profile','profile');
		$this->m_konfig->validasi_global();
	}
	
	function _template($data)
	{
	$this->load->view('template/main',$data);	
	}
	function generate()
	{
	$key=substr(str_shuffle("23456789ANCDEFGHJKLMPQRSTUVWXYA"),0,10); 
	echo $key;
	$this->db->where("id_admin",$this->session->userdata("id"));
	$this->db->update("admin",array("key"=>$key));
	
	}
	public function index()
	{

	$data['konten']="index";
	$data['dataProfil']=$this->profile->dataProfile($this->session->userdata("id"));
	$this->_template($data);
	}
	function update()
	{
	$data=$this->profile->update($this->session->userdata("id"));
	echo json_encode($data);
	}
	public function upload_img()
	{
	$this->profile->upload_img($this->session->userdata("id"));
	redirect("profile");
	}
	//data referensi
	function saveHeaderKode()
	{
	echo $this->profile->saveHeaderKode();
	}
	function saveBlacklist()
	{
	echo $this->profile->saveBlacklist();
	}
	function dataForm()
	{
	$data["data"]=$this->profile->dataForm();
	$this->load->view("dataForm",$data);
	}
	function updateForm()
	{
	echo $this->profile->updateForm();
	}
	function saveFormField()
	{
	echo $this->profile->saveFormField();
	}
}

