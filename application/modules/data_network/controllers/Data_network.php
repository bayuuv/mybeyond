<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Data_network extends CI_Controller {

	

	function __construct()
	{
		parent::__construct();	
		$this->m_konfig->validasi_session(array("user","operator","agen","designer","dm","network"));
		$this->load->model("M_data_agen","agen");
	}
	
	function _template($data)
	{
	$this->load->view('template/main',$data);	
	}
	
	public function index()
	{

	$data['konten']="index";
	$this->_template($data);
	}
	function ajax_agen()
	{
		$list = $this->agen->get_dataAgen();
        $data = array();
        $no = $_POST['start']+1;$jk="";
        foreach ($list as $val) {
			if($val->jk=="l"){
				$jk="Mr.";
			}
			if($val->jk=="p"){
				$jk="Mrs.";
			}
			$row = array();
			$row[] =  '<input type="checkbox" class="pilih" onclick="pilcek()" name="hapus[]" value="'.$val->id_agen.'">';
			$row[] = $no++;
            $row[] = "<a href='javascript:detail(`".$val->id_agen."`)'>".$val->kode_agen."</a>";
            $row[] = $jk." ".$val->nama;
            $row[] = $this->tanggal->septik($val->hp);
            $row[] = $val->email;
            $row[] = $val->alamat;
            $row[] = $this->reff->jumlahListing($val->kode_agen);
            $row[] = $this->reff->jumlahSelling($val->kode_agen);
            $row[] = $this->reff->jmlPelanggan($val->kode_agen);
            $row[] = '
			<a href="#" style="font-size:14px" onclick="edit(`' . $val->id_agen.'`)" class=" "><i class="fa fa-edit "> </i> Edit </a>
			| <a href="#" style="font-size:14px" onclick="hapus(`' . $val->id_agen . '`);" class=" "><i class="fa fa-trash"> </i> Delete </a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->agen->counts(),
            "recordsFiltered" => $this->agen->counts(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	
	function ajax_costumer()
	{
		$list = $this->agen->ajax_costumer();
        $data = array();
        $no = $_POST['start']+1;
        foreach ($list as $val) {
			$nama=$this->reff->getNamaPelanggan($val->id_costumer);
			$hp=$this->reff->getHpPelanggan($val->id_costumer);
			$email=$this->reff->getEmailPelanggan($val->id_costumer);
			$row = array();
			$row[] = $no++;
           // $row[] = "<a href='javascript:detail(`".$val->id_agen."`)'>".$val->kode_agen."</a>";
            $row[] = $this->tanggal->indjam($val->tgl,"/");
            $row[] = "<a href='javascript:detailCostumer(`".$nama."`,`".$hp."`,`".$email."`)'>".$nama."</a>";
            $row[] = $this->reff->getNamaTitleCostumer($val->id_title);
            $row[] = $val->ket;
           
            $row[] = '
			<a href="#" style="font-size:14px" onclick="edit(`' . $val->id.'`)" class=" "><i class="fa fa-edit "> </i> Edit </a>
			| <a href="#" style="font-size:14px" onclick="hapus(`' . $val->id . '`);" class=" "><i class="fa fa-trash"> </i> Hapus </a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->agen->counts_costumer(),
            "recordsFiltered" => $this->agen->counts_costumer(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	function ajax_listing()
	{
		$list = $this->agen->ajax_listing();
        $data = array();
        $no = $_POST['start']+1;
        foreach ($list as $val) {
			$row = array();
			$row[] = $no++;
           // $row[] = "<a href='javascript:detail(`".$val->id_agen."`)'>".$val->kode_agen."</a>";
            $row[] = $this->tanggal->indjam($val->tgl,"/");
            $row[] = "<a href='javascript:detailListing(`".$val->kode_listing."`)'>".$val->kode_listing."</a>";
            $row[] = $this->reff->getNamaTitleListing($val->id_title);
            $row[] = $val->ket;
           
            $row[] = '
			<a href="#" style="font-size:14px" onclick="edit(`' . $val->id.'`)" class=" "><i class="fa fa-edit "> </i> Edit </a>
			| <a href="#" style="font-size:14px" onclick="hapus(`' . $val->id . '`);" class=" "><i class="fa fa-trash"> </i> Hapus </a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->agen->counts_listing(),
            "recordsFiltered" => $this->agen->counts_listing(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	function insert()
	{
		echo $this->agen->insert();
	}
	function getKode()
	{
		echo $this->reff->kodeNetwork();	 
	}
	function update()
	{
		echo $this->agen->update();
	}
	function HapusAll()
	{
	echo $this->agen->HapusAll();
	}
	function hapus($id)
	{
	echo $this->agen->hapus($id);
	}
	
	function export()
	{
		$this->load->model("M_data_pegawai","pegawai");
		$this->pegawai->export_network();
	}
	
	function downloadFormat()
	{
	$this->agen->downloadFormat();
	}
	function importData()
	{
	$this->load->library("PHPExcel");
	 $data=$this->agen->importData();
		$data=explode("-",$data);
               ?><br><br>
			  <p style="color:green"><b>Import Data Selesai</b></p>
                <table class="tabel table-hover table-bordered" style="100%">
			       <?php 
				   if($data[0]){		   echo "<tr><td>Berhasil di simpan : ".$data[0]." kontak</td></tr>"; 			}?>
					<?php
					if($data[1]){	  	   echo "<tr><td>Diperbaharui : ".$data[1]." kontak</td></tr>";			} 
					if($data[2]){	  	   echo "<tr><td>Gagal di import : ".$data[2]." kontak</td></tr>";			} ?>
                </table>
                <?php
	}
	function cekpas()
	{
		$this->db->where("username",$this->input->post("user"));
		$this->db->where("password",$this->input->post("pass"));
	echo	$this->db->get("data_agen")->num_rows();
	}
	function getDetail($id)
	{
		$data["id"]=$id;
		$this->load->view("getDetail",$data);
	}
	function getEdit($id)
	{
		$data=$this->db->get_where("data_agen",array("id_agen"=>$id))->row();
		$isi=$data->jabatan."::"; //0
		$isi.=$data->nama."::"; //1
		$isi.=$data->jk."::"; //2
		$isi.=$this->tanggal->eng($data->tgl_lahir,"/")."::"; //3
		$isi.=$data->hp."::"; //4
		$isi.=$data->email."::"; //5
		$isi.=$data->alamat."::"; //6
		$isi.=$this->tanggal->eng($data->tgl_masuk_kerja,"/")."::"; //7
		$isi.=$data->bank."::"; //8
		$isi.=$data->no_rek."::"; //9
		$isi.=$data->atas_nama_bank."::"; //10
		$isi.=$data->nama_wakil."::"; //11
		$isi.=$data->nomor_wakil."::"; //12
		$isi.=$data->hubungan_wakil."::"; //13
		$isi.=$data->nama_pasangan."::"; //14
		$isi.=$this->tanggal->eng($data->tgl_lahir_pasangan,"/")."::"; //15
		$isi.=$data->anak1."::"; //16
		$isi.=$this->tanggal->eng($data->tgl_lahir_anak1,"/")."::"; //17
		$isi.=$data->anak2."::"; //18
		$isi.=$this->tanggal->eng($data->tgl_lahir_anak2,"/")."::"; //19
		$isi.=$data->anak3."::"; //20
		$isi.=$this->tanggal->eng($data->tgl_lahir_anak3,"/")."::"; //21
		$isi.=$data->anak4."::"; //22
		$isi.=$this->tanggal->eng($data->tgl_lahir_anak4,"/")."::"; //23
		$isi.=$data->anak5."::"; //24
		$isi.=$this->tanggal->eng($data->tgl_lahir_anak5,"/")."::"; //25
		$isi.=$data->username."::"; //26
		$isi.=$data->password."::"; //27
		$isi.=$data->upline."::"; //28
		$isi.=$data->kode_agen."::"; //29
		if(isset($data->poto)){ $poto=$data->poto; }else{ $poto="nopund.jpg";}
		$isi.=$poto."::"; //30
		$isi.=$this->reff->jumlahListing($data->kode_agen)."::"; //31
		$isi.=$this->reff->getNamaJabatan($data->jabatan)."::"; //32
		$isi.=$this->reff->jumlahSelling($data->kode_agen)."::"; //33
		$isi.=$this->reff->jmlPelanggan($data->kode_agen)."::"; //34
		$isi.=$this->tanggal->eng($data->tgl_habis_kontrak,"/")."::"; //35
		$isi.=$data->hp2."::"; //36
		$isi.=$data->status_profile."::"; //37
		
		$isi.=$data->fb."::"; //38
		$isi.=$data->twt."::"; //39
		$isi.=$data->ig."::"; //40
		$isi.=$data->bpjs."::"; //40
		echo $isi;
	}
}