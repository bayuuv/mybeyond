


<span class="create">
<form action="javascript:simpan()" id="form">
<span class="msg"></span>
<button class="  btn-primary pull-right"  > <i class="fa fa-plus-circle"></i> Simpan</button>
<button  class="btn-danger pull-right" onclick="return cancel()"> <i class="fa fa-plus-circle"></i> Cancel</button>
 <div class="row">
  
    <div class="col-lg-12">
        <div class="main-box clearfix" >
			<div class="main-box-body clearfix left">
			<span class="msg pull-right" style="margin-top:10px;margin-right:20px"></span>
		<a href="#"><h4><i class="fa fa-thumb-tack"></i> Website:: Posting baru</h4></a>
		
		<hr>
		  
		<table width="100%"  >
		<tr><td width="100px">
		<label for="inputEmail1" class=" black control-label"><b>Judul Artikel:</b></label>
	 </td><td align="left">
		<input type="text" required class="form-control col-md-9" name="judul" style="margin-top:-10px"/>
	 </td></tr></table>
	 
	 
		
<div class="clearfix"></div>
<br>
		<textarea id="textarea" required name="isi" style="height:250px;margin-top:10px"><span id="contents"></span></textarea>
		
		
	   </div>
     </div>
   </div>
  
 </div>
 </form>
</span>


 <span class="data_artikel">
<button class="btn-primary pull-right" onclick="add()"> <i class="fa fa-plus-circle"></i> Tambah Baru</button>
<div class="row">
    <div class="col-lg-12">
        <div class="main-box clearfix" >
			<div class="main-box-body clearfix ">
			<div class="table-responsive">
			<span style='position:absolute;margin-top:48px;z-index:222' class="cursor btnhapus">
			<a href="#" onclick="hapusAll()"><i class='fa fa-trash'></i> Hapus Terpilih</a>
			</span>
			<form action="#" name="delcheck" id="delcheck" class="form-horizontal" method="post">
			<table id='table' class="tabel black table-striped table-bordered table-hover dataTable" width="100%">
						<thead style="font-size:13px">			
							<th class='thead' axis="date" width='5px'><input type="checkbox" id="checkbox-1" class="pilihsemua" value="ya" /></th>
						<!--	<th class='thead' axis="string" width='15px'>No</th> -->
							<th class='thead' axis="date" width='10px'>No</th>
							<th class='thead' >JUDUL</th>
							<th class='thead' axis="string" >KONTEN</th>
							<th class='thead' axis="string" width="50px">TANGGAL POSTING</th>
							 
							<th width='90px'>&nbsp;</th>
						</thead>
			</table></form>
		</div>
	   </div>
     </div>
   </div>
 </div>
</span>

<?php echo $this->load->view("js/tabel.phtml");?>

  <script>
  $(".create").hide();
  function hapusAll()
	{	
		var con=window.confirm("hapus data terpilih ?");
		if(con==false){ return false; };
		$.ajax({
		url:"<?php echo base_url();?>data_agen/HapusAll",
		type: "POST",
		data: $('#delcheck').serialize(),
	//	dataType: "JSON",
		success: function(data)
				{	 $(".btnhapus").hide();
					$(".pilihsemua").removeAttr("checked");
					$(".pilihsemua").val("ya");
					table.ajax.reload(null,false); //reload datatable ajax 
				},
				error: function (jqXHR, textStatus, errorThrown)
				{
					alert('Try Again!');
				}
		});
	
	
	}
  
  
  $(".btnhapus").hide();
  	$(".pilihsemua").click(function(){
	
		if($(".pilihsemua").val()=="ya") {
		$(".pilih").prop("checked", "checked");
		$(".pilihsemua").val("no");
		  $(".btnhapus").show();
		} else {
		$(".pilih").removeAttr("checked");
		$(".pilihsemua").val("ya");
		  $(".btnhapus").hide();
		}
	
	});
	
	function pilcek(){
		$(".btnhapus").show();
		$(".pilihsemua").removeAttr("checked");
		$(".pilihsemua").val("ya");
		 
	};
  
  
  
		var table;
		table = $('#table').DataTable({ 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('website/ajax_artikel/')?>",
            "type": "POST",
        },
		
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
		    "targets": [ 0,1,2,3,4,5], //last column
          "orderable": false, //set not orderable
        },
        ],

      });

	  function hapus(id)
	  {
	    var tanya=window.confirm("Hapus ?");
		if(tanya==false){ return false;};
	  	$.ajax({
		url:"<?php echo base_url();?>website/hapus_artikel",
		type: "POST",
		data:"id="+id,
		success: function(data)
				{
				table.ajax.reload(null,false); //reload datatable ajax 
				},
			});
	  }
	  var method;
	  
	  function add()
	  {
				$('.msg').html("");
				$('#form')[0].reset();
				$(".data_artikel").hide();
				$(".create").show();
	  } function cancel()
	  {
				$(".data_artikel").show();
				$(".create").hide();
				return false;
	  }
	  function edit(data)
	  {
		  method="edit";
		  var isi=data.split("::");
		  $("#modalAdd").modal("show");
		  $(".title").html(" Edit Data Agen");
		  $("[name='nama']").val(isi[2]);
		  $("[name='hp']").val(isi[3]);
		  $("[name='email']").val(isi[4]);
		  $("[name='alamat']").val(isi[5]);
		  $("[name='id_agen']").val(isi[0]);
		 
	  }
	  
  </script>
  
  
  
  
  
  
   
<script type="text/javascript" src="<?php echo base_url()?>plug/tinymce/tinymce.min.js"></script>
  
<script>
    tinymce.init({
selector: "#textarea",
theme: "modern",
  plugins: [
      "preview wordcount, advlist, autolink, lists,code, link,image, charmap, print, preview, anchor, pagebreak searchreplace wordcount, visualblocks, visualchars, fullscreen, insertdatetime, media, nonbreaking, save, table, contextmenu, directionality, emoticons, paste, textcolor, colorpicker, textpattern,"
  ],
 toolbar: "undo redo | fontselect fontsizeselect | styleselect | bold italic  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | blockquote | forecolor backcolor emoticons | table | link image media | preview wordcount fullscreen",
   convert_urls: false,
  theme_advanced_font_sizes : "8px,10px,12px,14px,16px,18px,20px,24px,32px,36px",
  theme_advanced_fonts : "Arial=arial,helvetica,sans-serif;"+
                         "Arial Black=arial black,avant garde;"+
                         "Book Antiqua=book antiqua,palatino;"+
                         "Comic Sans MS=comic sans ms,sans-serif;"+
                         "Courier New=courier new,courier;"+
                         "Century Gothic=century_gothic;"+
                         "Georgia=georgia,palatino;"+
                         "Gill Sans MT=gill_sans_mt;"+
                         "Gill Sans MT Bold=gill_sans_mt_bold;"+
                         "Gill Sans MT BoldItalic=gill_sans_mt_bold_italic;"+
                         "Gill Sans MT Italic=gill_sans_mt_italic;"+
                         "Helvetica=helvetica;"+
                         "Impact=impact,chicago;"+
                         "Iskola Pota=iskoola_pota;"+
                         "Iskola Pota Bold=iskoola_pota_bold;"+
                         "Symbol=symbol;"+
                         "Tahoma=tahoma,arial,helvetica,sans-serif;"+
                         "Terminal=terminal,monaco;"+
                         "Times New Roman=times new roman,times;"+
                         "Trebuchet MS=trebuchet ms,geneva;"+
                         "Verdana=verdana,geneva",
 file_browser_callback: function(field, url, type, win) {
        tinyMCE.activeEditor.windowManager.open({
            file: '<?php echo base_url()?>plug/tinymce/kcfinder/browse.php?opener=tinymce4&field=' + field + '&type=' + type,// sesuikan direktory KCfinder
            title: 'KCFinder',
            width: 900,
            height: 450,
            inline: true,
            close_previous: false
        }, {
            window: win,
            input: field
        });
        return false;
    }
});
</script>
<!------------>

 <script>
	 function simpan(id)
	  {
		var isi=$("[name='isi']").val();
		var judul=$("[name='judul']").val();
		$('.msg').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Mohon Tunggu ... ");
	  	$.ajax({
		url:"<?php echo base_url();?>website/simpan_artikel/",
		type: "POST",
		data:"isi="+isi+"&judul="+judul,
		success: function(data)
				{
				$('.msg').html("<i class='fa fa-check-circle'></i> Tersimpan ... ");
				table.ajax.reload(null,false); //reload datatable ajax 
				cancel();
				$('#form')[0].reset();
				},
			});
	  }
</script>	

 <script>
	 function edit(id)
	 {
	  	$.ajax({
		url:"<?php echo base_url();?>website/get_artikel/"+id,
		dataType: "JSON",
		success: function(data)
				{
				add();
				var isi=$("[name='judul']").val(data.title);
				$("[name='isi']").html(data.content);
			
				},
			});
	  }
</script>	

  