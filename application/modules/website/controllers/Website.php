<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Website extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->m_konfig->validasi_session(array("user","operator","website","promotion"));
		$this->load->model("M_website","web");
	}
	
	function _template($data)
	{
	$this->load->view('template/main',$data);	
	}
	function getDataOwner()
	{
		$id=$this->db->query("select max(id_owner) as maxi from data_owner")->row();
                                        $ref_type = $this->reff->getOwner();
                                        $array_pemilik[""] = "==== Pilih ====";
                                        foreach ($ref_type as $val) {
                                            $array_pemilik[$val->id_owner] = $val->nama;
                                        }
                                        $data = $array_pemilik;
                                        echo form_dropdown('owner', $data, $id->maxi, '  id="owner" style="width:100%"');
                                        echo "<script>$('#owner').select2();</script>";
	}
	public function index()
	{

	$this->tentang_kami();
	}
	
	public function slider()
	{
	$data['konten']="slider";
	$this->_template($data);
	}
	public function artikel()
	{
	$data['konten']="artikel";
	$this->_template($data);
	}
	public function testimoni()
	{
	$data['konten']="testimoni";
	$this->_template($data);
	}
	public function tentang_kami()
	{

	$data['konten']="tentang";
	$data['isi']=$this->web->tentang_kami();
	$this->_template($data);
	}
	function simpan_tentang_kami()
	{
	echo	$this->web->simpan_tentang_kami();
	}
	function simpan_kontak()
	{
	echo	$this->web->simpan_kontak();
	}
	
	public function kontak()
	{

	$data['konten']="kontak";
	$data['isi']=$this->web->kontak();
	$this->_template($data);
	}
	function simpan_hubungi_kami()
	{
	echo	$this->web->simpan_tentang_kami();
	}
	function hapus_artikel()
	{
	echo	$this->web->hapus_artikel();
	}
	
	function hapus_testimoni()
	{
	echo	$this->web->hapus_testimoni();
	}
	function hapusAll($id)
	{
	echo $this->web->hapusAll($id);
	}function HapusAllTestimoni($id)
	{
	echo $this->web->HapusAllTestimoni($id);
	}
	function edit_artikel($id)
	{
	$echo=$this->web->get_artikel($id);
	$data['data']=$echo;
	$data['konten']="edit_artikel";
	$this->_template($data);
	}function edit_testimoni($id)
	{
	$echo=$this->web->get_testimoni($id);
	$data['data']=$echo;
	$data['konten']="edit_testimoni";
	$this->_template($data);
	}
	
	function simpan_artikel()
	{
	echo	$this->web->simpan_artikel();
	}
	function simpan_testimoni()
	{
	echo	$this->web->simpan_testimoni();
	}
	
	function update_artikel()
	{
	echo	$this->web->update_artikel();
	}
	function update_testimoni()
	{
	echo	$this->web->update_testimoni();
	}
	
	
	function ajax_artikel()
	{
		$list = $this->web->get_dataArtikel();
        $data = array();
        $no = $_POST['start']+1;
        foreach ($list as $val) {
			$row = array();
			$row[] =  '<input type="checkbox" class="pilih" onclick="pilcek()" name="hapus[]" value="'.$val->id.'">';
			//$row[] = $no++;
            $row[] = $no++;
            $row[] = "<img alt='no-image' src='".base_url()."file_upload/slider/".$val->gambar."'/>";
            $row[] = $val->title;
            $row[] = "<i>".substr($val->content,0,150)."</i>";
            $row[] = $val->tgl;
            $row[] = '
			<a href="'.base_url().'public_page/single_page/'.str_replace(" ","-",strip_tags(preg_replace('/[^A-Za-z0-9\-]/','-',$val->title))).'?id='.$val->id.'" target="_new" style="font-size:14px" ><i class="fa fa-eye "> </i> Lihat Artikel </a>
			| <a href="'.base_url().'website/edit_artikel/'.$val->id.'" style="font-size:14px"  class=" "><i class="fa fa-edit "> </i> Edit </a>
			| <a href="#" style="font-size:14px" onclick="hapus(`' . $val->id . '`);" class=" "><i class="fa fa-trash"> </i> Delete </a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->web->counts(),
            "recordsFiltered" => $this->web->counts(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	 
	 
	 function ajax_testimoni()
	{
		$list = $this->web->get_dataTestimmoni();
        $data = array();
        $no = $_POST['start']+1;
        foreach ($list as $val) {
			$row = array();
			$row[] =  '<input type="checkbox" class="pilih" onclick="pilcek()" name="hapus[]" value="'.$val->id.'">';
			//$row[] = $no++;
            $row[] = $no++;
            $row[] = "<img src='".base_url()."file_upload/img/".$val->poto."'></img>";
            $row[] = $val->nama;
            $row[] = $val->jabatan;
            $row[] = $val->perusahaan;
            $row[] = $val->bintang;
            $row[] = $val->testimoni;
      
            $row[] = '
			 <a href="'.base_url().'website/edit_testimoni/'.$val->id.'" style="font-size:14px"  class=" "><i class="fa fa-edit "> </i> Edit </a>
			| <a href="#" style="font-size:14px" onclick="hapus(`' . $val->id . '`);" class=" "><i class="fa fa-trash"> </i> Delete </a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->web->counts_t(),
            "recordsFiltered" => $this->web->counts_t(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	 
	 
	 
	function upload_slider($id)
	{
		$this->web->upload_slider($id);
		redirect("website/slider");
	}
	
	 
	 
	  
}