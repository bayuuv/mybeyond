
 <?php $data=$this->db->get_where("data_agen",array("id_agen"=>$id))->row(); ?>
 <?php 
 if($data->poto2 <> ''){
	$fc = 'Ada';
 }else{
	$fc = 'Tidak Ada';
 }
 ?>
 <div class="row" id="user-profile" style="padding:10px">
<div class="col-lg-4 col-md-4 col-sm-4">
<div class="main-box clearfix">
<header class="main-box-header clearfix">
<center><a href="#"><h2 id='d_nama'><?php echo $data->nama; ?></h2></a>
<?php if($data->jk=="l"){ echo "Mr."; $pasangan="Wife";}else{ echo "Mrs."; $pasangan="Husband";};?></center>
</header>
<div class="main-box-body clearfix black">
<?php if(isset($data->poto)){ $poto=$data->poto; }else{ $poto="nopund.jpg";} ?>
<div id="d_poto" class='thumbnails'><a href='<?php echo base_url() ?>file_upload/agen/<?php echo $poto; ?>' download><img style="max-width:150px" src="<?php echo base_url() ?>file_upload/agen/<?php echo $poto; ?>" alt="" class=" img-responsive center-block"/></a></div>
<div class="profile-label">
<span class="label label-danger" id="d_jabatan"><?php echo $this->reff->getNamaJabatan($data->jabatan)  ?></span>
</div>
 
<div class="profile-since" id="d_tgl_masuk_kerja">
Date of work entry :<?php echo $this->tanggal->eng($data->tgl_masuk_kerja,"/"); ?><br>
Contract expiration date :<?php echo $this->tanggal->eng($data->tgl_habis_kontrak,"/"); ?>
</div>
<div class="profile-details">
<ul class="fa-ul">
<li><i class="fa-li fa fa-list"></i>Listing: <?php echo $this->reff->jumlahListing($data->kode_agen); ?></li>
<li><i class="fa-li fa fa-shopping-cart"></i>Selling: <?php echo $this->reff->jumlahSelling($data->kode_agen) ?></li>
<li><i class="fa-li fa fa-child"></i>Costumer: <?php echo $this->reff->jmlPelanggan($data->kode_agen); ?></li>
<li><i class="fa-li fa fa-eye"></i>Factsheet Foto: <?php echo $fc; ?></li>
</ul>
</div>
<div class="profile-message-btn center-block text-center">
<?php if($data->upline){ echo "Upline: ".$this->reff->getNamaAgenById($data->upline); } ; ?>
</div>
</div>
</div>
<a href="#"><b>STATUS PFOFILE</b></a>
<textarea class="form-control" name="sp" onchange="saveStatus()"><?php echo $data->status_profile; ?></textarea>
</div>
<div class="col-lg-8 col-md-8 col-sm-8">
<div class="main-box clearfix">
<div class="tabs-wrapper profile-tabs">
<ul class="nav nav-tabs">
<li class="active"><a href="#tab-newsfeed" data-toggle="tab">Profile</a></li>
<li><a href="#tab-activity" data-toggle="tab">Family</a></li>
<li><a href="#tab-friends" data-toggle="tab">Listing Report</a></li>
<li><a href="#tab-chat" data-toggle="tab">Costumer Report</a></li>

 
</ul>
<div class="tab-content">
<div class="tab-pane fade in active" id="tab-newsfeed">
<div id="newsfeed">
<table class="tabel black table-striped table-bordered table-hover dataTable">
<tr> <td>Code </td><td>:</td><td><?php echo $data->kode_agen; ?></td> </tr>
<tr> <td>Born </td><td>:</td><td><?php echo $this->tanggal->hariLengkap($data->tgl_lahir,"/"); ?></td> </tr>

<?php
$hpo1=isset($data->hp)?($data->hp):"";
$hp1=substr($hpo1,0,4);	$hp2=substr($hpo1,4,4);	$hp3=substr($hpo1,8,4);
$hp1=$hp1."<b>.</b>".$hp2."<b>.</b>".$hp3;
?>

<tr> <td>Hp </td><td>:</td><td><?php echo $this->tanggal->septik($hp1); ?></td> </tr>
<tr> <td>E-mail </td><td>:</td><td><?php echo $data->email; ?></td> </tr>
<tr> <td>Address </td><td>:</td><td><?php echo $data->alamat; ?></td> </tr>
<tr> <td>Facebook </td><td>:</td><td><?php echo $data->fb; ?></td> </tr>
<tr> <td>Twitter </td><td>:</td><td><?php echo $data->twt; ?></td> </tr>
<tr> <td>Instagram </td><td>:</td><td><?php echo $data->ig; ?></td> </tr>
<tr> <td>BPJS </td><td>:</td><td><?php echo $this->tanggal->septik($data->bpjs); ?></td> </tr>
<tr> <td>Date of work entry </td><td>:</td><td><?php echo $this->tanggal->eng($data->tgl_masuk_kerja,"/") ?></td> </tr>
<tr> <td>Contract expiration date </td><td>:</td><td><?php echo $this->tanggal->eng($data->tgl_habis_kontrak,"/") ?></td> </tr>
<tr> <td>Bank </td><td>:</td><td><?php echo ucwords($data->bank);?></td> </tr>
<tr> <td>No.Rek </td><td>:</td><td><?php echo  $data->no_rek; ?></td> </tr>
<tr> <td>Name</td><td>:</td><td><?php echo $data->atas_nama_bank; ?></td> </tr>

<tr > <td colspan="3"><b>Emergency Contact</b> </td> </tr>
<tr> <td>Name </td><td>:</td><td><?php echo $data->nama_wakil; ?></td> </tr>
<tr> <td>Hp </td><td>:</td><td><?php echo $data->nomor_wakil; ?></td> </tr>
<tr> <td>Relationship </td><td>:</td><td><?php echo $data->hubungan_wakil; ?></td> </tr>

<!--tr> <td colspan="3">Kontak ke 2 yang dapat dihubungi </td> </tr>
<tr> <td>Nama </td><td>:</td><td><?php echo $data->nama_wakil; ?></td> </tr>
<tr> <td>Hp </td><td>:</td><td><?php echo $this->tanggal->septik($data->nomor_wakil); ?></td> </tr>
<tr> <td>hubungan </td><td>:</td><td><?php echo $data->hubungan_wakil; ?></td> </tr>-->
</table>
<hr>
<?php if(isset($data->ktp)){ $ktp=$data->ktp; }else{ $ktp="nopund.jpg";} ?>
<?php if(isset($data->kk)){ $kk=$data->kk; }else{ $kk="nopund.jpg";} ?>
<?php if(isset($data->npwp)){ $npwp=$data->npwp; }else{ $npwp="nopund.jpg";} ?>
<style>
.thumbnails img {
      max-width: 20%;
      padding: 5px;
      border: 1px solid #ccc;
      height: auto;
      background: #fff;
      box-shadow: 1px 1px 7px rgba(0,0,0,0.1);
    }
</style>	
<div class="col-lg-4 thumbnails"   align="center"><a href='<?php echo base_url() ?>file_upload/agen/<?php echo $ktp; ?>' download><center><b>KTP</b></center><img style="max-width:150px" src="<?php echo base_url() ?>file_upload/agen/<?php echo $ktp; ?>"/></a></div>
<div class="col-lg-4 thumbnails"   align="center"><a href='<?php echo base_url() ?>file_upload/agen/<?php echo $kk; ?>' download><center><b>KK</b></center><img style="max-width:150px" src="<?php echo base_url() ?>file_upload/agen/<?php echo $kk; ?>"/></a></div>
<div class="col-lg-4 thumbnails"   align="center"><a href='<?php echo base_url() ?>file_upload/agen/<?php echo $npwp; ?>' download><center><b>NPWP</b></center><img style="max-width:150px" src="<?php echo base_url() ?>file_upload/agen/<?php echo $npwp; ?>"/></a></div>



<hr>

</div>

</div>
<div class="tab-pane fade" id="tab-activity">
<div class="table-responsive">

<table class="tabel black table-striped table-bordered table-hover dataTable">
    <?php if(strlen($data->nama_pasangan)>3){?>
<tr> <td> <?php echo $pasangan; ?> Name</td><td>:</td><td><?php echo $data->nama_pasangan; ?></td> </tr>
<tr> <td>Born </td><td>:</td><td><?php echo $this->tanggal->hariLengkap($data->tgl_lahir_pasangan,"/"); ?></td> </tr>
<?php 
}
if($data->anak1)
{?>
<tr> <td>Child 1 </td><td>:</td><td><?php echo $data->anak1; ?></td> </tr>
<tr> <td>Born </td><td>:</td><td><?php echo $this->tanggal->hariLengkap($data->tgl_lahir_anak1,"/"); ?></td> </tr>
<?php } ?>

<?php 
if($data->anak2)
{?>
<tr> <td>Child 2 </td><td>:</td><td><?php echo $data->anak2; ?></td> </tr>
<tr> <td>Born </td><td>:</td><td><?php echo $this->tanggal->hariLengkap($data->tgl_lahir_anak2,"/"); ?></td> </tr>
<?php } ?>

<?php 
if($data->anak3)
{?>
<tr> <td>Child 3 </td><td>:</td><td><?php echo $data->anak3; ?></td> </tr>
<tr> <td>Born </td><td>:</td><td><?php echo $this->tanggal->hariLengkap($data->tgl_lahir_anak3,"/"); ?></td> </tr>
<?php } ?>

<?php 
if($data->anak4)
{?>
<tr> <td>Child 4 </td><td>:</td><td><?php echo $data->anak4; ?></td> </tr>
<tr> <td>Born </td><td>:</td><td><?php echo $this->tanggal->hariLengkap($data->tgl_lahir_anak4,"/"); ?></td> </tr>
<?php } ?>

<?php 
if($data->anak5)
{?>
<tr> <td>Child 5 </td><td>:</td><td><?php echo $data->anak5; ?></td> </tr>
<tr> <td>Born </td><td>:</td><td><?php echo $this->tanggal->hariLengkap($data->tgl_lahir_anak5,"/"); ?></td> </tr>
<?php } ?>


</table>


</div>
</div>

<style>
#tableListing th {
	 padding:5px;
}#tableCostumer th {
	 padding:5px;
}
</style>
<div class="tab-pane clearfix fade" id="tab-friends">
<div class="table-responsive">
<table id="tableListing" class="tabel black table-striped table-bordered table-hover dataTable">
<thead>	
<th>No</th>
<th>Entry Date</th>
<th>Code</th>
<th>Kategori</th>
<th>Description</th>
</thead>
</table>
</div>
</div>


<div class="tab-pane fade" id="tab-chat">

 
<div class="table-responsive">
<table id="tableCostumer" class="tabel black table-striped table-bordered table-hover dataTable">
<thead>	
<th>No</th>
<th>Date Entry</th>
<th>Costumer</th>
<th>Kategori</th>
<th>Description</th>
</thead>
</table>
</div>

</div>

 


</div>
</div>
</div>
</div>
</div>


<script src="<?php echo base_url('plug/dataTables/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('plug/dataTables/js/dataTables.bootstrap.js')?>"></script>

  <script> 
		var table;
		table = $('#tableCostumer').DataTable({ 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('data_agen/ajax_costumer/'.$id.'')?>",
            "type": "POST",
        },
		
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
		    "targets": [ 0,1], //last column
          "orderable": false, //set not orderable
        },
        ],

      });

  </script>
  
   <script> 
		var table;
		table = $('#tableListing').DataTable({ 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('data_agen/ajax_listing/'.$id.'')?>",
            "type": "POST",
        },
		
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
		    "targets": [ 0,1], //last column
          "orderable": false, //set not orderable
        },
        ],

      });

	
  </script>
  <script>
  function saveStatus()
  {
	  var s=$("[name='sp']").val();
	            $.ajax({
                url: "<?php echo base_url() ?>data_property/saveStatus/?s="+s+"&id=<?php echo $id;?>",
                success: function (data) {
		            
		              }
            });
}
  </script>