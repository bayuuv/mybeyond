<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Data_agen extends CI_Controller {

	

	function __construct()
	{
		parent::__construct();	
		$this->m_konfig->validasi_session(array("user","agen","operator","designer","dm","network"));
		$this->load->model("M_data_agen","agen");
	}
	
	function _template($data)
	{
	$this->load->view('template/main',$data);	
	}
	
	public function index()
	{

	$data['konten']="index";
	$this->_template($data);
	}
	function setAktifasi()
	{
	$this->db->set("aktifasi",$this->input->get_post("data"));
	$this->db->where("id_agen",$this->input->get_post("id"));
	echo $this->db->update("data_agen");	
	}
	function ajax_agen()
	{
		$list = $this->agen->get_dataAgen();
        $data = array();
        $no = $_POST['start']+1;$jk="";
        foreach ($list as $val) {
			if($val->jk=="l"){
				$jk="Mr.";
			}
			if($val->jk=="p"){
				$jk="Mrs.";
			}
			
			$tglsekarang = date("Y-m-d");
			$tgl_today = strtotime($tglsekarang);
			$tgl_kontrak = strtotime($val->tgl_habis_kontrak);
			$selisih = $tgl_kontrak - $tgl_today;
			$hari = floor($selisih/(60*60*24));
			
			if($hari <= 30 and $val->aktifasi == '0'){
				$tampilnama = "<font color='red'>".$jk."  ".$val->nama."</font>  ";
				$tampiljabatan = "<font color='red'>".$this->reff->getNamaJabatan($val->jabatan)."</font>  ";
			}elseif($hari <= 30 and $val->aktifasi == '1'){
				$tampilnama = "<font color='green'>".$jk."  ".$val->nama."</font>  ";
				$tampiljabatan = "<font color='green'>".$this->reff->getNamaJabatan($val->jabatan)."</font>  "; 
			}else{
				$tampilnama = "".$jk."  ".$val->nama."";
				$tampiljabatan = "".$this->reff->getNamaJabatan($val->jabatan)." ".$hari.""; 
			}
			
			$row = array();
			$row[] =  '<input type="checkbox" class="pilih" onclick="pilcek()" name="hapus[]" value="'.$val->id_agen.'">';
			$opsi="";
			$opsi[1]="ACTIVE";
			$opsi[0]="NOT ACTIVE";
				
			$row[]=form_dropdown('aktifasi'.$val->id_agen.'', $opsi, $val->aktifasi, 'onchange="setAktifasi(`'.$val->id_agen.'`)" class="form-control"');
			
			//$row[] = $no++;
            $row[] = "<a href='javascript:detail(`".$val->id_agen."`)'>".$val->kode_agen."</a>";
            $row[] = $tampilnama;
            $row[] = $tampiljabatan;
            $row[] = $this->tanggal->septik($val->hp);
            $row[] = $val->email;
            $row[] = $val->alamat;
            $row[] = $this->reff->jumlahListing($val->kode_agen);
            $row[] = $this->reff->jumlahSelling($val->kode_agen);
            $row[] = $this->reff->jmlPelanggan($val->kode_agen);
			$row[] = $val->last_login;
            $row[] = '
			<a href="#" style="font-size:14px" onclick="edit(`' . $val->id_agen.'`)" class=" "><i class="fa fa-edit "> </i> Edit </a>
			| <a href="#" style="font-size:14px" onclick="hapus(`' . $val->id_agen . '`);" class=" "><i class="fa fa-trash"> </i> Delete </a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->agen->counts(),
            "recordsFiltered" => $this->agen->counts(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	function getMemberNetwork($id)
	{
		$this->db->where("id_network",$id);
	$data=$this->db->get("data_property")->row();
	$kode_agen=isset($data->agen)?($data->agen):"";
	return $this->reff->getNamaAgenByKode($kode_agen);
	}
	function ajax_network()
	{
		$list = $this->agen->get_dataNetwork();
        $data = array();
        $no = $_POST['start']+1;
        foreach ($list as $val) {
				$row = array();
			if($val->jk=="p"){
			$jk="Mrs.";
			}elseif($val->jk=="l"){
				$jk="Mr.";
			}else{
				$jk="";
			}
			
			
		
			$row[] =  '<input type="checkbox" class="pilih" onclick="pilcek()" name="hapus[]" value="'.$val->id_agen.'">';
			$row[] = $no++;
           $row[] = "<a href='javascript:detail(`".$val->id_agen."`)'>".$val->kode_agen."</a>";
           
            //$row[] = $jk." ".$val->nama;
			$row[] = $val->nama;
            $row[] = $this->tanggal->septik($val->hp);
            $row[] = $val->email;
            $row[] = $val->alamat;
            //$row[] = $this->getMemberNetwork($val->id_agen);
			$row[] = $this->reff->getNamaAgenId($val->upline);
			$row[] = $val->last_login;
           
            $row[] = '
			<a href="#" style="font-size:14px" onclick="edit(`' . $val->id_agen.'`)" class=" "><i class="fa fa-edit "> </i> Edit </a>
			| <a href="#" style="font-size:14px" onclick="hapus(`' . $val->id_agen . '`);" class=" "><i class="fa fa-trash"> </i> Delete </a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->agen->countsN(),
            "recordsFiltered" => $this->agen->countsN(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	
	function ajax_costumer()
	{
		$list = $this->agen->ajax_costumer();
        $data = array();
        $no = $_POST['start']+1;
        foreach ($list as $val) {
			$nama=$this->reff->getNamaPelanggan($val->id_costumer);
			$hp=$this->reff->getHpPelanggan($val->id_costumer);
			$email=$this->reff->getEmailPelanggan($val->id_costumer);
			$row = array();
			$row[] = $no++;
           // $row[] = "<a href='javascript:detail(`".$val->id_agen."`)'>".$val->kode_agen."</a>";
            $row[] = $this->tanggal->indjam($val->tgl,"/");
            $row[] = "<a href='javascript:detailCostumer(`".$nama."`,`".$hp."`,`".$email."`)'>".$nama."</a>";
            $row[] = $this->reff->getNamaTitleCostumer($val->id_title);
            $row[] = $val->ket;
           
            $row[] = '
			<a href="#" style="font-size:14px" onclick="edit(`' . $val->id.'`)" class=" "><i class="fa fa-edit "> </i> Edit </a>
			| <a href="#" style="font-size:14px" onclick="hapus(`' . $val->id . '`);" class=" "><i class="fa fa-trash"> </i> Hapus </a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->agen->counts_costumer(),
            "recordsFiltered" => $this->agen->counts_costumer(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	function ajax_listing()
	{
		$list = $this->agen->ajax_listing();
        $data = array();
        $no = $_POST['start']+1;
        foreach ($list as $val) {
			$row = array();
			$row[] = $no++;
           // $row[] = "<a href='javascript:detail(`".$val->id_agen."`)'>".$val->kode_agen."</a>";
            $row[] = $this->tanggal->indjam($val->tgl,"/");
            $row[] = "<a href='javascript:detailListing(`".$val->kode_listing."`)'>".$val->kode_listing."</a>";
            $row[] = $this->reff->getNamaTitleListing($val->id_title);
            $row[] = $val->ket;
           
            $row[] = '
			<a href="#" style="font-size:14px" onclick="edit(`' . $val->id.'`)" class=" "><i class="fa fa-edit "> </i> Edit </a>
			| <a href="#" style="font-size:14px" onclick="hapus(`' . $val->id . '`);" class=" "><i class="fa fa-trash"> </i> Hapus </a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->agen->counts_listing(),
            "recordsFiltered" => $this->agen->counts_listing(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	function insert()
	{
		echo $this->agen->insert();
	}
	function getKodeAgen()
	{
		echo $this->agen->getKodeAgen();
	}
	function update()
	{
		echo $this->agen->update();
	}
	function HapusAll()
	{
	echo $this->agen->HapusAll();
	}
	function hapus($id)
	{
	echo $this->agen->hapus($id);
	}
	
	function export()
	{
		$type=$this->input->get("type");
		if($type=="member")
		{
		$this->agen->export_member();
		}
		
	}
	
	function downloadFormat()
	{
	$this->agen->downloadFormat();
	}
	function importData()
	{
	$this->load->library("PHPExcel");
	 $data=$this->agen->importData();
		$data=explode("-",$data);
               ?><br><br>
			  <p style="color:green"><b>Import Data Selesai</b></p>
                <table class="tabel table-hover table-bordered" style="100%">
			       <?php 
				   if($data[0]){		   echo "<tr><td>Berhasil di simpan : ".$data[0]." kontak</td></tr>"; 			}?>
					<?php
					if($data[1]){	  	   echo "<tr><td>Diperbaharui : ".$data[1]." kontak</td></tr>";			} 
					if($data[2]){	  	   echo "<tr><td>Gagal di import : ".$data[2]." kontak</td></tr>";			} ?>
                </table>
                <?php
	}
	function cekpas()
	{
		$this->db->where("username",$this->input->post("user"));
		$this->db->where("password",md5($this->input->post("pass")));
	$echo1=$this->db->get("admin")->num_rows();

	$this->db->where("username",$this->input->post("user"));
		$this->db->where("password",$this->input->post("pass"));
	$echo2=$this->db->get("data_agen")->num_rows();
	echo $echo1+$echo2;
	}
	function getDetail($id)
	{
		$data["id"]=$id;
		$this->load->view("getDetail",$data);
	}
}