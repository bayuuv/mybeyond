<!--<button class="btn-primary pull-right"  onclick="importData()"> <i class="fa fa-download"></i> Import</button>-->
<button class="btn-warning pull-right" onclick="exportData()">  <i class="fa fa-upload"></i> Export</button>
<button class="btn-danger pull-right" onclick="add()"> <i class="fa fa-plus-circle"></i> Add</button>
<div class="row">
    <div class="col-lg-12">
        <div class="main-box clearfix" >
			<div class="main-box-body clearfix ">
			<div class="table-responsive">
			<span style='position:absolute;margin-top:48px;z-index:222' class="cursor btnhapus">
			<a href="#" onclick="hapusAll()"><i class='fa fa-trash'></i> Hapus Terpilih</a>
			</span>
			<form action="#" name="delcheck" id="delcheck" class="form-horizontal" method="post">
			<table id='table' class="tabel black table-striped table-bordered table-hover dataTable" width="100%">
						<thead style="font-size:13px">			
							<th class='thead' axis="date" width='5px'><input type="checkbox" id="checkbox-1" class="pilihsemua" value="ya" /></th>
						<!--	<th class='thead' axis="string" width='15px'>No</th> -->
							<th class='thead' axis="date" width='20px'>NO</th>
							<th class='thead' >NAME</th>
							 
							<th class='thead' axis="string" >HP</th>
							<th class='thead' axis="string" >EMAIL </th>
							<th class='thead' axis="string" >ADRESS </th>
							<th width='90px'>&nbsp;</th>
						</thead>
			</table></form>
		</div>
	   </div>
     </div>
   </div>
 </div>


<?php echo $this->load->view("js/tabel.phtml");?>

  <script>
  function hapusAll()
	{	
		var con=window.confirm("hapus data terpilih ?");
		if(con==false){ return false; };
		$.ajax({
		url:"<?php echo base_url();?>data_owner_part/HapusAll",
		type: "POST",
		data: $('#delcheck').serialize(),
	//	dataType: "JSON",
		success: function(data)
				{	 $(".btnhapus").hide();
					$(".pilihsemua").removeAttr("checked");
					$(".pilihsemua").val("ya");
					table.ajax.reload(null,false); //reload datatable ajax 
				},
				error: function (jqXHR, textStatus, errorThrown)
				{
					alert('Try Again!');
				}
		});
	
	
	}
  
  
  $(".btnhapus").hide();
  	$(".pilihsemua").click(function(){
	
		if($(".pilihsemua").val()=="ya") {
		$(".pilih").prop("checked", "checked");
		$(".pilihsemua").val("no");
		  $(".btnhapus").show();
		} else {
		$(".pilih").removeAttr("checked");
		$(".pilihsemua").val("ya");
		  $(".btnhapus").hide();
		}
	
	});
	
	function pilcek(){
		$(".btnhapus").show();
		$(".pilihsemua").removeAttr("checked");
		$(".pilihsemua").val("ya");
		 
	};
  
  
  
		var table;
		table = $('#table').DataTable({ 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('data_owner_part/ajax_owner/')?>",
            "type": "POST",
        },
		
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
		    "targets": [ 0,1,2,3,4,5], //last column
          "orderable": false, //set not orderable
        },
        ],

      });

	  
	  var method;
	 
	  
	  getKodeAgen()
	  function getKodeAgen()
	  {
		  $.ajax({
		url:"<?php echo base_url();?>data_owner_part/getKodeAgen",
		type: "POST",
		success: function(data)
				{
				$("[name='kode']").val(data);
				},
			});
	  }
  </script>
  
  <form action="javascript:saveAdd()" id="formulirAgen"   method="post" >	
 <!-- Bootstrap modal -->
  <div class="modal fade" id="modalAdd" role="dialog" >
  <div class="modal-dialog"  style="width:80%" >
 
<div class="modal-content" >
     
      <div class="modal-body form" >
   
<section class="content">

  <div class="row">
    <div class="col-lg-12">

	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-plus-circle"></i><span class='title'></span></h4>
      </div>
	<br>
<script>
   $("document").ready(function () {
$(".akun").hide();
   });
function getAkun()
{
	var jab=$("[name='jabatan']").val();
	if(jab==2)
	{
		$(".akun").show();
	}else{
		$(".akun").hide();
	}
}
</script>
<div class="row">
<div class="col-lg-12">
<div class="main-box clearfix">
<div >
<div >
 
<div class="tab-content">
<div  >

<div class="form-horizontal black col-md-6">


<input type="hidden"  id="kode" name="kode"  >

 

<div class="form-group">
<label for="nama" class="b col-lg-2 control-label">Nama</label>
<div class="col-lg-9">
<input type="text" class="form-control" id="nama" name="nama"  >
<input type="hidden" id="id_agen" name="id_agen">
</div>
</div>

<div class="form-group">
<label for="nama" class="b col-lg-2 control-label">Jenis Kelamin</label>
<div class="col-lg-9">
<div class="radio">
<input type="radio" onclick="clikL()" id="l" name="jk" value="l"/>
<label for="l">
Laki-laki
</label>
 
<input type="radio" onclick="clikP()"  id="p" name="jk" value="p"/>
<label for="p">
Perempuan
</label>
</div>
</div>
</div>

 


<div class="form-group">
<label for="hp1" class="col-lg-2 control-label b">Hp1</label>
<div class="col-lg-9">
<input type="text" class="form-control" id="hp1" name="hp1"  >
</div>
</div>


<div class="form-group">
<label for="hp2" class="col-lg-2 control-label b">Hp2</label>
<div class="col-lg-9">
<input type="text" class="form-control" id="hp2" name="hp2"  >
</div>
</div>


<div class="form-group">
<label for="email" class="b col-lg-2 control-label">E-mail</label>
<div class="col-lg-9">
<input type="text" class="form-control" id="email" name="email" >
</div>
</div>

<div class="form-group">
<label for="alamat" class="b col-lg-2 control-label">Alamat</label>
<div class="col-lg-9">
<textarea name="alamat" id="alamat" class="form-control"></textarea>
</div>
</div>
</div>


<div class="form-horizontal black col-md-6">
  

<div class="form-group">
<label for="poto" class="b col-lg-2 control-label">Photo Profile</label>
<div class="col-lg-9">
<input type="file" class="form-control" id="poto" name="poto"  >
</div>
</div>

<div class="form-group">
<label for="ktp" class="b col-lg-2 control-label">Scand KTP</label>
<div class="col-lg-9">
<input type="file" class="form-control" id="ktp" name="ktp"  >
</div>
</div>

<div class="form-group">
<label for="kk" class="b col-lg-2 control-label">Scand KK</label>
<div class="col-lg-9">
<input type="file" class="form-control" id="kk" name="kk"  >
</div>
</div>


<div class="form-group">
<label for="email" class="b col-lg-2 control-label">Scand NPWP</label>
<div class="col-lg-9">
<input type="file" class="form-control" id="npwp" name="npwp" >
</div>
</div>




</div>





</div>
 
<div class="tab-pane fade" id="keluarga">

<div class="form-horizontal black col-md-12">
<div class="form-group">
<label for="nama_pasangan" class="b col-lg-2 control-label"><span class='pasangan'>Nama Pasangan</span></label>
<div class="col-lg-9">
<input type="text" class="form-control" id="nama_pasangan" name="nama_pasangan"  >
</div>
</div>

<div class="form-group">
<label for="tgl_lahir_pasangan" class="b col-lg-2 control-label"> Tgl Lahir </label>
<div class="col-lg-9">
<input type="text" class="form-control" id="maskedDate4" name="tgl_lahir_pasangan"  placeholder="contoh:31/12/1990" >
</div>
</div>

<div class="form-group">
<label for="anak1" class="b col-lg-2 control-label">Nama Anak 1</label>
<div class="col-lg-9">
<input type="text" class="form-control" id="anak1" name="anak1"  >
</div>
</div>


<div class="form-group">
<label for="tgl_lahir_anak1" class="b col-lg-2 control-label">Tgl lahir </label>
<div class="col-lg-9">
<input type="text" class="form-control" id="maskedDate2" name="tgl_lahir_anak1"  placeholder="contoh:31/12/1990" >
</div>
</div>



<div class="form-group">
<label for="anak1" class="b col-lg-2 control-label">Nama Anak 2</label>
<div class="col-lg-9">
<input type="text" class="form-control" id="anak2" name="anak2"  >
</div>
</div>


<div class="form-group">
<label for="tgl_lahir_anak2" class="b col-lg-2 control-label">Tgl lahir </label>
<div class="col-lg-9">
<input type="text" class="form-control" id="maskedDate3" name="tgl_lahir_anak2"   placeholder="contoh:31/12/1990">
</div>
</div>





</div>


</div>
<div class="tab-pane fade" id="akun">

<div class="form-horizontal black col-md-12">
<div class="form-group password">
<label for="username" class="b col-lg-3 control-label">Username </label>
<div class="col-lg-8">
<input type="text" class="form-control" id="username"  onkeyup="cekpass()" name="username"   >
</div>
</div>

<div class="form-group password">
<label for="password" class="b col-lg-3 control-label">Password</label>
<div class="col-lg-8">
<input type="text" class="form-control" id="password" onkeyup="cekpass()" name="password"   >
<span class="help-block"></span>
</div>
</div>

<script src="<?php echo base_url();?>plug/boostrap/js/jquery.maskedinput.min.js"></script>  
<script>
$("#maskedDate1").mask("99/99/9999");
$("#maskedDate2").mask("99/99/9999");
$("#maskedDate3").mask("99/99/9999");
$("#maskedDate4").mask("99/99/9999");
$("#maskedDate5").mask("99/99/9999");
$("#tgl").mask("99/99/9999");
 
</script>



<script>
function cekpass()
{
	var user=$("[name='username']").val();
	var pass=$("[name='password']").val();
	
	 	$.ajax({
		url:"<?php echo base_url();?>data_owner_part/cekpas",
		type: "POST",
		data:"user="+user+"&pass="+pass,
		success: function(data)
				{
					if(data>0)
					{
						$(".password").addClass("has-error");
						$(".help-block").html("Silahkan ganti username dan password anda");
						document.getElementById("submit").disabled = true;
					}else{
						$(".password").removeClass("has-error");
						$(".help-block").html("");
						document.getElementById("submit").disabled = false;
					}
				},
			});
}
</script>

</div>



</div>

</div>
</div>
</div>
</div>
</div>
</div>

    </div><span class='load'></span>
<button type="submit" onclick="saveAdd()" id="submit" class="btn btn-success pull-right"><i class='fa fa-save'></i> Simpan</button>
  </div>   <!-- /.row -->

</section><!-- /.content -->
  </div>
   </div><!-- /.modal-content -->
		

      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form>
  <!-- End Bootstrap modal -->
  

<script>
function clikL()
{
	$(".pasangan").html("Nama Istri");
}
function clikP()
{
	$(".pasangan").html("Nama Suami");
}
</script>



<?php echo $this->load->view("js/form.phtml"); ?>
 

 <script>
  
	 function hapus(id)
	  {
	    var tanya=window.confirm("Hapus ?");
		if(tanya==false){ return false;};
	  	$.ajax({
		url:"<?php echo base_url();?>data_owner_part/hapus/"+id,
		type: "POST",
		data:"id="+id,
		success: function(data)
				{
				table.ajax.reload(null,false); //reload datatable ajax 
				},
			});
	  }

</script>	



<script>
function exportData()
{
	window.location.href="<?php echo base_url()?>data_owner_part/export";
}
function importData()
{
	 $('.msg').html('');
	  $('.hasil').html('');
	$("#modalImport").modal("show");
}
</script>






 <!-- Bootstrap modal -->
  <div class="modal fade" id="modalImport" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
     
      <div class="modal-body form">
<section class="content">
  <div class="row">
    <div class="col-lg-12">
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-download"></i> Import Data Agen <span class='namaGroup'></span></h4>
      </div>
      <!-- general form elements -->
      <div class="box box-primary black">	   <br>
        Silahkan <a href='<?php echo base_url();?>data_owner_part/downloadFormat'><i class="fa fa-file-excel-o"></i> download format</a> sebelum upload.
          <div class="box-body">                      
            <form role="form" name="uploadfilexl" id="uploadfilexl" action="javascript:void();" method="post" enctype="multipart/form-data">
                <div class="form-group">
                  <input id="userfile" required name="userfile" type="file" class="form-control">
                  <input  name="idGroup" type="hidden">
                </div>				
                <button type="submit" onclick="javascript:simpanfile();" class="btn btn-primary pull-right">
                    <span class="fa fa-upload"></span>&nbsp;Upload
                </button>
                <div class="form-group">
                    <div class="msg"></div>
                    <div class="hasil"></div>
                </div>
            </form>
          </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div>   <!-- /.row -->
</section><!-- /.content -->
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
  


  
 <div class="modal fade" id="modalDetail" role="dialog" >
  <div class="modal-dialog"  style="width:80%" >

<div class="modal-content" >
<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-info-circle"></i> Informasi Detail</h4>
      </div>
	  <!----------------------------------------------->
	 <div id="kontenDetail"></div>
	  <!----------------------------------------------->
</div>	  
</div>	  
</div>	  

  


  <script type="text/javascript">
  var f=jQuery.noConflict();
function simpanfile(){
    var userfile=$('#userfile').val();
    f('#uploadfilexl').ajaxForm({
     url:'<?php echo base_url();?>data_owner_part/importData/',
     type: 'post',
     data:{"userfile":userfile},
     beforeSend: function() {
        var percentVal = 'Mengupload 0%';
        f('.msg').html(percentVal);
     },
     uploadProgress: function(event, position, total, percentComplete) {
        var percentVal = 'Mengupload ' + percentComplete + '%';
        f('.msg').html(percentVal);
     },
     beforeSubmit: function() {
      f('.hasil').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Silahkan Tunggu ... ");
     },
     complete: function(xhr) {
        f('.msg').html('');
     }, 
     success: function(resp) {
        f('.hasil').html(resp);
		table.ajax.reload(null,false);
		f("#uploadfilexl")[0].reset();
     },
    });     
};


</script>   


<script>
function saveAdd()
	{	
	 f(".load").html('<img src="<?php echo base_url()?>plug/img/load.gif"> Process Simpan...');
	if(method=="edit")
	{
		
		var url="<?php echo base_url();?>data_owner_part/update";
	}else{
		var url="<?php echo base_url();?>data_owner_part/insert";
	}
		f(".load").html("<img src='<?php echo base_url();?>plug/img/load.gif'> Please wait...");
		f('#formulirAgen').ajaxForm({
		url:url,
		type: "post",
		data: f('#formulirAgen').serialize(),
	//	dataType: "JSON",
		success: function(data)
				{
				if(data==false){ alert("Gagal! Data sudah ada pada database"); f(".load").html(""); f("[name='group']").focus(); return false;}
				f(".load").html('<font color="green"><i class="fa fa-check-circle fa-fw fa-lg"></i> Berhasil di simpan</font>');
				table.ajax.reload(null,false);
				f("#formulirAgen")[0].reset();
				//f(".load").html("");
				if(method=="edit")
				{
					f("#modalAdd").modal("hide");
					f(".load").html('');
				}
				},
				
		});
	}
	 function add()
	  {
				f("#formulirAgen")[0].reset();
				method="add";
				f("#modalAdd").modal("show");
				f(".title").html(" Tambah Data Pegawai");
				//getKodeAgen();
	  }
	  function edit(id)
	  {
		
		  method="edit";
		  f("#modalAdd").modal("show");
			 	f.ajax({
				url:"<?php echo base_url();?>data_owner_part/getEdit/"+id,
				success: function(data)
						{		
				  var isi=data.split("::");
				  // alert(isi[3]);
				  f(".title").html(" Edit Data Owner");
				  
				  f("[name='nama']").val(isi[0]);
				  f("[name='hp1']").val(isi[2]);
				  f("[name='hp2']").val(isi[3]);
				  f("[name='email']").val(isi[4]);
				  f("[name='alamat']").val(isi[5]);
				  f("[name='kode']").val(id);
				  
				  f('input:radio[name=jk][value='+isi[1]+']')[0].checked = true;	
				},
			});
		 
		
	  }
</script>	


  <script>
//var f=jQuery.noConflict();
	 function detail(id)
	  {
	//	getAkun();
		  f("#modalDetail").modal("show");
			 	f.ajax({
				url:"<?php echo base_url();?>data_owner_part/getDetail/"+id,
				success: function(data)
						{			
						f("#kontenDetail").html(data);
				},
			});
		 
		
	  }
</script>


