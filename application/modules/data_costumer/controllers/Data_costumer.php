<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Data_costumer extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->m_konfig->validasi_session(array("agen"));
		$this->load->model("M_data_costumer","owner");
	}
	
	function _template($data)
	{
	$this->load->view('template/main',$data);	
	}
	function getDataOwner()
	{
		$id=$this->db->query("select max(id_pelanggan) as maxi from data_owner")->row();
                                        $ref_type = $this->reff->getOwner();
                                        $array_pemilik[""] = "==== Pilih ====";
                                        foreach ($ref_type as $val) {
                                            $array_pemilik[$val->id_pelanggan] = $val->nama;
                                        }
                                        $data = $array_pemilik;
                                        echo form_dropdown('owner', $data, $id->maxi, '  id="owner" style="width:100%"');
                                        echo "<script>$('#owner').select2();</script>";
	}
	public function index()
	{

	$data['konten']="index";
	$this->_template($data);
	}
	function ajax_agen()
	{
		$list = $this->owner->get_dataAgen();
        $data = array();
        $no = $_POST['start']+1;$jk="";
        foreach ($list as $val) {
			if($val->jk=="l")
			{
				$jk="Mr.";
			}
			if($val->jk=="l")
			{
				$jk="Mrs.";
			}
			$row = array();
			$row[] =  '<input type="checkbox" class="pilih" onclick="pilcek()" name="hapus[]" value="'.$val->id_pelanggan.'">';
			//$row[] = $no++;
            $row[] = '<a href="#" style="font-size:14px" onclick="detail(`'.$val->id_pelanggan.'`)">'.$val->nama.'</a>';
            $row[] = $jk;
            $row[] = $val->hp;
            $row[] = $val->email;
            $row[] = $val->ket;
            $row[] = '
			<a href="#" style="font-size:14px" onclick="detail(`' . $val->id_pelanggan.'`)" class=" "><i class="fa fa-list "> </i> Progress </a>
			| <a href="#" style="font-size:14px" onclick="edit(`' . $val->id_pelanggan . '::' . $val->nama . '::' . $val->hp . '::' . $val->email . '::' . $val->ket . '::' . $val->jk . '`)" class=" "><i class="fa fa-edit "> </i> Edit </a>
			| <a href="#" style="font-size:14px" onclick="hapus(`' . $val->id_pelanggan . '`);" class=" "><i class="fa fa-trash"> </i> Delete </a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->owner->counts(),
            "recordsFiltered" => $this->owner->counts(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	
	
	function insert()
	{
		echo $this->owner->insert();
	}
	function update()
	{
		echo $this->owner->update();
	}
	function HapusAll()
	{
	echo $this->owner->HapusAll();
	}
	function hapus($id)
	{
	echo $this->owner->hapus($id);
	}
	
	function export()
	{
		$this->owner->export();
	}
	
	function downloadFormat()
	{
	$this->owner->downloadFormat();
	}
	function importData()
	{
	$this->load->library("PHPExcel");
	 $data=$this->owner->importData();
		$data=explode("-",$data);
               ?><br><br>
			  <p style="color:green"><b>Import Data Selesai</b></p>
                <table class="tabel table-hover table-bordered" style="100%">
			       <?php 
				   if($data[0]){		   echo "<tr><td>Berhasil di simpan : ".$data[0]." data</td></tr>"; 			}?>
					<?php
					if($data[1]){	  	   echo "<tr><td>Diperbaharui : ".$data[1]." data</td></tr>";			} 
					if($data[2]){	  	   echo "<tr><td>Gagal di import : ".$data[2]." data</td></tr>";			} ?>
                </table>
                <?php
	}
	function getDataDetail($id)
	{
		$data["id"]=$id;
		$this->load->view("getListing",$data);
	}
	
	
	function ajax_property($id)
	{
		$list = $this->owner->get_dataProperty($id);
        $data = array();
        $no = $_POST['start']+1;
        foreach ($list as $val) {
			$status=$val->status;
			if($status)
			{
				$status="Terjual/Tersewa";
			}else{
				$status="Belum Laku";
			}
			$row = array();
		 
            $row[] = "<a href='javascript:details(`".$val->kode_prop."`)'>".$val->kode_prop."</a>";
            $row[] = $this->reff->getNamaJenis($val->jenis_prop)." ".$val->type_jual;
         
            $row[] = number_format($val->harga,0,",",".");
            $row[] = $val->nama_area." - ".$val->alamat_detail;
            $row[] = $this->reff->getNamaOwner($val->id_pelanggan);
            $row[] = $this->reff->getNamaAgen($val->agen);
            $row[] = "Diupload : ".$this->reff->cekJumlahGambar($val->id_prop)."   <br>Factsheet :".$this->reff->cekGambarDesain($val->id_prop);
     
			$row[] = $status;
          
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->owner->countsList($id),
            "recordsFiltered" => $this->owner->countsList($id),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	function goReportCostumer($id)
	{
		$data["id"]=$id;
		$this->load->view("goReportCostumer",$data);
	}
	function saveReport()
	{
		echo $this->owner->saveReport();
	}function delHistory($id)
	{
		echo $this->owner->delHistory($id);
	}
	function getHistory()
	{
		$id=$this->input->post("id");
		$this->db->where("id_costumer",$id);
		$this->db->order_by("tgl","ASC");
		$data=$this->db->get("report_costumer")->result();
		foreach($data as $val)
		{
			?>
			 
			<div class="conversation-item item-right clearfix black">
			<div class="conversation-user" style='margin-top:20px;color:red'>
			&nbsp;&nbsp;&nbsp;<a href='javascript:hapusH(`<?php echo $val->id;?>`,`<?php echo $id ?>`)'><i class='fa fa-trash' style='margin-left:-5px'></i></a>
			</div>
			<div class="conversation-body">
			<div class="name">
			<?php echo $this->reff->getNamaTitleCostumer($val->id_title)?>
			<a class="hapusmobile pull-right" style='color:red' href='javascript:hapusH(`<?php echo $val->id;?>`,`<?php echo $id ?>`)'><i class='fa fa-trash' style='margin-left:-5px'></i></a>
			</div>
			<div class="time hidden-xs">
			<?php echo $this->tanggal->hariLengkapJam($val->tgl,"/")?>
			</div>
			<div class="text">
			<?php echo $val->ket;?>
			</div>
			</div>
			</div>
			<?php
		}
	}
}