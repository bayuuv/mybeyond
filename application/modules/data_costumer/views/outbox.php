<?php echo $this->load->view("js/tanggal.phtml"); ?>  
<a href="" style="font-size:16px;text-decoration:none"><i class="fa fa-envelope"></i> Antrian SMS</a>
<div class="row">
<div class="col-lg-12">

<div class="main-box clearfix ">

<header class="main-box-header clearfix black">





<div class="form-group col-md-4">
<div class="input-group">
<span class="input-group-addon reportrange cursor"><i class="fa fa-calendar-o"></i></span>
<input type="text" readonly class="form-control" id="range" name="range" >
<span class="input-group-btn">
<button onclick="refresh()" class="btn-block btn btn-xs btn-primary pull-right"><i class="fa fa-refresh"></i> GO!</button>
</span>
</div>



</div>
<div class="form-group col-md-2">
<button class=" btn-block btn btn-xs btn-primary" onclick="download()"><i class="fa fa-cloud-download"></i> Download</button>
</div>

<!--
<div class="col-md-3 pull-right">

<div class="input-group">
<select name="grafik" class="cursor form-control">
<option value="0"/>- - - Grafik - - -
<option value="Harian"/>Harian
<option value="Mingguan"/>Mingguan
<option value="Bulanan"/>Bulanan
<option value="Tahunan"/>Tahunan
</select>
<span class="input-group-btn">
<button class="btn btn-primary" type="button" onclick="grafik()">Go!</button>
</span>
</div>
</div>

-->

</header>



<div id="vdata"></div>


</div>
</div>
</div>
<!--<link href="http://localhost/ajax_crud_datatables/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">-->
  


  <script type="text/javascript">	  
    var start 	= moment().subtract(6, 'days');
    var end 	= moment();
    function cb(start, end) {
        $('#range').val(start.format('D/M/YYYY') + ' - ' + end.format('D/M/YYYY'));
    }

    $('.reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Hari ini': [moment(), moment()],
           'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           '7 hari terakhir': [moment().subtract(6, 'days'), moment()],
           '30 hari terakhir': [moment().subtract(29, 'days'), moment()],
           'Bulan ini': [moment().startOf('month'), moment().endOf('month')],
           'Bulan kemarin': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);
  
  


	$(document).ready(function() {
		   dataTable();
      });
	

function escapeHtml(unsafe) {
  return unsafe
      .replace(/&/g, ":dan:")
      .replace(/</g, ":kurang:")
      .replace(/>/g, ":lebih:")
      .replace(/"/g, ":tikwa:")
      .replace(/'/g, ":tikji:")
	  .replace("/", "_");
}	
	
function dataTable()
{
$('#vdata').html("<img src='<?php echo base_url();?>plug/img/load.gif'>");
  var range=$("#range").val();
  $.ajax({
            url:"<?php echo base_url();?>sms/dataTableOutbox/",
			type:"POST",
			data:"range="+range,
            success: function(data)
            {
              $("#vdata").html(data);
            }
        });
}	
	
	function refresh()
	{
	var range=$("#range").val();
	 dataTable(range);
	}

	
	function download()
	{
	var range=$("#range").val();
	var cari=$(".cari").val();
	var cari=escapeHtml(cari);
	window.location.href="<?php echo base_url();?>sms/downloadOutbox/?range="+range+"&cari="+cari;	
	}
	
	function grafik()
	{
	var fre=$("[name='grafik']").val();
	if(fre==0){  $("[name='grafik']").focus(); return false;}
	var range=$("#range").val();
	var cari=$(".cari").val();
	window.location.href="<?php echo base_url();?>sms/outbox_grafik/?range="+range+"&cari="+cari+"&frekuensi="+fre;	
	}
  </script>		
  
  
