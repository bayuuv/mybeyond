<div class="col-lg-12">
 
<div class="clearfix">
   
<!--<button class="btn-primary pull-right"  onclick="importData()"> <i class="fa fa-download"></i> Import</button>-->
<button class="btn-warning pull-right" onclick="exportData()">  <i class="fa fa-upload"></i> Export</button>
<button class="btn-danger pull-right" onclick="add()"> <i class="fa fa-plus-circle"></i> Add</button>
</div>
</div>


<div class="row">
    <div class="col-lg-12">
        <div class="main-box clearfix" >
			<div class="main-box-body clearfix ">
			<div class="table-responsive">
			<span style='position:absolute;margin-top:48px;z-index:222' class="cursor btnhapus">
			<a href="#" onclick="hapusAll()"><i class='fa fa-trash'></i> Hapus Terpilih</a>
			</span>
			<form action="#" name="delcheck" id="delcheck" class="form-horizontal" method="post">
			<table id='table' class="tabel black table-striped table-bordered table-hover dataTable" width="100%">
						<thead style="font-size:13px">			
							<th class='thead' axis="date" width='5px'><input type="checkbox" id="checkbox-1" class="pilihsemua" value="ya" /></th>
						<!--	<th class='thead' axis="string" width='15px'>No</th> -->
								<th class='thead' >NAME</th>
								<th class='thead' >GENDER</th>
							<th class='thead' axis="string" >HP</th>
							<th class='thead' axis="string" >EMAIL </th>
							<th class='thead' axis="string" >NOTE </th>
							<th style='min-width:200px'>&nbsp;</th>
						</thead>
			</table></form>
		</div>
	   </div>
     </div>
   </div>
 </div>


<?php echo $this->load->view("js/tabel.phtml");?>

  <script>
  function hapusAll()
	{	
		var con=window.confirm("hapus data terpilih ?");
		if(con==false){ return false; };
		$.ajax({
		url:"<?php echo base_url();?>data_costumer/HapusAll",
		type: "POST",
		data: $('#delcheck').serialize(),
	//	dataType: "JSON",
		success: function(data)
				{	 $(".btnhapus").hide();
					$(".pilihsemua").removeAttr("checked");
					$(".pilihsemua").val("ya");
					table.ajax.reload(null,false); //reload datatable ajax 
				},
				error: function (jqXHR, textStatus, errorThrown)
				{
					alert('Try Again!');
				}
		});
	
	
	}
  
  
  $(".btnhapus").hide();
  	$(".pilihsemua").click(function(){
	
		if($(".pilihsemua").val()=="ya") {
		$(".pilih").prop("checked", "checked");
		$(".pilihsemua").val("no");
		  $(".btnhapus").show();
		} else {
		$(".pilih").removeAttr("checked");
		$(".pilihsemua").val("ya");
		  $(".btnhapus").hide();
		}
	
	});
	
	function pilcek(){
		$(".btnhapus").show();
		$(".pilihsemua").removeAttr("checked");
		$(".pilihsemua").val("ya");
		 
	};
  
  
  
		var table;
		table = $('#table').DataTable({ 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('data_costumer/ajax_agen/')?>",
            "type": "POST",
        },
		
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
		    "targets": [ 0,1,2,3,4,5], //last column
          "orderable": false, //set not orderable
        },
        ],

      });

	  
	  var method;
	  function add()
	  {
				method="add";
				$("#modalAdd").modal("show");
				$(".title").html(" Tambah Data Costumer");
	  }
	  function edit(data)
	  {
		  method="edit";
		  var isi=data.split("::");
		  $("#modalAdd").modal("show");
		  $(".title").html(" Edit Data Costumer");
		  $("[name='nama']").val(isi[1]);
		  $("[name='hp']").val(isi[2]);
		  $("[name='email']").val(isi[3]);
		  $("[name='ket']").val(isi[4]);
		  $('input:radio[name=jk][value='+isi[5]+']')[0].checked = true;
		  $("[name='id_pelanggan']").val(isi[0]);
		 
	  }
  </script>
  
  
 <!-- Bootstrap modal -->
  <div class="modal fade" id="modalAdd" role="dialog">
  <div class="modal-dialog">
 
<div class="modal-content">
     
      <div class="modal-body form">
   
<section class="content">
  <div class="row">
    <div class="col-lg-12">
	
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-plus-circle"></i><span class='title'></span></h4>
      </div>
	<br>

<form action="javascript:saveAdd()" name="formAgen" id="formAgen" class="form-horizontal black" method="post" >
<div class="form-group">
<label for="nama" class="b col-lg-2 control-label">Name</label>
<div class="col-lg-9">
<input type="text" class="form-control" id="nama" name="nama" required="required" >
<input type="hidden" id="id_pelanggan" name="id_pelanggan">
</div>
</div>

<div class="form-group">
<div class="form-group">
<label for="nama" class="b col-lg-2 control-label">Gender</label>
<div class="col-lg-9">
<div class="radio">
<input type="radio" onclick="clikL()" id="l" name="jk" value="l"/>
<label for="l">
Mr.
</label>
 
<input type="radio" onclick="clikP()"  id="p" name="jk" value="p"/>
<label for="p">
Mrs.
</label>
</div>
</div>
</div>
</div>


<div class="form-group">
<label for="hp" class="col-lg-2 control-label b">Hp</label>
<div class="col-lg-9">
<input type="text" class="form-control" id="hp" name="hp" required="required">
</div>
</div>


<div class="form-group">
<label for="email" class="b col-lg-2 control-label">E-mail</label>
<div class="col-lg-9">
<input type="text" class="form-control" id="email" name="email" required="required">
</div>
</div>

<div class="form-group">
<label for="ket" class="b col-lg-2 control-label">Keterangan</label>
<div class="col-lg-9">
<textarea name="ket" id="ket" class="form-control"></textarea>
</div>
</div>


<div class="form-group">
<div class="col-lg-offset-2 col-lg-9">
<span class='load'></span>
<button type="submit" class="btn btn-success pull-right"><i class='fa fa-save'></i> Simpan</button>
</div>
</div>
</form>
    </div>
  </div>   <!-- /.row -->

</section><!-- /.content -->
  </div>
   </div><!-- /.modal-content -->
		

      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
  



 <script>
  function saveAdd()
	{	
	if(method=="edit")
	{
		var url="<?php echo base_url();?>data_costumer/update";
	}else{
		var url="<?php echo base_url();?>data_costumer/insert";
	}
		$(".load").html("<img src='<?php echo base_url();?>plug/img/load.gif'> Please wait...");
		$.ajax({
		url:url,
		type: "POST",
		data: $('#formAgen').serialize(),
	//	dataType: "JSON",
		success: function(data)
				{
				if(data==false){ alert("Gagal! Data sudah ada pada database"); $(".load").html(""); $("[name='group']").focus(); return false;}
				$(".load").html('<font color="green"><i class="fa fa-check-circle fa-fw fa-lg"></i> Berhasil di simpan</font>');
				table.ajax.reload(null,false);
				$("#formAgen")[0].reset();
				//$(".load").html("");
				if(method=="edit")
				{
					$("#modalAdd").modal("hide");
					$(".load").html('');
				}
				},
				
		});
	}
	
	 function hapus(id)
	  {
	    var tanya=window.confirm("Hapus ?");
		if(tanya==false){ return false;};
	  	$.ajax({
		url:"<?php echo base_url();?>data_costumer/hapus/"+id,
		type: "POST",
		data:"id="+id,
		success: function(data)
				{
				table.ajax.reload(null,false); //reload datatable ajax 
				},
			});
	  }

</script>	



<script>
function exportData()
{
	window.location.href="<?php echo base_url()?>data_costumer/export";
}
function importData()
{
	 $('.msg').html('');
	  $('.hasil').html('');
	$("#modalImport").modal("show");
}
</script>






 <!-- Bootstrap modal -->
  <div class="modal fade" id="modalImport" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
     
      <div class="modal-body form">
<section class="content">
  <div class="row">
    <div class="col-lg-12">
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-download"></i> Import Data Costumer <span class='namaGroup'></span></h4>
      </div>
      <!-- general form elements -->
      <div class="box box-primary black">	   <br>
        Silahkan <a href='<?php echo base_url();?>data_costumer/downloadFormat'><i class="fa fa-file-excel-o"></i> download format</a> sebelum upload.
          <div class="box-body">                      
            <form role="form" name="uploadfilexl" id="uploadfilexl" action="javascript:void();" method="post" enctype="multipart/form-data">
                <div class="form-group">
                  <input id="userfile" required name="userfile" type="file" class="form-control">
                  <input  name="idGroup" type="hidden">
                </div>				
                <button type="submit" onclick="javascript:simpanfile();" class="btn btn-primary pull-right">
                    <span class="fa fa-upload"></span>&nbsp;Upload
                </button>
                <div class="form-group">
                    <div class="msg"></div>
                    <div class="hasil"></div>
                </div>
            </form>
          </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div>   <!-- /.row -->
</section><!-- /.content -->
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
  
   <?php echo $this->load->view("js/form.phtml"); ?>
  <script type="text/javascript">
function simpanfile(){
    var userfile=$('#userfile').val();
    $('#uploadfilexl').ajaxForm({
     url:'<?php echo base_url();?>data_costumer/importData/',
     type: 'post',
     data:{"userfile":userfile},
     beforeSend: function() {
        var percentVal = 'Mengupload 0%';
        $('.msg').html(percentVal);
     },
     uploadProgress: function(event, position, total, percentComplete) {
        var percentVal = 'Mengupload ' + percentComplete + '%';
        $('.msg').html(percentVal);
     },
     beforeSubmit: function() {
      $('.hasil').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Silahkan Tunggu ... ");
     },
     complete: function(xhr) {
        $('.msg').html('');
     }, 
     success: function(resp) {
        $('.hasil').html(resp);
		table.ajax.reload(null,false);
		$("#uploadfilexl")[0].reset();
     },
    });     
};
</script>   


<script>
function detail(id)
{
$("#modalDetailListing").modal("show");
	 $('.dataDetail').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Mohon Tunggu ... ");
	            $.ajax({
                type: "POST",
                dataType: "html",
                url: "<?php echo base_url();?>data_costumer/goReportCostumer/"+id,
                data: "id=" + id,
                success: function (data) {
		            $(".dataDetail").html(data);
                }
            });
}	
</script>


 <!-- Bootstrap modal -->
  <div class="modal fade" id="modalDetailListing" role="dialog">
  <div class="modal-dialog">
<div class="modal-content" >
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-info-circle"></i> Report Costumer<span class='namaGroup'></span></h4>
      </div>
      <div class="modal-body form">
<section class="content">
  <div class="row">
    <div class="col-lg-12 dataDetail">
		<!------------------------->
	
		<!------------------------->
    </div>
  </div>   <!-- /.row -->
</section><!-- /.content -->
  </div>
   </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
  
  
  
  
  
   <!-- Bootstrap modal -->
  <div class="modal fade" id="modalDetails" role="dialog">
  <div class="modal-dialog" style="width:80%">
  
<div class="modal-content" >
<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-info-circle"></i> Data Listing <span class='namaGroup'></span></h4>
      </div>
      <div class="modal-body form">
	  
<section class="content">
  <div class="row">
  
    <div class="col-lg-12 dataDetails">
		
		<!------------------------->
		<!------------------------->
    </div>
  </div>   <!-- /.row -->
</section><!-- /.content -->
  </div>
   </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
  
  
<script src="<?php echo base_url()?>plug/boostrap/js/jquery.js"></script>

  