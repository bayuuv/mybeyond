<div class="main-box-body clearfix ">
<div class="table-responsive">
<span style='position:absolute;margin-top:48px;z-index:222' class="cursor btnhapus">
<a href="#" onclick="hapusAll()"><i class='fa fa-trash'></i> Hapus Terpilih</a>
</span>
<form action="#" name="delcheck" id="delcheck" class="form-horizontal" method="post">
<table id='table' class="tabel black table-striped table-bordered table-hover dataTable" width="100%">
		  	<thead>			
			<!--	<th class='thead' axis="string" width='15px'>No</th> --->
				<th class='thead' axis="date" width='5px'><input type="checkbox" id="checkbox-1" class="pilihsemua" value="ya" /></th>
				<th class='thead' axis="date" width='170px'>Tanggal</th>
				<th class='thead' width='110px'>Nomor Tujuan</th>
				<th class='thead' axis="string" >Sms Antrian</th>
				<th class='thead' axis="string" width="50px">Modem </th>
				<th width='50px'>&nbsp;</th>
			</thead>
</table></form>
</div>
</div>

<?php echo $this->load->view("js/tabel.phtml");?>			
  
  <script>
  function hapusAll()
	{	
		var con=window.confirm("hapus data terpilih ?");
		if(con==false){ return false; };
		$.ajax({
		url:"<?php echo base_url();?>sms/outboxHapusAll",
		type: "POST",
		data: $('#delcheck').serialize(),
	//	dataType: "JSON",
		success: function(data)
				{	 $(".btnhapus").hide();
					$(".pilihsemua").removeAttr("checked");
					$(".pilihsemua").val("ya");
					table.ajax.reload(null,false); //reload datatable ajax 
				},
				error: function (jqXHR, textStatus, errorThrown)
				{
					alert('Try Again!');
				}
		});
	
	
	}
  
  
  $(".btnhapus").hide();
  	$(".pilihsemua").click(function(){
	
		if($(".pilihsemua").val()=="ya") {
		$(".pilih").prop("checked", "checked");
		$(".pilihsemua").val("no");
		  $(".btnhapus").show();
		} else {
		$(".pilih").removeAttr("checked");
		$(".pilihsemua").val("ya");
		  $(".btnhapus").hide();
		}
	
	});
	
	function pilcek(){
		$(".btnhapus").show();
		$(".pilihsemua").removeAttr("checked");
		$(".pilihsemua").val("ya");
		 
	};
  
  
  
		var table;
		table = $('#table').DataTable({ 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('sms/ajax_outbox/')?>?range=<?php echo str_replace("/","_",$range);?>",
            "type": "POST",
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
           "targets": [ 0,1,2,3,4,5], //last column
          "orderable": false, //set not orderable
        },
        ],

      });

	  function hapus(id)
	  {
	    var tanya=window.confirm("Hapus ?");
		if(tanya==false){ return false;};
	  	$.ajax({
		url:"<?php echo base_url();?>sms/outbox_hapus",
		type: "POST",
		data:"id="+id,
		success: function(data)
				{
				table.ajax.reload(null,false); //reload datatable ajax 
				},
			});
	  }
	  
	  
  </script>
  
  
  