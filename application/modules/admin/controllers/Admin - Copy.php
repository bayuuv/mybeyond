<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	

	function __construct()
	{
		parent::__construct();	
		$this->load->model('m_admin','admin');
		$this->load->model('m_profile','profile');
		$this->m_konfig->validasi_session(array("admin"));
	}
	
	function _template($data)
	{
	$this->load->view('template/main',$data);	
	}
	
	public function index()
	{
	$data['konten']="data_user";
	$data['dataProfil']=$this->profile->dataProfile(3);
	$this->_template($data);
	}
	
	
	function akun()
	{
	$this->index();
	}
	
	
	function manajemen()
	{
	$this->index();
	}
	function cekID($id)
	{
	$this->db->where("id_menu",$id);
	echo $this->db->get("main_menu")->num_rows();
	}
	function updateKonfig()
	{
	$this->admin->updateKonfig();
	redirect("admin/konfig");
	}	
	function editMenu($id)
	{
	$data=$this->admin->editMenu($id);
	echo json_encode($data);
	}
	
	function updateMenu()
	{
	$level=$this->input->post("Level");
	if($level==2){	$this->admin->updateIdMain($this->input->post("Induk")); }
	echo $this->admin->updateMenu();
	}
	function hapus_UG($id)
	{
	$this->admin->hapus_UG($id);
	redirect("admin/manajemen");
	}
	function HapusMenu($id)
	{
	$this->admin->HapusMenu($id);
	}
	
	function simpanMenu()
	{
	$level=$this->input->post("Level");
	if($level==2){	$this->admin->updateIdMain($this->input->post("Induk")); }
	echo $this->admin->simpanMenu();
	}
	
	function menuLevel1($id,$val)
	{
	$dataMenu=$this->db->query("select * from main_menu where level='1' and hak_akses ='".$id."' ");
		  $dt="";
		  foreach($dataMenu->result() as $op)
		  {
		  $dt[$op->id_menu]=$op->nama;
		  }
		  $array=$dt;
	echo form_dropdown("Induk",$array,$val,"class='form-control'");
	}
	
	function profile_admin()
	{
	$data['konten']="profile_admin";
	$data['dataProfil']=$this->profile->dataProfile(3);
	$this->_template($data);
	}
	
	function add_dataUser()
	{
	$data=$this->profile->add_dataUser();
	echo json_encode($data);
	}
	
	function update_profile($id)
	{
	$data=$this->profile->update($id);
	echo json_encode($data);
	}
	public function upload_img()
	{
	$this->profile->upload_img(3);
	redirect("admin/profile_admin");
	}
	
	function addUserGroup()
	{
	echo $this->admin->addUserGroup();
	}
	function editUserGroup()
	{
	echo $this->admin->editUserGroup();
	}
	function getUG($id)
	{
	$data=$this->admin->getUG($id);
	echo json_encode($data);
	}
	function getDataUg($id)
	{
	 $dataMenu=$this->db->get("main_level");
		  $dt="";
		  foreach($dataMenu->result() as $op)
		  {
		  $dt[$op->id_level]=$op->nama;
		  }
		  $array=$dt;
	echo form_dropdown("Hak",$array,$id,'style="width:380px" id="sel2"');
	}
	//<!----------------------------------------------------------------------------------------->
	
	
	
	function ajax_open()
	{
			
		$list = $this->admin->get_open();
		$data = array();
		$no = $_POST['start'];
		$no =$no+1;
		foreach ($list as $dataDB) {
		////
			$row = array();
			$row[] = "<span class='size'>".$no++."</span>";
			$row[] = "<img height='70px' src='".base_url()."/file_upload/dp/".$dataDB->poto."' align='left' style='padding:5px;border-radius:10px'>
						".$dataDB->owner."<br>".$dataDB->telp."<br>".$dataDB->email."</span>
					";
			$row[] = "<span class='size'>".$dataDB->Name."<br> <i>Key: </i><font color='blue'>".$dataDB->key."</font></span>";
		
			$row[] = "<span class='size'>".$dataDB->no_center."</span>";
			$row[] = "<span class='size'>".$dataDB->modem."</span>";
					
			//add html for action
			$row[] = '
			
			<a class="table-link" href="javascript:void()" title="Edit" onclick="edit('.$dataDB->id_admin.')">
			<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
			</span> </a>
			
			
			<a class="table-link danger" href="javascript:void()" title="Hapus" onclick="deleted('.$dataDB->id_admin.')">
			<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
			</span> </a>';		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->admin->count_file("admin"),
						"recordsFiltered" =>$this->admin->count_file('admin'),
						"data" => $data,
						);
		//output to json format
		echo json_encode($output);

	}
	
	
	
	function ajax_edit($id)
	{
	$data=$this->admin->getDataUser($id);
	echo json_encode($data);
	}	
	
	function deleted_UG($id)
	{
	$data=$this->admin->deleted_UG($id);
	echo json_encode($data);
	}
	
	function dropdownHak($id)
	{
	$val=$this->admin->dataProfile($id);
	$dataMenu=$this->db->query("select * from main_level where id_level='3' or id_level='9' ");
		  $dt="";
		  foreach($dataMenu->result() as $op)
		  {
		  $dt[$op->id_level]=$op->nama;
		  }
		  $array=$dt;
	echo form_dropdown("level",$array,isset($val->level)?($val->level):"","class='form-control'");
	}
	
	
}

