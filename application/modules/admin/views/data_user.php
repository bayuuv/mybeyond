<div class="row">
<div class="col-lg-12">
<div class="main-box clearfix ">

<header class="main-box-header clearfix">
<h2 class="sadow05 black b">Data User <a href="javascript:add()" class="btn btn-primary pull-right">
<i class="fa fa-plus-circle fa-lg"></i> Tambah Akun
</a></h2>

</header>
<div class="main-box-body clearfix ">




<table id='table' class="tabel black table-striped table-bordered table-hover dataTable">
		  	<thead>			
				<th class='thead' axis="string" width='15px'>No</th>
				<th class='thead' width='220px'>Profile</th>
				<th class='thead' axis="date">Nama akun</th>
				<th class='thead' axis="string">No Center</th>			
				<th class='thead' axis="string">Modem</th>			
				<th class='thead' axis="string">Akses Inbox</th>			
				<th width="50px">&nbsp;</th>
			</thead>
</table>




</div>
</div>
</div>
</div>

 <?php echo $this->load->view("js/tabel.phtml");?>		




  <script type="text/javascript">
      var save_method; //for save method string
    var table;
    $(document).ready(function() {
      table = $('#table').DataTable({ 
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('admin/ajax_open/')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
          "targets": [ 0,-1 ], //last column
          "orderable": false, //set not orderable
        },
        ],

      });
    });

    function reload_table()
    {
      table.ajax.reload(null,false); //reload datatable ajax 
    }


    function deleted(id)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data to database
          $.ajax({
            url:"<?php echo base_url();?>admin/deleted_UG/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               //if success reload ajax table
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
         
      }
    }
	
	
	
	function pilih(id)
	{
	$.ajax({
		url:"<?php echo base_url();?>admin/dropdownHak/"+id,
		type: "POST",
		data:"",
		 success: function(data)
				{
				$('#pilih').html(data);
				}
		});
	}

	function edit(id)
    {
		$('.msgkey').html("");
	$("#gambar").attr("required", false);
	$("#inputPassword1").attr("required", false);
	save_method="update";
	$("#msg").html("");
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
	  //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo base_url();?>admin/ajax_edit/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {	 pilih(id)
            $('[name="Hak"]').val(data.id_admin);
            $('[name="owner"]').val(data.owner);
            $('[name="telp"]').val(data.telp);
            $('[name="email"]').val(data.email);
            $('[name="username"]').val(data.username);
            $('[name="center"]').val(data.no_center);
            $('[name="modem"]').val(data.modem);
            $('[name="akun"]').val(data.Name);
            $('[name="key"]').val(data.key);
            $('[name="aksesinbox"]').val(data.akses_inbox);
           					
            $('#modaledit').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').html('<b>Edit Data</b>'); // Set title to Bootstrap modal title
			
            
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }
	
	function uploadUlang()
	{
	var tanya=confirm("upload file baru ?");
	if(tanya==true)
	{
	$(".inputan").hide();
	$("#inputan").show();
	}
		
	}
	
  </script>		
  
  

  
  <!-- Bootstrap modal -->
  <div class="modal fade" id="modaledit" role="dialog">
		<div class="modal-dialog" style="width:90%" >
               <div class="md-contents">
				
				<div class="modal-body">
	<form  action="javascript:simpan()" id="form" class="form-horizontal " method="post" enctype="multipart/form-data">
<div class="col-md-12" style="background-color:white">	
<div class="col-md-6" style="background-color:white">
<h3>Data Profile</h3>	
<input type="hidden" name="Hak" >


<div class="form-group">
	<label for="poto" class="black col-lg-2 control-label">Photo</label>
	 <div class="col-lg-9">
		<input type="file" class="form-control"  value='' name="gambar" id="gambar" >
	 </div>
</div>

<div class="form-group">
	<label for="inputNama" class="black col-lg-2 control-label">Nama</label>
	 <div class="col-lg-9">
		<input required type="text" class="form-control"  value='' name="owner" id="inputNama" placeholder="Nama ">
	 </div>
</div>

<div class="form-group">
	<label for="inputTelp" class="black col-lg-2 control-label">No.Telp</label>
	 <div class="col-lg-9">
		<input  type="text" class="form-control"  value='' name="telp" id="inputTelp" placeholder="Nomor Telpon">
	 </div>
</div>

<div class="form-group">
	<label for="inputEmail1" class="black col-lg-2 control-label">E-mail</label>
	 <div class="col-lg-9">
		<input  type="email" class="form-control"  value='' name="email" id="inputEmail1" placeholder="Email">
	 </div>
</div>

<div class="form-group">
	<label for="inputuser" class="black col-lg-2 control-label">Username</label>
	 <div class="col-lg-9">
		<input required type="text" class="form-control" value='' name="username" id="inputuser" placeholder="Username">
	 </div>
</div>

<div class="form-group">
	<label for="inputPassword1" class="black col-lg-2 control-label">Password baru</label>
	 <div class="col-lg-9">
		<input type="text" class="form-control"  name="password" id="inputPassword1" placeholder="Password baru (jika password akan diubah)">
	 </div>
</div>
</div>
<div class="col-md-6" style="background-color:white">
<h3>Data Akun</h3>
<div class="form-group">
	<label for="akun" class="black col-lg-2 control-label">Nama Akun</label>
	 <div class="col-lg-9">
		<input type="text" class="form-control"  name="akun" id="akun" >
	 </div>
</div>
<div class="form-group">
	<label for="key" class="black col-lg-2 control-label">Key</label>
	 <div class="col-lg-9">
	 <span class="help-block msgkey" style="right:7px;position:absolute;margin-top:-14px;color:red"></span>
		<input type="text" class="form-control"  name="key" id="key" onkeyup="keys()">
			<span class="help-block" style="right:7px;position:absolute;margin-top:-2px;color:brown">*key merupakan ID akun</span>
	 </div>
</div>

<div class="form-group">
	<label for="center" class="black col-lg-2 control-label">Nomor Center</label>
	 <div class="col-lg-9">
	 <?php $this->load->model("m_pengaturan","pengaturan"); $center=$this->pengaturan->pengaturan(2); ?>
		<input type="text" class="form-control"  name="center" value="<?php echo $center;?>" id="center" >
	 </div>
</div>


<div class="form-group">
	<label for="modem" class="black col-lg-2 control-label">Modem</label>
	 <div class="col-lg-9">
		<input type="text" class="form-control"  value="<?php echo $this->pengaturan->pengaturan(6);?>" name="modem" id="modem" >
		<span class="help-block" style="right:7px;position:absolute;margin-top:-2px;color:brown">*pisahkan dengan tanda koma(,) jika modem lebih dari 1</span>
	 </div>
</div>

<div class="form-group">
	<label for="modem" class="black col-lg-2 control-label">Akses Inbox</label>
	 <div class="col-lg-9">
		<select class="form-control" style="margin-top:-7px" name="aksesinbox">
<option value="terbatas">Terbatas (hanya terdaftar pada kontak)</option>
<option value="bebas">Bebas (kecuali kontak akun lain)</option>
</select>
	 </div>
</div>

 <div class="modal-footer">
            <button type="submit" id="btnSave" class="btn btn-primary pull-right" onclick="javascript:simpan()">Save</button>
			<span id="msg" style='padding-right:15px;margin-top:20px'></span>
            <button type="button" class="btn btn-danger pull-right" style="margin-right:10px" data-dismiss="modal">Cancel</button>
          </div>
  </div>   
</div>

</div>

  
</form>
				</div>
				</div>
						
				</div>
         </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
		

<script src="<?php echo base_url('plug/jqueryform/jquery.form.js');?>"></script>
<script type="text/javascript">
var save_method="";
function add()
	{
	save_method="add";
	 $('#form')[0].reset(); // reset form on modals
	$("#gambar").attr("required", "true");
	$("#inputPassword1").attr("required", "true");
	
	$("#msg").html("");
	$('#modaledit').modal('show'); 
	}
	
	
	
function simpan(){
var pass=$("#inputPassword1").val();
if(pass!=null && pass!=""){
$('#msg').html('<img src="<?php echo base_url();?>plug/img/load.gif"/> Loading...');
}
    var id=$('[name="Hak"]').val();
	if(save_method=="add"){
	var link='<?php echo base_url("admin/add_akun"); ?>'; }
	  else{
	 var link='<?php echo base_url("admin/update_akun"); ?>/'+id; }
	
	
    $('#form').ajaxForm({
	 url:link,
     data: $('#form').serialize(),
	 dataType: "JSON",
     success: function(data)
            {
			$("#inputPassword1").val("");
			if(data==true){
			 $('#msg').html('<font color="green"><i class="fa fa-check-circle fa-fw fa-lg"></i> Berhasil disimpan.</font>');
			  $('#modaledit').modal('hide');
               reload_table();
			}else{
			$("#inputuser").val("");
			 $('#msg').html('<font color="red"><i class="fa fa-warning fa-fw fa-lg"></i>Silahkan cari username/password lain!</font>');
			}
			

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
    });     
};

function keys()
{
	var key=$("[name='key']").val();
	var id=$("[name='Hak']").val();
	$.ajax({
		url:"<?php echo base_url();?>admin/cekKey",
		type:"post",
		data:"GroupID="+id+"&key="+key,
		 success: function(data)
				{
					if(data=="ada"){
					$('.msgkey').html("Silahkan cari key lain!");	$("#btnSave").hide();
					}else{
						$('.msgkey').html("");
						$("#btnSave").show();
					}
				}
		});
	
}
</script> 