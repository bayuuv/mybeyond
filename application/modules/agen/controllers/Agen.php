<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Agen extends CI_Controller {

	

	function __construct()
	{
		parent::__construct();	
	//	$this->m_konfig->validasi_session(array("agen"));
		//$this->load->model("M_data_agen","agen");
	}
	
	function _template($data)
	{
	$this->load->view('template/main',$data);	
	}
	
	public function index()
	{

	$data['konten']="index";
	$this->_template($data);
	}
	function ajax_agen()
	{
		$list = $this->agen->get_dataAgen();
        $data = array();
        $no = $_POST['start']+1;
        foreach ($list as $val) {
			$row = array();
			$row[] =  '<input type="checkbox" class="pilih" onclick="pilcek()" name="hapus[]" value="'.$val->id_agen.'">';
			//$row[] = $no++;
            $row[] = $val->kode_agen;
            $row[] = $val->nama;
            $row[] = $val->hp;
            $row[] = $val->email;
            $row[] = $val->alamat;
            $row[] = $this->agen->jmlListing($val->id_agen);
            $row[] = $this->agen->jmlPenjualan($val->id_agen);
            $row[] = $this->agen->jmlPelanggan($val->id_agen);
            $row[] = '
			<a href="#" style="font-size:14px" onclick="edit(`' . $val->id_agen . '::' . $val->kode_agen . '::' . $val->nama . '::' . $val->hp . '::' . $val->email . '::' . $val->alamat . '`)" class=" "><i class="fa fa-edit "> </i> Edit </a>
			| <a href="#" style="font-size:14px" onclick="hapus(`' . $val->id_agen . '`);" class=" "><i class="fa fa-trash"> </i> Hapus </a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->agen->counts(),
            "recordsFiltered" => $this->agen->counts(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	function insert()
	{
		echo $this->agen->insert();
	}
	function getKodeAgen()
	{
		echo $this->agen->getKodeAgen();
	}
	function update()
	{
		echo $this->agen->update();
	}
	function HapusAll()
	{
	echo $this->agen->HapusAll();
	}
	function hapus($id)
	{
	echo $this->agen->hapus($id);
	}
	
	function export()
	{
		$this->agen->export();
	}
	
	function downloadFormat()
	{
	$this->agen->downloadFormat();
	}
	function importData()
	{
	$this->load->library("PHPExcel");
	 $data=$this->agen->importData();
		$data=explode("-",$data);
               ?><br><br>
			  <p style="color:green"><b>Import Data Selesai</b></p>
                <table class="tabel table-hover table-bordered" style="100%">
			       <?php 
				   if($data[0]){		   echo "<tr><td>Berhasil di simpan : ".$data[0]." kontak</td></tr>"; 			}?>
					<?php
					if($data[1]){	  	   echo "<tr><td>Diperbaharui : ".$data[1]." kontak</td></tr>";			} 
					if($data[2]){	  	   echo "<tr><td>Gagal di import : ".$data[2]." kontak</td></tr>";			} ?>
                </table>
                <?php
	}
	function cekpas()
	{
		$this->db->where("username",$this->input->post("user"));
		$this->db->where("password",$this->input->post("pass"));
	echo	$this->db->get("data_agen")->num_rows();
	}
}