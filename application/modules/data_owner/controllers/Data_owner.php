<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Data_owner extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->m_konfig->validasi_session(array("user","operator","promotion"));
		$this->load->model("M_data_owner","owner");
	}
	
	function _template($data)
	{
	$this->load->view('template/main',$data);	
	}
	function getDataOwner()
	{
		$id=$this->db->query("select max(id_owner) as maxi from data_owner")->row();
                                        $ref_type = $this->reff->getOwner();
                                        $array_pemilik[""] = "==== Pilih ====";
                                        foreach ($ref_type as $val) {
                                            $array_pemilik[$val->id_owner] = $val->nama;
                                        }
                                        $data = $array_pemilik;
                                        echo form_dropdown('owner', $data, $id->maxi, '  id="owner" style="width:100%"');
                                        echo "<script>$('#owner').select2();</script>";
	}
		function getEdit($id)
	{
		$data=$this->db->get_where("data_owner",array("id_owner"=>$id))->row();
		$isi=$data->hp2."::"; //4
		$isi.=$data->nama."::"; //1
		$isi.=$data->jk."::"; //2
		$isi.=$this->tanggal->eng($data->tgl_lahir,"/")."::"; //3
		$isi.=$data->hp."::"; //4
		$isi.=$data->email."::"; //5
		$isi.=$data->alamat."::"; //6
		$isi.=" ::"; //7
		$isi.=$data->bank."::"; //8
		$isi.=$data->no_rek."::"; //9
		$isi.=$data->atas_nama_bank."::"; //10
		$isi.=$data->nama_wakil."::"; //11
		$isi.=$data->nomor_wakil."::"; //12
		$isi.=$data->hubungan_wakil."::"; //13
		$isi.=$data->nama_pasangan."::"; //14
		$isi.=$this->tanggal->eng($data->tgl_lahir_pasangan,"/")."::"; //15
		$isi.=$data->anak1."::"; //16
		$isi.=$this->tanggal->eng($data->tgl_lahir_anak1,"/")."::"; //17
		$isi.=$data->anak2."::"; //18
		$isi.=$this->tanggal->eng($data->tgl_lahir_anak2,"/")."::"; //19
		$isi.=$data->anak3."::"; //20
		$isi.=$this->tanggal->eng($data->tgl_lahir_anak3,"/")."::"; //21
		$isi.=$data->anak4."::"; //22
		$isi.=$this->tanggal->eng($data->tgl_lahir_anak4,"/")."::"; //23
		$isi.=$data->anak5."::"; //24
		$isi.=$this->tanggal->eng($data->tgl_lahir_anak5,"/")."::"; //25
		$isi.=" ::"; //26
		$isi.=" ::"; //27
		$isi.="::"; //28
		$isi.="0::"; //29
		if(isset($data->poto)){ $poto=$data->poto; }else{ $poto="nopund.jpg";}
		$isi.=$poto."::"; //30
		$isi."::"; //31
		$isi.="::"; //32
		$isi.="::"; //33
		$isi.="::"; //34
		$isi.=" ::"; //35
		echo $isi;
	}
	function getDataOwnerByName()
	{
		$nama=$this->input->post("nama");
		$data=$this->owner->getDataOwnerByName($nama);
		echo json_encode($data);
	}
	function getDataOwnerByHP()
	{
		$hp1_own=$this->input->post("hp1_own");
		$data=$this->owner->getDataOwnerByHP($hp1_own);
		echo json_encode($data);
	}
	function go_cek_duplikat_owner_ada()
	{
		echo $this->owner->go_cek_duplikat_owner_ada();
	}
	function go_cek_duplikat_owner()
	{
		echo $this->owner->go_cek_duplikat_owner();
	}
	public function index()
	{

	$data['konten']="index";
	$this->_template($data);
	}
		function getDetail($id)
	{
		$data["id"]=$id;
		$this->load->view("getDetail",$data);
	}
	function ajax_agen()
	{
		$list = $this->owner->get_dataAgen();
        $data = array();
        $no = $_POST['start']+1;
        foreach ($list as $val) {
			
			if($val->jk=="l")
			{
				$jk="Mr.";
			}elseif($val->jk=="p"){
				$jk="Mrs.";
			}else{
				$jk="";
			}
			$row = array();
			$row[] =  '<input type="checkbox" class="pilih" onclick="pilcek()" name="hapus[]" value="'.$val->id_owner.'">';
			//$row[] = $no++;
            $row[] = '<a href="#" style="font-size:14px" onclick="detailProfile(`'.$val->id_owner.'`)">'.$jk." ".$val->nama.'</a>';
            $row[] = $this->tanggal->septik($val->hp);
            $row[] = $this->tanggal->septik($val->hp2);
            $row[] = $val->email;
            $row[] = $val->alamat;
            $row[] = '
			<a href="#" style="font-size:14px" onclick="detail(`' . $val->id_owner.'`)" class=" "><i class="fa fa-list "> </i> Data Listing </a>
			| <a href="#" style="font-size:14px" onclick="edit(`' . $val->id_owner .'`)" class=" "><i class="fa fa-edit "> </i> Edit </a>
			| <a href="#" style="font-size:14px" onclick="hapus(`' . $val->id_owner . '`);" class=" "><i class="fa fa-trash"> </i> Delete </a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->owner->counts(),
            "recordsFiltered" => $this->owner->counts(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	function insert()
	{
		echo $this->owner->insert();
	}
	function update()
	{
		echo $this->owner->update();
	}
	function HapusAll()
	{
	echo $this->owner->HapusAll();
	}
	function hapus($id)
	{
	echo $this->owner->hapus($id);
	}
	
	function export()
	{
		$this->owner->export();
	}
	
	function downloadFormat()
	{
	$this->owner->downloadFormat();
	}
	function importData()
	{
	$this->load->library("PHPExcel");
	 $data=$this->owner->importData();
		$data=explode("-",$data);
               ?><br><br>
			  <p style="color:green"><b>Import Data Selesai</b></p>
                <table class="tabel table-hover table-bordered" style="100%">
			       <?php 
				   if($data[0]){		   echo "<tr><td>Berhasil di simpan : ".$data[0]." data</td></tr>"; 			}?>
					<?php
					if($data[1]){	  	   echo "<tr><td>Diperbaharui : ".$data[1]." data</td></tr>";			} 
					if($data[2]){	  	   echo "<tr><td>Gagal di import : ".$data[2]." data</td></tr>";			} ?>
                </table>
                <?php
	}
	function getDataDetail($id)
	{
		$data["id"]=$id;
		$this->load->view("getListing",$data);
	}
	
	
	function ajax_property($id)
	{
		$list = $this->owner->get_dataProperty($id);
        $data = array();
        $no = $_POST['start']+1;
        foreach ($list as $val) {
			$status=$val->status;
			if($status)
			{
				$status="Sold";
			}else{
				$status="Unsold";
			}
			$row = array();
		 
            $row[] = "<a href='javascript:details(`".$val->kode_prop."`)'>".$val->kode_prop."</a>";
            $row[] = $this->reff->getNamaJenis($val->jenis_prop)." ".$val->type_jual;
         
            $row[] = number_format($val->harga,0,",",".");
            $row[] = $val->nama_area." - ".$val->alamat_detail;
            $row[] = $this->reff->getNamaOwner($val->id_owner);
            $row[] = $this->reff->getNamaAgen($val->agen);
            $row[] = "Diupload : ".$this->reff->cekJumlahGambar($val->id_prop)."   <br>Factsheet :".$this->reff->cekGambarDesain($val->id_prop);
     
			$row[] = $status;
          
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->owner->countsList($id),
            "recordsFiltered" => $this->owner->countsList($id),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
}