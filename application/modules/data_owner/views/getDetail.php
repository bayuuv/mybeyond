 <?php $data=$this->db->get_where("data_owner",array("id_owner"=>$id))->row(); ?>
 <div class="row" id="user-profile" style="padding:10px">
<div class="col-lg-4 col-md-4 col-sm-4">
<div class="main-box clearfix">
<header class="main-box-header clearfix">
<center><a href="#"><h2 id='d_nama'><?php echo $data->nama; ?></h2></a><?php if($data->jk=="l"){ echo "Laki-laki"; $pasangan="Istri";}else{ echo "Perempuan"; $pasangan="Suami";};?></center>
</header>
<div class="main-box-body clearfix black">
<?php if(isset($data->poto)){ $poto=$data->poto; }else{ $poto="nopund.jpg";} ?>
<div id="d_poto" class='thumbnails'><a href='<?php echo base_url() ?>file_upload/owner/<?php echo $poto; ?>' download><img style="max-width:150px" src="<?php echo base_url() ?>file_upload/owner/<?php echo $poto; ?>" alt="" class="  img-responsive center-block"/></a></div>
 
  
 
<div class="profile-message-btn center-block text-center">
 
</div>
</div>
</div>
</div>
<div class="col-lg-8 col-md-8 col-sm-8">
<div class="main-box clearfix">
<div class="tabs-wrapper profile-tabs">
<ul class="nav nav-tabs">
<li class="active"><a href="#tab-newsfeed" data-toggle="tab">Profile</a></li>
<li><a href="#tab-activity" data-toggle="tab">Family</a></li> 
 
 
</ul>
<div class="tab-content">
<div class="tab-pane fade in active" id="tab-newsfeed">
<div id="newsfeed">
<table class="tabel black table-striped table-bordered table-hover dataTable">
 
<tr> <td>Tanggal Lahir </td><td>:</td><td><?php echo $this->tanggal->hariLengkap($data->tgl_lahir,"/"); ?></td> </tr>
<tr> <td>Hp1 </td><td>:</td><td><?php echo $data->hp; ?></td> </tr>
<tr> <td>Hp2 </td><td>:</td><td><?php echo $data->hp2; ?></td> </tr>
<tr> <td>E-mail </td><td>:</td><td><?php echo $data->email; ?></td> </tr>
<tr> <td>Alamat </td><td>:</td><td><?php echo $data->alamat; ?></td> </tr>
 
<tr> <td>Akun Bank </td><td>:</td><td><?php echo ucwords($data->bank).": ".$data->no_rek. " a/n ".$data->atas_nama_bank.""; ?></td> </tr>

<tr> <td colspan="3">Kontak ke 2 yang dapat dihubungi </td> </tr>
<tr> <td>Nama </td><td>:</td><td><?php echo $data->nama_wakil; ?></td> </tr>
<tr> <td>Hp </td><td>:</td><td><?php echo $data->nomor_wakil; ?></td> </tr>
<tr> <td>hubungan </td><td>:</td><td><?php echo $data->hubungan_wakil; ?></td> </tr>
</table>
<hr>
<?php if(isset($data->ktp)){ $ktp=$data->ktp; }else{ $ktp="nopund.jpg";} ?>
<?php if(isset($data->kk)){ $kk=$data->kk; }else{ $kk="nopund.jpg";} ?>
<?php if(isset($data->npwp)){ $npwp=$data->npwp; }else{ $npwp="nopund.jpg";} ?>
<style>
.thumbnails img {
      max-width: 20%;
      padding: 5px;
      border: 1px solid #ccc;
      height: auto;
      background: #fff;
      box-shadow: 1px 1px 7px rgba(0,0,0,0.1);
    }
</style>	
<div class="col-lg-4 thumbnails"   align="center"><a href='<?php echo base_url() ?>file_upload/owner/<?php echo $ktp; ?>' download><center><b>KTP</b></center><img style="max-width:150px" src="<?php echo base_url() ?>file_upload/owner/<?php echo $ktp; ?>"/></a></div>
<div class="col-lg-4 thumbnails"   align="center"><a href='<?php echo base_url() ?>file_upload/owner/<?php echo $kk; ?>' download><center><b>KK</b></center><img style="max-width:150px" src="<?php echo base_url() ?>file_upload/owner/<?php echo $kk; ?>"/></a></div>
<div class="col-lg-4 thumbnails"   align="center"><a href='<?php echo base_url() ?>file_upload/owner/<?php echo $npwp; ?>' download><center><b>NPWP</b></center><img style="max-width:150px" src="<?php echo base_url() ?>file_upload/owner/<?php echo $npwp; ?>"/></a></div>



<hr>

</div>

</div>
<div class="tab-pane fade" id="tab-activity">
<div class="table-responsive">

<table class="tabel black table-striped table-bordered table-hover dataTable">
<tr> <td>Nama <?php $pasangan; ?> </td><td>:</td><td><?php echo $data->nama_pasangan; ?></td> </tr>
<tr> <td>Tanggal Lahir </td><td>:</td><td><?php echo $this->tanggal->hariLengkap($data->tgl_lahir_pasangan,"/"); ?></td> </tr>
<?php 
if($data->anak1)
{?>
<tr> <td>Anak ke-1 </td><td>:</td><td><?php echo $data->anak1; ?></td> </tr>
<tr> <td>Tanggal Lahir </td><td>:</td><td><?php echo $this->tanggal->hariLengkap($data->tgl_lahir_anak1,"/"); ?></td> </tr>
<?php } ?>

<?php 
if($data->anak2)
{?>
<tr> <td>Anak ke-2 </td><td>:</td><td><?php echo $data->anak2; ?></td> </tr>
<tr> <td>Tanggal Lahir </td><td>:</td><td><?php echo $this->tanggal->hariLengkap($data->tgl_lahir_anak2,"/"); ?></td> </tr>
<?php } ?>

<?php 
if($data->anak3)
{?>
<tr> <td>Anak ke-3 </td><td>:</td><td><?php echo $data->anak3; ?></td> </tr>
<tr> <td>Tanggal Lahir </td><td>:</td><td><?php echo $this->tanggal->hariLengkap($data->tgl_lahir_anak3,"/"); ?></td> </tr>
<?php } ?>

<?php 
if($data->anak4)
{?>
<tr> <td>Anak ke-4 </td><td>:</td><td><?php echo $data->anak4; ?></td> </tr>
<tr> <td>Tanggal Lahir </td><td>:</td><td><?php echo $this->tanggal->hariLengkap($data->tgl_lahir_anak4,"/"); ?></td> </tr>
<?php } ?>

<?php 
if($data->anak5)
{?>
<tr> <td>Anak ke-5 </td><td>:</td><td><?php echo $data->anak5; ?></td> </tr>
<tr> <td>Tanggal Lahir </td><td>:</td><td><?php echo $this->tanggal->hariLengkap($data->tgl_lahir_anak5,"/"); ?></td> </tr>
<?php } ?>

<tr> <td colspan="3">Kontak ke 2 yang dapat dihubungi </td> </tr>
<tr> <td>Nama </td><td>:</td><td><?php echo $data->nama_wakil; ?></td> </tr>
<tr> <td>Hp </td><td>:</td><td><?php echo $data->nomor_wakil; ?></td> </tr>
<tr> <td>hubungan </td><td>:</td><td><?php echo $data->hubungan_wakil; ?></td> </tr>
</table>

</div>
</div>

<style>
#tableListing th {
	 padding:5px;
}#tableCostumer th {
	 padding:5px;
}
</style>
 


 


</div>
</div>
</div>
</div>
</div>


 
 