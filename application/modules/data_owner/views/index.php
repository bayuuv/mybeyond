<!--<button class="btn-primary pull-right"  onclick="importData()"> <i class="fa fa-download"></i> Import</button>-->
<button class="btn-warning pull-right" onclick="exportData()">  <i class="fa fa-upload"></i> Export</button>
<button class="btn-danger pull-right" onclick="add()"> <i class="fa fa-plus-circle"></i> Add</button>
<div class="row">
    <div class="col-lg-12">
        <div class="main-box clearfix" >
			<div class="main-box-body clearfix ">
			<div class="table-responsive">
			<span style='position:absolute;margin-top:48px;z-index:222' class="cursor btnhapus">
			<a href="#" onclick="hapusAll()"><i class='fa fa-trash'></i> Hapus Terpilih</a>
			</span>
			<form action="#" name="delcheck" id="delcheck" class="form-horizontal" method="post">
			<table id='table' class="tabel black table-striped table-bordered table-hover dataTable" width="100%">
						<thead style="font-size:13px">			
							<th class='thead' axis="date" width='5px'><input type="checkbox" id="checkbox-1" class="pilihsemua" value="ya" /></th>
						<!--	<th class='thead' axis="string" width='15px'>No</th> -->
								<th class='thead' >NAME</th>
							<th class='thead' axis="string" >HP1</th>
							<th class='thead' axis="string" >HP2</th>
							<th class='thead' axis="string" >EMAIL </th>
							<th class='thead' axis="string" >ADRESS </th>
							<th width='200px'>&nbsp;</th>
						</thead>
			</table></form>
		</div>
	   </div>
     </div>
   </div>
 </div>


<?php echo $this->load->view("js/tabel.phtml");?>

  <script>
  function hapusAll()
	{	
		var con=window.confirm("hapus data terpilih ?");
		if(con==false){ return false; };
		$.ajax({
		url:"<?php echo base_url();?>data_owner/HapusAll",
		type: "POST",
		data: $('#delcheck').serialize(),
	//	dataType: "JSON",
		success: function(data)
				{	 $(".btnhapus").hide();
					$(".pilihsemua").removeAttr("checked");
					$(".pilihsemua").val("ya");
					table.ajax.reload(null,false); //reload datatable ajax 
				},
				error: function (jqXHR, textStatus, errorThrown)
				{
					alert('Try Again!');
				}
		});
	
	
	}
  
  
  $(".btnhapus").hide();
  	$(".pilihsemua").click(function(){
	
		if($(".pilihsemua").val()=="ya") {
		$(".pilih").prop("checked", "checked");
		$(".pilihsemua").val("no");
		  $(".btnhapus").show();
		} else {
		$(".pilih").removeAttr("checked");
		$(".pilihsemua").val("ya");
		  $(".btnhapus").hide();
		}
	
	});
	
	function pilcek(){
		$(".btnhapus").show();
		$(".pilihsemua").removeAttr("checked");
		$(".pilihsemua").val("ya");
		 
	};
  
  
  
		var table;
		table = $('#table').DataTable({ 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('data_owner/ajax_agen/')?>",
            "type": "POST",
        },
		
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
		    "targets": [ 0,1,2,3,4,5], //last column
          "orderable": false, //set not orderable
        },
        ],

      });

	  
	  var method;
	  function add()
	  {
				method="add";
				$("#modalAdd").modal("show");
				$(".title").html(" Tambah Data Owner");
	  }
	/*  function edit(data)
	  {
		  method="edit";
		  var isi=data.split("::");
		  $("#modalAdd").modal("show");
		  $(".title").html(" Edit Data Owner");
		  $("[name='nama']").val(isi[1]);
		  $("[name='hp']").val(isi[2]);
		  $("[name='email']").val(isi[3]);
		  $("[name='alamat']").val(isi[4]);
		  $("[name='id_owner']").val(isi[0]);
		  $("[name='hp2']").val(isi[5]);
		 
	  }*/
	    function edit(id)
	  {
		
		  method="edit";
		  $("#modalAdd").modal("show");
			 	$.ajax({
				url:"<?php echo base_url();?>data_owner/getEdit/"+id,
				success: function(data)
						{			
						
				  var isi=data.split("::");
				  // alert(isi[3]);
				  $(".title").html(" Edit Vendor");
				  $("[name='hp2']").val(isi[0]);
				  if(isi[0]=="2"){
					  $(".akun").show();
				  }
				  $("[name='kode']").val(isi[29]);
				  $("[name='nama']").val(isi[1]);
				  $("[name='hp']").val(isi[4]);
				  $("[name='email']").val(isi[5]);
				  $("[name='alamat']").val(isi[6]);
				  $("[name='id_owner']").val(id);
				  $("[name='tgl_lahir']").val(isi[3]);
				  $("[name='tgl_masuk_kerja']").val(isi[7]);
				  $("[name='bank']").val(isi[8]);
				  $("[name='rek']").val(isi[9]);
				  $("[name='an']").val(isi[10]);
				  $("[name='nama2']").val(isi[11]);
				  $("[name='hp_wakil']").val(isi[12]);
				  $("[name='hubungan']").val(isi[13]);
				  $("[name='nama_pasangan']").val(isi[14]);
				  $("[name='tgl_lahir_pasangan']").val(isi[15]);
				  $("[name='anak1']").val(isi[16]);
				  $("[name='tgl_lahir_anak1']").val(isi[17]);
				  
				  $("[name='anak2']").val(isi[18]);
				  $("[name='tgl_lahir_anak2']").val(isi[19]);
				  
				  $("[name='anak3']").val(isi[20]);
				  $("[name='tgl_lahir_anak3']").val(isi[21]);
				  
				  $("[name='anak4']").val(isi[22]);
				  $("[name='tgl_lahir_anak4']").val(isi[23]);
				  
				  $("[name='anak5']").val(isi[24]);
				  $("[name='tgl_lahir_anak5']").val(isi[25]);
				  $("[name='username']").val(isi[26]);
				  $("[name='password']").val(isi[27]);
				  $("[name='tgl_habis_kontrak']").val(isi[35]);
				 
				  $('input:radio[name=jk][value='+isi[2]+']')[0].checked = true;
				
	
				},
			});
		 
		
	  }
</script>	
	  
  </script>
  
   <form action="javascript:saveAdd()" id="formulirAgen"   method="post" >	
 <!-- Bootstrap modal -->
  <div class="modal fade" id="modalAdd" role="dialog" >
  <div class="modal-dialog"  style="width:80%" >
 
<div class="modal-content" >
     
      <div class="modal-body form" >
   
<section class="content">

  <div class="row">
    <div class="col-lg-12">

	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-plus-circle"></i><span class='title'></span></h4>
      </div>
	<br>
<script>
   $("document").ready(function () {
$(".akun").hide();
   });
function getAkun()
{
	var jab=$("[name='jabatan']").val();
	if(jab==2)
	{
		$(".akun").show();
	}else{
		$(".akun").hide();
	}
}
</script>
<div class="row">
<div class="col-lg-12">
<div class="main-box clearfix">
<div >
<div class="tabs-wrapper">
<ul class="nav nav-tabs">
<li class="pr active b"><a href="#tab-home" data-toggle="tab">Profile</a></li>
<li class="k2 b"><a href="#kontak2" data-toggle="tab">Kontak 2</a></li>
<li class="kl b"><a href="#keluarga" data-toggle="tab">Family</a></li>

 

</ul>
<div class="tab-content">
<div class="tab-pane fade in active" id="tab-home">

<div class="form-horizontal black col-md-6">


<input type="hidden"  id="kode" name="kode"  >
<input type="hidden"  id="id_owner" name="id_owner"  >
  

<div class="form-group">
<label for="nama" class="b col-lg-2 control-label">NAME</label>
<div class="col-lg-9">
<input type="text" class="form-control" id="nama" name="nama"  >
<input type="hidden" id="id_agen" name="id_agen">
</div>
</div>

<div class="form-group">
<label for="nama" class="b col-lg-2 control-label">GENDER</label>
<div class="col-lg-9">
<div class="radio">
<input type="radio" onclick="clikL()" id="l" name="jk" value="l"/>
<label for="l">
Mr.
</label>
 
<input type="radio" onclick="clikP()"  id="p" name="jk" value="p"/>
<label for="p">
Mrs.
</label>
</div>
</div>
</div>


<div class="form-group">
<label for="tgl_lahir" class="col-lg-2 control-label b">Tgl lahir</label>
<div class="col-lg-9">
<input type="text" class="form-control" id="maskedDate1" name="tgl_lahir" placeholder="contoh:31/12/1990">
</div>
</div>


<div class="form-group">
<label for="hp" class="col-lg-2 control-label b">Hp1</label>
<div class="col-lg-9">
<input type="text" class="form-control" id="hp" name="hp" required>
</div>
</div>
<div class="form-group">
<label for="hp" class="col-lg-2 control-label b">Hp2</label>
<div class="col-lg-9">
<input type="text" class="form-control" id="hp2" name="hp2" >
</div>
</div>


<div class="form-group">
<label for="email" class="b col-lg-2 control-label">E-mail</label>
<div class="col-lg-9">
<input type="text" class="form-control" id="email" name="email" >
</div>
</div>

<div class="form-group">
<label for="alamat" class="b col-lg-2 control-label">Address</label>
<div class="col-lg-9">
<textarea name="alamat" id="alamat" class="form-control"></textarea>
</div>
</div>
</div>


<div class="form-horizontal black col-md-6">
 
<div class="form-group">
<label for="poto" class="b col-lg-2 control-label">Photo Profile</label>
<div class="col-lg-9">
<input type="file" class="form-control" id="poto" name="poto"  >
</div>
</div>

<div class="form-group">
<label for="ktp" class="b col-lg-2 control-label">Scan KTP</label>
<div class="col-lg-9">
<input type="file" class="form-control" id="ktp" name="ktp"  >
</div>
</div>

<div class="form-group">
<label for="kk" class="b col-lg-2 control-label">Scan KK</label>
<div class="col-lg-9">
<input type="file" class="form-control" id="kk" name="kk"  >
</div>
</div>


<div class="form-group">
<label for="email" class="b col-lg-2 control-label">Scan NPWP</label>
<div class="col-lg-9">
<input type="file" class="form-control" id="npwp" name="npwp" >
</div>
</div>




<div class="form-group">
<label for="rek" class="b col-lg-2 control-label">Bank</label>
<div class="col-lg-9">
<select name="bank" class="form-control" id="bank">
<option value=' '>=== Pilih ===</option>
<option value='bca'>BCA</option>
<option value='bni'>BNI</option>
<option value='bri'>BRI</option>
<option value='bjb'>BJB</option>
<option value='nisp'>NISP</option>
<option value='mandiri'>MANDIRI</option>
</select>
</div>
</div>


<div class="form-group">
<label for="rek" class="b col-lg-2 control-label">Nomor Rek.</label>
<div class="col-lg-9">
<input type="text" class="form-control" id="rek" name="rek" >
</div>
</div>

<div class="form-group">
<label for="an" class="b col-lg-2 control-label">Atas nama no.rek</label>
<div class="col-lg-9">
<input type="text" class="form-control" id="an" name="an" >
</div>
</div>


</div>





</div>
<div class="tab-pane fade" id="kontak2">


<div class="form-horizontal black col-md-12">
<div class="form-group">
<label for="nama2" class="b col-lg-2 control-label">Nama </label>
<div class="col-lg-9">
<input type="text" class="form-control" id="nama2" name="nama2"  >
</div>
</div>

<div class="form-group">
<label for="hp_wakil" class="b col-lg-2 control-label">Hp</label>
<div class="col-lg-9">
<input type="text" class="form-control" id="hp_wakil" name="hp_wakil"  >
</div>
</div>


<div class="form-group">
<label for="hubungan" class="b col-lg-2 control-label">Hubungan </label>
<div class="col-lg-9">
<input type="text" class="form-control" id="hubungan" name="hubungan"  >
</div>
</div>





</div>



</div>
<div class="tab-pane fade" id="keluarga">

<div class="form-horizontal black col-md-12">
<div class="form-group">
<label for="nama_pasangan" class="b col-lg-2 control-label"><span class='pasangan'>Nama Pasangan</span></label>
<div class="col-lg-9">
<input type="text" class="form-control" id="nama_pasangan" name="nama_pasangan"  >
</div>
</div>

<div class="form-group">
<label for="tgl_lahir_pasangan" class="b col-lg-2 control-label"> Tgl Lahir </label>
<div class="col-lg-9">
<input type="text" class="form-control" id="maskedDate4" name="tgl_lahir_pasangan"  placeholder="contoh:31/12/1990" >
</div>
</div>

<div class="form-group">
<label for="anak1" class="b col-lg-2 control-label">Nama Anak 1</label>
<div class="col-lg-9">
<input type="text" class="form-control" id="anak1" name="anak1"  >
</div>
</div>


<div class="form-group">
<label for="tgl_lahir_anak1" class="b col-lg-2 control-label">Tgl lahir </label>
<div class="col-lg-9">
<input type="text" class="form-control" id="maskedDate2" name="tgl_lahir_anak1"  placeholder="contoh:31/12/1990" >
</div>
</div>



<div class="form-group">
<label for="anak1" class="b col-lg-2 control-label">Nama Anak 2</label>
<div class="col-lg-9">
<input type="text" class="form-control" id="anak2" name="anak2"  >
</div>
</div>


<div class="form-group">
<label for="tgl_lahir_anak2" class="b col-lg-2 control-label">Tgl lahir </label>
<div class="col-lg-9">
<input type="text" class="form-control" id="maskedDate3" name="tgl_lahir_anak2"   placeholder="contoh:31/12/1990">
</div>
</div>





</div>


</div>
<div class="tab-pane fade" id="akun">

<div class="form-horizontal black col-md-12">
 

<script src="<?php echo base_url();?>plug/boostrap/js/jquery.maskedinput.min.js"></script>  
<script>
$("#maskedDate1").mask("99/99/9999");
$("#maskedDate2").mask("99/99/9999");
$("#maskedDate3").mask("99/99/9999");
$("#maskedDate4").mask("99/99/9999");
$("#maskedDate5").mask("99/99/9999");
$("#tgl").mask("99/99/9999");
 
</script>



<script>
function cekpass()
{
	var user=$("[name='username']").val();
	var pass=$("[name='password']").val();
	
	 	$.ajax({
		url:"<?php echo base_url();?>costumer/cekpas",
		type: "POST",
		data:"user="+user+"&pass="+pass,
		success: function(data)
				{
					if(data>0)
					{
						$(".password").addClass("has-error");
						$(".help-block").html("Silahkan ganti username dan password anda");
						document.getElementById("submit").disabled = true;
					}else{
						$(".password").removeClass("has-error");
						$(".help-block").html("");
						document.getElementById("submit").disabled = false;
					}
				},
			});
}
</script>

</div>



</div>

</div>
</div>
</div>
</div>
</div>
</div>

    </div><span class='load'></span>
<button type="submit" onclick="saveAdd()" id="submit" class="btn btn-success pull-right"><i class='fa fa-save'></i> Simpan</button>
  </div>   <!-- /.row -->

</section><!-- /.content -->
  </div>
   </div><!-- /.modal-content -->
		

      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	</form>
  <!-- End Bootstrap modal -->



 <script>
function saveAdd()
	{	
	 $(".load").html('<img src="<?php echo base_url()?>plug/img/load.gif"> Process Simpan...');
	if(method=="edit")
	{
		var url="<?php echo base_url();?>data_owner/update";
	}else{
		var url="<?php echo base_url();?>data_owner/insert";
	}
		$(".load").html("<img src='<?php echo base_url();?>plug/img/load.gif'> Please wait...");
		$('#formulirAgen').ajaxForm({
		url:url,
		type: "post",
		data: $('#formulirAgen').serialize(),
	//	dataType: "JSON",
		success: function(data)
				{
				if(data==false){ alert("Gagal! Data sudah ada pada database"); $(".load").html(""); $("[name='group']").focus(); return false;}
				$(".load").html('<font color="green"><i class="fa fa-check-circle fa-fw fa-lg"></i> Berhasil di simpan</font>');
				table.ajax.reload(null,false);
				$("#formulirAgen")[0].reset();
				//f(".load").html("");
				if(method=="edit")
				{
					$("#modalAdd").modal("hide");
					$(".load").html('');
				}
				},
				
		});
	}
	
	 function hapus(id)
	  {
	    var tanya=window.confirm("Hapus ?");
		if(tanya==false){ return false;};
	  	$.ajax({
		url:"<?php echo base_url();?>data_owner/hapus/"+id,
		type: "POST",
		data:"id="+id,
		success: function(data)
				{
				table.ajax.reload(null,false); //reload datatable ajax 
				},
			});
	  }

</script>	



<script>
function exportData()
{
	window.location.href="<?php echo base_url()?>data_owner/export";
}
function importData()
{
	 $('.msg').html('');
	  $('.hasil').html('');
	$("#modalImport").modal("show");
}
</script>






 <!-- Bootstrap modal -->
  <div class="modal fade" id="modalImport" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
     
      <div class="modal-body form">
<section class="content">
  <div class="row">
    <div class="col-lg-12">
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-download"></i> Import Data Owner <span class='namaGroup'></span></h4>
      </div>
      <!-- general form elements -->
      <div class="box box-primary black">	   <br>
        Silahkan <a href='<?php echo base_url();?>data_owner/downloadFormat'><i class="fa fa-file-excel-o"></i> download format</a> sebelum upload.
          <div class="box-body">                      
            <form role="form" name="uploadfilexl" id="uploadfilexl" action="javascript:void();" method="post" enctype="multipart/form-data">
                <div class="form-group">
                  <input id="userfile" required name="userfile" type="file" class="form-control">
                  <input  name="idGroup" type="hidden">
                </div>				
                <button type="submit" onclick="javascript:simpanfile();" class="btn btn-primary pull-right">
                    <span class="fa fa-upload"></span>&nbsp;Upload
                </button>
                <div class="form-group">
                    <div class="msg"></div>
                    <div class="hasil"></div>
                </div>
            </form>
          </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div>   <!-- /.row -->
</section><!-- /.content -->
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
  
   <?php echo $this->load->view("js/form.phtml"); ?>
  <script type="text/javascript">
function simpanfile(){
    var userfile=$('#userfile').val();
    $('#uploadfilexl').ajaxForm({
     url:'<?php echo base_url();?>data_owner/importData/',
     type: 'post',
     data:{"userfile":userfile},
     beforeSend: function() {
        var percentVal = 'Mengupload 0%';
        $('.msg').html(percentVal);
     },
     uploadProgress: function(event, position, total, percentComplete) {
        var percentVal = 'Mengupload ' + percentComplete + '%';
        $('.msg').html(percentVal);
     },
     beforeSubmit: function() {
      $('.hasil').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Silahkan Tunggu ... ");
     },
     complete: function(xhr) {
        $('.msg').html('');
     }, 
     success: function(resp) {
        $('.hasil').html(resp);
		table.ajax.reload(null,false);
		$("#uploadfilexl")[0].reset();
     },
    });     
};
</script>   


<script>
function detail(id)
{
$("#modalDetailListing").modal("show");
	 $('.dataDetail').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Mohon Tunggu ... ");
	            $.ajax({
                type: "POST",
                dataType: "html",
                url: "<?php echo base_url();?>data_owner/getDataDetail/"+id,
                data: "id=" + id,
                success: function (data) {
		            $(".dataDetail").html(data);
                }
            });
}	
</script>

<script>
function detailProfile(id)
{
		  $("#modalDetail").modal("show");
			 	$.ajax({
				url:"<?php echo base_url();?>data_owner/getDetail/"+id,
				success: function(data)
						{			
						$("#kontenDetail").html(data);
				},
			});
}	
</script>


  
 <div class="modal fade" id="modalDetail" role="dialog" >
  <div class="modal-dialog"  style="width:80%" >

<div class="modal-content" >
<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-info-circle"></i> Informasi Detail</h4>
      </div>
	  <!----------------------------------------------->
	 <div id="kontenDetail"></div>
	  <!----------------------------------------------->
</div>	  
</div>	  
</div>	  

 <!-- Bootstrap modal -->
  <div class="modal fade" id="modalDetailListing" role="dialog">
  <div class="modal-dialog" style="width:80%">
<div class="modal-content" >
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-info-circle"></i> Data Listing <span class='namaGroup'></span></h4>
      </div>
      <div class="modal-body form">
<section class="content">
  <div class="row">
    <div class="col-lg-12 dataDetail">
		<!------------------------->
	
		<!------------------------->
    </div>
  </div>   <!-- /.row -->
</section><!-- /.content -->
  </div>
   </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
  
   <!-- Bootstrap modal -->
  <div class="modal fade" id="modalDetails" role="dialog">
  <div class="modal-dialog" style="width:80%">
  
<div class="modal-content" >
<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-info-circle"></i> Data Listing <span class='namaGroup'></span></h4>
      </div>
      <div class="modal-body form">
	  
<section class="content">
  <div class="row">
  
    <div class="col-lg-12 dataDetails">
		
		<!------------------------->
		<!------------------------->
    </div>
  </div>   <!-- /.row -->
</section><!-- /.content -->
  </div>
   </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
  
  
  