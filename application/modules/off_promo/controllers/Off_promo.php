<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Off_promo extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->m_konfig->validasi_session(array("user","promotion"));
		$this->load->model("M_offpromo","offpromo");
	}
	
	function _template($data)
	{
	$this->load->view('template/main',$data);	
	}
	
	public function index()
	{

	$data['konten']="index";
	$this->_template($data);
	}
		function getDetail($id)
	{
		$data["id"]=$id;
		$this->load->view("getDetail",$data);
	}
	function getPromo()
	{
	$agen=$this->input->post("agen");
	$kelengkapan=$this->input->post("kelengkapan");
	$jenis_promo=$this->input->post("jenis_promox");
	$status_promo=$this->input->post("status_promo");
	$filter['filter']="?agen=$agen&kelengkapan=$kelengkapan&jenis_promo=$jenis_promo&status_promo=$status_promo";
	echo	$this->load->view("getPromo",$filter);
	}
	function ajax_offpromo()
	{
		$list = $this->offpromo->get_dataOff();
        $data = array();
        $no = $_POST['start']+1;
        foreach ($list as $val) {
			if($val->jenis_promo == '1'){
				$jenis_promo = "Koran";
			}else{
				$jenis_promo = "Spanduk";
			}
			
			if($val->status == '1'){
				$status = "Aktif";
			}else{
				$status = "<font color='red'>Tidak Aktif</font>";
			}
			
			$row = array();
			$row[] =  '<input type="checkbox" class="pilih" onclick="pilcek()" name="hapus[]" value="'.$val->kode_prop.'">';
			//$row[] = $no++;
            $row[] = $this->tanggal->ind($val->tgl_promo,"/");
			$row[] = $val->kode_prop;
			$row[] = $val->alamat_detail;
			$row[] = $val->area_listing;
			$row[] = $val->nama;
            $row[] = $val->alamat_promo;
            $row[] = '<img src="'.base_url().'file_upload/img/'.$val->foto_promo.'" width="200px" height="200px" >';
			$row[] = $jenis_promo;
			$row[] = $status;
            $row[] = '
			<a href="#" style="font-size:14px" onclick="editpromo(`' . $val->id_promo .'`)" class=" "><i class="fa fa-edit "> </i> Edit </a>
			| <a href="#" style="font-size:14px" onclick="hapuss(`' . $val->id_promo . '`);" class=" "><i class="fa fa-trash"> </i> Hapus </a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->offpromo->counts(),
            "recordsFiltered" => $this->offpromo->counts(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	function insert()
	{
		echo $this->offpromo->insert();
	}
	function updatePromo()
	{
		echo $this->offpromo->updatePromo();
	}
	function HapusAll()
	{
	echo $this->offpromo->HapusAll();
	}
	function hapus($id)
	{
	echo $this->offpromo->hapus($id);
	}
	
	function export()
	{
		$this->offpromo->export();
	}
	function getEditpromo($id)
	{	
		$data["id"]=$id;
		$this->load->view("formEditpromo",$data);
	}
	
}