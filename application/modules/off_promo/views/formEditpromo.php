<!--  <div class="modal-dialog">
 
<div class="modal-content">
     
      <div class="modal-body form">
   
<section class="content">
  <div class="row">
    <div class="col-lg-12">
	
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-plus-circle"></i><span class='title'> Edit Promosi Offline</span></h4>
      </div>
	<br> -->
<?php
$this->db->where("id_promo",$id);
$db=$this->db->get("data_promo")->row();
?>
<form action="javascript:saveAddPromo()"  id="formEditx" class="form-horizontal black" method="post"  enctype="multipart/form-data"  >
<input type="hidden" name="kode_prop" id="kode_prop" value="<?php echo $db->kode_prop;?>">
<input type="hidden" name="id_promo" id="id_promo" value="<?php echo $id;?>">
<div class="form-group">
<label for="tgl_promo" class="b col-lg-3 control-label">Date</label>
<div class="col-lg-8">
<input type="text" class="form-control" id="tgl_promo"  name="tgl_promo" value="<?php echo $this->tanggal->ind($db->tgl_promo,"/");?>" required >
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>

<label for="nama" class="b col-lg-3 control-label">Images</label>
 
<div class="col-lg-5">
<input type="file" class="form-control" id="foto_promo" name="foto_promo"  >
</div>
 
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
 
<label for="alamat_promo" class="b col-lg-3 control-label">Alamat</label>
<div class="col-lg-8">
<textarea name="alamat_promo" id="alamat_promo" class="form-control" required><?php echo $db->alamat_promo;?></textarea>
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<label for="jenis" class="b col-lg-3 control-label">Jenis</label>
 
<div class="col-lg-5">
<?php                                        
		$arrayT[""] = "==== Pilih Jenis Promosi ====";
		$arrayT["0"] = "SPANDUK";
		$arrayT["1"] = "KORAN";
		$arrayT["2"] = "MAXI BANNER";
		$arrayT["3"] = "PAPAN TANAH";
		$data = $arrayT;
		echo form_dropdown('jenis_promo', $data, $db->jenis_promo, '  id="jenis_promo"  class="form-control" required');
	?>
</div>
<!--
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<label for="status" class="b col-lg-3 control-label">Status</label>
 
<div class="col-lg-5">
<?php                                        
		$arrayP[""] = "==== Pilih Status Promosi ====";
		$arrayP["0"] = "Tidak Aktif";
		$arrayP["1"] = "Aktif";
		$data = $arrayP;
		echo form_dropdown('status', $data, $db->status, '  id="status"  class="form-control" required');
	?>
</div>-->
 
  <div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<div class="col-lg-offset-2 col-lg-9">
<span class='load'></span>
<button type="submit" class="btn btn-success pull-right" ><i class='fa fa-save'></i> Save</button>
</div>
</div>
</form>
<!--
    </div>
  </div>   <!-- /.row -->
<!--
</section><!-- /.content -->
 <!-- </div>
   </div><!-- /.modal-content -->
		

 <!--     </div><!-- /.modal-dialog -->
 
<script>
function saveAddPromo()
	{	
		var url="<?php echo base_url();?>off_promo/updatePromo";
		$(".load").html("<img src='<?php echo base_url();?>plug/img/load.gif'> Please wait...");
		$.ajax({
		url:url,
		type: "post",
		data: $('#formEditx').serialize(),
	//	dataType: "JSON",
		success: function(data)
				{
					//alert(data);
					window.location.href="<?php echo base_url();?>off_promo";	
				},
				
		});
	}
</script>
<script src="<?php echo base_url();?>plug/boostrap/js/jquery.maskedinput.min.js"></script>  
<script>
$("#tgl_promo").mask("99/99/9999");
</script>