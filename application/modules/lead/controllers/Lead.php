<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lead extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->m_konfig->validasi_session(array("user","promotion"));
		$this->load->model("M_lead","lead");
	}
	
	function _template($data)
	{
	$this->load->view('template/main',$data);	
	}
	
	public function index()
	{

	$data['konten']="index";
	$this->_template($data);
	}
		function getDetail($id)
	{
		$data["id"]=$id;
		$this->load->view("getDetail",$data);
	}
	function getLead()
	{
	$agen=$this->input->post("agen");
	$tahun=$this->input->post("tahun");
	$bulan=$this->input->post("bulan");
	$filter['filter']="?agen=$agen&tahun=$tahun&bulan=$bulan";
	echo	$this->load->view("getLead",$filter);
	}
	function getAddLead($id)
	{	$data["id"]=$id;
		$this->load->view("FormLead",$data);
	}
	function ajax_lead()
	{
		$list = $this->lead->get_dataLead();
        $data = array();
        $no = $_POST['start']+1;
        foreach ($list as $val) {
		
			$row = array();
			$row[] =  '<input type="checkbox" class="pilih" onclick="pilcek()" name="hapus[]" value="'.$val->id_lead.'">';
			//$row[] = $no++;
            $row[] = $this->tanggal->ind($val->tgl_lead,"/");
			$row[] = $val->nama;
			$row[] = $val->buyer;
			$row[] = $val->hp;
			$row[] = number_format($val->budget,0,",",".");
			$row[] = $val->lokasi;
			$row[] = $val->ket;
            $row[] = '
			<a href="#" style="font-size:14px" onclick="editlead(`' . $val->id_lead .'`)" class=" "><i class="fa fa-edit "> </i> Edit </a>
			| <a href="#" style="font-size:14px" onclick="hapuss(`' . $val->id_lead . '`);" class=" "><i class="fa fa-trash"> </i> Hapus </a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->lead->counts(),
            "recordsFiltered" => $this->lead->counts(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	function insert()
	{
		echo $this->lead->insert();
	}
	function update()
	{
		echo $this->lead->update();
	}
	function HapusAll()
	{
	echo $this->lead->HapusAll();
	}
	function hapus($id)
	{
	echo $this->lead->hapus($id);
	}
	
	function export()
	{
		$this->lead->export();
	}
	function getEditlead($id)
	{	
		$data["id"]=$id;
		$this->load->view("formEditlead",$data);
	}
	
}