<form action="javascript:saveAddLead()"  id="formLead" class="form-horizontal black" method="post"  enctype="multipart/form-data"  >
<div class="form-group">
<label for="tgl_promo" class="b col-lg-3 control-label">Date</label>
<div class="col-lg-8">
<input type="text" class="form-control" id="tgl_lead"  name="tgl_lead" value="" >
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<label for="jenis" class="b col-lg-3 control-label">Agen</label>
<div class="col-lg-8">
<?php
    if($this->session->userdata("id")==64){
		$ref_agen = $this->reff->getAgenAjeng();
	}elseif($this->session->userdata("id")==151){
		$ref_agen = $this->reff->getAgenRhafa();
	}elseif($this->session->userdata("id")==133){
		$ref_agen = $this->reff->getAgenKiki();
	}elseif($this->session->userdata("id")==146){
		$ref_agen = $this->reff->getAgenYudi();
	}elseif($this->session->userdata("id")==152){
		$ref_agen = $this->reff->getAgenVivi();
	}else{
		$ref_agen = $this->reff->getAgen();
    }
	$array_agen[""] = "==== choose ====";
    foreach ($ref_agen as $val) {
    $array_agen[$val->kode_agen] = $val->nama;
    }
    $data = $array_agen;
    echo form_dropdown('agen', $data, '', ' id="agenz" class="select2-container" style="width:100%"');
?>
 <span class="help-block err_agen"></span>
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<label for="alamat_promo" class="b col-lg-3 control-label">Buyer</label>
<div class="col-lg-8">
<input type="text" class="form-control" id="buyer"  name="buyer" value="" >
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<label for="alamat_promo" class="b col-lg-3 control-label">HP</label>
<div class="col-lg-8">
<input type="text" class="form-control" id="hp"  name="hp" value="" >
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<label for="alamat_promo" class="b col-lg-3 control-label">Budget</label>
<div class="col-lg-8">
<input type="text" class="form-control" id="budget"  name="budget" onkeydown="return numbersonly(this, event);" value="" >
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<label for="alamat_promo" class="b col-lg-3 control-label">Location</label>
<div class="col-lg-8">
<input type="text" class="form-control" id="lokasi"  name="lokasi" value="" >
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<label for="alamat_promo" class="b col-lg-3 control-label">Result</label>
<div class="col-lg-8">
<input type="text" class="form-control" id="ket"  name="ket" value="" >
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<div class="col-lg-offset-2 col-lg-9">
<span class='load'></span>
<button type="submit" class="btn btn-success pull-right" onclick="saveAddLead()"><i class='fa fa-save'></i> Save</button>
</div>
</div>
</form>


<?php echo $this->load->view("js/form.phtml"); ?>
<script>
function saveAddLead()
	{	
		var url="<?php echo base_url();?>lead/insert";
		$(".load").html("<img src='<?php echo base_url();?>plug/img/load.gif'> Please wait...");
		$("#formLead").ajaxForm({
		url:url,
		type: "post",
		data: $('#formShowing').serialize(),
	//	dataType: "JSON",
		success: function(data)
				{
						  //if(data==false){ alert("Gagal! Data Sudah Ada"); $(".load").html(""); $("[name='kategori']").focus(); return false;}
						  closemodal("modalLead");
						  table.ajax.reload(null,false); //reload datatable ajax 
				},
				
		});
	}
</script>

<script src="<?php echo base_url();?>plug/boostrap/js/jquery.maskedinput.min.js"></script>  
<script>
$("#tgl_lead").mask("99/99/9999");
</script>
<script src="<?php echo base_url() ?>plug/boostrap/js/select2.min.js"></script>
 <script>
  $('#agenz').select2();
</script>