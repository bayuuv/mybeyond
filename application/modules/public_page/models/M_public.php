<?php

class M_public extends CI_Model  {
    
		
	function __construct()
    {
        parent::__construct();
    }
	function get_query()
	{
	$q=$this->input->get("q");
	 
	$this->db->order_by("id","DESC");
	return $this->db->get("web_artikel");
	}
	function view_($num, $offset)  {
		$this->db->limit($num, $offset);
		$query = $this->get_query();
	return $query->result(); 
	}
	/*------------------------TESTIMONI---------------------------------*/
	function get_query_testimoni()
	{
	$this->db->order_by("id","DESC");
	return $this->db->get("testimoni");
	}
	function view_testi($num, $offset)  {
   $this->db->limit($num, $offset);
   $query = $this->get_query_testimoni();
  return $query->result(); 
  }
	/*------------------------AGEN---------------------------------*/
	//$sql=$this->db->query("select * from data_agen where jabatan='1' and aktifasi='1' order by id_agen ASC LIMIT 27");
	function get_query_agen()
	{
	$categories = array('1', '11','12');
	$this->db->select('a.*,b.nama AS jabatan');
	$this->db->from('data_agen a');
	$this->db->where_in("a.jabatan",$categories);
	$this->db->where("a.aktifasi","1");
	$this->db->join('tr_jabatan b', 'b.id = a.jabatan', 'left');
	$this->db->order_by("(SELECT COUNT(id_prop) as jumlah FROM data_property WHERE agen = a.kode_agen AND status = '0')","DESC");
	return $this->db->get();
	}
	function view_agen($num, $offset)  {
   $this->db->limit($num, $offset);
   $query = $this->get_query_agen();
  return $query->result(); 
  }
	/*------------------------AGEN LISTING---------------------------------*/
	//$sql=$this->db->query("select * from data_agen where jabatan='1' and aktifasi='1' order by id_agen ASC LIMIT 27");
	function get_query_agen_listing($id)
	{
		$kode_prop=$this->db->query("SELECT kode_agen from data_agen where id_agen='".$id."'")->row();
		$kode_prop=isset($kode_prop->kode_agen)?($kode_prop->kode_agen):"";
	$this->db->order_by("id_prop","DESC");
	//$this->db->where("agen",$kode_prop);
	//$this->db->where("status","0");
	$where = "agen='$kode_prop' AND status IN ('0','1')";
    $this->db->where($where);
	return $this->db->get("data_property");
	}
	function view_agen_listing($num, $offset,$id)  {
   $this->db->limit($num, $offset);
   $query = $this->get_query_agen_listing($id);
  return $query->result(); 
  }
	
	
}