 <div class="row">
    <div class="col-sm-9">
    <h2>Real Estate Agency Template</h2>

    <div class="agency-detail">
        <div class="row">
            <div class="col-sm-3">
                <div class="agency-detail-picture">
                    <img src="assets/img/tmp/agents/medium/4.jpg" alt="" class="img-responsive">
                </div><!-- /.agent-detail-picture -->
            </div>

            <div class="col-sm-8">
                <p>
                    Donec faucibus metus sed eros euismod, eu viverra augue viverra. Sed auctor vel ligula nec molestie. Aenean a gravida metus, non sagittis nisl. Nunc quis sem sit amet leo tincidunt laoreet. Praesent a tempor nisl, id suscipit elit. Cras dolor turpis, posuere ut mollis id, rutrum eget augue. Aenean ut ligula quis neque ullamcorper tristique ut a ante. Vivamus enim erat, sollicitudin non facilisis accumsan, dictum nec libero.
                </p>

                <ul class="social social-boxed">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="#"><i class="fa fa-vimeo-square"></i></a></li>
                    <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                </ul><!-- /.social-->
            </div>
        </div><!-- /.row -->
    </div><!-- /.agency-detail -->

    <h2>Assigned Properties</h2>

    <div class="row">
        <div class="property-item property-featured col-sm-6 col-md-4">
            <div class="property-box">
                <div class="property-box-inner">
                    <h3 class="property-box-title"><a href="#">Jefferson Blvd</a></h3>
                    <h4 class="property-box-subtitle"><a href="#">Brooklyn</a></h4>
                    <div class="property-box-label property-box-label-primary">Rent</div><!-- /.property-box-label -->

                    <div class="property-box-picture">
                        <div class="property-box-price">$ 545 000</div><!-- /.property-box-price -->
                        <div class="property-box-picture-inner">
                            <a href="#" class="property-box-picture-target">
                                <img src="assets/img/tmp/properties/medium/7.jpg" alt="">
                            </a><!-- /.property-box-picture-target -->
                        </div><!-- /.property-picture-inner -->
                    </div><!-- /.property-picture -->

                    <div class="property-box-meta">
                        <div class="property-box-meta-item col-sm-3">
                            <strong>1</strong>
                            <span>Baths</span>
                        </div><!-- /.col-sm-3 -->

                        <div class="property-box-meta-item col-sm-3">
                            <strong>2</strong>
                            <span>Beds</span>
                        </div><!-- /.col-sm-3 -->

                        <div class="property-box-meta-item col-sm-3">
                            <strong>194</strong>
                            <span>Area</span>
                        </div><!-- /.col-sm-3 -->

                        <div class="property-box-meta-item col-sm-3">
                            <strong>1</strong>
                            <span>Garages</span>
                        </div><!-- /.col-sm-3 -->
                    </div><!-- /.property-box-meta -->
                </div><!-- /.property-box-inner -->
            </div><!-- /.property-box -->
        </div><!-- /.property-item -->

        <div class="property-item property-rent col-sm-6 col-md-4">
            <div class="property-box">
                <div class="property-box-inner">
                    <h3 class="property-box-title"><a href="#">Bedford Ave</a></h3>
                    <h4 class="property-box-subtitle"><a href="#">Manhattan</a></h4>

                    <div class="property-box-picture">
                        <div class="property-box-price">$ 2400 / month</div><!-- /.property-box-price -->
                        <div class="property-box-picture-inner">
                            <a href="#" class="property-box-picture-target">
                                <img src="assets/img/tmp/properties/medium/3.jpg" alt="">
                            </a><!-- /.property-box-picture-target -->
                        </div><!-- /.property-picture-inner -->
                    </div><!-- /.property-picture -->

                    <div class="property-box-meta">
                        <div class="property-box-meta-item col-sm-3">
                            <strong>3</strong>
                            <span>Baths</span>
                        </div><!-- /.col-sm-3 -->

                        <div class="property-box-meta-item col-sm-3">
                            <strong>2</strong>
                            <span>Beds</span>
                        </div><!-- /.col-sm-3 -->

                        <div class="property-box-meta-item col-sm-3">
                            <strong>330</strong>
                            <span>Area</span>
                        </div><!-- /.col-sm-3 -->

                        <div class="property-box-meta-item col-sm-3">
                            <strong>1</strong>
                            <span>Garages</span>
                        </div><!-- /.col-sm-3 -->
                    </div><!-- /.property-box-meta -->
                </div><!-- /.property-box-inner -->
            </div><!-- /.property-box -->
        </div><!-- /.property-item -->

        <div class="property-item property-sale col-sm-6 col-md-4">
            <div class="property-box">
                <div class="property-box-inner">
                    <h3 class="property-box-title"><a href="#">Emerson Street</a></h3>
                    <h4 class="property-box-subtitle"><a href="#">Kingman Park</a></h4>
                    <div class="property-box-label">Sale</div><!-- /.property-box-label -->

                    <div class="property-box-picture">
                        <div class="property-box-price">$ 145 000</div><!-- /.property-box-price -->
                        <div class="property-box-picture-inner">
                            <a href="#" class="property-box-picture-target">
                                <img src="assets/img/tmp/properties/medium/12.jpg" alt="">
                            </a><!-- /.property-box-picture-target -->
                        </div><!-- /.property-picture-inner -->
                    </div><!-- /.property-picture -->

                    <div class="property-box-meta">
                        <div class="property-box-meta-item col-sm-3">
                            <strong>1</strong>
                            <span>Baths</span>
                        </div><!-- /.col-sm-3 -->

                        <div class="property-box-meta-item col-sm-3">
                            <strong>3</strong>
                            <span>Beds</span>
                        </div><!-- /.col-sm-3 -->

                        <div class="property-box-meta-item col-sm-3">
                            <strong>335</strong>
                            <span>Area</span>
                        </div><!-- /.col-sm-3 -->

                        <div class="property-box-meta-item col-sm-3">
                            <strong>2</strong>
                            <span>Garages</span>
                        </div><!-- /.col-sm-3 -->
                    </div><!-- /.property-box-meta -->
                </div><!-- /.property-box-inner -->
            </div><!-- /.property-box -->
        </div><!-- /.property-item -->

        <div class="property-item property-featured col-sm-6 col-md-4">
            <div class="property-box">
                <div class="property-box-inner">
                    <h3 class="property-box-title"><a href="#">Evergreen Tr</a></h3>
                    <h4 class="property-box-subtitle"><a href="#">Civic Betterment</a></h4>
                    <div class="property-box-label property-box-label-primary">Rent</div><!-- /.property-box-label -->

                    <div class="property-box-picture">
                        <div class="property-box-price">$ 350 000</div><!-- /.property-box-price -->
                        <div class="property-box-picture-inner">
                            <a href="#" class="property-box-picture-target">
                                <img src="assets/img/tmp/properties/medium/8.jpg" alt="">
                            </a><!-- /.property-box-picture-target -->
                        </div><!-- /.property-picture-inner -->
                    </div><!-- /.property-picture -->

                    <div class="property-box-meta">
                        <div class="property-box-meta-item col-sm-3">
                            <strong>2</strong>
                            <span>Baths</span>
                        </div><!-- /.col-sm-3 -->

                        <div class="property-box-meta-item col-sm-3">
                            <strong>1</strong>
                            <span>Beds</span>
                        </div><!-- /.col-sm-3 -->

                        <div class="property-box-meta-item col-sm-3">
                            <strong>166</strong>
                            <span>Area</span>
                        </div><!-- /.col-sm-3 -->

                        <div class="property-box-meta-item col-sm-3">
                            <strong>2</strong>
                            <span>Garages</span>
                        </div><!-- /.col-sm-3 -->
                    </div><!-- /.property-box-meta -->
                </div><!-- /.property-box-inner -->
            </div><!-- /.property-box -->
        </div><!-- /.property-item -->

        <div class="property-item property-rent col-sm-6 col-md-4">
            <div class="property-box">
                <div class="property-box-inner">
                    <h3 class="property-box-title"><a href="#">McLaugh Ave</a></h3>
                    <h4 class="property-box-subtitle"><a href="#">Palo Alto, SA</a></h4>

                    <div class="property-box-picture">
                        <div class="property-box-price">$ 1900 / month</div><!-- /.property-box-price -->
                        <div class="property-box-picture-inner">
                            <a href="#" class="property-box-picture-target">
                                <img src="assets/img/tmp/properties/medium/4.jpg" alt="">
                            </a><!-- /.property-box-picture-target -->
                        </div><!-- /.property-picture-inner -->
                    </div><!-- /.property-picture -->

                    <div class="property-box-meta">
                        <div class="property-box-meta-item col-sm-3">
                            <strong>3</strong>
                            <span>Baths</span>
                        </div><!-- /.col-sm-3 -->

                        <div class="property-box-meta-item col-sm-3">
                            <strong>2</strong>
                            <span>Beds</span>
                        </div><!-- /.col-sm-3 -->

                        <div class="property-box-meta-item col-sm-3">
                            <strong>199</strong>
                            <span>Area</span>
                        </div><!-- /.col-sm-3 -->

                        <div class="property-box-meta-item col-sm-3">
                            <strong>2</strong>
                            <span>Garages</span>
                        </div><!-- /.col-sm-3 -->
                    </div><!-- /.property-box-meta -->
                </div><!-- /.property-box-inner -->
            </div><!-- /.property-box -->
        </div><!-- /.property-item -->

        <div class="property-item property-sale col-sm-6 col-md-4">
            <div class="property-box">
                <div class="property-box-inner">
                    <h3 class="property-box-title"><a href="#">Culver Blvd</a></h3>
                    <h4 class="property-box-subtitle"><a href="#">Silicon Valley, SA</a></h4>
                    <div class="property-box-label">Sale</div><!-- /.property-box-label -->

                    <div class="property-box-picture">
                        <div class="property-box-price">$ 545 000</div><!-- /.property-box-price -->
                        <div class="property-box-picture-inner">
                            <a href="#" class="property-box-picture-target">
                                <img src="assets/img/tmp/properties/medium/1.jpg" alt="">
                            </a><!-- /.property-box-picture-target -->
                        </div><!-- /.property-picture-inner -->
                    </div><!-- /.property-picture -->

                    <div class="property-box-meta">
                        <div class="property-box-meta-item col-sm-3">
                            <strong>3</strong>
                            <span>Baths</span>
                        </div><!-- /.col-sm-3 -->

                        <div class="property-box-meta-item col-sm-3">
                            <strong>3</strong>
                            <span>Beds</span>
                        </div><!-- /.col-sm-3 -->

                        <div class="property-box-meta-item col-sm-3">
                            <strong>243</strong>
                            <span>Area</span>
                        </div><!-- /.col-sm-3 -->

                        <div class="property-box-meta-item col-sm-3">
                            <strong>3</strong>
                            <span>Garages</span>
                        </div><!-- /.col-sm-3 -->
                    </div><!-- /.property-box-meta -->
                </div><!-- /.property-box-inner -->
            </div><!-- /.property-box -->
        </div><!-- /.property-item -->
    </div><!-- /.row -->

    <h2>Contact Agency Directly</h2>

    <form method="post" action="?" class="box">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control">
                </div><!-- /.form-group -->
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label>Subject</label>
                    <input type="text" class="form-control">
                </div><!-- /.form-group -->
            </div>
        </div><!-- /.row -->

        <div class="form-group">
            <label>Message</label>
            <textarea rows="8" class="form-control"></textarea>
        </div><!-- /.form-group -->

        <div class="form-group">
            <input type="submit" value="Contact Agent" class="btn btn-primary btn-inversed">
        </div><!-- /.form-group -->
    </form>
    </div>

    <div class="col-sm-3">
        <div class="sidebar">
            <div class="sidebar-inner">
                <div class="widget">
    <h3 class="widget-title">Contact</h3>

    <div class="widget-content">
        <form method="post" action="?">
            <div class="form-group">
                <label>Name</label>
                <input type="text" value="" class="form-control">
            </div><!-- /.form-group -->

            <div class="form-group">
                <label>Subject</label>
                <input type="text" value="" class="form-control">
            </div><!-- /.form-group -->

            <div class="form-group">
                <label>Message</label>
                <textarea class="form-control"></textarea>
            </div><!-- /.form-group -->

            <div class="form-group">
                <input type="text" value="Contact" class="btn btn-block btn-primary btn-inversed">
            </div><!-- /.form-group -->
        </form>
    </div><!-- /.widget-content -->
</div><!-- /.widget -->               


</div>
</div>
</div>
</div>