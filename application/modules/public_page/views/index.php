<h2 class="center">Best Rated Properties</h2>

 
<!-- /.property-filter -->

<div class="properties-items">
<div id="konten_filter">
	 
	 
	<div id="load_more_ctnt">
	<?php
	$page=0;$isigambar="";
	$sql=$this->db->query("select * from data_property where status IN ('0','1') ORDER BY gambar1 DESC,gambar2 DESC,gambar_utama DESC   LIMIT 12");
	foreach($sql->result() as $val)
	{	
		$id=$val->id_prop;
		if($val->status=="0"){
    		if($val->type_jual=="jual")
    		{
    			$class="property-box-label property-box-label-primary";
    		}else
    		{
    			$class="property-box-label";
    		}
		}else{
		    $class="property-box-label property-box-label-danger";
		}
		$gambar="not-avaliable.jpg";
		if(strlen($val->gambar_utama)>3)
		{
			$utama=$val->gambar_utama;
			$gambar=$val->$utama;
			$isigambar=$gambar;
		}
		
		if(strlen($isigambar)<3)
		{
		 	$gambar="not-avaliable.jpg";
		}
	?>
		 
	 <div class="property-item property-sale col-sm-6 col-md-3" style="cursor:pointer" onclick="getlink('<?php echo base_url()?>public_page/show_listing/<?php echo $val->id_prop;?>/<?php echo str_replace(" ","-",$val->desc)?>')">
        <div class="property-box" style="border-radius:15px">
            <div class="property-box-inner">
                <h4 class="property-box-title" style="min-height:50px;max-height:50px"> <a href="javascript:void(0)"><?php echo $this->reff->titleWeb($val->kode_prop);?></a></h4>
                <h4 class="property-box-subtitle"> ID : <?php echo $val->kode_prop;?> </h4>
                <div class="<?php echo $class;?>">
                    <?php 
                    if($val->status=="0")
                    { 
                        echo $val->type_jual;
                    }else{   
                        if($val->type_jual=="jual"){
                            echo 'TERJUAL';
                        }elseif($val->type_jual=="sewa"){
                            echo 'TERSEWA';
                        }
                    } 
                     ?>
                </div>
                <!-- /.property-box-label -->
                <div class="property-box-picture" style="margin-top:-30px">
                    <div class="property-box-price">Rp <?php echo number_format($val->harga+$val->fee_up,0,",",".")?></div>
                    <!-- /.property-box-price -->
                    <div class="property-box-picture-inner">
                      <a  href="javascript:void(0)" class="property-box-picture-target">
                            <img height="270px" src="<?php echo base_url()?>file_upload/img/<?php echo $gambar;?>" alt="">
                      </a>
                    </div>
                    <!-- /.property-picture-inner -->
                </div> 
                <!-- /.property-picture -->
				
                <div class="property-box-meta">
                    <div class="property-box-meta-item col-xs-6 col-sm-6">
                        <strong><?php echo isset($val->luas_tanah)?($val->luas_tanah):"-";?> M<sup>2</sup></strong>
                        <span>LT</span>
                    </div>
                    <!-- /.col-sm-3  

                    <div class="property-box-meta-item col-xs-4 col-sm-4">
                        <strong><?php echo isset($val->kamar_tidur)?($val->kamar_tidur):"-";?></strong>
                        <span>Beds</span>
                    </div>
                    <!-- /.col-sm-3 -->

                   

                    <div class="property-box-meta-item col-xs-6 col-sm-6">
                        <strong><?php echo  isset($val->luas_bangunan)?($val->luas_bangunan):"-";?>  M<sup>2</sup></strong>
                        <span>LB</span>
                    </div>
                    <!-- /.col-sm-3 -->
                </div>
                <!-- /.property-box-meta -->
            </div>
            <!-- /.property-box-inner -->
        </div> 
        <!-- /.property-box -->
    </div>
    <!-- /.property-item -->
	<?php	 $page++; } ?>
	</div>
	<center id="load_more_<?php echo $page; ?>"><div class="row"></div>
	<div class="more_div"> <div  class="more_tab">
	<button  onclick="moreLoad(`<?php echo $page; ?>`)" class="more_buttons btn btn-primary"  id="<?php echo $page; ?>">Load More Content</button> </div>
	</div>
	</center>
	
	
</div> 
</div> 
