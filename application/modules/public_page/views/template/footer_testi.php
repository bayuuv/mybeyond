 <link href="<?php echo base_url()?>plug/testi/css/responsive_bootstrap_carousel_mega_min.css" rel="stylesheet" media="all">
 <div class="background-secondary background-map fullwidth"><br>
 
<div>
		<div id="testimonial_carousel" class="carousel testimonial_carousel_fade  " data-ride="carousel" data-interval="2000" data-pause="hover">

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
			<?php
			$db=$this->db->get("testimoni")->result();
			$i=1;
			foreach($db as $val)
			{
			if($i==1){ $aktif="active";}else{ $aktif="";}	
				?>
				<!--========= First slide =========-->
				<div class="item <?php echo $aktif;?>">
					<div class="testimonial_carousel_caption">
					<?php if(strlen($val->poto)>4){?>
						 <img  src="<?php echo base_url()?>file_upload/img/<?php echo $val->poto;?>"   
						 style="border-radius:10px;padding-right:5px;margin-top:15px;max-width:90px">
					<?php } ?>	 
						<h3><a href="#"><?php echo $val->nama;?></a></h3>
						
					
							<ul class="testimonial_rating" style="margin-top:-10px">
						<?php for($i=1;$i<=$val->bintang;$i++){?>
                            <i class="fa fa-star"  style="color:orange" ></i>
                        <?php } 
						$sisa=5-($i-1);
						for($y=1;$y<=$sisa;$y++)
						{
							echo ' <i class="fa fa-star" style="color:#858c8c"></i>';
						}
						?>
						</ul>
						<span  style="color:black;font-size:15px"><i class="fa fa-quote-left"></i> <?php echo $val->testimoni;?> <i class="fa fa-quote-right"></i> </span>
						<a href="#" style="margin-top:5px"><?php echo $val->jabatan;?></a>
						<p style="margin-top:-20px"><?php echo $val->perusahaan;?></p>
					</div>
				</div>
			<?php $i++; } ?>
			</div>
<br>
			<!--========= Indicators  
			<ol class="carousel-indicators testimonial_carousel_indicators">
			<?php
			//$db=$this->db->get("testimoni")->result();$i=0;
			//foreach($db as $val)
			//{
			//if($i==0){ $aktif="active";}else{ $aktif="";}	
				?>
				<li data-target="#testimonial_carousel" data-slide-to="<?php //echo $i;?>" class="<?php //echo $aktif;?>"></li>
			<?php //$i++;} ?>	 
			</ol> -->

		</div> <!--*-*-*-*-*-*-*-*-*-*- END BOOTSTRAP CAROUSEL *-*-*-*-*-*-*-*-*-*-->
		</div> <!--*-*-*-*-*-*-*-*-*-*- END BOOTSTRAP CAROUSEL *-*-*-*-*-*-*-*-*-*-->
		</div> <!--*-*-*-*-*-*-*-*-*-*- END BOOTSTRAP CAROUSEL *-*-*-*-*-*-*-*-*-*-->


		 

	<!--======= jQuery =========-->
	

	<!--======= Bootstrap =========-->
	<script src="<?php echo base_url()?>plug/testi/js/bootstrap.min.js"></script>
	
	<!--======= Touch Swipe =========-->
	<script src="<?php echo base_url()?>plug/testi/js/jquery.touchSwipe.min.js"></script>
	
	<!--======= Customize =========-->
	<script src="<?php echo base_url()?>plug/testi/js/responsive_bootstrap_carousel.js"></script>

 