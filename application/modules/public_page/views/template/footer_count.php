<div class="block-contents fullwidth background-primary background-map clearfix">
    <div class="block-content-inner row">
         
        
 
			<div class="center" style="margin-top:20px;color:white">
				<h2 ><?php echo number_format($this->reff->totalListingReady(),0,",",".");?> Properties In Our Directory</h2>
			</div><!-- /.center -->

			<div class="row" style="padding:10px">
					<?php 
					$getTopProp=$this->reff->getTopProp(6);
					foreach($getTopProp as $top)
					{
					?>
					<div class="col-sm-2">
					<div class="block-stats background-dots background-primary color-white"  style="max-width:120px;max-height:80px">
						<strong><?php echo $top->jml;?></strong>
						<span> <?php echo $this->reff->getNamaJenis($top->jenis_prop);?> </span>
					</div><!-- /.block-stats -->
					</div>
					<?php } ?>
			</div><!-- /.row -->
         
		
    </div><!-- /.block-content-inner -->
</div><!-- /.block-content -->        
