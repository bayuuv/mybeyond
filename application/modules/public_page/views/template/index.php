<!doctype html>
<html itemscope itemtype="http://schema.org/Article">
<head>  
<?php
$uri=$this->uri->segment(4);
if(strlen($uri)>5)
{
$val=$this->reff->getDataProperty($this->uri->segment(3));
$g=isset($val->gambar1)?($val->gambar1):"";
?>

    <title>    Di<?php echo $val->type_jual; ?> <?php echo $this->reff->getNamaJenis($val->jenis_prop); ?> di <?php echo $this->reff->getNamaKab($val->id_kab); ?> - <?php echo $val->desc;?> - Hubungi <?php echo $this->reff->getNamaAgen($val->agen)?> dari Beyond Property  </title>
 <?php  $titile_share=str_replace("-"," ",$this->uri->segment(4));?>
 
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /> 
<meta itemprop="image" content="<?php echo base_url()?>file_upload/img/<?php echo $g;?>">
<meta itemprop="name" content="Beyond Property Bandung">
<meta itemprop="keywords" content="Di<?php echo $val->type_jual; ?> <?php echo $this->reff->getNamaJenis($val->jenis_prop); ?> di <?php echo $this->reff->getNamaKab($val->id_kab); ?>, <?php echo $val->desc;?>">
<meta itemprop="description" content="Di<?php echo $val->type_jual; ?> <?php echo $this->reff->getNamaJenis($val->jenis_prop); ?> di <?php echo $this->reff->getNamaKab($val->id_kab); ?> <?php echo $val->desc;?> dengan harga Rp <?php echo number_format($val->harga+$val->fee_up,0,",",".")?>, Segera Hubungi Agent Kami : <?php echo $this->reff->getNamaAgen($val->agen)?> Beyond Property">
<!-- Twitter Card data -->
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@publisher_handle">
<meta name="twitter:title" content=" Beyond Property | <?php echo $titile_share; ?>">
<meta name="twitter:description" content="Di<?php echo $val->type_jual; ?> <?php echo $this->reff->getNamaJenis($val->jenis_prop); ?> di <?php echo $this->reff->getNamaKab($val->id_kab); ?> - <?php echo $val->desc;?> - Hubungi <?php echo $this->reff->getNamaAgen($val->agen)?> dari Beyond Property">
<meta name="twitter:creator" content="@beyond_offc">
<!-- Twitter summary card with large image must be at least 280x150px -->
<meta name="twitter:image:src" content="<?php echo base_url()?>file_upload/img/<?php echo $g;?>">

<!-- Open Graph data for facebook-->
<meta property="og:title" content=" Beyond Property | <?php echo $titile_share; ?>" />
<meta property="og:type" content="article" />
<meta property="og:url" content="<?php echo base_url()."public_page/show_listing/".$id."/".$this->uri->segment(4); ?>" />
<meta property="og:image" content="<?php echo base_url()?>file_upload/img/<?php echo $g;?>" />
<meta property="og:description" content="Di<?php echo $val->type_jual; ?> <?php echo $this->reff->getNamaJenis($val->jenis_prop); ?> di <?php echo $this->reff->getNamaKab($val->id_kab); ?> - <?php echo $val->desc;?> - Hubungi <?php echo $this->reff->getNamaAgen($val->agen)?> dari Beyond Property" />
<meta property="og:site_name" content="Beyond Property Bandung" />
<meta property="article:published_time" content="<?php echo date('Y-m-d');?>T05:59:00+01:00" />
<meta property="article:modified_time" content="<?php echo date('Y-m-d');?>T19:08:47+01:00" />
<meta property="article:section" content="Article Section" />
<meta property="article:tag" content="Beyond Agen property, perusahaan property,agen property bandung,beyond property" />
<meta property="fb:admins" content="100000343840075" />
<meta property="fb:app_id" content="185337818678158" />
 
    
<?php }else{?>
    
     <title>
       Beyond Property | Agen Properti Bandung
    </title>
    <meta charset="utf-8">
    <meta name="description" content="Beyond Property Bandung">
    <meta name="keywords" content="beyond, beyond property,jual rumah, dijual rumah,real estate agent,broker,agent,real estate,property,bandung,hunian terbaik dibandung,hunian indah bandung">
    <meta name="author" content="Beyond Property">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    
    
<?php } ?>
   
<!--------------------------------->
	  <!-- Owl stylesheet -->
<script src="<?php echo base_url()?>plug/testi/js/jquery-1.12.4.min.js"></script>
	<script src="<?php echo base_url()?>blog/assets/bootstrap/js/bootstrap.js"></script>
	<script src="<?php echo base_url()?>blog/assets/script.js"></script>
 
	<script src="<?php echo base_url()?>blog/assets/owl-carousel/owl.carousel.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>blog/assets/slitslider/css/style.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>blog/assets/slitslider/css/custom.css" />
    <script type="text/javascript" src="<?php echo base_url()?>blog/assets/slitslider/js/modernizr.custom.79639.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>blog/assets/slitslider/js/jquery.ba-cond.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>blog/assets/slitslider/js/jquery.slitslider.js"></script>
	
	<!-- slitslider -->
	<!--------------------------------->
    <link type="image/x-icon" href="http://mybeyond.co.id/file_upload/img/loggo.jpg" rel="shortcut icon"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>public_page_asset/assets/libraries/font-awesome/css/font-awesome.css" media="screen, projection">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>public_page_asset/assets/libraries/jquery-bxslider/jquery.bxslider.css" media="screen, projection">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>public_page_asset/assets/libraries/flexslider/flexslider.css" media="screen, projection">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>public_page_asset/assets/css/variants/cyan.css" media="screen, projection" id="css-main">

    <link href="http://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet" type="text/css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>public_page_asset/assets/css/slippry.css" media="screen, projection" id="css-main">
	
<style>
 
@media only screen and (max-width:632px)
{
	.css-loggo{
	 max-height:70px;
	 margin-top:-15px
	}
	.m-hide{
		display:none;
	}
}
.to_top{
    display: inline-block;
    padding: 6px 10px;
    z-index:87979879;
    position: fixed;
    right: 10px;
    cursor: pointer;
    bottom: 10px;
    font-weight: bold;
}

@media only screen and (min-width:632px)
{
	.css-loggo{
	 max-height:100px;
	 width:210px;
	 margin-top:-22px
	}
	.sd{
		height:70px;
	}
}
</style>

	 
</head>

<body>
    <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=185337818678158";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


<!--------------------------------------------------------->
  	<?php 
			$uri=$this->uri->segment(2);
				if($uri=="network")
				{	$cp=" "; ?>
				    
			<?php	}else{ $cp="color-primary"; ?>
				     
			<?php	}	?>
<div id="wrapper">
    <div id="header-wrapper">
        <div id="header">
    <div id="header-inner">
        <div class="header-bar">
            <div class="container">
              

            </div><!-- /.container -->
        </div><!-- /.header-bar -->

          <div class="header-top  background-secondary background-map "  >
            <div class="container   ">	
			<?php
			if($this->uri->segment(2)=="network")
			{?>
			<img class='css-loggo' src="<?php echo base_url()?>plug/img/web-02.png" height="90px;margin-top:-10px" style="position:absolute;">
			<?php }else{?>
			<img class='css-loggo' src="<?php echo base_url()?>plug/img/watermark.png" height="90px;margin-top:-10px" style="position:absolute;">
			<?php } ?>
                <div class="header-identity" style="margin-left:250px">
	
                <!--      <a href="<?php echo base_url()?>" class="header-identity-target" style="height:100px">
				 
                       <span class="header-icon"><i class="fa fa-home"></i></span> 
                   <span class="header-title">BEYOND <span class='mobile-hide'>PROPERTY</span> </span >
                        <span class="header-slogan" style="color:black"><br>
                            <a href="http://facebook.com/beyondproperty.id"><i class="fa fa-facebook"></i></a><br>
                            <a href="http://facebook.com/beyondproperty.id"><i class="fa fa-facebook"></i></a>
                            </span>
                        
                    </a>   --->
                     </div><!-- /.header-identity   
                
                     <div class="header-infobox pull-right">
                    <strong><i class="fa fa-envelope"></i> E-mail:</strong> <a href="#">sales@beyondproperty.id</a>
                </div><!-- /.header-infobox 

                <div class="header-infobox pull-right">
                    <strong><i class="fa fa-phone-square"></i> Phone:</strong> (022) 607-0322 / (022) 604-6690
                </div><!-- /.header-infobox--> 

                  <div class="header-actions pull-right sd" style="color:black;margin-top:-20px;">
                  <strong><i class="fa fa-envelope"></i>   </strong>  <a style="color:black" href="mailto:<?php echo $this->reff->getWeb()->email;?>"><?php echo $this->reff->getWeb()->email;?></a><br>
                  <strong><i class="fa fa-phone-square"></i>   </strong>  <a style="color:black" href="tel:<?php echo $this->reff->getWeb()->telp;?>"><?php echo $this->reff->getWeb()->telp;?></a><br>
                   <strong><img src="<?php echo base_url()?>plug/img/wa.png" width="16px"> </strong>  <a target="new" style="color:black" href="https://api.whatsapp.com/send?phone=6285258889888&text=Hello%20%20Beyond"><?php echo $this->reff->getWeb()->wa;?></a><br>
        
                </div><!-- /.header-actions -->
		<div class="m-hide pull-right <?php echo $cp; ?>" style="right:166px;margin-top:25px;position:absolute;">
                    <ul class="social">
                        <li><a  target="new" style="color:black" href="http://facebook.com/<?php echo $this->reff->getWeb()->fb;?>"><i class="fa fa-facebook"></i></a></li>
                        <li><a  target="new"  style="color:black" href="http://twitter.com/<?php echo $this->reff->getWeb()->twitter;?>"><i class="fa fa-twitter"></i></a></li>
                        <li><a  target="new"  style="color:black" href="http://instagram.com/<?php echo $this->reff->getWeb()->ig;?>"><i class="fa fa-instagram"></i></a></li>
                        <li><a  target="new"  style="color:black" href="https://www.youtube.com/<?php echo $this->reff->getWeb()->youtube;?>"><i class="fa fa-youtube"></i></a></li>
                    </ul><!-- /.social -->
         </div>
                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".header-navigation">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div><!-- /.container -->
        </div><!-- .header-top -->
        	<?php 
			$uri=$this->uri->segment(2);
				if($uri=="network")
				{	$cs="style='color:red'"; $cp=$ss=" ";$ss="style='background-color:white;color:black'"; ?>
				     <div class="header-navigation" style="background-color:#a32a0c">
			<?php	}else{ $cp="color-primary"; $cs=$ss=" ";?>
				     <div class="header-navigation">
			<?php	}	?>
				


       
            <div class="container">
                <div class="row" style="z-index:9999999">
                    <ul class="header-nav nav nav-pills" >
    <li class="menuparents" >
        <a href="<?php echo base_url()?>"><i class="fa fa-home <?php echo $cp; ?>"></i> Home</a>
    </li>

	<li class="menuparents">
        <a href="<?php echo base_url()?>public_page/property"><i class="fa fa-th <?php echo $cp; ?>"></i>  Property</a>
    </li>
	 
	
	<li class="menuparents">
        <a href="<?php echo base_url()?>public_page/peta"><i class="fa fa-map-marker <?php echo $cp; ?>"></i> Map</a>
    </li>
	<li class="menuparents">
        <a href="<?php echo base_url()?>public_page/agen"><i class="fa fa-user <?php echo $cp; ?>"></i> Member</a>
    </li>
	<li class="menuparents">
        <a href="<?php echo base_url()?>public_page/tentang_kami"><i class="fa  fa-building-o <?php echo $cp; ?>"></i>  About Us</a>
    </li>
	
	<li class="menuparents">
        <a href="<?php echo base_url()?>public_page/hubungi_kami"><i class="fa fa-phone-square <?php echo $cp; ?>"></i> Contact</a>
    </li>

	<li class="menuparents">
        <a href="<?php echo base_url()?>public_page/testimoni"><i class="fa fa-rss-square <?php echo $cp; ?>"></i>  Testimony</a>
    </li>	
	<li class="menuparents" style="z-index:9999999">
        <a href="<?php echo base_url()?>public_page/network"><i class="fa fa-signal <?php echo $cp; ?>"></i>  Join Network</a>
    </li>
	<li class="menuparents pulll-right" style="z-index:9999999">
        <a  class="menuparents pulll-right" href="<?php echo base_url()?>login"><i class="fa  fa-unlock-alt <?php echo $cp; ?>"></i>  Login</a>
    </li>
	
   
</ul><!-- /.header-nav -->
                     <div class="form-search-wrapper col-sm-2">
                        <form  action="<?php echo base_url();?>" method="get" class="form-horizontal form-search">
                            <div class="form-group has-feedback no-margin">
                                <input <?php echo $ss; ?> type="text" name="q" class="form-control" value="<?php echo $this->input->get("q");?>" placeholder="Search Article">

                                <span class="form-control-feedback">
                                    <i class="fa fa-search" <?php echo $cs; ?>></i>
                                </span><!-- /.form-control-feedback -->
                            </div><!-- /.form-group -->
                        </form>
                    </div>
					
                </div>
            </div><!-- /.container -->
        </div><!-- /.header-navigation -->
		
    </div><!-- /.header-inner -->
</div><!-- /#header -->    </div><!-- /#header-wrapper -->
<div id="main-wrapper">
    <div id="main">
        <div id="main-inner">

     <?php echo $this->load->view($header);?>
            <div class="container">
                <!-- SLOGAN -->

 <?php $this->load->view($filter);?>

 
<div class="block-content-inners">
<div class="row">
   <?php echo $this->load->view($konten);?>
 

<!-- /.properties-items -->

 
<!-- /.block-content-inner -->
</div><!-- /.block-content -->                
<!-- CAROUSEL -->
<?php $this->load->view($footer_agen);?>
</div><!-- /.block-content -->                <!-- STATISTICS -->
 

<?php $this->load->view($footer_count);?>
    </div><!-- /.container -->
        </div><!-- /#main-inner -->
    </div><!-- /#main -->
</div><!-- /#main-wrapper -->

    <div id="footer-wrapper">
        <div id="footer">
            <div id="footer-inner">
                <div class="footer-top">
    <div class="container">
        <div class="row">
    <div class="widget col-sm-8">
        <div class="row">
		
		
            <div class="feature col-xs-12 col-sm-6">
                 
                <div class="feature-content col-xs-12 col-sm-12">
                    <h3 class="feature-title" style="border-bottom:grey solid 1px">BEYOND PROPERTY</h3>

                    <p class="feature-body">
					 
                       BEYOND property berdiri sejak tahun 2014 merupakan sebuah perusahaan yang bergerak dalam bidang jasa jual, beli, dan sewa properti.  BEYOND property sudah berhasil menjual lebih dari 2.700 unit properti baik berupa apartemen, ruko, rumah, maupun tanah.  BEYOND property memiliki tim konsultan properti yang terpercaya dan terlatih yang akan membantu memasarkan properti...
                    </p>
                </div><!-- /.feature-content -->
            </div><!-- /.feature -->


           
            <div class="feature col-xs-12 col-sm-6">
                <div class="feature-content col-xs-10 col-sm-10">
                    <h3 class="feature-title" style="border-bottom:grey solid 1px">Contact Us</h3>

                    <p class="feature-body">
             <p><i class="fa fa-map-marker "></i>  <?php echo $this->reff->getWeb()->alamat;?> 
<p><i class="fa fa-phone-square"></i> <?php echo $this->reff->getWeb()->telp;?> </p>
<p><img src="<?php echo base_url()?>plug/img/wa2.png" width="16px"> <?php echo $this->reff->getWeb()->wa;?> </p>
<p><i  class="fa fa-envelope"></i> <?php echo $this->reff->getWeb()->email;?> </p>

					</p>
                </div><!-- /.feature-content -->
            </div><!-- /.feature -->


           
 
 
  
        </div><!-- /.row -->
    </div><!-- /.widget -->

    <div class="widget col-sm-4">
       
	 <div class="panel-group" id="accordion">
           
<div class="widget">
   <a href="#" class="pull-right color-primary" style="margin-top:-5px">Selengkapnya</a>
<h3 class="feature-title" style="border-bottom:grey solid 1px">ARTIKEL TERBARU</h3>
    <div class="properties-small-list">
	
	<?php
	$this->db->limit(5);
	$this->db->order_by("id","desc");
	$data=$this->db->get("web_artikel")->result();$i=1;
	foreach($data as $data)
	{?>
	
      
	  
	  
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour<?php echo $i?>">
                    <?php echo $data->title; ?>
                        </a>
                    </h4>
                </div><!-- /.panel-heading -->

                <div id="collapseFour<?php echo $i++;?>" class="panel-collapse collapse">
                    <div class="panel-body">
                      <?php echo substr(strip_tags($data->content),0,50);?>.... <a class="pull-right color-primary" href="<?php echo base_url()?>public_page/single_page/<?php echo str_replace(" ","-",strip_tags(preg_replace('/[^A-Za-z0-9\-]/','-',$data->title)));?>?id=<?php echo $data->id?>">read more</a> 
                    </div><!-- /.panel-body -->
                </div><!-- /.panel-collapse -->
            </div><!-- /.panel -->
	  
	  
	  
	  
	  
	<?php } ?>
        
    </div><!-- /.properties-small-list -->
    </div><!-- /.properties-small-list -->
</div><!-- /.widget -->                   
       
    </div><!-- /.widget-->
</div><!-- /.row -->
 
    </div><!-- /.container -->
</div><!-- /.footer-top -->
                <div class="footer-bottom">
                    <div class="container">
                        <p class="center no-margin">
                            &copy; 2018 Beyond Property, All Right reserved
                        </p>

                <!--    <div class="center">
                            <ul class="social">
                                <li><a href="https://www.facebook.com/beyondproperty.id"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://www.twitter.com/beyond_offc"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="https://www.instagram.com/beyondproperty.id"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="https://www.linkedin.com/company-beta/13375943"><i class="fa fa-linkedin"></i></a></li>
                            </ul> 
                        </div><!-- /.center -->
                    </div><!-- /.container -->
                </div><!-- /.footer-bottom -->
            </div><!-- /#footer-inner -->
        </div><!-- /#footer -->
    </div><!-- /#footer-wrapper -->
</div><!-- /#wrapper -->
 
 
 <button class="to_top ace-icon fa fa-chevron-circle-up bigger-230"> Go to top</button>

<script type="text/javascript" src="<?php echo base_url()?>public_page_asset/assets/libraries/bootstrap-sass/vendor/assets/javascripts/bootstrap/transition.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>public_page_asset/assets/libraries/bootstrap-sass/vendor/assets/javascripts/bootstrap/collapse.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>public_page_asset/assets/libraries/jquery-bxslider/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>public_page_asset/assets/libraries/flexslider/jquery.flexslider.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>public_page_asset/assets/js/jquery.chained.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>public_page_asset/assets/js/realocation.js"></script>
 

   <!-- Histats.com  START  (aync)-->
<script type="text/javascript">var _Hasync= _Hasync|| [];
_Hasync.push(['Histats.start', '1,3954768,4,0,0,0,00010000']);
_Hasync.push(['Histats.fasi', '1']);
_Hasync.push(['Histats.track_hits', '']);
(function() {
var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
hs.src = ('//s10.histats.com/js15_as.js');
(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
})();</script>
<noscript><a href="/" target="_blank"><img  src="//sstatic1.histats.com/0.gif?3954768&101" alt="" border="0"></a></noscript>
<!-- Histats.com  END  -->

</body>

<!-- Mirrored from preview.byaviators.com/template/realocation/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 26 Jun 2017 09:54:32 GMT -->
</html>


 <script>
        $("#provinsi").change(function () {
            var prov = $("#provinsi").val();
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "<?php echo base_url() ?>public_page/ajaxGetKab",
                data: "prov=" + prov,
                success: function (data) {
                    $(".dataKab").html(data);
                }
            });

        });
		 
	function loadkab(){
            var prov = "0";
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "<?php echo base_url() ?>public_page/ajaxGetKab",
                data: "def=0&prov=" + prov,
                success: function (data) {
                    $(".dataKab").html(data);
                }
            });

        };
    </script>
	
<script type="text/javascript">
	function moreLoad(getId)
	{	 
		 var pilih=$("[name='pilih']").val();
		 var type_pro=$("[name='type_pro']").val();
		 var provinsi=$("[name='provinsi']").val();
		 var kabupaten=$("[name='kabupaten']").val();
		 var ID=$("[name='ID']").val();
		 var kt=$("[name='kt']").val();
		 var km=$("[name='km']").val();
		 var harga=$("[name='harga']").val();
		 var urut=$("[name='urut']").val();
	
		if(getId)
		{
			$("#load_more_"+getId).html('<img src="<?php echo base_url()?>plug/img/load.gif" style="padding:10px 0 0 100px;"/>'); 
			$.ajax({
			type: "POST",
			url: "<?php echo base_url()?>public_page/more_content",
			data: "urut="+urut+"&getLastContentId="+ getId+"&pilih="+pilih+"&type_pro="+type_pro+"&provinsi="+provinsi+"&kabupaten="+kabupaten+"&ID="+ID+"&kt="+kt+"&km="+km+"&harga="+harga,
			cache: false,
			success: function(data){
			$("#load_more_ctnt").append(data);
			$("#load_more_"+getId).remove();
			}
			});
		}
		else
		{
			$(".more_tab").html('The End');
		}
		return false;
	 
	};
	
	
	 function moreLoad_brosur(getId)
	{	 
		 var pilih=$("[name='pilih']").val();
		 var type_pro=$("[name='type_pro']").val();
		 var provinsi=$("[name='provinsi']").val();
		 var kabupaten=$("[name='kabupaten']").val();
		 var ID=$("[name='ID']").val();
		 var kt=$("[name='kt']").val();
		 var km=$("[name='km']").val();
		 var harga=$("[name='harga']").val();
	
		if(getId)
		{
			$("#load_more_"+getId).html('<img src="<?php echo base_url()?>plug/img/load.gif" style="padding:10px 0 0 100px;"/>'); 
			$.ajax({
			type: "POST",
			url: "<?php echo base_url()?>public_page/more_content_brosur",
			data: "getLastContentId="+ getId+"&pilih="+pilih+"&type_pro="+type_pro+"&provinsi="+provinsi+"&kabupaten="+kabupaten+"&ID="+ID+"&kt="+kt+"&km="+km+"&harga="+harga,
			cache: false,
			success: function(data){
			$("#load_more_ctnt").append(data);
			$("#load_more_"+getId).remove();
			}
			});
		}
		else
		{
			$(".more_tab").html('The End');
		}
		return false;
	 
	};
	  function moreLoad_agen(getId)
	{	 
		if(getId)
		{
			$("#load_more_"+getId).html('<img src="<?php echo base_url()?>plug/img/load.gif" style="padding:10px 0 0 100px;"/>'); 
			$.ajax({
			type: "POST",
			url: "<?php echo base_url()?>public_page/more_content_agen",
			data: "getLastContentId="+ getId,
			success: function(data){
			$("#load_more_ctnt").append(data);
			$("#load_more_"+getId).remove();
			}
			});
		}
		else
		{
			$(".more_tab").html('The End');
		}
		return false;
	 
	};
	 
	 function filter()
	 {
		 var pilih=$("[name='pilih']").val();
		 var type_pro=$("[name='type_pro']").val();
		 var provinsi=$("[name='provinsi']").val();
		 var kabupaten=$("[name='kabupaten']").val();
		 var ID=$("[name='ID']").val();
		 var kt=$("[name='kt']").val();
		 var km=$("[name='km']").val();
		 var harga=$("[name='harga']").val();
		 var urut=$("[name='urut']").val();
			$("#konten_filter").html('<img src="<?php echo base_url()?>plug/img/load.gif" style="padding:10px 0 0 100px;"/>'); 
			$.ajax({
			type: "POST",
			url: "<?php echo base_url()?>public_page/filter",
			data: "urut="+urut+"&pilih="+pilih+"&type_pro="+type_pro+"&provinsi="+provinsi+"&kabupaten="+kabupaten+"&ID="+ID+"&kt="+kt+"&km="+km+"&harga="+harga,
			cache: false,
			success: function(data){
			$("#konten_filter").html(data);
			}
			});
	 } 
	 
	 function filter_brosur()
	 {
		 var pilih=$("[name='pilih']").val();
		 var type_pro=$("[name='type_pro']").val();
		 var provinsi=$("[name='provinsi']").val();
		 var kabupaten=$("[name='kabupaten']").val();
		 var ID=$("[name='ID']").val();
		 var kt=$("[name='kt']").val();
		 var km=$("[name='km']").val();
		 var harga=$("[name='harga']").val();
			$("#konten_filter").html('<img src="<?php echo base_url()?>plug/img/load.gif" style="padding:10px 0 0 100px;"/>'); 
			$.ajax({
			type: "POST",
			url: "<?php echo base_url()?>public_page/filter_konten_brosur",
			data: "pilih="+pilih+"&type_pro="+type_pro+"&provinsi="+provinsi+"&kabupaten="+kabupaten+"&ID="+ID+"&kt="+kt+"&km="+km+"&harga="+harga,
			cache: false,
			success: function(data){
			$("#konten_filter").html(data);
			}
			});
	 }
	</script>
	
	
	
	<script type="text/javascript">
function getlink(url)
{
  window.open(url, '_blank');
 
}
</script>

<script type="text/javascript">
$(function(){
    $('.to_top').hide().on('click', function(){
        $('body,html').animate({scrollTop : 0}, 800);
    });
    $(window).on('scroll', function(){
        if($(this).scrollTop() > 50){
            $('.to_top').show();
        }else{
            $('.to_top').hide();
        }
    });
});
</script>