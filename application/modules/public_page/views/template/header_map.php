 <?php
$lat=$this->reff->getLatById($id);
$long=$this->reff->getLongById($id);
if($lat)
{
 ?> 
  
  <head>
   <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCsbzuJDUEOoq-jS1HO-LUXW4qo0gW9FNs&amp;sensor=false"></script>
   <script src="http://map.komodoexercise.id/theme/assets/plugin/gmap3.js"></script>	
<script>
		$(document).ready(function(){
			$("#map-property").gmap3({
			  map:{
				options:{
				   center:[<?php echo $lat;?>,<?php echo $long;?>],
				  zoom: 13,
					mapTypeId: google.maps.MapTypeId.MAP,
					mapTypeControl: true,
					mapTypeControlOptions: {
					  style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
					},
					navigationControl: true,
					scrollwheel: true,
					streetViewControl: true
				}
			  },
			  marker:{
				values:[
				  <?php
				  $icon="icon.png";
			  ?>
			  <?php
			$val=$this->reff->getDataProperty($id);
			?>
				  	
					  				  				  {latLng:[<?php echo $lat;?>,<?php echo $long;?>], data:"<div><?php echo $val->area_listing;?></div>", options:{icon: "<?php echo base_url();?>plug/img/<?php echo $icon;?>"}},
				   
									],
				options:{
				  draggable: false
				},
				events:{
				  click: function(marker, event, context){
					var map = $(this).gmap3("get"),
					  infowindow = $(this).gmap3({get:{name:"infowindow"}});
					if (infowindow){
					  infowindow.open(map, marker);
					  infowindow.setContent(context.data);
					} else {
					  $(this).gmap3({
						infowindow:{
						  anchor:marker, 
						  options:{content: context.data}
						}
					  });
					}
				  },
				  function(){
					var infowindow = $(this).gmap3({get:{name:"infowindow"}});
					if (infowindow){
					  infowindow.close();
					}
				  }
				}
			  }
			});

		});
		</script>

  </head>
 
             <!-- MAP -->
<div class="block-content no-padding">
    <div class="block-content-inner">
	
        <div id="map-property">
 
        </div><!-- /.map-wrapper -->
    </div><!-- /.block-content-inner -->
</div><!-- /.block-content -->

<?php
}else{
	?>
	<div class="block-content no-padding">
    <div class="block-content-inner">
	
        <div id="map-property" style="background-image:url('<?php echo base_url()?>file_upload/img/<?php echo $this->reff->getGambarUtamaById($id);?>');background-size:cover">

        </div><!-- /.map-wrapper -->
    </div><!-- /.block-content-inner -->
</div><!-- /.block-content -->
<?php } ?>
 
 