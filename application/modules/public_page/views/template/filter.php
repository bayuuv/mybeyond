<style>
.dro{
	background-color:black;opacity:0.9;z-index:4;
}
@media only screen and (max-width:632px)
{
	 
	.dro{
	background-color:#0aa69;opacity:0.9;z-index:4;
	min-height:550px;
	}
	.sis{
		height:580px;
	}
}
 

</style>

<div class="block-content sis background-primary  block-content-small-padding fullwidth b-filter">  
  <div class=" background-primary" style="margin-top:-28px">
 
<!------------------------------------------------------#############################-->     
	 <div class="box col-md-12 dro ">
                                        <form method="post" action="javascript:filter()">
                                           
										  <div class="form-group col-md-3 col-xs-12" >
                                                <label>Sewa/Jual</label>
											 
                                                <select id="pilih" name="pilih" class="form-control" style="width:100%">
												<option value=""  >===== All =====</option>
												<option value="jual"  >DIJUAL</option>
												<option value="sewa"  >DISEWAKAN</option>
												</select>
									 
                                            </div><!-- /.form-group -->

											   <div class="form-group col-md-3 col-xs-12" >
                                                <label>Type Property</label>
                                                  <?php
                                        $ref_type = $this->reff->getTypePro();
                                        $array[""] = "===== All =====";
                                        foreach ($ref_type as $val) {
                                            $array[$val->id_type] = $val->nama;
                                        }
                                        $data = $array;
                                        echo form_dropdown('type_pro', $data, '', '  id="type_pro"  class="form-control" onchange="cek_type_pro()"');
                                        ?>
                                            </div><!-- /.form-group -->

											
											


										   <div class="form-group col-md-3 col-xs-12" >
                                                <label>Provinsi</label>
                                                 <?php
                                        $ref_type = $this->reff->getProvinsi();
                                        $array_prov[""] = "====== All ======";
                                        foreach ($ref_type as $val) {
                                            $array_prov[$val->id] =   $val->provinsi;
                                        }
                                        $data = $array_prov;
                                        echo form_dropdown('provinsi', $data, '', 'onchange="return cek_prov()"   id="provinsi" class="form-control" style="width:100%"');
                                        ?>
                                            </div><!-- /.form-group 

											
											  <div class="form-group col-md-3" >
                                                <label>Kab/Kota</label>
												<span class='dataKab'>
                                                <select id="kabupaten" name="kabupaten" class="form-control" style="width:100%">
												<option value="" selected>===== All =====</option></select>
										</span>
                                            </div><!-- /.form-group -->
									<input type="hidden" name="kabupaten" value="">	
								
								<div class="form-group col-md-3 col-xs-12" >
                                                <label>Area</label>				 
                                                <input class="form-control" type="text" name="ID" placeholder="Alamat Property">
	                                    </div><!-- /.form-group -->  
										
									 
											 
									<span class="detail-filter">		
										<input type="hidden" name="kt" value="">	
										<input type="hidden" name="km" value="">
										<!--
										  <div class="form-group col-md-2" >
                                                <label>Kamar Tidur</label>
		                                        <select id="kt" name="kt" class="form-control" style="width:100%">
												<option value="">===== All =====</option>
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
												<option value="5">5</option>
												<option value="6">6</option>
												<option value="7">7</option>
												<option value="8">8</option>
												<option value="9">9</option>
												<option value="10">=>10</option>
												</select>
                                          </div><!-- /.form-group -->
										<!--
										  <div class="form-group col-md-2" >
                                                <label>Kamar Mandi</label>
		                                        <select id="km" name="km" class="form-control" style="width:100%">
												<option value="">===== All =====</option>
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
												<option value="5">5</option>
												<option value="6">6</option>
												<option value="7">7</option>
												<option value="8">8</option>
												<option value="9">9</option>
												<option value="10">=>10</option>
												</select>
                                          </div><!-- /.form-group -->
										
										  <div class="form-group col-md-4 col-xs-12" >
                                                <label>Harga Property</label>
		                                        <select id="harga" name="harga" class="form-control" style="width:100%">
												<option value="">===== All =====</option>
												<option value="0-50000000">Dibawah 50 juta</option>
												<option value="50000000-100000000">50 - 100 juta</option>
												<option value="100000000-200000000">100 - 200 juta</option>
												<option value="200000000-500000000">200 - 500 juta</option>
												<option value="500000000-800000000">500 - 800 juta</option>
												<option value="800000000-1500000000">800 juta - 1.5 miliar</option>
												<option value="1500000000-5000000000">1.5 - 5 miliar</option>
												<option value="5000000000-10000000000">5 - 10 miliar</option>
												<option value="10000000000-50000000000">10 - 50 miliar</option>
												<option value=">50000000000">50 miliar keatas</option>
												</select>
                                          </div><!-- /.form-group --> 

										 <div class="form-group col-md-4 col-xs-12" >
                                                <label>Urutkan</label>
		                                        <select id="urut" name="urut" class="form-control" style="width:100%">
                                                <option value="terbaru">Property Terbaru</option>
												<option value="ASC">Harga Terendah -- Termahal</option>
												<option value="DESC">Harga Termahal -- Terendah </option>
												</select>
                                          </div><!-- /.form-group --> 
									</span>	
									<div class="form-group col-md-4 col-xs-12" ><br>
                                		 <button   class="btn btn-primary btn-block" style="margin-top:3px"><i class="fa fa-search"></i> Cari</button>
									 
                                    </div><!-- /.form-group -->
                                            
                                        </form>
                                    </div><!-- /.box -->
  <!------------------------------------------------------#############################-->   
    </div><!-- /.block-content-iner -->
 
</div>