 <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCsbzuJDUEOoq-jS1HO-LUXW4qo0gW9FNs&amp;sensor=false"></script>
   <script src="<?php echo base_url()?>plug/js/map/gmap3.js"></script>	
   <div class="block-content  background-primary background-map fullwidth block-content-small-padding fullwidth">  
  <h1 class="title-top">Need Help? Contact Us.</h1>
</div>
 
   <div  >
            
    <div >
	
        <div class="content">
            
                          

                            
     
      
  
<script>
<?php   $val=$this->reff->getWebKontak(); ?>
		$(document).ready(function(){
			$("#map").gmap3({
			  map:{
				options:{
				   center:[<?php echo $val->lat;?>,<?php echo $val->long;?>],
				  zoom: 15,
					mapTypeId: google.maps.MapTypeId.MAP,
					mapTypeControl: true,
					mapTypeControlOptions: {
					  style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
					},
					navigationControl: true,
					scrollwheel: true,
					streetViewControl: true
				}
			  },
			  marker:{
				values:[
				  <?php
				  $icon="icon.png";
				  $data="<div style='z-index: 107; width: 100%;'><a href='#'>Head Office : ".$val->nama_perusahaan."</a></div>";
				 
			  ?>
			 
				
				  	
			   {latLng:[<?php echo $val->lat;?>,<?php echo $val->long;?>], data:"<?php echo $data;?>", options:{icon: "<?php echo base_url();?>plug/img/<?php echo $icon;?>"}},
			 	   
									],
				options:{
				  draggable: false
				},
				events:{
				  click: function(marker, event, context){
			 
					  
					var map = $(this).gmap3("get"),
					  infowindow = $(this).gmap3({get:{name:"infowindow"}});
					if (infowindow){
					  infowindow.open(map, marker);
					  infowindow.setContent(context.data);
					} else {
					  $(this).gmap3({
						infowindow:{
						  anchor:marker, 
						  options:{content: context.data}
						}
					  });
					}
				  },
				  function(){
					var infowindow = $(this).gmap3({get:{name:"infowindow"}});
					if (infowindow){
						
					  infowindow.close();
					}
				  }
				}
			  }
			});

		});
		</script>
	 
		<div class="col-md-7" id="map" style="height:370px;margin-top:3px;border-top:#0aa699 solid 3px"></div>
		
		
        
        
        <div id="col-md-6" class="background-map center" style='font-size:12px'>                        
                                                            
                <div class=" background-map contact-box col-md-4">
                    <div class="title">
                       
                        <h3 class="color-primary"> <span class="color-primary fa fa-map-marker"></span> Address</h3>
                    </div>
                    <p ><?php echo str_replace(",","<br>",$val->alamat);?></p>
                </div><!-- /.contact-box -->
                                                            
                <div class=" background-map contact-box col-md-4">
                    <div class="title">
                       
                        <h3 class="color-primary"> <span class="fa fa-phone"></span> Phone</h3>
                    </div>
                    <p><a style="color:black" href="tel:<?php echo $this->reff->getWeb()->telp;?>"><?php echo str_replace(",","<br>",$val->telp);?></a></p>
                </div><!-- /.contact-box -->
                                                            
                <div class=" background-map contact-box col-md-4">
                    <div class="title">
                        
                        <h3 class="color-primary"><span class="fa fa-envelope"></span> Email</h3>
                    </div>
                    <p><a style="color:black" href="mailto:<?php echo $this->reff->getWeb()->email;?>"><?php echo str_replace(",","<br>",$val->email);?></p></a>
                </div><!-- /.contact-box -->
            
        </div>
   
		</div>
		</div>
		</div>
 

         