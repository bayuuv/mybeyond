<div class="block-content  background-primary background-map fullwidth block-content-small-padding fullwidth">  
  <h1 class="title-top">ABOUT US</h1>
</div>
<br>
<ul id="thumbnails">
 <li>
    <a href="#slide1">
      <img src="<?php echo base_url()?>file_upload/image/IMG_2564.jpg" alt="Front Office">
    </a>
  </li>
  <li>
    <a href="#slide2">
      <img src="<?php echo base_url()?>file_upload/image/IMG_2563.jpg"  alt="Beyond Property Workroom">
    </a>
  </li>
  <li>
    <a href="#slide3">
      <img src="<?php echo base_url()?>file_upload/image/IMG_2538.jpg" alt="Beyond Property Workroom">
    </a>
  </li>
  <li>
    <a href="#slide4">
      <img src="<?php echo base_url()?>file_upload/image/IMG_2537.jpg" alt="Beyond Property Workroom">
    </a>
  </li>
</ul>
<div class="thumb-box">
  <ul class="thumbs">
    <li>
        <a href="#1" data-slide="1"><img src="<?php echo base_url()?>file_upload/image/IMG_2564.jpg" alt="This is caption 1"></a>
    </li>
    <li>
        <a href="#2" data-slide="2"><img src="<?php echo base_url()?>file_upload/image/IMG_2563.jpg" alt="This is caption 2"></a>
    </li>
    <li>
        <a href="#3" data-slide="3"><img src="<?php echo base_url()?>file_upload/image/IMG_2538.jpg" alt="This is caption 3"></a>
    </li>
    <li>
        <a href="#4" data-slide="4"><img src="<?php echo base_url()?>file_upload/image/IMG_2537.jpg" alt="This is caption 4"></a>
    </li>
  </ul>
</div>
<!--
<div class="block-content  background-primary background-map fullwidth block-content-small-padding fullwidth">  
  <h1 class="title-top">About Us.</h1>
</div>
<br>

<div class="col-sm-9">
<ul id="pictures-demo">
  <li title="This is caption 1">
    <img src="http://mybeyond.co.id/file_upload/image/kantor1.jpeg"  alt="Beyond Property">
  </li>
  <li title="This is caption 2">
    <img src="http://mybeyond.co.id/file_upload/image/kantor2.jpeg"  alt="Beyond Property">
  </li>
  <li title="This is caption 2">
    <img src="http://mybeyond.co.id/file_upload/image/kantor3.jpeg"  alt="Beyond Property">
  </li>
  <li title="This is caption 2">
    <img src="http://mybeyond.co.id/file_upload/image/kantor4.jpeg"  alt="Beyond Property">
  </li>
</ul>
</div>
<div class="col-sm-3">
    
</div>-->
<!--

-->
<br>
<div class="content col-md-12 col-lg-12 background-map">
<?php echo $this->reff->getTentang();?>
</div>
<br>




<script src="<?php echo base_url()?>public_page_asset/assets/js/slippry.min.js"></script>
<script>
/*
    jQuery('#pictures-demo').slippry({
  // general elements & wrapper
  slippryWrapper: '<div class="sy-box pictures-slider" />', // wrapper to wrap everything, including pager

  // options
  adaptiveHeight: false, // height of the sliders adapts to current slide
  captions: false, // Position: overlay, below, custom, false

  // pager
  pager: false,
  
  // controls
  controls: false,
  autoHover: false,

  // transitions
  transition: 'kenburns', // fade, horizontal, kenburns, false
  kenZoom: 0,
  speed: 2000 // time the transition takes (ms)
});
*/
var thumbs = jQuery('#thumbnails').slippry({
  // general elements & wrapper
  slippryWrapper: '<div class="slippry_box thumbnails" />',
  // options
  transition: 'horizontal',
  pager: false,
  auto: false,
  onSlideBefore: function (el, index_old, index_new) {
    jQuery('.thumbs a img').removeClass('active');
    jQuery('img', jQuery('.thumbs a')[index_new]).addClass('active');
  }
});

jQuery('.thumbs a').click(function () {
  thumbs.goToSlide($(this).data('slide'));
  return false;
});
</script>
		 
         