<?php
$val=$this->reff->getDataProperty($id);
?>

<div class="container">
                <div class="block-content block-content-small-padding">
                    <div class="block-content-inner">
                        <div class="row">
                            <div class="col-sm-9">
                                <h2 class="property-detail-title"><?php echo $val->desc;?></h2>
                                <h3 class="property-detail-subtitle"><i class="fa fa-map-marker"></i> <?php echo $val->nama_area;?> 
                                    <?php if ($val->status == 0){ ?>
                                    <strong class='pull-right'>
                                        Rp <?php echo number_format($val->harga+$val->fee_up,0,",",".")?>
                                    </strong>
                                    <?php }else{ ?>
                                    <strong class='pull-right' style="background-color:red">
                                        <?php if($val->type_jual == 'jual'){ ?>
                                            TERJUAL
                                        <?php }elseif($val->type_jual == 'sewa'){ ?>
                                            TERSEWA
                                        <?php } ?>
                                    </strong>
                                    <?php } ?>
                                </h3>
                                <div class="property-detail-overview">
                                    <div class="property-detail-overview-inner clearfix">
                                        <div class="property-detail-overview-item col-sm-6 col-md-2">
                                            <strong>ID :</strong>
                                            <span><?php echo $val->kode_prop; ?></span>
                                        </div><!-- /.property-detail-overview-item -->

                                        <div class="property-detail-overview-item col-sm-6 col-md-2">
                                            <strong>Type:</strong>
                                            <span><?php echo $val->type_jual; ?></span>
                                        </div><!-- /.property-detail-overview-item -->

                                        <div class="property-detail-overview-item col-sm-6 col-md-2">
                                            <strong>Area:</strong>
                                            <span><?php //echo $this->reff->getNamaKab($id); 
                                                     echo $val->area_listing; ?></span>
                                        </div><!-- /.property-detail-overview-item -->

                                        <div class="property-detail-overview-item col-sm-6 col-md-2">
                                            <strong>Kamar Mandi:</strong>
                                            <span><?php echo $val->kamar_mandi?></span>
                                        </div><!-- /.property-detail-overview-item -->

                                        <div class="property-detail-overview-item col-sm-6 col-md-2">
                                            <strong>Kamar Tidur:</strong>
                                            <span><?php echo $val->kamar_tidur?></span>
                                        </div><!-- /.property-detail-overview-item -->

                                        <div class="property-detail-overview-item col-sm-6 col-md-2">
                                            <strong>Garasi:</strong>
                                            <span><?php echo $val->jml_garasi?></span>
                                        </div><!-- /.property-detail-overview-item -->
                                    </div><!-- /.property-detail-overview-inner -->
                                </div><!-- /.property-detail-overview -->

                                <div class="flexslider">
                                    <ul class="slides">
										<?php

										if($val->gambar1 <> ''){
											$gambar1 = $val->gambar1;
										}else{
											$gambar1 = 'not-avaliable.png';
										}
										
										if($val->gambar2 <> ''){
											$gambar2 = $val->gambar2;
										}else{
											$gambar2 = 'not-avaliable.png';
										}
										
										if($val->gambar3 <> ''){
											$gambar3 = $val->gambar3;
										}else{
											$gambar3 = 'not-avaliable.png';
										}
										
										if($val->gambar4 <> ''){
											$gambar4 = $val->gambar4;
										}else{
											$gambar4 = 'not-avaliable.png';
										}
										
										if($val->gambar5 <> ''){
											$gambar5 = $val->gambar5;
										}else{
											$gambar5 = 'not-avaliable.png';
										}
										?>
											 <li data-thumb="<?php echo base_url()?>/file_upload/img/<?php echo $gambar1;?>"><center><img style="max-height:360px;max-width:300px" src="<?php echo base_url()?>/file_upload/img/<?php echo $gambar1;?>" alt="Thumbnails"></center></li>
											 <li data-thumb="<?php echo base_url()?>/file_upload/img/<?php echo $gambar2;?>"><center><img style="max-height:360px;max-width:300px" src="<?php echo base_url()?>/file_upload/img/<?php echo $gambar2;?>" alt="Thumbnails"></center></li>
											  <li data-thumb="<?php echo base_url()?>/file_upload/img/<?php echo $gambar3;?>"><center><img  style="max-height:360px;max-width:300px" src="<?php echo base_url()?>/file_upload/img/<?php echo $gambar3;?>" alt="Thumbnails"></center></li>
											 <li data-thumb="<?php echo base_url()?>/file_upload/img/<?php echo $gambar4;?>"><center><img  style="max-height:360px;max-width:300px" src="<?php echo base_url()?>/file_upload/img/<?php echo $gambar4;?>" alt="Thumbnails"></center></li>
											 <li data-thumb="<?php echo base_url()?>/file_upload/img/<?php echo $gambar5;?>"><center><img  style="max-height:360px;max-width:300px" src="<?php echo base_url()?>/file_upload/img/<?php echo $gambar5;?>" alt="Thumbnails"></center></li>
									 

                                    </ul><!-- /.slides -->
                                </div><!-- /.flexslider -->

                                <hr>

                                <h2>Description</h2>

                                <p>
                                    <?php echo $val->keterangan;?>
                                </p>

                                <hr>

                                <h2>Detail</h2>

                                <div class="row">
								
								 
 
								
										
								
                                    <ul class="property-detail-amenities">
									        <li class="col-xs-6 col-sm-4"><i class="fa fa-arrow-circle-right"></i> Property : <?php echo $this->reff->getNamaJenis($val->jenis_prop); ?></li>
											<li class="col-xs-6 col-sm-4"><i class="fa fa-arrow-circle-right"></i>Air : <?php echo $this->reff->getNamaAir($val->air);?></li>
											<li class="col-xs-6 col-sm-4"><i class="fa fa-arrow-circle-right"></i> Harga : Rp <?php echo number_format($val->harga+$val->fee_up,0,",","."); ?></li>
									   <li class="col-xs-6 col-sm-4"><i class="fa fa-arrow-circle-right"></i>Luas Tanah : <?php echo $val->luas_tanah;?> M<sup>2<sup></li>
                                     <li class="col-xs-6 col-sm-4"><i class="fa fa-arrow-circle-right"></i> Kamar Tidur :  <?php echo $val->kamar_tidur; ?></li>
                                     <li class="col-xs-6 col-sm-4"><i class="fa fa-arrow-circle-right"></i> Sertifikat :  <?php echo $this->reff->getNamaJenisSertifikat($val->jenis_sertifikat); ?></li>
                                        <li class="col-xs-6 col-sm-4"><i class="fa fa-arrow-circle-right"></i>Luas Bangunan : <?php echo $val->luas_bangunan;?> M<sup>2<sup></li>
                                 <li class="col-xs-6 col-sm-4"><i class="fa fa-arrow-circle-right"></i> Kamar Tidur Pembantu : <?php echo  $val->kamar_tidur_p;?></li>
                                        <li class="col-xs-6 col-sm-4"><i class="fa fa-arrow-circle-right"></i> Jml Garasi : <?php echo  $val->jml_garasi;?></li>
                                       <li class="col-xs-6 col-sm-4"><i class="fa fa-arrow-circle-right"></i> Jml Lantai : <?php echo  $val->jml_lantai;?></li>
                                          <li class="col-xs-6 col-sm-4"><i class="fa fa-arrow-circle-right"></i> Kamar Mandi :  <?php echo $val->kamar_mandi; ?></li>
                                            <li class="col-xs-6 col-sm-4"><i class="fa fa-arrow-circle-right"></i> Jml Carport : <?php echo  $val->jml_carports;?></li>
                                    <li class="col-xs-6 col-sm-4"><i class="fa fa-arrow-circle-right"></i>Daya Listrik : <?php echo  $val->daya_listrik;?></li>
                                    <li class="col-xs-6 col-sm-4"><i class="fa fa-arrow-circle-right"></i> Kamar Mandi Pembantu:  <?php echo $val->kamar_mandi_p; ?></li>
                                      <li class="col-xs-6 col-sm-4"><i class="fa fa-arrow-circle-right"></i>Furniture : <?php echo $this->reff->getNamaFurniture($val->furniture);?></li>
                                      

									     
                                        
                                       
                                       
                                    </ul>
                                </div><!-- /.row -->
                            </div>

                            <div class="col-sm-3">
                                <div class="sidebar">
                                    <div class="sidebar-inner">
                                                              
<div class="widget">

 <?php if($val->desain)
{?>
    <h3 class="widget-title">Factsheet</h3>
		<div class="agent-box" style="margin-top:-35px">
			<center><img  style="max-height:360px;max-width:300px" src="<?php echo base_url()?>/file_upload/img/<?php echo $val->desain;?>" alt="Thumbnails">
			<a href="<?php echo base_url()?>/file_upload/img/<?php echo $val->desain;?>" download class="btn-blue btn">Download</a>
<?php } ?>
<?php if($val->desaintv){ ?>
			| <a href="<?php echo base_url()?>/file_upload/img/<?php echo $val->desaintv;?>" download class="btn-blue btn">Download Big Size</a>      
			</center>
		</div>
<?php } ?>



<h3 class="widget-title">Hubungi Agen</h3>
<?php $agen=$this->reff->getAgenRow($val->agen); ?>
   <div class="agent-box" style="margin-top:-22px">
                                            <div class="row">
                                                <div class="agent-box-picture col-sm-12">
                                                  <center>
                                                    <div class="agent-box-picture-inner">
                                                        <a href="#" class="agent-box-picture-target">
                                                            <img style="max-width:200px"  src="<?php echo base_url()?>file_upload/agen/<?php echo $this->reff->getPotoAgen($val->agen)?>" alt="">
                                                        </a><!-- /.agent-row-picture-target -->
                                                    </div><!-- /.agent-row-picture-inner --></center>
                                                </div><!-- /.agent-row-picture --> 

                                                <div class="agent-box-content col-sm-12">
                                                    <h3 class="agent-box-title"><a href="#"><?php echo $this->reff->getNamaAgen($val->agen)?></a></h3><!-- /.agent-row-title -->
													<hr>
                                                 <p>   <i class="fa fa-phone"></i> <a style="color:black" href="tel:<?php echo $agen->hp;?>"><?php echo $this->tanggal->septik2($agen->hp)?></a> <br>
                                                  <i class="fa fa-envelope"></i> <a style="color:black" href="mailto:<?php echo $agen->email;?>"><?php echo $agen->email?></a></p>

                                                  
                                                </div><!-- /.agent-row-content -->
                                            </div><!-- /.row -->
                                        </div><!-- /.agent-row -->
     
</div><!-- /.widget -->       

 
 <div class="widget">
     <hr>
<b>Bagikan</b><br>
     <button class="btn-blue btn-blok" onclick="share_fb()"><i class="fa fa-facebook"></i>  Facebook</button> 
        <button class="btn-blue btn-blok" onclick="share_twitter()"><i class="fa fa-twitter"></i>  Twitter</button>
        <button class="btn-blue btn-blok" onclick="share_gplus()"><i class="fa fa-google-plus"></i>  Google Plus</button>
<script>
function share_fb()
{	var url="https://www.facebook.com/sharer.php?<?php echo base_url()."public_page/show_listing/".$id."/".$this->uri->segment(4); ?>";
	var windowName="share";
	 var newwindow=window.open(url,windowName,'height=200,width=350');
       if (window.focus) {newwindow.focus()}
       return false;
}
</script>
<script>
function share_twitter()
{	var url="https://twitter.com/intent/tweet?text=Di<?php echo $val->type_jual; ?> <?php echo $this->reff->getNamaJenis($val->jenis_prop); ?> <?php echo $val->desc;?>&url=<?php echo base_url()."public_page/show_listing/".$id."/".$this->uri->segment(4); ?>";
	var windowName="share";
	 var newwindow=window.open(url,windowName,'height=200,width=350');
       if (window.focus) {newwindow.focus()}
       return false;
}
</script>
<script>
function share_gplus()
{	var url="https://plus.google.com/share?url=<?php echo base_url()."public_page/show_listing/".$id."/".$this->uri->segment(4); ?>";
	var windowName="share";
	 var newwindow=window.open(url,windowName,'height=200,width=350');
       if (window.focus) {newwindow.focus()}
       return false;
}
</script>
     
</div>
<!-- /.widget -->                    




                </div><!-- /.sidebar-inner -->
                                </div><!-- /.sidebar -->
                            </div>
                        </div><!-- /.row -->
                    </div><!-- /.block-content-inner -->
                </div><!-- /.block-content -->
            </div><!-- /.container -->