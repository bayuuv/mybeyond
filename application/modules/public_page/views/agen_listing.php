<?php $isigambar=""; $agen=$this->reff->getAgenRowById($this->uri->segment(3)); if(!$agen){ return "<center>ID AGEN TIDAK DITEMUKAN</center>";}?>
<br>
<div class="content col-sm-12 col-md-12">
            
<div class="agent-detail">

    <div class="row">

        <div class="col-sm-3">
            <div class="agent-detail-picture">
                                    <img   src="<?php echo base_url()?>file_upload/agen/<?php echo $agen->poto;?>" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" height="250"  >                            
			</div> <!-- /.agent-detail-picture -->
        </div> <!-- /.col-sm-3 -->

        <div class="col-sm-9" style="margin-top:-20px">
           
			  <a href="javascript:void(0)"><h1><?php echo $agen->nama;?> </h1></a>
			  <a href="javascript:void(0)"><p style="font-size:22px"><?php echo $agen->jabatan;?></p></a>
             <p><ul style="list-style:none;margin-left:-40px;font-size:16px">
			 <li><i class="fa fa-phone-square"></i>  <a href="tel:<?php echo $agen->hp;?>" target="_blank"><?php echo $this->tanggal->septik($agen->hp);?></a> 
			 
			 <?php if($agen->hp2){?>
             | <i class="fa fa-phone-square"></i>  <a href="tel:<?php echo $agen->hp2;?>" target="_blank"><?php echo $this->tanggal->septik($agen->hp2);?></a>
			 <?php } ?>
			 
			 <?php if($agen->fb){?>
             | <i class="fa fa-facebook"></i>  <a href="https://www.facebook.com/<?php echo $agen->fb;?>" target="_blank"><?php echo $agen->fb;?></a>
			  <?php } ?>
			  
			 <?php if($agen->twt){?>
             | <i class="fa fa-twitter"></i>  <a href="https://www.twitter.com/<?php echo $agen->twt;?>" target="_blank"><?php echo $agen->twt;?></a>
			  <?php } ?>
			  
			 <?php if($agen->ig){?>
             | <i class="fa fa-instagram"></i>  <a href="https://www.instagram.com/<?php echo $agen->ig;?>" target="_blank"><?php echo $agen->ig;?></a></li>
			  <?php } ?>
			 
			 
			 </ul>
			 </p>
			 <p style="font-size:18px"><?php echo $agen->status_profile;?></p>
		 
        </div> <!-- /.col-sm-9-->
		
    </div><!-- /.row -->
</div><!-- /.agent-detail -->
    <hr>
	
<div class="row">


<?php
	$i=1;
	$no = $offset; 
	foreach($data as $val ) { 
    ++$no;
 	$i++;


$id=$val->id_prop;
		
		if($val->status=="0"){
    		if($val->type_jual=="jual")
    		{
    			$class="property-box-label property-box-label-primary";
    		}else
    		{
    			$class="property-box-label";
    		}
		}else{
		    $class="property-box-label property-box-label-danger";
		}
		
		$gambar="not-avaliable.jpg";
		if(strlen($val->gambar_utama)>3)
		{
			$utama=$val->gambar_utama;
			$gambar=$val->$utama;
			$isigambar=$gambar;
		}
		
		if(strlen($isigambar)<3)
		{
		 	$gambar="not-avaliable.png";
		}
		
?>	
<!---------------------------------------------------->
 <div class="property-item property-sale col-sm-6 col-md-3" style="cursor:pointer" onclick="getlink('<?php echo base_url()?>public_page/show_listing/<?php echo $val->id_prop;?>/<?php echo str_replace(" ","-",$val->desc)?>')">
        <div class="property-box" style="border-radius:15px">
            <div class="property-box-inner">
                <h4 class="property-box-title" style="min-height:50px;max-height:50px"> <a target="_blank" href="<?php echo base_url()?>public_page/show_listing/<?php echo $val->id_prop;?>/<?php echo str_replace(" ","-",$val->desc)?>"><?php echo $this->reff->titleWeb($val->kode_prop);?></a></h4>
                <h4 class="property-box-subtitle"> ID : <?php echo $val->kode_prop;?> </h4>
                <div class="<?php echo $class;?>">
                    <?php 
                    if($val->status=="0")
                    { 
                        echo $val->type_jual;
                    }else{   
                        if($val->type_jual=="jual"){
                            echo 'TERJUAL';
                        }elseif($val->type_jual=="sewa"){
                            echo 'TERSEWA';
                        }
                    } 
                     ?>
                </div><!-- /.property-box-label -->
                <div class="property-box-picture" style="margin-top:-30px">
                    <div class="property-box-price">Rp <?php echo number_format($val->harga+$val->fee_up,0,",","."); ?></div><!-- /.property-box-price -->
                    <div class="property-box-picture-inner">
                      <a href="<?php echo base_url()?>public_page/show_listing/<?php echo $val->id_prop;?>/<?php echo str_replace(" ","-",$val->desc)?>" class="property-box-picture-target">
                            <img height="300px" src="<?php echo base_url()?>file_upload/img/<?php echo $gambar;?>" alt="">
                      </a>
                    </div><!-- /.property-picture-inner -->
                </div> <!-- /.property-picture -->
				
                <div class="property-box-meta">
                    <div class="property-box-meta-item col-xs-6 col-sm-6">
                        <strong><?php echo isset($val->luas_tanah)?($val->luas_tanah):"-";?> M<sup>2</sup></strong>
                        <span>LT</span>
                    </div><!-- /.property-box-meta-item col-xs-6 col-sm-6--> 

                    <div class="property-box-meta-item col-xs-4 col-sm-4">
                        <strong><?php echo isset($val->kamar_tidur)?($val->kamar_tidur):"-";?></strong>
                        <span>Beds</span>
                    </div><!-- /.property-box-meta-item col-xs-4 col-sm-4 -->

                   

                    <div class="property-box-meta-item col-xs-6 col-sm-6">
                        <strong><?php echo  isset($val->luas_bangunan)?($val->luas_bangunan):"-";?>  M<sup>2</sup></strong>
                        <span>LB</span>
                    </div><!-- /.property-box-meta-item col-xs-6 col-sm-6 -->
                </div><!-- /.property-box-meta -->
            </div><!-- /.property-box-inner -->
        </div> <!-- /.property-box -->
    </div> <!-- / .property-item property-sale -->                           
<!---------------------------------------------------->                           						   
					<?php } ?>	   
	<div class="center clearfix col-md-12">
	<?php echo $halaman ?> <!--Memanggil variable pagination-->
	</div> <!-- / .center clearfix col-md-12 -->
						   

    </div><!-- /.row -->
</div><!-- /.content col-sm-12 col-md-12 -->
<hr>