<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Public_page extends CI_Controller {

	

	function __construct()
	{
		parent::__construct();	
		$sesi=$this->session->userdata("level");
		if($sesi)
		{
			//redirect("login");
		}
		$this->load->model("M_public","model");
	}
	
	function _template($data)
	{
	$this->load->view('template/index',$data);	
	}

	public function property()
	{

		$data['header']="template/header_slider";
		$data['filter']="template/filter";
		$data['footer_agen']="template/kosong"; 
		$data['footer_count']="template/footer_testi";
		$data['konten']="index";
		$this->_template($data);
	}public function brosur()
	{

		$data['header']="template/kosong";
		$data['filter']="template/filter_brosur";
		$data['konten']="brosur";
		$data['footer_agen']="template/kosong";
		$data['footer_count']="template/footer_count";
		$this->_template($data);
	}
	public function peta()
	{

		$data['header']="template/kosong";
		$data['filter']="template/kosong";
		$data['footer_agen']="template/kosong";
		$data['footer_count']="template/footer_count";
		$data['konten']="peta";
		$this->_template($data);
	}
	function cekIdPage($id)
	{
		$this->db->where("id",$id);
	return	$this->db->get("web_artikel")->num_rows();
	}
	function getSingle($id)
	{
		$this->db->where("id",$id);
	return	$this->db->get("web_artikel")->row();
	}
	function addCount($id)
	{
	$data=$this->db->query("SELECT  count FROM web_artikel WHERE id='".$id."'")->row();
	$jml1=isset($data->count)?($data->count):"0";
	return $this->db->query("UPDATE web_artikel set count='".($jml1+1)."' WHERE id='".$id."'");
	}
	public function single_page()
	{
		$id=$this->input->get("id");
		$cek=$this->cekIdPage($id);
		if($cek)
		{		$this->addCount($id);
			$data['konten']="single_page";
		}else{
			$data['konten']="404";
		}
		$data['data']=$this->getSingle($id);
		$data['header']="template/kosong";
		$data['filter']="template/kosong";
		$data['footer_agen']="template/kosong";
	    $data['footer_count']="template/footer_testi";
		 
		$this->_template($data);
	}
	public function agen($offset=0)
	{
		 $jml = $this->model->get_query_agen();

   $config['base_url'] = base_url().'public_page/agen';
   
   $config['total_rows'] = $jml->num_rows();
   $config['per_page'] = 9; /*Jumlah data yang dipanggil perhalaman*/ 
   $config['uri_segment'] = 3; /*data selanjutnya di parse diurisegmen 3*/
   
   /*Class bootstrap pagination yang digunakan*/
   $config['full_tag_open'] = "<ul class='pagination pagination-sm' style='position:relative; top:-25px;'>";
   $config['full_tag_close'] ="</ul>";
   $config['num_tag_open'] = '<li>';
   $config['num_tag_close'] = '</li>';
   $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
   $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
   $config['next_tag_open'] = "<li>";
   $config['next_tagl_close'] = "</li>";
   $config['prev_tag_open'] = "<li>";
   $config['prev_tagl_close'] = "</li>";
   $config['first_tag_open'] = "<li>";
   $config['first_tagl_close'] = "</li>";
   $config['last_tag_open'] = "<li>";
   $config['last_tagl_close'] = "</li>";
  
   $this->pagination->initialize($config);
   
   $data['halaman'] = $this->pagination->create_links();
   /*membuat variable halaman untuk dipanggil di view nantinya*/
   $data['offset'] = $offset;

   $data['data'] = $this->model->view_agen($config['per_page'], $offset);
		$data['header']="template/kosong";
		$data['filter']="template/kosong";
		$data['footer_agen']="template/kosong";
		$data['footer_count']="template/footer_testi";
		$data['konten']="agen";
		$this->_template($data);
	}
	
	public function agen_listing($id,$offset=0)
	{
		 $jml = $this->model->get_query_agen_listing($id);

   $config['base_url'] = base_url().'public_page/agen_listing/'.$id;
   
   $config['total_rows'] = $jml->num_rows();
   $config['per_page'] = 8; /*Jumlah data yang dipanggil perhalaman*/ 
   $config['uri_segment'] = 4; /*data selanjutnya di parse diurisegmen 3*/
   
   /*Class bootstrap pagination yang digunakan*/
   $config['full_tag_open'] = "<ul class='pagination pagination-sm' style='position:relative; top:-25px;'>";
   $config['full_tag_close'] ="</ul>";
   $config['num_tag_open'] = '<li>';
   $config['num_tag_close'] = '</li>';
   $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
   $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
   $config['next_tag_open'] = "<li>";
   $config['next_tagl_close'] = "</li>";
   $config['prev_tag_open'] = "<li>";
   $config['prev_tagl_close'] = "</li>";
   $config['first_tag_open'] = "<li>";
   $config['first_tagl_close'] = "</li>";
   $config['last_tag_open'] = "<li>";
   $config['last_tagl_close'] = "</li>";
  
   $this->pagination->initialize($config);
   
   $data['halaman'] = $this->pagination->create_links();
   /*membuat variable halaman untuk dipanggil di view nantinya*/
   $data['offset'] = $offset;

   $data['data'] = $this->model->view_agen_listing($config['per_page'], $offset,$id);
		$data['header']="template/kosong";
		$data['filter']="template/kosong";
		$data['footer_agen']="template/kosong";
		$data['footer_count']="template/footer_testi";
		$data['konten']="agen_listing";
		$this->_template($data);
	}
	public function network()
	{

		$data['header']="template/kosong";
		$data['filter']="template/kosong";
		$data['footer_agen']="template/kosong";
		$data['footer_count']="template/kosong";
		$data['konten']="network";
		$this->_template($data);
	}public function hubungi_kami()
	{

		$data['header']="template/kosong";
		$data['filter']="template/kosong";
		$data['footer_agen']="template/kosong";
		$data['footer_count']="template/footer_count";
		$data['konten']="hubungi_kami";
		$this->_template($data);
	}public function tentang_kami()
	{

		$data['header']="template/kosong";
		$data['filter']="template/kosong";
		$data['footer_agen']="template/kosong";
	   $data['footer_count']="template/footer_testi";
		$data['konten']="tentang_kami";
		$this->_template($data);
	}
	function direct($id)
	{
		$data=$this->db->query("select * from data_property where id_prop='".$id."'")->num_rows();
		if(!$data){ redirect(""); }
	}
	public function show_listing($id)
	{
		$this->direct($id);
		$data['header']="template/kosong";
		$data['filter']="template/kosong";
		$data['konten']="show_listing";
		$data['footer_agen']="template/footer_agen";
		$data['footer_count']="template/footer_count";
		$data['id']=$id;		
		$this->_template($data);
	}
	

		function ajaxGetKab()
	{
		$id=$this->input->post("prov");
		$def=$this->input->post("def");
		$data=$this->reff->getKab($id);
		if($data)
		{
			$kab[""]="===== All =====";
		 foreach ($data as $val) {
			// $kab=str_replace("KABUPATEN","KAB",);
				 $kab[$val->id] = strtolower($val->kabupaten);
                 }
                 $dataKab = $kab;
                echo form_dropdown('kabupaten', $dataKab, $def, ' id="kabupaten" class="form-control" style="width:100%" onchange="return cek_kab()"');     
		echo "<script>	$('#kabupaten').select2();</script>";
		}else{
			$kab[""]="===== All =====";
			echo form_dropdown('kabupaten', $kab, '', ' id="kabupaten" class="form-control" style="width:100%"');     
			echo "<script>	$('#kabupaten').select2();</script>";
		}
	}
	
	//******#########################################################################---------------------------------------//
	public function more_content_agen()
	{
			
		 
		
		
		if(isset($_POST['getLastContentId']))
		{
			$getLastContentId=$_POST['getLastContentId'];
		$sql=$this->db->query("select *  from data_agen where jabatan IN ('1','11','12') and aktifasi='1' order by id_agen ASC limit $getLastContentId,27");
		$count=$sql->num_rows();
		if($count>0){
		foreach($sql->result() as $agen)
		{
			   $id=$agen->id_agen;
				if($agen->poto){
				$poto=$agen->poto;
			}else{
				if($agen->jk=="l")
				{
				$poto="ag-boy.png";
				}else{
				$poto="ag-girl.png";
				}
			}
				$hp1=	$hpo1=isset($agen->hp)?($agen->hp):"";
	$hp1=substr($hpo1,0,4);	$hp2=substr($hpo1,4,4);	$hp3=substr($hpo1,8,4);
	$hp1=$hp1."-".$hp2."-".$hp3;
	
	$hp2=	$hpo2=isset($agen->hp2)?($agen->hp2):"";
	if($hp2){
	$hp2=substr($hpo2,0,4);	$hp3=substr($hpo2,4,4);	$hp4=substr($hpo2,8,4);
	$hp2=" / ".$hp2."-".$hp3."-".$hp4;
	}
		?>
		  <div class="col-sm-4">
	  <div class="agent-small">
                                        <div class="agent-small-top" style="background-color:white">
                                            <div class="clearfix">
                                                <div class="agent-small-picture col-sm-12 col-md-4">
                                                    <div class="agent-small-picture-inner">
                                                        <a href="javascript:void(0)" class="agent-small-picture-inner ">
                                                           <img style="height:120px" src="<?php echo base_url()?>file_upload/agen/<?php echo $poto;?>" alt="">
                                                        </a><!-- /.agent-small-picture-target -->
                                                    </div><!-- /.agent-small-picture-inner -->
                                                </div><!-- /.agent-small-picture -->

                                                <div class="agent-small-content col-sm-12 col-md-8">
                                                    <h3 class="agent-small-title"><a href="javascript:void(0)"><?php echo SUBSTR($agen->nama,0,23); ?></a></h3>
                                                    <h4 class="agent-small-subtitle"><a href="javascript:void(0)"><?php echo $this->reff->totalListingReadyAgenByKode($agen->kode_agen);?> properties in catalogue</a></h4>
                                             <!--       <a href="#" class="btn btn-regular" style="margin-top:-10px">View Profile</a>-->
                                                </div><!-- /.agent-small-content -->
                                            </div><!-- /.row -->
                                        </div><!-- /.agent-small-top -->

                                        <div class="agent-small-bottom" style="color:white">
                                            <ul class="list-unstyled">
                                               <li><i class="fa fa-phone"></i> <?php echo $hp1; ?>   <?php echo $hp2; ?> </li>
                                                <li><i class="fa fa-envelope-o"></i>  <?php echo isset($agen->email)?($agen->email):"-";?> </li>
                                            </ul>
                                        </div><!-- /.agent-small-bottom -->
                                        </div><!-- /.agent-small-bottom -->
	
	</div>
		<!-- /.property-item -->
		<?php } ?>

	<center id="load_more_<?php echo $id; ?>"><div class="row"></div>
	<div class="more_div"> <div  class="more_tab">
	<button  onclick="moreLoad_agen(`<?php echo $id; ?>`)" class="more_buttons btn btn-primary"  id="<?php echo $id; ?>">Load More Content</button> </div>
	</div>
	</center>
		
		<?php
		} else{
		echo "<center><div class='row'></div> <div class='all_loaded'>No More Content to Load...</div>	</div></center>";
		}
		
		}
	//******#########################################################################---------------------------------------//
	
}
public function more_content()
	{
			
		 $pilih=$this->input->post("pilih"); 
		 $type_pro=$this->input->post("type_pro"); 
		 $provinsi=$this->input->post("provinsi"); 
		 $kabupaten=$this->input->post("kabupaten"); 
		 $ID=$this->input->post("ID"); 
		 $kt=$this->input->post("kt"); 
		 $km=$this->input->post("km"); 
		 $harga=$this->input->post("harga"); 
		 $urut=$this->input->post("urut"); 
		 if($urut=="terbaru")
		 {
			 //$by="id_prop DESC"; //$urut="ASC";
			 $by="created_time DESC";
		 }
		 elseif($urut=="ASC")
		 {
		     $by="harga ASC";
		 }elseif($urut=="DESC"){
			 $by ="harga DESC"; 
		 }else{
		     //$by="id_prop DESC";
			 $by="update_time DESC";
			 //$by = "gambar1 DESC,gambar2 DESC,gambar_utama DESC";
		 }
		$filter="";
		if($pilih)
		{	
		 $filter.="AND type_jual='".$pilih."'";
		}
		
		if($type_pro)
		{	
		 $filter.="AND jenis_prop='".$type_pro."'";
		}
		
		if($provinsi)
		{	
		 $filter.="AND id_prov='".$provinsi."'";
		}
		if($kabupaten)
		{	
		 $filter.="AND id_kab='".$kabupaten."'";
		}
		if($ID)
		{	
	         $filter.="AND (area_listing  LIKE '%".$ID."%' or komplek  LIKE '%".$ID."%'  or nama_area  LIKE '%".$ID."%' or alamat_detail  LIKE '%".$ID."%') ";
		}
		if($kt)
		{	
			if($kt=="10")
			{
				 $filter.="AND kamar_tidur>='".$kt."'";
			}else{
			 $filter.="AND kamar_tidur='".$kt."'";
			}
		}
		if($km)
		{	
			if($km=="10")
			{
				 $filter.="AND kamar_mandi>='".$km."'";
			}else{
			 $filter.="AND kamar_mandi='".$km."'";
			}
		}
		if($harga)
		{	
			$ha=explode("-",$harga);
			if(count($ha)>1)
			{
				 $filter.="AND (harga>='".$ha[0]."' AND harga<='".$ha[1]."') ";
			}else{
				 $filter.="AND harga>='50000000000'";
			}
		}
		
		
		
		
		if(isset($_POST['getLastContentId']))
		{
			$getLastContentId=$_POST['getLastContentId'];
			$page=$_POST['getLastContentId'];
		//$sql=$this->db->query("select *  from data_property WHERE status='0' $filter    ORDER BY id_prop DESC limit $getLastContentId,12");
		$sql=$this->db->query("select *  from data_property WHERE status IN ('0','1') $filter ORDER BY $by limit $getLastContentId,12");
		$count=$sql->num_rows();
		if($count>0){
			
		foreach($sql->result() as $val)
		{	 
			$id=$val->id_prop;
			if($val->status=="0"){
    		    if($val->type_jual=="jual")
    		    {
    			    $class="property-box-label property-box-label-primary";
    		    }else
    		    {
    			    $class="property-box-label";
    		    }   
		    }else{
		        $class="property-box-label property-box-label-danger";
		    }
		$isigambar=0;
		$gambar="not-avaliable.jpg";
		if(strlen($val->gambar_utama)>3)
		{
			$utama=$val->gambar_utama;
			$gambar=$val->$utama;
			$isigambar=$gambar;
		}
		
		if(strlen($isigambar)<3)
		{
		 	$gambar="not-avaliable.jpg";
		}
		?>
		 <div class="property-item property-sale col-sm-6 col-md-3" style="cursor:pointer" onclick="getlink('<?php echo base_url()?>public_page/show_listing/<?php echo $val->id_prop;?>/<?php echo str_replace(" ","-",$val->desc)?>')">
        <div class="property-box" style="border-radius:15px">
            <div class="property-box-inner">
                <h4 class="property-box-title" style="min-height:50px;max-height:50px"><a href="javascript:void(0)"><?php echo $this->reff->titleWeb($val->kode_prop);?></a></h4>
                <h4 class="property-box-subtitle"><a href="javascript:void(0)">ID : <?php echo $val->kode_prop;?></a></h4>

                <div class="<?php echo $class;?>">
                    <?php 
                    if($val->status=="0")
                    { 
                        echo $val->type_jual;
                    }else{   
                        if($val->type_jual=="jual"){
                            echo 'terjual';
                        }elseif($val->type_jual=="sewa"){
                            echo 'tersewa';
                        }
                    } 
                     ?>
                </div>
                <!-- /.property-box-label -->

                <div class="property-box-picture" style="margin-top:-30px">
                    <div class="property-box-price">Rp <?php echo number_format($val->harga+$val->fee_up,0,",",".")?></div>
                    <!-- /.property-box-price -->
                    <div class="property-box-picture-inner">
                         <span class="property-box-picture-target">
                                <a href="javascript:void(0)" class="property-box-picture-target">
                            <img height="270px" src="<?php echo base_url()?>file_upload/img/<?php echo $gambar;?>" alt="">
                            </a>
                        </span><!-- /.property-box-picture-target -->
                    </div>
                    <!-- /.property-picture-inner -->
                </div>
                <!-- /.property-picture -->

               <div class="property-box-meta">
                    <div class="property-box-meta-item col-xs-6 col-sm-6">
                        <strong><?php echo isset($val->luas_tanah)?($val->luas_tanah):"-";?> M<sup>2</sup></strong>
                        <span>LT</span>
                    </div>
                    <!-- /.col-sm-3  

                    <div class="property-box-meta-item col-xs-4 col-sm-4">
                        <strong><?php echo isset($val->kamar_tidur)?($val->kamar_tidur):"-";?></strong>
                        <span>Beds</span>
                    </div>
                    <!-- /.col-sm-3 -->

                   

                    <div class="property-box-meta-item col-xs-6 col-sm-6">
                        <strong><?php echo  isset($val->luas_bangunan)?($val->luas_bangunan):"-";?>  M<sup>2</sup></strong>
                        <span>LB</span>
                    </div>
                    <!-- /.col-sm-3 -->
                </div>
                <!-- /.property-box-meta -->
            </div>
            <!-- /.property-box-inner -->
        </div>
        <!-- /.property-box -->
    </div>
		<!-- /.property-item -->
		<?php $page++; } ?>

	<center id="load_more_<?php echo $page; ?>"><div class="row"></div>
	<div class="more_div"> <div  class="more_tab">
	<button  onclick="moreLoad(`<?php echo $page; ?>`)" class="more_buttons btn btn-primary"  id="<?php echo $page; ?>">Load More Content</button> </div>
	</div>
	</center>
		
		<?php
		} else{
		echo "<center><div class='row'></div> <div class='all_loaded'>No More Content to Load...</div>	</div></center>";
		}
		
		}
	//******#########################################################################---------------------------------------//
	
}

//******#########################################################################---------------------------------------//
	public function more_content_brosur()
	{
			
		 $pilih=$this->input->post("pilih"); 
		 $type_pro=$this->input->post("type_pro"); 
		 $provinsi=$this->input->post("provinsi"); 
		 $kabupaten=$this->input->post("kabupaten"); 
		 $ID=$this->input->post("ID"); 
		 $kt=$this->input->post("kt"); 
		 $km=$this->input->post("km"); 
		 $harga=$this->input->post("harga"); 
				$filter="";
		if($pilih)
		{	
		 $filter.="AND type_jual='".$pilih."'";
		}
		
		if($type_pro)
		{	
		 $filter.="AND jenis_prop='".$type_pro."'";
		}
		
		if($provinsi)
		{	
		 $filter.="AND id_prov='".$provinsi."'";
		}
		if($kabupaten)
		{	
		 $filter.="AND id_kab='".$kabupaten."'";
		}
		if($ID)
		{	
		  $filter.="AND alamat_detail  LIKE '%".$ID."%'";
		}
		if($kt)
		{	
			if($kt=="10")
			{
				 $filter.="AND kamar_tidur>='".$kt."'";
			}else{
			 $filter.="AND kamar_tidur='".$kt."'";
			}
		}
		if($km)
		{	
			if($km=="10")
			{
				 $filter.="AND kamar_mandi>='".$km."'";
			}else{
			 $filter.="AND kamar_mandi='".$km."'";
			}
		}
		if($harga)
		{	
			$ha=explode("-",$harga);
			if(count($ha)>1)
			{
				 $filter.="AND (harga>='".$ha[0]."' AND harga<='".$ha[1]."') ";
			}else{
				 $filter.="AND harga>='50000000000'";
			}
		}
		
		
		
		
		if(isset($_POST['getLastContentId']))
		{
			$getLastContentId=$_POST['getLastContentId'];
		//$sql=$this->db->query("select *  from data_property WHERE status='0' and desain!='' $filter order by desain DESC  limit $getLastContentId,12");
		$sql=$this->db->query("select *  from data_property WHERE status in ('0','1') and desain!='' $filter order by desain DESC  limit $getLastContentId,12");
		$count=$sql->num_rows();
		if($count>0){
		foreach($sql->result() as $val)
		{
			$id=$val->id_prop;
		    if($val->status=="0"){
    		    if($val->type_jual=="jual")
    		    {
    			    $class="property-box-label property-box-label-primary";
    	    	}else
    		    {
    			    $class="property-box-label";
    		    }
		    }else{
		        $class="property-box-label property-box-label-danger";
		    }
		?>
		 <div class="property-item property-sale col-sm-6 col-md-3" >
        <div class="property-box">
            <div class="property-box-inner">
                  <div class="property-box-picture" style="margin-top:-43px">
                   
                    <!-- /.property-box-price -->
                    <div class="property-box-picture-inner">
                      <span  class="property-box-picture-target">
                             <a href="javascript:void(0)" class="property-box-picture-target">
                            <img height="270px" src="<?php echo base_url()?>file_upload/img/<?php echo $val->desain;?>" alt="">
                            </a>
                        </span><!-- /.property-box-picture-target -->
                    </div>
                    <!-- /.property-picture-inner -->
                </div>
                <!-- /.property-picture -->

              
            </div>
            <!-- /.property-box-inner -->
        </div>
        <!-- /.property-box -->
    </div>
    <!-- /.property-item -->
		<?php } ?>

	<center id="load_more_<?php echo $id; ?>"><div class="row"></div>
	<div class="more_div"> <div  class="more_tab">
	<button  onclick="moreLoad_brosur(`<?php echo $id; ?>`)" class="more_buttons btn btn-primary"  id="<?php echo $id; ?>">Load More Content</button> </div>
	</div>
	</center>
		
		<?php
		} else{
		echo "<center><div class='row'></div> <div class='all_loaded'>No More Content to Load...</div>	</div></center>";
		}
		
		}
	//******#########################################################################---------------------------------------//
	
	}
	
	
	function filter()
	{
		$this->load->view("filter");
	}
	
	function filter_konten_brosur()
	{
		$this->load->view("filter_konten_brosur");
	}
	function cekAkun($email,$hp)
	{	 
		$this->db->where("username",$email);
		$this->db->where("password",md5($hp));
		$akun1=$this->db->get("admin")->num_rows();
		
		$this->db->where("username",$email);
		$this->db->where("password", $hp);
		$akun2=$this->db->get("data_agen")->num_rows();
	return $akun1+$akun2;		
	}
	function insert_network()
	{
		 
		$hp=$this->input->post("hp");
		$email=$this->input->post("email");
		$cek=$this->cekAkun(strip_tags($email),strip_tags($hp));
		if($cek)
		{
			echo "no";
		}else{
			$this->insertNetwork();
		}
	}
	private function insertNetwork()
	{
		$nama=strip_tags($this->input->post("nama"));
		$ktp=strip_tags($this->input->post("ktp"));
		$hp=strip_tags($this->input->post("hp"));
		$email=strip_tags($this->input->post("email"));
		$alamat=strip_tags($this->input->post("alamat"));
		$kota=strip_tags($this->input->post("kota"));
		$rek=strip_tags($this->input->post("rek"));
		$kode=$this->reff->kodeNetwork();
		$data=array(
		"kode_agen"=>$kode,
		"jabatan"=>100,
		"nama"=>$nama,
		"hp"=>$hp,
		"email"=>$email,
		"alamat"=>$kota." - ".$alamat,
		"ktp"=>$ktp,
		"no_rek"=>$rek,
		"username"=>$email,
		"password"=>$hp,
		);
		$this->db->insert("data_agen",$data);
	return $this->reff->updateCountNetwork();
		
	}
	
	/*---------------------------------------------------------------*/
  
  public function index()  {
  
   $this->view();
  }
  function artikel($offset=0)
  {
	     $this->view($offset);
  }
  public function view($offset=0) {
  
   $jml = $this->model->get_query();

   $config['base_url'] = base_url().'public_page/artikel';
   
   $config['total_rows'] = $jml->num_rows();
   $config['per_page'] = 5; /*Jumlah data yang dipanggil perhalaman*/ 
   $config['uri_segment'] = 3; /*data selanjutnya di parse diurisegmen 3*/
   
   /*Class bootstrap pagination yang digunakan*/
   $config['full_tag_open'] = "<ul class='pagination pagination-sm' style='position:relative; top:-25px;'>";
   $config['full_tag_close'] ="</ul>";
   $config['num_tag_open'] = '<li>';
   $config['num_tag_close'] = '</li>';
   $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
   $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
   $config['next_tag_open'] = "<li>";
   $config['next_tagl_close'] = "</li>";
   $config['prev_tag_open'] = "<li>";
   $config['prev_tagl_close'] = "</li>";
   $config['first_tag_open'] = "<li>";
   $config['first_tagl_close'] = "</li>";
   $config['last_tag_open'] = "<li>";
   $config['last_tagl_close'] = "</li>";
  
   $this->pagination->initialize($config);
   
   $data['halaman'] = $this->pagination->create_links();
   /*membuat variable halaman untuk dipanggil di view nantinya*/
   $data['offset'] = $offset;

   $data['data'] = $this->model->view_($config['per_page'], $offset);
   
    $this->_view($data);
   /*memanggil view pagination*/
  }
  
  private function _view($data)
	{

		$data['header']="template/header_slider";
		$data['filter']="template/filter";
		$data['footer_agen']="template/kosong";
		$data['footer_count']="template/footer_count";
		$data['konten']="artikel";
		$this->_template($data);
	}
	
	function testimoni($offset=0)
	{
	$jml = $this->model->get_query_testimoni();
     $config['base_url'] = base_url().'public_page/testimoni';
   
   $config['total_rows'] = $jml->num_rows();
   $config['per_page'] = 6; /*Jumlah data yang dipanggil perhalaman*/ 
   $config['uri_segment'] = 3; /*data selanjutnya di parse diurisegmen 3*/
   
   /*Class bootstrap pagination yang digunakan*/
   $config['full_tag_open'] = "<ul class='pagination pagination-sm' style='position:relative; top:-25px;'>";
      $config['full_tag_close'] ="</ul>";
   $config['num_tag_open'] = '<li>';
   $config['num_tag_close'] = '</li>';
   $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
   $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
   $config['next_tag_open'] = "<li>";
   $config['next_tagl_close'] = "</li>";
   $config['prev_tag_open'] = "<li>";
   $config['prev_tagl_close'] = "</li>";
   $config['first_tag_open'] = "<li>";
   $config['first_tagl_close'] = "</li>";
   $config['last_tag_open'] = "<li>";
   $config['last_tagl_close'] = "</li>";
  
   $this->pagination->initialize($config);
   
   $data['halaman'] = $this->pagination->create_links();
   /*membuat variable halaman untuk dipanggil di view nantinya*/
   $data['offset'] = $offset;

   $data['data'] = $this->model->view_testi($config['per_page'], $offset);
   
		$data['header']="template/kosong";
		$data['filter']="template/kosong";
		$data['footer_agen']="template/kosong";
		$data['footer_count']="template/footer_count";
		$data['konten']="testi";
		$this->_template($data);
	}
  
	/*---------------------------------------------------------------*/

}
