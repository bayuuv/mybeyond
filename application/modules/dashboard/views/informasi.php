<div class="row">
<div class="col-lg-6 col-sm-6 col-xs-12" style="color:white">
<div class="main-box infographic-box emerald-bg ">
<i class="fa fa-cubes purple-bg"></i>
<span class="headline">Asset</span>
<span class="value ">Rp <?php echo number_format($this->reff->jumlahAset(),0,",",".");?></span>
</div>
</div>


<?php 
    date_default_timezone_set('Asia/Jakarta');
    $today = date("Y");
?>
<div class="col-lg-6 col-sm-6 col-xs-12" style="color:white">
<div class="main-box infographic-box emerald-bg  ">
<i class="fa  fa-shopping-cart emerald-bg"></i>
<span class="headline">Omzet <?php echo $today; ?></span>
<span class="value">Rp <?php echo number_format($this->reff->jumlahOmzet($today),0,",",".");?></span>
</div>
</div>

 


<div class="col-lg-4 col-sm-4 col-xs-12">
<div class="main-box infographic-box colored red-bg">
<i class="fa fa-list"></i>
<span class="headline">Total Listing</span>
<span class="value"><?php echo $this->reff->totalListingReady();?></span>
</div>
</div>
<div class="col-lg-4 col-sm-4 col-xs-12">
<div class="main-box infographic-box colored red-bg">
<i class="fa fa-check-circle"></i>
<span class="headline">Sold By Member</span>
<span class="value"><?php echo $this->reff->listingTerjualTersewa();?></span>
</div>
</div>
<div class="col-lg-4 col-sm-4 col-xs-12">
<div class="main-box infographic-box colored red-bg">
<i class="fa fa-check-circle"></i>
<span class="headline">Sold By Other</span>
<span class="value"><?php echo $this->reff->listingTerjualTersewaOther();?></span>
</div>
</div>

<div class="col-lg-4 col-sm-6 col-xs-12">
<div class="main-box infographic-box colored purple-bg">
<i class="fa fa-bank"></i>
<span class="headline">Selling</span>
<span class="value"><?php echo $this->reff->dijualReady();?></span>
</div>
</div>
<div class="col-lg-4 col-sm-6 col-xs-12">
<div class="main-box infographic-box colored blue-bg">
<i class="fa fa-bank"></i>
<span class="headline">Lease</span>
<span class="value"><?php echo $this->reff->disewakanReady();?></span>
</div>
</div>

<div class="col-lg-4 col-sm-6 col-xs-12">
<div class="main-box infographic-box colored purple-bg">
<i class="fa fa-bank"></i>
<span class="headline">  Cancel </span>
<span class="value"><?php echo $this->reff->listingCancel();?></span>
</div>
</div>
</div>
<div class="row">
<div class="col-lg-4 col-sm-6 col-xs-12">
<div class="main-box infographic-box">
<i class="fa fa-user red-bg"></i>
<span class="headline">Member</span>
<span class="value">
<span class="timer" data-from="120" data-to="2562" data-speed="1000" data-refresh-interval="50">
<?php echo $this->reff->jumlahAgen();?>
</span>
</span>
</div>
</div>

<div class="col-lg-4 col-sm-6 col-xs-12">
<div class="main-box infographic-box">
<i class="fa fa-user emerald-bg"></i>
<span class="headline">Staff</span>
<span class="value">
<span class="timer" data-from="30" data-to="658" data-speed="800" data-refresh-interval="30">
<?php echo $this->reff->jumlahPegawai();?>
</span>
</span>
</div>
</div>

<div class="col-lg-4 col-sm-6 col-xs-12">
<div class="main-box infographic-box">
<i class="fa fa-user red-bg"></i>
<span class="headline">Buyer</span>
<span class="value">
<span class="timer" data-from="30" data-to="658" data-speed="800" data-refresh-interval="30">
<?php echo $this->reff->jumlahCostumer();?>
</span>
</span>
</div>
</div>



<div class="col-lg-6 col-sm-6 col-xs-12">
<div class="main-box infographic-box">
<i class="fa fa-user emerald-bg"></i>
<span class="headline">Vendor</span>
<span class="value">
<span class="timer" data-from="30" data-to="658" data-speed="800" data-refresh-interval="30">
<?php echo $this->reff->jumlahOwner();?>
</span>
</span>
</div>
</div>


<div class="col-lg-6 col-sm-6 col-xs-12">
<div class="main-box infographic-box">
<i class="fa fa-user red-bg"></i>
<span class="headline">Network</span>
<span class="value">
<span class="timer" data-from="30" data-to="658" data-speed="800" data-refresh-interval="30">
<?php echo $this->reff->jumlahNetwork();?>
</span>
</span>
</div>
</div>

 
</div>




<style>
.history {
    height: 350px;
    overflow: scroll;
}
.title,.item{
color:black;
font-size:13px;
line-height:15px;
}
.time-ago{
	font-size:13px;
}
</style>


 <script>
 setTimeout(function(){   $('.history').scrollTop(100000000000000); }, 1000);
 </script>
 
 
 
 <script>
	getHistory();
	function getHistory()
	{		 $('.datahistory').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Mohon Tunggu ... ");
	            $.ajax({
                type: "POST",
            
                url: "<?php echo base_url();?>dashboard_ag/getHistoryTimeLine/",
                success: function (data) {
			       $(".datahistory").html(data);
				   $('.history').scrollTop(100000000000000);
                }
            });
	}
</script>	

<script>
	function simpan()
	{		
			var text = $("[name='textListing']").val();
			$('.datahistory').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Mohon Tunggu ... ");
	            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>dashboard_ag/saveReport/",
                data: "text="+text,
                success: function (data) {
					getHistory();
			        $("[name='textListing']").val("");
                }
            });
	}
	function hapus(id)
	{	var a=window.confirm("Hapus ?");
		if(a==false){
			return false;
		}
		$('.datahistory').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Mohon Tunggu ... ");
		     $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>dashboard_ag/hapusTimeLine/"+id,
                success: function (data) {
					getHistory();
			    }
            });
	}
	
	</script>

<div class="col-lg-12 col-sm-12 col-xs-12">
<div class="main-box infographic-box">
<span class="value">
<span class="timer" data-from="30" data-to="658" data-speed="800" data-refresh-interval="30">
<div style="width:75%;">
        <canvas id="canvas"></canvas>
</div>
</span>
</span>
</div>
</div>

<div class="col-lg-12 col-sm-12 col-xs-12">
<div class="main-box infographic-box">
<span class="value">
<span class="timer" data-from="30" data-to="658" data-speed="800" data-refresh-interval="30">
<div style="width:75%;">
        <canvas id="canvas1"></canvas>
</div>
</span>
</span>
</div>
</div>

<?php 
$dataOmzets=$this->reff->getDataOmzet();
foreach($dataOmzets as $vala)
{
    $tahun[]	 	= $vala->TAHUN;
    $jmlharga[]		= $vala->jmlHarga;
}
$dataOmzets=$this->reff->getDataOmzetbulanan();
foreach($dataOmzets as $vala)
{
    $bulan[]	 	= $vala->bulan;
    $jmlharga1[]		= $vala->jmlHarga1;
}
?>

    <!--Load chart js-->
    <script type="text/javascript" src="<?php echo base_url().'public_page_asset/assets/libraries/chartjs/Chart.bundle.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'public_page_asset/assets/libraries/chartjs/utils.js'?>"></script>
    <script>
        var config = {
            type: 'line',
            data: {
                labels: <?php echo json_encode($tahun);?>,
                datasets: [{
                    label: "Omset",
                    backgroundColor: window.chartColors.red,
                    borderColor: window.chartColors.red,
                    data: <?php echo json_encode($jmlharga);?>,
                    fill: false,
                }]
            },
            options: {
                responsive: true,
                title:{
                    display:true,
                    text:'Grafik Omzet Per Tahun'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                    callbacks: {
                        label: function(tooltipItem, data) {
                            return 'Rp. ' + tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + ',00';
                        },
                    },
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        ticks: {},
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Tahun'
                        }
                    }],
                    yAxes: [{
						/*display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Rupiah'
                        }*/
                        ticks: {
                            callback: function(label, index, labels) { return label/1000000; },
                            beginAtZero:true
                        },
                        gridLines: {
                            display: false
                        },
                        scaleLabel: { 
                            display: true,
                            labelString: 'Rp Perjuta'
                        }
                        /*
                        ticks: {
                            beginAtZero: true,
                            stepSize: 500000,
           	                // Return an empty string to draw the tick line but hide the tick label
           	                // Return `null` or `undefined` to hide the tick line entirely
           	                userCallback: function(value, index, values) {
                                // Convert the number to a string and splite the string every 3 charaters from the end
                                value = value.toString();
                                value = value.split(/(?=(?:...)*$)/);
                
                                // Convert the array to a string and format the output
                                value = value.join('.');
                                return '€' + value;
            	            }
                        }*/
                        
                    }]
                }
            }
        };
		
		var config1 = {
            type: 'line',
            data: {
                labels: <?php echo json_encode($bulan);?>,
                datasets: [{
                    label: "Omset",
                    backgroundColor: window.chartColors.blue,
                    borderColor: window.chartColors.blue,
                    data: <?php echo json_encode($jmlharga1);?>,
                    fill: false,
                }]
            },
            options: {
                responsive: true,
                title:{
                    display:true,
                    text:'Grafik Omzet Per Bulan'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                    callbacks: {
                        label: function(tooltipItem, data) {
                            return 'Rp. ' + tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + ',00';
                        },
                    },
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        ticks: {},
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Bulan'
                        }
                    }],
                    yAxes: [{
						/*display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Rupiah'
                        }*/
                        ticks: {
                            callback: function(label, index, labels) { return label/1000000; },
                            beginAtZero:true
                        },
                        gridLines: {
                            display: false
                        },
                        scaleLabel: { 
                            display: true,
                            labelString: 'Rp Perjuta'
                        }
                        /*
                        ticks: {
                            beginAtZero: true,
                            stepSize: 500000,
           	                // Return an empty string to draw the tick line but hide the tick label
           	                // Return `null` or `undefined` to hide the tick line entirely
           	                userCallback: function(value, index, values) {
                                // Convert the number to a string and splite the string every 3 charaters from the end
                                value = value.toString();
                                value = value.split(/(?=(?:...)*$)/);
                
                                // Convert the array to a string and format the output
                                value = value.join('.');
                                return '€' + value;
            	            }
                        }*/
                        
                    }]
                }
            }
        };

        window.onload = function() {
            var ctx = document.getElementById("canvas").getContext("2d");
            window.myLine = new Chart(ctx, config);
			
			 var ctx1 = document.getElementById("canvas1").getContext("2d");
            window.myLine = new Chart(ctx1, config1);
        };
         
    </script>



<div class="row">

<!--------------------->
<div class="col-lg-4 col-sm-12 col-xs-12" style="color:white">
<!------------------------>
<div class="main-box feed ">
<header class="black sadow main-box-header clearfix">
<h2 class="pull-left">Timeline</h2>
</header>
<div class="main-box-body clearfix history">
<ul>
<!---------------------->
<span class="datahistory"> </span>
<!---------------------->
</ul>
</div>
</div>


<div class="conversation-new-message">
<form action="javascript:simpan()">
<div class="form-group">
<textarea class="form-control chat-input" rows="2" name="textListing" placeholder="Enter your message..."></textarea>
</div>
<div class="clearfix chat-send">
<button type="submit" class="btn btn-success pull-right">Send message</button>
</div>
</form>
</div>
<!------------------------>
</div>
<!--------------------->













<div class="col-lg-4">
<div class="main-box no-header clearfix">
<div class="main-box-body clearfix">
<div class="table-responsive">
<table class="table user-list table-hover">
<thead>
<tr>
<th><span>Top Member</span></th>
<th class="text-center"><span>  Closing</span></th>
 
 
</tr>
</thead>
<tbody> 

<?php 
$dataTopAgen=$this->reff->getTopAgen(5,$today);
foreach($dataTopAgen as $vala)
{
?>

<tr>
<td>
<img src="<?php echo base_url()?>file_upload/agen/<?php echo $this->reff->getPotoAgen($vala->kode_agen);?>" alt=""/>
<a href="javascript:void(0)" class="user-link"><?php echo $vala->nama;?></a>
 <span class="user-subhead">Rp <?php echo number_format($vala->biaya,0,",",".");?></span>
</td>
 
<td class="text-center" style="font-size:17px">
<span class="label label-info"><?php echo $vala->j;?></span>
</td>
</tr>
 
<?php } ?>
</tbody>
</table>
</div>
</div>
</div>
</div>
 
 
 
 
 
 
<div class="col-lg-4">
<div class="main-box no-header clearfix">
<div class="main-box-body clearfix">
<div class="table-responsive">
<table class="tabel table-bordered table-hover black">
<thead>
<tr>
<th><span>&nbsp;Summary</span></th>
<th class="text-center"><span>  Listing</span></th>
 
 
</tr>
</thead>
<tbody> 

<?php 
$getTopProp=$this->reff->getTopProp(10);
foreach($getTopProp as $top)
{
?>

<tr>
<td>
 <?php echo $this->reff->getNamaJenis($top->jenis_prop);?> 
</td>
 
<td class="text-center black">
 <b><?php echo $top->jml;?></b>
</td>
</tr>
 
<?php } ?>
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
 