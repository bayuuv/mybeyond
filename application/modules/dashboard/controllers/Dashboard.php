<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	

	function __construct()
	{
		parent::__construct();	
		$this->m_konfig->validasi_session(array("user","dm","operator"));
	}
	
	function _template($data)
	{
	 $this->load->view('template/main',$data);	
	}
	
	public function index()
	{
	$data['konten']="informasi";
	$this->_template($data);
	}

	
}

