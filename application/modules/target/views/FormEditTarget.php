<?php
$this->db->where("id_target",$id);
$db=$this->db->get("tr_target")->row();

$bln = $db->bulan;
$bulandb = explode("-",$bln);
?>
<form action="javascript:saveEditTarget()"  id="formTarget" class="form-horizontal black" method="post"  enctype="multipart/form-data"  >
<input type="hidden" name="id_target" id="id_target" value="<?php echo $id;?>">
<div class="form-group">
<label for="tgl_promo" class="b col-lg-3 control-label">Date</label>
<div class="col-lg-4">
<?php                                        
	$arrayT[""] = "==== Bulan ====";
	$arrayT["01"] = "Januari";
	//$arrayT["02"] = "Februari";
	//$arrayT["03"] = "Maret";
	//$arrayT["04"] = "April";
	$arrayT["05"] = "Mei";
	//$arrayT["06"] = "Juni";
	//$arrayT["07"] = "Juli";
	//$arrayT["08"] = "Agustus";
	$arrayT["09"] = "September";
	//$arrayT["10"] = "Oktober";
	//$arrayT["11"] = "Nopember";
	//$arrayT["12"] = "Desember";
	$data = $arrayT;
	echo form_dropdown('bulan', $data, $bulandb[1], '  id="bulan" class="form-control" style="width:100%"');
?>
</div>
<div class="col-lg-4">
<input type="text" id="tahun"  name="tahun" value="<?php echo $bulandb[0]; ?>" class="form-control" style="width:100%">    
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<label for="jenis" class="b col-lg-3 control-label">Agen</label>
<div class="col-lg-8">
<?php
     if($this->session->userdata("id")==64){
										$ref_agen = $this->reff->getAgenAjeng();
										}elseif($this->session->userdata("id")==151){
										$ref_agen = $this->reff->getAgenRhafa();
										}elseif($this->session->userdata("id")==133){
										$ref_agen = $this->reff->getAgenKiki();
										}elseif($this->session->userdata("id")==146){
										$ref_agen = $this->reff->getAgenYudi();
										}elseif($this->session->userdata("id")==152){
										$ref_agen = $this->reff->getAgenVivi();
										}elseif($this->session->userdata("id")==161){
										$ref_agen = $this->reff->getAgenYema();
										}elseif($this->session->userdata("id")==162){
										$ref_agen = $this->reff->getAgenFrans();
										}elseif($this->session->userdata("id")==165){
										$ref_agen = $this->reff->getAgenRena();
										}else{
										$ref_agen = $this->reff->getAgen();
                                        }
	$array_agen[""] = "==== choose ====";
    foreach ($ref_agen as $val) {
    $array_agen[$val->kode_agen] = $val->nama;
    }
    $data = $array_agen;
    echo form_dropdown('agenx', $data, $db->agen, ' id="agenx2" class="select2-container" style="width:100%"');
?>
 <span class="help-block err_agen"></span>
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<label for="jenis" class="b col-lg-3 control-label">Kategori</label>
<div class="col-lg-8">
<?php                                        
    $arrayS[""] = "==== Pilih Kategori ====";
	$arrayS["1"] = "GOAL";
    $arrayS["2"] = "DEALING";
	$arrayS["3"] = "LISTING";
	$arrayS["4"] = "PROMOTION";
	//$arrayS["7"] = "PROMOTION OFFLINE";
	$arrayS["5"] = "CALL IN";
	$arrayS["6"] = "SHOWING";
    $data = $arrayS;
    echo form_dropdown('kategori', $data, $db->kategori, '  id="kategori"  class="form-control"');
?>
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<label for="alamat_promo" class="b col-lg-3 control-label">Jumlah </label>
<div class="col-lg-8">
<input type="text" class="form-control" id="jumlah"  name="jumlah" onkeydown="return numbersonly(this, event);" value="<?php echo number_format($db->jumlah,0,",",".");?>" >
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<div class="col-lg-offset-2 col-lg-9">
<span class='load'></span>
<button type="submit" class="btn btn-success pull-right" onclick="saveEditTarget()"><i class='fa fa-save'></i> Save</button>
</div>
</div>
</form>


<?php echo $this->load->view("js/form.phtml"); ?>
<script>
function saveEditTarget()
	{	
		var url="<?php echo base_url();?>target/update";
		$(".load").html("<img src='<?php echo base_url();?>plug/img/load.gif'> Please wait...");
		$("#formTarget").ajaxForm({
		url:url,
		type: "post",
		data: $('#formTarget').serialize(),
	//	dataType: "JSON",
		success: function(data)
				{
						  //if(data==false){ alert("Gagal! Data Sudah Ada"); $(".load").html(""); $("[name='kategori']").focus(); return false;}
						  closemodal("modalEditTarget");
						  table.ajax.reload(null,false); //reload datatable ajax 
				},
				
		});
	}
</script>

<script src="<?php echo base_url();?>plug/boostrap/js/jquery.maskedinput.min.js"></script>  
<script>
$("#tgl_promo").mask("99/99/9999");
</script>
<script src="<?php echo base_url() ?>plug/boostrap/js/select2.min.js"></script>
 <script>
  $('#agen').select2();
  $('#agenx2').select2();
</script>