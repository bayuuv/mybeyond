<div class="row">
    <div class="col-lg-12">

<div class="panel-group accordion" id="accordion">

<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
<i class="fa fa-filter"></i> Filter Pencarian
</a>
</h4>
</div>
<div id="collapseThree" class="panel-collapse collapse">
<div class="panel-body">
<!--------------------------->
<form id="formFilter" action="javascript:getTarget()">
								<div class="form-group fg_prov col-md-3" > 
                                         <label for="agen"></label>
									 <?php
									  if($this->session->userdata("id")==64){
										$ref_agen = $this->reff->getAgenAjeng();
										}elseif($this->session->userdata("id")==151){
										$ref_agen = $this->reff->getAgenRhafa();
										}elseif($this->session->userdata("id")==133){
										$ref_agen = $this->reff->getAgenKiki();
										}elseif($this->session->userdata("id")==146){
										$ref_agen = $this->reff->getAgenYudi();
										}elseif($this->session->userdata("id")==152){
										$ref_agen = $this->reff->getAgenVivi();
										}elseif($this->session->userdata("id")==161){
										$ref_agen = $this->reff->getAgenYema();
										}elseif($this->session->userdata("id")==162){
										$ref_agen = $this->reff->getAgenFrans();
										}elseif($this->session->userdata("id")==165){
										$ref_agen = $this->reff->getAgenRena();
										}else{
										$ref_agen = $this->reff->getAgen();
                                        }
                                        $array_agen[""] = "==== Pilih Agen ====";
                                        foreach ($ref_agen as $val) {
                                            $array_agen[$val->kode_agen] = $val->nama;
                                        }
                                        $data = $array_agen;
                                        echo form_dropdown('agen', $data, '', 'onchange="return cek_agen()"   id="agen" class="select2-container" style="width:100%"');   
										?>
									   <span class="help-block err_agen"></span>
                                  								
								</div>	
	
	
								<div class='col-md-3'>
                                     <div class="form-group black fg_tanah col-md-12">
                                    	 
												<label class="control-label black b" ></label>
												<?php                                        
												  $arrayT[""] = "==== Bulan ====";
												  //$arrayT["1111"] = "Semua Bulan";
												  $arrayT["01"] = "Januari";
												  /*$arrayT["02"] = "Februari";
												  $arrayT["03"] = "Maret";
												  $arrayT["04"] = "April";*/
												  $arrayT["05"] = "Mei";
												  /*$arrayT["06"] = "Juni";
												  $arrayT["07"] = "Juli";
												  $arrayT["08"] = "Agustus";*/
												  $arrayT["09"] = "September";
												  /*$arrayT["10"] = "Oktober";
												  $arrayT["11"] = "Nopember";
												  $arrayT["12"] = "Desember";*/
												  $data = $arrayT;
												  echo form_dropdown('bulan', $data, '', '  id="bulan"  class="form-control"');
												?>      
                                        
										   
												
									
									</div>
								</div>			
								<div class='col-md-3'>
                                     <div class="form-group black fg_tanah col-md-12">
                              
									<label for="status"></label>
												<?php                                        
												  $arrayB[""] = "==== Tahun ====";
												  $arrayB["1111"] = "Semua Tahun";
												  $arrayB["2018"] = "2018";
												  $data = $arrayB;
												  echo form_dropdown('tahun', $data, '', '  id="tahun"  class="form-control"');
												?>      
									</div>		
								</div>	 
							
							
								<div class='col-md-3'>
                                     <div class="form-group black fg_tanah col-md-12">							 
                              
									<label for="kategori"></label>
									 <?php
                                        $array_l[""] = "==== Kategori ====";
										$array_l["1"] = "GOAL";
                                        $array_l["2"] = "DEALING";
										$array_l["3"] = "LISTING";
                                        $array_l["4"] = "PROMOTION";
										//$array_l["7"] = "PROMOTION OFFLINE";
										$array_l["5"] = "CALL IN";
										$array_l["6"] = "SHOWING";
                                        $data = $array_l;
                                        echo form_dropdown('kategori', $data, '', 'id="kategori" class="form-control" style="width:100%;margin-top:5px"');
                                        ?>
									   
									   <br>
											<button class='btn-primary btn btn-block'><i class='fa fa-search'></i> Cari</button>
									
								 
									</div>		
								</div>
	
	
<!--------------------------->

</div>
</div>
<?php
  $useragent=$_SERVER['HTTP_USER_AGENT'];
           
                       if(preg_match('/(android|bb\d+|meego).+mobile|Android|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
                    {
                       ?>

<span style="margin-left:10px" class="mbl">
			<label>Filter :<input onkeyup="getPA()" name="area" style="width:140%" class="form-control" placeholder=""  type="search"></label>
		 </span>
<?php }  ;?>
</div>
<!--<button class="btn-danger pull-right" onclick="addPA()"> <i class="fa fa-plus-circle"></i> Add</button>-->
			<div id="listing"></div>
	   </div>
     </div>
 <!--  </div>
 </div> -->
 
 <?php echo $this->load->view("js/tabel.phtml");?>

  <script>
  getTarget();
	function getTarget()
	{	
	$("#listing").html("<img src='<?php echo base_url();?>plug/img/load.gif'> Please wait...");
		$.ajax({
		url:"<?php echo base_url();?>target/getTarget",
		type: "POST",
		data: $('#formFilter').serialize(),
		success: function(data)
				{	
				$("#listing").html(data);
				},
		});	
	}
</script>
  
  
   
   <!-- Bootstrap modal Add Data-->
  <div class="modal fade" id="modalTarget" role="dialog">
  <div class="modal-dialog">
 
<div class="modal-content">
     
      <div class="modal-body form">
   
<section class="content">
  <div class="row">
    <div class="col-lg-12">
	
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-plus-circle"></i><span class='title'> Add Data Form</span></h4>
      </div>
	<br>
<div id="kontenTarget">
</div>


    </div>
  </div>   <!-- /.row -->

</section><!-- /.content -->
  </div>
   </div><!-- /.modal-content -->
		

      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal Add Data-->  
  
     <!-- Bootstrap modal Edit Data-->
  <div class="modal fade" id="modalEditTarget" role="dialog">
  <div class="modal-dialog">
 
<div class="modal-content">
     
      <div class="modal-body form">
   
<section class="content">
  <div class="row">
    <div class="col-lg-12">
	
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-plus-circle"></i><span class='title'> Edit Form</span></h4>
      </div>
	<br>
<div id="kontenEditTarget">
</div>


    </div>
  </div>   <!-- /.row -->

</section><!-- /.content -->
  </div>
   </div><!-- /.modal-content -->
		

      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal Edit Data-->  


<script>
	  var f=jQuery.noConflict();

	   function edittarget(id)
{  
	f("#modalEditTarget").modal("show");
	  f('#kontenEditTarget').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Mohon Tunggu ... ");
	            f.ajax({
                type: "POST",
                dataType: "html",
                url: "<?php echo base_url() ?>target/getEditTarget/"+id,
                success: function (data) {
		              f("#kontenEditTarget").html(data);
                }
            });
}

function addTarget(id)
{  
	f("#modalTarget").modal("show");
	  f('#kontenTarget').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Mohon Tunggu ... ");
	            f.ajax({
                type: "POST",
                dataType: "html",
                url: "<?php echo base_url() ?>target/getTargetAdd/"+id,
                success: function (data) {
		              f("#kontenTarget").html(data);
                }
            });
}

   function closemodal(data)
{
	f("#"+data).modal("hide");
}
</script>	