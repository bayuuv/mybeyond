<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Target extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->m_konfig->validasi_session(array("user","promotion"));
		$this->load->model("M_target","target");
	}
	
	function _template($data)
	{
	$this->load->view('template/main',$data);	
	}
	
	public function index()
	{

	$data['konten']="index";
	$this->_template($data);
	}
	function getTarget()
	{
	$agen=$this->input->post("agen");
	$bulan=$this->input->post("bulan");
	$tahun=$this->input->post("tahun");
	$kategori=$this->input->post("kategori");
	$filter['filter']="?agen=$agen&bulan=$bulan&tahun=$tahun&kategori=$kategori";
	echo	$this->load->view("getTarget",$filter);
	}
	function getTargetAdd($id)
	{	$data["id"]=$id;
		$this->load->view("FormTarget",$data);
	}
	function ajax_target()
	{
		$list = $this->target->get_dataTarget();
        $data = array();
        $no = $_POST['start']+1;
        foreach ($list as $val) {
			if($val->kategori == '1'){
				$kategori_promo = "Goal";
			}elseif($val->kategori == '2'){
				$kategori_promo = "Dealing";
			}elseif($val->kategori == '3'){
				$kategori_promo = "Listing";
			}elseif($val->kategori == '4'){
				$kategori_promo = "Promotion";
			//}elseif($val->kategori == '7'){
				//$kategori_promo = "Promotion Offline";
			}elseif($val->kategori == '5'){
				$kategori_promo = "Call In";
			}else{
				$kategori_promo = "Showing";
			}
			
			$blnthn = explode("-",$val->bulan);
			
			if($blnthn[1] == '1'){
				$bln = "Januari ".$blnthn[0]."";
			}elseif($blnthn[1] == '2'){
				$bln = "Februari ".$blnthn[0]."";
			}elseif($blnthn[1] == '3'){
				$bln = "Maret ".$blnthn[0]."";
			}elseif($blnthn[1] == '4'){
				$bln = "April ".$blnthn[0]."";
			}elseif($blnthn[1] == '5'){
				$bln = "Mei ".$blnthn[0]."";
			}elseif($blnthn[1] == '6'){
				$bln = "Juni ".$blnthn[0]."";
			}elseif($blnthn[1] == '7'){
				$bln = "Juli ".$blnthn[0]."";
			}elseif($blnthn[1] == '8'){
				$bln = "Agustus ".$blnthn[0]."";
			}elseif($blnthn[1] == '9'){
				$bln = "September ".$blnthn[0]."";
			}elseif($blnthn[1] == '10'){
				$bln = "Oktober ".$blnthn[0]."";
			}elseif($blnthn[1] == '11'){
				$bln = "Nopember ".$blnthn[0]."";
			}elseif($blnthn[1] == '12'){
				$bln = "Desember ".$blnthn[0]."";
			}elseif($blnthn[1] == '00'){
				$bln = "XXX ".$blnthn[0]."";
			}
			
			$row = array();
			$row[] =  '<input type="checkbox" class="pilih" onclick="pilcek()" name="hapus[]" value="'.$val->id_target.'">';
			//$row[] = $no++;
            $row[] = $bln;
			$row[] = $val->nama;
			$row[] = $kategori_promo;
			$row[] = number_format($val->jumlah,0,",",".");
            $row[] = '
			<a href="#" style="font-size:14px" onclick="edittarget(`' . $val->id_target .'`)" class=" "><i class="fa fa-edit "> </i> Edit </a>
			| <a href="#" style="font-size:14px" onclick="hapuss(`' . $val->id_target . '`);" class=" "><i class="fa fa-trash"> </i> Hapus </a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->target->counts(),
            "recordsFiltered" => $this->target->counts(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	function insert()
	{
		echo $this->target->insert();
	}
	function update()
	{
		echo $this->target->update();
	}
	function HapusAll()
	{
	echo $this->target->HapusAll();
	}
	function hapus($id)
	{
	echo $this->target->hapus($id);
	}
	
	function export()
	{
		$this->target->export();
	}
	function getEditTarget($id)
	{	
		$data["id"]=$id;
		$this->load->view("FormEditTarget",$data);
	}
	
}