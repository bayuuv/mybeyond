<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_login extends ci_Model
{
	
	public function __construct() {
        parent::__construct();
     	}
	
	function captcha($captcha)
	{
	$this->session->set_userdata(array("captcha"=>$captcha));
	}
	private function cekCaptcha($no)
	{
		$sescap=$this->session->userdata("captcha");
		if($sescap==$no){
		  return 1; 	}else
		{ return 1; 	}
	}
	function getKodeAgen($id)
	{
		$this->db->where("id_agen",$id);
	$data=	$this->db->get("data_agen")->row();
	return isset($data->kode_agen)?($data->kode_agen):"admin";
	}
	private function saveSession($id,$level,$pass)
	{
	$array=array(
	"id"=>$id,
	"level"=>$level,
	"kode"=>$this->getKodeAgen($id),
	"pass"=>$pass,
	);
	$this->session->set_userdata($array);
	}
	function getDataLevel($id)//id_level
	{
	$this->db->where("id_level",$id);
	$data=$this->db->get("main_level")->row();
	return $data->nama;
	}
	function cekLogin()
	{	$no=$this->input->post('captcha');
		$cekcap=$this->cekCaptcha($no);
		$this->db->where("username",$this->input->post('username'));
		$this->db->where("password",md5($this->input->post('password')));
		$login=$this->db->get("admin");
		
		$this->db->where("username",$this->input->post('username'));
		$this->db->where("password",$this->input->post('password'));
		$opr=$this->db->get("data_agen");		
		if($login->num_rows()==1)
		{
			$login=$login->row();
			if($cekcap){ $this->saveSession($login->id_admin,$this->getDataLevel($login->level),$this->input->post('password'));  /*success*/ };
			if($cekcap){ return "1_".$this->getDataLevel($login->level);  /*success*/ }else{  return "2_captcha";/*captcha salah*/ };
		}elseif($opr->num_rows()==1)
		{
			
			$opr=$opr->row();
			if($opr->jabatan=="1" or $opr->jabatan=="11" or $opr->jabatan=="12")
			{
			$this->saveSession($opr->id_agen,"agen",$this->input->post('password')); 
				//last login
				$array=array(
				"last_login"=> date('Y-m-d H:i:s'),
				);
				$this->db->where("id_agen",$opr->id_agen);
				$this->db->update("data_agen",$array);
			return "1_agen"; 
			}elseif($opr->jabatan=="2")
			{
				$this->saveSession($opr->id_agen,"designer",$this->input->post('password')); 
			return "1_designer"; 
			}elseif($opr->jabatan=="5")
			{
				$this->saveSession($opr->id_agen,"dm",$this->input->post('password')); 
			return "1_dm"; 
			}elseif($opr->jabatan=="9"){
				$this->saveSession($opr->id_agen,"promotion",$this->input->post('password'));
				//last login
				$array=array(
				"last_login"=> date('Y-m-d H:i:s'),
				);
				$this->db->where("id_agen",$opr->id_agen);
				$this->db->update("data_agen",$array);
			return "1_promotion"; 
			}elseif($opr->jabatan=="100")
			{
				$this->saveSession($opr->id_agen,"network",$this->input->post('password')); 
				//last login
				$array=array(
				"last_login"=> date('Y-m-d H:i:s'),
				);
				$this->db->where("id_agen",$opr->id_agen);
				$this->db->update("data_agen",$array);
			return "1_network"; 
			}
		//	if($cekcap){ return "1_".$this->getDataLevel($login->level);  /*success*/ }else{  return "2_captcha";/*captcha salah*/ };
		}else
		{
			return "0_user/pass";//username/password salah
		}
	}
}

?>