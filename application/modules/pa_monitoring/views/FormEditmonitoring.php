<?php
$this->db->where("id_pa",$id);
$db=$this->db->get("tr_pa")->row();
?>
<form action="javascript:saveEdit()"  id="formMonitoring" class="form-horizontal black" method="post"  enctype="multipart/form-data"  >
<input type="hidden" name="id_pa" id="id_pa" value="<?php echo $id;?>">
<input type="hidden" name="kategori" id="kategori" value="<?php echo $db->kategori;?>">
<div class="form-group">
<label for="tgl_promo" class="b col-lg-3 control-label">Date</label>
<div class="col-lg-8">
<input type="text" class="form-control" id="tgl_promo"  name="tgl_promo" value="<?php echo $this->tanggal->ind($db->tgl_promo,"/");?>" >
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<label for="jenis" class="b col-lg-3 control-label">Agen</label>
<div class="col-lg-8">
<?php
    if($this->session->userdata("id")==64){
		$ref_agen = $this->reff->getAgenAjeng();
	}elseif($this->session->userdata("id")==151){
		$ref_agen = $this->reff->getAgenRhafa();
	}elseif($this->session->userdata("id")==133){
		$ref_agen = $this->reff->getAgenKiki();
	}elseif($this->session->userdata("id")==146){
		$ref_agen = $this->reff->getAgenYudi();
	}elseif($this->session->userdata("id")==152){
		$ref_agen = $this->reff->getAgenVivi();
	}elseif($this->session->userdata("id")==161){
		$ref_agen = $this->reff->getAgenYema();
	}elseif($this->session->userdata("id")==162){
		$ref_agen = $this->reff->getAgenFrans();
	}else{
		$ref_agen = $this->reff->getAgen();
    }
	$array_agen[""] = "==== choose ====";
    foreach ($ref_agen as $val) {
    $array_agen[$val->kode_agen] = $val->nama;
    }
    $data = $array_agen;
    echo form_dropdown('agenx', $data, $db->agen, ' id="agenz" class="select2-container" style="width:100%"');
?>
 <span class="help-block err_agen"></span>
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<label for="alamat_promo" class="b col-lg-3 control-label">Buyer</label>
<div class="col-lg-8">
<input type="text" class="form-control" id="buyer"  name="buyer" value="<?php echo $db->buyer ;?>" >
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<label for="alamat_promo" class="b col-lg-3 control-label">Budget</label>
<div class="col-lg-8">
<input type="text" class="form-control" id="budget"  name="budget" onkeydown="return numbersonly(this, event);" value="<?php echo $db->budget ;?>" >
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<label for="alamat_promo" class="b col-lg-3 control-label">Location</label>
<div class="col-lg-8">
<input type="text" class="form-control" id="location"  name="location" value="<?php echo $db->location ;?>" >
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<label for="nama" class="b col-lg-3 control-label">Image</label>
<div class="col-lg-8">
<input type="file" class="form-control" id="foto" name="foto"  >
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<label for="alamat_promo" class="b col-lg-3 control-label">Result</label>
<div class="col-lg-8">
<input type="text" class="form-control" id="result"  name="result" value="<?php echo $db->result ;?>" >
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<!--<label for="jenis" class="b col-lg-3 control-label">Kategori</label>
<div class="col-lg-8">
<?php                                        
    $arrayS[""] = "==== Pilih Kategori ====";
	$arrayS["1"] = "Call In";
	$arrayS["2"] = "Showing";
    $data = $arrayS;
    echo form_dropdown('kategori', $data, $db->kategori, '  id="kategori"  class="form-control"');
?>
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<label for="alamat_promo" class="b col-lg-3 control-label">Jumlah Call In </label>
<div class="col-lg-8">
<input type="text" class="form-control" id="jumlah"  name="jumlah" value="<?php echo $db->jumlah ;?>" >
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>-->
<input type="hidden" class="form-control" id="fotox"  name="fotox" value="<?php echo $db->foto ;?>" >
<input type="hidden" class="form-control" id="jumlah"  name="jumlah" value="<?php echo $db->jumlah ;?>" >
<div class="col-lg-offset-2 col-lg-9">
<span class='load'></span>
<button type="submit" class="btn btn-success pull-right" onclick="saveEdit()"><i class='fa fa-save'></i> Save</button>
</div>
</div>
</form>


<?php echo $this->load->view("js/form.phtml"); ?>
<script>
function saveEdit()
	{	
		var url="<?php echo base_url();?>pa_monitoring/update";
		$(".load").html("<img src='<?php echo base_url();?>plug/img/load.gif'> Please wait...");
		$("#formMonitoring").ajaxForm({
		url:url,
		type: "post",
		data: $('#formMonitoring').serialize(),
	//	dataType: "JSON",
		success: function(data)
				{
						  closemodal("modalEditmonitoring");
						  table.ajax.reload(null,false); //reload datatable ajax 
				},
				
		});
	}
</script>

<script src="<?php echo base_url();?>plug/boostrap/js/jquery.maskedinput.min.js"></script>  
<script>
$("#tgl_promo").mask("99/99/9999");
</script>
<script src="<?php echo base_url() ?>plug/boostrap/js/select2.min.js"></script>
 <script>
  $('#agen').select2();
  $('#agenz').select2();
</script>