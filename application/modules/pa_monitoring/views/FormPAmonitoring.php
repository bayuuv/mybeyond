<form action="javascript:saveAddPromooff()"  id="formMonitoring" class="form-horizontal black" method="post"  enctype="multipart/form-data"  >
<div class="form-group">
<label for="tgl_promo" class="b col-lg-3 control-label">Date</label>
<div class="col-lg-8">
<input type="text" class="form-control" id="tgl_promo"  name="tgl_promo" value="" >  
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<label for="jenis" class="b col-lg-3 control-label">Agen</label>
<div class="col-lg-8">
<?php
    if($this->session->userdata("id")==64){
		$ref_agen = $this->reff->getAgenAjeng();
	}elseif($this->session->userdata("id")==151){
		$ref_agen = $this->reff->getAgenRhafa();
	}elseif($this->session->userdata("id")==133){
		$ref_agen = $this->reff->getAgenKiki();
	}elseif($this->session->userdata("id")==146){
		$ref_agen = $this->reff->getAgenYudi();
	}elseif($this->session->userdata("id")==152){
		$ref_agen = $this->reff->getAgenVivi();
	}elseif($this->session->userdata("id")==161){
		$ref_agen = $this->reff->getAgenYema();
	}elseif($this->session->userdata("id")==162){
		$ref_agen = $this->reff->getAgenFrans();
	}else{
		$ref_agen = $this->reff->getAgen();
    }
	$array_agen[""] = "==== choose ====";
    foreach ($ref_agen as $val) {
    $array_agen[$val->kode_agen] = $val->nama;
    }
    $data = $array_agen;
    echo form_dropdown('agenx', $data, '', ' id="agenx" class="select2-container" style="width:100%"');
?>
 <span class="help-block err_agen"></span>
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<label for="alamat_promo" class="b col-lg-3 control-label">Buyer</label>
<div class="col-lg-8">
<input type="text" class="form-control" id="buyer"  name="buyer" value="" >
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<label for="alamat_promo" class="b col-lg-3 control-label">Budget</label>
<div class="col-lg-8">
<input type="text" class="form-control" id="budget"  name="budget" onkeydown="return numbersonly(this, event);" value="" >
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<label for="alamat_promo" class="b col-lg-3 control-label">Location</label>
<div class="col-lg-8">
<input type="text" class="form-control" id="location"  name="location" value="" >
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<label for="nama" class="b col-lg-3 control-label">Image</label>
<div class="col-lg-8">
<input type="file" class="form-control" id="foto" name="foto" required />
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<label for="alamat_promo" class="b col-lg-3 control-label">Result</label>
<div class="col-lg-8">
<input type="text" class="form-control" id="result"  name="result" value="" >
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>

<!--<label for="jenis" class="b col-lg-3 control-label">Kategori</label>
<div class="col-lg-8">
<?php                                        
    $arrayS[""] = "==== Pilih Kategori ====";
	$arrayS["1"] = "Call In";
	$arrayS["2"] = "Showing";
    $data = $arrayS;
    echo form_dropdown('kategori', $data, '', '  id="kategori"  class="form-control"');
?>
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>
<label for="alamat_promo" class="b col-lg-3 control-label">Jumlah Call In</label>
<div class="col-lg-8">
<input type="text" class="form-control" id="jumlah"  name="jumlah" value="" >
</div>
<div class="cleafix col-md-12 col-sx-12" style="height:5px">&nbsp;</div>-->
<input type="hidden" class="form-control" id="kategori"  name="kategori" value="1" > 
<input type="hidden" class="form-control" id="jumlah"  name="jumlah" value="1" > 
<div class="col-lg-offset-2 col-lg-9">
<span class='load'></span>
<button type="submit" class="btn btn-success pull-right" onclick="saveAddMonitoring()"><i class='fa fa-save'></i> Save</button>
</div>
</div>
</form>


<?php echo $this->load->view("js/form.phtml"); ?>
<script>
function saveAddMonitoring()
	{	
		var url="<?php echo base_url();?>pa_monitoring/insert";
		$(".load").html("<img src='<?php echo base_url();?>plug/img/load.gif'> Please wait...");
		$("#formMonitoring").ajaxForm({
		url:url,
		type: "post",
		data: $('#formMonitoring').serialize(),
	//	dataType: "JSON",
		success: function(data)
				{
						  if(data==false){ alert("Gagal! Data Sudah Ada"); $(".load").html(""); $("[name='kategori']").focus(); return false;}
						  closemodal("modalPAmonitoring");
						  table.ajax.reload(null,false); //reload datatable ajax 
				},
				
		});
	}
</script>

<script src="<?php echo base_url();?>plug/boostrap/js/jquery.maskedinput.min.js"></script>  
<script>
$("#tgl_promo").mask("99/99/9999");
</script>
<script src="<?php echo base_url() ?>plug/boostrap/js/select2.min.js"></script>
 <script>
  $('#agen').select2();
  $('#agenx').select2();
</script>