<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pa_monitoring extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->m_konfig->validasi_session(array("user","promotion"));
		$this->load->model("M_pamonitoring","pamonitoring");
	}
	
	function _template($data)
	{
	$this->load->view('template/main',$data);	
	}
	
	public function index()
	{

	$data['konten']="index";
	$this->_template($data);
	}
	function getPA()
	{
	$agen=$this->input->post("agen");
	$bulan_sell=$this->input->post("bulan_sell");
	$tahun_sell=$this->input->post("tahun_sell");
	//$kategori=$this->input->post("kategori");
	$filter['filter']="?agen=$agen&bulan_sell=$bulan_sell&tahun_sell=$tahun_sell";
	echo	$this->load->view("getPA",$filter);
	}
	function getPAmonitoring($id)
	{	$data["id"]=$id;
		$this->load->view("FormPAmonitoring",$data);
	}
	function getEditmonitoring($id)
	{	$data["id"]=$id;
		$this->load->view("FormEditmonitoring",$data);
	}
	function ajax_pamonitoring()
	{
		$list = $this->pamonitoring->get_dataPA();
        $data = array();
        $no = $_POST['start']+1;
        foreach ($list as $val) {
			/*if($val->kategori == '1'){
				$kategori_promo = "Call In";
			}elseif($val->kategori == '2'){
				$kategori_promo = "Showing";
			}*/
			
			$row = array();
			$row[] =  '<input type="checkbox" class="pilih" onclick="pilcek()" name="hapus[]" value="'.$val->id_pa.'">';
			//$row[] = $no++;
            $row[] = $this->tanggal->ind($val->tgl_promo,"/");
			$row[] = $val->nama;
			$row[] = $val->buyer;
			$row[] = number_format($val->budget,0,",",".");
			$row[] = $val->location;
			$row[] = '<img src="'.base_url().'file_upload/img/'.$val->foto.'" width="200px" height="200px" >';
			$row[] = $val->result;
			//$row[] = $kategori_promo;
			//$row[] = $val->jumlah;
            $row[] = '
			<a href="#" style="font-size:14px" onclick="editpromo(`' . $val->id_pa .'`)" class=" "><i class="fa fa-edit "> </i> Edit </a>
			| <a href="#" style="font-size:14px" onclick="hapuss(`' . $val->id_pa . '`);" class=" "><i class="fa fa-trash"> </i> Hapus </a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->pamonitoring->counts(),
            "recordsFiltered" => $this->pamonitoring->counts(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	function insert()
	{
		echo $this->pamonitoring->insert();
	}
	function update()
	{
		echo $this->pamonitoring->update();
	}
	function HapusAll()
	{
	echo $this->pamonitoring->HapusAll();
	}
	function hapus($id)
	{
	echo $this->pamonitoring->hapus($id);
	}
	
	function export()
	{
		$this->offpromo->export();
	}
	function getEditpromo($id)
	{	
		$data["id"]=$id;
		$this->load->view("formEditpromo",$data);
	}
	
}