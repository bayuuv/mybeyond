 <style>
  .table-responsive img {
      max-width: 30%;
      padding: 5px;
      border: 1px solid #ccc;
      height: auto;
      background: #fff;
      box-shadow: 1px 1px 7px rgba(0,0,0,0.1);
    }
	</style>
 <div class="main-box clearfix" >
			<div class="main-box-body clearfix ">
			<div class="table-responsive">
			
			<table id='table' class="table user-bordered " width="100%" border='1'>
						<thead style="font-size:13px">			
							
							<th style="text-align:center;color:black">POTO </th>
							<th style="text-align:center;color:black" >UPLOAD</th>
						</thead>
			</table>
		</div>
	   </div>
     </div>
	 
	 <?php echo $this->load->view("js/tabel.phtml");?>
	 
  <script>
   
		var table;
		table = $('#table').DataTable({ 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('designer/ajax_property/'.$filter.'')?>",
            "type": "POST",
        },
		
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
		    "targets": [ 0], //last column
          "orderable": false, //set not orderable
        },
        ],

      });

	  function hapus(id)
	  {
	    var tanya=window.confirm("Hapus ?");
		if(tanya==false){ return false;};
	  	$.ajax({
		url:"<?php echo base_url();?>sms/outbox_hapus",
		type: "POST",
		data:"id="+id,
		success: function(data)
				{
				table.ajax.reload(null,false); //reload datatable ajax 
				},
			});
	  }
	  var method;
	  function add()
	  {
				method="add";
				$("#modalAdd").modal("show");
				$(".title").html(" Tambah Data Agen");
	  }
	 function gethistory(id)
	 {
			 $('.history').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Mohon Tunggu ... ");
	            $.ajax({
                type: "POST",
                dataType: "html",
                url: "<?php echo base_url();?>data_costumer/getHistory/",
                data: "id=" + id,
                success: function (data) {
			       $(".history").html(data);
                }
            });
	 }
	  
  </script>
  
  
  
  
  
 