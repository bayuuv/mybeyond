<script>
	function getArea()
	{
		var area=$("[name='area']").val();
		if(!area)
		{
			$("[name='lat_area']").val("");
			$("[name='long_area']").val("");
		}
	}
</script>

<script>
function direct()
{
	window.location.href="<?php echo base_url()?>listing_ag/add";
}
</script>



<div class="row">
    <div class="col-lg-12">

<div class="panel-group accordion" id="accordion">

<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
<i class="fa fa-filter"></i> Filter Pencarian
</a>
</h4>
</div>
<div id="collapseThree" class="panel-collapse collapse">
<div class="panel-body">
<!--------------------------->
<form id="form" action="javascript:getListing()">
  <div class="form-group fg_prov col-md-12" > 
                                        <label class="control-label black b" ></label>
                                        <?php
                                        $ref_type = $this->reff->getProvinsi();
                                        $array_prov[""] = "==== Pilih Provinsi ====";
                                        foreach ($ref_type as $val) {
                                            $array_prov[$val->id] = '[' . $val->id . ']' . ' &nbsp;' . $val->provinsi;
                                        }
                                        $data = $array_prov;
                                        echo form_dropdown('provinsi', $data, '', 'onchange="return cek_prov()"   id="provinsi" class="select2-container" style="width:100%"');
                                        ?>
                                     
                                  
                                 
                                        <label class="control-label black b" ></label>
                                        <span class="dataKab">
										<?php
                                          $kab[""]="==== Pilih ====";
										  $data=$this->reff->getKab(32);
										 foreach ($data as $val) {
												 $kab[$val->id] = '['.$val->id.']'.' &nbsp;'.$val->kabupaten;
												 }
												 $dataKab = $kab;
												echo form_dropdown('kabupaten', $dataKab, "", ' id="kabupaten" class="select2-container" style="width:100%" onchange="return cek_kab()"');     
										echo "<script>	$('#kabupaten').select2();</script>"; ?>
                                        </span>
										
							        	 
									  
                              
									<label for="kelengkapan"></label>
									 <?php
                                        $array_l[""] = "==== Kelengkapan Photo ====";
										$array_l["1"] = "Listing terdapat photo";
                                        $array_l["2"] = "Listing tidak Terdapat photo";
                                        $array_l["3"] = "Listing terdapat fetchsheet";
                                        $array_l["4"] = "Listing tidak terdapat fetchsheet";
                                        
                                        $data = $array_l;
                                        echo form_dropdown('kelengkapan', $data, 4, 'id="kelengkapan" class="form-control" style="width:100%;margin-top:0px"');
                                        ?>
									

								
									   
									   
											<button class='btn-primary btn btn-block' style="margin-top:20px"><i class='fa fa-search'></i> Cari</button>
									
								
											
    </div>	
	
		
	 
						
	
	</form>
<!--------------------------->

</div>
</div>
</div>

<br>

	 
	 <div id="listing"></div>
	 
	 
   </div>
   </div>
   </div>
 

 <?php echo $this->load->view("js/tabel.phtml");?>
 <script>
 getListing();
 function getListing()
	{	
	$("#listing").html("<img src='<?php echo base_url();?>plug/img/load.gif'> Please wait...");
		$.ajax({
		url:"<?php echo base_url();?>designer/getListing",
		type: "POST",
		data: $('#form').serialize(),
		success: function(data)
				{	
				$("#listing").html(data);
				},
		});	
	}

 </script>
 
 <!-- Bootstrap modal -->
  <div class="modal fade" id="modalDetail" role="dialog">
  <div class="modal-dialog" >
<div class="modal-content" >
	<div class="modal-header"><i class="fa fa-home"></i> Data Listing
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
&nbsp;
      </div>
      <div class="modal-body form">
<section class="content">
  <div class="row">
    <div class="col-lg-12 dataDetail">
	
		<!------------------------->
		<!------------------------->
    </div>
  </div>   <!-- /.row -->
</section><!-- /.content -->
  </div>
   </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
  



 <script>
  function saveAdd()
	{	
	if(method=="edit")
	{
		var url="<?php echo base_url();?>listing_ag/update";
	}else{
		var url="<?php echo base_url();?>listing_ag/insert";
	}
		$(".load").html("<img src='<?php echo base_url();?>plug/img/load.gif'> Please wait...");
		$.ajax({
		url:url,
		type: "POST",
		data: $('#formAgen').serialize(),
	//	dataType: "JSON",
		success: function(data)
				{
				if(data==false){ alert("Gagal! Agen sudah ada pada database"); $(".load").html(""); $("[name='group']").focus(); return false;}
				$(".load").html('<font color="green"><i class="fa fa-check-circle fa-fw fa-lg"></i> Berhasil di simpan</font>');
				table.ajax.reload(null,false);
				$("#formAgen")[0].reset();
				//$(".load").html("");
				if(method=="edit")
				{
					$("#modalAdd").modal("hide");
					$(".load").html('');
				}
				},
				
		});
	}
	
	 function hapus(id)
	  {
	    var tanya=window.confirm("Hapus ?");
		if(tanya==false){ return false;};
	  	$.ajax({
		url:"<?php echo base_url();?>listing_ag/hapus/"+id,
		type: "POST",
		data:"id="+id,
		success: function(data)
				{
				table.ajax.reload(null,false); //reload datatable ajax 
				},
			});
	  }

</script>	



<script>
function exportData()
{
	window.location.href="<?php echo base_url()?>listing_ag/export";
}

</script>





  
  


<script src="<?php echo base_url() ?>plug/boostrap/js/select2.min.js"></script>
 <script>
                                   
                                      //nice select boxes
                                      $('#sertifikat').select2();
                                      $('#provinsi').select2();
                                      $('#kabupaten').select2();
                                      $('#hadap').select2();
                                      //$('#sewa').select2();
                                      $('#agen').select2();
                                      $('#owner').select2();
    </script>
 <script>
        $("#provinsi").change(function () {
            var prov = $("#provinsi").val();
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "<?php echo base_url() ?>listing_ag/ajaxGetKab2",
                data: "prov=" + prov,
                success: function (data) {
                    $(".dataKab").html(data);
                }
            });

        });

	function loadkab(){
            var prov = "32";
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "<?php echo base_url() ?>listing_ag/ajaxGetKab",
                data: "def=3273&prov=" + prov,
                success: function (data) {
                    $(".dataKab").html(data);
                }
            });

        };
    </script>
	 <script>
        $('input.typeahead').typeahead({
            source: function (query, process) {
                var kab = $("#kabupaten").val();
                return $.get('<?php echo base_url() ?>listing_ag/getKomplek/' + kab, {query: query}, function (data) {
                    console.log(data);
                    data = $.parseJSON(data);
                    return process(data);
                });
            }
        });
    </script>
	
	
	
  
 

	
 <?php echo $this->load->view("js/form.phtml"); ?>
	
	<script>

	var f=jQuery.noConflict();
function godetail(id)
{
			f("#modalDetail").modal("show");
	  f('.dataDetail').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Mohon Tunggu ... ");
	            f.ajax({
                type: "POST",
                dataType: "html",
                url: "<?php echo base_url() ?>designer/getDataDetail",
                data: "id=" + id,
                success: function (data) {
			
                    f(".dataDetail").html(data);
                }
            });
	return false;
}

	</script>
	
  <script type="text/javascript">
function goUpload(id,kode){
	
    var userfile=$('[name="formUpload'+id+'"]').val();
	   f('#formUpload'+id).ajaxForm({
		
     url:'<?php echo base_url();?>designer/uploadFile/'+id,
     type: 'post',
     data:{"formUpload":userfile,"kode":kode},
     beforeSend: function() {
        var percentVal = 'Mengupload 0%';
        f('.load'+id).html(percentVal);
     },
     uploadProgress: function(event, position, total, percentComplete) {
        var percentVal = 'Mengupload ' + percentComplete + '%';
        f('.load'+id).html(percentVal);
     },
     beforeSubmit: function() {
      f('.load'+id).html("<img src='<?php echo base_url();?>plug/img/load.gif'> Silahkan Tunggu ... ");
     },
     complete: function(xhr) {
        f('.load'+id).html('');
     }, 
     success: function(resp) {
        f('.load'+id).html(resp);
		table.ajax.reload(null,false);
		f("#formUpload"+id)[0].reset();
     },
    });     
};
</script>   



 
<script>

function history(id,kode_listing)
{
	f("#modalListingReport").modal("show");
	 f('.dataDetails').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Mohon Tunggu ... ");
	           f.ajax({
                type: "POST",
                dataType: "html",
                url: "<?php echo base_url();?>listing_ag/goReportListing/"+id+"/"+kode_listing,
                success: function (data) {
		        f(".dataDetails").html(data);
                }
            });
			return false;
}	
</script>


 <!-- Bootstrap modal -->
  <div class="modal fade" id="modalListingReport" role="dialog">
  <div class="modal-dialog">
<div class="modal-content" >
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-info-circle"></i> Report Listing<span class='namaGroup'></span></h4>
      </div>
      <div class="modal-body form">
<section class="content">
  <div class="row">
    <div class="col-lg-12 dataDetails">
		<!------------------------->
	
		<!------------------------->
		
    </div>
	<div class="conversation-new-message">
<form action="javascript:simpan()" id="formL" >
<div class="form-group">
 <?php
                                        $ref_sert = $this->reff->getTitleListing();
                                        $array_sert[""] = "==== Pilih Katagori Report ====";
                                        foreach ($ref_sert as $val) {
                                            $array_sert[$val->id] = $val->nama;
                                        }
                                        $data = $array_sert;
                                        echo form_dropdown('titleListing', $data, '', 'required id="titleListing" class="form-control" style="width:100%"');
                                        ?>
<textarea class="form-control" rows="2" name="textListing" id="textListing" placeholder="Enter your repport..." required></textarea>
</div>
<div class="clearfix">
<span class="msg pull-left"></span>
<button type="submit"    class="btn btn-success pull-right">Kirim Report</button>
</div>
</form>
</div>
  </div>   <!-- /.row -->
</section><!-- /.content -->
  </div>
   </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
  
