<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Designer extends CI_Controller {

	

	function __construct()
	{
		parent::__construct();	
		$this->m_konfig->validasi_session(array("designer"));
		$this->load->model("M_designer","agen");
	}
	
	function _template($data)
	{
	$this->load->view('template/main',$data);	
	}
	
	public function index()
	{

	$this->listing();
	}
	public function listing()
	{

	$data['konten']="listing";
	$this->_template($data);
	}
	public function uploadFile($id)
	{
	echo $this->agen->uploadFile($id);
	}
	function ajaxGetKab()
	{
		$id=$this->input->post("prov");
		$def=$this->input->post("def");
		$data=$this->reff->getKab($id);
		if($data)
		{
			$kab[""]="==== Pilih ====";
		 foreach ($data as $val) {
				 $kab[$val->id] = '['.$val->id.']'.' &nbsp;'.$val->kabupaten;
                 }
                 $dataKab = $kab;
                echo form_dropdown('kabupaten', $dataKab, $def, ' id="kabupaten" class="select2-container" style="width:100%" onchange="return cek_kab()"');     
		echo "<script>	$('#kabupaten').select2();</script>";
		}else{
			
			echo form_dropdown('kabupaten', array(), '', ' id="kabupaten" class="select2-container" style="width:100%"');     
			echo "<script>	$('#kabupaten').select2();</script>";
		}
	}
	function ajaxGetKab2()
	{
		$id=$this->input->post("prov");
		$def=$this->input->post("def");
		$data=$this->reff->getKab($id);
		if($data)
		{
			$kab[""]="==== Pilih ====";
		 foreach ($data as $val) {
				 $kab[$val->id] = '['.$val->id.']'.' &nbsp;'.$val->kabupaten;
                 }
                 $dataKab = $kab;
                echo form_dropdown('kabupaten', $dataKab, $def, ' id="kabupaten" class="form-control" style="width:100%" onchange="return cek_kab()"');     
		echo "<script>	$('#kabupaten').select2();</script>";
		}else{
			
			echo form_dropdown('kabupaten', array(), '', ' id="kabupaten" class="select2-container" style="width:100%"');     
			echo "<script>	$('#kabupaten').select2();</script>";
		}
	}
	function getKomplek($id)
	{
		$data=$this->reff->getKomplek($id);
		$result="";
		foreach($data as $data)
		{
			$result[]=$data->komplek;
		}
		echo json_encode($result);
	}
	function infoFlash()
	{
		$msg='<script>alert("Success! Tersimpan")</script>';
		$this->session->set_flashdata("info",$msg);
	}
	function insert()
	{
			$this->infoFlash();
	echo	$this->agen->insert();
	}
	
	function ajax_property()
	{
		$list = $this->agen->get_dataproperty();
        $data = array();
        $no = $_POST['start']+1;
        foreach ($list as $val) {
		if($val->desain)
		{
			$fetcsheet="<a href='".base_url()."file_upload/img/".$val->desain."' download><img style='min-width:150px;max-width:150px' src='".base_url()."file_upload/img/".$val->desain."'></a> ";
		}else{
			$fetcsheet="";
		}		
			
			
			$row = array();
			$gambar=$val->gambar_utama;
			if(!$gambar)
			{$gambar="nopund.jpg";}else{
				$gambar=$val->$gambar;
			}
			
			$g="";
			if($val->gambar1)
			{
				$g.="<a href='".base_url()."file_upload/img/".$val->gambar1."' download><img  width='150px' style='padding:5px' src='".base_url()."file_upload/img/".$val->gambar1."'/></a>";
			}
			if($val->gambar2)
			{
				$g.="<a href='".base_url()."file_upload/img/".$val->gambar2."' download><img  width='150px' style='padding:5px' src='".base_url()."file_upload/img/".$val->gambar2."'/></a>";
			}
			if($val->gambar3)
			{
				$g.="<a href='".base_url()."file_upload/img/".$val->gambar3."' download><img  width='150px' style='padding:5px' src='".base_url()."file_upload/img/".$val->gambar3."'/></a>";
			}if($val->gambar4)
			{
				$g.="<a href='".base_url()."file_upload/img/".$val->gambar4."' download><img  width='150px' style='padding:5px' src='".base_url()."file_upload/img/".$val->gambar4."'/></a>";
			}
			if($val->gambar5)
			{
				$g.="<a href='".base_url()."file_upload/img/".$val->gambar5."' download><img  width='150px' style='padding:5px' src='".base_url()."file_upload/img/".$val->gambar5."'/></a>";
			}
			
			
			
			
			$row[] = '<div class="col-md-12  "><a href="#" onclick="godetail(`'.$val->id_prop.'`,`'.$val->kode_prop.'`)" 
			class="user-link" style="color:black;font-weight:bold;font-size:12px"><i class="fa fa-home"></i> '.$val->alamat_detail.'</a>
</div>
<div class="col-md-12  " >'.$g.'</div>


';
$gb="";
if($gb){
	$gb='<img style="max-width:250px" alt="gambar belum diupload" src="'.base_url().'file_upload/img/'.$val->desain.'">';
}
$row[] ='<center><form id="formUpload'.$val->id_prop.'" action="#"><div class="col-md-1" style="max-width:200px">	<div class="form-group">
'.$fetcsheet.'														
														<div class="col-xs-12">
																<label>
															'.$gb.'
																<input  type="file"  name="formUpload'.$val->id_prop.'" />
															</label>
																<button  onclick="goUpload(`'.$val->id_prop.'`,`'.$val->kode_prop.'`)" class="btn-block btn-primary" style="max-width:150px">Upload</button>
																<button  onclick="return history(`'.$val->id_prop.'`,`'.$val->kode_prop.'`)" class="  btn-danger" style="max-width:150px">History Report</button>
																<button  onclick="return godetail(`'.$val->id_prop.'`,`'.$val->kode_prop.'`)" class=" btn-success" style="max-width:150px">Detail</button>
															<center><span class="load'.$val->id_prop.'"></span></center></div>
														</div></div></form></center>';
			
			$data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->agen->counts(),
            "recordsFiltered" => $this->agen->counts(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	function getListing()
	{
	$provinsi=$this->input->post("provinsi");
	$kabupaten=$this->input->post("kabupaten");
	$area=$this->input->post("area");
	$lat_area=$this->input->post("lat_area");
	$long_area=$this->input->post("long_area");
	$jenis_pro=$this->input->post("jenis_pro");
	$type_pro=$this->input->post("type_pro");
	$kamar_tidur=$this->input->post("kamar_tidur");
	$kamar_mandi=$this->input->post("kamar_mandi");
	$garasi=$this->input->post("garasi");
	$daya_listrik=$this->input->post("daya_listrik");
	$harga_min=$this->input->post("harga_min");
	$harga_max=$this->input->post("harga_max");
	$sertifikat=$this->input->post("sertifikat");
	$agen=$this->input->post("agen");
	$type_sewa=$this->input->post("type_sewa");
	$kelengkapan=$this->input->post("kelengkapan");
	$status_penjualan=$this->input->post("status_penjualan");
	$filter['filter']="?type_pro=$type_pro&provinsi=$provinsi&kabupaten=$kabupaten&area=$area&lat_area=$lat_area&long_area=$long_area&jenis_pro=$jenis_pro&kamar_mandi=$kamar_mandi&kamar_tidur=$kamar_tidur&garasi=$garasi&daya_listrik=$daya_listrik&harga_min=$harga_min&harga_max=$harga_max&sertifikat=$sertifikat&agen=$agen&type_sewa=$type_sewa&kelengkapan=$kelengkapan&status_penjualan=$status_penjualan";
	echo	$this->load->view("getListing",$filter);
	}
	function getDataDetail()
	{
		$data['kode_prop']=$this->input->post("id");
	echo	$this->load->view("dataDetail",$data);
	}
	function goReportListing($id,$kode_listing)
	{
		$data["id"]=$id;
		$data["kode_listing"]=$kode_listing;
		$this->load->view("goReportListing",$data);
	}
	function delHistory($id)
	{
		echo $this->agen->delHistory($id);
	}
		function getHistory()
	{
		$id=$this->input->post("id");
		$this->db->where("kode_listing",$id);
		$this->db->order_by("tgl","ASC");
		$data=$this->db->get("report_listing")->result();
		foreach($data as $val)
		{
			?>
			 
			<div class="conversation-item item-right clearfix black">
			<div class="conversation-user" style='margin-top:20px;color:red'>
			&nbsp;&nbsp;&nbsp;<a href='javascript:hapusH(`<?php echo $val->id;?>`,`<?php echo $id ?>`)'><i class='fa fa-trash' style='margin-left:-5px'></i></a>
			</div>
			<div class="conversation-body">
			<div class="name">
			<?php echo $this->reff->getNamaTitleListing($val->id_title)?>
			<a class="hapusmobile pull-right" style='color:red' href='javascript:hapusH(`<?php echo $val->id;?>`,`<?php echo $id ?>`)'><i class='fa fa-trash' style='margin-left:-5px'></i></a>
			</div>
			<div class="time hidden-xs">
			<?php echo $this->tanggal->hariLengkapJam($val->tgl,"/")?>
			</div>
			<div class="text">
			<?php echo $val->ket;?>
			</div>
			</div>
			</div>
			<?php
		}
	}
		function saveReport()
	{
		echo $this->agen->saveReport();
	}
}