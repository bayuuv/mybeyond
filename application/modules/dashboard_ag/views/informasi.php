
<style>
.history {
    height: 350px;
    overflow: scroll;
}
.title,.item{
color:black;
font-size:13px;
line-height:15px;
}
.time-ago{
	font-size:13px;
}
</style>

<div class="row">
<div class="col-lg-4 col-sm-12 col-xs-12" style="color:white">
<!------------------------>
<div class="main-box feed ">
<header class="black sadow main-box-header clearfix">
<h2 class="pull-left">Timeline</h2>
</header>
<div class="main-box-body clearfix history">
<ul>
<!---------------------->
<span class="datahistory"> </span>
<!---------------------->
</ul>
</div>
</div>


<div class="conversation-new-message">
<form action="javascript:simpan()">
<div class="form-group">
<textarea class="form-control chat-input" rows="2" name="textListing" placeholder="Enter your message..."></textarea>
</div>
<div class="clearfix chat-send">
<button type="submit" class="btn btn-success pull-right">Send message</button>
</div>
</form>
</div>
<!------------------------>
</div>
 
<div class="col-lg-8 col-sm-12 col-xs-12" style="color:white">

<div class="row">
<div class="col-lg-6 col-sm-6 col-xs-12" style="color:white">
<div class="main-box infographic-box emerald-bg ">
<i class="fa fa-cubes purple-bg"></i>
<span class="headline">Asset</span>
<span class="value " style="font-size:26px">Rp <?php echo number_format($this->reff->jumlahAsetAgen(),0,",",".");?></span>
</div>
</div>

<div class="col-lg-6 col-sm-6 col-xs-12">
<div class="main-box infographic-box colored emerald-bg">
<i class="fa fa-list"></i>
<span class="headline">  Listing</span>
<span class="value" style="font-size:26px"><?php echo $this->reff->totalListingReadyAgen();?></span>
</div>
</div>
<div class="col-lg-6 col-sm-6 col-xs-12">
<div class="main-box infographic-box colored red-bg">
<i class="fa fa-bank"></i>
<span class="headline"> Sell</span>
<span class="value" style="font-size:26px"><?php echo $this->reff->dijualReadyAgen();?></span>
</div>
</div>
<div class="col-lg-6 col-sm-6 col-xs-12">
<div class="main-box infographic-box colored blue-bg">
<i class="fa fa-bank"></i>
<span class="headline"> Lease</span>
<span class="value" style="font-size:26px"><?php echo $this->reff->disewakanReadyAgen();?></span>
</div>
</div>

<div class="col-lg-6 col-sm-6 col-xs-12">
<div class="main-box infographic-box colored purple-bg">
<i class="fa fa-bank"></i>
<span class="headline">  Closing </span>
<span class="value" style="font-size:26px"><?php echo $this->reff->listingTerjualTersewaAgen();?></span>
</div>
</div>
<div class="col-lg-6 col-sm-6 col-xs-12">
<div class="main-box infographic-box colored purple-bg">
<i class="fa fa-bank"></i>
<span class="headline">  Cancel </span>
<span class="value" style="font-size:26px"><?php echo $this->reff->listingCancelAgen();?></span>
</div>
</div>
</div>
<?php     date_default_timezone_set('Asia/Jakarta');    $today = date("Y");?>
<div class="col-lg-6 col-sm-6 col-xs-12 black sadow5">
<div class="main-box infographic-box">
<i class="fa fa-user red-bg"></i>
<span class="headline">Omzet <?php echo $today ; ?></span>
<span class="value">
<span class="timer" data-from="30" data-to="658" data-speed="800" data-refresh-interval="30" style="font-size:26px">
<?php 
//$omzet_net = $this->reff->jumlahOmzetByAgen(2017) + $this->reff->jumlahOmzetByAgen2(2017);
?>
Rp <?php echo number_format($this->reff->jumlahOmzetByAgen($today),0,",",".");?>
</span>
</span>
</div>
</div>



<div class="col-lg-6 col-sm-6 col-xs-12 black sadow5">
<div class="main-box infographic-box">
<i class="fa fa-user emerald-bg"></i>
<span class="headline">Fee <?php echo $today ; ?></span>
<span class="value">
<span class="timer" data-from="30" data-to="658" data-speed="800" data-refresh-interval="30" style="font-size:26px">
<?php
$komisigross = $this->reff->totalKomisiAgen($this->session->userdata("kode"),$today) + $this->reff->totalKomisiAgen2($this->session->userdata("kode"),$today);
?>
Rp <?php echo number_format($komisigross,0,",",".");?>
</span>
</span>
</div>
</div>



<div class="row"> 
 

<div class="col-lg-6 col-sm-6 col-xs-12 black sadow5">
<div class="main-box infographic-box">
<i class="fa fa-user red-bg"></i>
<span class="headline">Buyer</span>
<span class="value">
<span class="timer" data-from="30" data-to="658" data-speed="800" data-refresh-interval="30">
<?php echo $this->reff->jumlahCostumerAgen();?>
</span>
</span>
</div>
</div>



<div class="col-lg-6 col-sm-6 col-xs-12 black sadow5">
<div class="main-box infographic-box">
<i class="fa fa-user emerald-bg"></i>
<span class="headline">Vendor</span>
<span class="value">
<span class="timer" data-from="30" data-to="658" data-speed="800" data-refresh-interval="30">
<?php echo $this->reff->jumlahOwnerAgen();?>
</span>
</span>
</div>
</div>



 
</div>









<div class="row">
 
 <div class="col-lg-6 black">
<div class="main-box no-header clearfix">
<div class="main-box-body clearfix">
<div class="table-responsive">
<table class="table user-list table-hover">
<thead>
<tr>
<th><span>Top Agen</span></th>
<th class="text-center"><span>  Closing</span></th>
 
 
</tr>
</thead>
<tbody> 

<?php 
$dataTopAgen=$this->reff->getTopAgen(5,$today);
foreach($dataTopAgen as $vala)
{
?>

<tr>
<td>
<img src="<?php echo base_url()?>file_upload/agen/<?php echo $this->reff->getPotoAgen($vala->kode_agen);?>" alt=""/>
<a href="#" class="user-link"><?php echo $this->reff->getNamaAgen($vala->kode_agen);?></a>
 <!--<span class="user-subhead">Rp <?php echo number_format($this->reff->jumlahOmzetByAgenByKode($vala->kode_agen),0,",",".");?></span>-->
 <span class="user-subhead"><?php echo number_format($vala->biaya,0,",",".");?></span>
</td>
 
<td class="text-center" style="font-size:17px">
<span class="label label-info"><?php echo $vala->j;?></span>
</td>
</tr>
 
<?php } ?>
</tbody>
</table>
</div>
</div>
</div>
</div>
 
 
 
 
 
<div class="col-lg-6 col-md-6">
<div class="main-box no-header clearfix">
<div class="main-box-body clearfix">
<div class="table-responsive">
<table class="tabel table-bordered table-hover black">
<thead>
<tr>
<th><span>&nbsp;Summary</span></th>
<th class="text-center"><span>  Listing</span></th>
 
 
</tr>
</thead>
<tbody> 

<?php 
$getTopProp=$this->reff->getTopPropAgen(10);
foreach($getTopProp as $top)
{
?>

<tr>
<td>
 <?php echo $this->reff->getNamaJenis($top->jenis_prop);?> 
</td>
 
<td class="text-center black">
 <b><?php echo $top->jml;?></b>
</td>
</tr>
 
<?php } ?>
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
 
 
 </div>
 </div>
 
 <script>
 setTimeout(function(){   $('.history').scrollTop(100000000000000); }, 1000);
 </script>
 
 
 
 <script>
	getHistory();
	function getHistory()
	{		 $('.datahistory').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Mohon Tunggu ... ");
	            $.ajax({
                type: "POST",
            
                url: "<?php echo base_url();?>dashboard_ag/getHistoryTimeLine/",
                success: function (data) {
			       $(".datahistory").html(data);
				   $('.history').scrollTop(100000000000000);
                }
            });
	}
</script>	

<script>
	function simpan()
	{		
			var text = $("[name='textListing']").val();
			$('.datahistory').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Mohon Tunggu ... ");
	            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>dashboard_ag/saveReport/",
                data: "text="+text,
                success: function (data) {
					getHistory();
			        $("[name='textListing']").val("");
                }
            });
	}
	function hapus(id)
	{	var a=window.confirm("Hapus ?");
		if(a==false){
			return false;
		}
		$('.datahistory').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Mohon Tunggu ... ");
		     $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>dashboard_ag/hapusTimeLine/"+id,
                success: function (data) {
					getHistory();
			    }
            });
	}
	
	</script>