<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_ag extends CI_Controller {

	

	function __construct()
	{
		parent::__construct();	
		$this->m_konfig->validasi_session(array("agen","user","dm"));
	}
	
	function _template($data)
	{
	$this->load->view('template/main',$data);	
	}
	
	public function index()
	{

	$data['konten']="informasi";
	$this->_template($data);
	}
	function getHistoryTimeLine()
	{
	 
		 $time=$this->reff->dataTimeLine();
		 $hapus="";$sesi=$this->session->userdata("kode");$id=$this->session->userdata("id");
		 foreach($time as $time )
		 {	 
	
		if($time->kode_agen==$sesi)
		{
			  $hapus="| <a href='#' style='color:red' onclick='return hapus(`".$time->id."`)'>Delete</a>";
		}else{
			$hapus="";
		}
	
		if($time->kode_agen=="admin")
		 {
			 $dir="dp";
			 $img='<img src="'.base_url().'file_upload/dp/'.$this->reff->photoAdmin("11").'" alt=""/>';
			 
		 }else{
			  $dir="agen";
			   $img='<img src="'.base_url().'file_upload/agen/'.$this->reff->getPotoAgen($time->kode_agen).'" alt=""/>';
		 }
			 
		if($sesi=="admin")
		{
			 $hapus="| <a href='#' style='color:red' onclick='return hapus(`".$time->id."`)'>Delete</a>";
		}			
			 
			 
			
		 ?>
				<li class="clearfix">
				<div class="img">
				<?php echo $img; ?>
				</div>
				<div class="title">
				<a href="#"><?php echo $this->reff->getNamaAgen($time->kode_agen);?></a> 
				</div>
				 
				<div class="photos clearfix">

				<div class="item">
				<?php echo $time->ket;?>
				</div>

				</div>
				<div class="time-ago">
				<i class="fa fa-calendar"></i> <?php echo SUBSTR($this->tanggal->indjam($time->tgl,"/"),0,10);?>  <?php echo $hapus; ?>
				</div>
				</li>

		<?php }  
	}
	function saveReport()
	{
		$kode=$this->session->userdata("kode");
		$data=array(
		"kode_agen"=>$kode,
		"ket"=>$this->input->post("text"),
		);
		echo $this->db->insert("timeline",$data);
	}
	function hapusTimeLine($id)
	{
		$this->db->where("id",$id);
		echo $this->db->delete("timeline");
	}
	
}

