<!-----------------------------------------	|
|	ID PRODUCT   : 1101201602				|
|	APP NAME     : SMSVISION				|
|   APP VERSION  : 2.1						|
|   Product by   : DivisionIT				|
|   Website      : Http://smsvision.net		|
 ------------------------------------------->

<?php $con=new konfig(); ?> 
<meta charset="UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<title>  <?php echo $con->konfigurasi(2); ?> </title>
 
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>plug/boostrap/css/bootstrap/bootstrap.min.css"/>


<script src="<?php echo base_url();?>plug/js/jquery-2.2.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>plug/boostrap/css/libs/font-awesome.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>plug/boostrap/css/libs/nanoscroller.css"/>
 
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>plug/boostrap/css/compiled/theme_styles3.css"/>
 <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>plug/boostrap/css/libs/nifty-component.css"/>
<link rel="stylesheet" href="<?php echo base_url();?>plug/boostrap/css/libs/select2.css" type="text/css"/>
 
<link type="image/x-icon" href="<?php echo base_url();?>file_upload/img/<?php echo $con->konfigurasi(1); ?> " rel="shortcut icon"/>
  <link href="<?php echo base_url();?>plug/datatables/css/dataTables.bootstrap.css" rel="stylesheet">
<script src="<?php echo base_url();?>plug/input-tag/dist/js/standalone/selectize.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>plug/tag-select/pickout.css">
<script src="<?php echo base_url();?>plug/input-tag/index.js"></script>
 <script type="text/javascript" src="<?php echo base_url();?>plug/js/myjs.js"></script>	
<script>
$('head').append('<link rel="stylesheet" href="<?php echo base_url();?>plug/input-tag/dist/css/selectize.css">');
</script>
<?php

?>

<style>
.spesial{
text-shadow: 2px 2px 2px #999999;
}
.sadow{
text-shadow: 1px 1px 1px #999999;
}.sadow05{
text-shadow: 0.5px 0.5px 0.5px #999999;
}
.black{
color:black;
}

.bold{
font-weight:bold;
}
.tabel{
width:100%;
 font-family: times;
}
.tabel tr td
{
padding:2px;
padding-left:6px;
}
.thead{
padding:5px;
}
.b{
font-weight:bold;}
.imgborder
{
border-radius:10px;
}
.sr130
{
font-size:130%;
}
.sr135
{
font-size:135%;
}
.sr180
{
font-size:180%;
}
.sr170
{
font-size:170%;
}
.sr160
{
font-size:160%;
}
.sr120
{
font-size:120%;
}.sr110
{
font-size:110%;
}
.sr100
{
font-size:100%;
}
.sr80
{
font-size:80%;
}.sr140
{
font-size:140%;
}
.sr90
{
font-size:90%;
}

.cursor{
cursor:pointer;
}
</style>
<script>
function loadPage()
{
$('#konten').html("<img src='<?php echo base_url();?>/plug/img/load.gif'> Please wait...");
}
</script>
<!--[if lt IE 9]>
		<script src="js/html5shiv.js"></script>
		<script src="js/respond.min.js"></script>
	<![endif]-->
