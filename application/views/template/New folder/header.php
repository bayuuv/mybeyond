<?php $con=new konfig(); ?> 
<?php
$sesi=$this->session->userdata("level");
if(!$sesi){ redirect("login/logout"); }?>
<header class="navbar" id="header-navbar">
<div class="container" >
<a href="" id="logo" class="navbar-brand">
<img  src="<?php echo base_url();?>file_upload/img/<?php echo $con->konfigurasi(1); ?>" alt=""  class="normal-logo pull-left"/>

<span style='font-size:24px;text-shadow: 2px 2px 2px #A6250C;' class="headerText"><?php echo $this->m_konfig->headerText(); ?></span>
</a>
<div class="clearfix" >
<button class="navbar-toggle" data-target=".navbar-ex1-collapse" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="fa fa-bars"></span>
</button>
<div class="nav-no-collapse navbar-left pull-left hidden-sm hidden-xs">
<ul class="nav navbar-nav pull-left">
<li>
<a class="btn" id="make-small-nav">
<i class="fa fa-bars"></i>
</a>
</li>
</ul>
</div>




<div class="nav-no-collapse" id="header-nav" >

<ul class="nav navbar-nav pull-right"  >


<li class="dropdown hidden-xs" id="newSMS">
<a class="btn dropdown-toggle" data-toggle="dropdown">
<i class="fa fa-bell"></i>
<span id="countInbox"></span>
</a>
<ul class="dropdown-menu notifications-list" style='margin-left:-140px'>
<li class="pointer">
<div class="pointer-inner">
<div class="arrow"></div>
</div>
</li>
<li class="item-header"><span class="ketNot"> Tidak ada </span> <span class="jmlNotif"></span> pesan baru</li>

<span class="isiSMsBaru">
</span>

<li class="item-footer">
<a href="<?php echo base_url();?>sms/inbox">
Lihat Pesan Masuk
</a>
</li>
</ul>
</li>




<?php $con=new konfig(); $dp=$con->dataProfile($this->session->userdata("id")); ?> 

<li class="dropdown hidden-xs">
<a class="btn dropdown-toggle" data-toggle="dropdown">
<i class="fa fa-cogs"></i>
</a>
<ul class="dropdown-menu notifications-list" style='margin-left:-140px'>
<li class="pointer">
<div class="pointer-inner">
<div class="arrow"></div>
</div>
</li>

<li class="item-headerd" style="font-weight:bold;padding:10px"><i class="fa fa-cog"></i> Pengaturan</li>
<li class="item">
	<a href="<?php echo base_url(); ?>profile"> <i class="fa fa-user"></i>
		<div class="task-info">
			<div class="desc"> Akun Profile</div>
		</div>
		
	</a>
</li>
<li class="item">
	<a href="javascript:modalHeader()"> <i class="fa fa-key"></i>
		<div class="task-info">
			<div class="desc">Key</div>
		</div>
	</a>
</li>
<li class="item">
<a href="javascript:nomorBlacklist()"> <i class="fa fa-list-ol"></i>
		<div class="task-info">
			<div class="desc">Nomor Blacklist</div>
		</div>
	</a>
</li>
<li class="item">
<a href="javascript:ModalForm()"> <i class="fa fa-edit"></i>
		<div class="task-info">
			<div class="desc">Customize Field</div>
		</div>
	</a>
</li>
<li class="item">
<a href="javascript:ModalSMSReg()"> <i class="fa fa-slack"></i>
		<div class="task-info">
			<div class="desc">Aktifasi SMS </div>
		</div>
	</a>
</li>

<li class="item">
<a href="javascript:autoDelete()"> <i class="fa fa-trash"></i>
		<div class="task-info">
			<div class="desc">Auto Delete SMS </div>
		</div>
	</a>
</li>

<li class="item">
   <a href="javascript:themeApp()"><i class='fa fa-tint'></i> 
		<div class="task-info">
			<div class="desc">Tampilan Aplikasi</div>
		</div>
	</a>
</li>
<li class="item-footer">
<a href="#">
No Center : <?php echo $this->m_konfig->no_center();?>
</a>
</li>
</ul>
</li>





<li class="dropdown profile-dropdown">
<a href="#" class="dropdown-toggle " data-toggle="dropdown">
<img src="<?php echo base_url();?>file_upload/dp/<?php echo $dp->poto;?>" alt=""/>
<span class="hidden-xs spesial"><?php echo $dp->owner; ?></span> <b class="caret"></b>
</a>
<?php echo $this->load->view("template/dropdown");?>
</li>

<!--
<li class="dropdown language hidden-xs">
<a class="btn dropdown-toggle" data-toggle="dropdown">
English
<i class="fa fa-caret-down"></i>
</a>
<ul class="dropdown-menu">
<li class="item">
<a href="#">
Spanish
</a>
</li>
<li class="item">
<a href="#">
German
</a>
</li>
<li class="item">
<a href="#">
Italian
</a>
</li>

</ul>
-->
</li>
</ul>
</div>



</div>
</div>
</header>



<script>
function modalHeader()
{
$("#modalHeader").modal("show");
}
function nomorBlacklist()
{
$("#nomorBlacklist").modal("show");
}

function ModalForm()
{
$("#ModalForm").modal("show");
loadModalForm();
}
function tanyaSMS()
{
$("#ModaltanyaSMS").modal("show");
}function tanyaSMSBalasan()
{
$("#ModaltanyaSMSBalasan").modal("show");
}function tanyaSMSKirim()
{
$("#ModaltanyaSMSKirim").modal("show");
}
function tanyaKirimUlang()
{
$("#ModaltanyaKirimUlang").modal("show");
}

function ModalSMSReg()
{
	   setTimeout(function(){ cekbalasanreg(); }, 300);
$("#ModalSMSReg").modal("show");
//------
$.ajax({
            url : "<?php echo base_url()?>welcome/getDataGroup",
			dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               $('[name="smsReg"]').val(data.aktifasi_sms_reg);
               $('[name="smsBalasan"]').val(data.aktifasi_balasan);
               $('[name="smsFormat"]').val(data.aktifasi_kirim);
               $('[name="kontenSmsReg"]').val(data.sms_reg);
               $('[name="kontenSmsUnreg"]').val(data.sms_unreg);
               $('[name="kirimUlang"]').val(data.kirim_ulang);
            }
});
//----------			
}

function autoDelete()
{
$("#ModalautoDelete").modal("show");
//------		
}

function saveHeaderKode()
{
  var headerText=$("[name='header']").val();
  $.ajax({
            url : "<?php echo base_url()?>profile/saveHeaderKode",
            type: "POST",
            data: $('#formModalHeader').serialize(),
            dataType: "JSON",
            success: function(data)
            {	
				if(data==false)
				{
					alert("Key akun tida tersedia\nMohon cari key lain");
					return false;
				}
               //if success close modal and reload ajax table
               $('#modalHeader').modal('hide');
               $('.headerText').html(headerText);
            
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert("Key akun tidak tersedia\nMohon cari key lain");
		    }
        });
}

function generate()
{
  $('#keyapi').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Generating...");
  $.ajax({
            url : "<?php echo base_url()?>profile/generate",
            success: function(data)
            {
               //if success close modal and reload ajax table
               $('#keyapi').html(data);
            
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
}

function saveBlacklist()
{
  $.ajax({
            url : "<?php echo base_url()?>profile/saveBlacklist",
            type: "POST",
            data: $('#formBlacklist').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               $('#nomorBlacklist').modal('hide');
            
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
}
function updateForm()
{
		$.ajax({
            url : "<?php echo base_url()?>profile/updateForm",
            type: "POST",
            data: $('#formForm').serialize(),
            success: function(data)
            {
               //if success close modal and reload ajax table
              loadModalForm();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
}

function saveFormField()
{
		$.ajax({
            url : "<?php echo base_url()?>profile/saveFormField",
            type: "POST",
            data: $('#saveFormField').serialize(),
            success: function(data)
            {
               //if success close modal and reload ajax table
			   loadModalForm();
			    $("#saveFormField")[0].reset();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
}

function hapusForm(id)
{
var tanya=window.confirm("Update Data Form Akan Menghapus Seluruh Kontak\nLanjutkan Hapus?");
if(tanya==false){ return false;}
$(".hapusForm"+id).html("");
updateForm();
}

function themeApp()
{
$("#themeApp").modal("show");
}

function loadModalForm()
{
			$.ajax({
            url : "<?php echo base_url()?>profile/dataForm",
            success: function(data)
            {
               //if success close modal and reload ajax table
               $('#viewForm').html(data);
            
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
}
function execAktifasi()
{	$("#msgexecAktifasi").show();
	$('#msgexecAktifasi').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Please wait...");
	$.ajax({
	url:"<?php echo base_url();?>welcome/execAktifasi",
	type: "POST",
    data: $('#formAktifasi').serialize(),
	success: function(data)
            {
			$("#msgexecAktifasi").hide();
			$("#ModalSMSReg").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Try Again!');
            }
	});
}

function execAutoDelete()
{	$("#msgautodelete").show();
	$('#msgautodelete').html("<img src='<?php echo base_url();?>plug/img/load.gif'> Please wait...");
	$.ajax({
	url:"<?php echo base_url();?>welcome/execAutoDelete",
	type: "POST",
    data: $('#formAutoDelete').serialize(),
	success: function(data)
            {
			$("#msgautodelete").hide();
			$("#ModalautoDelete").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Try Again!');
            }
	});
}
</script>


  <!-- Bootstrap modal -->
  <div class="modal fade" id="themeApp" role="dialog" style="margin-left:-400px;margin-top:-100px">
  <div class="modal-dialog">
   
		   
		   <div id="config-tool" class="closed">
<a  class='pull-right cursor' style="padding:5px" data-dismiss="modal" aria-label="Close">
<i class="fa fa-times-circle"></i>
</a>
<div id="config-tool-options">
<h4>Tampilan Aplikasi</h4>
<ul>
<li>
<div class="checkbox-nice">
<input type="checkbox" id="config-fixed-header"/>
<label for="config-fixed-header">
Fixed Header
</label>
</div>
</li>
<li>
<div class="checkbox-nice">
<input type="checkbox" id="config-fixed-sidebar"/>
<label for="config-fixed-sidebar">
Fixed Left Menu
</label>
</div>
</li>
<li>
<div class="checkbox-nice">
<input type="checkbox" id="config-fixed-footer"/>
<label for="config-fixed-footer">
Fixed Footer
</label>
</div>
</li>
<li>
<div class="checkbox-nice">
<input type="checkbox" id="config-boxed-layout"/>
<label for="config-boxed-layout">
Boxed Layout
</label>
</div>
</li>

</ul>

<h4>Skin Color</h4>
<ul id="skin-colors" class="clearfix">
<li>
<a class="skin-changer" data-skin="theme-navyBlue" data-toggle="tooltip" title="Navy Blue" style="background-color: #34495e;">
</a>
</li>
<li>
<a class="skin-changer" data-skin="theme-white" data-toggle="tooltip" title="White/Green" style="background-color: #2B93B5;">
</a>
</li>
<li>
<a class="skin-changer blue-gradient" data-skin="theme-blue-gradient" data-toggle="tooltip" title="Gradient">
</a>
</li>
<li>
<a class="skin-changer" data-skin="theme-greenSea" data-toggle="tooltip" title="Green Sea" style="background-color: #F21487;">
</a>
</li>
<li>
<a class="skin-changer" data-skin="theme-amethyst" data-toggle="tooltip" title="Amethyst" style="background-color: #950223;">
</a>
</li>
<li>
<a class="skin-changer" data-skin="theme-blue" data-toggle="tooltip" title="Pink" style="background-color: rgba(0,138,92,1);">
</a>
</li>
<li>
<a class="skin-changer" data-skin="theme-red" data-toggle="tooltip" title="Red" style="background-color: #e74c3c;">
</a>
</li>
<li>
<a class="skin-changer" data-skin="theme-whbl" data-toggle="tooltip" title="White/Blue" style="background-color: #1ABC9C;">
</a>
</li>
</ul>
</div>
</div>
		   
		   
		  
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
  
  

  <!-- Bootstrap modal -->
  <div class="modal fade" id="modalHeader" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
     
      <div class="modal-body form">
<section class="content">
  <div class="row">
    <div class="col-lg-12">
	
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-key"></i> Key </h4>
      </div>
	
      <!-- general form elements -->
      <div class="box box-primary black">	  
        
          <div class="box-body">                      
            <form  action="javascript:saveHeaderKode();" id="formModalHeader" method="post" >
              <!--  <div class="form-group">Header-->
				  <input required name="header" type="hidden" class="form-control" value="<?php echo $this->m_konfig->headerText();?>">
            <!--    </div>  -->
				
				<div class="form-group">Key Akun
				<span class="pull-right" style="color:brown;font-size:12px">digunakan untuk kirim sms,add kontak via format sms</span>
				  <input required name="kode" type="text" class="form-control" value="<?php echo $this->m_konfig->kodeAkun();?>">
                </div>
				
			
				<span class="pull-right" style="color:brown;font-size:12px;margin-top:-10">digunakan untuk kirim sms via Http / API</span>
				Key API SMS
				<div class="form-inline form-inline-box" style="margin-top:0px" >
				<div class="form-groups">
				<span id="keyapi"><?php echo $this->m_konfig->kodeApi();?></span>
				<div class="form-grouep pull-right">
							<a href="#" class='fa fa-refresh' onclick="generate()"> Generate Key Baru</a>	
				</div>
				</div>
				</div>
				
				<hr>
			
                <button type="submit" class="btn btn-primary pull-right">
                    <span class="fa fa-save"></span>&nbsp;Simpan
                </button>
                <div class="form-group">
                    <div class="msg"></div>
                    <div class="hasil"></div>
                </div>
            </form>
          </div><!-- /.box-body -->
      </div><!-- /.box -->
	     
    </div>
  </div>   <!-- /.row -->

</section><!-- /.content -->

          </div>
         
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
  
  
  <!-- Bootstrap modal -->
  <div class="modal fade" id="nomorBlacklist" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
     
      <div class="modal-body form">
<section class="content">
  <div class="row">
    <div class="col-lg-12">
	
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-list-ol"></i> Nomor Blacklist</h4>
      </div>
	
      <!-- general form elements -->
      <div class="box box-primary black">	  
        
          <div class="box-body">                      
            <form  action="javascript:saveBlacklist();" id="formBlacklist" method="post" >
                <div class="form-group">Nomor Blacklist
				<input id="nomorList" name="nomor" type="text" class="demo-default"  value="<?php echo $this->m_konfig->nomorBlokir();?>" placeholder="Input Nomor Hp">
                </div>	   			
				<script>
				$('#nomorList').selectize({
					persist: false,
					createOnBlur: true,
					create: true
				});
				</script>

                <button type="submit" class="btn btn-primary pull-right">
                    <span class="fa fa-save"></span>&nbsp;Simpan
                </button>
                <div class="form-group">
                    <div class="msg"></div>
                    <div class="hasil"></div>
                </div>
            </form>
          </div><!-- /.box-body -->
      </div><!-- /.box -->
	     
    </div>
  </div>   <!-- /.row -->

</section><!-- /.content -->

          </div>
         
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
  
  
  
  
  <!-- Bootstrap modal -->
  <div class="modal fade" id="ModalForm" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
     
      <div class="modal-body form">
<section class="content">
  <div class="row">
    <div class="col-lg-12">
	
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-edit"></i> Customize Field</h4>
      </div>
	
      <!-- general form elements -->
      <div class="box box-primary black">	  
        
          <div class="box-body">                   
           <form class="form-inlined" role="form" id='saveFormField'>
			<div class="form-group">
			<label class="sr-onldy" for="newform"> Tambahkan Field Baru </label><br>
			<input type="text" class="form-control" name="formField" id="newform" style="width:80%" >
			<button type="button" class="btn btn-primary pull-right" style="margin-top:-35px" onclick="saveFormField()"><i class="fa fa-plus"></i> Tambahkan</button>
			</div>
			
			</form>
			<hr>
			<div id="viewForm"></div>
			
          </div><!-- /.box-body -->
      </div><!-- /.box -->
	     
    </div>
  </div>   <!-- /.row -->

</section><!-- /.content -->

          </div>
         
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
    
  
  
  
  
  
  <!-- Bootstrap modal -->
  <div class="modal fade" id="ModalSMSReg" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
     
      <div class="modal-body form">
<section class="content">
  <div class="row">
    <div class="col-lg-12">
	
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-slack"></i> Aktifasi SMS </h4>
      </div>
	
      <!-- general form elements -->
      <div class="box box-primary black">	  
        
          <div class="box-body">                   
         
		 
		 
		 
		 
		 
<form class="black" id="formAktifasi" name="formAktifasi" action="javascript:void()">

<div class="form-inline form-inline-box">
<div class="form-group cursor" onclick="tanyaSMS()">
<i class="fa fa-question-circle " style="color:green" ></i> SMS Registrasi
</div>
<div class="form-group pull-right">
<select class="form-control" style="margin-top:-7px" name="smsReg">
<option value="1">Aktif</option>
<option value="0">Tidak Aktif</option>
</select>
</div>
</div>

<div class="form-inline form-inline-box">
<div class="form-group cursor" onclick="tanyaSMSBalasan()">
<i class="fa fa-question-circle " style="color:green" ></i> SMS Balasan Registrasi 
</div>
<div class="form-group pull-right">
<select class="form-control" style="margin-top:-7px" name="smsBalasan" onclick="cekbalasanreg()">
<option value="1">Aktif</option>
<option value="0">Tidak Aktif</option>
</select>
</div>
</div>

<script>

function cekbalasanreg()
{
var cekbal=$("[name='smsBalasan']").val();
if(cekbal=="0"){ $("#kontensms").hide(); }else
	{	$("#kontensms").show(); }
}
</script>

<div class="form-inline form-inline-box">
<div class="form-group cursor" onclick="tanyaSMSKirim()">
<i class="fa fa-question-circle " style="color:green" ></i> Kirim SMS Dengan Format
</div>
<div class="form-group pull-right">
<select class="form-control" style="margin-top:-7px" name="smsFormat">
<option value="1">Aktif</option>
<option value="0">Tidak Aktif</option>
</select>
</div>
</div>
<!--
<div class="form-inline form-inline-box">
<div class="form-group cursor" onclick="tanyaKirimUlang()">
<i class="fa fa-question-circle " style="color:green" ></i> Kirim Ulang SMS Gagal
</div>
<div class="form-group pull-right">
<select class="form-control" style="margin-top:-7px" name="kirimUlang">
<option value="1">Aktif</option>
<option value="0">Tidak Aktif</option>
</select>
</div>
</div>

-->

<hr>
<span id="kontensms">
Konten SMS Balasan Registrasi:
<textarea class="form-control" name="kontenSmsReg"></textarea>
<span class="pull-right" style="color:brown;font-size:12px">Kode : {NAMA} otomatis akan menjadi nama pendaftar</span>
<br>
Konten SMS Balasan Unregistrasi:
<textarea class="form-control" name="kontenSmsUnreg"></textarea>
</span>




<hr>
<span id="msgexecAktifasi"></span>
<button class="pull-right btn btn-primary" onclick="execAktifasi()"><i class="fa fa-save"></i> Simpan</button>		 
	</form>	 
		 
		 
		 
		 
	
			
			
			
          </div><!-- /.box-body -->
      </div><!-- /.box -->
	     
    </div>
  </div>   <!-- /.row -->

</section><!-- /.content -->

          </div>
         
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
  
  
  
  
  
  
  
  
  
  
  <!-- Bootstrap modal -->
  <div class="modal fade" id="ModalautoDelete" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
     
      <div class="modal-body form">
<section class="content">
  <div class="row">
    <div class="col-lg-12">
	
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-trash"></i> Auto Delete SMS </h4>
      </div>
	
      <!-- general form elements -->
      <div class="box box-primary black">	  
        
          <div class="box-body">                   
         
		 
		 
		 
		 
		 
<form class="black" id="formAutoDelete" name="formAutoDelete" action="javascript:void()">

<div class="form-inline form-inline-box">
<div class="form-group cursor" >
 Batas Maksimal SMS Inbox
</div>
<div class="form-group pull-right" style="margin-top:-7px">
<input type="number" name="delInbox"  class="form-control" value="<?php echo $this->m_konfig->batasInbox();?>">
</div>
</div>
</div>

<div class="form-inline form-inline-box">
<div class="form-group cursor">
 Batas Maksimal SMS Terkirim
</div>
<div class="form-group pull-right" style="margin-top:-7px">
<input type="number" name="delOutbox"  class="form-control" value="<?php echo $this->m_konfig->batasOutbox();?>">
</div>
</div>
<span  style="color:brown;font-size:12px">*Jika SMS Melebihi batas yang ditentukan maka sms sebelumnya akan terhapus otomatis.</span>
<hr>
<span id="msgautodelete"></span>
<button class="pull-right btn btn-primary" onclick="execAutoDelete()"><i class="fa fa-save"></i> Simpan</button>		 
	</form>	 
		 
		 
		 
		 
	
			
			
			
          </div><!-- /.box-body -->
      </div><!-- /.box -->
	     
    </div>
  </div>   <!-- /.row -->

</section><!-- /.content -->

          </div>
         
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
   
  
  
  
  
  
  
  
  <!-- Bootstrap modal -->
  <div class="modal fade" id="ModaltanyaSMS" role="dialog" >
  <div class="modal-dialog">
    <div class="modal-content">
     
      <div class="modal-body form">
<section class="content">
  <div class="row">
    <div class="col-lg-12">
	
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-circle"></i> SMS Registrasi </h4>
      </div>
	
      <!-- general form elements -->
      <div class="form-inline form-inline-box">
<div class="form-group black">	  
        SMS Registrasi adalah Format SMS yang di kirim ke nomor center untuk menambahkan nomor ke dalam group kontak secara otomatis.<br>
		Format SMS Registrasi adalah : <font color="red">REG#<?php echo $this->m_konfig->kodeAkun();?>#NAMA GROUP#NAMA#</font><?php echo $this->m_konfig->getTitle(); ?> <br>
		Format yang berwarna merah merupakan format wajib disertakan lainnya hanya opsional.
		
        
      </div><!-- /.box -->
      </div><!-- /.box -->
	     
    </div>
  </div>   <!-- /.row -->

</section><!-- /.content -->

          </div>
         
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal --> 
  
  
  
  <!-- Bootstrap modal -->
  <div class="modal fade" id="ModaltanyaSMSBalasan" role="dialog" >
  <div class="modal-dialog">
    <div class="modal-content">
     
      <div class="modal-body form">
<section class="content">
  <div class="row">
    <div class="col-lg-12">
	
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-circle"></i> SMS Balasan Registrasi </h4>
      </div>
	
      <!-- general form elements -->
      <div class="form-inline form-inline-box">
<div class="form-group black">	  
        SMS Balasan Registrasi merupakan sms yang dikirim secara otomatis ketika seseorang berhasil Registrasi/Unregistrasi.
		
        
      </div><!-- /.box -->
      </div><!-- /.box -->
	     
    </div>
  </div>   <!-- /.row -->

</section><!-- /.content -->

          </div>
         
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
  
  
   <!-- Bootstrap modal -->
  <div class="modal fade" id="ModaltanyaSMSKirim" role="dialog" >
  <div class="modal-dialog">
    <div class="modal-content">
     
      <div class="modal-body form">
<section class="content">
  <div class="row">
    <div class="col-lg-12">
	
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-circle"></i> Kirim SMS Dengan Format </h4>
      </div>
	
      <!-- general form elements -->
      <div class="form-inline form-inline-box">
<div class="form-group black">	  
       Mengirim SMS secara masal ke Group kontak yang telah dibuat via sms format.<br>
	   Untuk mengirim sms ke group adalah : <font color="red">KIRIM#NAMA GROUP#ISI PESAN</font><br>
	   Panjang karakter sms yang dikirim tidak melibihi 1 slide SMS.
		
        
      </div><!-- /.box -->
      </div><!-- /.box -->
	     
    </div>
  </div>   <!-- /.row -->

</section><!-- /.content -->

          </div>
         
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal --> 
  
  
   <!-- Bootstrap modal -->
  <div class="modal fade" id="ModaltanyaKirimUlang" role="dialog" >
  <div class="modal-dialog">
    <div class="modal-content">
     
      <div class="modal-body form">
<section class="content">
  <div class="row">
    <div class="col-lg-12">
	
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title black"><i class="fa fa-circle"></i> Kirim Ulang SMS Gagal </h4>
      </div>
	
      <!-- general form elements -->
      <div class="form-inline form-inline-box">
<div class="form-group black">	  
      Memungkinkan Sistem secara otomatis mengirim ulang sms yang berstatus gagal kirim. <br>
	  Sms gagal kirim dapat disebabkan karena gangguan signal provider ,Nomor tujuan sudah tidak aktif atau pulsa dalam keadaan kosong.
		
        
      </div><!-- /.box -->
      </div><!-- /.box -->
	     
    </div>
  </div>   <!-- /.row -->

</section><!-- /.content -->

          </div>
         
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
  