<?php $con=new konfig(); ?> 
<?php
$sesi=$this->session->userdata("level");
if(!$sesi){ redirect("login/logout"); }?>
<header class="navbar" id="header-navbar">
<div class="container" >
<a href="" id="logo" class="navbar-brand">
<img  src="<?php echo base_url();?>file_upload/img/<?php echo $con->konfigurasi(1); ?>" alt=""  class="normal-logo pull-left"/>

<span style='font-size:24px;text-shadow: 2px 2px 2px #A6250C;' class="headerText"><?php echo $this->m_konfig->headerText(); ?></span>
</a>
<div class="clearfix" >
<button class="navbar-toggle" data-target=".navbar-ex1-collapse" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="fa fa-bars"></span>
</button>
<div class="nav-no-collapse navbar-left pull-left hidden-sm hidden-xs">
<ul class="nav navbar-nav pull-left">
<li>
<a class="btn" id="make-small-nav">
<i class="fa fa-bars"></i>
</a>
</li>
</ul>
</div>




<div class="nav-no-collapse" id="header-nav" >

<ul class="nav navbar-nav pull-right"  >






<?php $con=new konfig(); $dp=$con->dataProfile($this->session->userdata("id")); ?> 

<li class="dropdown hidden-xs">

</li>





<li class="dropdown profile-dropdown">
<a href="#" class="dropdown-toggle " data-toggle="dropdown">
<img src="<?php echo base_url();?>file_upload/dp/<?php echo $dp->poto;?>" alt=""/>
<span class="hidden-xs spesial"><?php echo $dp->owner; ?></span> <b class="caret"></b>
</a>
<?php echo $this->load->view("template/dropdown");?>
</li>


</li>
</ul>
</div>



</div>
</div>
</header>
<script>


function themeApp()
{
$("#themeApp").modal("show");
}

</script>


  <!-- Bootstrap modal -->
  <div class="modal fade" id="themeApp" role="dialog">
  <div class="modal-dialog">
   
		   
		   <div id="config-tool" class="closed">
<a  class='pull-right cursor' style="padding:5px" data-dismiss="modal" aria-label="Close">
<i class="fa fa-times-circle"></i>
</a>
<div id="config-tool-options">
<h4>Tampilan Aplikasi</h4>
<ul>
<li>
<div class="checkbox-nice">
<input type="checkbox" id="config-fixed-header"/>
<label for="config-fixed-header">
Fixed Header
</label>
</div>
</li>
<li>
<div class="checkbox-nice">
<input type="checkbox" id="config-fixed-sidebar"/>
<label for="config-fixed-sidebar">
Fixed Left Menu
</label>
</div>
</li>
<li>
<div class="checkbox-nice">
<input type="checkbox" id="config-fixed-footer"/>
<label for="config-fixed-footer">
Fixed Footer
</label>
</div>
</li>
<li>
<div class="checkbox-nice">
<input type="checkbox" id="config-boxed-layout"/>
<label for="config-boxed-layout">
Boxed Layout
</label>
</div>
</li>

</ul>

<h4>Skin Color</h4>
<ul id="skin-colors" class="clearfix">
<li>
<a class="skin-changer" data-skin="theme-navyBlue" data-toggle="tooltip" title="Navy Blue" style="background-color: #34495e;">
</a>
</li>
<li>
<a class="skin-changer" data-skin="theme-white" data-toggle="tooltip" title="White/Green" style="background-color: #2B93B5;">
</a>
</li>
<li>
<a class="skin-changer blue-gradient" data-skin="theme-blue-gradient" data-toggle="tooltip" title="Gradient">
</a>
</li>
<li>
<a class="skin-changer" data-skin="theme-greenSea" data-toggle="tooltip" title="Green Sea" style="background-color: #F21487;">
</a>
</li>
<li>
<a class="skin-changer" data-skin="theme-amethyst" data-toggle="tooltip" title="Amethyst" style="background-color: #950223;">
</a>
</li>
<li>
<a class="skin-changer" data-skin="theme-blue" data-toggle="tooltip" title="Pink" style="background-color: rgba(0,138,92,1);">
</a>
</li>
<li>
<a class="skin-changer" data-skin="theme-red" data-toggle="tooltip" title="Red" style="background-color: #e74c3c;">
</a>
</li>
<li>
<a class="skin-changer" data-skin="theme-whbl" data-toggle="tooltip" title="White/Blue" style="background-color: #1ABC9C;">
</a>
</li>
</ul>
</div>
</div>
		   
		   
		  
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
  
  

  
  
  